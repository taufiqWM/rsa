﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;

namespace RSA.Controllers.Other
{

    //[Authorize]
    public class AccountController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        //private string imgpath = "~/Images/RawImages";
        //private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public AccountController()
        {
            db = new QL_RSAEntities();
            db.Database.CommandTimeout = 0;
        }
        // GET: Account
        public ActionResult LogIn()
        {
            return View();
            //return RedirectToAction("Index.html", "Login");
        }
        [HttpPost]
        public ActionResult LogIn(QL_mstprof us)
        {
            if (string.IsNullOrEmpty(us.profoid))
                ModelState.AddModelError("usoid", "User ID harap di isi");
            if (string.IsNullOrEmpty(us.profpass))
                ModelState.AddModelError("uspassword", "Password harap di isi");

            if (ModelState.IsValid)
            {
                var user = db.QL_mstprof.Where(w => w.profoid.ToLower() == us.profoid.ToLower()).FirstOrDefault();
                
                if (user == null)
                {
                    ModelState.AddModelError("", "User ID atau Password Salah");
                }
                else if (user.profpass != us.profpass)
                {
                    ModelState.AddModelError("", "User ID atau Password Salah");
                }
                else
                {
                    Session["UserID"] = user.profoid;
                    Session["UserName"] = user.profname;
                    Session["CompnyCode"] = user.cmpcode;
                    Session["DivGroup"] = user.divgroupoid;
                    Session["UserLevel"] = user.proflevel;
                    Session["ApprovalLimit"] = 0;

                    if (user.proftype.ToUpper() == "ADMIN" | user.profoid.ToUpper() == "ADMIN")
                    {
                        sSql = "SELECT distinct mn.* FROM QL_m04MN mn WHERE mn.mnflag='ACTIVE'";
                    }
                    else
                    {
                        sSql = "SELECT distinct mn.* FROM QL_m04MN mn INNER JOIN QL_mstroledtl rld ON rld.mnoid=mn.mnoid INNER JOIN QL_mstuserrole ur ON ur.roleoid=rld.roleoid WHERE mn.mnflag='ACTIVE' AND ur.profoid='" + user.profoid + "'";
                    }
                    Session["Role"] = db.Database.SqlQuery<QL_m04MN>(sSql).ToList();
                    sSql = "SELECT DISTINCT REPLACE(mn.mnfileloc, ' ', '') formaddress, ur.special FROM QL_mstroledtl rdtl INNER JOIN QL_mstuserrole ur ON rdtl.roleoid=ur.roleoid INNER JOIN QL_m04MN mn ON mn.mnoid=rdtl.mnoid WHERE ur.special='Yes' AND ur.profoid='" + user.profoid + "'";
                    Session["SpecialAccess"] = db.Database.SqlQuery<RoleSpecial>(sSql).ToList();

                    FormsAuthentication.SetAuthCookie(user.profoid, false);
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(us);
        }
        // GET: /Profile/Update/UserID
        public ActionResult Update(string Id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
          

            if (string.IsNullOrEmpty(Id))
            {
                return HttpNotFound();
            }
            else
            {
                QL_mstprof tbl = db.QL_mstprof.Find(CompnyCode, Id);
                List<QL_mstprof> pr = db.QL_mstprof.ToList();
                if (tbl == null)
                    return HttpNotFound();
                else
                {
                    if (tbl.profapplimit == 1)
                        sSql = "SELECT personemail FROM QL_mstprof a INNER JOIN QL_mstperson p ON a.personoid=p.personoid WHERE profoid='" + tbl.profoid + "'";
                    else
                        sSql = "SELECT suppemail FROM QL_mstprof mp INNER JOIN QL_mstsupp ms ON mp.personoid=ms.suppoid WHERE profoid='" + tbl.profoid + "'";

                    tbl.profpass = "";
                    ViewBag.cbpasswordvalue = true;
                    ViewBag.cbemailvalue = true;
                    ViewBag.profemail = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                    return View(tbl);
                }
            }
        }

        // POST: Profile/Update
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(QL_mstprof tbl, bool cbpasswordvalue, string newprofpass, string confirmprofpass)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
           

            var oldpassword = db.QL_mstprof.Where(p => p.profoid == tbl.profoid).Select(p => p.profpass).FirstOrDefault();

            if (cbpasswordvalue)
            {
                if (string.IsNullOrEmpty(tbl.profpass))
                    ModelState.AddModelError("profpass", "This field is required.");
                else
                {
                    if (tbl.profpass != oldpassword)
                        ModelState.AddModelError("profpass", "Old Password is wrong.");
                }
                if (string.IsNullOrEmpty(newprofpass))
                    ModelState.AddModelError("newprofpass", "This field is required.");
                if (string.IsNullOrEmpty(confirmprofpass))
                    ModelState.AddModelError("confirmprofpass", "This field is required.");
                if (!string.IsNullOrEmpty(newprofpass) && !string.IsNullOrEmpty(confirmprofpass) && newprofpass != confirmprofpass)
                    ModelState.AddModelError("confirmprofpass", "Confirm New Password didn't match with New Password.");
            }

           

            if (ModelState.IsValid)
            {
                var servertime = ClassFunction.GetServerTime();
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.upduser = Session["UserID"].ToString();
                        tbl.updtime = servertime;
                        if (cbpasswordvalue)
                            tbl.profpass = newprofpass;
                        else
                            tbl.profpass = oldpassword;
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                      

                        objTrans.Commit();
                        return RedirectToAction("Index", "Dashboard");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }

            return View(tbl);
        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("LogIn", "Account");
        }

        // GET: Account
        public ActionResult NotAuthorize()
        {
            return View();
        }
        
    }
}