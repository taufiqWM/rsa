﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.Models;
using RSA.Models.DB;
using System.Data;



namespace RSA.Controllers
{
    public class DashboardController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Dashboard
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            //if (ClassFunction.checkPagePermission("Dashboard", (List<QL_m04MN>)Session["Role"]))
            //    return HttpNotFound();

            return View();
        }

        public class dbshipmentmodel
        {
            public string divname { get; set; }
            public decimal totalshipment { get; set; }
        }

        [HttpPost]
        public ActionResult GetNotifSOAppSPK()
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                string userID = Session["UserID"].ToString();
                sSql = "select us.* from QL_mstuserrole us Inner Join QL_mstroledtl rd on us.roleoid=rd.roleoid Inner Join QL_m04MN mn on mn.mnoid=rd.mnoid where mnname='SPK' and us.profoid='"+userID+"'";
                var dtrolesel = db.Database.SqlQuery<QL_mstuserrole>(sSql).ToList();
                if (dtrolesel.Count > 0)
                {
                    sSql = "SELECT CAST(soitemmstoid AS VARCHAR(100)) [Aksi], soitemno [No. SO], FORMAT(soitemdate, 'dd/MM/yyyy') [Tgl SO], custname [Customer],s.createuser [Pembuat SO], soitemmstnote [Note] FROM QL_trnsoitemmst s inner join ql_mstcust c on c.custoid=s.custoid WHERE s.cmpcode='" + CompnyCode + "' AND soitemmststatus='Approved' and isnull(statusspk,'')='' ORDER BY soitemno";
                    DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSOAppSpk");
                    if (tbl.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbl.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in tbl.Columns)
                            {
                                var item = dr[col].ToString();
                                if (col.ColumnName == "Aksi")
                                    item = "<a href='" + Url.Action("Form", "JobCostingMO") + "'>Buat SPK</a>";
                                row.Add(col.ColumnName, item);
                                if (!tblcols.Contains(col.ColumnName))
                                    tblcols.Add(col.ColumnName);
                            }
                            tblrows.Add(row);
                        }
                    }
                    else
                        result = "Data tidak ditemukan";
                }
                else
                    result = "No Notif";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetNotifSOSheetSPKApp()
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                string userID = Session["UserID"].ToString();
                sSql = "select us.* from QL_mstuserrole us Inner Join QL_mstroledtl rd on us.roleoid=rd.roleoid Inner Join QL_m04MN mn on mn.mnoid=rd.mnoid where mnname='SPK Sheet' and us.profoid='" + userID + "'";
                var dtrolesel = db.Database.SqlQuery<QL_mstuserrole>(sSql).ToList();
                if (dtrolesel.Count > 0)
                {
                    sSql = "SELECT CAST(sorawmstoid AS VARCHAR(100)) [Aksi], sorawno [No. SO], FORMAT(sorawdate, 'dd/MM/yyyy') [Tgl SO], custname [Customer],s.createuser [Pembuat SO], sorawmstnote [Note] FROM QL_trnsorawmst s inner join ql_mstcust c on c.custoid=s.custoid WHERE s.cmpcode='" + CompnyCode + "' AND sorawmststatus='Approved' and isnull(statusspk,'')='' ORDER BY sorawno";
                    DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSOAppSpksheet");
                    if (tbl.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbl.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in tbl.Columns)
                            {
                                var item = dr[col].ToString();
                                if (col.ColumnName == "Aksi")
                                    item = "<a href='" + Url.Action("Form", "JobCostingMOSheet") + "'>Buat SPK</a>";
                                row.Add(col.ColumnName, item);
                                if (!tblcols.Contains(col.ColumnName))
                                    tblcols.Add(col.ColumnName);
                            }
                            tblrows.Add(row);
                        }
                    }
                    else
                        result = "Data tidak ditemukan";
                }
                else
                    result = "No Notif";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetDataDashboardShipment()
        {
            var startperiod = ClassFunction.GetServerTime().ToString("01/01/yyyy") + " 00:00:00";
            var endperiod = ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59";

            sSql = "SELECT divname, SUM(totalshipment) totalshipment FROM (";
            sSql += "SELECT arm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=arm.cmpcode) divname, (SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dgm WHERE dgm.cmpcode=arm.cmpcode AND dgm.groupoid=som.groupoid) groupdesc, (SELECT currcode FROM QL_mstcurr c WHERE c.curroid=arm.curroid) currcode, SUM(aritemdtlnetto) totalshipment FROM QL_trnaritemmst arm INNER JOIN QL_trnaritemdtl ard ON ard.cmpcode=arm.cmpcode AND ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=shd.cmpcode AND dod.doitemdtloid=shd.doitemdtloid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=dod.cmpcode AND som.soitemmstoid=dod.soitemmstoid WHERE aritemmststatus IN ('Approved', 'Closed') AND aritemdate <= CAST('" + endperiod + "' AS DATETIME) GROUP BY arm.cmpcode, som.groupoid, arm.curroid";
            //sSql += " UNION ALL ";
            //sSql += "SELECT retm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=retm.cmpcode) divname, (SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dgm WHERE dgm.cmpcode=retm.cmpcode AND dgm.groupoid=som.groupoid) groupdesc, (SELECT currcode FROM QL_mstcurr c WHERE c.curroid=som.curroid) currcode, SUM(arretitemgrandtotal)*-1 totalshipment FROM QL_trnarretitemmst retm INNER JOIN QL_trnarretitemdtl retd ON retd.cmpcode=retm.cmpcode AND retd.arretitemmstoid=retm.arretitemmstoid INNER JOIN QL_trnaritemdtl ard ON ard.cmpcode=retd.cmpcode AND ard.aritemdtloid=retd.aritemdtloid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=shd.cmpcode AND dod.doitemdtloid=shd.doitemdtloid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=dod.cmpcode AND som.soitemmstoid=dod.soitemmstoid WHERE arretitemmststatus IN ('Approved', 'Closed') AND retm.approvaldatetime >= CAST('" + startperiod + "' AS DATETIME) AND retm.approvaldatetime <= CAST('" + endperiod + "' AS DATETIME) GROUP BY retm.cmpcode, som.groupoid, som.curroid";
            sSql += ") QL_totalshipment GROUP BY divname ORDER BY divname";

            List<dbshipmentmodel> dataDtl = db.Database.SqlQuery<dbshipmentmodel>(sSql).ToList();
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public class dbprodtotalkikmodel
        {
            public string statuskik { get; set; }
            public int totalkik { get; set; }
        }

        public class dbproddurationmodel
        {
            public string dayrange { get; set; }
            public int totalkik { get; set; }
        }

        public class dbprodprogressmodel
        {
            public int unclosedkik { get; set; }
            public int closedkik { get; set; }
        }

        //public ActionResult GetDataDashboardProduction(string StartPeriod, string EndPeriod, string BusinessUnit)
        //{
        //    sSql = "SELECT womststatus statuskik, COUNT(womstoid) totalkik FROM QL_trnwomst WHERE cmpcode='" + BusinessUnit + "' AND wodate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND wodate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) GROUP BY womststatus ORDER BY womststatus";
        //    List<dbprodtotalkikmodel> datatotalkik = db.Database.SqlQuery<dbprodtotalkikmodel>(sSql).ToList();

        //    sSql = "SELECT * FROM QL_trnwomst WHERE cmpcode='" + BusinessUnit + "' AND wodate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND wodate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
        //    List<QL_trnwomst> datawo = db.Database.SqlQuery<QL_trnwomst>(sSql).ToList();

        //    List<dbproddurationmodel> dataduration = new List<dbproddurationmodel>();
        //    var totalkik1 = 0;
        //    var totalkik2 = 0;
        //    var totalkik3 = 0;

        //    for (int i = 0; i < datawo.Count; i++)
        //    {
        //        if (datawo[i].womststatus.ToUpper() == "CLOSED")
        //        {
        //            int totaldays = ((TimeSpan)(datawo[i].woclosingdate - datawo[i].wodate)).Days;
        //            if (totaldays <= 15)
        //                totalkik1++;
        //            else if (totaldays > 15 && totaldays <= 30)
        //                totalkik2++;
        //            else
        //                totalkik3++;
        //        }
        //    }
        //    dataduration.Add(new dbproddurationmodel() { dayrange = "<15", totalkik = totalkik1 });
        //    dataduration.Add(new dbproddurationmodel() { dayrange = "16 to 30", totalkik = totalkik2 });
        //    dataduration.Add(new dbproddurationmodel() { dayrange = "> 30", totalkik = totalkik3 });

        //    sSql = "SELECT SUM(unclosedkik) unclosedkik, SUM(closedkik) closedkik FROM (SELECT COUNT(womstoid) unclosedkik, 0 closedkik FROM QL_trnwomst WHERE cmpcode='" + BusinessUnit + "' AND wodate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND wodate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) AND womststatus IN ('In Process', 'Post') UNION ALL SELECT 0 unclosedkik, COUNT(womstoid) closedkik FROM QL_trnwomst WHERE cmpcode='" + BusinessUnit + "' AND wodate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND wodate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) AND womststatus NOT IN ('In Process', 'Post')) wo";
        //    List<dbprodprogressmodel> dataprogress = db.Database.SqlQuery<dbprodprogressmodel>(sSql).ToList();

        //    return Json(new { datatotalkik, dataduration, dataprogress }, JsonRequestBehavior.AllowGet);
        //}
    }
}