﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers
{
    public class WaitingActionController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string NumberCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";
        private string sqlPlus = "";

       

        public class waitactmodel
        {
            public int mstoid { get; set; }
            public string cmpcode { get; set; }
            public string tblname { get; set; }
        }

        public class modelforapp
        {
            public string caption { get; set; }
            public string value { get; set; }
        }

        [HttpPost]
        public ActionResult setrevisednote(string rv)
        {
            Session["rvnote"] = rv;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult setrejectednote(string rj)
        {
            Session["rjnote"] = rj;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: WaitingAction
        public ActionResult Index(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT apppersonnote FROM QL_approvalperson WHERE tablename='" + id + "'").FirstOrDefault();
            ViewBag.tblname = id;
            return View();
        }

        [HttpPost]
        public ActionResult GetDataList(string tblname)
        {
            var msg = "";

            if (tblname == "ql_prrawmst")
            {
                sSql = "SELECT h.cmpcode, prrawmstoid [Draft No.], CONVERT(VARCHAR(10), prrawdate, 101) [PR Date], deptname [Department], prrawmstnote [Note], a.approvaluser [Approval User], h.createuser [Create User], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_prrawmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=prrawmstoid INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND prrawmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_prgenmst")
            {
                sSql = "SELECT h.cmpcode, prgenmstoid [Draft No.], CONVERT(VARCHAR(10), prgendate, 101) [PR Date], deptname [Department], prgenmstnote [Note], a.approvaluser [Approval User], h.createuser [Create User], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_prgenmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=prgenmstoid INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND prgenmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_prspmst")
            {
                sSql = "SELECT h.cmpcode, prspmstoid [Draft No.], CONVERT(VARCHAR(10), prspdate, 101) [PR Date], (SELECT deptname FROM QL_mstdept d WHERE d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid)  [Department] , prspmstnote [Note], a.approvaluser [Approval User], h.createuser [Create User], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_prspmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=prspmstoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND prspmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_pritemmst")
            {
                sSql = "SELECT h.cmpcode, pritemmstoid [Draft No.], CONVERT(VARCHAR(10), pritemdate, 101) [PR Date], deptname [Department], pritemmstnote [Note], a.approvaluser [Approval User], h.createuser [Create User], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_pritemmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=pritemmstoid INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND pritemmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_prassetmst")
            {
                sSql = "SELECT h.cmpcode, prassetmstoid [Draft No.], CONVERT(VARCHAR(10), prassetdate, 101) [PR Date], deptname [Department], prassetmstnote [Note], a.approvaluser [Approval User], h.createuser [Create User], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_prassetmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=prassetmstoid INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND prassetmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_trnporawmst")
            {
                sSql = "SELECT po.cmpcode, po.porawmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.porawdate, 101) AS [PO Date], s.suppname AS [Supplier], po.porawmstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnporawmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.porawmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname +  "' AND po.porawmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnpogenmst")
            {
                sSql = "SELECT po.cmpcode, po.pogenmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.pogendate, 101) AS [PO Date], s.suppname AS [Supplier], po.pogenmstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnpogenmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.pogenmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname +  "' AND po.pogenmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnpospmst")
            {
                sSql = "SELECT po.cmpcode, po.pospmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.pospdate, 101) AS [PO Date], s.suppname AS [Supplier], po.pospmstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnpospmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.pospmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname +  "' AND po.pospmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnpoitemmst")
            {
                sSql = "SELECT po.cmpcode, po.poitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poitemdate, 101) AS [PO Date], s.suppname AS [Supplier], po.poitemmstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnpoitemmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.poitemmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname +  "' AND po.poitemmststatus='In Approval' ORDER BY ap.requestdate DESC";
            }
            else if (tblname == "ql_trnposervicemst")
            {
                sSql = "SELECT po.cmpcode, po.poservicemstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poservicedate, 101) AS [PO Date], s.suppname AS [Supplier], po.poservicemstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnposervicemst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.poservicemstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND po.poservicemststatus='In Approval' ORDER BY ap.requestdate DESC";
            }
            else if (tblname == "ql_trnpoassetmst")
            {
                sSql = "SELECT po.cmpcode, po.poassetmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poassetdate, 101) AS [PO Date], s.suppname AS [Supplier], po.poassetmstnote AS [PO Note], ISNULL(po.approvalcode,'-') AS [Approval User], po.createuser AS [Create User], ap.requestuser AS [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnpoassetmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.cmpcode=ap.cmpcode AND po.poassetmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND po.poassetmststatus='In Approval' ORDER BY ap.requestdate DESC";
            }
            else if (tblname == "ql_trnpretrawmst")
            {
                sSql = "SELECT h.cmpcode, pretrawmstoid [Draft No.], CONVERT(VARCHAR(10), pretrawdate, 101) [Return Date], suppname [Supplier], pretrawmstnote [Note], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_trnpretrawmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=pretrawmstoid INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND pretrawmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_trnpretgenmst")
            {
                sSql = "SELECT h.cmpcode, pretgenmstoid [Draft No.], CONVERT(VARCHAR(10), pretgendate, 101) [Return Date], suppname [Supplier], pretgenmstnote [Note], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_trnpretgenmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=pretgenmstoid INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND pretgenmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_trnpretspmst")
            {
                sSql = "SELECT h.cmpcode, pretspmstoid [Draft No.], CONVERT(VARCHAR(10), pretspdate, 101) [Return Date], suppname [Supplier], pretspmstnote [Note], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_trnpretspmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=pretspmstoid INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND pretspmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_trnpretitemmst")
            {
                sSql = "SELECT h.cmpcode, pretitemmstoid [Draft No.], CONVERT(VARCHAR(10), pretitemdate, 101) [Return Date], suppname [Supplier], pretitemmstnote [Note], a.requestuser [Request User], CONVERT(VARCHAR(10), a.requestdate, 101) [Request Date] FROM QL_trnpretitemmst h INNER JOIN QL_approval a ON a.cmpcode=h.cmpcode AND a.oid=pretitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE a.statusrequest='New' AND a.event='In Approval' AND a.approvaluser='" + Session["UserID"].ToString() + "' AND a.tablename='" + tblname + "' AND pretitemmststatus='In Approval' ORDER BY a.requestdate DESC";
            } else if (tblname == "ql_trnaprawmst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.aprawmstoid AS [Draft No.], ap.aprawno AS [A/P No.], CONVERT(VARCHAR(10), ap.aprawdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=aprawpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),aprawdate), 101) [Due Date], CONVERT(VARCHAR(10), ap.aprawdatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.aprawmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnaprawmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.aprawmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.aprawpaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.aprawmststatus='In Approval' ORDER BY [Request Date] DESC"; 
            }  else if (tblname == "ql_trnapgenmst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.apgenmstoid AS [Draft No.], ap.apgenno AS [A/P No.], CONVERT(VARCHAR(10), ap.apgendate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apgenpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apgendate), 101) [Due Date], CONVERT(VARCHAR(10), ap.apgendatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.apgenmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapgenmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apgenmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apgenpaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.apgenmststatus='In Approval' ORDER BY [Request Date] DESC"; 
            }  else if (tblname == "ql_trnapspmst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.apspmstoid AS [Draft No.], ap.apspno AS [A/P No.], CONVERT(VARCHAR(10), ap.apspdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apsppaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apspdate), 101) [Due Date], CONVERT(VARCHAR(10), ap.apspdatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.apspmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapspmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apspmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apsppaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.apspmststatus='In Approval' ORDER BY [Request Date] DESC"; 
            }  else if (tblname == "ql_trnapitemmst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.apitemmstoid AS [Draft No.], ap.apitemno AS [A/P No.], CONVERT(VARCHAR(10), ap.apitemdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apitempaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apitemdate), 101) [Due Date], CONVERT(VARCHAR(10), ap.apitemdatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.apitemmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapitemmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apitemmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apitempaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.apitemmststatus='In Approval' ORDER BY [Request Date] DESC";
            }
            else if (tblname == "ql_trnapservicemst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.apservicemstoid AS [Draft No.], ap.apserviceno AS [A/P No.], CONVERT(VARCHAR(10), ap.apservicedate, 101) AS [A/P Date], g.gendesc AS [Payment Type], CONVERT(VARCHAR(10), DATEADD(day, (CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END), apservicedate), 101) [Due Date], CONVERT(VARCHAR(10), ap.apservicedatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.apservicemstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapservicemst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apservicemstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apservicepaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.apservicemststatus='In Approval' ORDER BY [Request Date] DESC";
            }
            else if (tblname == "ql_trnapassetmst")
            {
                sSql = "SELECT DISTINCT ap.cmpcode, ap.apassetmstoid AS [Draft No.], ap.apassetno AS [A/P No.], CONVERT(VARCHAR(10), ap.apassetdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apassetpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apassetdate), 101) [Due Date], CONVERT(VARCHAR(10), ap.apassetdatetakegiro, 101) AS [Date Take Giro], s.suppname AS [Supplier], ap.apassetmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapassetmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apassetmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apassetpaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr cu ON cu.curroid=ap.curroid WHERE apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND ap.apassetmststatus='In Approval' ORDER BY [Request Date] DESC";
            }
            else if (tblname == "ql_trnsorawmst")
            {
                sSql = "SELECT so.cmpcode, so.sorawmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.sorawdate, 101) AS [SO Date], c.custname [Customer], so.sorawmstnote [SO Note], so.createuser [Create user], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsorawmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sorawmstoid=ap.oid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND so.sorawmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnsogenmst")
            {
                sSql = "SELECT so.cmpcode, so.sogenmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.sogendate, 101) AS [SO Date], c.custname [Customer], so.sogenmstnote [SO Note], so.createuser [Create user], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsogenmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sogenmstoid=ap.oid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND so.sogenmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnsospmst")
            {
                sSql = "SELECT so.cmpcode, so.sospmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.sospdate, 101) AS [SO Date], c.custname [Customer], so.sospmstnote [SO Note], so.createuser [Create user], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsospmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sospmstoid=ap.oid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND so.sospmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnsoitemmst")
            {
                sSql = "SELECT so.cmpcode, so.soitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.soitemdate, 101) AS [SO Date], c.custname [Customer], so.soitemcustref [PO Customer], CONVERT(VARCHAR(10), so.soitemetd, 101) [Delivery Date], so.soitemmstnote [SO Note], so.createuser [Create user], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsoitemmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.soitemmstoid=ap.oid WHERE  ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND so.soitemmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnshipmentrawmst")
            {
                sSql = "SELECT sm.cmpcode, sm.shipmentrawmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentrawdate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentrawmstnote [Shipment Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnshipmentrawmst sm INNER JOIN QL_approval ap ON sm.shipmentrawmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sm.shipmentrawmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnshipmentgenmst")
            {
                sSql = "SELECT sm.cmpcode, sm.shipmentgenmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentgendate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentgenmstnote [Shipment Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnshipmentgenmst sm INNER JOIN QL_approval ap ON sm.shipmentgenmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sm.shipmentgenmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnshipmentspmst")
            {
                sSql = "SELECT sm.cmpcode, sm.shipmentspmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentspdate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentspmstnote [Shipment Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnshipmentspmst sm INNER JOIN QL_approval ap ON sm.shipmentspmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sm.shipmentspmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnshipmentitemmst")
            {
                sSql = "SELECT sm.cmpcode, sm.shipmentitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentitemdate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentitemmstnote [Shipment Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnshipmentitemmst sm INNER JOIN QL_approval ap ON sm.shipmentitemmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sm.shipmentitemmststatus='In Approval' ORDER BY ap.requestdate DESC";
            } else if (tblname == "ql_trnsretrawmst")
            {
                sSql = "SELECT sretm.cmpcode, sretrawmstoid AS [Draft No.], CONVERT(VARCHAR(10), sretrawdate, 101) AS [Return Date], custname [Customer], sretrawmstnote AS [Return Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsretrawmst sretm INNER JOIN QL_approval ap ON sretrawmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sretrawmststatus='In Approval' ORDER BY CAST(ap.requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnsretgenmst")
            {
                sSql = "SELECT sretm.cmpcode, sretgenmstoid AS [Draft No.], CONVERT(VARCHAR(10), sretgendate, 101) AS [Return Date], custname [Customer], sretgenmstnote AS [Return Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsretgenmst sretm INNER JOIN QL_approval ap ON sretgenmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sretgenmststatus='In Approval' ORDER BY CAST(ap.requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnsretspmst")
            {
                sSql = "SELECT sretm.cmpcode, sretspmstoid AS [Draft No.], CONVERT(VARCHAR(10), sretspdate, 101) AS [Return Date], custname [Customer], sretspmstnote AS [Return Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsretspmst sretm INNER JOIN QL_approval ap ON sretspmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sretspmststatus='In Approval' ORDER BY CAST(ap.requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnsretitemmst")
            {
                sSql = "SELECT sretm.cmpcode, sretitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), sretitemdate, 101) AS [Return Date], custname [Customer], sretitemmstnote AS [Return Note], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnsretitemmst sretm INNER JOIN QL_approval ap ON sretitemmstoid=ap.oid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE ap.statusrequest='New' AND ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='" + tblname + "' AND sretitemmststatus='In Approval' ORDER BY CAST(ap.requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarrawmst")
            {
                sSql = "SELECT arm.cmpcode, arrawmstoid AS [Draft No.], arrawno AS arno, CONVERT(VARCHAR(10), arrawdate, 101) AS [PEB Date], custname [Customer], ISNULL(arrawmstres3,'') AS [Invoice No.], arrawmstnote AS [A/R Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date] FROM QL_trnarrawmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_approval apr ON apr.oid=arrawmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arrawmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnargenmst")
            {
                sSql = "SELECT arm.cmpcode, argenmstoid AS [Draft No.], argenno AS arno, CONVERT(VARCHAR(10), argendate, 101) AS [PEB Date], custname [Customer], ISNULL(argenmstres3,'') AS [Invoice No.], argenmstnote AS [A/R Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date] FROM QL_trnargenmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_approval apr ON apr.oid=argenmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arrawmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarspmst")
            {
                sSql = "SELECT arm.cmpcode, arspmstoid AS [Draft No.], arspno AS arno, CONVERT(VARCHAR(10), arspdate, 101) AS [PEB Date], custname [Customer], ISNULL(arspmstres3,'') AS [Invoice No.], arspmstnote AS [A/R Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date] FROM QL_trnarspmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_approval apr ON apr.oid=arspmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arrawmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnaritemmst")
            {
                sSql = "SELECT arm.cmpcode, aritemmstoid AS [Draft No.], aritemno AS arno, CONVERT(VARCHAR(10), aritemdate, 101) AS [PEB Date], custname [Customer], ISNULL(aritemmstres3,'') AS [Invoice No.], aritemmstnote AS [A/R Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date] FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_approval apr ON apr.oid=aritemmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND aritemmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarretrawmst")
            {
                sSql = "SELECT arretm.cmpcode, arretrawmstoid AS [Draft No.], CONVERT(VARCHAR(10), arretrawdate, 101) AS [Return Date], custname [Customer], arretrawmstnote [Return Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date]  FROM QL_trnarretrawmst arretm INNER JOIN QL_mstcust c ON c.custoid=arretm.custoid INNER JOIN QL_approval apr ON apr.oid=arretrawmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arretrawmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarretgenmst")
            {
                sSql = "SELECT arretm.cmpcode, arretgenmstoid AS [Draft No.], CONVERT(VARCHAR(10), arretgendate, 101) AS [Return Date], custname [Customer], arretgenmstnote [Return Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date]  FROM QL_trnarretgenmst arretm INNER JOIN QL_mstcust c ON c.custoid=arretm.custoid INNER JOIN QL_approval apr ON apr.oid=arretgenmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arretgenmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarretspmst")
            {
                sSql = "SELECT arretm.cmpcode, arretspmstoid AS [Draft No.], CONVERT(VARCHAR(10), arretspdate, 101) AS [Return Date], custname [Customer], arretspmstnote [Return Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date]  FROM QL_trnarretspmst arretm INNER JOIN QL_mstcust c ON c.custoid=arretm.custoid INNER JOIN QL_approval apr ON apr.oid=arretspmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arretspmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            } else if (tblname == "ql_trnarretitemmst")
            {
                sSql = "SELECT arretm.cmpcode, arretitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), arretitemdate, 101) AS [Return Date], custname [Customer], arretitemmstnote [Return Note], requestuser [Request User], CONVERT(VARCHAR(10), requestdate, 101) AS [Request Date]  FROM QL_trnarretitemmst arretm INNER JOIN QL_mstcust c ON c.custoid=arretm.custoid INNER JOIN QL_approval apr ON apr.oid=arretitemmstoid WHERE statusrequest='New' AND apr.event='In Approval' AND apr.approvaluser='" + Session["UserID"].ToString() + "' AND apr.tablename='" + tblname + "' AND arretitemmststatus='In Approval' ORDER BY CAST(requestdate AS DATETIME) DESC";
            }else if (tblname == "ql_trnstockadj")
            {
                sSql = "SELECT a.cmpcode, CAST(resfield1 AS INTEGER) AS [Draft No.], CONVERT(VARCHAR(10), stockadjdate, 101) AS [Adj Date], stockadjtype AS [Type], ap.requestuser [Request User], CONVERT(VARCHAR(10), ap.requestdate, 101) AS [Request Date] FROM QL_trnstockadj a INNER JOIN QL_approval ap ON ap.oid=CAST(resfield1 AS INTEGER) AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" + Session["UserID"].ToString() + "' AND ap.tablename='QL_trnstockadj' AND stockadjstatus='In Approval' GROUP BY a.cmpcode, resfield1, stockadjno, stockadjdate, ap.approvaloid, ap.requestuser, ap.requestdate, a.upduser, stockadjtype ORDER BY ap.requestdate DESC";
            } else
                msg = "Approval action haven't be set";
            
            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, tblname);

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Draft No.")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + item + "/" , "WaitingAction") + "?tblname=" + tblname + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    return Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }
      
        // GET: WaitingAction/Form/id/cmp
        public ActionResult Form(int id, string cmp, string tblname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT apppersonnote FROM QL_approvalperson WHERE tablename='" + tblname + "'").FirstOrDefault();
            ViewBag.BusinessUnit = db.Database.SqlQuery<string>("SELECT divname FROM QL_mstdivision WHERE cmpcode='" + CompnyCode + "'").FirstOrDefault();

            waitactmodel tbl = new waitactmodel();
            tbl.mstoid = id;
            tbl.cmpcode = CompnyCode;
            tbl.tblname = tblname;

            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataApproval(int id, string cmp, string tblname)
        {
            var msg = "";
            var sSqlHdr = "";
            var info = "";
            var sSqlDtl = "";
            var sSqlFtr = "";
            var type = "";

            if (tblname == "ql_prrawmst")
            {
                type = "RawMaterial";
                sSqlHdr = "SELECT h.prrawmstoid [Draft No.], CONVERT(VARCHAR(10), prrawdate, 101) [PR Date], deptname [Department], prrawmstnote [Note], h.createuser [Create User], h.upduser [Last Update By], h.updtime [Last Update On] FROM QL_prrawmst h INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE h.cmpcode='" + CompnyCode + "' AND h.prrawmstoid=" + id + "";

                //info += "* free stok : refer to mrp<br />";
                //info += "* last free stok : refer to last mrp<br />";
                //info += "* last price : on same PT, refer to last approved PO<br />";
                //info += "* last price idr : on same PT, refer to last approved PO in IDR";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT prrawdtlseq [No.], matrawcode [Mat. Code], matrawshortdesc [Mat. Desc.], prrawarrdatereq [Arr. Date. Req], prrawqty [DEC_Qty], stockqty [DEC_Last Stock], unit [Unit], lastprice[DEC_Last Price], lastpriceidr [DEC_Last Price IDR], (prrawqty * lastpriceidr) [DEC_Netto], prrawdtlnote [Note] FROM (SELECT prd.prrawdtlseq, m.matrawcode, m.matrawlongdesc AS matrawshortdesc, prd.prrawqty, prd.prrawqty * -1 requireqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prrawarrdatereq, prd.prrawdtlnote, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='RAW MATERIAL' AND crd.refoid=prd.matrawoid ), 0.0) AS stockqty FROM QL_prrawmst pr INNER JOIN QL_prrawdtl prd ON pr.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON prd.prrawunitoid=g.genoid  WHERE pr.cmpcode='" + CompnyCode + "' AND pr.prrawmstoid=" + id + ") AS tblPR ORDER BY prrawdtlseq";

                sSqlFtr = "SELECT SUM(prrawqty * lastpriceidr) [Total] FROM (SELECT prrawqty, ISNULL((SELECT TOP 1 lastpurchaseprice /** ratetoidr*/ FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=prd.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) lastpriceidr FROM QL_prrawdtl prd WHERE cmpcode='" + CompnyCode + "' AND prrawmstoid=" + id + ") AS tblPR";
            }
            else if (tblname == "ql_prgenmst")
            {
                type = "GeneralMaterial";

                sSqlHdr = "SELECT h.prgenmstoid [Draft No.], CONVERT(VARCHAR(10), prgendate, 101) [PR Date], deptname [Department], prgenmstnote [Note], h.createuser [Create User], h.upduser [Last Update By], h.updtime [Last Update On] FROM QL_prgenmst h INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE h.cmpcode='" + CompnyCode + "' AND h.prgenmstoid=" + id + "";

                //info += "* free stok : refer to mrp<br />";
                //info += "* last free stok : refer to last mrp<br />";
                //info += "* last price : on same PT, refer to last approved PO<br />";
                //info += "* last price idr : on same PT, refer to last approved PO in IDR";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT prgendtlseq [No.], matgencode [Mat. Code], matgenshortdesc [Mat. Desc.], prgenarrdatereq [Arr. Date. Req], prgenqty [DEC_Qty], stockqty [DEC_Last Stock], unit [Unit], lastprice[DEC_Last Price], lastpriceidr [DEC_Last Price IDR], (prgenqty * lastpriceidr) [DEC_Netto], prgendtlnote [Note] FROM (SELECT prd.prgendtlseq, m.matgencode, m.matgenlongdesc AS matgenshortdesc, prd.prgenqty, prd.prgenqty * -1 requireqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prgenarrdatereq, prd.prgendtlnote, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='GENERAL MATERIAL' AND crd.refoid=prd.matgenoid ), 0.0) AS stockqty FROM QL_prgenmst pr INNER JOIN QL_prgendtl prd ON pr.prgenmstoid=prd.prgenmstoid INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON prd.prgenunitoid=g.genoid  WHERE pr.cmpcode='" + CompnyCode + "' AND pr.prgenmstoid=" + id + ") AS tblPR ORDER BY prgendtlseq";

                sSqlFtr = "SELECT SUM(prgenqty * lastpriceidr) [Total] FROM (SELECT prgenqty, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) lastpriceidr FROM QL_prgendtl prd WHERE cmpcode='" + CompnyCode + "' AND prgenmstoid=" + id + ") AS tblPR";
            }
            else if (tblname == "ql_prspmst")
            {
                type = "Sparepart";

                sSqlHdr = "SELECT h.prspmstoid [Draft No.], CONVERT(VARCHAR(10), prspdate, 101) [PR Date], deptname [Department], prspmstnote [Note], h.createuser [Create User], h.upduser [Last Update By], h.updtime [Last Update On] FROM QL_prspmst h INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE h.cmpcode='" + CompnyCode + "' AND h.prspmstoid=" + id + "";

                //info += "* free stok : refer to mrp<br />";
                //info += "* last free stok : refer to last mrp<br />";
                //info += "* last price : on same PT, refer to last approved PO<br />";
                //info += "* last price idr : on same PT, refer to last approved PO in IDR";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT prspdtlseq [No.], sparepartcode [Mat. Code], sparepartshortdesc [Mat. Desc.], prsparrdatereq [Arr. Date. Req], prspqty [DEC_Qty], stockqty [DEC_Last Stock], unit [Unit], lastprice[DEC_Last Price], lastpriceidr [DEC_Last Price IDR], (prspqty * lastpriceidr) [DEC_Netto], prspdtlnote [Note] FROM (SELECT prd.prspdtlseq, m.sparepartcode, m.sparepartlongdesc AS sparepartshortdesc, prd.prspqty, prd.prrawqty * -1 requireqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prsparrdatereq, prd.prspdtlnote, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SPARE PART' AND crd.refoid=prd.sparepartoid ), 0.0) AS stockqty FROM QL_prspmst pr INNER JOIN QL_prspdtl prd ON pr.prspmstoid=prd.prspmstoid INNER JOIN QL_mstsparepart m ON prd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON prd.prspunitoid=g.genoid  WHERE pr.cmpcode='" + CompnyCode + "' AND pr.prspmstoid=" + id + ") AS tblPR ORDER BY prspdtlseq";

                sSqlFtr = "SELECT SUM(prspqty * lastpriceidr) [Total] FROM (SELECT prspqty, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=prd.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) lastpriceidr FROM QL_prspdtl prd WHERE cmpcode='" + CompnyCode + "' AND prspmstoid=" + id + ") AS tblPR";
            }
            else if (tblname == "ql_pritemmst")
            {
                type = "FinishGood";

                sSqlHdr = "SELECT h.pritemmstoid [Draft No.], CONVERT(VARCHAR(10), pritemdate, 101) [PR Date], deptname [Department], pritemmstnote [Note], h.createuser [Create User], h.upduser [Last Update By], h.updtime [Last Update On] FROM QL_pritemmst h INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE h.cmpcode='" + CompnyCode + "' AND h.pritemmstoid=" + id + "";

                //info += "* free stok : refer to mrp<br />";
                //info += "* last free stok : refer to last mrp<br />";
                //info += "* last price : on same PT, refer to last approved PO<br />";
                //info += "* last price idr : on same PT, refer to last approved PO in IDR";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT pritemdtlseq [No.], itemcode [Mat. Code], itemshortdesc [Mat. Desc.], pritemarrdatereq [Arr. Date. Req], pritemqty [DEC_Qty], stockqty [DEC_Last Stock], unit [Unit], lastprice[DEC_Last Price], lastpriceidr [DEC_Last Price IDR], (pritemqty * lastpriceidr) [DEC_Netto], pritemdtlnote [Note] FROM (SELECT prd.pritemdtlseq, m.itemcode, m.itemlongdesc AS itemshortdesc, prd.pritemqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq, prd.pritemdtlnote, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='FINISH GOOD' AND crd.refoid=prd.itemoid), 0.0) AS stockqty FROM QL_pritemmst pr INNER JOIN QL_pritemdtl prd ON pr.pritemmstoid=prd.pritemmstoid INNER JOIN QL_mstitem m ON prd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON prd.pritemunitoid=g.genoid  WHERE pr.cmpcode='" + CompnyCode + "' AND pr.pritemmstoid=" + id + ") AS tblPR ORDER BY pritemdtlseq";

                sSqlFtr = "SELECT SUM(pritemqty * lastpriceidr) [Total] FROM (SELECT pritemqty, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=prd.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) lastpriceidr FROM QL_pritemdtl prd WHERE cmpcode='" + CompnyCode + "' AND pritemmstoid=" + id + ") AS tblPR";
            }
            else if (tblname == "ql_prassetmst")
            {
                type = "Aset";

                sSqlHdr = "SELECT h.prassetmstoid [Draft No.], CONVERT(VARCHAR(10), prassetdate, 101) [PR Date], deptname [Department], prassetmstnote [Note], h.createuser [Create User], h.upduser [Last Update By], h.updtime [Last Update On] FROM QL_prassetmst h INNER JOIN QL_mstdept d ON d.cmpcode=h.cmpcode AND d.deptoid=h.deptoid WHERE h.cmpcode='" + CompnyCode + "' AND h.prassetmstoid=" + id + "";

                //info += "* free stok : refer to mrp<br />";
                //info += "* last free stok : refer to last mrp<br />";
                //info += "* last price : on same PT, refer to last approved PO<br />";
                //info += "* last price idr : on same PT, refer to last approved PO in IDR";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT prassetdtlseq [No.], matgencode [Mat. Code], matgenshortdesc [Mat. Desc.], prassetarrdatereq [Arr. Date. Req], prassetqty [DEC_Qty], stockqty [DEC_Last Stock], unit [Unit], lastprice[DEC_Last Price], lastpriceidr [DEC_Last Price IDR], (prassetqty * lastpriceidr) [DEC_Netto], prassetdtlnote [Note] FROM (SELECT prd.prassetdtlseq, m.matgencode, m.matgenlongdesc AS matgenshortdesc, prd.prassetqty, prd.prassetqty * -1 requireqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prassetarrdatereq, prd.prassetdtlnote, 0.0 AS stockqty FROM QL_prassetmst pr INNER JOIN QL_prassetdtl prd ON pr.prassetmstoid=prd.prassetmstoid INNER JOIN QL_mstmatgen m ON prd.prassetrefoid=m.matgenoid INNER JOIN QL_mstgen g ON prd.prassetunitoid=g.genoid  WHERE pr.cmpcode='" + CompnyCode + "' AND pr.prassetmstoid=" + id + ") AS tblPR ORDER BY prassetdtlseq";

                sSqlFtr = "SELECT SUM(prassetqty * lastpriceidr) [Total] FROM (SELECT prassetqty, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) lastpriceidr FROM QL_prassetdtl prd WHERE cmpcode='" + CompnyCode + "' AND prassetmstoid=" + id + ") AS tblPR";
            }
            else if (tblname == "ql_trnporawmst")
            {
                sSqlHdr = "SELECT po.porawmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.porawdate, 101) AS[PO Date], s.suppname AS[Supplier], po.porawmstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnporawmst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.porawmstoid=" + id;

                sSqlDtl = "SELECT pod.porawdtlseq AS [No.], m.matrawcode AS [Mat.Code], m.matrawlongdesc AS[Mat.Desc], CONVERT(VARCHAR(10), prrawarrdatereq, 101) AS [ETA], pod.porawqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode = po.cmpcode AND mp.matrawoid = pod.matrawoid AND mp.refname = 'SUPPLIER' AND mp.curroid = po.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.porawprice AS [DEC_Price], pod.porawdtlamt AS [DEC_Amount], pod.porawdtldiscamt AS [DEC_Disc], pod.porawdtlnetto AS [DEC_Netto], pod.porawdtlnote AS [Note] FROM QL_trnporawmst po INNER JOIN QL_trnporawdtl pod ON po.cmpcode = pod.cmpcode AND po.porawmstoid = pod.porawmstoid INNER JOIN QL_mstmatraw m ON pod.matrawoid = m.matrawoid INNER JOIN QL_mstgen g ON pod.porawunitoid = g.genoid INNER JOIN QL_prrawdtl prd ON prd.cmpcode = pod.cmpcode AND prd.prrawdtloid = pod.prrawdtloid AND prd.prrawmstoid = pod.prrawmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pod.porawmstoid=" + id + " ORDER BY pod.porawdtlseq";

                sSqlFtr = "SELECT (po.porawtotalamt - po.porawtotaldiscdtl) AS [Total], po.porawmstdiscamt AS [Header Disc.], po.porawvat AS [Tax], (po.porawdeliverycost + po.porawothercost) AS [Other Cost], po.porawgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnporawmst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.porawmstoid=" + id;
            }
            else if (tblname == "ql_trnpogenmst")
            {
                sSqlHdr = "SELECT po.pogenmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.pogendate, 101) AS[PO Date], s.suppname AS[Supplier], po.pogenmstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnpogenmst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.pogenmstoid=" + id;

                sSqlDtl = "SELECT pod.pogendtlseq AS [No.], m.matgencode AS [Mat.Code], m.matgenlongdesc AS[Mat.Desc], CONVERT(VARCHAR(10), prgenarrdatereq, 101) AS [ETA], pod.pogenqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode = po.cmpcode AND mp.matgenoid = pod.matgenoid AND mp.refname = 'SUPPLIER' AND mp.curroid = po.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.pogenprice AS [DEC_Price], pod.pogendtlamt AS [DEC_Amount], pod.pogendtldiscamt AS [DEC_Disc], pod.pogendtlnetto AS [DEC_Netto], pod.pogendtlnote AS [Note] FROM QL_trnpogenmst po INNER JOIN QL_trnpogendtl pod ON po.cmpcode = pod.cmpcode AND po.pogenmstoid = pod.pogenmstoid INNER JOIN QL_mstmatgen m ON pod.matgenoid = m.matgenoid INNER JOIN QL_mstgen g ON pod.pogenunitoid = g.genoid INNER JOIN QL_prgendtl prd ON prd.cmpcode = pod.cmpcode AND prd.prgendtloid = pod.prgendtloid AND prd.prgenmstoid = pod.prgenmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pod.pogenmstoid=" + id + " ORDER BY pod.pogendtlseq";

                sSqlFtr = "SELECT (po.pogentotalamt - po.pogentotaldiscdtl) AS [Total], po.pogenmstdiscamt AS [Header Disc.], po.pogenvat AS [Tax], (po.pogendeliverycost + po.pogenothercost) AS [Other Cost], po.pogengrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnpogenmst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.pogenmstoid=" + id;
            }
            else if (tblname == "ql_trnpospmst")
            {
                sSqlHdr = "SELECT po.pospmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.pospdate, 101) AS[PO Date], s.suppname AS[Supplier], po.pospmstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnpospmst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.pospmstoid=" + id;

                sSqlDtl = "SELECT pod.pospdtlseq AS [No.], m.sparepartcode AS [Mat.Code], m.sparepartlongdesc AS[Mat.Desc], CONVERT(VARCHAR(10), prsparrdatereq, 101) AS [ETA], pod.pospqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode = po.cmpcode AND mp.sparepartoid = pod.sparepartoid AND mp.refname = 'SUPPLIER' AND mp.curroid = po.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.pospprice AS [DEC_Price], pod.pospdtlamt AS [DEC_Amount], pod.pospdtldiscamt AS [DEC_Disc], pod.pospdtlnetto AS [DEC_Netto], pod.pospdtlnote AS [Note] FROM QL_trnpospmst po INNER JOIN QL_trnpospdtl pod ON po.cmpcode = pod.cmpcode AND po.pospmstoid = pod.pospmstoid INNER JOIN QL_mstsparepart m ON pod.sparepartoid = m.sparepartoid INNER JOIN QL_mstgen g ON pod.pospunitoid = g.genoid INNER JOIN QL_prspdtl prd ON prd.cmpcode = pod.cmpcode AND prd.prspdtloid = pod.prspdtloid AND prd.prspmstoid = pod.prspmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pod.pospmstoid=" + id + " ORDER BY pod.pospdtlseq";

                sSqlFtr = "SELECT (po.posptotalamt - po.posptotaldiscdtl) AS [Total], po.pospmstdiscamt AS [Header Disc.], po.pospvat AS [Tax], (po.pospdeliverycost + po.pospothercost) AS [Other Cost], po.pospgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnpospmst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.pospmstoid=" + id;
            }
            else if (tblname == "ql_trnpoitemmst")
            {
                sSqlHdr = "SELECT po.poitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poitemdate, 101) AS[PO Date], s.suppname AS[Supplier], po.poitemmstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnpoitemmst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poitemmstoid=" + id;

                sSqlDtl = "SELECT pod.poitemdtlseq AS [No.], m.itemcode AS [Mat.Code], m.itemlongdesc AS[Mat.Desc], CONVERT(VARCHAR(10), pritemarrdatereq, 101) AS [ETA], pod.poitemqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode = po.cmpcode AND mp.itemoid = pod.itemoid AND mp.refname = 'SUPPLIER' AND mp.curroid = po.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.poitemprice AS [DEC_Price], pod.poitemdtlamt AS [DEC_Amount], pod.poitemdtldiscamt AS [DEC_Disc], pod.poitemdtlnetto AS [DEC_Netto], pod.poitemdtlnote AS [Note] FROM QL_trnpoitemmst po INNER JOIN QL_trnpoitemdtl pod ON po.cmpcode = pod.cmpcode AND po.poitemmstoid = pod.poitemmstoid INNER JOIN QL_mstitem m ON pod.itemoid = m.itemoid INNER JOIN QL_mstgen g ON pod.poitemunitoid = g.genoid INNER JOIN QL_pritemdtl prd ON prd.cmpcode = pod.cmpcode AND prd.pritemdtloid = pod.pritemdtloid AND prd.pritemmstoid = pod.pritemmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pod.poitemmstoid=" + id + " ORDER BY pod.poitemdtlseq";

                sSqlFtr = "SELECT (po.poitemtotalamt - po.poitemtotaldiscdtl) AS [Total], po.poitemmstdiscamt AS [Header Disc.], po.poitemvat AS [Tax], (po.poitemdeliverycost + po.poitemothercost) AS [Other Cost], po.poitemgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnpoitemmst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poitemmstoid=" + id;
            }
            else if (tblname == "ql_trnposervicemst")
            {
                sSqlHdr = "SELECT po.poservicemstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poservicedate, 101) AS[PO Date], s.suppname AS[Supplier], po.poservicemstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnposervicemst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poservicemstoid=" + id;

                sSqlDtl = "SELECT pod.poservicedtlseq AS [No.], m.servicecode AS [Mat.Code], m.servicelongdesc AS[Mat.Desc], pod.poserviceqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 poserviceprice FROM QL_trnposervicedtl mp where mp.poservicedtloid <> pod.poservicedtloid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.poserviceprice AS [DEC_Price], pod.poserviceqty * pod.poserviceprice AS [DEC_Amount], ISNULL(pod.poservicedtldiscamt,0.0) AS [DEC_Disc], pod.poservicedtlnetto AS [DEC_Netto], pod.poservicedtlnote AS [Note] FROM QL_trnposervicemst po INNER JOIN QL_trnposervicedtl pod ON po.cmpcode = pod.cmpcode AND po.poservicemstoid = pod.poservicemstoid INNER JOIN QL_mstservice m ON pod.serviceoid = m.serviceoid INNER JOIN QL_mstgen g ON pod.poserviceunitoid = g.genoid  WHERE pod.cmpcode='" + CompnyCode + "' AND pod.poservicemstoid=" + id + " ORDER BY pod.poservicedtlseq";

                sSqlFtr = "SELECT (po.poservicetotalamt - po.poservicetotaldiscdtl) AS [Total], po.poservicemstdiscamt AS [Header Disc.], po.poservicevat AS [Tax], 0.0 AS [Other Cost], po.poservicegrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnposervicemst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poservicemstoid=" + id;
            }
            else if (tblname == "ql_trnpoassetmst")
            {
                sSqlHdr = "SELECT po.poassetmstoid AS [Draft No.], CONVERT(VARCHAR(10), po.poassetdate, 101) AS[PO Date], s.suppname AS[Supplier], po.poassetmstnote AS[PO Note], c.currcode AS[Currency], po.createuser AS[Create User], po.upduser[Last Update By], po.updtime AS[Last Update On] FROM QL_trnpoassetmst po INNER JOIN QL_mstsupp s ON po.suppoid = s.suppoid INNER JOIN QL_mstcurr c ON c.curroid = po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poassetmstoid=" + id;

                sSqlDtl = "SELECT pod.poassetdtlseq AS [No.], m.matgencode AS [Mat.Code], m.matgenlongdesc AS[Mat.Desc], CONVERT(VARCHAR(10), prassetarrdatereq, 101) AS [ETA], pod.poassetqty AS [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode = po.cmpcode AND mp.matgenoid = pod.poassetrefoid AND mp.refname = 'SUPPLIER' AND mp.curroid = po.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], pod.poassetprice AS [DEC_Price], pod.poassetdtlamt AS [DEC_Amount], pod.poassetdtldiscamt AS [DEC_Disc], pod.poassetdtlnetto AS [DEC_Netto], pod.poassetdtlnote AS [Note] FROM QL_trnpoassetmst po INNER JOIN QL_trnpoassetdtl pod ON po.cmpcode = pod.cmpcode AND po.poassetmstoid = pod.poassetmstoid INNER JOIN QL_mstmatgen m ON pod.poassetrefoid = m.matgenoid INNER JOIN QL_mstgen g ON pod.poassetunitoid = g.genoid INNER JOIN QL_prassetdtl prd ON prd.cmpcode = pod.cmpcode AND prd.prassetdtloid = pod.prassetdtloid AND prd.prassetmstoid = pod.prassetmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pod.poassetmstoid=" + id + " ORDER BY pod.poassetdtlseq";

                sSqlFtr = "SELECT (po.poassettotalamt - po.poassettotaldiscdtl) AS [Total], po.poassetmstdiscamt AS [Header Disc.], po.poassetvat AS [Tax], (po.poassetdeliverycost + po.poassetothercost) AS [Other Cost], po.poassetgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnpoassetmst po INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE po.cmpcode='" + CompnyCode + "' AND po.poassetmstoid=" + id;
            }
            else if (tblname == "ql_trnpretrawmst")
            {
                sSqlHdr = "SELECT h.pretrawmstoid [Draft No.], CONVERT(VARCHAR(10), pretrawdate, 101) [Return Date], suppname [Supplier], pretrawmstnote [Return Note] FROM QL_trnpretrawmst h INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE h.cmpcode='" + CompnyCode + "' AND h.pretrawmstoid=" + id + "";

                sSqlDtl = "SELECT pretrawdtlseq [No.], (SELECT mrrawno FROM QL_trnmrrawmst mrm WHERE mrm.cmpcode=pretd.cmpcode AND mrm.mrrawmstoid=pretd.mrrawmstoid) [MR No.], matrawcode [Mat. Code], matrawlongdesc [Mat. Desc], pretrawqty [DEC_Quantity], g.gendesc [Unit], g2.gendesc [Warehouse], pretrawdtlnote [Note] FROM QL_trnpretrawdtl pretd INNER JOIN QL_mstmatraw m ON m.matrawoid=pretd.matrawoid INNER JOIN QL_mstgen g ON g.genoid=pretd.pretrawunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=pretd.pretrawwhoid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretd.pretrawmstoid=" + id + " ORDER BY pretrawdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnpretgenmst")
            {
                sSqlHdr = "SELECT h.pretgenmstoid [Draft No.], CONVERT(VARCHAR(10), pretgendate, 101) [Return Date], suppname [Supplier], pretgenmstnote [Return Note] FROM QL_trnpretgenmst h INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE h.cmpcode='" + CompnyCode + "' AND h.pretgenmstoid=" + id + "";

                sSqlDtl = "SELECT pretgendtlseq [No.], (SELECT mrgenno FROM QL_trnmrgenmst mrm WHERE mrm.cmpcode=pretd.cmpcode AND mrm.mrgenmstoid=pretd.mrgenmstoid) [MR No.], matgencode [Mat. Code], matgenlongdesc [Mat. Desc], pretgenqty [DEC_Quantity], g.gendesc [Unit], g2.gendesc [Warehouse], pretgendtlnote [Note] FROM QL_trnpretgendtl pretd INNER JOIN QL_mstmatgen m ON m.matgenoid=pretd.matgenoid INNER JOIN QL_mstgen g ON g.genoid=pretd.pretgenunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=pretd.pretgenwhoid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretd.pretgenmstoid=" + id + " ORDER BY pretgendtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnpretspmst")
            {
                sSqlHdr = "SELECT h.pretspmstoid [Draft No.], CONVERT(VARCHAR(10), pretspdate, 101) [Return Date], suppname [Supplier], pretspmstnote [Return Note] FROM QL_trnpretspmst h INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE h.cmpcode='" + CompnyCode + "' AND h.pretspmstoid=" + id + "";

                sSqlDtl = "SELECT pretspdtlseq [No.], (SELECT mrspno FROM QL_trnmrspmst mrm WHERE mrm.cmpcode=pretd.cmpcode AND mrm.mrspmstoid=pretd.mrspmstoid) [MR No.], sparepartcode [Mat. Code], sparepartlongdesc [Mat. Desc], pretspqty [DEC_Quantity], g.gendesc [Unit], g2.gendesc [Warehouse], pretspdtlnote [Note] FROM QL_trnpretspdtl pretd INNER JOIN QL_mstsparepart m ON m.sparepartoid=pretd.sparepartoid INNER JOIN QL_mstgen g ON g.genoid=pretd.pretspunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=pretd.pretspwhoid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretd.pretspmstoid=" + id + " ORDER BY pretspdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnpretitemmst")
            {
                sSqlHdr = "SELECT h.pretitemmstoid [Draft No.], CONVERT(VARCHAR(10), pretitemdate, 101) [Return Date], suppname [Supplier], pretitemmstnote [Return Note] FROM QL_trnpretitemmst h INNER JOIN QL_mstsupp s ON s.suppoid=h.suppoid WHERE h.cmpcode='" + CompnyCode + "' AND h.pretitemmstoid=" + id + "";

                sSqlDtl = "SELECT pretitemdtlseq [No.], (SELECT mritemno FROM QL_trnmritemmst mrm WHERE mrm.cmpcode=pretd.cmpcode AND mrm.mritemmstoid=pretd.mritemmstoid) [MR No.], itemcode [Mat. Code], itemlongdesc [Mat. Desc], pretitemqty [DEC_Quantity], g.gendesc [Unit], g2.gendesc [Warehouse], pretitemdtlnote [Note] FROM QL_trnpretitemdtl pretd INNER JOIN QL_mstitem m ON m.itemoid=pretd.itemoid INNER JOIN QL_mstgen g ON g.genoid=pretd.pretitemunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=pretd.pretitemwhoid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretd.pretitemmstoid=" + id + " ORDER BY pretitemdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnaprawmst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.aprawmstoid AS [Draft No.], ap.aprawno AS [A/P No.], CONVERT(VARCHAR(10), ap.aprawdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=aprawpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),aprawdate), 101) AS [Due Date], CONVERT(VARCHAR(10), ap.aprawdatetakegiro, 101) AS [Date Take Giro], s.suppname [Supplier], ap.aprawmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnaprawmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.aprawmstoid=apr.oid AND apr.statusrequest='New' AND tablename='ql_trnaprawmst' INNER JOIN QL_mstgen g ON g.genoid=ap.aprawpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND ap.aprawmstoid=" + id + " ORDER BY [Request Date] DESC";

                sSqlDtl = "SELECT apd.aprawdtlseq AS [No.], m.matrawlongdesc AS [Mat. Desc], apd.aprawqty AS [DEC_Quantity], g.gendesc AS [Unit], apd.aprawprice AS [DEC_Price], apd.aprawdtlamt AS [DEC_Amount], apd.aprawdtldiscamt AS [DEC_Disc.], apd.aprawdtlnetto AS [DEC_Netto], apd.aprawdtlnote AS [Note] FROM QL_trnaprawmst ap INNER JOIN QL_trnaprawdtl apd ON ap.cmpcode=apd.cmpcode AND ap.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstmatraw m ON apd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON apd.aprawunitoid=g.genoid INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrrawdtloid=apd.mrrawdtloid AND mrd.mrrawmstoid=apd.mrrawmstoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.aprawmstoid=" + id + " ORDER BY apd.aprawdtlseq";

                sSqlFtr = "SELECT ap.aprawtotalamt AS [Total Amount], ap.aprawtotaltax AS [Total Tax], ap.aprawgrandtotal AS [Grand Total], c.currcode [Currency] FROM QL_trnaprawmst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid  WHERE ap.cmpcode='" + CompnyCode + "' AND ap.aprawmstoid=" + id;
            }
            else if (tblname == "ql_trnapgenmst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.apgenmstoid AS [Draft No.], ap.apgenno AS [A/P No.], CONVERT(VARCHAR(10), ap.apgendate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apgenpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apgendate), 101) AS [Due Date], CONVERT(VARCHAR(10), ap.apgendatetakegiro, 101) AS [Date Take Giro], s.suppname [Supplier], ap.apgenmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapgenmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apgenmstoid=apr.oid AND apr.statusrequest='New' AND tablename='ql_trnapgenmst' INNER JOIN QL_mstgen g ON g.genoid=ap.apgenpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apgenmstoid=" + id + " ORDER BY [Request Date] DESC";

                sSqlDtl = "SELECT apd.apgendtlseq AS [No.], m.matgenlongdesc AS [Mat. Desc], apd.apgenqty AS [DEC_Quantity], g.gendesc AS [Unit], apd.apgenprice AS [DEC_Price], apd.apgendtlamt AS [DEC_Amount], apd.apgendtldiscamt AS [DEC_Disc.], apd.apgendtlnetto AS [DEC_Netto], apd.apgendtlnote AS [Note] FROM QL_trnapgenmst ap INNER JOIN QL_trnapgendtl apd ON ap.cmpcode=apd.cmpcode AND ap.apgenmstoid=apd.apgenmstoid INNER JOIN QL_mstmatgen m ON apd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON apd.apgenunitoid=g.genoid INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrgendtloid=apd.mrgendtloid AND mrd.mrgenmstoid=apd.mrgenmstoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apgenmstoid=" + id + " ORDER BY apd.apgendtlseq";

                sSqlFtr = "SELECT ap.apgentotalamt AS [Total Amount], ap.apgentotaltax AS [Total Tax], ap.apgengrandtotal AS [Grand Total], c.currcode [Currency] FROM QL_trnapgenmst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid  WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apgenmstoid=" + id;
            }
            else if (tblname == "ql_trnapspmst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.apspmstoid AS [Draft No.], ap.apspno AS [A/P No.], CONVERT(VARCHAR(10), ap.apspdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apsppaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apspdate), 101) AS [Due Date], CONVERT(VARCHAR(10), ap.apspdatetakegiro, 101) AS [Date Take Giro], s.suppname [Supplier], ap.apspmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapspmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apspmstoid=apr.oid AND apr.statusrequest='New' AND tablename='ql_trnapspmst' INNER JOIN QL_mstgen g ON g.genoid=ap.apsppaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apspmstoid=" + id + " ORDER BY [Request Date] DESC";

                sSqlDtl = "SELECT apd.apspdtlseq AS [No.], m.sparepartlongdesc AS [Mat. Desc], apd.apspqty AS [DEC_Quantity], g.gendesc AS [Unit], apd.apspprice AS [DEC_Price], apd.apspdtlamt AS [DEC_Amount], apd.apspdtldiscamt AS [DEC_Disc.], apd.apspdtlnetto AS [DEC_Netto], apd.apspdtlnote AS [Note] FROM QL_trnapspmst ap INNER JOIN QL_trnapspdtl apd ON ap.cmpcode=apd.cmpcode AND ap.apspmstoid=apd.apspmstoid INNER JOIN QL_mstsparepart m ON apd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON apd.apspunitoid=g.genoid INNER JOIN QL_trnmrspdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrspdtloid=apd.mrspdtloid AND mrd.mrspmstoid=apd.mrspmstoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apspmstoid=" + id + " ORDER BY apd.apspdtlseq";

                sSqlFtr = "SELECT ap.apsptotalamt AS [Total Amount], ap.apsptotaltax AS [Total Tax], ap.apspgrandtotal AS [Grand Total], c.currcode [Currency] FROM QL_trnapspmst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apspmstoid=" + id;
            }
            else if (tblname == "ql_trnapitemmst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.apitemmstoid AS [Draft No.], ap.apitemno AS [A/P No.], CONVERT(VARCHAR(10), ap.apitemdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apitempaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apitemdate), 101) AS [Due Date], CONVERT(VARCHAR(10), ap.apitemdatetakegiro, 101) AS [Date Take Giro], s.suppname [Supplier], ap.apitemmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapitemmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apitemmstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.apitempaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apitemmstoid=" + id + " ORDER BY [Request Date] DESC";

                sSqlDtl = "SELECT apd.apitemdtlseq AS [No.], m.itemlongdesc AS [Mat. Desc], apd.apitemqty AS [DEC_Quantity], g.gendesc AS [Unit], apd.apitemprice AS [DEC_Price], apd.apitemdtlamt AS [DEC_Amount], apd.apitemdtldiscamt AS [DEC_Disc.], apd.apitemdtlnetto AS [DEC_Netto], apd.apitemdtlnote AS [Note] FROM QL_trnapitemmst ap INNER JOIN QL_trnapitemdtl apd ON ap.cmpcode=apd.cmpcode AND ap.apitemmstoid=apd.apitemmstoid INNER JOIN QL_mstitem m ON apd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON apd.apitemunitoid=g.genoid INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mritemdtloid=apd.mritemdtloid AND mrd.mritemmstoid=apd.mritemmstoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apitemmstoid=" + id + " ORDER BY apd.apitemdtlseq";

                sSqlFtr = "SELECT ap.apitemtotalamt AS [Total Amount], ap.apitemtotaltax AS [Total Tax], ap.apitemgrandtotal AS [Grand Total], c.currcode [Currency] FROM QL_trnapitemmst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apitemmstoid=" + id;
            }
            else if (tblname == "ql_trnapservicemst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.apservicemstoid [Draft No.], apserviceno [A/P No.], CONVERT(VARCHAR(10), apservicedate, 101) [A/P Date], suppname [Supplier], currcode [Currency], apservicemstnote [A/P Note], ap.upduser [Last Update By], ap.updtime [Last Update On], CONVERT(VARCHAR(10), DATEADD(day, (CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END), apservicedate), 101) [Recom. Due Date] FROM QL_trnapservicemst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid INNER JOIN QL_mstgen g ON g.genoid=ap.apservicepaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + cmp + "' AND ap.apservicemstoid=" + id;

                sSqlDtl = "SELECT apservicedtlseq AS [No.], servicecode [Code], servicelongdesc [Description], apserviceqty [DEC_Qty], g.gendesc [Unit], apserviceprice [DEC_Price], apservicedtlamt [DEC_Amount], apservicedtlnote [Note] FROM QL_trnapservicemst ap INNER JOIN QL_trnapservicedtl apd ON ap.cmpcode=apd.cmpcode AND ap.apservicemstoid=apd.apservicemstoid INNER JOIN QL_mstservice m ON apd.serviceoid=m.serviceoid INNER JOIN QL_mstgen g ON apserviceunitoid=g.genoid WHERE ap.cmpcode='" + cmp + "' AND ap.apservicemstoid=" + id + " ORDER BY apservicedtlseq";

                sSqlFtr = "SELECT apservicetotalamt [Total Amount], apservicetotaltax [Total Tax], apservicegrandtotal [Grand Total], currcode [Currency] FROM QL_trnapservicemst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid WHERE ap.cmpcode='" + cmp + "' AND ap.apservicemstoid=" + id;
            }
            else if (tblname == "ql_trnapassetmst")
            {
                sSqlHdr = "SELECT DISTINCT ap.cmpcode, ap.apassetmstoid AS [Draft No.], ap.apassetno AS [A/P No.], CONVERT(VARCHAR(10), ap.apassetdate, 101) AS [A/P Date], (SELECT ga.gendesc FROM QL_mstgen ga WHERE ga.genoid=apassetpaytypeoid ) AS [Payment Type], CONVERT(VARCHAR(10),DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apassetdate), 101) AS [Due Date], CONVERT(VARCHAR(10), ap.apassetdatetakegiro, 101) AS [Date Take Giro], s.suppname [Supplier], ap.apassetmstnote AS [A/P Note], apr.requestuser AS [Request User], CONVERT(VARCHAR(10), apr.requestdate, 101) AS [Request Date] FROM QL_trnapassetmst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.apassetmstoid=apr.oid AND apr.statusrequest='New' AND tablename='ql_trnapassetmst' INNER JOIN QL_mstgen g ON g.genoid=ap.apassetpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apassetmstoid=" + id + " ORDER BY [Request Date] DESC";

                sSqlDtl = "SELECT apd.apassetdtlseq AS [No.], m.matgenlongdesc AS [Mat. Desc], apd.apassetqty AS [DEC_Quantity], g.gendesc AS [Unit], apd.apassetprice AS [DEC_Price], apd.apassetdtlamt AS [DEC_Amount], apd.apassetdtldiscamt AS [DEC_Disc.], apd.apassetdtlnetto AS [DEC_Netto], apd.apassetdtlnote AS [Note] FROM QL_trnapassetmst ap INNER JOIN QL_trnapassetdtl apd ON ap.cmpcode=apd.cmpcode AND ap.apassetmstoid=apd.apassetmstoid INNER JOIN QL_mstmatgen m ON apd.apassetrefoid=m.matgenoid INNER JOIN QL_mstgen g ON apd.apassetunitoid=g.genoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrassetdtloid=apd.mrassetdtloid AND mrd.mrassetmstoid=apd.mrassetmstoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apassetmstoid=" + id + " ORDER BY apd.apassetdtlseq";

                sSqlFtr = "SELECT ap.apassettotalamt AS [Total Amount], ap.apassettotaltax AS [Total Tax], ap.apassetgrandtotal AS [Grand Total], c.currcode [Currency] FROM QL_trnapassetmst ap INNER JOIN QL_mstcurr c ON c.curroid=ap.curroid  WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apassetmstoid=" + id;
            }
            else if (tblname == "ql_trnstockadj")
            {
                sSqlHdr = "SELECT DISTINCT CAST(resfield1 AS INTEGER) [Draft No.], CONVERT(VARCHAR(10), stockadjdate, 101) [Adj Date], stockadjtype [Type], stockadjmstnote [Header Note] FROM QL_trnstockadj h WHERE h.cmpcode='" + CompnyCode + "' AND CAST(resfield1 AS INTEGER)=" + id + "";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT 0 [No], '' dataall, a.resfield1 [mstoid], a.stockadjoid [dtloid], a.refoid [itemoid], a.mtrwhoid [whoid], a.refname [refname], (CASE a.refname WHEN 'RAW MATERIAL' THEN (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=a.refoid) WHEN 'GENERAL MATERIAL' THEN (SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid=a.refoid) WHEN 'SPARE PART' THEN (SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid=a.refoid) WHEN 'FINISH GOOD' THEN (SELECT m.itemcode FROM QL_mstitem m WHERE m.itemoid=a.refoid) WHEN 'LOG' THEN (SELECT (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=l.refoid) + ' - ' + l.logno FROM QL_mstlog l WHERE l.logoid=a.refoid) WHEN 'PALLET' THEN (SELECT (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=p.refoid) + ' - ' + p.palletno FROM QL_mstpallet p WHERE p.cmpcode=a.cmpcode AND p.palletoid=a.refoid) ELSE '' END) AS [Mat_Code], (CASE a.refname WHEN 'RAW MATERIAL' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=a.refoid) WHEN 'GENERAL MATERIAL' THEN (SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=a.refoid) WHEN 'SPARE PART' THEN (SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid=a.refoid) WHEN 'FINISH GOOD' THEN (SELECT m.itemlongdesc FROM QL_mstitem m WHERE m.itemoid=a.refoid) WHEN 'LOG' THEN (SELECT (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=l.refoid) FROM QL_mstlog l WHERE l.logoid=a.refoid) WHEN 'PALLET' THEN (SELECT (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=p.refoid) FROM QL_mstpallet p WHERE p.cmpcode=a.cmpcode AND p.palletoid=a.refoid) ELSE '' END) AS [Mat_Desc], g1.gendesc AS [Location], a.stockadjqtybefore AS [Current_Qty], a.stockadjqty AS [Adj_Qty], a.stockadjqtyafter AS [New_Qty], g2.gendesc AS [Unit], (CASE stockadjtype WHEN 'ADJUSTMENT' THEN ISNULL((SELECT SUM(ISNULL(st.stockqty, 0) * ISNULL(st.stockvalueidr, 0)) / NULLIF(SUM(ISNULL(st.stockqty, 0)), 0) FROM QL_stockvalue st WHERE st.cmpcode=a.cmpcode AND st.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND st.refoid=a.refoid AND st.refname=a.refname AND closeflag=''), 0.0) ELSE 0.0 END) AS [Value], stockadjnote [Detail_Note] FROM QL_trnstockadj a INNER JOIN QL_mstgen g1 ON g1.genoid=a.mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=a.stockadjunit WHERE a.cmpcode='" + CompnyCode + "' AND CAST(a.resfield1 AS INTEGER)='" + id + "' ORDER BY a.stockadjoid";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnsoitemmst") {
                sSqlHdr = "SELECT so.soitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.soitemdate, 101) AS [SO Date], c.custname [Customer], so.soitemmstnote [SO Note], isnull(soitemmstres1,0) [Toleransi], currcode [Currency], ISNULL(rate2res1, 0) AS [Rate To IDR], so.createuser [Create User], so.upduser [Last Update By], so.updtime [Last Update On] FROM QL_trnsoitemmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid LEFT JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE so.cmpcode='" + cmp + "' AND so.soitemmstoid=" + id + "";

                sSqlDtl = "SELECT sod.soitemdtlseq [No.], m.itemlongdesc AS [Mat. Desc], CONVERT(VARCHAR(10), sod.soitemdtletd, 101) AS [ETD], sod.soitemqty [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode=so.cmpcode AND mp.itemoid=sod.itemoid AND mp.refname='CUSTOMER' AND mp.curroid=so.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], sod.soitemprice [DEC_Price], sod.soitemdtlamt [DEC_Amount], sod.soitemdtldiscamt [DEC_Disc.], sod.soitemdtlnetto [DEC_Netto], sod.soitemdtlnote [Note] FROM QL_trnsoitemmst so INNER JOIN QL_trnsoitemdtl sod ON so.soitemmstoid=sod.soitemmstoid INNER JOIN QL_mstitem m ON sod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sod.soitemunitoid=g.genoid WHERE so.cmpcode='" + cmp + "' AND so.soitemmstoid=" + id + " ORDER BY sod.soitemdtlseq";

                sSqlFtr = "SELECT (so.soitemtotalamt - so.soitemtotaldiscdtl) AS [Total], so.soitemmstdiscamt [Header Disc.], so.soitemvat AS [Tax], so.soitemgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnsoitemmst so INNER JOIN QL_mstcurr c ON c.curroid=so.curroid WHERE so.cmpcode='" + cmp + "' AND so.soitemmstoid=" + id;
            }
            else if (tblname == "ql_trnsorawmst")
            {
                sSqlHdr = "SELECT so.sorawmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.sorawdate, 101) AS [SO Date], c.custname [Customer], so.sorawmstnote [SO Note], currcode [Currency], rate2res1 AS [Rate To IDR], so.createuser [Create User], so.upduser [Last Update By], so.updtime [Last Update On] FROM QL_trnsorawmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid INNER JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id + "";

                sSqlDtl = "SELECT sod.sorawdtlseq [No.], sodtldesc AS [Mat. Desc], sod.sorawqty [DEC_Quantity], g.gendesc AS [Unit], sod.sorawprice [DEC_Price], sod.sorawdtlamt [DEC_Amount], sod.sorawdtldiscamt [DEC_Disc.], sod.sorawdtlnetto [DEC_Netto], sod.sorawdtlnote [Note] FROM QL_trnsorawmst so INNER JOIN QL_trnsorawdtl sod ON so.sorawmstoid=sod.sorawmstoid INNER JOIN QL_mstgen g ON sod.sorawunitoid=g.genoid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id + " ORDER BY sod.sorawdtlseq";

                sSqlFtr = "SELECT (so.sorawtotalamt - so.sorawtotaldiscdtl) AS [Total], so.sorawvat AS [Tax], so.sorawgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnsorawmst so INNER JOIN QL_mstcurr c ON c.curroid=so.curroid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id;
            }
            else if(tblname == "ql_trnshipmentitemmst")
            {
                sSqlHdr = "SELECT sm.shipmentitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentitemdate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentitemmstnote [Shipment Note], sm.upduser [Last Update By], sm.updtime [Last Update On] FROM QL_trnshipmentitemmst sm INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE sm.cmpcode='" + cmp + "' AND sm.shipmentitemmstoid=" + id + "";

                sSqlDtl = "SELECT sd.shipmentitemdtlseq [No.], m.itemcode [Code], m.itemlongdesc [Description], sd.shipmentitemqty [Qty], g.gendesc AS [Unit], g2.gendesc AS [Warehouse], sd.shipmentitemdtlnote [Note] FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid INNER JOIN QL_mstitem m ON sd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sd.shipmentitemunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentitemwhoid=g2.genoid WHERE sd.shipmentitemmstoid=" + id + " ORDER BY sd.shipmentitemdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnshipmentrawmst")
            {
                sSqlHdr = "SELECT sm.shipmentrawmstoid AS [Draft No.], CONVERT(VARCHAR(10), sm.shipmentrawdate, 101) AS [Shipment Date], c.custname [Customer], sm.shipmentrawmstnote [Shipment Note], sm.upduser [Last Update By], sm.updtime [Last Update On] FROM QL_trnshipmentrawmst sm INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE sm.cmpcode='" + cmp + "' AND sm.shipmentrawmstoid=" + id + "";

                sSqlDtl = "SELECT sd.shipmentrawdtlseq [No.], 'Sheet' [Code], (select x.sodtldesc from ql_trnsorawdtl x where x.sorawdtloid=sd.matrawoid) [Description], sd.shipmentrawqty [Qty], g.gendesc AS [Unit], g2.gendesc AS [Warehouse], sd.shipmentrawdtlnote [Note] FROM QL_trnshipmentrawmst sm INNER JOIN QL_trnshipmentrawdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid INNER JOIN QL_mstgen g ON sd.shipmentrawunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentrawwhoid=g2.genoid WHERE sd.shipmentrawmstoid=" + id + " ORDER BY sd.shipmentrawdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnsretitemmst")
            {
                sSqlHdr = "SELECT sretitemmstoid AS [Draft No.], CONVERT(VARCHAR(10), sretitemdate, 101) AS [Return Date], custname [Customer], sretitemmstnote AS [Return Note], sretm.upduser [Last Update By], sretm.updtime [Last Update On] FROM QL_trnsretitemmst sretm INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE sretm.sretitemmstoid=" + id;

                sSqlDtl = "SELECT sretitemdtlseq AS [No.], itemcode AS [Code], itemlongdesc AS [Description], sretitemqty AS [Qty], gunit.gendesc AS [Unit], gwh.gendesc AS [Warehouse], sretitemdtlnote AS [Note] FROM QL_trnsretitemmst sretm INNER JOIN QL_trnsretitemdtl sretd ON sretd.cmpcode=sretm.cmpcode AND sretd.sretitemmstoid=sretm.sretitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=sretd.itemoid INNER JOIN QL_mstgen gunit ON gunit.genoid=sretitemunitoid INNER JOIN QL_mstgen gwh ON gwh.genoid=sretitemwhoid WHERE sretm.sretitemmstoid=" + id + " ORDER BY sretitemdtlseq";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "ql_trnaritemmst")
            {
                sSqlHdr = "SELECT aritemmstoid AS [Draft No.], CONVERT(VARCHAR(10), aritemdate, 101) AS [PEB Date], custname [Customer], ISNULL(aritemmstres3,'') AS [Invoice No.], aritemmstnote AS [A/R Note], arm.upduser [Last Update By], arm.updtime [Last Update On] FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE arm.aritemmstoid=" + id;

                sSqlDtl = "SELECT aritemdtlseq AS [No.], itemcode AS [Code], itemlongdesc AS [Description], aritemqty AS [Qty], gendesc AS [Unit], aritemprice AS [Price], aritemdtlamt AS [Amount], aritemdtldiscamt AS [Disc.], aritemdtlnetto AS [Netto], aritemdtlnote AS [Note] FROM QL_trnaritemmst arm INNER JOIN QL_trnaritemdtl ard ON ard.cmpcode=arm.cmpcode AND ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_mstgen g ON genoid=aritemunitoid WHERE arm.aritemmstoid=" + id + " ORDER BY aritemdtlseq";

                sSqlFtr = "SELECT (aritemtotalamt - aritemtotaldiscdtl) [Total], aritemmstdiscamt [Header Disc.], aritemtaxamt [Tax], aritemgrandtotal [Grand Total], c.currcode [Currency] FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid=arm.curroid WHERE arm.aritemmstoid=" + id;
            }
            else if (tblname == "ql_trnarrawmst")
            {
                sSqlHdr = "SELECT arrawmstoid AS [Draft No.], CONVERT(VARCHAR(10), arrawdate, 101) AS [PEB Date], custname [Customer], ISNULL(arrawmstres3,'') AS [Invoice No.], arrawmstnote AS [A/R Note], arm.upduser [Last Update By], arm.updtime [Last Update On] FROM QL_trnarrawmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE arm.arrawmstoid=" + id;

                sSqlDtl = "SELECT arrawdtlseq AS [No.], 'Sheet' AS [Code], (select x.sodtldesc from ql_trnsorawdtl x where x.sorawdtloid=ard.matrawoid) AS [Description], arrawqty AS [Qty], gendesc AS [Unit], arrawprice AS [Price], arrawdtlamt AS [Amount], arrawdtldiscamt AS [Disc.], arrawdtlnetto AS [Netto], arrawdtlnote AS [Note] FROM QL_trnarrawmst arm INNER JOIN QL_trnarrawdtl ard ON ard.cmpcode=arm.cmpcode AND ard.arrawmstoid=arm.arrawmstoid INNER JOIN QL_mstgen g ON genoid=arrawunitoid WHERE arm.arrawmstoid=" + id + " ORDER BY arrawdtlseq";

                sSqlFtr = "SELECT (arrawtotalamt - arrawtotaldiscdtl) [Total], arrawmstdiscamt [Header Disc.], arrawtaxamt [Tax], arrawgrandtotal [Grand Total], c.currcode [Currency] FROM QL_trnarrawmst arm INNER JOIN QL_mstcurr c ON c.curroid=arm.curroid WHERE arm.arrawmstoid=" + id;
            }
            else if (tblname == "ql_trnsorawmst")
            {
                sSqlHdr = "SELECT so.sorawmstoid AS [Draft No.], CONVERT(VARCHAR(10), so.sorawdate, 101) AS [SO Date], c.custname [Customer], so.sorawmstnote [SO Note], currcode [Currency], rate2res1 AS [Rate To IDR], so.createuser [Create User], so.upduser [Last Update By], so.updtime [Last Update On] FROM QL_trnsorawmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid INNER JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id + "";

                sSqlDtl = "SELECT sod.sorawdtlseq [No.], m.matrawlongdesc AS [Mat. Desc], sod.sorawqty [DEC_Quantity], g.gendesc AS [Unit], ISNULL((SELECT TOP 1 mp.lastsalesprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode=so.cmpcode AND mp.matrawoid=sod.matrawoid AND mp.refname='CUSTOMER' AND mp.curroid=so.curroid ORDER BY mp.updtime DESC), 0.0) AS [DEC_Last Price], sod.sorawprice [DEC_Price], sod.sorawdtlamt [DEC_Amount], sod.sorawdtldiscamt [DEC_Disc.], sod.sorawdtlnetto [DEC_Netto], sod.sorawdtlnote [Note] FROM QL_trnsorawmst so INNER JOIN QL_trnsorawdtl sod ON so.sorawmstoid=sod.sorawmstoid INNER JOIN QL_mstmatraw m ON sod.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON sod.sorawunitoid=g.genoid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id + " ORDER BY sod.sorawdtlseq";

                sSqlFtr = "SELECT (so.sorawtotalamt - so.sorawtotaldiscdtl) AS [Total], so.sorawmstdiscamt [Header Disc.], so.sorawvat AS [Tax], so.sorawgrandtotalamt AS [Grand Total], c.currcode [Currency] FROM QL_trnsorawmst so INNER JOIN QL_mstcurr c ON c.curroid=so.curroid WHERE so.cmpcode='" + cmp + "' AND so.sorawmstoid=" + id;
            }
            else
                msg = "Approval action haven't be set";

            if (msg == "")
            {
                // Process Data for Header
                DataTable tblhdr = new ClassConnection().GetDataTable(sSqlHdr, tblname + "_hdr");
                List<modelforapp> rowshdr = new List<modelforapp>();
                if (tblhdr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblhdr.Rows)
                    {
                        foreach (DataColumn col in tblhdr.Columns)
                        {
                            var item = dr[col].ToString();
                            rowshdr.Add(new modelforapp()
                            {
                                caption = col.ColumnName,
                                value = item
                            });
                        }
                    }
                }
                else
                    msg = "Data Header Not Found";

                // Process Data for Detail
                DataTable tbldtl = new ClassConnection().GetDataTable(sSqlDtl, tblname + "_dtl");
                List<string> colsdtl = new List<string>();
                List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
                if (msg == "" && tbldtl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbldtl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbldtl.Columns)
                        {
                            var item = dr[col].ToString();
                            decimal decval = 0;
                            if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                            {
                                if (decval == 0)
                                    item = "0";
                                else
                                    item = decval.ToString("#,##0.00");
                            }
                            if (tblname == "QL_prrawmst" || tblname == "QL_prgenmst" || tblname == "QL_prspmst" || tblname == "ql_pritemmst")
                            {
                                if (col.ColumnName == "Mat. Code")
                                {
                                    item = "<a class='text-primary' href='" + Url.Action("Report/" + type + "/", "StockReport") + "' target='_blank'>" + item + "</a>";
                                }
                            }                            
                            row.Add(col.ColumnName.Replace("DEC_", ""), item);
                            if (!colsdtl.Contains(col.ColumnName.Replace("DEC_", "")))
                                colsdtl.Add(col.ColumnName.Replace("DEC_", ""));
                        }
                        rowsdtl.Add(row);
                    }
                }
                else
                    msg = "Data Detail Not Found";

                // Process Data for Footer
                DataTable tblftr = new ClassConnection().GetDataTable(sSqlFtr, tblname + "_ftr");
                List<modelforapp> rowsftr = new List<modelforapp>();
                var curr = "";
                if (msg == "" && tblftr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblftr.Rows)
                    {
                        foreach (DataColumn col in tblftr.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName.StartsWith("Currency"))
                            {
                                curr = item;
                            }
                        }
                            foreach (DataColumn col in tblftr.Columns)
                        {
                            var item = dr[col].ToString();
                            decimal decval = 0;

                            if (!col.ColumnName.StartsWith("Currency"))
                            {
                                if (!string.IsNullOrEmpty(item) && Decimal.TryParse(item, out decval))
                                    item = decval.ToString("#,##0.00");
                                if (col.ColumnName == "Grand Total")
                                {
                                    item =  "(" + curr + ") " + item;
                                }
                                if (col.ColumnName != "No Footer")
                                {
                                    rowsftr.Add(new modelforapp()
                                    {
                                        caption = col.ColumnName,
                                        value = item
                                    });
                                }
                            }
                        }
                    }
                }

                if (msg == "")
                    return Json(new { msg, info, rowshdr, colsdtl, rowsdtl, rowsftr }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: WaitingAction/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(waitactmodel tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var transnofld = tbl.tblname.ToLower().Replace("ql_trn", "").Replace("ql_", "").Replace("mst", "") + "no";
            var transoidfld = tbl.tblname.ToLower().Replace("ql_trn", "").Replace("ql_", "") + "oid";
            var transno = (action == "Approved"? db.Database.SqlQuery<string>("SELECT " + transnofld + " FROM " + tbl.tblname + " WHERE cmpcode='" + tbl.cmpcode + "' AND " + transoidfld + "=" + tbl.mstoid + "").FirstOrDefault(): "");
            if (action == "Approved" && string.IsNullOrEmpty(transno))
            {
                var prefix = db.Database.SqlQuery<string>("SELECT TOP 1 SUBSTRING(" + transnofld + ", 1, CHARINDEX('-', " + transnofld + ") - 1) FROM " + tbl.tblname + " WHERE " + transnofld + "<>'' AND " + transoidfld + " > 0 ORDER BY " + transoidfld + " DESC").FirstOrDefault();
                if (tbl.tblname.ToLower() == "ql_prrawmst")
                    prefix = "PRRM";
                else if (tbl.tblname.ToLower() == "ql_prgenmst")
                    prefix = "PRGM";
                else if (tbl.tblname.ToLower() == "ql_pritemmst")
                    prefix = "PRFG";
                else if (tbl.tblname.ToLower() == "ql_prassetmst")
                    prefix = "PRAS";
                else if (tbl.tblname.ToLower() == "ql_trnporawmst")
                    prefix = "PORM";
                else if (tbl.tblname.ToLower() == "ql_trnpogenmst")
                    prefix = "POGM";
                else if (tbl.tblname.ToLower() == "ql_trnpoitemmst")
                    prefix = "POFG";
                else if (tbl.tblname.ToLower() == "ql_trnpospmst")
                    prefix = "POSP";
                else if (tbl.tblname.ToLower() == "ql_trnpoassetmst")
                    prefix = "POAS";
                else if (tbl.tblname.ToLower() == "ql_trnaprawmst")
                    prefix = "APRM";
                else if (tbl.tblname.ToLower() == "ql_trnapgenmst")
                    prefix = "APGM";
                else if (tbl.tblname.ToLower() == "ql_trnapservicemst")
                    prefix = "APSC";
                else if (tbl.tblname.ToLower() == "ql_trnapassetmst")
                    prefix = "APAS";
                else if (tbl.tblname.ToLower() == "ql_trnsoitemmst")
                    prefix = "SOFG";
                else if (tbl.tblname.ToLower() == "ql_trnsorawmst")
                    prefix = "SORM";
                else if (tbl.tblname.ToLower() == "ql_trnshipmentitemmst")
                    prefix = "SJFG";
                else if (tbl.tblname.ToLower() == "ql_trnsretitemmst")
                    prefix = "SRFG";
                else if (tbl.tblname.ToLower() == "ql_trnaritemmst")
                    prefix = "ARFG";
                else if (tbl.tblname.ToLower() == "ql_trnarrawmst")
                    prefix = "ARST";
                else if (tbl.tblname.ToLower() == "ql_trnstockadj")
                    prefix = "ADJFG";
                else if (tbl.tblname.ToLower() == "ql_trnposervicemst")
                    prefix = "POSVC";

                transno = GenerateTransNo(prefix, tbl.cmpcode, tbl.tblname, transnofld);
            }

            var error = "";
            if (tbl.tblname.ToLower() == "ql_prrawmst")
                ApprovalPRRM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_prgenmst")
                ApprovalPRGM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_prspmst")
                ApprovalPRSP(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_pritemmst")
                ApprovalPRFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_prassetmst")
                ApprovalPRAS(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnporawmst")
                ApprovalPORM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnpogenmst")
                ApprovalPOGM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnpospmst")
                ApprovalPOSP(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnpoitemmst")
                ApprovalPOFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnpoassetmst")
                ApprovalPOAS(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnposervicemst")
                ApprovalPOSVC(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnpretrawmst")
            //    ApprovalPretRM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnpretgenmst")
            //    ApprovalPretGM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnpretspmst")
            //    ApprovalPretSP(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnpretitemmst")
            //    ApprovalPretFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnaprawmst")
                ApprovalAPRM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnapgenmst")
                ApprovalAPGM(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnapservicemst")
                ApprovalAPService(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnapassetmst")
                ApprovalAPAS(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnapspmst")
            //    ApprovalAPSP(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //else if (tbl.tblname.ToLower() == "ql_trnapitemmst")
            //    ApprovalAPFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnsoitemmst")
                ApprovalSOFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnsorawmst")
                ApprovalSORaw(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //shipment
            else if (tbl.tblname.ToLower() == "ql_trnshipmentrawmst")
                ApprovalShipmentRaw(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnshipmentitemmst")
                ApprovalShipmentFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnsretitemmst")
                ApprovalSretFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            //ar
            else if (tbl.tblname.ToLower() == "ql_trnarrawmst")
                ApprovalARRaw(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname.ToLower() == "ql_trnaritemmst")
                ApprovalARFG(tbl.mstoid, tbl.cmpcode, transno, action, out error);
            else if (tbl.tblname == "ql_trnstockadj")
                ApprovalStockAdj(tbl.mstoid, CompnyCode, transno, action, out error);

            if (string.IsNullOrEmpty(error))
                return RedirectToAction("Index/" + tbl.tblname);

            ModelState.AddModelError("", error);
            return View(tbl);
        }

        public class stockadj
        {
            public int stockadjoid { get; set; }
            public decimal stockvalue { get; set; }
            public int resfield1 { get; set; }
            public int itemoid { get; set; }
            public string refno { get; set; }
            public string refname { get; set; }
            public int mtrwhoid { get; set; }
            public decimal stockadjqty { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<stockadj> dtDtl)
        {
            Session["QL_trnstockadj"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string GenerateTransNo(string prefix, string cmp, string tblname, string transnofld)
        {
            var sNo = prefix + "-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(" + transnofld + ", " + int.Parse(NumberCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM " + tblname + " WHERE cmpcode='" + CompnyCode + "' AND " + transnofld + " LIKE '" + sNo + "%'";
            sNo = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(NumberCounter));
            return sNo;
        }


        #region Approval PR
        private void ApprovalPRRM(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            error = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    sSql = "UPDATE QL_prrawmst SET prrawno='" + transno + "', prrawmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE cmpcode='" + CompnyCode + "' AND prrawmststatus='In Approval' AND prrawmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_prrawmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch(Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPRGM(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            error = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    sSql = "UPDATE QL_prgenmst SET prgenno='" + transno + "', prgenmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' "+ sqlPlus +" WHERE cmpcode='" + CompnyCode + "' AND prgenmststatus='In Approval' AND prgenmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_prgenmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPRSP(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            error = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    sSql = "UPDATE QL_prspmst SET prspno='" + transno + "', prspmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' "+ sqlPlus +" WHERE cmpcode='" + CompnyCode + "' AND prspmststatus='In Approval' AND prspmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_prspmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPRFG(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            error = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    sSql = "UPDATE QL_pritemmst SET pritemno='" + transno + "', pritemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' "+ sqlPlus +" WHERE cmpcode='" + CompnyCode + "' AND pritemmststatus='In Approval' AND pritemmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_pritemmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPRAS(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            error = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    sSql = "UPDATE QL_prassetmst SET prassetno='" + transno + "', prassetmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE cmpcode='" + CompnyCode + "' AND prassetmststatus='In Approval' AND prassetmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_prassetmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }
        #endregion

        #region Approval Purchase Order
        private void ApprovalPORM(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var iPriceOid = ClassFunction.GenerateID("QL_MSTMATRAWPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnporawmst WHERE cmpcode='" + CompnyCode + "' AND porawmstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnporawmst WHERE cmpcode='" + CompnyCode + "' AND porawmstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT porawdate FROM QL_trnporawmst WHERE cmpcode='" + CompnyCode + "' AND porawmstoid=" + id).FirstOrDefault();
            List<QL_trnporawdtl> tbldtl = db.QL_trnporawdtl.Where(x => x.cmpcode == CompnyCode && x.porawmstoid == id).ToList();
            //if (action == "Approved")
            //{
            cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

            //    if (cRate.GetRateDailyLastError != "")
            //        error = cRate.GetRateDailyLastError;
            //    if (cRate.GetRateMonthlyLastError != "")
            //        error = cRate.GetRateMonthlyLastError;
            //}            
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnporawmst SET porawno='" + transno + "', porawmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", porawratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', porawratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', porawrate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', porawrate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' "+ sqlPlus +" WHERE porawmststatus='In Approval' AND porawmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnporawmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_prrawdtl SET prrawdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND prrawdtloid IN (SELECT prrawdtloid FROM QL_trnporawdtl WHERE cmpcode='" + CompnyCode + "' AND porawmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prrawmst SET prrawmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND prrawmstoid IN (SELECT DISTINCT prrawmstoid FROM QL_trnporawdtl WHERE cmpcode='" + CompnyCode + "' AND porawmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                    }

                    if (action == "Approved")
                    {
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var matoid = tbldtl[i].matrawoid;
                            QL_mstmatrawprice tblMatPrice = db.QL_mstmatrawprice.Where(x => x.cmpcode == CompnyCode && x.matrawoid == matoid && x.refoid == suppoid && x.refname == "SUPPLIER" && x.curroid== curroid).FirstOrDefault();

                            if (tblMatPrice != null)
                            {
                                tblMatPrice.totalpurchaseqty = (tblMatPrice.totalpurchaseqty + tbldtl[i].porawqty);
                                tblMatPrice.avgpurchaseprice = ((tblMatPrice.avgpurchaseprice * tblMatPrice.totalpurchaseqty) + tbldtl[i].porawdtlamt) / (tblMatPrice.totalpurchaseqty + tbldtl[i].porawqty);
                                tblMatPrice.lastpurchaseprice = tbldtl[i].porawprice;
                                tblMatPrice.lasttrans = "QL_trnporawmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                db.Entry(tblMatPrice).State = EntityState.Modified;
                                //db.SaveChanges();
                            }
                            else
                            {
                                tblMatPrice = new QL_mstmatrawprice();
                                tblMatPrice.cmpcode = cmp;
                                tblMatPrice.matrawpriceoid = iPriceOid;
                                tblMatPrice.matrawoid = tbldtl[i].matrawoid;
                                tblMatPrice.refoid = suppoid;
                                tblMatPrice.refname = "SUPPLIER";
                                tblMatPrice.totalpurchaseqty = tbldtl[i].porawqty;
                                tblMatPrice.totalsalesqty = 0;
                                tblMatPrice.avgpurchaseprice = tbldtl[i].porawprice;
                                tblMatPrice.lastpurchaseprice = tbldtl[i].porawprice;
                                tblMatPrice.avgsalesprice = 0;
                                tblMatPrice.lastsalesprice = 0;
                                tblMatPrice.lasttrans = "QL_trnporawmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.note = "";
                                tblMatPrice.res1 = "";
                                tblMatPrice.res2 = "";
                                tblMatPrice.res3 = "";
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.curroid = curroid;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                db.QL_mstmatrawprice.Add(tblMatPrice);
                                //db.SaveChanges();
                            }

                            iPriceOid++;
                        }
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTMATRAWPRICE'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                    }

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPOGM(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var iPriceOid = ClassFunction.GenerateID("QL_MSTMATGENPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnpogenmst WHERE cmpcode='" + CompnyCode + "' AND pogenmstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnpogenmst WHERE cmpcode='" + CompnyCode + "' AND pogenmstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT pogendate FROM QL_trnpogenmst WHERE cmpcode='" + CompnyCode + "' AND pogenmstoid=" + id).FirstOrDefault();
            List<QL_trnpogendtl> tbldtl = db.QL_trnpogendtl.Where(x => x.cmpcode == CompnyCode && x.pogenmstoid == id).ToList();
            //if (action == "Approved")
            //{
            cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

            //    if (cRate.GetRateDailyLastError != "")
            //        error = cRate.GetRateDailyLastError;
            //    if (cRate.GetRateMonthlyLastError != "")
            //        error = cRate.GetRateMonthlyLastError;
            //}            
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {

                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnpogenmst SET pogenno='" + transno + "', pogenmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", pogenratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', pogenratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', pogenrate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', pogenrate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' "+ sqlPlus +" WHERE pogenmststatus='In Approval' AND pogenmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpogenmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_prgendtl SET prgendtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND prgendtloid IN (SELECT prgendtloid FROM QL_trnpogendtl WHERE cmpcode='" + CompnyCode + "' AND pogenmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prgenmst SET prgenmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND prgenmstoid IN (SELECT DISTINCT prgenmstoid FROM QL_trnpogendtl WHERE cmpcode='" + CompnyCode + "' AND pogenmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                    }

                    if (action == "Approved")
                    {
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var matoid = tbldtl[i].matgenoid;
                            QL_mstmatgenprice tblMatPrice = new QL_RSAEntities().QL_mstmatgenprice.Where(x => x.cmpcode == CompnyCode && x.matgenoid == matoid && x.refoid == suppoid && x.refname == "SUPPLIER" && x.curroid == curroid).FirstOrDefault();

                            if (tblMatPrice != null)
                            {
                                tblMatPrice.totalpurchaseqty = (tblMatPrice.totalpurchaseqty + tbldtl[i].pogenqty);
                                tblMatPrice.avgpurchaseprice = ((tblMatPrice.avgpurchaseprice * tblMatPrice.totalpurchaseqty) + tbldtl[i].pogendtlamt) / (tblMatPrice.totalpurchaseqty + tbldtl[i].pogenqty);
                                tblMatPrice.lastpurchaseprice = tbldtl[i].pogenprice;
                                tblMatPrice.lasttrans = "QL_trnpogenmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                db.Entry(tblMatPrice).State = EntityState.Modified;
                                //db.SaveChanges();
                            }
                            else
                            {
                                tblMatPrice = new QL_mstmatgenprice();
                                tblMatPrice.cmpcode = cmp;
                                tblMatPrice.matgenpriceoid = iPriceOid;
                                tblMatPrice.matgenoid = tbldtl[i].matgenoid;
                                tblMatPrice.refoid = suppoid;
                                tblMatPrice.refname = "SUPPLIER";
                                tblMatPrice.totalpurchaseqty = tbldtl[i].pogenqty;
                                tblMatPrice.totalsalesqty = 0;
                                tblMatPrice.avgpurchaseprice = tbldtl[i].pogenprice;
                                tblMatPrice.lastpurchaseprice = tbldtl[i].pogenprice;
                                tblMatPrice.avgsalesprice = 0;
                                tblMatPrice.lastsalesprice = 0;
                                tblMatPrice.lasttrans = "QL_trnpogenmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.note = "";
                                tblMatPrice.res1 = "";
                                tblMatPrice.res2 = "";
                                tblMatPrice.res3 = "";
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.curroid = curroid;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                db.QL_mstmatgenprice.Add(tblMatPrice);
                                //db.SaveChanges();
                            }

                            iPriceOid++;
                        }
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTMATGENPRICE'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                    }

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPOSP(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var iPriceOid = ClassFunction.GenerateID("QL_MSTSPAREPARTPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnpospmst WHERE cmpcode='" + CompnyCode + "' AND pospmstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnpospmst WHERE cmpcode='" + CompnyCode + "' AND pospmstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT pospdate FROM QL_trnpospmst WHERE cmpcode='" + CompnyCode + "' AND pospmstoid=" + id).FirstOrDefault();
            List<QL_trnpospdtl> tbldtl = db.QL_trnpospdtl.Where(x => x.cmpcode == CompnyCode && x.pospmstoid == id).ToList();
            //if (action == "Approved")
            //{
            cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

            //    if (cRate.GetRateDailyLastError != "")
            //        error = cRate.GetRateDailyLastError;
            //    if (cRate.GetRateMonthlyLastError != "")
            //        error = cRate.GetRateMonthlyLastError;
            //}            
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnpospmst SET pospno='" + transno + "', pospmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", pospratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', pospratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', posprate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', posprate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' "+ sqlPlus +" WHERE pospmststatus='In Approval' AND pospmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpospmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_prspdtl SET prspdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND prspdtloid IN (SELECT prspdtloid FROM QL_trnpospdtl WHERE cmpcode='" + CompnyCode + "' AND pospmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prspmst SET prspmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND prspmstoid IN (SELECT DISTINCT prspmstoid FROM QL_trnpospdtl WHERE cmpcode='" + CompnyCode + "' AND pospmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                    }

                    if (action == "Approved")
                    {
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var matoid = tbldtl[i].sparepartoid;
                            QL_mstsparepartprice tblMatPrice = new QL_RSAEntities().QL_mstsparepartprice.Where(x => x.cmpcode == CompnyCode && x.sparepartoid == matoid && x.refoid == suppoid && x.refname == "SUPPLIER" && x.curroid == curroid).FirstOrDefault();

                            if (tblMatPrice != null)
                            {
                                tblMatPrice.totalpurchaseqty = (tblMatPrice.totalpurchaseqty + tbldtl[i].pospqty);
                                tblMatPrice.avgpurchaseprice = ((tblMatPrice.avgpurchaseprice * tblMatPrice.totalpurchaseqty) + tbldtl[i].pospdtlamt) / (tblMatPrice.totalpurchaseqty + tbldtl[i].pospqty);
                                tblMatPrice.lastpurchaseprice = tbldtl[i].pospprice;
                                tblMatPrice.lasttrans = "QL_trnpospmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                db.Entry(tblMatPrice).State = EntityState.Modified;
                                //db.SaveChanges();
                            }
                            else
                            {
                                tblMatPrice = new QL_mstsparepartprice();
                                tblMatPrice.cmpcode = cmp;
                                tblMatPrice.sparepartpriceoid = iPriceOid;
                                tblMatPrice.sparepartoid = tbldtl[i].sparepartoid;
                                tblMatPrice.refoid = suppoid;
                                tblMatPrice.refname = "SUPPLIER";
                                tblMatPrice.totalpurchaseqty = tbldtl[i].pospqty;
                                tblMatPrice.totalsalesqty = 0;
                                tblMatPrice.avgpurchaseprice = tbldtl[i].pospprice;
                                tblMatPrice.lastpurchaseprice = tbldtl[i].pospprice;
                                tblMatPrice.avgsalesprice = 0;
                                tblMatPrice.lastsalesprice = 0;
                                tblMatPrice.lasttrans = "QL_trnpospmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.note = "";
                                tblMatPrice.res1 = "";
                                tblMatPrice.res2 = "";
                                tblMatPrice.res3 = "";
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.curroid = curroid;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                db.QL_mstsparepartprice.Add(tblMatPrice);
                                //db.SaveChanges();
                            }

                            iPriceOid++;
                        }
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTSPAREPARTPRICE'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                    }

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPOFG(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var iPriceOid = ClassFunction.GenerateID("QL_MSTITEMPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnpoitemmst WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnpoitemmst WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT poitemdate FROM QL_trnpoitemmst WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id).FirstOrDefault();
            List<QL_trnpoitemdtl> tbldtl = db.QL_trnpoitemdtl.Where(x => x.cmpcode == CompnyCode && x.poitemmstoid == id).ToList();
            //if (action == "Approved")
            //{
            cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

            //    if (cRate.GetRateDailyLastError != "")
            //        error = cRate.GetRateDailyLastError;
            //    if (cRate.GetRateMonthlyLastError != "")
            //        error = cRate.GetRateMonthlyLastError;
            //}            
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnpoitemmst SET poitemno='" + transno + "', poitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", poitemratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', poitemratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', poitemrate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', poitemrate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' "+ sqlPlus +" WHERE poitemmststatus='In Approval' AND poitemmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpoitemmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_pritemdtl SET pritemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND pritemdtloid IN (SELECT pritemdtloid FROM QL_trnpoitemdtl WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_pritemmst SET pritemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND pritemmstoid IN (SELECT DISTINCT pritemmstoid FROM QL_trnpoitemdtl WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                    }

                    if (action == "Approved")
                    {
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var matoid = tbldtl[i].itemoid;
                            QL_mstitemprice tblMatPrice = new QL_RSAEntities().QL_mstitemprice.Where(x => x.cmpcode == CompnyCode && x.itemoid == matoid && x.refoid == suppoid && x.refname == "SUPPLIER" && x.curroid == curroid).FirstOrDefault();

                            if (tblMatPrice != null)
                            {
                                tblMatPrice.totalpurchaseqty = (tblMatPrice.totalpurchaseqty + tbldtl[i].poitemqty);
                                tblMatPrice.avgpurchaseprice = ((tblMatPrice.avgpurchaseprice * tblMatPrice.totalpurchaseqty) + tbldtl[i].poitemdtlamt) / (tblMatPrice.totalpurchaseqty + tbldtl[i].poitemqty);
                                tblMatPrice.lastpurchaseprice = tbldtl[i].poitemprice;
                                tblMatPrice.lasttrans = "QL_trnpoitemmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                db.Entry(tblMatPrice).State = EntityState.Modified;
                                //db.SaveChanges();
                            }
                            else
                            {
                                tblMatPrice = new QL_mstitemprice();
                                tblMatPrice.cmpcode = cmp;
                                tblMatPrice.itempriceoid = iPriceOid;
                                tblMatPrice.itemoid = tbldtl[i].itemoid;
                                tblMatPrice.refoid = suppoid;
                                tblMatPrice.refname = "SUPPLIER";
                                tblMatPrice.totalpurchaseqty = tbldtl[i].poitemqty;
                                tblMatPrice.totalsalesqty = 0;
                                tblMatPrice.avgpurchaseprice = tbldtl[i].poitemprice;
                                tblMatPrice.lastpurchaseprice = tbldtl[i].poitemprice;
                                tblMatPrice.avgsalesprice = 0;
                                tblMatPrice.lastsalesprice = 0;
                                tblMatPrice.lasttrans = "QL_trnpoitemmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.note = "";
                                tblMatPrice.res1 = "";
                                tblMatPrice.res2 = "";
                                tblMatPrice.res3 = "";
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.curroid = curroid;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                db.QL_mstitemprice.Add(tblMatPrice);
                                //db.SaveChanges();
                            }

                            iPriceOid++;
                        }
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTITEMPRICE'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                    }

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPOSVC(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            //var iPriceOid = ClassFunction.GenerateID("QL_MSTSPAREPARTPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnposervicemst WHERE cmpcode='" + CompnyCode + "' AND poservicemstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnposervicemst WHERE cmpcode='" + CompnyCode + "' AND poservicemstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT poservicedate FROM QL_trnposervicemst WHERE cmpcode='" + CompnyCode + "' AND poservicemstoid=" + id).FirstOrDefault();
            List<QL_trnposervicedtl> tbldtl = db.QL_trnposervicedtl.Where(x => x.cmpcode == CompnyCode && x.poservicemstoid == id).ToList();
            if (action == "Approved")
            {
                cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;
            }
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnposervicemst SET poserviceno='" + transno + "', poservicemststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", poserviceratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', poserviceratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', poservicerate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', poservicerate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' " + sqlPlus + " WHERE poservicemststatus='In Approval' AND poservicemstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnposervicemst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalPOAS(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var iPriceOid = ClassFunction.GenerateID("QL_MSTMATGENPRICE");
            error = "";
            var cRate = new ClassRate();
            var curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id).FirstOrDefault();
            var suppoid = db.Database.SqlQuery<int>("SELECT suppoid FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id).FirstOrDefault();
            var podate = db.Database.SqlQuery<DateTime>("SELECT poassetdate FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id).FirstOrDefault();
            List<QL_trnpoassetdtl> tbldtl = db.QL_trnpoassetdtl.Where(x => x.cmpcode == CompnyCode && x.poassetmstoid == id).ToList();
            //if (action == "Approved")
            //{
            cRate.SetRateValue(curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));

            //    if (cRate.GetRateDailyLastError != "")
            //        error = cRate.GetRateDailyLastError;
            //    if (cRate.GetRateMonthlyLastError != "")
            //        error = cRate.GetRateMonthlyLastError;
            //}            
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {

                    if (action == "Revised")
                    {
                        sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                    }
                    else if (action == "Rejected")
                    {
                        sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                    }

                    if (action != "Approved")
                        transno = "";

                    sSql = "UPDATE QL_trnpoassetmst SET poassetno='" + transno + "', poassetmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", poassetratetoidrchar='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', poassetratetousdchar='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', poassetrate2toidrchar='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', poassetrate2tousdchar='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "' " + sqlPlus + " WHERE poassetmststatus='In Approval' AND poassetmstoid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpoassetmst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);
                    db.SaveChanges();

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_prgendtl SET prgendtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND prgendtloid IN (SELECT prgendtloid FROM QL_trnpoassetdtl WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prgenmst SET prgenmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND prgenmstoid IN (SELECT DISTINCT prgenmstoid FROM QL_trnpoassetdtl WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                    }

                    if (action == "Approved")
                    {
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var matoid = tbldtl[i].poassetrefoid;
                            QL_mstmatgenprice tblMatPrice = new QL_RSAEntities().QL_mstmatgenprice.Where(x => x.cmpcode == CompnyCode && x.matgenoid == matoid && x.refoid == suppoid && x.refname == "SUPPLIER" && x.curroid == curroid).FirstOrDefault();

                            if (tblMatPrice != null)
                            {
                                tblMatPrice.totalpurchaseqty = (tblMatPrice.totalpurchaseqty + tbldtl[i].poassetqty);
                                tblMatPrice.avgpurchaseprice = ((tblMatPrice.avgpurchaseprice * tblMatPrice.totalpurchaseqty) + tbldtl[i].poassetdtlamt) / (tblMatPrice.totalpurchaseqty + tbldtl[i].poassetqty);
                                tblMatPrice.lastpurchaseprice = tbldtl[i].poassetprice;
                                tblMatPrice.lasttrans = "QL_trnpoassetmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                db.Entry(tblMatPrice).State = EntityState.Modified;
                                //db.SaveChanges();
                            }
                            else
                            {
                                tblMatPrice = new QL_mstmatgenprice();
                                tblMatPrice.cmpcode = cmp;
                                tblMatPrice.matgenpriceoid = iPriceOid;
                                tblMatPrice.matgenoid = tbldtl[i].poassetrefoid;
                                tblMatPrice.refoid = suppoid;
                                tblMatPrice.refname = "SUPPLIER";
                                tblMatPrice.totalpurchaseqty = tbldtl[i].poassetqty;
                                tblMatPrice.totalsalesqty = 0;
                                tblMatPrice.avgpurchaseprice = tbldtl[i].poassetprice;
                                tblMatPrice.lastpurchaseprice = tbldtl[i].poassetprice;
                                tblMatPrice.avgsalesprice = 0;
                                tblMatPrice.lastsalesprice = 0;
                                tblMatPrice.lasttrans = "QL_trnpoassetmst";
                                tblMatPrice.lasttransoid = id;
                                tblMatPrice.lasttransno = transno;
                                tblMatPrice.lasttransdate = podate;
                                tblMatPrice.note = "";
                                tblMatPrice.res1 = "";
                                tblMatPrice.res2 = "";
                                tblMatPrice.res3 = "";
                                tblMatPrice.upduser = Session["UserID"].ToString();
                                tblMatPrice.updtime = servertime;
                                tblMatPrice.curroid = curroid;
                                tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                db.QL_mstmatgenprice.Add(tblMatPrice);
                                //db.SaveChanges();
                            }

                            iPriceOid++;
                        }
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTMATGENPRICE'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                    }

                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }
        #endregion

        //#region Approval Purchase Return
        //private void ApprovalPretRM(int id, string cmp, string transno, string action, out string error)
        //{
        //    error = "";
        //    // Interface Validation
        //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_STOCK_RM");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_SELISIH_EFISIENSI", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_SELISIH_EFISIENSI");

        //    sSql = "SELECT pretd.cmpcode, pretrawdtloid, pretrawmstoid, pretrawdtlseq, pretd.mrrawmstoid, pretd.mrrawdtloid, pretrawwhoid, pretd.matrawoid, pretrawqty, pretrawunitoid, pretrawdtlstatus, pretrawdtlnote, pretrawdtlres1, pretrawdtlres2, pretrawdtlres3, pretd.upduser, pretd.updtime, mrrawvalue pretrawvalue, mrrawvalueidr pretrawvalueidr, mrrawvalueusd pretrawvalueusd, reasonoid, pretrawvalueidr_stock, pretrawvalueusd_stock FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrrawmstoid=pretd.mrrawmstoid AND mrd.mrrawdtloid=pretd.mrrawdtloid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretrawmstoid=" + id + " ORDER BY pretrawdtlseq";
        //    List<QL_trnpretrawdtl> tbldtl = db.Database.SqlQuery<QL_trnpretrawdtl>(sSql).ToList();

        //    // Available Stock Validation & Stock Value Assignment
        //    var servertime = ClassFunction.GetServerTime();
        //    var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
        //    for (int i = 0; i < tbldtl.Count; i++)
        //    {
        //        var totalqty = tbldtl.Where(x => x.matrawoid == tbldtl[i].matrawoid && x.pretrawwhoid == tbldtl[i].pretrawwhoid).Sum(x => x.pretrawqty);
        //        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].matrawoid, tbldtl[i].pretrawwhoid, totalqty, "RAW MATERIAL"))
        //        {
        //            error = "Every Return Qty must be less than Stock Qty!";
        //            break;
        //        }
        //        else
        //        {
        //            tbldtl[i].pretrawvalueidr_stock = ClassFunction.GetStockValueIDR(cmp, sPeriod, "RAW MATERIAL", tbldtl[i].matrawoid);
        //            tbldtl[i].pretrawvalueusd_stock = ClassFunction.GetStockValueUSD(cmp, sPeriod, "RAW MATERIAL", tbldtl[i].matrawoid);
        //        }
        //    }

        //    if (error == "")
        //    {
        //        var conmtroid = ClassFunction.GenerateID("QL_conmat");
        //        var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
        //        var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
        //        var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
        //        var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
        //        var iVAR_STOCK_RM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", cmp));
        //        var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp));
        //        var iVAR_SELISIH_EFISIENSI = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_SELISIH_EFISIENSI", cmp));

        //        using (var objTrans = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                sSql = "UPDATE QL_trnpretrawmst SET pretrawno='" + transno + "', pretrawmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND pretrawmststatus='In Approval' AND pretrawmstoid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpretrawmst' AND oid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                if (action == "Rejected")
        //                {
        //                    sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnpretrawdtl WHERE cmpcode='" + CompnyCode + "' AND pretrawmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();

        //                    sSql = "UPDATE QL_trnmrrawmst SET mrrawmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnpretrawdtl WHERE cmpcode='" + CompnyCode + "' AND pretrawmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }
        //                else if (action == "Approved")
        //                {
        //                    QL_trnpretrawdtl tbltmp;
        //                    for (int i = 0; i < tbldtl.Count; i++)
        //                    {
        //                        // Update Stock Value in Detail Table
        //                        tbltmp = tbldtl[i];
        //                        tbltmp.pretrawvalue = tbldtl[i].pretrawvalue;
        //                        tbltmp.pretrawvalueidr = tbldtl[i].pretrawvalueidr;
        //                        tbltmp.pretrawvalueusd = tbldtl[i].pretrawvalueusd;
        //                        tbltmp.pretrawvalueidr_stock = tbldtl[i].pretrawvalueidr_stock;
        //                        tbltmp.pretrawvalueusd_stock = tbldtl[i].pretrawvalueusd_stock;
        //                        db.Entry(tbltmp).State = EntityState.Modified;
        //                        db.SaveChanges();

        //                        // Insert QL_conmat
        //                        db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, conmtroid++, "PRETRM", "QL_trnpretrawdtl", tbldtl[i].pretrawmstoid, tbldtl[i].matrawoid, "RAW MATERIAL", tbldtl[i].pretrawwhoid, tbldtl[i].pretrawqty * -1, "Purchase Return Raw Material", transno, Session["UserID"].ToString(), null, tbldtl[i].pretrawvalueidr_stock, tbldtl[i].pretrawvalueusd_stock, 0, null, tbldtl[i].pretrawdtloid));
        //                        db.SaveChanges();

        //                        // Update/Insert QL_crdmtr
        //                        var flagcrd = "";
        //                        QL_crdmtr crdmtr = ClassFunction.UpdateOrInsertCrdMtr(cmp, crdmatoid, tbldtl[i].matrawoid, "RAW MATERIAL", tbldtl[i].pretrawwhoid, tbldtl[i].pretrawqty * -1, 0, "QL_trnpretrawdtl", Session["UserID"].ToString(), null, tbldtl[i].pretrawvalueidr_stock * tbldtl[i].pretrawqty * -1, tbldtl[i].pretrawvalueusd_stock * tbldtl[i].pretrawqty * -1, out flagcrd);
        //                        if (flagcrd == "Update")
        //                            db.Entry(crdmtr).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_crdmtr.Add(crdmtr);
        //                            crdmatoid++;
        //                        }
        //                        db.SaveChanges();

        //                        // Update/Insert QL_stockvalue
        //                        var flagstval = "";
        //                        QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, stockvalueoid, tbldtl[i].matrawoid, "RAW MATERIAL", tbldtl[i].pretrawqty * -1, tbldtl[i].pretrawvalueidr_stock, tbldtl[i].pretrawvalueusd_stock, "QL_trnpretrawdtl", null, Session["UserID"].ToString(), out flagstval);
        //                        if (flagstval == "Update")
        //                            db.Entry(stockvalue).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_stockvalue.Add(stockvalue);
        //                            stockvalueoid++;
        //                        }
        //                        db.SaveChanges();
        //                    }

        //                    // Insert QL_trnglmst
        //                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "PRet Raw|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 0, 0, 0, 0));
        //                    db.SaveChanges();

        //                    decimal glamt = tbldtl.Sum(x => x.pretrawqty * x.pretrawvalue);
        //                    decimal glamtidr = tbldtl.Sum(x => x.pretrawqty * x.pretrawvalueidr);
        //                    decimal glamtusd = tbldtl.Sum(x => x.pretrawqty * x.pretrawvalueusd);
        //                    decimal glamtidr_st = tbldtl.Sum(x => x.pretrawqty * x.pretrawvalueidr_stock);
        //                    decimal glamtusd_st = tbldtl.Sum(x => x.pretrawqty * x.pretrawvalueusd_stock);
        //                    var glseq = 1;

        //                    // Insert QL_trngldtl
        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", glamt, transno, "PRet Raw|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnpretrawmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();

        //                    var glamtidr_def = glamtidr - glamtidr_st;
        //                    if (glamtidr_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtidr_def > 0? "C": "D", 0, transno, "PRet Raw|No. " + transno, "Post", Session["UserID"].ToString(), servertime, Math.Abs(glamtidr_def), 0, "QL_trnpretrawmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    var glamtusd_def = glamtusd - glamtusd_st;
        //                    if (glamtusd_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtusd_def > 0 ? "C" : "D", 0, transno, "PRet Raw|No. " + transno, "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(glamtusd_def), "QL_trnpretrawmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_STOCK_RM, "C", glamt, transno, "PRet Raw|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr_st, glamtusd_st, "QL_trnpretrawmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();
        //                    glmstoid++;

        //                    // Update Last ID
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (stockvalueoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }

        //                objTrans.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                objTrans.Rollback();
        //                error = ex.ToString();
        //            }
        //        }
        //    }
        //}

        //private void ApprovalPretGM(int id, string cmp, string transno, string action, out string error)
        //{
        //    error = "";
        //    // Interface Validation
        //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_GM", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_STOCK_GM");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_SELISIH_EFISIENSI", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_SELISIH_EFISIENSI");

        //    sSql = "SELECT pretd.cmpcode, pretgendtloid, pretgenmstoid, pretgendtlseq, pretd.mrgenmstoid, pretd.mrgendtloid, pretgenwhoid, pretd.matgenoid, pretgenqty, pretgenunitoid, pretgendtlstatus, pretgendtlnote, pretgendtlres1, pretgendtlres2, pretgendtlres3, pretd.upduser, pretd.updtime, mrgenvalue pretgenvalue, mrgenvalueidr pretgenvalueidr, mrgenvalueusd pretgenvalueusd, reasonoid, pretgenvalueidr_stock, pretgenvalueusd_stock FROM QL_trnpretgendtl pretd INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrgenmstoid=pretd.mrgenmstoid AND mrd.mrgendtloid=pretd.mrgendtloid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretgenmstoid=" + id + " ORDER BY pretgendtlseq";
        //    List<QL_trnpretgendtl> tbldtl = db.Database.SqlQuery<QL_trnpretgendtl>(sSql).ToList();

        //    // Available Stock Validation & Stock Value Assignment
        //    var servertime = ClassFunction.GetServerTime();
        //    var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
        //    for (int i = 0; i < tbldtl.Count; i++)
        //    {
        //        var totalqty = tbldtl.Where(x => x.matgenoid == tbldtl[i].matgenoid && x.pretgenwhoid == tbldtl[i].pretgenwhoid).Sum(x => x.pretgenqty);
        //        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].matgenoid, tbldtl[i].pretgenwhoid, totalqty, "GENERAL MATERIAL"))
        //        {
        //            error = "Every Return Qty must be less than Stock Qty!";
        //            break;
        //        }
        //        else
        //        {
        //            tbldtl[i].pretgenvalueidr_stock = ClassFunction.GetStockValueIDR(cmp, sPeriod, "GENERAL MATERIAL", tbldtl[i].matgenoid);
        //            tbldtl[i].pretgenvalueusd_stock = ClassFunction.GetStockValueUSD(cmp, sPeriod, "GENERAL MATERIAL", tbldtl[i].matgenoid);
        //        }
        //    }

        //    if (error == "")
        //    {
        //        var conmtroid = ClassFunction.GenerateID("QL_conmat");
        //        var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
        //        var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
        //        var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
        //        var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
        //        var iVAR_STOCK_GM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_GM", cmp));
        //        var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp));
        //        var iVAR_SELISIH_EFISIENSI = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_SELISIH_EFISIENSI", cmp));

        //        using (var objTrans = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                sSql = "UPDATE QL_trnpretgenmst SET pretgenno='" + transno + "', pretgenmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND pretgenmststatus='In Approval' AND pretgenmstoid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpretgenmst' AND oid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                if (action == "Rejected")
        //                {
        //                    sSql = "UPDATE QL_trnmrgendtl SET mrgendtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnpretgendtl WHERE cmpcode='" + CompnyCode + "' AND pretgenmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();

        //                    sSql = "UPDATE QL_trnmrgenmst SET mrgenmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnpretgendtl WHERE cmpcode='" + CompnyCode + "' AND pretgenmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }
        //                else if (action == "Approved")
        //                {
        //                    QL_trnpretgendtl tbltmp;
        //                    for (int i = 0; i < tbldtl.Count; i++)
        //                    {
        //                        // Update Stock Value in Detail Table
        //                        tbltmp = tbldtl[i];
        //                        tbltmp.pretgenvalue = tbldtl[i].pretgenvalue;
        //                        tbltmp.pretgenvalueidr = tbldtl[i].pretgenvalueidr;
        //                        tbltmp.pretgenvalueusd = tbldtl[i].pretgenvalueusd;
        //                        tbltmp.pretgenvalueidr_stock = tbldtl[i].pretgenvalueidr_stock;
        //                        tbltmp.pretgenvalueusd_stock = tbldtl[i].pretgenvalueusd_stock;
        //                        db.Entry(tbltmp).State = EntityState.Modified;
        //                        db.SaveChanges();

        //                        // Insert QL_conmat
        //                        db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, conmtroid++, "PRETGM", "QL_trnpretgendtl", tbldtl[i].pretgenmstoid, tbldtl[i].matgenoid, "GENERAL MATERIAL", tbldtl[i].pretgenwhoid, tbldtl[i].pretgenqty * -1, "Purchase Return General Material", transno, Session["UserID"].ToString(), null, tbldtl[i].pretgenvalueidr_stock, tbldtl[i].pretgenvalueusd_stock, 0, null, tbldtl[i].pretgendtloid));
        //                        db.SaveChanges();

        //                        // Update/Insert QL_crdmtr
        //                        var flagcrd = "";
        //                        QL_crdmtr crdmtr = ClassFunction.UpdateOrInsertCrdMtr(cmp, crdmatoid, tbldtl[i].matgenoid, "GENERAL MATERIAL", tbldtl[i].pretgenwhoid, tbldtl[i].pretgenqty * -1, 0, "QL_trnpretgendtl", Session["UserID"].ToString(), null, tbldtl[i].pretgenvalueidr_stock * tbldtl[i].pretgenqty * -1, tbldtl[i].pretgenvalueusd_stock * tbldtl[i].pretgenqty * -1, out flagcrd);
        //                        if (flagcrd == "Update")
        //                            db.Entry(crdmtr).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_crdmtr.Add(crdmtr);
        //                            crdmatoid++;
        //                        }
        //                        db.SaveChanges();

        //                        // Update/Insert QL_stockvalue
        //                        var flagstval = "";
        //                        QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, stockvalueoid, tbldtl[i].matgenoid, "GENERAL MATERIAL", tbldtl[i].pretgenqty * -1, tbldtl[i].pretgenvalueidr_stock, tbldtl[i].pretgenvalueusd_stock, "QL_trnpretgendtl", null, Session["UserID"].ToString(), out flagstval);
        //                        if (flagstval == "Update")
        //                            db.Entry(stockvalue).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_stockvalue.Add(stockvalue);
        //                            stockvalueoid++;
        //                        }
        //                        db.SaveChanges();
        //                    }

        //                    // Insert QL_trnglmst
        //                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "PRet Gen|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 0, 0, 0, 0));
        //                    db.SaveChanges();

        //                    decimal glamt = tbldtl.Sum(x => x.pretgenqty * x.pretgenvalue);
        //                    decimal glamtidr = tbldtl.Sum(x => x.pretgenqty * x.pretgenvalueidr);
        //                    decimal glamtusd = tbldtl.Sum(x => x.pretgenqty * x.pretgenvalueusd);
        //                    decimal glamtidr_st = tbldtl.Sum(x => x.pretgenqty * x.pretgenvalueidr_stock);
        //                    decimal glamtusd_st = tbldtl.Sum(x => x.pretgenqty * x.pretgenvalueusd_stock);
        //                    var glseq = 1;

        //                    // Insert QL_trngldtl
        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", glamt, transno, "PRet Gen|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnpretgenmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();

        //                    var glamtidr_def = glamtidr - glamtidr_st;
        //                    if (glamtidr_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtidr_def > 0 ? "C" : "D", 0, transno, "PRet Gen|No. " + transno, "Post", Session["UserID"].ToString(), servertime, Math.Abs(glamtidr_def), 0, "QL_trnpretgenmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    var glamtusd_def = glamtusd - glamtusd_st;
        //                    if (glamtusd_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtusd_def > 0 ? "C" : "D", 0, transno, "PRet Gen|No. " + transno, "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(glamtusd_def), "QL_trnpretgenmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_STOCK_GM, "C", glamt, transno, "PRet Gen|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr_st, glamtusd_st, "QL_trnpretgenmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();
        //                    glmstoid++;

        //                    // Update Last ID
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (stockvalueoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }

        //                objTrans.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                objTrans.Rollback();
        //                error = ex.ToString();
        //            }
        //        }
        //    }
        //}

        //private void ApprovalPretSP(int id, string cmp, string transno, string action, out string error)
        //{
        //    error = "";
        //    // Interface Validation
        //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_SP", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_STOCK_SP");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_SELISIH_EFISIENSI", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_SELISIH_EFISIENSI");

        //    sSql = "SELECT pretd.cmpcode, pretspdtloid, pretspmstoid, pretspdtlseq, pretd.mrspmstoid, pretd.mrspdtloid, pretspwhoid, pretd.sparepartoid, pretspqty, pretspunitoid, pretspdtlstatus, pretspdtlnote, pretspdtlres1, pretspdtlres2, pretspdtlres3, pretd.upduser, pretd.updtime, mrspvalue pretspvalue, mrspvalueidr pretspvalueidr, mrspvalueusd pretspvalueusd, reasonoid, pretspvalueidr_stock, pretspvalueusd_stock FROM QL_trnpretspdtl pretd INNER JOIN QL_trnmrspdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrspmstoid=pretd.mrspmstoid AND mrd.mrspdtloid=pretd.mrspdtloid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretspmstoid=" + id + " ORDER BY pretspdtlseq";
        //    List<QL_trnpretspdtl> tbldtl = db.Database.SqlQuery<QL_trnpretspdtl>(sSql).ToList();

        //    // Available Stock Validation & Stock Value Assignment
        //    var servertime = ClassFunction.GetServerTime();
        //    var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
        //    for (int i = 0; i < tbldtl.Count; i++)
        //    {
        //        var totalqty = tbldtl.Where(x => x.sparepartoid == tbldtl[i].sparepartoid && x.pretspwhoid == tbldtl[i].pretspwhoid).Sum(x => x.pretspqty);
        //        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].sparepartoid, tbldtl[i].pretspwhoid, totalqty, "SPARE PART"))
        //        {
        //            error = "Every Return Qty must be less than Stock Qty!";
        //            break;
        //        }
        //        else
        //        {
        //            tbldtl[i].pretspvalueidr_stock = ClassFunction.GetStockValueIDR(cmp, sPeriod, "SPARE PART", tbldtl[i].sparepartoid);
        //            tbldtl[i].pretspvalueusd_stock = ClassFunction.GetStockValueUSD(cmp, sPeriod, "SPARE PART", tbldtl[i].sparepartoid);
        //        }
        //    }

        //    if (error == "")
        //    {
        //        var conmtroid = ClassFunction.GenerateID("QL_conmat");
        //        var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
        //        var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
        //        var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
        //        var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
        //        var iVAR_STOCK_SP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_SP", cmp));
        //        var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp));
        //        var iVAR_SELISIH_EFISIENSI = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_SELISIH_EFISIENSI", cmp));

        //        using (var objTrans = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                sSql = "UPDATE QL_trnpretspmst SET pretspno='" + transno + "', pretspmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND pretspmststatus='In Approval' AND pretspmstoid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpretspmst' AND oid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                if (action == "Rejected")
        //                {
        //                    sSql = "UPDATE QL_trnmrspdtl SET mrspdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mrspdtloid IN (SELECT mrspdtloid FROM QL_trnpretspdtl WHERE cmpcode='" + CompnyCode + "' AND pretspmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();

        //                    sSql = "UPDATE QL_trnmrspmst SET mrspmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mrspmstoid IN (SELECT mrspmstoid FROM QL_trnpretspdtl WHERE cmpcode='" + CompnyCode + "' AND pretspmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }
        //                else if (action == "Approved")
        //                {
        //                    QL_trnpretspdtl tbltmp;
        //                    for (int i = 0; i < tbldtl.Count; i++)
        //                    {
        //                        // Update Stock Value in Detail Table
        //                        tbltmp = tbldtl[i];
        //                        tbltmp.pretspvalue = tbldtl[i].pretspvalue;
        //                        tbltmp.pretspvalueidr = tbldtl[i].pretspvalueidr;
        //                        tbltmp.pretspvalueusd = tbldtl[i].pretspvalueusd;
        //                        tbltmp.pretspvalueidr_stock = tbldtl[i].pretspvalueidr_stock;
        //                        tbltmp.pretspvalueusd_stock = tbldtl[i].pretspvalueusd_stock;
        //                        db.Entry(tbltmp).State = EntityState.Modified;
        //                        db.SaveChanges();

        //                        // Insert QL_conmat
        //                        db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, conmtroid++, "PRETSP", "QL_trnpretspdtl", tbldtl[i].pretspmstoid, tbldtl[i].sparepartoid, "SPARE PART", tbldtl[i].pretspwhoid, tbldtl[i].pretspqty * -1, "Purchase Return Spare Part", transno, Session["UserID"].ToString(), null, tbldtl[i].pretspvalueidr_stock, tbldtl[i].pretspvalueusd_stock, 0, null, tbldtl[i].pretspdtloid));
        //                        db.SaveChanges();

        //                        // Update/Insert QL_crdmtr
        //                        var flagcrd = "";
        //                        QL_crdmtr crdmtr = ClassFunction.UpdateOrInsertCrdMtr(cmp, crdmatoid, tbldtl[i].sparepartoid, "SPARE PART", tbldtl[i].pretspwhoid, tbldtl[i].pretspqty * -1, 0, "QL_trnpretspdtl", Session["UserID"].ToString(), null, tbldtl[i].pretspvalueidr_stock * tbldtl[i].pretspqty * -1, tbldtl[i].pretspvalueusd_stock * tbldtl[i].pretspqty * -1, out flagcrd);
        //                        if (flagcrd == "Update")
        //                            db.Entry(crdmtr).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_crdmtr.Add(crdmtr);
        //                            crdmatoid++;
        //                        }
        //                        db.SaveChanges();

        //                        // Update/Insert QL_stockvalue
        //                        var flagstval = "";
        //                        QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, stockvalueoid, tbldtl[i].sparepartoid, "SPARE PART", tbldtl[i].pretspqty * -1, tbldtl[i].pretspvalueidr_stock, tbldtl[i].pretspvalueusd_stock, "QL_trnpretspdtl", null, Session["UserID"].ToString(), out flagstval);
        //                        if (flagstval == "Update")
        //                            db.Entry(stockvalue).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_stockvalue.Add(stockvalue);
        //                            stockvalueoid++;
        //                        }
        //                        db.SaveChanges();
        //                    }

        //                    // Insert QL_trnglmst
        //                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "PRet SP|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 0, 0, 0, 0));
        //                    db.SaveChanges();

        //                    decimal glamt = tbldtl.Sum(x => x.pretspqty * x.pretspvalue);
        //                    decimal glamtidr = tbldtl.Sum(x => x.pretspqty * x.pretspvalueidr);
        //                    decimal glamtusd = tbldtl.Sum(x => x.pretspqty * x.pretspvalueusd);
        //                    decimal glamtidr_st = tbldtl.Sum(x => x.pretspqty * x.pretspvalueidr_stock);
        //                    decimal glamtusd_st = tbldtl.Sum(x => x.pretspqty * x.pretspvalueusd_stock);
        //                    var glseq = 1;

        //                    // Insert QL_trngldtl
        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", glamt, transno, "PRet SP|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnpretspmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();

        //                    var glamtidr_def = glamtidr - glamtidr_st;
        //                    if (glamtidr_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtidr_def > 0 ? "C" : "D", 0, transno, "PRet SP|No. " + transno, "Post", Session["UserID"].ToString(), servertime, Math.Abs(glamtidr_def), 0, "QL_trnpretspmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    var glamtusd_def = glamtusd - glamtusd_st;
        //                    if (glamtusd_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtusd_def > 0 ? "C" : "D", 0, transno, "PRet SP|No. " + transno, "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(glamtusd_def), "QL_trnpretspmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_STOCK_SP, "C", glamt, transno, "PRet SP|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr_st, glamtusd_st, "QL_trnpretspmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();
        //                    glmstoid++;

        //                    // Update Last ID
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (stockvalueoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }

        //                objTrans.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                objTrans.Rollback();
        //                error = ex.ToString();
        //            }
        //        }
        //    }
        //}

        //private void ApprovalPretFG(int id, string cmp, string transno, string action, out string error)
        //{
        //    error = "";
        //    // Interface Validation
        //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_STOCK_FG");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
        //    if (error == "" && !ClassFunction.IsInterfaceExists("VAR_SELISIH_EFISIENSI", cmp))
        //        error = ClassFunction.GetInterfaceWarning("VAR_SELISIH_EFISIENSI");

        //    sSql = "SELECT pretd.cmpcode, pretitemdtloid, pretitemmstoid, pretitemdtlseq, pretd.mritemmstoid, pretd.mritemdtloid, pretitemwhoid, pretd.itemoid, pretitemqty, pretitemunitoid, pretitemdtlstatus, pretitemdtlnote, pretitemdtlres1, pretitemdtlres2, pretitemdtlres3, pretd.upduser, pretd.updtime, mritemvalue pretitemvalue, mritemvalueidr pretitemvalueidr, mritemvalueusd pretitemvalueusd, reasonoid, pretitemvalueidr_stock, pretitemvalueusd_stock FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mritemmstoid=pretd.mritemmstoid AND mrd.mritemdtloid=pretd.mritemdtloid WHERE pretd.cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + " ORDER BY pretitemdtlseq";
        //    List<QL_trnpretitemdtl> tbldtl = db.Database.SqlQuery<QL_trnpretitemdtl>(sSql).ToList();

        //    // Available Stock Validation & Stock Value Assignment
        //    var servertime = ClassFunction.GetServerTime();
        //    var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
        //    for (int i = 0; i < tbldtl.Count; i++)
        //    {
        //        var totalqty = tbldtl.Where(x => x.itemoid == tbldtl[i].itemoid && x.pretitemwhoid == tbldtl[i].pretitemwhoid).Sum(x => x.pretitemqty);
        //        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].itemoid, tbldtl[i].pretitemwhoid, totalqty, "FINISH GOOD"))
        //        {
        //            error = "Every Return Qty must be less than Stock Qty!";
        //            break;
        //        }
        //        else
        //        {
        //            tbldtl[i].pretitemvalueidr_stock = ClassFunction.GetStockValueIDR(cmp, sPeriod, "FINISH GOOD", tbldtl[i].itemoid);
        //            tbldtl[i].pretitemvalueusd_stock = ClassFunction.GetStockValueUSD(cmp, sPeriod, "FINISH GOOD", tbldtl[i].itemoid);
        //        }
        //    }

        //    if (error == "")
        //    {
        //        var conmtroid = ClassFunction.GenerateID("QL_conmat");
        //        var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
        //        var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
        //        var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
        //        var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
        //        var iVAR_STOCK_FG = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", cmp));
        //        var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp));
        //        var iVAR_SELISIH_EFISIENSI = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_SELISIH_EFISIENSI", cmp));

        //        using (var objTrans = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                sSql = "UPDATE QL_trnpretitemmst SET pretitemno='" + transno + "', pretitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND pretitemmststatus='In Approval' AND pretitemmstoid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnpretitemmst' AND oid=" + id;
        //                db.Database.ExecuteSqlCommand(sSql);
        //                db.SaveChanges();

        //                if (action == "Rejected")
        //                {
        //                    sSql = "UPDATE QL_trnmritemdtl SET mritemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemdtloid IN (SELECT mritemdtloid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();

        //                    sSql = "UPDATE QL_trnmritemmst SET mritemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid IN (SELECT mritemmstoid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }
        //                else if (action == "Approved")
        //                {
        //                    QL_trnpretitemdtl tbltmp;
        //                    for (int i = 0; i < tbldtl.Count; i++)
        //                    {
        //                        // Update Stock Value in Detail Table
        //                        tbltmp = tbldtl[i];
        //                        tbltmp.pretitemvalue = tbldtl[i].pretitemvalue;
        //                        tbltmp.pretitemvalueidr = tbldtl[i].pretitemvalueidr;
        //                        tbltmp.pretitemvalueusd = tbldtl[i].pretitemvalueusd;
        //                        tbltmp.pretitemvalueidr_stock = tbldtl[i].pretitemvalueidr_stock;
        //                        tbltmp.pretitemvalueusd_stock = tbldtl[i].pretitemvalueusd_stock;
        //                        db.Entry(tbltmp).State = EntityState.Modified;
        //                        db.SaveChanges();

        //                        // Insert QL_conmat
        //                        db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, conmtroid++, "PRETFG", "QL_trnpretitemdtl", tbldtl[i].pretitemmstoid, tbldtl[i].itemoid, "FINISH GOOD", tbldtl[i].pretitemwhoid, tbldtl[i].pretitemqty * -1, "Purchase Return Finish Good", transno, Session["UserID"].ToString(), null, tbldtl[i].pretitemvalueidr_stock, tbldtl[i].pretitemvalueusd_stock, 0, null, tbldtl[i].pretitemdtloid));
        //                        db.SaveChanges();

        //                        // Update/Insert QL_crdmtr
        //                        var flagcrd = "";
        //                        QL_crdmtr crdmtr = ClassFunction.UpdateOrInsertCrdMtr(cmp, crdmatoid, tbldtl[i].itemoid, "FINISH GOOD", tbldtl[i].pretitemwhoid, tbldtl[i].pretitemqty * -1, 0, "QL_trnpretitemdtl", Session["UserID"].ToString(), null, tbldtl[i].pretitemvalueidr_stock * tbldtl[i].pretitemqty * -1, tbldtl[i].pretitemvalueusd_stock * tbldtl[i].pretitemqty * -1, out flagcrd);
        //                        if (flagcrd == "Update")
        //                            db.Entry(crdmtr).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_crdmtr.Add(crdmtr);
        //                            crdmatoid++;
        //                        }
        //                        db.SaveChanges();

        //                        // Update/Insert QL_stockvalue
        //                        var flagstval = "";
        //                        QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, stockvalueoid, tbldtl[i].itemoid, "FINISH GOOD", tbldtl[i].pretitemqty * -1, tbldtl[i].pretitemvalueidr_stock, tbldtl[i].pretitemvalueusd_stock, "QL_trnpretitemdtl", null, Session["UserID"].ToString(), out flagstval);
        //                        if (flagstval == "Update")
        //                            db.Entry(stockvalue).State = EntityState.Modified;
        //                        else
        //                        {
        //                            db.QL_stockvalue.Add(stockvalue);
        //                            stockvalueoid++;
        //                        }
        //                        db.SaveChanges();
        //                    }

        //                    // Insert QL_trnglmst
        //                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "PRet Raw|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 0, 0, 0, 0));
        //                    db.SaveChanges();

        //                    decimal glamt = tbldtl.Sum(x => x.pretitemqty * x.pretitemvalue);
        //                    decimal glamtidr = tbldtl.Sum(x => x.pretitemqty * x.pretitemvalueidr);
        //                    decimal glamtusd = tbldtl.Sum(x => x.pretitemqty * x.pretitemvalueusd);
        //                    decimal glamtidr_st = tbldtl.Sum(x => x.pretitemqty * x.pretitemvalueidr_stock);
        //                    decimal glamtusd_st = tbldtl.Sum(x => x.pretitemqty * x.pretitemvalueusd_stock);
        //                    var glseq = 1;

        //                    // Insert QL_trngldtl
        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", glamt, transno, "PRet FG|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnpretitemmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();

        //                    var glamtidr_def = glamtidr - glamtidr_st;
        //                    if (glamtidr_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtidr_def > 0 ? "C" : "D", 0, transno, "PRet FG|No. " + transno, "Post", Session["UserID"].ToString(), servertime, Math.Abs(glamtidr_def), 0, "QL_trnpretitemmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    var glamtusd_def = glamtusd - glamtusd_st;
        //                    if (glamtusd_def != 0)
        //                    {
        //                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_SELISIH_EFISIENSI, glamtusd_def > 0 ? "C" : "D", 0, transno, "PRet FG|No. " + transno, "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(glamtusd_def), "QL_trnpretitemmst " + id.ToString(), null, null, null, 0));
        //                        db.SaveChanges();
        //                    }

        //                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_STOCK_FG, "C", glamt, transno, "PRet FG|No. " + transno, "Post", Session["UserID"].ToString(), servertime, glamtidr_st, glamtusd_st, "QL_trnpretitemmst " + id.ToString(), null, null, null, 0));
        //                    db.SaveChanges();
        //                    glmstoid++;

        //                    // Update Last ID
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (stockvalueoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                    sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
        //                    db.Database.ExecuteSqlCommand(sSql);
        //                    db.SaveChanges();
        //                }

        //                objTrans.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                objTrans.Rollback();
        //                error = ex.ToString();
        //            }
        //        }
        //    }
        //}
        //#endregion

        #region Approval A/P

        public class trnimportdtl
        {
            public int importmstoid { get; set; }
            public int importacctgoid { get; set; }
            public decimal importvalue { get; set; }
            public int curroid { get; set; }
            public int suppoid { get; set; }
            public string suppic { get; set; }
            public string importno { get; set; }
            public DateTime registerdate { get; set; }
            public Decimal importvalueidr { get; set; }
            public Decimal importvalueusd { get; set; }
            public Decimal importvaluepost { get; set; }
            public int rateoid { get; set; }
            public Decimal ratetoidr { get; set; }
            public Decimal ratetousd { get; set; }
            public int rate2oid { get; set; }
            public Decimal rate2toidr { get; set; }
            public Decimal rate2tousd { get; set; }
            public int importdtloid { get; set; }
            public string apimportno { get; set; }
            public int apimportmstoid { get; set; }
        }

        public class trnmrdtl
        {
            public decimal apqty { get; set; }
            public decimal mrvalue { get; set; }
            public decimal mrvalueidr { get; set; }
            public decimal mrvalueusd { get; set; }
        }

        private void ApprovalAPRM(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnaprawmst tblmst;
            tblmst = db.QL_trnaprawmst.Find(cmp, id);

            var cRate = new ClassRate();
            var cRateTmp = new ClassRate();
            DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),aprawdate) AS [Due Date] FROM QL_trnaprawmst ap INNER JOIN QL_mstgen g ON g.genoid=ap.aprawpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id).FirstOrDefault();
            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tblmst.suppoid).FirstOrDefault();
            var servertime = ClassFunction.GetServerTime();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);

            List<trnmrdtl> tblMR = null;

            sSql = "SELECT apd.aprawqty apqty, mrd.mrrawvalue mrvalue, mrd.mrrawvalueidr mrvalueidr, mrd.mrrawvalueusd mrvalueusd FROM QL_trnaprawdtl apd INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrrawdtloid=apd.mrrawdtloid AND mrd.mrrawmstoid=apd.mrrawmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id;

            tblMR = db.Database.SqlQuery<trnmrdtl>(sSql).ToList();


            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;

                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PPN_IN", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_IDR");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_USD", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_USD");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");
                if (!ClassFunction.IsInterfaceExists("VAR_AP_LOCAL", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_AP_LOCAL");

                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_APIC_ASSIGNMENT", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_APIC_ASSIGNMENT");                
            }

            if (error == "")
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
                var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid.Value));
                var iVAR_PPN_IN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", cmp));
                //var iVAR_DIFF_CURR_IDR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", cmp));
                //var iVAR_DIFF_CURR_USD = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_USD", cmp));
                var iVAR_ROUNDING = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));

                var iVAR_AP = 0;
                iVAR_AP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_LOCAL", cmp));


                //var iVAR_APIC_ASSIGNMENT = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_APIC_ASSIGNMENT", cmp));
                //decimal dTotalImport_IDR = 0;
                //decimal dTotalImport_USD = 0;
                decimal dMRAmt = 0;
                decimal dMRAmtIDR = 0;
                decimal dMRAmtUSD = 0;
                decimal dTotalImportIDR = 0;
                decimal dTotalImportUSD = 0;
                decimal dTotalImport = 0;
                decimal dRound = 0;
                decimal dDiffIDR = 0;
                decimal dDiffUSD = 0;

                if (action == "Approved")
                {

                    dMRAmt = tblMR.Sum(x => (x.apqty * x.mrvalue));
                    dMRAmtIDR = tblMR.Sum(x => (x.apqty * x.mrvalueidr));
                    dMRAmtUSD = tblMR.Sum(x => (x.apqty * x.mrvalueusd));
                }
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnaprawmst SET aprawmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += " , aprawno='"+transno+"', rateoid = " + cRate.GetRateDailyOid + ", aprawratetoidr = '" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', aprawratetousd = '" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', rate2oid = " + cRate.GetRateMonthlyOid + ", aprawrate2toidr = '" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', aprawrate2tousd = '" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "', aprawtotalamtidr = aprawtotalamt * " + cRate.GetRateMonthlyIDRValue + ", aprawtotalamtusd = aprawtotalamt * " + cRate.GetRateMonthlyUSDValue + ", aprawtotaldiscidr = aprawtotaldisc * " + cRate.GetRateMonthlyIDRValue + ", aprawtotaldiscusd = aprawtotaldisc * " + cRate.GetRateMonthlyUSDValue + ", aprawtotaltaxidr = aprawtotaltax * " + cRate.GetRateMonthlyIDRValue + ", aprawtotaltaxusd = aprawtotaltax * " + cRate.GetRateMonthlyUSDValue + ", aprawgrandtotalidr = aprawgrandtotal * " + cRate.GetRateMonthlyIDRValue + ", aprawgrandtotalusd = aprawgrandtotal * " + cRate.GetRateMonthlyUSDValue;
                        }
                        sSql += " WHERE cmpcode='" + CompnyCode + "' AND aprawmststatus='In Approval' AND aprawmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnaprawmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnaprawdtl WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrrawmst SET mrrawmststatus='' WHERE cmpcode='" + CompnyCode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnaprawmst WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnaprawdtl WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrrawmst SET mrrawmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnaprawdtl WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(cmp, conapoid++, "QL_trnaprawmst", id, 0, tblmst.suppoid, iVAR_AP, "Post", "APRM", servertime, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tblmst.aprawgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tblmst.aprawgrandtotal * cRate.GetRateMonthlyIDRValue, 0, tblmst.aprawgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0,tblmst.divgroupoid??0, ""));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conapoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "A/P Raw|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            // Insert QL_trngldtl
                            dRound = tblmst.aprawtotalamt + dTotalImport - dMRAmt;
                            dDiffIDR = (tblmst.aprawtotalamt * cRate.GetRateMonthlyIDRValue) + dTotalImportIDR - dMRAmtIDR;
                            dDiffUSD = (tblmst.aprawtotalamt * cRate.GetRateMonthlyUSDValue) + dTotalImportUSD - dMRAmtUSD;
                            var glseq = 1;

                            // Insert QL_trngldtl
                            // D - GRIR
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", dMRAmt, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dMRAmtIDR, dMRAmtUSD, "QL_trnaprawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            decimal totaltax = Convert.ToDecimal(tblmst.aprawtotaltax);

                            // D - PPN IN
                            if (tblmst.aprawtotaltax > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PPN_IN, "D", totaltax, transno, "A/P Raw| Note. PPN | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, (totaltax * cRate.GetRateMonthlyIDRValue), (totaltax * cRate.GetRateMonthlyUSDValue), "QL_trnaprawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            // D - ROUNDING (+)
                            if (dRound > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "D", dRound, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnaprawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "D", 0, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dDiffIDR, 0, "QL_trnaprawmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "D", 0, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, dDiffUSD, "QL_trnaprawmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            // C - HUTANG
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_AP, "C", tblmst.aprawgrandtotal, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, (tblmst.aprawgrandtotal * cRate.GetRateMonthlyIDRValue), (tblmst.aprawgrandtotal * cRate.GetRateMonthlyUSDValue), "QL_trnaprawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            // C - ROUNDING (-)
                            if (dRound < 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "C", -dRound, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnaprawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "C", 0, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, -dDiffIDR, 0, "QL_trnaprawmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "C", 0, transno, "A/P Raw|No. " + transno + tblmst.aprawmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, -dDiffUSD, "QL_trnaprawmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}


                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalAPGM(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnapgenmst tblmst;
            tblmst = db.QL_trnapgenmst.Find(cmp, id);

            var cRate = new ClassRate();
            var cRateTmp = new ClassRate();
            DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apgendate) AS [Due Date] FROM QL_trnapgenmst ap INNER JOIN QL_mstgen g ON g.genoid=ap.apgenpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id).FirstOrDefault();
            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tblmst.suppoid).FirstOrDefault();
            var servertime = ClassFunction.GetServerTime();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);

            List<trnmrdtl> tblMR = null;

            sSql = "SELECT apd.apgenqty apqty, mrd.mrgenvalue mrvalue, mrd.mrgenvalueidr mrvalueidr, mrd.mrgenvalueusd mrvalueusd FROM QL_trnapgendtl apd INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrgendtloid=apd.mrgendtloid AND mrd.mrgenmstoid=apd.mrgenmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id;

            tblMR = db.Database.SqlQuery<trnmrdtl>(sSql).ToList();


            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;

                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PPN_IN", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_IDR");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_USD", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_USD");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");

                if (!ClassFunction.IsInterfaceExists("VAR_AP_LOCAL", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_AP_LOCAL");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_APIC_ASSIGNMENT", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_APIC_ASSIGNMENT");                
            }

            if (error == "")
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
                var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid.Value));
                var iVAR_PPN_IN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", cmp));
                //var iVAR_DIFF_CURR_IDR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", cmp));
                //var iVAR_DIFF_CURR_USD = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_USD", cmp));
                var iVAR_ROUNDING = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));

                var iVAR_AP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_LOCAL", cmp));
                //var iVAR_APIC_ASSIGNMENT = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_APIC_ASSIGNMENT", cmp));
                //decimal dTotalImport_IDR = 0;
                //decimal dTotalImport_USD = 0;
                decimal dMRAmt = 0;
                decimal dMRAmtIDR = 0;
                decimal dMRAmtUSD = 0;
                decimal dTotalImportIDR = 0;
                decimal dTotalImportUSD = 0;
                decimal dTotalImport = 0;
                decimal dRound = 0;
                decimal dDiffIDR = 0;
                decimal dDiffUSD = 0;

                if (action == "Approved")
                {

                    dMRAmt = tblMR.Sum(x => (x.apqty * x.mrvalue));
                    dMRAmtIDR = tblMR.Sum(x => (x.apqty * x.mrvalueidr));
                    dMRAmtUSD = tblMR.Sum(x => (x.apqty * x.mrvalueusd));
                }
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnapgenmst SET apgenmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += " , apgenno='"+transno+"', rateoid = " + cRate.GetRateDailyOid + ", apgenratetoidr = '" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', apgenratetousd = '" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', rate2oid = " + cRate.GetRateMonthlyOid + ", apgenrate2toidr = '" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', apgenrate2tousd = '" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "', apgentotalamtidr = apgentotalamt * " + cRate.GetRateMonthlyIDRValue + ", apgentotalamtusd = apgentotalamt * " + cRate.GetRateMonthlyUSDValue + ", apgentotaldiscidr = apgentotaldisc * " + cRate.GetRateMonthlyIDRValue + ", apgentotaldiscusd = apgentotaldisc * " + cRate.GetRateMonthlyUSDValue + ", apgentotaltaxidr = apgentotaltax * " + cRate.GetRateMonthlyIDRValue + ", apgentotaltaxusd = apgentotaltax * " + cRate.GetRateMonthlyUSDValue + ", apgengrandtotalidr = apgengrandtotal * " + cRate.GetRateMonthlyIDRValue + ", apgengrandtotalusd = apgengrandtotal * " + cRate.GetRateMonthlyUSDValue;
                        }
                        sSql += " WHERE cmpcode='" + CompnyCode + "' AND apgenmststatus='In Approval' AND apgenmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnapgenmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnmrgendtl SET mrgendtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnapgendtl WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrgenmst SET mrgenmststatus='' WHERE cmpcode='" + CompnyCode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnapgenmst WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            sSql = "UPDATE QL_trnmrgendtl SET mrgendtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnapgendtl WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrgenmst SET mrgenmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnapgendtl WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(cmp, conapoid++, "QL_trnapgenmst", id, 0, tblmst.suppoid, iVAR_AP, "Post", "APGM", servertime, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tblmst.apgengrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tblmst.apgengrandtotal * cRate.GetRateMonthlyIDRValue, 0, tblmst.apgengrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0,tblmst.divgroupoid??0, ""));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conapoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "A/P Gen|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            // Insert QL_trngldtl
                            dRound = tblmst.apgentotalamt + dTotalImport - dMRAmt;
                            dDiffIDR = (tblmst.apgentotalamt * cRate.GetRateMonthlyIDRValue) + dTotalImportIDR - dMRAmtIDR;
                            dDiffUSD = (tblmst.apgentotalamt * cRate.GetRateMonthlyUSDValue) + dTotalImportUSD - dMRAmtUSD;
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", dMRAmt, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dMRAmtIDR, dMRAmtUSD, "QL_trnapgenmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            decimal totaltax = Convert.ToDecimal(tblmst.apgentotaltax);

                            if (tblmst.apgentotaltax > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PPN_IN, "D", totaltax, transno, "A/P Gen| Note. PPN | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, (totaltax * cRate.GetRateMonthlyIDRValue), (totaltax * cRate.GetRateMonthlyUSDValue), "QL_trnapgenmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            if (dRound > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "D", dRound, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnapgenmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "D", 0, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dDiffIDR, 0, "QL_trnapgenmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "D", 0, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, dDiffUSD, "QL_trnapgenmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_AP, "C", tblmst.apgengrandtotal, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, (tblmst.apgengrandtotal * cRate.GetRateMonthlyIDRValue), (tblmst.apgengrandtotal * cRate.GetRateMonthlyUSDValue), "QL_trnapgenmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            //if (dTotalImport > 0 || dTotalImportIDR > 0 || dTotalImportUSD > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_APIC_ASSIGNMENT, "C", dTotalImport, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dTotalImportIDR, dTotalImportUSD, "QL_trnapgenmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            if (dRound < 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "C", -dRound, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnapgenmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "C", 0, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, -dDiffIDR, 0, "QL_trnapgenmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "C", 0, transno, "A/P Gen|No. " + transno + tblmst.apgenmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, -dDiffUSD, "QL_trnapgenmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalAPService(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            var servertime = ClassFunction.GetServerTime();
            var acctgoid_ap = 0; var acctgoid_ppn = 0; var acctgoid_round = 0; var acctgoid_idr = 0; var acctgoid_usd = 0;
            var tbl = db.QL_trnapservicemst.Where(x => x.cmpcode == cmp && x.apservicemstoid == id && x.apservicemststatus == "In Approval").First();
            if (tbl == null)
            {
                error = "Please select data for approval!";
                return;
            }
            List<QL_trnapservicedtl> tbldtl = db.QL_trnapservicedtl.Where(x => x.cmpcode == cmp && x.apservicemstoid == id).ToList();
            if (tbldtl == null || tbldtl.Count == 0)
            {
                error = "Please select data for approval!";
                return;
            }
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    error = cRate.GetRateDailyLastError;
                    return;
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                    return;
                }
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", cmp))
                {
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");
                    return;
                }
                else
                    acctgoid_ppn = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", cmp));
                if (!ClassFunction.IsInterfaceExists("VAR_AP_LOCAL", cmp))
                {
                    error = ClassFunction.GetInterfaceWarning("VAR_AP_LOCAL");
                    return;
                }
                else
                    acctgoid_ap = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_LOCAL", cmp));
                if (!ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                {
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");
                    return;
                }
                else
                    acctgoid_round = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));
                if (!ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", cmp))
                {
                    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_IDR");
                    return;
                }
                else
                    acctgoid_idr = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", cmp));
                if (!ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_USD", cmp))
                {
                    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_USD");
                    return;
                }
                else
                    acctgoid_usd = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_USD", cmp));
            }
            var conapoid = ClassFunction.GenerateID("QL_CONAP");
            var glmstoid = ClassFunction.GenerateID("QL_TRNGLMST");
            var gldtloid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
            DateTime duedate = db.Database.SqlQuery<DateTime>("SELECT DATEADD(day, (CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END), apservicedate) [Due Date] FROM QL_trnapservicemst ap INNER JOIN QL_mstgen g ON g.genoid=ap.apservicepaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + cmp + "' AND apservicemstoid=" + id).FirstOrDefault();
            var suppname = db.QL_mstsupp.Where(u => u.suppoid == tbl.suppoid).First().suppname;

            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    tbl.apservicemststatus = action;
                    tbl.upduser = Session["UserID"].ToString();
                    tbl.updtime = servertime;
                    tbl.approvaluser = Session["UserID"].ToString();
                    tbl.approvaldatetime = servertime;
                    if (action == "Approved")
                    {
                        tbl.rateoid = cRate.GetRateDailyOid;
                        tbl.apserviceratetoidr = cRate.GetRateDailyIDRValue.ToString();
                        tbl.apserviceratetousd = cRate.GetRateDailyUSDValue.ToString();
                        tbl.rate2oid = cRate.GetRateMonthlyOid;
                        tbl.apservicerate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                        tbl.apservicerate2tousd = cRate.GetRateMonthlyUSDValue.ToString();
                        tbl.apservicetotalamtidr = tbl.apservicetotalamtidr * cRate.GetRateMonthlyIDRValue;
                        tbl.apservicetotalamtusd = tbl.apservicetotalamtidr * cRate.GetRateMonthlyUSDValue;
                        tbl.apservicetotaltaxidr = tbl.apservicetotaltax * cRate.GetRateMonthlyIDRValue;
                        tbl.apservicetotaltaxusd = tbl.apservicetotaltax * cRate.GetRateMonthlyUSDValue;
                        tbl.apservicegrandtotalidr = tbl.apservicegrandtotal * cRate.GetRateMonthlyIDRValue;
                        tbl.apservicegrandtotalusd = tbl.apservicegrandtotal * cRate.GetRateMonthlyUSDValue;
                    }
                    db.Entry(tbl).State = EntityState.Modified;

                    sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + cmp + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnapservicemst' AND oid=" + id;
                    db.Database.ExecuteSqlCommand(sSql);

                    if (action == "Rejected")
                    {
                        sSql = "UPDATE QL_trnmrservicedtl SET mrservicedtlstatus='' WHERE cmpcode='" + cmp + "' AND mrservicedtloid IN (SELECT mrservicedtloid FROM QL_trnapservicedtl WHERE cmpcode='" + cmp + "' AND apservicemstoid=" + id + ") AND mrservicemstoid IN (SELECT mrservicemstoid FROM QL_trnapservicedtl WHERE cmpcode='" + cmp + "' AND apservicemstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        sSql = "UPDATE QL_trnmrservicemst SET mrservicemststatus='Post' WHERE cmpcode='" + cmp + "' AND mrservicemstoid IN (SELECT mrservicemstoid FROM QL_trnapservicedtl WHERE cmpcode='" + cmp + "' AND apservicemstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                    }
                    else if (action == "Approved")
                    {
                        // Insert QL_conap                        
                        db.QL_conap.Add(ClassFunction.InsertConAP(cmp, conapoid++, "QL_trnapservicemst", id, 0, tbl.suppoid, acctgoid_ap, "Post", "APSVC", servertime, periodacctg, 0, new DateTime(1900, 01, 01), "", 0, duedate, tbl.apservicegrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.apservicegrandtotal * cRate.GetRateMonthlyIDRValue, 0, tbl.apservicegrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, tbl.divgroupoid??0,""));
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (conapoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_CONAP'";
                        db.Database.ExecuteSqlCommand(sSql);

                        // Insert QL_trnglmst
                        db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), periodacctg, "A/P Service|No. " + tbl.apserviceno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));

                        // Insert QL_trngldtl
                        var glseq = 1;
                        decimal dTotalService = 0.0000M; decimal dTotalServiceIDR = 0.0000M; decimal dTotalServiceUSD = 0.000000M;
                        for (int i = 0; i < tbldtl.Count; i++)
                        {
                            var xxx = tbldtl[i].serviceoid;
                            var acctgoid_svc = db.QL_mstservice.Where(s => s.serviceoid == xxx).First().serviceacctgoid;
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_svc, "D", Math.Round(tbldtl[i].apservicedtlnetto, 4), tbl.apserviceno, "A/P Service|Note. " + tbldtl[i].apservicedtlnote + "|Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, Math.Round(tbldtl[i].apservicedtlnetto * cRate.GetRateMonthlyIDRValue, 4), Math.Round(tbldtl[i].apservicedtlnetto * cRate.GetRateMonthlyUSDValue, 6), "QL_trnapservicemst " + id.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                            dTotalService += Math.Round(tbldtl[i].apservicedtlnetto, 4);
                            dTotalServiceIDR += Math.Round(tbldtl[i].apservicedtlnetto * cRate.GetRateMonthlyIDRValue, 4);
                            dTotalServiceUSD += Math.Round(tbldtl[i].apservicedtlnetto * cRate.GetRateMonthlyUSDValue, 6);
                        }

                        if (tbl.apservicetotaltax > 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_ppn, "D", Math.Round((decimal)tbl.apservicetotaltax, 4), tbl.apserviceno, "A/P Service| Note. PPN | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, Math.Round((decimal)tbl.apservicetotaltax * cRate.GetRateMonthlyIDRValue, 4), Math.Round((decimal)tbl.apservicetotaltax * cRate.GetRateMonthlyUSDValue, 6), "QL_trnapservicemst " + id.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                        }

                        decimal dSelisih = dTotalService + Math.Round((decimal)tbl.apservicetotaltax, 4) - Math.Round(tbl.apservicegrandtotal, 4);
                        if (dSelisih != 0)
                        {
                            var dbcr = "D";
                            if (dSelisih < 0) dbcr = "C";
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_round, dbcr, Math.Abs(dSelisih), tbl.apserviceno, "A/P Service| Note. Selisih | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnapservicemst " + id.ToString(), null, null, null, 0,tbl.divgroupoid??0));
                        }
                        decimal dSelisihIDR = dTotalServiceIDR + Math.Round((decimal)tbl.apservicetotaltax * cRate.GetRateMonthlyIDRValue, 4) - Math.Round(tbl.apservicegrandtotal * cRate.GetRateMonthlyIDRValue, 4);
                        if (dSelisihIDR != 0)
                        {
                            var dbcr = "D";
                            if (dSelisihIDR < 0) dbcr = "C";
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_idr, dbcr, 0, tbl.apserviceno, "A/P Service| Note. Selisih IDR | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, Math.Abs(dSelisihIDR), 0, "QL_trnapservicemst " + id.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                        }
                        decimal dSelisihUSD = dTotalServiceUSD + Math.Round((decimal)tbl.apservicetotaltax * cRate.GetRateMonthlyUSDValue, 6) - Math.Round(tbl.apservicegrandtotal * cRate.GetRateMonthlyUSDValue, 6);
                        if (dSelisihUSD != 0)
                        {
                            var dbcr = "D";
                            if (dSelisihUSD < 0) dbcr = "C";
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_usd, dbcr, 0, tbl.apserviceno, "A/P Service| Note. Selisih USD | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(dSelisihUSD), "QL_trnapservicemst " + id.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                        }

                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, acctgoid_ap, "C", Math.Round(tbl.apservicegrandtotal, 4), tbl.apserviceno, "A/P Service|Note. " + tbl.apservicemstnote + "|Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, Math.Round(tbl.apservicegrandtotal * cRate.GetRateMonthlyIDRValue, 4), Math.Round(tbl.apservicegrandtotal * cRate.GetRateMonthlyUSDValue, 6), "QL_trnapservicemst " + id.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_TRNGLMST'";
                        db.Database.ExecuteSqlCommand(sSql);
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_TRNGLDTL'";
                        db.Database.ExecuteSqlCommand(sSql);
                    }

                    db.SaveChanges();
                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    objTrans.Rollback();
                    error = ex.ToString();
                }
            }
        }

        private void ApprovalAPAS(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnapassetmst tblmst;
            tblmst = db.QL_trnapassetmst.Find(cmp, id);

            var cRate = new ClassRate();
            var cRateTmp = new ClassRate();
            DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g.gendesc AS INT) END),apassetdate) AS [Due Date] FROM QL_trnapassetmst ap INNER JOIN QL_mstgen g ON g.genoid=ap.apassetpaytypeoid AND g.gengroup='PAYMENT TYPE' WHERE ap.cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id).FirstOrDefault();
            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tblmst.suppoid).FirstOrDefault();
            var servertime = ClassFunction.GetServerTime();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);

            List<trnmrdtl> tblMR = null;

            sSql = "SELECT apd.apassetqty apqty, mrd.mrassetvalue mrvalue, mrd.mrassetvalueidr mrvalueidr, mrd.mrassetvalueusd mrvalueusd FROM QL_trnapassetdtl apd INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrassetdtloid=apd.mrassetdtloid AND mrd.mrassetmstoid=apd.mrassetmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id;

            tblMR = db.Database.SqlQuery<trnmrdtl>(sSql).ToList();


            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, ClassFunction.GetServerTime().ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;

                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid))
                    error = ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PPN_IN", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_IDR");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_USD", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_DIFF_CURR_USD");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");

                if (!ClassFunction.IsInterfaceExists("VAR_AP_LOCAL", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_AP_LOCAL");
                //if (error == "" && !ClassFunction.IsInterfaceExists("VAR_APIC_ASSIGNMENT", cmp))
                //    error = ClassFunction.GetInterfaceWarning("VAR_APIC_ASSIGNMENT");                
            }

            if (error == "")
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
                var iVAR_PURC_RECEIVED = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", cmp, tblmst.divgroupoid));
                var iVAR_PPN_IN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", cmp));
                //var iVAR_DIFF_CURR_IDR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", cmp));
                //var iVAR_DIFF_CURR_USD = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_USD", cmp));
                var iVAR_ROUNDING = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));

                var iVAR_AP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_LOCAL", cmp));
                //var iVAR_APIC_ASSIGNMENT = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_APIC_ASSIGNMENT", cmp));
                //decimal dTotalImport_IDR = 0;
                //decimal dTotalImport_USD = 0;
                decimal dMRAmt = 0;
                decimal dMRAmtIDR = 0;
                decimal dMRAmtUSD = 0;
                decimal dTotalImportIDR = 0;
                decimal dTotalImportUSD = 0;
                decimal dTotalImport = 0;
                decimal dRound = 0;
                decimal dDiffIDR = 0;
                decimal dDiffUSD = 0;

                if (action == "Approved")
                {

                    dMRAmt = tblMR.Sum(x => (x.apqty * x.mrvalue));
                    dMRAmtIDR = tblMR.Sum(x => (x.apqty * x.mrvalueidr));
                    dMRAmtUSD = tblMR.Sum(x => (x.apqty * x.mrvalueusd));
                }
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += " , apassetno='" + transno + "', rateoid = " + cRate.GetRateDailyOid + ", apassetratetoidr = '" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', apassetratetousd = '" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', rate2oid = " + cRate.GetRateMonthlyOid + ", apassetrate2toidr = '" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', apassetrate2tousd = '" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "', apassettotalamtidr = apassettotalamt * " + cRate.GetRateMonthlyIDRValue + ", apassettotalamtusd = apassettotalamt * " + cRate.GetRateMonthlyUSDValue + ", apassettotaldiscidr = apassettotaldisc * " + cRate.GetRateMonthlyIDRValue + ", apassettotaldiscusd = apassettotaldisc * " + cRate.GetRateMonthlyUSDValue + ", apassettotaltaxidr = apassettotaltax * " + cRate.GetRateMonthlyIDRValue + ", apassettotaltaxusd = apassettotaltax * " + cRate.GetRateMonthlyUSDValue + ", apassetgrandtotalidr = apassetgrandtotal * " + cRate.GetRateMonthlyIDRValue + ", apassetgrandtotalusd = apassetgrandtotal * " + cRate.GetRateMonthlyUSDValue;
                        }
                        sSql += " WHERE cmpcode='" + CompnyCode + "' AND apassetmststatus='In Approval' AND apassetmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND statusrequest='New' AND event='In Approval' AND tablename='QL_trnapassetmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid IN (SELECT mrassetdtloid FROM QL_trnapassetdtl WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetmst WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid IN (SELECT mrassetdtloid FROM QL_trnapassetdtl WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetdtl WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(cmp, conapoid++, "QL_trnapassetmst", id, 0, tblmst.suppoid, iVAR_AP, "Post", "APAS", servertime, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tblmst.apassetgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tblmst.apassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, tblmst.apassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, tblmst.divgroupoid, ""));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conapoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "A/P Gen|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid));
                            db.SaveChanges();

                            // Insert QL_trngldtl
                            dRound = tblmst.apassettotalamt + dTotalImport - dMRAmt;
                            dDiffIDR = (tblmst.apassettotalamt * cRate.GetRateMonthlyIDRValue) + dTotalImportIDR - dMRAmtIDR;
                            dDiffUSD = (tblmst.apassettotalamt * cRate.GetRateMonthlyUSDValue) + dTotalImportUSD - dMRAmtUSD;
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PURC_RECEIVED, "D", dMRAmt, transno, "A/P Aset|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dMRAmtIDR, dMRAmtUSD, "QL_trnapassetmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid));
                            db.SaveChanges();

                            decimal totaltax = Convert.ToDecimal(tblmst.apassettotaltax);

                            if (tblmst.apassettotaltax > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_PPN_IN, "D", totaltax, transno, "A/P Aset| Note. PPN | Penerima." + suppname, "Post", Session["UserID"].ToString(), servertime, (totaltax * cRate.GetRateMonthlyIDRValue), (totaltax * cRate.GetRateMonthlyUSDValue), "QL_trnapassetmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid));
                                db.SaveChanges();
                            }

                            if (dRound > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "D", dRound, transno, "A/P Aset|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnapassetmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "D", 0, transno, "A/P Gen|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dDiffIDR, 0, "QL_trnapassetmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "D", 0, transno, "A/P Gen|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, dDiffUSD, "QL_trnapassetmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_AP, "C", tblmst.apassetgrandtotal, transno, "A/P Aset|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, (tblmst.apassetgrandtotal * cRate.GetRateMonthlyIDRValue), (tblmst.apassetgrandtotal * cRate.GetRateMonthlyUSDValue), "QL_trnapassetmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid));
                            db.SaveChanges();

                            //if (dTotalImport > 0 || dTotalImportIDR > 0 || dTotalImportUSD > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_APIC_ASSIGNMENT, "C", dTotalImport, transno, "A/P Gen|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, dTotalImportIDR, dTotalImportUSD, "QL_trnapassetmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            if (dRound < 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_ROUNDING, "C", -dRound, transno, "A/P Aset|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, 0, "QL_trnapassetmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid));
                                db.SaveChanges();
                            }

                            //if (dDiffIDR < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_IDR, "C", 0, transno, "A/P Gen|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, -dDiffIDR, 0, "QL_trnapassetmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            //if (dDiffUSD < 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iVAR_DIFF_CURR_USD, "C", 0, transno, "A/P Gen|No. " + transno + tblmst.apassetmstnote + " | Penerima. " + suppname, "Post", Session["UserID"].ToString(), servertime, 0, -dDiffUSD, "QL_trnapassetmst " + id.ToString(), null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion


        #region Approval SO
        private void ApprovalSOFG(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnsoitemmst tblmst = db.QL_trnsoitemmst.Find(cmp, id);

            var cRate = new ClassRate();
            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var iPriceOid = ClassFunction.GenerateID("QL_MSTITEMPRICE");
            var custoid = tblmst.custoid;
            var curroid = tblmst.curroid;

            //Data Detail
            var tbldtl = (from sod in db.QL_trnsoitemdtl where sod.cmpcode == cmp && sod.soitemmstoid == id select new { sod.itemoid, sod.soitemqty, sod.soitemprice, sod.soitemdtlamt }).ToList();
            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;
            }

            //validasi credit limit
            decimal cr_cust = db.QL_mstcust.FirstOrDefault(x => x.custoid == tblmst.custoid).custcreditlimit;
            decimal by_cust = tblmst.soitemgrandtotalamt + db.Database.SqlQuery<decimal>($"select dbo.getCreditLimitBalance({tblmst.custoid})").FirstOrDefault();
            if (by_cust > cr_cust) error += "- Nilai SO Melebihi Total Credit limit!!<br>";

            if (error == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        // Update SO
                        sSql = "UPDATE QL_trnsoitemmst SET soitemno='" + transno + "', soitemmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "' " + sqlPlus;
                        if (action == "Approved")
                        {
                            sSql += ", rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid;
                        }
                        sSql += " WHERE soitemmststatus='In Approval' AND soitemmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsoitemmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Approved")
                        {
                            if (tbldtl != null)
                            {
                                if (tbldtl.Count() > 0)
                                {
                                    for (var i = 0; i < tbldtl.Count(); i++)
                                    {
                                        var matoid = tbldtl[i].itemoid;
                                        QL_mstitemprice tblMatPrice = db.QL_mstitemprice.Where(x => x.cmpcode == cmp && x.itemoid == matoid && x.refoid == custoid && x.refname == "CUSTOMER" && x.curroid == curroid).FirstOrDefault();

                                        if (tblMatPrice != null)
                                        {
                                            tblMatPrice.totalsalesqty = (tblMatPrice.totalsalesqty + tbldtl[i].soitemqty);
                                            tblMatPrice.avgsalesprice = ((tblMatPrice.avgsalesprice * tblMatPrice.totalsalesqty) + tbldtl[i].soitemdtlamt) / (tblMatPrice.totalsalesqty + tbldtl[i].soitemqty);
                                            tblMatPrice.lastsalesprice = tbldtl[i].soitemprice;
                                            tblMatPrice.lasttrans = "QL_trnsoitemmst";
                                            tblMatPrice.lasttransoid = id;
                                            tblMatPrice.lasttransno = transno;
                                            tblMatPrice.lasttransdate = tblmst.soitemdate;
                                            tblMatPrice.upduser = userid;
                                            tblMatPrice.updtime = servertime;
                                            tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                            db.Entry(tblMatPrice).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            tblMatPrice = new QL_mstitemprice();
                                            tblMatPrice.cmpcode = cmp;
                                            tblMatPrice.itempriceoid = iPriceOid;
                                            tblMatPrice.itemoid = tbldtl[i].itemoid;
                                            tblMatPrice.refoid = tblmst.custoid;
                                            tblMatPrice.refname = "CUSTOMER";
                                            tblMatPrice.totalsalesqty = tbldtl[i].soitemqty;
                                            tblMatPrice.totalpurchaseqty = 0;
                                            tblMatPrice.avgsalesprice = tbldtl[i].soitemprice;
                                            tblMatPrice.lastsalesprice = tbldtl[i].soitemprice;
                                            tblMatPrice.avgpurchaseprice = 0;
                                            tblMatPrice.lastpurchaseprice = 0;
                                            tblMatPrice.lasttrans = "QL_trnsoitemmst";
                                            tblMatPrice.lasttransoid = id;
                                            tblMatPrice.lasttransno = transno;
                                            tblMatPrice.lasttransdate = tblmst.soitemdate;
                                            tblMatPrice.note = "";
                                            tblMatPrice.res1 = "";
                                            tblMatPrice.res2 = "";
                                            tblMatPrice.res3 = "";
                                            tblMatPrice.upduser = userid;
                                            tblMatPrice.updtime = servertime;
                                            tblMatPrice.curroid = tblmst.curroid;
                                            tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                            db.QL_mstitemprice.Add(tblMatPrice);
                                            db.SaveChanges();
                                        }

                                        iPriceOid++;
                                    }

                                    sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTITEMPRICE'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalSORaw(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnsorawmst tblmst = db.QL_trnsorawmst.Find(cmp, id);

            var cRate = new ClassRate();
            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var iPriceOid = ClassFunction.GenerateID("QL_MSTMATRAWPRICE");
            var custoid = tblmst.custoid;
            var curroid = tblmst.curroid;

            //Data Detail
            var tbldtl = (from sod in db.QL_trnsorawdtl where sod.cmpcode == cmp && sod.sorawmstoid == id select new { sod.matrawoid, sod.sorawqty, sod.sorawprice, sod.sorawdtlamt }).ToList();
            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;
            }

            //validasi credit limit
            decimal cr_cust = db.QL_mstcust.FirstOrDefault(x => x.custoid == tblmst.custoid).custcreditlimit;
            decimal by_cust = tblmst.sorawgrandtotalamt + db.Database.SqlQuery<decimal>($"select dbo.getCreditLimitBalance({tblmst.custoid})").FirstOrDefault();
            if (by_cust > cr_cust) error += "- Nilai SO Melebihi Total Credit limit!!<br>";

            if (error == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Update SO
                        sSql = "UPDATE QL_trnsorawmst SET sorawno='" + transno + "', sorawmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += ", rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid;
                        }
                        sSql += " WHERE sorawmststatus='In Approval' AND sorawmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsorawmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Approved")
                        {
                            if (tbldtl != null)
                            {
                                if (tbldtl.Count() > 0)
                                {
                                    for (var i = 0; i < tbldtl.Count(); i++)
                                    {
                                        var matoid = tbldtl[i].matrawoid;
                                        QL_mstmatrawprice tblMatPrice = db.QL_mstmatrawprice.Where(x => x.cmpcode == cmp && x.matrawoid == matoid && x.refoid == custoid && x.refname == "CUSTOMER" && x.curroid == curroid).FirstOrDefault();

                                        if (tblMatPrice != null)
                                        {
                                            tblMatPrice.totalsalesqty = (tblMatPrice.totalsalesqty + tbldtl[i].sorawqty);
                                            tblMatPrice.avgsalesprice = ((tblMatPrice.avgsalesprice * tblMatPrice.totalsalesqty) + tbldtl[i].sorawdtlamt) / (tblMatPrice.totalsalesqty + tbldtl[i].sorawqty);
                                            tblMatPrice.lastsalesprice = tbldtl[i].sorawprice;
                                            tblMatPrice.lasttrans = "QL_trnsorawmst";
                                            tblMatPrice.lasttransoid = id;
                                            tblMatPrice.lasttransno = transno;
                                            tblMatPrice.lasttransdate = tblmst.sorawdate;
                                            tblMatPrice.upduser = userid;
                                            tblMatPrice.updtime = servertime;
                                            tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;
                                            db.Entry(tblMatPrice).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            tblMatPrice = new QL_mstmatrawprice();
                                            tblMatPrice.cmpcode = cmp;
                                            tblMatPrice.matrawpriceoid = iPriceOid;
                                            tblMatPrice.matrawoid = tbldtl[i].matrawoid;
                                            tblMatPrice.refoid = tblmst.custoid;
                                            tblMatPrice.refname = "CUSTOMER";
                                            tblMatPrice.totalsalesqty = tbldtl[i].sorawqty;
                                            tblMatPrice.totalpurchaseqty = 0;
                                            tblMatPrice.avgsalesprice = tbldtl[i].sorawprice;
                                            tblMatPrice.lastsalesprice = tbldtl[i].sorawprice;
                                            tblMatPrice.avgpurchaseprice = 0;
                                            tblMatPrice.lastpurchaseprice = 0;
                                            tblMatPrice.lasttrans = "QL_trnsorawmst";
                                            tblMatPrice.lasttransoid = id;
                                            tblMatPrice.lasttransno = transno;
                                            tblMatPrice.lasttransdate = tblmst.sorawdate;
                                            tblMatPrice.note = "";
                                            tblMatPrice.res1 = "";
                                            tblMatPrice.res2 = "";
                                            tblMatPrice.res3 = "";
                                            tblMatPrice.upduser = userid;
                                            tblMatPrice.updtime = servertime;
                                            tblMatPrice.curroid = tblmst.curroid;
                                            tblMatPrice.ratetoidr = cRate.GetRateMonthlyIDRValue;

                                            db.QL_mstmatrawprice.Add(tblMatPrice);
                                            db.SaveChanges();
                                        }

                                        iPriceOid++;
                                    }

                                    sSql = "UPDATE QL_mstoid SET lastoid=" + (iPriceOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_MSTMATRAWPRICE'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion

        #region Approval Shipment

        public class shipmentdtl
        {
            public int shipmentdtloid { get; set; }
            public int shipmentdtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal shipmentqty { get; set; }
            public string shipmentdtlnote { get; set; }
            public int shipmentwhoid { get; set; }
            public string sono { get; set; }
            public string shipmentonsostatus { get; set; }
            public int sodtloid { get; set; }
            public int somstoid { get; set; }
            public decimal shipmentvalueidr { get; set; }
            public decimal shipmentvalueusd { get; set; }

            //assets
            public string assetno { get; set; }
            public decimal assetpurchase { get; set; }
            public decimal assetpurchaseidr { get; set; }
            public decimal assetpurchaseusd { get; set; }
            public decimal assetaccumdep { get; set; }
            public decimal assetaccumdepidr { get; set; }
            public decimal assetaccumdepusd { get; set; }
            public int assetacctgoid { get; set; }
            public int accumdepacctgoid { get; set; }
            public int depacctgoid { get; set; }
        }

        private void ApprovalShipmentFG(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnshipmentitemmst tblmst = db.QL_trnshipmentitemmst.Find(cmp, id);

            var cRate = new ClassRate();
            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var iConOid = ClassFunction.GenerateID("QL_CONMAT");
            var iGlMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
            var iGlDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var iStockValOid = ClassFunction.GenerateID("QL_STOCKVALUE");
            var refname = "FINISH GOOD";

            var custoid = tblmst.custoid;
            var curroid = tblmst.curroid;

            //Data Detail
            sSql = "SELECT shipmentitemdtloid shipmentdtloid, sd.shipmentitemdtlseq shipmentdtlseq, m.itemoid matoid, m.itemcode matcode, m.itemlongdesc matlongdesc, sd.shipmentitemqty shipmentqty, sd.shipmentitemdtlnote shipmentdtlnote, sd.shipmentitemwhoid shipmentwhoid, som.soitemno sono, ISNULL(shipmentitemdtlres3, '') AS shipmentonsostatus, sod.soitemdtloid sodtloid, som.soitemmstoid somstoid, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / SUM(NULLIF(stockqty, 0)) FROM QL_stockvalue st WHERE st.cmpcode=sd.cmpcode AND st.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND st.refoid=sd.itemoid AND refname='" + refname + "' AND closeflag=''), 0.0) AS shipmentvalueidr, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / SUM(NULLIF(stockqty, 0)) FROM QL_stockvalue st WHERE st.cmpcode=sd.cmpcode AND st.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND st.refoid=sd.itemoid AND refname='" + refname + "' AND closeflag=''), 0.0) AS shipmentvalueusd FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid INNER JOIN QL_mstitem m ON sd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sd.shipmentitemunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentitemwhoid=g2.genoid /*INNER JOIN QL_trndoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.doitemmstoid=sd.doitemmstoid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid*/ INNER JOIN QL_trnsoitemmst som ON som.cmpcode=sm.cmpcode AND som.soitemmstoid=sd.soitemmstoid INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=sm.cmpcode AND sod.soitemdtloid=sd.soitemdtloid WHERE sd.shipmentitemmstoid=" + id;
            var tbldtl = db.Database.SqlQuery<shipmentdtl>(sSql).ToList();

            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //    error = cRate.GetRateDailyLastError;
                //if (cRate.GetRateMonthlyLastError != "")
                //    error = cRate.GetRateMonthlyLastError;

                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_STOCK_FG", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_FG");
                if (tbldtl != null)
                {
                    for (var i = 0; i < tbldtl.Count(); i++)
                    {
                        var totalqty = tbldtl.Where(x => x.matoid == tbldtl[i].matoid && x.shipmentwhoid == tbldtl[i].shipmentwhoid).Sum(x => x.shipmentqty);
                        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].matoid, tbldtl[i].shipmentwhoid, totalqty, refname))
                        {
                            error = "Shipment Qty for Item Code : <STRONG>" + tbldtl[i].matcode + "</STRONG> must be less than Stock Qty!";
                            break;
                        }
                    }
                }
            }

            if (error == "")
            {
                var iDelAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value));
                var iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", cmp, tblmst.divgroupoid.Value));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        // Update Shipment
                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemno='" + transno + "', shipmentitemmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "' " + sqlPlus + " WHERE shipmentitemmststatus='In Approval' AND shipmentitemmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Update Approval
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnshipmentitemmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Post' WHERE cmpcode='" + cmp + "' AND doitemmstoid IN (SELECT DISTINCT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            if (tbldtl != null)
                            {
                                for (var i = 0; i < tbldtl.Count(); i++)
                                {
                                    if (tbldtl[i].shipmentonsostatus != "")
                                    {
                                        sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND soitemdtloid=" + tbldtl[i].sodtloid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        //db.SaveChanges();
                                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + cmp + "' AND soitemmstoid=" + tbldtl[i].somstoid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        //db.SaveChanges();
                                    }
                                    sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trndoitemdtl WHERE cmpcode='" + cmp + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ") AND doitemdtloid NOT IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();
                                    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + cmp + "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trndoitemdtl WHERE cmpcode='" + cmp + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ") AND doitemdtloid NOT IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();
                                    sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemvalueidr=" + tbldtl[i].shipmentvalueidr + ", shipmentitemvalueusd=" + tbldtl[i].shipmentvalueusd + " WHERE cmpcode='" + cmp + "' AND shipmentitemdtloid=" + tbldtl[i].shipmentdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();

                                    //Insert conmat
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, iConOid++, "SJFG", "QL_trnshipmentitemdtl", id, tbldtl[i].matoid, refname, tbldtl[i].shipmentwhoid, tbldtl[i].shipmentqty, "Shipment Finish Good", transno, userid, "","", tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, 0, null, tbldtl[i].shipmentdtloid, tblmst.divgroupoid??0,""));

                                  

                                    // Update/Insert QL_stockvalue
                                    var flagstval = "";
                                    QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, iStockValOid, tbldtl[i].matoid, refname, tbldtl[i].shipmentqty * -1, tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, "QL_trnshipmentitemdtl", null, userid, out flagstval);
                                    if (flagstval == "Update")
                                        db.Entry(stockvalue).State = EntityState.Modified;
                                    else
                                    {
                                        db.QL_stockvalue.Add(stockvalue);
                                        iStockValOid++;
                                    }
                                    //db.SaveChanges();
                                }
                                db.SaveChanges();
                            }

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, iGlMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Shipment FG|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            decimal glamt = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueidr);
                            decimal glamtidr = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueidr);
                            decimal glamtusd = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueusd);
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, iGlDtlOid++, glseq++, iGlMstOid, iDelAcctgOid, "D", glamt, transno, "Shipment FG|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnshipmentitemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, iGlDtlOid++, glseq++, iGlMstOid, iStockAcctgOid, "C", glamt, transno, "Shipment FG|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnshipmentitemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();
                            iGlMstOid++;

                            // Update Last ID
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iConOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                           
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlMstOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlDtlOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalShipmentRaw(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnshipmentrawmst tblmst = db.QL_trnshipmentrawmst.Find(cmp, id);

            var cRate = new ClassRate();
            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var iConOid = ClassFunction.GenerateID("QL_CONMAT");
            var iCrdOid = ClassFunction.GenerateID("QL_CRDMTR");
            var iGlMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
            var iGlDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var iStockValOid = ClassFunction.GenerateID("QL_STOCKVALUE");
            var refname = "RAW MATERIAL";

            var custoid = tblmst.custoid;
            var curroid = tblmst.curroid;

            //Data Detail
            sSql = "SELECT shipmentrawdtloid shipmentdtloid, sd.shipmentrawdtlseq shipmentdtlseq, m.matrawoid matoid, m.matrawcode matcode, m.matrawlongdesc matlongdesc, sd.shipmentrawqty shipmentqty, sd.shipmentrawdtlnote shipmentdtlnote, sd.shipmentrawwhoid shipmentwhoid, som.sorawno sono, ISNULL(shipmentrawdtlres3, '') AS shipmentonsostatus, sod.sorawdtloid sodtloid, som.sorawmstoid somstoid, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / SUM(NULLIF(stockqty, 0)) FROM QL_stockvalue st WHERE st.cmpcode=sd.cmpcode AND st.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND st.refoid=sd.matrawoid AND refname='" + refname + "' AND closeflag=''), 0.0) AS shipmentvalueidr, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / SUM(NULLIF(stockqty, 0)) FROM QL_stockvalue st WHERE st.cmpcode=sd.cmpcode AND st.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND st.refoid=sd.matrawoid AND refname='" + refname + "' AND closeflag=''), 0.0) AS shipmentvalueusd FROM QL_trnshipmentrawmst sm INNER JOIN QL_trnshipmentrawdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid INNER JOIN QL_mstmatraw m ON sd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON sd.shipmentrawunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentrawwhoid=g2.genoid INNER JOIN QL_trndorawmst dom ON dom.cmpcode=sd.cmpcode AND dom.dorawmstoid=sd.dorawmstoid INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=sd.cmpcode AND dod.dorawdtloid=sd.dorawdtloid INNER JOIN QL_trnsorawmst som ON som.cmpcode=dod.cmpcode AND som.sorawmstoid=dod.sorawmstoid INNER JOIN QL_trnsorawdtl sod ON sod.cmpcode=dod.cmpcode AND sod.sorawdtloid=dod.sorawdtloid WHERE sd.shipmentrawmstoid=" + id;
            var tbldtl = db.Database.SqlQuery<shipmentdtl>(sSql).ToList();

            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;

                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_STOCK_RM", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_RM");
                if (tbldtl != null)
                {
                    for (var i = 0; i < tbldtl.Count(); i++)
                    {
                        var totalqty = tbldtl.Where(x => x.matoid == tbldtl[i].matoid && x.shipmentwhoid == tbldtl[i].shipmentwhoid).Sum(x => x.shipmentqty);
                        if (!ClassFunction.IsStockAvailable(cmp, sPeriod, tbldtl[i].matoid, tbldtl[i].shipmentwhoid, totalqty, refname))
                        {
                            error = "Shipment Qty for Item Code : <STRONG>" + tbldtl[i].matcode + "</STRONG> must be less than Stock Qty!";
                            break;
                        }
                    }
                }
            }

            if (error == "")
            {
                var iDelAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value));
                var iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", cmp, tblmst.divgroupoid.Value));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Update Shipment
                        sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawno='" + transno + "', shipmentrawmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "' WHERE shipmentrawmststatus='In Approval' AND shipmentrawmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Update Approval
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnshipmentrawmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trndorawdtl SET dorawdtlstatus='' WHERE cmpcode='" + cmp + "' AND dorawdtloid IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trndorawmst SET dorawmststatus='Post' WHERE cmpcode='" + cmp + "' AND dorawmstoid IN (SELECT DISTINCT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            if (tbldtl != null)
                            {
                                for (var i = 0; i < tbldtl.Count(); i++)
                                {
                                    if (tbldtl[i].shipmentonsostatus != "")
                                    {
                                        sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + cmp + "' AND sorawdtloid=" + tbldtl[i].sodtloid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                        sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + cmp + "' AND sorawmstoid=" + tbldtl[i].somstoid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                    sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + cmp + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trndorawdtl WHERE cmpcode='" + cmp + "' AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ") AND dorawdtloid NOT IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + cmp + "' AND sorawmstoid IN (SELECT sorawmstoid FROM QL_trndorawdtl WHERE cmpcode='" + cmp + "' AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ") AND dorawdtloid NOT IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawvalueidr=" + tbldtl[i].shipmentvalueidr + ", shipmentrawvalueusd=" + tbldtl[i].shipmentvalueusd + " WHERE cmpcode='" + cmp + "' AND shipmentrawdtloid=" + tbldtl[i].shipmentdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                               
                                    //Insert conmat
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, iConOid++, "SJRM", "QL_trnshipmentrawdtl", id, tbldtl[i].matoid, refname, tbldtl[i].shipmentwhoid, tbldtl[i].shipmentqty, "Shipment Sheet", transno, userid, "", "", tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, 0, null, tbldtl[i].shipmentdtloid, tblmst.divgroupoid ?? 0,""));

                                   

                                    // Update/Insert QL_stockvalue
                                    var flagstval = "";
                                    QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, iStockValOid, tbldtl[i].matoid, refname, tbldtl[i].shipmentqty * -1, tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, "QL_trnshipmentrawdtl", null, userid, out flagstval);
                                    if (flagstval == "Update")
                                        db.Entry(stockvalue).State = EntityState.Modified;
                                    else
                                    {
                                        db.QL_stockvalue.Add(stockvalue);
                                        iStockValOid++;
                                    }
                                    db.SaveChanges();
                                }
                            }

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, iGlMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Shipment RM|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            decimal glamt = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueidr);
                            decimal glamtidr = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueidr);
                            decimal glamtusd = tbldtl.Sum(x => x.shipmentqty * x.shipmentvalueusd);
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, iGlDtlOid++, glseq++, iGlMstOid, iDelAcctgOid, "D", glamt, transno, "Shipment RM|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnshipmentrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, iGlDtlOid++, glseq++, iGlMstOid, iStockAcctgOid, "C", glamt, transno, "Shipment RM|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnshipmentrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            db.SaveChanges();
                            iGlMstOid++;

                            // Update Last ID
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iConOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iCrdOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlMstOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlDtlOid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        #endregion

        #region Approval Sales Return
        public class sretdtl
        {
            public int sretdtlseq { get; set; }
            public int sretdtloid { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal sretqty { get; set; }
            public int sretwhoid { get; set; }
            public decimal shipmentvalueidr { get; set; }
            public decimal shipmentvalueusd { get; set; }
            public int shipmentdtloid { get; set; }
        }

        private void ApprovalSretFG(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnsretitemmst tblmst = db.QL_trnsretitemmst.Find(cmp, id);

            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var conmtroid = ClassFunction.GenerateID("QL_CONMAT");
            var crdmatoid = ClassFunction.GenerateID("QL_CRDMTR");
            var glmstoid = ClassFunction.GenerateID("QL_TRNGLMST");
            var gldtloid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var stockvaloid = ClassFunction.GenerateID("QL_STOCKVALUE");
            var refname = "FINISH GOOD";

            //Data Detail
            sSql = "SELECT sretitemdtlseq AS sretdtlseq, sretitemdtloid AS sretdtloid, sretd.itemoid AS matoid, itemcode AS matcode, itemlongdesc AS matlongdesc, sretitemqty AS sretqty, sretitemdtlnote AS sretdtlnote, sretitemwhoid AS sretwhoid, shipmentitemvalueidr AS shipmentvalueidr, shipmentitemvalueusd AS shipmentvalueusd, sretd.shipmentitemdtloid AS shipmentdtloid FROM QL_trnsretitemmst sretm INNER JOIN QL_trnsretitemdtl sretd ON sretd.cmpcode=sretm.cmpcode AND sretd.sretitemmstoid=sretm.sretitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=sretd.itemoid INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sretd.cmpcode AND sd.shipmentitemdtloid=sretd.shipmentitemdtloid WHERE sretm.sretitemmstoid=" + id;
            var tbldtl = db.Database.SqlQuery<sretdtl>(sSql).ToList();

            if (action == "Approved")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_STOCK_FG", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_FG");
            }

            if (error == "")
            {
                var iDebetAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", cmp));
                var iCreditAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", cmp));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        // Update Sret
                        sSql = "UPDATE QL_trnsretitemmst SET sretitemno='" + transno + "', sretitemmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "' " + sqlPlus + " WHERE sretitemmststatus='In Approval' AND sretitemmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Update Approval
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsretitemmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlres1='' WHERE cmpcode='" + cmp + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnsretitemdtl WHERE cmpcode='" + cmp + "' AND sretitemmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmstres1='' WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnsretitemmst WHERE cmpcode='" + cmp + "' AND sretitemmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            if (tbldtl != null)
                            {
                                for (var i = 0; i < tbldtl.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND soitemdtlstatus<>'' AND soitemdtloid IN (SELECT dod.soitemdtloid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid WHERE sd.cmpcode='" + cmp + "' AND shipmentitemdtloid=" + tbldtl[i].shipmentdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();
                                    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + cmp + "' AND soitemmststatus<>'Approved' AND soitemmstoid IN (SELECT dod.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid WHERE sd.cmpcode='" + cmp + "' AND shipmentitemdtloid=" + tbldtl[i].shipmentdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();

                                    sSql = "UPDATE QL_trnsretitemdtl SET sretitemvalueidr=" + tbldtl[i].shipmentvalueidr + ", sretitemvalueusd=" + tbldtl[i].shipmentvalueusd + " WHERE sretitemmstoid=" + id + " AND sretitemdtloid=" + tbldtl[i].sretdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();

                                    //Insert conmat
                                    //db.QL_conmat.Add(ClassFunction.InsertConMat(cmp, conmtroid++, "SRETFG", "QL_trnsretitemdtl", id, tbldtl[i].matoid, refname, tbldtl[i].sretwhoid, tbldtl[i].sretqty, "Sales Return Finish Good", transno, userid, "","", tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, 0, null, tbldtl[i].sretdtloid));

                                  
                                    //db.SaveChanges();

                                    // Update/Insert QL_stockvalue
                                    var flagstval = "";
                                    QL_stockvalue stockvalue = ClassFunction.UpdateOrInsertStockValue(cmp, stockvaloid, tbldtl[i].matoid, refname, tbldtl[i].sretqty, tbldtl[i].shipmentvalueidr, tbldtl[i].shipmentvalueusd, "QL_trnsretitemdtl", null, userid, out flagstval);
                                    if (flagstval == "Update")
                                        db.Entry(stockvalue).State = EntityState.Modified;
                                    else
                                    {
                                        db.QL_stockvalue.Add(stockvalue);
                                        stockvaloid++;
                                    }
                                    //db.SaveChanges();
                                }
                                db.SaveChanges();
                            }

                            decimal glamt = tbldtl.Sum(x => x.sretqty * x.shipmentvalueidr);
                            decimal glamtidr = tbldtl.Sum(x => x.sretqty * x.shipmentvalueidr);
                            decimal glamtusd = tbldtl.Sum(x => x.sretqty * x.shipmentvalueusd);

                            // Insert QL_trnglmst
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Sret FG|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, 0, 0, 1, 1, 0, 0)); //glamtidr / glamtusd, glamtidr / glamtusd
                            //db.SaveChanges();

                            //var glseq = 1;

                            // Insert QL_trngldtl
                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iDebetAcctgOid, "D", glamt, transno, "Sret FG|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnsretitemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            //db.SaveChanges();

                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iCreditAcctgOid, "C", glamt, transno, "Sret FG|No. " + transno, "Post", userid, servertime, glamtidr, glamtusd, "QL_trnsretitemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                            //db.SaveChanges();
                            //glmstoid++;

                            // Update Last ID
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_crdmtr'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (stockvaloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion

        #region Approval AR
        private void ApprovalARFG(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnaritemmst tblmst = db.QL_trnaritemmst.Find(cmp, id);
            List<QL_trnaritemdtl> tbldtl = db.Database.SqlQuery<QL_trnaritemdtl>("select * from ql_trnaritemdtl where aritemmstoid='"+id+"'").ToList();

            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sDate = tblmst.aritemdate;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var conaroid = ClassFunction.GenerateID("QL_CONAR");
            var glmstoid = ClassFunction.GenerateID("QL_TRNGLMST");
            var gldtloid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var sotype = db.QL_mstcust.Where(x => x.custoid == tblmst.custoid).Select(x => x.custtype).FirstOrDefault();
            var pengakuanp = db.QL_mstcust.Where(x => x.custoid == tblmst.custoid).Select(x => x.pengakuanp).FirstOrDefault();
            DateTime conardate = tblmst.aritemdate;
            var sVarJual = "VAR_JUAL_LOCAL";
            var cRate = new ClassRate();
            decimal argrandtotal = Convert.ToDecimal(tblmst.aritemgrandtotal);
            decimal artotalamt = Convert.ToDecimal(tblmst.aritemtotalamt - tblmst.aritemtotaldiscdtl);
            decimal artaxamt = Convert.ToDecimal(tblmst.aritemtaxamt);
            var shipmentdtl = (from sd in db.QL_trnshipmentitemdtl join ard in db.QL_trnaritemdtl on new { cmpcode = sd.cmpcode, shipmentitemdtloid = sd.shipmentitemdtloid } equals new { cmpcode = ard.cmpcode, shipmentitemdtloid = ard.shipmentitemdtloid } where ard.cmpcode == cmp && ard.aritemmstoid == id select new { sd.shipmentitemvalueidr, sd.shipmentitemvalueusd, ard.aritemqty }).ToList();
            decimal shipmentamtidr = shipmentdtl.Sum(x => x.shipmentitemvalueidr * x.aritemqty);
            decimal shipmentamtusd = shipmentdtl.Sum(x => x.shipmentitemvalueidr * x.aritemqty);
            var glseq = 1;

            if (pengakuanp != "INVOICE")
            {
                conardate = db.Database.SqlQuery<DateTime>("select shipmentitemdate from ql_trnshipmentitemmst where shipmentitemmstoid =" + tbldtl[0].shipmentitemmstoid + "").FirstOrDefault();
            }

            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //    error = cRate.GetRateDailyLastError;
                //if (cRate.GetRateMonthlyLastError != "")
                //    error = cRate.GetRateMonthlyLastError;
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_HPP_JUAL", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_HPP_JUAL");
                sVarJual = sotype.ToUpper() == "LOCAL" ? "VAR_JUAL_LOCAL" : "VAR_JUAL_EXPORT";
                if (error == "" && !ClassFunction.IsInterfaceExists(sVarJual, cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning(sVarJual);
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_AR", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_AR");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PPN_OUT", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_OUT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");
            }

            if (error == "")
            {
                var iTransitAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value));
                var iHPPAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_HPP_JUAL", cmp, tblmst.divgroupoid.Value));
                var iJualAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface(sVarJual, cmp, tblmst.divgroupoid.Value));
                var iARAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", cmp, tblmst.divgroupoid.Value));
                var iPPNAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", cmp));
                var iRoundAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Update Sret  , aritemdate='"+ conardate.ToString("MM/dd/yyyy") +"'
                        sSql = "UPDATE QL_trnaritemmst SET aritemno='" + transno + "', aritemmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += ", rateoid=" + cRate.GetRateDailyOid + ", aritemratetoidr='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', aritemratetousd='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', rate2oid=" + cRate.GetRateMonthlyOid + ", aritemrate2toidr='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', aritemrate2tousd='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "'";
                        }
                        sSql += " WHERE aritemmststatus='In Approval' AND aritemmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Update Approval
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnaritemmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl WHERE cmpcode='" + cmp + "' AND aritemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Approved' WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl WHERE cmpcode='" + cmp + "' AND aritemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            //Insert QL_conar
                            db.QL_conar.Add(ClassFunction.InsertConAR(cmp, conaroid++, "QL_trnaritemmst", id, 0, tblmst.custoid, iARAcctgOid, "Post", "ARFG", conardate, ClassFunction.GetDateToPeriodAcctg(conardate), 0, DateTime.Parse("01/01/1900"), "", 0, DateTime.Parse("01/01/1900"), argrandtotal, 0, "", "", "", "", userid, servertime, userid, servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, 0, argrandtotal * cRate.GetRateMonthlyUSDValue, 0, 0, "", tblmst.divgroupoid ?? 0));
                            if (shipmentamtidr > 0 && shipmentamtusd > 0)
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(conardate.ToString("MM/dd/yyyy")), sPeriod, "A/R FG|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, 1, 1, 1, 1, shipmentamtidr / shipmentamtusd, shipmentamtidr / shipmentamtusd, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // Insert GL DTL 1st
                                // D : HPP Penjualan
                                glseq = 1;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iHPPAcctgOid, "D", shipmentamtidr, transno, "A/R FG|No. " + transno, "Post", userid, servertime, shipmentamtidr, shipmentamtusd, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // C : Stok Dalam Perjalanan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iTransitAcctgOid, "C", shipmentamtidr, transno, "A/R FG|No. " + transno, "Post", userid, servertime, shipmentamtidr, shipmentamtusd, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                glmstoid++;
                            }
                            if (argrandtotal > 0)
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(conardate.ToString("MM/dd/yyyy")), sPeriod, "A/R FG|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();

                                decimal dSelIDR = (argrandtotal * cRate.GetRateMonthlyIDRValue) - ((artotalamt * cRate.GetRateMonthlyIDRValue) + (artaxamt * cRate.GetRateMonthlyIDRValue));
                                decimal dSelUSD = (argrandtotal * cRate.GetRateMonthlyUSDValue) - ((artotalamt * cRate.GetRateMonthlyUSDValue) + (artaxamt * cRate.GetRateMonthlyUSDValue));

                                glseq = 1;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iARAcctgOid, "D", argrandtotal, transno, "A/R FG|No. " + transno, "Post", userid, servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, argrandtotal * cRate.GetRateMonthlyUSDValue, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // D/C : Pembulatan IDR
                                if (dSelIDR != 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iRoundAcctgOid, (dSelIDR > 0 ? "C" : "D"), 0, transno, "Pembulatan IDR A/R FG|No. " + transno, "Post", userid, servertime, Math.Abs(dSelIDR), 0, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                    db.SaveChanges();
                                }
                                // D/C : Pembulatan USD
                                if (dSelUSD != 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iRoundAcctgOid, (dSelUSD > 0 ? "C" : "D"), 0, transno, "Pembulatan USD A/R FG|No. " + transno, "Post", userid, servertime, 0, Math.Abs(dSelUSD), "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                    db.SaveChanges();
                                }
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iJualAcctgOid, "C", artotalamt, transno, "A/R FG|No. " + transno, "Post", userid, servertime, artotalamt * cRate.GetRateMonthlyIDRValue, artotalamt * cRate.GetRateMonthlyUSDValue, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                if (artaxamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iPPNAcctgOid, "C", artaxamt, transno, "A/R FG|No. " + transno, "Post", userid, servertime, artaxamt * cRate.GetRateMonthlyIDRValue, artaxamt * cRate.GetRateMonthlyUSDValue, "QL_trnaritemmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                    db.SaveChanges();
                                }
                                glmstoid++;
                            }

                            // Update Last ID
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conaroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalARRaw(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            QL_trnarrawmst tblmst = db.QL_trnarrawmst.Find(cmp, id);
            List<QL_trnarrawdtl> tbldtl = db.Database.SqlQuery<QL_trnarrawdtl>("select * from ql_trnarrawdtl where arrawmstoid='" + id + "'").ToList();
            var servertime = ClassFunction.GetServerTime();
            var userid = Session["UserID"].ToString();
            var sDate = tblmst.arrawdate;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var conaroid = ClassFunction.GenerateID("QL_CONAR");
            var glmstoid = ClassFunction.GenerateID("QL_TRNGLMST");
            var gldtloid = ClassFunction.GenerateID("QL_TRNGLDTL");
            var sotype = db.QL_mstcust.Where(x => x.custoid == tblmst.custoid).Select(x => x.custtype).FirstOrDefault();
            var pengakuanp = db.QL_mstcust.Where(x => x.custoid == tblmst.custoid).Select(x => x.pengakuanp).FirstOrDefault();
            DateTime conardate = tblmst.arrawdate;
            var sVarJual = "VAR_JUAL_LOCAL";
            var cRate = new ClassRate();
            decimal argrandtotal = Convert.ToDecimal(tblmst.arrawgrandtotal);
            decimal artotalamt = Convert.ToDecimal(tblmst.arrawtotalamt - tblmst.arrawtotaldiscdtl);
            decimal artaxamt = Convert.ToDecimal(tblmst.arrawtaxamt);
            var shipmentdtl = (from sd in db.QL_trnshipmentrawdtl join ard in db.QL_trnarrawdtl on new { cmpcode = sd.cmpcode, shipmentrawdtloid = sd.shipmentrawdtloid } equals new { cmpcode = ard.cmpcode, shipmentrawdtloid = ard.shipmentrawdtloid } where ard.cmpcode == cmp && ard.arrawmstoid == id select new { sd.shipmentrawvalueidr, sd.shipmentrawvalueusd, ard.arrawqty }).ToList();
            decimal shipmentamtidr = shipmentdtl.Sum(x => x.shipmentrawvalueidr * x.arrawqty);
            decimal shipmentamtusd = shipmentdtl.Sum(x => x.shipmentrawvalueidr * x.arrawqty);
            var glseq = 1;
            if (pengakuanp != "INVOICE")
            {
                conardate = db.Database.SqlQuery<DateTime>("select shipmentrawdate from ql_trnshipmentrawmst where shipmentrawmstoid =" + tbldtl[0].shipmentrawmstoid + "").FirstOrDefault();
            }

            if (action == "Approved")
            {
                cRate.SetRateValue(tblmst.curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    error = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    error = cRate.GetRateMonthlyLastError;
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_HPP_JUAL", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_HPP_JUAL");
                sVarJual = sotype.ToUpper() == "LOCAL" ? "VAR_JUAL_LOCAL" : "VAR_JUAL_EXPORT";
                if (error == "" && !ClassFunction.IsInterfaceExists(sVarJual, cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning(sVarJual);
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_AR", cmp, tblmst.divgroupoid.Value))
                    error = ClassFunction.GetInterfaceWarning("VAR_AR");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_PPN_OUT", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_OUT");
                if (error == "" && !ClassFunction.IsInterfaceExists("VAR_ROUNDING", cmp))
                    error = ClassFunction.GetInterfaceWarning("VAR_ROUNDING");
            }

            if (error == "")
            {
                var iTransitAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", cmp, tblmst.divgroupoid.Value));
                var iHPPAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_HPP_JUAL", cmp, tblmst.divgroupoid.Value));
                var iJualAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface(sVarJual, cmp, tblmst.divgroupoid.Value));
                var iARAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", cmp, tblmst.divgroupoid.Value));
                var iPPNAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", cmp));
                var iRoundAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ROUNDING", cmp));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Update Sret
                        sSql = "UPDATE QL_trnarrawmst SET arrawno='" + transno + "', arrawmststatus='" + action + "', approvaluser='" + userid + "', approvaldatetime='" + servertime + "', upduser='" + userid + "', updtime='" + servertime + "'";
                        if (action == "Approved")
                        {
                            sSql += ", rateoid=" + cRate.GetRateDailyOid + ", arrawratetoidr='" + ClassFunction.Left(cRate.GetRateDailyIDRValue.ToString(), 50) + "', arrawratetousd='" + ClassFunction.Left(cRate.GetRateDailyUSDValue.ToString(), 50) + "', rate2oid=" + cRate.GetRateMonthlyOid + ", arrawrate2toidr='" + ClassFunction.Left(cRate.GetRateMonthlyIDRValue.ToString(), 50) + "', arrawrate2tousd='" + ClassFunction.Left(cRate.GetRateMonthlyUSDValue.ToString(), 50) + "'";
                        }
                        sSql += " WHERE arrawmststatus='In Approval' AND arrawmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Update Approval
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnarrawmst' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawdtlstatus='' WHERE cmpcode='" + cmp + "' AND shipmentrawdtloid IN (SELECT shipmentrawdtloid FROM QL_trnarrawdtl WHERE cmpcode='" + cmp + "' AND arrawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawmststatus='Approved' WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid IN (SELECT shipmentrawmstoid FROM QL_trnarrawdtl WHERE cmpcode='" + cmp + "' AND arrawmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Approved")
                        {
                            //Insert QL_conar
                            db.QL_conar.Add(ClassFunction.InsertConAR(cmp, conaroid++, "QL_trnarrawmst", id, 0, tblmst.custoid, iARAcctgOid, "Post", "ARRM", conardate, ClassFunction.GetDateToPeriodAcctg(conardate), 0, DateTime.Parse("01/01/1900"), "", 0, DateTime.Parse("01/01/1900"), argrandtotal, 0, "", "", "", "", userid, servertime, userid, servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, 0, argrandtotal * cRate.GetRateMonthlyUSDValue, 0, 0, "", tblmst.divgroupoid ?? 0));
                            if (shipmentamtidr > 0 && shipmentamtusd > 0)
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "A/R RM|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, 1, 1, 1, 1, shipmentamtidr / shipmentamtusd, shipmentamtidr / shipmentamtusd, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // Insert GL DTL 1st
                                // D : HPP Penjualan
                                glseq = 1;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iHPPAcctgOid, "D", shipmentamtidr, transno, "A/R RM|No. " + transno, "Post", userid, servertime, shipmentamtidr, shipmentamtusd, "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // C : Stok Dalam Perjalanan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iTransitAcctgOid, "C", shipmentamtidr, transno, "A/R RM|No. " + transno, "Post", userid, servertime, shipmentamtidr, shipmentamtusd, "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                glmstoid++;
                            }
                            if (argrandtotal > 0)
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmp, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "A/R RM|No. " + transno, "Post", servertime, userid, servertime, userid, servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();

                                decimal dSelIDR = (argrandtotal * cRate.GetRateMonthlyIDRValue) - ((artotalamt * cRate.GetRateMonthlyIDRValue) + (artaxamt * cRate.GetRateMonthlyIDRValue));
                                decimal dSelUSD = (argrandtotal * cRate.GetRateMonthlyUSDValue) - ((artotalamt * cRate.GetRateMonthlyUSDValue) + (artaxamt * cRate.GetRateMonthlyUSDValue));

                                glseq = 1;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iARAcctgOid, "D", argrandtotal, transno, "A/R RM|No. " + transno, "Post", userid, servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, argrandtotal * cRate.GetRateMonthlyUSDValue, "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                // D/C : Pembulatan IDR
                                if (dSelIDR != 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iRoundAcctgOid, (dSelIDR > 0 ? "C" : "D"), 0, transno, "Pembulatan IDR A/R RM|No. " + transno, "Post", userid, servertime, Math.Abs(dSelIDR), 0, "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                    db.SaveChanges();
                                }
                                // D/C : Pembulatan USD
                                if (dSelUSD != 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iRoundAcctgOid, (dSelUSD > 0 ? "C" : "D"), 0, transno, "Pembulatan USD A/R RM|No. " + transno, "Post", userid, servertime, 0, Math.Abs(dSelUSD), "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                    db.SaveChanges();
                                }
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iJualAcctgOid, "C", artotalamt, transno, "A/R RM|No. " + transno, "Post", userid, servertime, artotalamt * cRate.GetRateMonthlyIDRValue, artotalamt * cRate.GetRateMonthlyUSDValue, "QL_trnarrawmst " + id.ToString(), null, null, null, 0, tblmst.divgroupoid ?? 0));
                                db.SaveChanges();
                                if (artaxamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmp, gldtloid++, glseq++, glmstoid, iPPNAcctgOid, "C", artaxamt, transno, "A/R RM|No. " + transno, "Post", userid, servertime, artaxamt * cRate.GetRateMonthlyIDRValue, artaxamt * cRate.GetRateMonthlyUSDValue, "QL_trnarrawmst " + id.ToString(), null, null, null, 0,tblmst.divgroupoid??0));
                                    db.SaveChanges();
                                }
                                glmstoid++;
                            }

                            // Update Last ID
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conaroid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        #endregion

        #region Approval Inventory        
        private void ApprovalStockAdj(int id, string cmp, string transno, string action, out string error)
        {
            error = "";
            var servertime = ClassFunction.GetServerTime();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var sqlPlus = "";

            List<stockadj> dtDtl = (List<stockadj>)Session["QL_trnstockadj"];

            if (action == "Approved")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_FG");
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_ADJUSTMENT", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_ADJUSTMENT");

                //Set Avg Value
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtDtl[i].refno))
                        dtDtl[i].refno = "";

                    decimal dQtyAdj = dtDtl[i].stockadjqty; string sType = "IN";
                    if (dtDtl[i].stockadjqty < 0)
                    {
                        dQtyAdj = dtDtl[i].stockadjqty * -1;
                        sType = "OUT";
                    }
                    decimal sValue = dtDtl[i].stockvalue;
                    decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dQtyAdj, sValue, sType);
                    dtDtl[i].stockvalue = sAvgValue;
                }
            }
            error = "";
            if (error == "")
            {
                var iAcctgOidAdj = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_ADJUSTMENT", CompnyCode));
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Update QL_APP
                        sSql = "UPDATE QL_approval SET statusrequest='" + action + "', event='" + action + "', approvaldate='" + servertime + "' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnstockadj' AND oid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);

                        if (action != "Approved")
                        {
                            if (action == "Revised")
                            {
                                sqlPlus = " ,revisednote='" + Session["rvnote"].ToString() + "', revisedtime='" + servertime + "', reviseduser='" + Session["UserID"].ToString() + "'";
                            }
                            else if (action == "Rejected")
                            {
                                sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                            }
                            // Update QL_trnstockadj
                            sSql = "UPDATE QL_trnstockadj SET stockadjno='" + transno + "',stockadjstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE stockadjstatus='In Approval' AND resfield1=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (action == "Approved")
                        {
                            // Insert QL_trnglmst
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Stock Opname|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            //db.SaveChanges();

                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                // Update QL_trnstockadj
                                sSql = "UPDATE QL_trnstockadj SET stockadjno='" + transno + "',stockadjstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', stockadjamtidr=" + dtDtl[i].stockvalue + ", stockadjamtusd=0 WHERE stockadjstatus='In Approval' AND resfield1=" + id + " AND stockadjoid=" + dtDtl[i].stockadjoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                // Insert QL_conmat
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "ADJ", "QL_trnstockadj", dtDtl[i].resfield1, dtDtl[i].itemoid, dtDtl[i].refname, dtDtl[i].mtrwhoid, dtDtl[i].stockadjqty, "Stock Opname", "Stock Opname No. " + transno, Session["UserID"].ToString(), dtDtl[i].refno,"", dtDtl[i].stockvalue, 0, 0, null, dtDtl[i].stockadjoid, 0,""));
                                db.SaveChanges();

                                //Insert QL_crdmtr IN
                                sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " + dtDtl[i].stockadjqty + ", saldoakhir=saldoakhir + " + dtDtl[i].stockadjqty + ", lasttranstype='QL_trnstockadj', lasttransdate='" + ClassFunction.GetServerTime() + "', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + CompnyCode + "' AND refoid=" + dtDtl[i].itemoid + " AND refname='" + dtDtl[i].refname + "' AND mtrwhoid=" + dtDtl[i].mtrwhoid + " AND periodacctg='" + sPeriod + "'";
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" + CompnyCode + "', " + crdmatoid++ + ", '" + sPeriod + "', " + dtDtl[i].itemoid + ", '" + dtDtl[i].refname + "', " + dtDtl[i].mtrwhoid + ", " + dtDtl[i].stockadjqty + ", 0, 0, 0, 0, " + dtDtl[i].stockadjqty + ", 'QL_trnstockadj', '" + ClassFunction.GetServerTime() + "', '" + Session["UserID"].ToString() + "', CURRENT_TIMESTAMP, '" + Session["UserID"].ToString() + "', CURRENT_TIMESTAMP, '', '1/1/1900')";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                db.SaveChanges();

                                //var flagcrd = "";
                                //QL_crdmtr crdmtr = ClassFunction.UpdateOrInsertCrdMtr(CompnyCode, crdmatoid++, dtDtl[i].itemoid, dtDtl[i].refname, dtDtl[i].mtrwhoid, dtDtl[i].stockadjqty, 0, "QL_trnstockadj", Session["UserID"].ToString(), null, dtDtl[i].stockadjqty * dtDtl[i].stockvalue, 0, out flagcrd);
                                //if (flagcrd == "Update")
                                //    db.Entry(crdmtr).State = EntityState.Modified;
                                //else
                                //{
                                //    db.QL_crdmtr.Add(crdmtr);
                                //    crdmatoid++;
                                //}
                                //db.SaveChanges();

                                var sVar = "";
                                if (dtDtl[i].refname == "RAW MATERIAL")
                                {
                                    sVar = "VAR_STOCK_RM";
                                }
                                else if (dtDtl[i].refname == "GENERAL MATERIAL")
                                {
                                    sVar = "VAR_STOCK_GM";
                                }
                                else if (dtDtl[i].refname == "SPARE PART")
                                {
                                    sVar = "VAR_STOCK_SP";
                                }
                                else if (dtDtl[i].refname == "FINISH GOOD")
                                {
                                    sVar = "VAR_STOCK_FG";
                                }

                                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface(sVar, CompnyCode));

                                //var dbcrFG = "D"; var dbcrADJ = "C";
                                //if (dtDtl[i].stockadjqty < 0)
                                //{
                                //    dbcrFG = "C"; dbcrADJ = "D";
                                //}
                                //var glseq = 1;
                                // Insert QL_trngldtl
                                //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, dbcrFG, dtDtl[i].stockvalue, transno, "Stock Opname|No. " + transno, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].stockvalue, 0, "QL_trnstockadj " + id.ToString(), null, null, null, 0));
                                //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAdj, dbcrADJ, dtDtl[i].stockvalue, transno, "Stock Opname|No. " + transno, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].stockvalue, 0, "QL_trnstockadj " + id.ToString(), null, null, null, 0));
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (crdmatoid - 1) + " WHERE tablename='QL_crdmtr'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    //catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    //{
                    //    objTrans.Rollback();
                    //    var err = "";
                    //    foreach (var eve in e.EntityValidationErrors)
                    //    {
                    //        err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                    //        foreach (var ve in eve.ValidationErrors)
                    //        {
                    //            err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                    //        }
                    //    }
                    //    ModelState.AddModelError("", err);
                    //}
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion
    }
}