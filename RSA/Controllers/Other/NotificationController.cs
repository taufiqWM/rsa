﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using RSA.Models.DB;

namespace RSA.Controllers
{
    public class NotificationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class notifclass
        {
            public int notifqty { get; set; }
            public string notiftype { get; set; }
            public string notiftitle { get; set; }
            public string notifmsg { get; set; }
            public string notifaction { get; set; }
            public string notifctrl { get; set; }
            public string notifcss { get; set; }
            public string notiflink { get; set; }
        }

        [HttpPost]
        public ActionResult GetNotification(string userID)
        {
            List<notifclass> dtnotif = null;

            sSql = "SELECT COUNT(DISTINCT a.oid) notifqty, 'Approval' notiftype, apppersonnote notiftitle, 'are waiting for your approval' notifmsg, 'Index' notifaction, 'WaitingAction' notifctrl, 'fa fa-envelope-o' notifcss, ('/' + a.tablename) notiflink FROM QL_approval a INNER JOIN QL_approvalperson ap ON a.tablename=ap.tablename WHERE a.approvaluser='" + userID + "' AND a.statusrequest='New' AND a.event='In Approval' GROUP BY apppersonnote, a.tablename ORDER BY apppersonnote";
            dtnotif = db.Database.SqlQuery<notifclass>(sSql).ToList();

            for (int i = 0; i < dtnotif.Count; i++)
            {
                dtnotif[i].notiflink = Url.Action(dtnotif[i].notifaction + dtnotif[i].notiflink.ToLower(), dtnotif[i].notifctrl);
                dtnotif[i].notifmsg = dtnotif[i].notifqty.ToString() + " " + dtnotif[i].notiftitle + " " + dtnotif[i].notifmsg;
            }

            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "profile");

            return View();
        }
    }
}