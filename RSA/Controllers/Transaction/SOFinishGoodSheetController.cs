﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class SOFinishGoodSheetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class soitemmst
        {
            public string cmpcode { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string soitemmststatus { get; set; }
            public string soitemmstnote { get; set; }
            public string divname { get; set; }
            public string sotype { get; set; }
        }

        public class soitemdtl
        {
            public int soitemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string itemnote { get; set; }
            public decimal stockqty { get; set; }
            public decimal soitemqty { get; set; }
            public int soitemunitoid { get; set; }
            public string soitemunit { get; set; }
            public decimal soitemlastprice { get; set; }
            public decimal soitemprice { get; set; }
            public decimal soitemprice_intax { get; set; }
            public decimal soitemdtlamt { get; set; }
            public decimal soitemdtlamt_intax { get; set; }
            public string soitemdtldisctype { get; set; }
            public decimal soitemdtldiscvalue { get; set; }
            public decimal soitemdtldiscvalue_intax { get; set; }
            public decimal soitemdtldiscamt { get; set; }
            public decimal soitemdtldiscamt_intax { get; set; }
            public decimal soitemdtlnetto { get; set; }
            public decimal soitemdtlnetto_intax { get; set; }
            public string soitemdtlnote { get; set; }
            public string sub1 { get; set; }
            public string sub2 { get; set; }
            public string sub3 { get; set; }
            public string sub4 { get; set; }
            public string sub5 { get; set; }
            public string tipesheet { get; set; }
            public int fluteoid { get; set; }
            public string trim { get; set; }
            public decimal psheet { get; set; }
            public decimal lsheet { get; set; }
            public string pcspb { get; set; }
            public string cr1 { get; set; }
            public string cr2 { get; set; }
            public string cr3 { get; set; }
            public string cr4 { get; set; }
            public string cr5 { get; set; }
            public decimal toleransi { get; set; }
            public decimal soitemdtlpriceinc { get; set; }
            public decimal soitemdtldiscmarginamt { get; set; }

        }

        [HttpPost]
        public ActionResult getmargin(decimal psheet, decimal lsheet, decimal trim, int fluteoid, decimal s1, decimal s2, decimal s3, decimal s4, decimal s5, decimal discp, decimal hargasat,string tw)
        {
            var lmesin = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='LEBAR MESIN'").FirstOrDefault());
            decimal luas = Math.Round((psheet * lsheet) / 1000000, 4);
            decimal vout = Math.Floor(lmesin / lsheet);
            if (vout > 7)
            {
                vout = 7;
            }
            decimal varlbrbhn = Math.Ceiling(((lsheet * vout) + trim) / 50);
            decimal lbrbhnrecm = varlbrbhn * 50;

            decimal lbrukuran = Math.Round(lbrbhnrecm / vout, 2);

            decimal hargaindex = 0;
            if (tw == "DW")
            {
                hargaindex = db.Database.SqlQuery<decimal>("select isnull(hargaindex,0) from ql_msthargaindex where fluteoid='" + fluteoid + "' and sub1='" + s1 + "' and sub2='" + s2 + "' and sub3='" + s3 + "'  and isnull(sub4='" + s4 + "' and sub5='" + s5 + "' and activeflag='ACTIVE'").FirstOrDefault();
            }
            else
            {
                hargaindex = db.Database.SqlQuery<decimal>("select isnull(hargaindex,0) from ql_msthargaindex where fluteoid='" + fluteoid + "' and sub1='" + s1 + "' and sub2='" + s2 + "' and sub3='" + s3 + "' and activeflag='ACTIVE' ").FirstOrDefault();
            }
            decimal hargaexc = hargaindex * luas;
            decimal hargainc = hargaexc + (discp * hargaexc);
            decimal discmar = Math.Round(((hargasat - hargaexc) / hargaexc) * 100, 2);
            List<soitemdtl> sodtl = new List<soitemdtl>();
            sodtl.Add(new soitemdtl { soitemdtlpriceinc = hargainc, soitemdtldiscmarginamt = discmar});

            return Json(sodtl, JsonRequestBehavior.AllowGet);
        }

        public class mstaccount
        {
            public int accountoid { get; set; }
            public string accountdesc { get; set; }
        }

        public class container
        {
            public int containerseq { get; set; }
            public int containeroid { get; set; }
            public string containertype { get; set; }
            public decimal soitemcontqty { get; set; }
            public decimal soitemcontpct { get; set; }

        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal soratetousd { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal sorate2tousd { get; set; }
        }

        private void InitDDL(QL_trnsoitemmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var sopaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.soitempaytypeoid);
            ViewBag.sopaytypeoid = sopaytypeoid;

            sSql = "select * from QL_mstperson p inner join QL_mstlevel l on p.leveloid=l.leveloid and leveldesc='SALES' where p.cmpcode='" + CompnyCode + "' order by personname";
            var salesoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personname", "personname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            sSql = "SELECT * FROM QL_mstaccount a WHERE a.cmpcode='" + tbl.cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY a.accountno";
            var accountoid = new SelectList(db.Database.SqlQuery<QL_mstaccount>(sSql).ToList(), "accountoid", "accountno", tbl.accountoid);
            ViewBag.accountoid = accountoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            //sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=2 ORDER BY gendesc";
            if (tbl.tipewall == "DW")
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=3 ORDER BY gendesc";
            }
            var fluteoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.fluteoid);
            ViewBag.fluteoid = fluteoid;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub1 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub1);
            ViewBag.sub1 = sub1;
            ViewBag.sub2 = sub1;
            ViewBag.sub3 = sub1;
            ViewBag.sub4 = sub1;
            ViewBag.sub5 = sub1;

        }

        [HttpPost]
        public ActionResult InitDDLFlute()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=2 ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Getflute(string fl)
        {
            List<QL_mstgen> objgen = new List<QL_mstgen>();
            if (fl == "SW")
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=2 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            else
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=3 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            objgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objgen, "genoid", "gendesc", 0);

            return Json(DDLCat2);
        }

        [HttpPost]
        public ActionResult Getsub1(string sub1)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub1 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub2(string sub2)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub2 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub3(string sub3)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub3 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub4(string sub4)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub4 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub5(string sub5)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub5 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }


        [HttpPost]
        public ActionResult InitDDLSub()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
      

        [HttpPost]
        public ActionResult InitDDLDivision(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<mstaccount> tbl = new List<mstaccount>();
            sSql = "SELECT a.accountoid, b.bankname+' - '+a.accountno accountdesc  FROM QL_mstaccount a INNER JOIN QL_mstbank b ON b.bankoid = a.bankoid WHERE a.cmpcode='" + cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY b.bankname+a.accountno";
            tbl = db.Database.SqlQuery<mstaccount>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetContainerData()
        {
            List<container> tbl = new List<container>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY gendesc) AS INT) AS containerseq, genoid AS containeroid, gendesc AS containertype, 0.0 AS soitemcontqty, 0.0 AS soitemcontpct FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CONTAINER TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<container>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust c WHERE c.cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND custoid>0 ORDER BY custname DESC";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }
                
                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.soratetousd = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.sorate2tousd = cRate.GetRateMonthlyUSDValue;
                }else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int curroid, int custoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<soitemdtl> tbl = new List<soitemdtl>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdmtr crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND refoid=itemoid AND closingdate='01/01/1900'), 0.0) AS stockqty, 0.0 AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' /*AND m.itemoid IN (SELECT DISTINCT sod.itemoid FROM QL_trnsoitemdtl sod WHERE sod.upduser='" + Session["UserID"].ToString() + "')*/ ORDER BY itemcode";

            tbl = db.Database.SqlQuery<soitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<soitemdtl> dtDtl)
        {
            Session["QL_trnsoitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsCont(List<container> dtDtlCont)
        {
            Session["QL_trnsoitemcont"] = dtDtlCont;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsoitemdtl"] == null)
            {
                Session["QL_trnsoitemdtl"] = new List<soitemdtl>();
            }

            List<soitemdtl> dataDtl = (List<soitemdtl>)Session["QL_trnsoitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailContainer()
        {
            if (Session["QL_trnsoitemcont"] == null)
            {
                Session["QL_trnsoitemcont"] = new List<container>();
            }

            List<container> dataDtlCont = (List<container>)Session["QL_trnsoitemcont"];
            return Json(dataDtlCont, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsoitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.custpaytype = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid=" + tbl.soitempaytypeoid + "").FirstOrDefault();
        }

        // GET: SOitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.soitemmstoid, som.soitemno, som.soitemdate, c.custname, som.soitemmststatus, som.soitemmstnote, div.divname, som.soitemtype sotype FROM QL_TRNSOitemMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND soitemdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND soitemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND soitemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND soitemdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND soitemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND soitemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND soitemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND soitemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND soitemmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else

            {
                sSql += " AND soitemmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " AND isnull(som.sssflag,0)=0 ORDER BY som.soitemdate DESC, som.soitemmstoid DESC";

            List<soitemmst> dt = db.Database.SqlQuery<soitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: SOitemMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnsoitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.soitemmstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                tbl.soitemdate = ClassFunction.GetServerTime();
                tbl.socustrefdate = ClassFunction.GetServerTime();
                tbl.soitemetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.lastrevtime = new DateTime(1900, 1, 1);
                tbl.soitemmststatus = "In Process";
                tbl.ismadetoorder = true;
                tbl.isexcludetax = true;
                
                Session["QL_trnsoitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnsoitemmst.Find(cmp, id);
                string sAdd = "";
                //if (tbl.isexcludetax)
                //{
                //    sAdd = "";
                //}

                sSql = "SELECT sod.soitemdtlseq, sod.itemoid, itemcode, m.itemlongdesc, sod.soitemqty, sod.soitemunitoid, g.gendesc AS soitemunit, sod.soitemprice" + sAdd + " soitemprice, sod.soitemprice_intax, sod.soitemdtlamt" + sAdd + " soitemdtlamt, sod.soitemdtldisctype, soitemdtldiscvalue" + sAdd + " soitemdtldiscvalue, soitemdtldiscamt" + sAdd + " soitemdtldiscamt, soitemdtlnetto" + sAdd + " soitemdtlnetto, sod.soitemdtlnote, ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstitemprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstitemprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), 0.0)) AS soitemlastprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), 0.0)) AS lastcurroid FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.soitemunitoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=sod.cmpcode AND som.soitemmstoid=sod.soitemmstoid WHERE sod.soitemmstoid=" + id + " ORDER BY sod.soitemdtlseq";
                Session["QL_trnsoitemdtl"] = db.Database.SqlQuery<soitemdtl>(sSql).ToList();

                List<container> tblcont = new List<container>();
                sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY gendesc) AS INT) AS containerseq, c.genoid AS containeroid, c.gendesc AS containertype, ISNULL(socont.soitemcontqty,0) AS soitemcontqty , ISNULL(socont.soitemcontpct,0) AS soitemcontpct FROM QL_mstgen c LEFT JOIN QL_trnsoitemcont socont ON c.genoid=socont.containeroid AND socont.soitemmstoid=" + id + " AND socont.cmpcode='" + tbl.cmpcode + "' WHERE c.cmpcode='" + CompnyCode + "' AND c.gengroup='CONTAINER TYPE' ORDER BY c.gendesc";
                Session["QL_trnsoitemcont"] = db.Database.SqlQuery<container>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: SOitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsoitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.soitemno == null)
                tbl.soitemno = "";

            if (tbl.soitemdate > tbl.soitemetd)
            {
                ModelState.AddModelError("", "ETD must be more than SO DATE!");
            }

            List<soitemdtl> dtDtl = (List<soitemdtl>)Session["QL_trnsoitemdtl"];
            //List<container> dtDtlCont = (List<container>)Session["QL_trnsoitemcont"];

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.soitemdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            msg = "";
            if (msg != "")
                ModelState.AddModelError("", msg);

            if (dtDtl == null)
            {
                ModelState.AddModelError("", "Please fill detail data!");
            }
            else
            {
                if (dtDtl.Count <= 0)
                {
                    ModelState.AddModelError("", "Please fill detail data!");
                }
                else
                {
                    if (dtDtl != null)
                    {
                        if (dtDtl.Count > 0)
                        {
                            for (var i = 0; i < dtDtl.Count; i++)
                            {
                                if (tbl.ismadetoorder)
                                {
                                    if (tbl.soitemmststatus == "In Approval")
                                    {
                                        //sSql = "SELECT COUNT(*) FROM QL_mstitem m WHERE m.cmpcode='" + tbl.cmpcode + "' AND m.activeflag='ACTIVE' AND m.itemoid IN (SELECT itemoid FROM QL_mstitemdtl2 WHERE cmpcode='" + tbl.cmpcode + "' AND itemoid=" + dtDtl[i].itemoid + " AND custoid=" + tbl.custoid + ")";
                                        //if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() < 0)
                                        //{
                                        //    ModelState.AddModelError("", "Kode " + dtDtl[i].itemcode + "  Belum Di buatkan External Name!");
                                        //}
                                    }
                                    if (dtDtl[i].soitemprice <= 0)
                                    {
                                        ModelState.AddModelError("", "SO PRICE for code " + dtDtl[i].itemcode + " must more than 0");
                                    }
                                    if (dtDtl[i].soitemdtldisctype == "P")
                                    {
                                        if (dtDtl[i].soitemdtldiscvalue < 0 || dtDtl[i].soitemdtldiscvalue > 100)
                                            ModelState.AddModelError("", dtDtl[i].itemcode + " Percentage of DETAIL DISC must be between 0 and 100!");
                                    }
                                    else
                                    {
                                        if (dtDtl[i].soitemdtldiscvalue < 0 || dtDtl[i].soitemdtldiscvalue > dtDtl[i].soitemdtldiscamt)
                                            ModelState.AddModelError("", dtDtl[i].itemcode + " Amount of DETAIL DISC must be between 0 and " + string.Format("{0:#,0.00}", dtDtl[i].soitemdtldiscamt) + " (DETAIL AMOUNT)!");
                                    }
                                }
                                if (dtDtl[i].soitemqty <= 0)
                                {
                                    ModelState.AddModelError("", "SO QTY for code " + dtDtl[i].itemcode + " must more than 0");
                                }
                            }
                        }
                    }
                }
            }
            if (tbl.ismadetoorder)
            {
                //if (dtDtlCont == null)
                //{
                //    ModelState.AddModelError("", "CONTAINER DATA is Empty!");
                //}
                if (tbl.soitemportship == null)
                {
                    //ModelState.AddModelError("", "Please Fill PORT OF SHIP Field!");
                    tbl.soitemportship = "";
                }
                if (tbl.soitemportdischarge == null)
                {
                    //ModelState.AddModelError("", "Please Fill PORT OF DISCHARGE Field!");
                    tbl.soitemportdischarge = "";
                }
            }else
            {
                if (tbl.soitemportship == null)
                {
                    tbl.soitemportship = "";
                }
                if (tbl.soitemportdischarge == null)
                {
                    tbl.soitemportdischarge = "";
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.soitemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvaluser + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsoitemdtl");
                var socontoid = ClassFunction.GenerateID("QL_trnsoitemcont");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                if (tbl.custoid == 0)
                    tbl.custoid = -1;
                if (tbl.curroid == 0)
                    tbl.curroid = 1;
                if (tbl.accountoid == 0)
                    tbl.accountoid = -1;
                tbl.soitemmstdisctype = "P";
                tbl.soitemmstdiscvalue = 0;
                tbl.soitemmstdiscamt = 0;
                tbl.soiteminvoiceval = "";
                tbl.accountoid = 0;
                tbl.groupoid = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;
                        tbl.revstatus = tbl.revstatus == null ? "" : tbl.revstatus;
                        tbl.lastrevuser = tbl.lastrevuser == null ? "" : tbl.lastrevuser;
                        tbl.lastrevtime = tbl.lastrevtime == null ? new DateTime(1900, 1, 1) : tbl.lastrevtime;
                        tbl.soitempino = tbl.soitempino == null ? "" : tbl.soitempino;
                        tbl.soitemreqshipdate = new DateTime(1900, 1, 1);
                        tbl.soitemrevetd = new DateTime(1900, 1, 1);

                        if (action == "New Data")
                        {
                            if (db.QL_trnsoitemmst.Find(tbl.cmpcode, tbl.soitemmstoid) != null)
                                tbl.soitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.soitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.soitembank = "18";
                            db.QL_trnsoitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.soitemmstoid + " WHERE tablename='QL_trnsoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            tbl.soitembank = "18";
                            db.SaveChanges();

                            var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == tbl.soitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtlcont = db.QL_trnsoitemcont.Where(a => a.soitemmstoid == tbl.soitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsoitemcont.RemoveRange(trndtlcont);
                            db.SaveChanges();
                        }

                        QL_trnsoitemdtl tbldtl;
                        //QL_trnsoitemcont tbldtlcont;
                        decimal dDiv = 1M;
                        if (!tbl.isexcludetax)
                        {
                            dDiv = 1.1M;
                        }
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsoitemdtl();
                            dtDtl[i].soitemprice_intax = dtDtl[i].soitemprice / dDiv;
                            dtDtl[i].soitemdtlamt_intax = dtDtl[i].soitemdtlamt / dDiv;
                            dtDtl[i].soitemdtldiscvalue_intax = dtDtl[i].soitemdtldiscvalue / dDiv;
                            dtDtl[i].soitemdtldiscamt_intax = dtDtl[i].soitemdtldiscamt / dDiv;
                            dtDtl[i].soitemdtlnetto_intax = dtDtl[i].soitemdtlnetto / dDiv;

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.soitemdtloid = dtloid++;
                            tbldtl.soitemmstoid = tbl.soitemmstoid;
                            tbldtl.soitemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.soitemqty = dtDtl[i].soitemqty;
                            tbldtl.soitemunitoid = dtDtl[i].soitemunitoid;
                            tbldtl.soitemprice = dtDtl[i].soitemprice;
                            tbldtl.soitemprice_intax = dtDtl[i].soitemprice_intax;
                            tbldtl.soitemdtlamt = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtlamt_intax = dtDtl[i].soitemdtlamt_intax;
                            tbldtl.soitemdtldisctype = dtDtl[i].soitemdtldisctype;
                            tbldtl.soitemdtldiscvalue = dtDtl[i].soitemdtldiscvalue;
                            tbldtl.soitemdtldiscvalue_intax = dtDtl[i].soitemdtldiscvalue_intax;
                            tbldtl.soitemdtldiscamt = dtDtl[i].soitemdtldiscamt;
                            tbldtl.soitemdtldiscamt_intax = dtDtl[i].soitemdtldiscamt_intax;
                            tbldtl.soitemdtlnetto = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlnetto_intax = dtDtl[i].soitemdtlnetto_intax;
                            tbldtl.soitemdtlstatus = "";
                            tbldtl.soitemdtlnote = (dtDtl[i].soitemdtlnote == null ? "" : dtDtl[i].soitemdtlnote);
                            tbldtl.isfoc = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnsoitemdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnsoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //for (var i = 0; i < dtDtlCont.Count(); i++) 
                        //{
                        //    if (dtDtlCont[i].soitemcontqty > 0)
                        //    {
                        //        tbldtlcont = new QL_trnsoitemcont();
                        //        tbldtlcont.cmpcode = tbl.cmpcode;
                        //        tbldtlcont.soitemcontoid = socontoid++;
                        //        tbldtlcont.soitemmstoid = tbl.soitemmstoid;
                        //        tbldtlcont.containeroid = dtDtlCont[i].containeroid;
                        //        tbldtlcont.soitemcontqty = dtDtlCont[i].soitemcontqty;
                        //        tbldtlcont.soitemcontpct = dtDtlCont[i].soitemcontpct;
                        //        tbldtlcont.soitemcontstatus = "";
                        //        tbldtlcont.upduser = tbl.upduser;
                        //        tbldtlcont.updtime = tbl.updtime;

                        //        db.QL_trnsoitemcont.Add(tbldtlcont);
                        //        db.SaveChanges();
                        //    }
                        //}

                        //sSql = "UPDATE QL_mstoid SET lastoid=" + socontoid + " WHERE tablename='QL_trnsoitemcont'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.soitemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SO-FG" + tbl.soitemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnsoitemmst";
                                tblApp.oid = tbl.soitemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.soitemmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: SOitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tbl = db.QL_trnsoitemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnsoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtlcont = db.QL_trnsoitemcont.Where(a => a.soitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnsoitemcont.RemoveRange(trndtlcont);
                        db.SaveChanges();

                        db.QL_trnsoitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string sotype, string printtype, Boolean cbprice)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (printtype == "Default")
            {
                if (!cbprice)
                {
                    if (sotype == "LOCAL")
                        report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItem.rpt"));
                    else
                        report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItemEn.rpt"));
                } else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItem2.rpt"));
                }
            } else if (printtype == "PI")
            {
                var sReportFile = db.Database.SqlQuery<string>("SELECT TOP 1 pireportfile FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.soitemmstoid=" + id).FirstOrDefault();
                if (sReportFile == "" || sReportFile == null)
                    sReportFile = "rptSOItem.rpt";
                report.Load(Path.Combine(Server.MapPath("~/Report"), sReportFile));
            } else
            {
                if (!cbprice)
                {
                    if (sotype == "LOCAL")
                        report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItemM3.rpt"));
                    else
                        report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItemEnM3.rpt"));
                }
                else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItem2M3.rpt"));
                }
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE som.cmpcode='"+ cmp +"' AND som.soitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "SOFinishGoodReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}