﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class SOClosingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class somst
        {
            public string cmpcode { get; set; }
            public int somstoid { get; set; }
            public int custoid { get; set; }
            public string sono { get; set; }
            public string sType { get; set; }
            public string sodate { get; set; }
            public string somststatus { get; set; }
            public string somstnote { get; set; }
            public string custname { get; set; }
            public decimal sograndtotalamt { get; set; }
        }

        public class sodtl
        {
            public string cmpcode { get; set; }
            public int sodtloid { get; set; }
            public int somstoid { get; set; }
            public string sotype { get; set; }
            public int sodtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal soqty { get; set; }
            public string sounit { get; set; }
            public string sodtlnote { get; set; }
            public string sodtlstatus { get; set; }
            public string somststatus { get; set; }
            public decimal soprice { get; set; }
            public int assetmstoid { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetSalesOrderData(string cmp, string sType)
        {
            List<somst> tbl = new List<somst>();
            sSql = "SELECT so" + sType + "mstoid AS somstoid, So" + sType + "no AS sono, CONVERT(VARCHAR(10), So" + sType + "date, 101) AS sodate, so" + sType + "mststatus AS somststatus, so" + sType + "mstnote AS somstnote, c.custname, so" + sType + "grandtotalamt AS sograndtotalamt FROM QL_trnso" + sType + "mst s INNER JOIN QL_mstcust C ON s.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE s.cmpcode='" + cmp + "' AND (so" + sType + "mststatus='Approved') AND so" + sType + "mstoid IN (SELECT so" + sType + "mstoid FROM QL_trndo" + sType + "dtl dod INNER JOIN QL_trndo" + sType + "mst dom ON dod.do" + sType + "mstoid=dom.do" + sType + "mstoid WHERE dod.cmpcode='" + cmp + "' AND dom.do" + sType + "mststatus<>'Cancel')";
            if (sType.ToUpper() == "ITEM")
            {
                //sSql += " AND so" + sType + "mstoid NOT IN (SELECT so" + sType + "mstoid FROM QL_trnwodtl1 wod INNER JOIN QL_trnwomst wom ON wod.cmpcode=wom.cmpcode AND wod.womstoid=wom.womstoid WHERE wod.cmpcode='" + cmp + "' AND womststatus<>'Cancel')";
            }
            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND s.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " ORDER BY so" + sType + "date DESC, somstoid DESC";

            tbl = db.Database.SqlQuery<somst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<sodtl> tbl = new List<sodtl>();
            if (sType.ToUpper() == "RAW")
            {
                sSql = "SELECT sod.sorawdtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.sorawdtlseq) AS INT) AS sodtlseq, sod.sorawdtloid AS matoid, '' AS matcode, sodtldesc AS matlongdesc, (sorawqty - ISNULL((SELECT SUM(dorawqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawmstoid=dod.dorawmstoid), 0)=0 THEN dorawqty ELSE ISNULL((SELECT shipmentrawqty FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawdtloid=dod.dorawdtloid), 0)-ISNULL((SELECT SUM(sretrawqty) FROM QL_trnshipmentrawdtl sd2 INNER JOIN QL_trnsretrawdtl sretd ON sd2.shipmentrawdtloid=sretd.shipmentrawdtloid INNER JOIN QL_trnsretrawmst sretm ON sretd.sretrawmstoid=sretm.sretrawmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretrawmststatus<>'Rejected' AND sd2.dorawdtloid=dod.dorawdtloid), 0) END) AS dorawqty FROM QL_trndorawdtl dod INNER JOIN QL_trndorawmst dom ON dom.dorawmstoid=dod.dorawmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.sorawdtloid=sod.sorawdtloid AND dorawmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.sorawdtlnote AS sodtlnote, sod.sorawdtlstatus AS sodtlstatus, sorawprice as soprice FROM QL_trnsorawdtl sod INNER JOIN QL_mstgen g ON sod.sorawunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sOrawdtlstatus='' AND sod.sorawmstoid=" + iOid;
            } else if (sType.ToUpper() == "GEN")
            {
                sSql = "SELECT sod.sogendtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.sogendtlseq) AS INT) AS sodtlseq, Sod.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, (sogenqty - ISNULL((SELECT SUM(dogenqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentgendtl sd INNER JOIN QL_trnshipmentgenmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentgenmstoid=sd.shipmentgenmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentgenmststatus<>'Rejected' AND sd.dogenmstoid=dod.dogenmstoid), 0)=0 THEN dogenqty ELSE ISNULL((SELECT shipmentgenqty FROM QL_trnshipmentgendtl sd INNER JOIN QL_trnshipmentgenmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentgenmstoid=sd.shipmentgenmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentgenmststatus<>'Rejected' AND sd.dogendtloid=dod.dogendtloid), 0)-ISNULL((SELECT SUM(sretgenqty) FROM QL_trnshipmentgendtl sd2 INNER JOIN QL_trnsretgendtl sretd ON sd2.shipmentgendtloid=sretd.shipmentgendtloid INNER JOIN QL_trnsretgenmst sretm ON sretd.sretgenmstoid=sretm.sretgenmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretgenmststatus<>'Rejected' AND sd2.dogendtloid=dod.dogendtloid), 0) END) AS dogenqty FROM QL_trndogendtl dod INNER JOIN QL_trndogenmst dom ON dom.dogenmstoid=dod.dogenmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.sogendtloid=sod.sogendtloid AND dogenmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.sogendtlnote AS sodtlnote, sod.sogendtlstatus AS sodtlstatus, sogenprice as soprice FROM QL_trnsogendtl sod INNER JOIN QL_mstmatgen m ON sod.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON sod.sogenunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sOgendtlstatus='' AND sod.sogenmstoid=" + iOid;
            } else if (sType.ToUpper() == "SP")
            {
                sSql = "SELECT sod.sospdtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.sospdtlseq) AS INT) AS sodtlseq, sod.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, (sospqty - ISNULL((SELECT SUM(dospqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentspdtl sd INNER JOIN QL_trnshipmentspmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentspmstoid=sd.shipmentspmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentspmststatus<>'Rejected' AND sd.dospmstoid=dod.dospmstoid), 0)=0 THEN dospqty ELSE ISNULL((SELECT shipmentspqty FROM QL_trnshipmentspdtl sd INNER JOIN QL_trnshipmentspmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentspmstoid=sd.shipmentspmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentspmststatus<>'Rejected' AND sd.dospdtloid=dod.dospdtloid), 0)-ISNULL((SELECT SUM(sretspqty) FROM QL_trnshipmentspdtl sd2 INNER JOIN QL_trnsretspdtl sretd ON sd2.shipmentspdtloid=sretd.shipmentspdtloid INNER JOIN QL_trnsretspmst sretm ON sretd.sretspmstoid=sretm.sretspmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretspmststatus<>'Rejected' AND sd2.dospdtloid=dod.dospdtloid), 0) END) AS dospqty FROM QL_trndospdtl dod INNER JOIN QL_trndospmst dom ON dom.dospmstoid=dod.dospmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.sospdtloid=sod.sospdtloid AND dospmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.sospdtlnote AS sodtlnote, sod.sospdtlstatus AS sodtlstatus, sospprice as soprice FROM QL_trnsospdtl sod INNER JOIN QL_mstsparepart m ON sod.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON sod.sospunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sospdtlstatus='' AND sod.sospmstoid=" + iOid;
            } else if (sType.ToUpper() == "ITEM")
            {
                sSql = "SELECT sod.soitemdtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.soitemdtlseq) AS INT) AS sodtlseq, sod.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, (soitemqty - ISNULL((SELECT SUM(doitemqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemmstoid=dod.doitemmstoid), 0)=0 THEN doitemqty ELSE ISNULL((SELECT shipmentitemqty FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dod.doitemdtloid), 0)-ISNULL((SELECT SUM(sretitemqty) FROM QL_trnshipmentitemdtl sd2 INNER JOIN QL_trnsretitemdtl sretd ON sd2.shipmentitemdtloid=sretd.shipmentitemdtloid INNER JOIN QL_trnsretitemmst sretm ON sretd.sretitemmstoid=sretm.sretitemmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretitemmststatus<>'Rejected' AND sd2.doitemdtloid=dod.doitemdtloid), 0) END) AS doitemqty FROM QL_trndoitemdtl dod INNER JOIN QL_trndoitemmst dom ON dom.doitemmstoid=dod.doitemmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.soitemdtloid=sod.soitemdtloid AND doitemmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.soitemdtlnote AS sodtlnote, sod.soitemdtlstatus AS sodtlstatus, soitemprice as soprice FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON sod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sod.soitemunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND soitemdtlstatus='' AND sod.soitemmstoid=" + iOid;
            } else if (sType.ToUpper() == "ASSET")
            {
                sSql = "SELECT sod.soassetdtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.soassetdtlseq) AS INT) AS sodtlseq, fam.refoid AS matoid, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matcode, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matlongdesc, (soassetqty - ISNULL((SELECT SUM(doqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentassetdtl sd INNER JOIN QL_trnshipmentassetmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentassetmstoid=sd.shipmentassetmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentassetmststatus<>'Rejected' AND sd.doassetmstoid=dod.doassetmstoid), 0)=0 THEN doassetqty ELSE ISNULL((SELECT shipmentassetqty FROM QL_trnshipmentassetdtl sd INNER JOIN QL_trnshipmentassetmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentassetmstoid=sd.shipmentassetmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentassetmststatus<>'Rejected' AND sd.doassetdtloid=dod.doassetdtloid), 0)-ISNULL((SELECT SUM(sretassetqty) FROM QL_trnshipmentassetdtl sd2 INNER JOIN QL_trnsretassetdtl sretd ON sd2.shipmentassetdtloid=sretd.shipmentassetdtloid INNER JOIN QL_trnsretassetmst sretm ON sretd.sretassetmstoid=sretm.sretassetmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretassetmststatus<>'Rejected' AND sd2.doassetdtloid=dod.doassetdtloid), 0) END) AS doqty FROM QL_trndoassetdtl dod INNER JOIN QL_trndoassetmst dom ON dom.doassetmstoid=dod.doassetmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.soassetdtloid=sod.soassetdtloid AND doassetmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.soassetdtlnote AS sodtlnote, sod.soassetdtlstatus AS sodtlstatus, soassetprice as soprice, fam.assetmstoid FROM QL_trnsoassetdtl sod INNER JOIN QL_assetmst fam ON fam.assetmstoid=sod.assetmstoid INNER JOIN QL_mstgen g ON sod.soassetunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND soassetdtlstatus='' AND sod.soassetmstoid=" + iOid;
            } else if (sType.ToUpper() == "LOG")
            {
                sSql = "SELECT sod.sologdtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.sologdtlseq) AS INT) AS sodtlseq, sod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, (sologqty - ISNULL((SELECT SUM(dologqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentlogdtl sd INNER JOIN QL_trnshipmentlogmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentlogmstoid=sd.shipmentlogmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentlogmststatus<>'Rejected' AND sd.dologmstoid=dod.dologmstoid), 0)=0 THEN dologqty ELSE ISNULL((SELECT SUM(shipmentlogqty) FROM QL_trnshipmentlogdtl sd INNER JOIN QL_trnshipmentlogmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentlogmstoid=sd.shipmentlogmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentlogmststatus<>'Rejected' AND sd.dologdtloid=dod.dologdtloid), 0)-ISNULL((SELECT SUM(sretlogqty) FROM QL_trnshipmentlogdtl sd2 INNER JOIN QL_trnsretlogdtl sretd ON sd2.shipmentlogdtloid=sretd.shipmentlogdtloid INNER JOIN QL_trnsretlogmst sretm ON sretd.sretlogmstoid=sretm.sretlogmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretlogmststatus<>'Rejected' AND sd2.dologdtloid=dod.dologdtloid), 0) END) AS dologqty FROM QL_trndologdtl dod INNER JOIN QL_trndologmst dom ON dom.dologmstoid=dod.dologmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.sologdtloid=sod.sologdtloid AND dologmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.sologdtlnote AS sodtlnote, sod.sologdtlstatus AS sodtlstatus, sologprice as soprice FROM QL_trnsologdtl sod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=sod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON sod.sologunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sologdtlstatus='' AND sod.sologmstoid=" + iOid;
            } else if (sType.ToUpper() == "SAWN")
            {
                sSql = "SELECT sod.sosawndtloid AS sodtloid, CAST(ROW_NUMBER() OVER(ORDER BY sod.sosawndtlseq) AS INT) AS sodtlseq, sod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, (sosawnqty - ISNULL((SELECT SUM(dosawnqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentsawndtl sd INNER JOIN QL_trnshipmentsawnmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentsawnmstoid=sd.shipmentsawnmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentsawnmststatus<>'Rejected' AND sd.dosawnmstoid=dod.dosawnmstoid), 0)=0 THEN dosawnqty ELSE ISNULL((SELECT SUM(shipmentsawnqty) FROM QL_trnshipmentsawndtl sd INNER JOIN QL_trnshipmentsawnmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentsawnmstoid=sd.shipmentsawnmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentsawnmststatus<>'Rejected' AND sd.dosawndtloid=dod.dosawndtloid), 0)-ISNULL((SELECT SUM(sretsawnqty) FROM QL_trnshipmentsawndtl sd2 INNER JOIN QL_trnsretsawndtl sretd ON sd2.shipmentsawndtloid=sretd.shipmentsawndtloid INNER JOIN QL_trnsretsawnmst sretm ON sretd.sretsawnmstoid=sretm.sretsawnmstoid  WHERE sd2.cmpcode=dod.cmpcode AND sretsawnmststatus<>'Rejected' AND sd2.dosawndtloid=dod.dosawndtloid), 0) END) AS dosawnqty FROM QL_trndosawndtl dod INNER JOIN QL_trndosawnmst dom ON dom.dosawnmstoid=dod.dosawnmstoid WHERE dod.cmpcode=sod.cmpcode AND dod.sosawndtloid=sod.sosawndtloid AND dosawnmststatus <>'Cancel') AS tbltmp), 0.0)) AS soqty, g.gendesc AS sounit, sod.sosawndtlnote AS sodtlnote, sod.sosawndtlstatus AS sodtlstatus, sosawnprice as soprice FROM QL_trnsosawndtl sod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=sod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON sod.sosawnunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sosawndtlstatus='' AND sod.sosawnmstoid=" + iOid;
            }
            tbl = db.Database.SqlQuery<sodtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<sodtl> dtDtl)
        {
            Session["QL_trnsodtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<sodtl> dtDtl = (List<sodtl>)Session["QL_trnsodtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].somstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnso" + dtDtl[0].sotype + "dtl WHERE so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND so" + dtDtl[0].sotype + "dtloid=" + dtDtl[i].sodtloid + " AND so" + dtDtl[0].sotype + "dtlstatus='Complete'";
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "This data " + dtDtl[i].matcode + " has been Closed by another user. Please CLOSING this transaction and try again!");
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            for(var i = 0; i < dtDtl.Count(); i++)
                            {
                                sSql = "UPDATE QL_trnso" + dtDtl[0].sotype + "dtl SET so" + dtDtl[0].sotype + "dtlstatus='Complete', soclosingqty='" + dtDtl[i].soqty + "', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND so" + dtDtl[0].sotype + "dtloid=" + dtDtl[i].sodtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                if (dtDtl[0].sotype.ToUpper() == "ASSET")
                                {
                                    sSql = "UPDATE QL_assetmst SET assetmstflag='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND assetmstoid=" + dtDtl[i].assetmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                sSql = "UPDATE QL_trnso" + dtDtl[0].sotype + "mst SET so" + dtDtl[0].sotype + "mststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND (SELECT COUNT(-1) FROM QL_trnso" + dtDtl[0].sotype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].sotype + "dtlstatus='' AND so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND so" + dtDtl[0].sotype + "dtloid<>" + dtDtl[i].sodtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            
                            objTrans.Commit();
                            Session["QL_trnsodtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}