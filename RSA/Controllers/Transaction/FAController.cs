﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Transaction
{
    public class FAController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public FAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class viewindex
        {
            public string cmpcode { get; set; }
            public int assetrecmstoid { get; set; }
            public string assetrecno { get; set; }
            public DateTime assetrecdate { get; set; }
            public string mrassetno { get; set; }
            public string assetreflongdesc { get; set; }
            public string assetrecmststatus { get; set; }
            public string assetrecmstnote { get; set; }
            public string divname { get; set; }
        }

        public class listassetmst
        {
            public decimal assetrecvalue { get; set; }
            public int accumdepacctgoid { get; set; }
            public decimal assetrecdepmonth { get; set; }
            public decimal assetrecdepvalue { get; set; }
            public int depacctgoid { get; set; }
            public int deptoid { get; set; }
            public string assetrecmstnote { get; set; }
            public int assetacctgoid { get; set; }
        }

        public class mritem
        {
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
            public DateTime mrassetdate { get; set; }
            public string mrassetdatestr { get; set; }
            public string mrassetmstnote { get; set; }
            public string poassetno { get; set; }
            public string suppname { get; set; }
            public int mrassetwhoid { get; set; }
        }

        public class assetmst
        {
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public string refcode { get; set; }
            public string reflongdesc { get; set; }
            public decimal assetvalue { get; set; }
            public decimal assetvalueusd { get; set; }
            public int assetacctgoid { get; set; }
        }

        public class pic
        {
            public string usoid { get; set; }
            public string usname { get; set; }
            public string NIK { get; set; }
            public string deptname { get; set; }
        }

        public class mrassetdtl
        {
            public int mrassetdtlseq { get; set; }
            public int mrassetdtloid { get; set; }
            public int mrassetrefoid { get; set; }
            public string mrassetrefcode { get; set; }
            public string mrassetreflongdesc { get; set; }
            public decimal mrassetqty { get; set; }
            public int mrassetunitoid { get; set; }
            public string mrassetunit { get; set; }
            public string mrassetdtlnote { get; set; }
            public decimal mrassetvalue { get; set; }
            public decimal mrassetvalueidr { get; set; }
            public decimal mrassetvalueusd { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int assetacctgoid { get; set; }
            public int accumdepacctgoid { get; set; }
            public int depacctgoid { get; set; }
            public int assetdepmonth { get; set; }
        }

        public class assetrecdtl
        {
            public int assetrecdtlseq { get; set; }
            public int assetrecmonth { get; set; }
            public int assetrecyear { get; set; }
            public string assetrecperiod { get; set; }
            public string assetrecdtlres1 { get; set; }
            public decimal assetrecperiodvalue { get; set; }
            public decimal assetrecperiodvalueidr { get; set; }
            public decimal assetrecperiodvalueusd { get; set; }
            public decimal assetrecperioddep { get; set; }
            public decimal assetrecperioddepidr { get; set; }
            public decimal assetrecperioddepusd { get; set; }
            public int periodacctgoid { get; set; }
            public int perioddepacctgoid { get; set; }
            public string assetrecdtlnote { get; set; }
        }

        string generateNo(DateTime tanggal)
        {
            string sNo = "FA-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(assetrecno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnassetrecmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND assetrecno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private string GenerateAssetNo(string cmp)
        {
            var assetno = "";
            if (cmp != "")
            {
                string sNo = "ASET-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(assetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_assetmst WHERE cmpcode='" + CompnyCode + "' AND assetno LIKE '" + sNo + "%' ";
                assetno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            }
            return assetno;
        }

        [HttpPost]
        public ActionResult GetPICData()
        {
            List<pic> tbl = new List<pic>();
            sSql = "select nip NIK, usoid, usname, ISNULL((SELECT groupdesc FROM QL_mstdept d WHERE d.deptoid=us.deptoid),'') deptname from QL_mstperson us WHERE activeflag='ACTIVE' order by us.personname";

            tbl = db.Database.SqlQuery<pic>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMRData()
        {
            List<mritem> tbl = new List<mritem>();
            sSql = "SELECT DISTINCT mrm.mrassetmstoid, mrm.mrassetno, CONVERT(VARCHAR(10), mrm.mrassetdate, 103) AS mrassetdatestr, mrassetdate AS mrassetdate, s.suppname, mrm.mrassetmstnote, mrm.curroid, (SELECT poassetno FROM QL_trnpoassetmst where poassetmstoid = mrm.poassetmstoid) poassetno, CONVERT(VARCHAR(10), mrm.mrassetdate, 103) AS mrassetdate, mrm.mrassetwhoid FROM QL_trnmrassetmst mrm INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + CompnyCode +"' AND mrm.mrassetmststatus IN ('Post', 'Closed') AND ISNULL(mrm.mrassetmstres2, '')='' AND ISNULL(mrm.mrassetmstres1, '')='' ORDER BY mrm.mrassetno";

            tbl = db.Database.SqlQuery<mritem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTBData()
        {
            List<mritem> tbl = new List<mritem>();
            sSql = "SELECT DISTINCT mrm.transformmstoid mrassetmstoid, mrm.transformno mrassetno, CONVERT(VARCHAR(10), mrm.transformdate, 103) AS mrassetdatestr, transformdate AS mrassetdate, '' suppname, mrm.transformmstnote mrassetmstnote, 1 curroid, '' poassetno, CONVERT(VARCHAR(10), mrm.transformdate, 103) AS mrassetdate, td.transformdtl2whoid mrassetwhoid FROM QL_trntransformmst mrm INNER JOIN QL_trntransformdtl2 td ON td.transformmstoid=mrm.transformmstoid INNER JOIN QL_mstitem i ON i.itemoid=td.transformdtl2refoid WHERE mrm.cmpcode='"+ CompnyCode +"' AND mrm.transformmststatus IN ('Post', 'Closed') AND i.itemtype='Asset' AND ISNULL(mrm.transformmstres2, '')='' ORDER BY mrm.transformno";

            tbl = db.Database.SqlQuery<mritem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetASsetData()
        {
            List<assetmst> tbl = new List<assetmst>();
            sSql = "SELECT assetmstoid, assetno, reftype, refoid, refcode, reflongdesc, (assetvalue + totalservice - assetreturn) AS assetvalue, (assetvalueusd + totalserviceusd - assetreturnusd) AS assetvalueusd, assetacctgoid FROM (SELECT am.assetmstoid, assetno, am.reftype, am.refoid, (SELECT itemcode FROM QL_mstitem m WHERE itemoid=am.refoid) AS refcode, (SELECT itemdesc FROM QL_mstitem m WHERE itemoid=am.refoid) AS reflongdesc, ISNULL((SELECT SUM(matusagevalueidr) FROM QL_trnmatusagedtl mud INNER JOIN QL_trnmatusagemst mum ON mum.cmpcode = mud.cmpcode AND mum.matusagemstoid = mud.matusagemstoid WHERE mud.cmpcode = am.cmpcode AND mum.assetmstoid = am.assetmstoid AND mum.matusagemststatus <> 'In Process' AND mum.assetmstoid > 0), 0.0) AS assetvalue, ISNULL((SELECT SUM(matusagevalueusd) FROM QL_trnmatusagedtl mud INNER JOIN QL_trnmatusagemst mum ON mum.cmpcode = mud.cmpcode AND mum.matusagemstoid = mud.matusagemstoid WHERE mud.cmpcode = am.cmpcode AND mum.assetmstoid = am.assetmstoid AND mum.matusagemststatus <> 'In Process' AND mum.assetmstoid > 0), 0.0) AS assetvalueusd, inprogacctgoid assetacctgoid, 0.0 totalservice, 0.0 totalserviceusd, 0.0 AS assetreturn, 0.0 AS assetreturnusd FROM QL_assetmst am WHERE am.cmpcode = '"+ CompnyCode +"' AND ISNULL(assetmstres1, '')= 'Asset In Progress' AND assetmststatus = 'Post' AND am.assetmstoid IN(SELECT mum.assetmstoid FROM QL_trnmatusagemst mum WHERE mum.cmpcode = am.cmpcode AND mum.matusagemststatus <> 'In Process' AND mum.assetmstoid > 0)) AS QL_assetmst ORDER BY assetno";

            tbl = db.Database.SqlQuery<assetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMatData(int mrassetmstoid, string assetrectype)
        {
            List<mrassetdtl> tbl = new List<mrassetdtl>();
            if (assetrectype == "Asset Confirmation")
            {
                sSql = "SELECT mrd.mrassetdtlseq, mrd.mrassetdtloid, mrd.mrassetreftype, mrd.mrassetrefoid, (SELECT itemcode FROM QL_mstitem where itemoid = mrd.mrassetrefoid) AS mrassetrefcode, (SELECT matgenlongdesc FROM QL_mstmatgen where matgenoid = mrd.mrassetrefoid) AS mrassetreflongdesc, (mrd.mrassetqty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.mrassetdtloid ), 0)  /* - ISNULL((SELECT SUM(pretassetqty) FROM QL_trnpretassetdtl pretd INNER JOIN QL_trnpretassetmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretassetmstoid=pretd.pretassetmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrassetdtloid=mrd.mrassetdtloid AND pretm.pretassetmststatus<>'Rejected'), 0)*/) AS mrassetqty, mrd.mrassetunitoid, g.gendesc AS mrassetunit, mrd.mrassetdtlnote, ISNULL((SELECT (((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) FROM QL_trnpoassetdtl pod WHERE pod.poassetdtloid=mrd.poassetdtloid AND pod.poassetrefoid=mrd.mrassetrefoid),0.0) mrassetvalue, ISNULL((SELECT (((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) FROM QL_trnpoassetdtl pod WHERE pod.poassetdtloid=mrd.poassetdtloid AND pod.poassetrefoid=mrd.mrassetrefoid),0.0) mrassetvalueidr, mrd.mrassetvalueusd, '' refno, '' serialnumber, (SELECT acctgoid FROM QL_mstmatgen where matgenoid = mrd.mrassetrefoid) assetacctgoid, ISNULL((SELECT accumdepacctgoid FROM QL_mstmatgen i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.acctgoid where i.matgenoid = mrd.mrassetrefoid),0) accumdepacctgoid, ISNULL((SELECT depacctgoid FROM QL_mstmatgen i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.acctgoid where i.matgenoid = mrd.mrassetrefoid),0) depacctgoid, ISNULL((SELECT assetdepmonth FROM QL_mstmatgen i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.acctgoid where i.matgenoid = mrd.mrassetrefoid),0) assetdepmonth FROM QL_trnmrassetdtl mrd INNER JOIN QL_mstgen g ON g.genoid=mrd.mrassetunitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrd.mrassetmstoid=" + mrassetmstoid + " AND ISNULL(mrd.mrassetdtlres2, '')='' AND ISNULL(mrd.mrassetdtlres1, '')='' ORDER BY mrd.mrassetdtlseq";
            }
            else
            {
                sSql = "SELECT mrd.transformdtl2seq mrassetdtlseq, mrd.transformdtl2oid mrassetdtloid, mrd.transformdtl2reftype mrassetreftype, mrd.transformdtl2refoid mrassetrefoid, (SELECT itemcode FROM QL_mstitem where itemoid = mrd.transformdtl2refoid) AS mrassetrefcode, (SELECT itemdesc FROM QL_mstitem where itemoid = mrd.transformdtl2refoid) AS mrassetreflongdesc, (mrd.transformdtl2qty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.transformdtl2oid ), 0)) AS mrassetqty, mrd.transformdtl2unitoid mrassetunitoid, g.gndesc AS mrassetunit, mrd.transformdtl2note mrassetdtlnote, CAST(CASE WHEN mrd.transformdtl2valueidr > 0 THEN (mrd.transformdtl2valueidr/mrd.transformdtl2qty) ELSE 0.0 END AS DECIMAL (18,4))  mrassetvalue, CAST(CASE WHEN mrd.transformdtl2valueidr > 0 THEN (mrd.transformdtl2valueidr/mrd.transformdtl2qty) ELSE 0.0 END AS DECIMAL (18,4)) mrassetvalueidr, mrd.transformdtl2valueusd mrassetvalueusd, ISNULL(mrd.refno,'') refno, ISNULL(mrd.serialnumber,'') serialnumber, (SELECT assetacctgoid FROM QL_mstitem where itemoid = mrd.transformdtl2refoid) assetacctgoid, ISNULL((SELECT accumdepacctgoid FROM QL_mstitem i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.assetacctgoid where i.itemoid = mrd.transformdtl2refoid),0) accumdepacctgoid, ISNULL((SELECT depacctgoid FROM QL_mstitem i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.assetacctgoid where i.itemoid = mrd.transformdtl2refoid),0) depacctgoid, ISNULL((SELECT assetdepmonth FROM QL_mstitem i INNER JOIN QL_mstassetgroup b ON b.assetacctgoid=i.assetacctgoid where i.itemoid = mrd.transformdtl2refoid),0) assetdepmonth FROM QL_trntransformdtl2 mrd INNER JOIN QL_m05gn g ON g.gnoid=mrd.transformdtl2unitoid WHERE mrd.cmpcode='" + CompnyCode +"' AND mrd.transformmstoid="+ mrassetmstoid +" AND ISNULL(mrd.transformdtl2res2, '')='' ORDER BY mrd.transformdtl2seq";
            }
            tbl = db.Database.SqlQuery<mrassetdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<assetrecdtl> dtDtl)
        {
            Session["QL_trnassetrecdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnassetrecdtl"] == null)
            {
                Session["QL_trnassetrecdtl"] = new List<assetrecdtl>();
            }

            List<assetrecdtl> dataDtl = (List<assetrecdtl>)Session["QL_trnassetrecdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: FixedAssetsConfirmation
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();

            sSql = "SELECT farm.cmpcode, farm.assetrecmstoid, farm.assetrecno, assetrecdate, (CASE WHEN assetrectype = 'Asset Confirmation' THEN (SELECT mrassetno FROM QL_trnmrassetmst where mrassetmstoid = farm.mrassetmstoid) WHEN assetrectype = 'Asset In Progress Closed' THEN (SELECT assetno FROM QL_assetmst where assetmstoid = farm.mrassetmstoid) WHEN assetrectype = 'Asset Transform' THEN (SELECT transformno FROM QL_trntransformmst where transformmstoid = farm.mrassetmstoid) ELSE '' END) mrassetno, (SELECT matgenlongdesc FROM QL_mstmatgen where matgenoid = farm.assetrefoid) AS assetreflongdesc, farm.assetrecmststatus, farm.assetrecmstnote, di.divname FROM QL_trnassetrecmst farm INNER JOIN QL_mstdivision di ON di.cmpcode=farm.cmpcode";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " WHERE farm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " WHERE farm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND assetrecmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND assetrecmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND assetrecmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND assetrecdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND assetrecdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND assetrecmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            List<viewindex> dt = db.Database.SqlQuery<viewindex>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnassetrecmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: FixedAssetsConfirmation/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnassetrecmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnassetrecmst();
                tbl.assetrecmstoid = ClassFunction.GenerateID("QL_trnassetrecmst");
                tbl.assetrecdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.assetrecmststatus = "In Process";

                Session["QL_trnassetrecdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnassetrecmst.Find(CompnyCode, id);
                FillAdditionalField(tbl);

                sSql = "SELECT fard.assetrecdtlseq,fard.assetrecmonth AS assetrecmonth, fard.assetrecyear, fard.assetrecperiod, fard.assetrecdtlres1, fard.assetrecperiodvalue, 0.0 assetrecperiodvalueidr, 0.0 assetrecperiodvalueusd, fard.assetrecperioddep, 0.0 assetrecperioddepidr, 0.0 assetrecperioddepusd, fard.periodacctgoid, fard.perioddepacctgoid, fard.assetrecdtlnote FROM QL_trnassetrecdtl fard WHERE fard.cmpcode='" + CompnyCode + "' AND fard.assetrecmstoid=" + id + " ORDER BY fard.assetrecdtlseq";
                var tbldtl = db.Database.SqlQuery<assetrecdtl>(sSql).ToList();

                Session["QL_trnassetrecdtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnassetrecmst tbl, string action, string tglmst, int mrassetwhoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tbl.assetrecmststatus == "Post")
            {
                tbl.assetrecno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.assetrecno = "";
            }
            //try
            //{
            //    tbl.assetrecdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            //}
            //catch (Exception ex)
            //{
            //    ModelState.AddModelError("assetrecdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            //}

            if (string.IsNullOrEmpty(tbl.assetreftype))
                tbl.assetreftype = "Asset";
            if (string.IsNullOrEmpty(tbl.assetrecmstnote))
                tbl.assetrecmstnote = "";
            if (string.IsNullOrEmpty(tbl.assetrecmstres1))
                tbl.assetrecmstres1 = "";
            if (string.IsNullOrEmpty(tbl.assetrecmstres2))
                tbl.assetrecmstres2 = "";
            if (string.IsNullOrEmpty(tbl.assetrecmstres3))
                tbl.assetrecmstres3 = "";
            
            //is Input Detail Valid
            List<assetrecdtl> dtDtl = (List<assetrecdtl>)Session["QL_trnassetrecdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].assetrecdtlnote))
                            dtDtl[i].assetrecdtlnote = "";
                    }
                    if (tbl.assetrecmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tbl.assetrecno))
                            ModelState.AddModelError("assetrecno", "Silahkan isi No. Confirm!");
                        else if (db.QL_trnassetrecmst.Where(w => w.assetrecno == tbl.assetrecno & w.assetrecmstoid != tbl.assetrecmstoid).Count() > 0)
                            ModelState.AddModelError("assetrecno", "No. Confirm yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");
                    }
                }
            }

            DateTime sDate = tbl.assetrecdate;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            var servertime = ClassFunction.GetServerTime();

            decimal QtyOS = 0; int whoid = 0;
            if  (tbl.assetrectype == "Asset Confirmation")
            {
                sSql = "SELECT (mrd.mrassetqty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.mrassetdtloid ), 0)  /* - ISNULL((SELECT SUM(pretassetqty) FROM QL_trnpretassetdtl pretd INNER JOIN QL_trnpretassetmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretassetmstoid=pretd.pretassetmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrassetdtloid=mrd.mrassetdtloid AND pretm.pretassetmststatus<>'Rejected'), 0)*/) AS mrassetqty FROM QL_trnmrassetdtl mrd INNER JOIN QL_mstgen g ON g.genoid=mrd.mrassetunitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrd.mrassetmstoid=" + tbl.mrassetmstoid + " AND mrd.mrassetdtloid="+ tbl.mrassetdtloid +" AND ISNULL(mrd.mrassetdtlres2, '')='' AND ISNULL(mrd.mrassetdtlres1, '')=''";
                QtyOS = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                sSql = "SELECT mrassetwhoid FROM QL_trnmrassetmst mrm WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mrassetmstoid=" + tbl.mrassetmstoid + "";
                whoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            else if (tbl.assetrectype == "Asset Transform")
            {
                sSql = "SELECT (mrd.transformdtl2qty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.transformdtl2oid ), 0)) AS mrassetqty FROM QL_trntransformdtl2 mrd INNER JOIN QL_m05gn g ON g.gnoid=mrd.transformdtl2unitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrd.transformmstoid=" + tbl.mrassetmstoid + " AND mrd.transformdtl2oid=" + tbl.mrassetdtloid + " AND ISNULL(mrd.transformdtl2res2, '')=''";
                QtyOS = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                sSql = "select transformdtl2whoid from QL_trntransformdtl2 where transformdtl2oid = " + tbl.mrassetdtloid + "";
                whoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }

            if (tbl.assetrecmststatus == "Post")
            {
                // Interface Validation
                //if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                //    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                //if (!ClassFunction.IsInterfaceExists("VAR_ASSET_IN_PROGRESS", CompnyCode))
                //    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_ASSET_IN_PROGRESS"));

                if (tbl.assetrectype == "Asset Confirmation" || tbl.assetrectype == "Asset Transform")
                {
                    ////Cek Stock
                    //if (!ClassFunction.IsStockAvailable(CompnyCode, servertime, tbl.assetrefoid, whoid, tbl.assetrecqty, tbl.refno, 0, tbl.serialnumber))
                    //{
                    //    ModelState.AddModelError("", "Qty Barang Asset tidak boleh lebih dari Stock Qty!");
                    //}
                }
            }

            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.assetrecmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.assetrecdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.assetrecmststatus = "In Process";
            }
            if (tbl.assetrecmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.assetrecmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var mstoid = ClassFunction.GenerateID("QL_trnassetrecmst");
                var dtloid = ClassFunction.GenerateID("QL_trnassetrecdtl");
                var assetmstoid = ClassFunction.GenerateID("QL_assetmst");
                var assetdtloid = ClassFunction.GenerateID("QL_assetdtl");

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                //var conmatoid = ClassFunction.GenerateID("QL_conmat");

                //var iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                //if (tbl.assetrectype == "Asset In Progress Closed")
                //{
                //    iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ASSET_IN_PROGRESS", CompnyCode));
                //}

                tbl.periodacctg = sPeriod;
                tbl.rateoid = 1;
                tbl.rate2oid = rate2oid;
                
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.assetrecmstoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();

                            db.QL_trnassetrecmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_mstoid set lastoid = " + mstoid + " Where tablename = 'QL_trnassetrecmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tbl.assetrectype == "Asset Confirmation")
                            {
                                sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres2='' WHERE mrassetdtloid=" + tbl.mrassetdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres2='' WHERE mrassetmstoid=" + tbl.mrassetmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } 
                            else if (tbl.assetrectype == "Asset Transform")
                            {
                                sSql = "UPDATE QL_trntransformdtl2 SET transformdtl2res2='' WHERE transformdtl2oid=" + tbl.mrassetdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trntransformmst SET transformmstres2='' WHERE transformmstoid=" + tbl.mrassetmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var trndtl = db.QL_trnassetrecdtl.Where(a => a.assetrecmstoid == tbl.assetrecmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnassetrecdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnassetrecdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnassetrecdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.assetrecdtloid = dtloid++;
                            tbldtl.assetrecmstoid = tbl.assetrecmstoid;
                            tbldtl.assetrecdtlseq = i + 1;
                            tbldtl.assetrecmonth = dtDtl[i].assetrecmonth;
                            tbldtl.assetrecyear = dtDtl[i].assetrecyear;
                            tbldtl.assetrecperiod = dtDtl[i].assetrecperiod;
                            tbldtl.assetrecperiodvalue = dtDtl[i].assetrecperiodvalue;
                            tbldtl.periodacctgoid = dtDtl[i].periodacctgoid;
                            tbldtl.assetrecperioddep = dtDtl[i].assetrecperioddep;
                            tbldtl.perioddepacctgoid = dtDtl[i].perioddepacctgoid;
                            tbldtl.assetrecdtlstatus = "";
                            tbldtl.assetrecdtlres1 = dtDtl[i].assetrecdtlres1;
                            tbldtl.assetrecdtlres2 = "";
                            tbldtl.assetrecdtlres3 = "";
                            tbldtl.assetrecdtlnote = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;                            

                            db.QL_trnassetrecdtl.Add(tbldtl);
                            db.SaveChanges(); 
                        }

                        if (tbl.assetrectype == "Asset Confirmation")
                        {
                            if (QtyOS <= tbl.assetrecqty)
                            {
                                sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres2='Complete' WHERE mrassetdtloid = " + tbl.mrassetdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres2='Closed' WHERE mrassetmstoid=" + tbl.mrassetmstoid + " AND (SELECT COUNT(*) FROM QL_trnmrassetdtl WHERE mrassetmstoid=" + tbl.mrassetmstoid + " AND ISNULL(mrassetdtlres1, '')='' AND ISNULL(mrassetdtlres2, '')='' AND mrassetdtloid<>" + tbl.mrassetdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }  
                        else if (tbl.assetrectype == "Asset Transform")
                        {
                            if (QtyOS <= tbl.assetrecqty)
                            {
                                sSql = "UPDATE QL_trntransformdtl2 SET transformdtl2res2='Complete' WHERE transformdtl2oid = " + tbl.mrassetdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trntransformmst SET transformmstres2='Closed' WHERE transformmstoid=" + tbl.mrassetmstoid + " AND (SELECT COUNT(*) FROM QL_trntransformdtl2 WHERE transformmstoid=" + tbl.mrassetmstoid + " AND ISNULL(transformdtl2res2, '')='' AND transformdtl2oid<>" + tbl.mrassetdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (tbl.assetrecmststatus == "Post")
                        {
                            int assetmstoid_backup = 0;
                            if (tbl.assetrectype == "Asset Confirmation" || tbl.assetrectype == "Asset Transform")
                            {
                                QL_assetmst tblass;
                                tblass = new QL_assetmst();
                                tblass.cmpcode = tbl.cmpcode;
                                tblass.assetmstoid = assetmstoid;
                                tblass.periodacctg = sPeriod;
                                tblass.assetno = GenerateAssetNo(CompnyCode);
                                tblass.assetdate = tbl.assetrecdate;
                                tblass.reftype = "Asset";
                                tblass.refoid = tbl.assetrefoid;
                                tblass.assetacctgoid = tbl.assetacctgoid;
                                tblass.assetpurchase = tbl.assetrecpurchase;
                                tblass.assetpurchaseidr = tbl.assetrecpurchase;
                                tblass.assetpurchaseusd = 1;
                                tblass.assetvalue = tbl.assetrecvalue;
                                tblass.assetvalueidr = tbl.assetrecvalue;
                                tblass.assetvalueusd = 1;
                                tblass.assetaccumdep = 0;
                                tblass.assetaccumdepidr = 0;
                                tblass.assetaccumdepusd = 0;
                                tblass.accumdepacctgoid = tbl.accumdepacctgoid;
                                tblass.assetdepmonth = tbl.assetrecdepmonth;
                                tblass.assetdepvalue = tbl.assetrecdepvalue;
                                tblass.assetdepvalueidr = tbl.assetrecdepvalue;
                                tblass.assetdepvalueusd = 1;
                                tblass.depacctgoid = tbl.depacctgoid;
                                tblass.assetmststatus = tbl.assetrecmststatus;
                                tblass.assetmstres1 = tbl.assetrectype;
                                tblass.assetmstres2 = tbl.assetrecmstoid.ToString();
                                tblass.assetmstres3 = "";
                                tblass.assetmstnote = "";
                                tblass.createuser = tbl.upduser;
                                tblass.createtime = tbl.updtime;
                                tblass.upduser = tbl.upduser;
                                tblass.updtime = tbl.updtime;
                                tblass.deptoid = tbl.deptoid;
                                tblass.curroid = tbl.curroid;
                                tblass.rateoid = tbl.rateoid;
                                tblass.rate2oid = tbl.rate2oid;
                                tblass.assetdepdate = tbl.assetrecdate;
                                tblass.inprogacctgoid = 0;
                                tblass.closeuser = "";
                                tblass.closetime = new DateTime(1900, 01, 01);
                                tblass.assetmstflag = "";
                                tblass.solduser = "";
                                tblass.soldtime = new DateTime(1900, 01, 01);
                                tblass.shipmentuser = "";
                                tblass.shipmenttime = new DateTime(1900, 01, 01);
                                tblass.disposeuser = "";
                                tblass.disposetime = new DateTime(1900, 01, 01);
                                tblass.shipmentamt = 0;
                                tblass.shipmentamtidr = 0;
                                tblass.shipmentamtusd = 0;
                                tblass.revaluser = "";
                                tblass.revaltime = new DateTime(1900, 01, 01);
                                tblass.disposeacctgoid = 0;
                                tblass.mtrwhoid = mrassetwhoid;

                                db.QL_assetmst.Add(tblass);
                                db.SaveChanges();

                                assetmstoid_backup = tblass.assetmstoid;

                                //// Insert QL_conmat
                                //db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "ASCO", "QL_trnassetrecmst", tbl.assetrecmstoid, tbl.assetrefoid, "FIXED ASSET", whoid, tbl.assetrecqty * -1, "Asset Conf", "Asset Conf No. " + tbl.assetrecno + "", Session["UserID"].ToString(), tbl.refno, (tbl.assetrecvalue * tbl.assetrecqty), 0, 0, null, tbl.assetrecmstoid, 0, (tbl.assetrecvalue * tbl.assetrecqty), tbl.serialnumber));
                                //db.SaveChanges();
                            }
                            else
                            {
                                QL_assetmst tblass = db.QL_assetmst.Where(a => a.cmpcode == CompnyCode && a.assetmstoid == tbl.mrassetmstoid).FirstOrDefault();
                                if (tblass != null)
                                {
                                    tblass.assetacctgoid = tbl.assetacctgoid;
                                    tblass.assetpurchase = tbl.assetrecpurchase;
                                    tblass.assetpurchaseidr = tbl.assetrecpurchase * cRate.GetRateMonthlyIDRValue;
                                    tblass.assetpurchaseusd = tbl.assetrecpurchase * cRate.GetRateMonthlyUSDValue;
                                    tblass.assetvalue = tbl.assetrecvalue;
                                    tblass.assetvalueidr = tbl.assetrecvalue * cRate.GetRateMonthlyIDRValue;
                                    tblass.assetvalueusd = tbl.assetrecvalue * cRate.GetRateMonthlyUSDValue;
                                    tblass.accumdepacctgoid = tbl.accumdepacctgoid;
                                    tblass.assetdepmonth = tbl.assetrecdepmonth;
                                    tblass.assetdepvalue = tbl.assetrecdepvalue;
                                    tblass.assetdepvalueidr = tbl.assetrecdepvalue;
                                    tblass.assetdepvalueusd = 1;
                                    tblass.depacctgoid = tbl.depacctgoid;
                                    tblass.assetmststatus = tbl.assetrecmststatus;
                                    tblass.assetmstres1 = tbl.assetrectype;
                                    tblass.assetmstres2 = tbl.assetrecmstoid.ToString();
                                    tblass.createuser = tbl.upduser;
                                    tblass.createtime = tbl.updtime;
                                    tblass.upduser = tbl.upduser;
                                    tblass.updtime = tbl.updtime;
                                    tblass.deptoid = tbl.deptoid;
                                    tblass.curroid = tbl.curroid;
                                    tblass.rateoid = tbl.rateoid;
                                    tblass.rate2oid = tbl.rate2oid;
                                    tblass.assetdepdate = tbl.assetrecdate;
                                    tblass.inprogacctgoid = tbl.assetacctgoid;
                                    db.Entry(tbl).State = EntityState.Modified;
                                    db.SaveChanges();

                                    assetmstoid_backup = tblass.assetmstoid;
                                }
                                
                            }   

                            QL_assetdtl dtlass;
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                dtlass = new QL_assetdtl();
                                dtlass.cmpcode = tbl.cmpcode;
                                dtlass.assetdtloid = assetdtloid++;
                                dtlass.assetmstoid = assetmstoid_backup;
                                dtlass.assetdtlseq = i + 1;
                                dtlass.assetmonth = dtDtl[i].assetrecmonth;
                                dtlass.assetyear = dtDtl[i].assetrecyear;
                                dtlass.assetperiod = dtDtl[i].assetrecperiod;
                                dtlass.assetperiodvalue = dtDtl[i].assetrecperiodvalue;
                                dtlass.assetperiodvalueidr = dtDtl[i].assetrecperiodvalue;
                                dtlass.assetperiodvalueusd = 1;
                                dtlass.periodacctgoid = dtDtl[i].periodacctgoid;
                                dtlass.assetperioddep = dtDtl[i].assetrecperioddep;
                                dtlass.assetperioddepidr = dtDtl[i].assetrecperioddep;
                                dtlass.assetperioddepusd = 1;
                                dtlass.perioddepacctgoid = dtDtl[i].perioddepacctgoid;
                                dtlass.assetdtlstatus = "";
                                dtlass.assetdtlres1 = "";
                                dtlass.assetdtlres2 = "";
                                dtlass.assetdtlres3 = "";
                                dtlass.assetdtlnote = "";
                                dtlass.postuser = "";
                                dtlass.postdate = new DateTime(1900,01,01);
                                dtlass.upduser = tbl.upduser;
                                dtlass.updtime = tbl.updtime;
                                dtlass.revalflag = "";

                                db.QL_assetdtl.Add(dtlass);
                                db.SaveChanges();
                            }

                            //// Insert QL_trnglmst
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, tbl.assetrectype + " | " + tbl.assetrecno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            //db.SaveChanges();

                            //var glseq = 1;

                            //// Insert QL_trngldtl
                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.assetacctgoid, "D", (tbl.assetrecvalue * tbl.assetrecqty), tbl.assetrecno, tbl.assetrectype + " | " + tbl.assetrecno + tbl.assetrecmstnote + "", "Post", Session["UserID"].ToString(), servertime, (tbl.assetrecvalue * tbl.assetrecqty) * cRate.GetRateMonthlyIDRValue, 0, "QL_trnassetrecmst " + tbl.assetrecmstoid, null, null, null, 0));
                            //db.SaveChanges();

                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iStockAcctgOid, "C", (tbl.assetrecvalue * tbl.assetrecqty), tbl.assetrecno, tbl.assetrectype + " | " + tbl.assetrecno + tbl.assetrecmstnote + "", "Post", Session["UserID"].ToString(), servertime, (tbl.assetrecvalue * tbl.assetrecqty) * cRate.GetRateMonthlyIDRValue, 0, "QL_trnassetrecmst " + tbl.assetrecmstoid, null, null, null, 0));
                            //db.SaveChanges();

                            //sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();

                            //sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();

                            //sSql = "UPDATE QL_mstoid SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + assetmstoid + " WHERE tablename='QL_assetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (assetdtloid - 1) + " WHERE tablename='QL_assetdtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnassetrecdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tbl.assetrecmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.assetrecmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: FA/Close
        [HttpPost, ActionName("Close")]
        [ValidateAntiForgeryToken]
        public ActionResult CloseConfirmed(int? id, decimal assetrecvalue, int accumdepacctgoid, int assetrecdepmonth, decimal assetrecdepvalue, int depacctgoid, string assetrecmstnote, int deptoid, int assetacctgoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var cRate = new ClassRate();
            QL_assetmst tbl = db.QL_assetmst.Find(CompnyCode, id);
            //var tblmst = db.QL_trnassetrecmst.Where(x => x.mrassetmstoid == id && x.mrassetdtloid == 0).ToList();

            //List<listassetmst> tblmst = new List<listassetmst>();
            //sSql = "SELECT * FROM QL_trnassetrecmst where mrassetmstoid = " + id + " AND mrassetdtloid = 0";
            //tblmst = db.Database.SqlQuery<listassetmst>(sSql).ToList();

            //var assetrecvalue = tblmst.(x => x.assetrecvalue);
            //var accumdepacctgoid = rabdtl.Sum(x => x.rabtaxamt);
            //var assetrecdepmonth = soitemtotaltaxamt;
            //var assetrecdepvalue = soitemtotalamt + soitemtotaltaxamt;
            //var depacctgoid = soitemtotalnetto;
            //var assetrecmstnote = soitemtotalnetto;
            //var assetacctgoid = soitemtotalnetto;

            var servertime = ClassFunction.GetServerTime();
            DateTime sDate = servertime;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            //is Input Detail Valid
            string msg = "";
            List<assetrecdtl> dtDtl = (List<assetrecdtl>)Session["QL_trnassetrecdtl"];
            if (dtDtl == null)
                msg = "Please fill detail data!";
            else if (dtDtl.Count <= 0)
                msg = "Please fill detail data!";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].assetrecdtlnote))
                            dtDtl[i].assetrecdtlnote = "";
                    }
                }
            }

            string result = "sukses";
            
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                var assetdtloid = ClassFunction.GenerateID("QL_assetdtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //sSql = "UPDATE QL_assetmst SET assetpurchase=" + tblmst.assetrecvalue + ", assetpurchaseidr=" + tblmst.assetrecvalue + ", assetvalue=" + tblmst.assetrecvalue + ", assetvalueidr=" + tblmst.assetrecvalue + ", accumdepacctgoid=" + tblmst.accumdepacctgoid + ", assetdepmonth=" + tblmst.assetrecdepmonth + ", assetdepvalue=" + tblmst.assetrecdepvalue + ", assetdepvalueidr=" + tblmst.assetrecdepvalue + ", depacctgoid=" + tblmst.depacctgoid + ", assetmstres1='Asset In Progress Closed', assetmstnote='" + tblmst.assetrecmstnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime= '"+ servertime + "', deptoid=" + tblmst.deptoid + ", curroid=1, rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", assetacctgoid=" + tblmst.assetacctgoid + " WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + id +"";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges(); 
                                               
                        tbl.assetpurchase = assetrecvalue;
                        tbl.assetpurchaseidr = assetrecvalue;
                        tbl.assetvalue = assetrecvalue;
                        tbl.assetvalueidr = assetrecvalue;
                        tbl.accumdepacctgoid = accumdepacctgoid;
                        tbl.assetdepmonth = assetrecdepmonth;
                        tbl.assetdepvalue = assetrecdepvalue;
                        tbl.assetdepvalueidr = assetrecdepvalue;
                        tbl.depacctgoid = depacctgoid;
                        tbl.assetmstres1 = "Asset In Progress Closed";
                        tbl.assetmstnote = assetrecmstnote;
                        tbl.closeuser = Session["UserID"].ToString();
                        tbl.closetime = servertime;
                        tbl.deptoid = deptoid;
                        tbl.curroid = 1;
                        tbl.rateoid = 1;
                        tbl.rate2oid = cRate.GetRateMonthlyOid;
                        tbl.assetacctgoid = assetacctgoid;
                        tbl.updtime = servertime;
                        tbl.upduser = Session["UserID"].ToString();
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        var trndtl = db.QL_assetdtl.Where(a => a.assetmstoid == id);
                        db.QL_assetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        QL_assetdtl dtlass;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtlass = new QL_assetdtl();
                            dtlass.cmpcode = tbl.cmpcode;
                            dtlass.assetdtloid = assetdtloid++;
                            dtlass.assetmstoid = tbl.assetmstoid;
                            dtlass.assetdtlseq = i + 1;
                            dtlass.assetmonth = dtDtl[i].assetrecmonth;
                            dtlass.assetyear = dtDtl[i].assetrecyear;
                            dtlass.assetperiod = dtDtl[i].assetrecperiod;
                            dtlass.assetperiodvalue = dtDtl[i].assetrecperiodvalue;
                            dtlass.assetperiodvalueidr = dtDtl[i].assetrecperiodvalue;
                            dtlass.assetperiodvalueusd = 1;
                            dtlass.periodacctgoid = dtDtl[i].periodacctgoid;
                            dtlass.assetperioddep = dtDtl[i].assetrecperioddep;
                            dtlass.assetperioddepidr = dtDtl[i].assetrecperioddep;
                            dtlass.assetperioddepusd = 1;
                            dtlass.perioddepacctgoid = dtDtl[i].perioddepacctgoid;
                            dtlass.assetdtlstatus = "";
                            dtlass.assetdtlres1 = "";
                            dtlass.assetdtlres2 = "";
                            dtlass.assetdtlres3 = "";
                            dtlass.assetdtlnote = "";
                            dtlass.postuser = "";
                            dtlass.postdate = new DateTime(1900, 01, 01);
                            dtlass.upduser = tbl.upduser;
                            dtlass.updtime = tbl.updtime;
                            dtlass.revalflag = "";

                            db.QL_assetdtl.Add(dtlass);
                            db.SaveChanges();
                        }                        

                        sSql = "UPDATE QL_ID SET lastoid=" + (assetdtloid - 1) + " WHERE tablename='QL_assetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //// Insert QL_trnglmst
                        //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, "Asset In Prog. Closed|No. " + tbl.assetno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                        //db.SaveChanges();

                        //var glseq = 1;

                        //// Insert QL_trngldtl
                        //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, assetacctgoid, "D", assetrecvalue, tbl.assetno, "Asset In Prog. Closed|No. " + tbl.assetno + " " + assetrecmstnote + "", "Post", Session["UserID"].ToString(), servertime, assetrecvalue * cRate.GetRateMonthlyIDRValue, 0, "QL_assetmst " + tbl.assetmstoid, null, null, null, 0));
                        //db.SaveChanges();

                        //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.assetacctgoid, "C", assetrecvalue, tbl.assetno, "Asset In Prog. Closed|No. " + tbl.assetno + " " + assetrecmstnote + "", "Post", Session["UserID"].ToString(), servertime, assetrecvalue * cRate.GetRateMonthlyIDRValue, 0, "QL_assetmst " + tbl.assetmstoid, null, null, null, 0));
                        //db.SaveChanges();

                        //sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        //sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnassetrecmst tblmst = db.QL_trnassetrecmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tblmst.assetrectype == "Asset Confirmation")
                        {
                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres2='' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid IN (SELECT mrassetdtloid FROM QL_trnassetrecmst where assetrecmstoid = " + id + ") ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres2='' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnassetrecmst where assetrecmstoid = " + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }  
                        else if (tblmst.assetrectype == "Asset Transform")
                        {
                            sSql = "UPDATE QL_trntransformdtl2 SET transformdtl2res2='' WHERE cmpcode='" + CompnyCode + "' AND transformdtl2oid IN (SELECT mrassetdtloid FROM QL_trnassetrecmst where assetrecmstoid = " + id + ") ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trntransformmst SET transformmstres2='' WHERE cmpcode='" + CompnyCode + "' AND transformmstoid IN (SELECT mrassetmstoid FROM QL_trnassetrecmst where assetrecmstoid = " + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trnassetrecdtl.Where(a => a.assetrecmstoid == id);
                        db.QL_trnassetrecdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnassetrecmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        private void InitDDL(QL_trnassetrecmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            var cmp = cmpcode.First().Value;
            if (!string.IsNullOrEmpty(tbl.cmpcode) && cmpcode.Count() > 0)
                cmp = tbl.cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(GetQueryInitDDLDepartment(cmp)).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            var assetacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_ASSET")).ToList(), "acctgoid", "acctgdesc", tbl.assetacctgoid);
            ViewBag.assetacctgoid = assetacctgoid;

            var accumdepacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_ASSET_ACCUM")).ToList(), "acctgoid", "acctgdesc", tbl.accumdepacctgoid);
            ViewBag.accumdepacctgoid = accumdepacctgoid;

            var depacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_DEPRECIATION")).ToList(), "acctgoid", "acctgdesc", tbl.depacctgoid);
            ViewBag.depacctgoid = depacctgoid;
        }

        private string GetQueryInitDDLDepartment(string cmp)
        {
            var result = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE' ORDER BY deptname";
            return result;
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void FillAdditionalField(QL_trnassetrecmst tbl)
        {
           
            //ViewBag.personname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE cmpcode='" + tbl.cmpcode + "' AND usoid='" + tbl.personoid + "'").FirstOrDefault();            
            
            if (tbl.assetrectype == "Asset Confirmation")
            {
                ViewBag.mrassetno = db.Database.SqlQuery<string>("SELECT mrassetno FROM QL_trnmrassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + tbl.mrassetmstoid + "").FirstOrDefault();

                ViewBag.assetreflongdesc = db.Database.SqlQuery<string>("SELECT matgenlongdesc FROM QL_mstmatgen WHERE cmpcode='" + CompnyCode + "' AND matgenoid=" + tbl.assetrefoid + "").FirstOrDefault();

                ViewBag.mrassetqty = db.Database.SqlQuery<decimal>("SELECT (mrd.mrassetqty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.mrassetdtloid ), 0)  /* - ISNULL((SELECT SUM(pretassetqty) FROM QL_trnpretassetdtl pretd INNER JOIN QL_trnpretassetmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretassetmstoid=pretd.pretassetmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrassetdtloid=mrd.mrassetdtloid AND pretm.pretassetmststatus<>'Rejected'), 0)*/) AS mrassetqty FROM QL_trnmrassetdtl mrd INNER JOIN QL_mstgen g ON g.genoid=mrd.mrassetunitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrd.mrassetmstoid=" + tbl.mrassetmstoid + " AND mrd.mrassetdtloid=" + tbl.mrassetdtloid + " AND ISNULL(mrd.mrassetdtlres2, '')='' AND ISNULL(mrd.mrassetdtlres1, '')=''").FirstOrDefault();

                ViewBag.assetrecunit = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid=" + tbl.assetrecunitoid + "").FirstOrDefault();

                ViewBag.mrassetwhoid = db.Database.SqlQuery<int>("select mrassetwhoid from QL_trnmrassetmst where mrassetmstoid = " + tbl.mrassetmstoid + "").FirstOrDefault();
            }
            else if (tbl.assetrectype == "Asset Transform")
            {
                ViewBag.transformno = db.Database.SqlQuery<string>("SELECT transformno FROM QL_trntransformmst WHERE cmpcode='" + tbl.cmpcode + "' AND transformmstoid=" + tbl.mrassetmstoid + "").FirstOrDefault();

                ViewBag.assetreflongdesc = db.Database.SqlQuery<string>("SELECT matgenlongdesc FROM QL_mstmatgen WHERE cmpcode='" + CompnyCode + "' AND matgenoid=" + tbl.assetrefoid + "").FirstOrDefault();

                ViewBag.mrassetqty = db.Database.SqlQuery<decimal>("SELECT (mrd.transformdtl2qty - ISNULL((SELECT SUM(farm.assetrecqty) FROM QL_trnassetrecmst farm WHERE farm.cmpcode=mrd.cmpcode AND farm.mrassetdtloid=mrd.transformdtl2oid ), 0)) AS mrassetqty FROM QL_trntransformdtl2 mrd INNER JOIN QL_m05gn g ON g.gnoid=mrd.transformdtl2unitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrd.transformmstoid=" + tbl.mrassetmstoid + " AND mrd.transformdtl2oid=" + tbl.mrassetdtloid + " AND ISNULL(mrd.transformdtl2res2, '')=''").FirstOrDefault();

                ViewBag.assetrecunit = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid=" + tbl.assetrecunitoid + "").FirstOrDefault();

                ViewBag.mrassetwhoid = db.Database.SqlQuery<int>("select transformdtl2whoid from QL_trntransformdtl2 where transformdtl2oid = " + tbl.mrassetdtloid + "").FirstOrDefault();
            }
            else if (tbl.assetrectype == "Asset In Progress Closed")
            {
                ViewBag.assetno = db.Database.SqlQuery<string>("SELECT assetno FROM QL_assetmst WHERE cmpcode='" + tbl.cmpcode + "' AND assetmstoid=" + tbl.mrassetmstoid + "").FirstOrDefault();
            }
            
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptFAConfirmation.rpt"));

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE farm.cmpcode='" + CompnyCode + "' AND farm.assetrecmstoid IN (" + id + ")");


            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "apassetMaterialReport.pdf");
        }

        public ActionResult PrintReport2(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJFAConfirm.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.assetrecmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SJFAConfirmReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}