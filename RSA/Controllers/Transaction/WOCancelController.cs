﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.Transaction
{
    public class WOCancelController : Controller
    {
        // GET: WOCancel
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        public class womst
        {
            public string wono { get; set; }
            public int womstoid { get; set; }
            public DateTime wodate { get; set; }
            public string custname { get; set; }
            public string sono { get; set; }
            public string itemdesc { get; set; }
            public decimal woqty { get; set; }
            public string womstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetWOData(string wotype)
        {
            List<womst> tbl = new List<womst>();
            if (wotype == "item")
            {
                sSql = "select wm.womstoid, wono, wodate, custname, soitemno sono, itemlongdesc itemdesc, spkqty woqty, wm.remark womstnote from QL_trnwomst wm inner join QL_mstitem m on m.itemoid=wm.itemoid inner join QL_trnsoitemdtl sd on sd.soitemdtloid=wm.sodtloid inner join QL_trnsoitemmst sm on sm.soitemmstoid=wm.somstoid inner join QL_mstcust c on c.custoid=wm.custoid where wm.sotype='QL_trnsoitemmst' and wm.womstoid not in(select womstoid from QL_trnprodresmst x where isnull(x.prodresmstres1,'')<>'Sheet')";
            }
            else
            {
                sSql = "select wm.womstoid, wono, wodate, custname, sorawno sono, sodtldesc itemdesc, spkqty woqty, wm.remark womstnote from QL_trnwomst wm inner join QL_trnsorawdtl sd on sd.sorawdtloid=wm.sodtloid inner join QL_trnsorawmst sm on sm.sorawmstoid=wm.somstoid inner join QL_mstcust c on c.custoid=wm.custoid where wm.sotype='QL_trnsorawmst' and wm.womstoid not in(select womstoid from QL_trnprodresmst x where isnull(x.prodresmstres1,'')='Sheet')";
            }
            tbl = db.Database.SqlQuery<womst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.womstoid = 0;
            InitDDL();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int womstoid, string reason)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            var tbl = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == womstoid);
            if (tbl == null) msg += "Silahkan Pilih SPK!<br>";
            if (string.IsNullOrWhiteSpace(reason)) msg += "Silahkan Isi Reason!<br>";

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.womststatus = "Cancel";
                        tbl.woclosingnote = reason;
                        tbl.woclosingdate = servertime;
                        tbl.woclosinguser = Session["UserID"].ToString();
                        db.Entry(tbl).State = EntityState.Modified;

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data Canceled!<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}