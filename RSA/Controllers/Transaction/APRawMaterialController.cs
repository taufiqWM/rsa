﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class APRawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class aprawmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int aprawmstoid { get; set; }
            public string aprawno { get; set; }
            public DateTime aprawdate { get; set; }
            public DateTime aprawdatetakegiro { get; set; }
            public DateTime duedate { get; set; }
            public string suppname { get; set; }
            public string aprawmststatus { get; set; }
            public string aprawmstnote { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal aprawgrandtotal { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal aprawgrandtotalsupp { get; set; }
            public string divname { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
        }

        public class aprawdtl
        {
            public int aprawdtlseq { get; set; }
            public int porawmstoid { get; set; }
            public string porawno { get; set; }
            public int porawdtloid { get; set; }
            public int mrrawmstoid { get; set; }
            public string mrrawno { get; set; }
            public int mrrawdtloid { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal aprawqty { get; set; }
            public int aprawunitoid { get; set; }
            public string aprawunit { get; set; }
            public decimal aprawprice { get; set; }
            public decimal aprawdtlamt { get; set; }
            public string aprawdtldisctype { get; set; }
            public decimal aprawdtldiscvalue { get; set; }
            public decimal aprawdtldiscamt { get; set; }
            public decimal aprawdtlnetto { get; set; }
            public decimal aprawdtltaxamt{ get; set; }
            public string aprawdtlnote { get; set; }
            public string aprawdtlres1 { get; set; }
            public string aprawdtlres2 { get; set; }
        }

        private void InitDDL(QL_trnaprawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var aprawpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.aprawpaytypeoid);
            ViewBag.aprawpaytypeoid = aprawpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaprawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaprawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp, int aprawmstoid, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            //sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid FROM QL_mstsupp s INNER JOIN QL_trnregistermst regm ON regm.suppoid=s.suppoid INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode=regm.cmpcode AND mrm.registermstoid=regm.registermstoid WHERE regm.cmpcode='" + cmp + "' AND registertype='RAW' AND registermststatus='Closed' AND ISNULL(mrrawmstres1, '')='' AND mrm.mrrawmststatus='Post' AND regm.registermstoid NOT IN (SELECT DISTINCT registermstoid FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid WHERE apd.cmpcode='" + cmp + "' AND aprawmststatus<>'Rejected' AND apm.aprawmstoid <> " + aprawmstoid + ") ORDER BY suppcode ";
            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid " +
                "FROM QL_mstsupp s " +
                "INNER JOIN QL_trnporawmst regm ON regm.suppoid = s.suppoid " +
                "INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.porawmstoid = regm.porawmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND porawmststatus IN('Approved', 'Closed') and s.divgroupoid=" + divgroupoid + " and mrm.divgroupoid=" + divgroupoid + " AND ISNULL(mrrawmstres1, '')= '' AND mrm.mrrawmststatus = 'Post' ORDER BY suppcode";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int porawmstoid { get; set; }
            public string porawno { get; set; }
            public DateTime porawdate { get; set; }
            public string porawdocrefno { get; set; }
            public string porawmstnote { get; set; }
            public string poflag { get; set; }
            public int mrrawmstoid { get; set; }
            public string mrrawno { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int aprawmstoid, int curroid, int divgroupoid)
        {
            List<pomst> tbl = new List<pomst>();
            cmp = CompnyCode;
            sSql = "SELECT DISTINCT regm.porawmstoid, regm.porawno, regm.porawdate, '' [porawdocrefno], ISNULL(regm.porawmstnote, '') [porawmstnote], mrm.mrrawmstoid, mrm.mrrawno " +
                "FROM QL_trnporawmst regm " +
                "INNER JOIN QL_trnporawdtl regd ON regd.cmpcode = regm.cmpcode AND regd.porawmstoid = regm.porawmstoid " +
                "INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.porawmstoid = regm.porawmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND regm.suppoid = " + suppoid + " AND regm.porawmststatus IN ('Approved', 'Closed') AND ISNULL(mrrawmstres1, '') = '' AND mrm.mrrawmststatus = 'Post' AND regm.porawmstoid NOT IN(SELECT pretm.porawmstoid FROM QL_trnpretrawmst pretm WHERE pretm.cmpcode = '" + cmp + "' AND pretrawmststatus IN ('In Process', 'Revised', 'In Approval')) AND regm.curroid = " + curroid + " and regm.divgroupoid=" + divgroupoid + " ORDER BY regm.porawmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int porawmstoid, string aprawdtlres1, string aprawdtlres2)
        {

            List<aprawdtl> tbl = new List<aprawdtl>();
           
            sSql = "SELECT aprawdtlseq, porawmstoid, porawno, porawdtloid, mrrawmstoid, mrrawno, mrrawdtloid, matrawoid, matrawcode, matrawlongdesc, aprawqty, aprawunitoid, aprawunit, aprawprice,(aprawqty * aprawprice) AS aprawdtlamt, aprawdtldisctype, aprawdtldiscvalue, " +
                "(CASE aprawdtldisctype WHEN 'P' THEN(aprawqty * aprawprice) * (aprawdtldiscvalue / 100) ELSE(aprawdtldiscvalue * aprawqty) END)aprawdtldiscamt, " +
                "((aprawqty * aprawprice) - (CASE aprawdtldisctype WHEN 'P' THEN(aprawqty * aprawprice) * (aprawdtldiscvalue / 100) ELSE(aprawdtldiscvalue * aprawqty) END)) AS aprawdtlnetto, " +
                "(CASE porawtaxtype WHEN 'TAX' THEN(((aprawqty * aprawprice) - (CASE aprawdtldisctype WHEN 'P' THEN(aprawqty * aprawprice) * (aprawdtldiscvalue / 100) ELSE(aprawdtldiscvalue * aprawqty) END)) *porawtaxamt) / 100 ELSE 0 END) aprawdtltaxamt, " +
                "aprawdtlnote ,aprawdtlres1 ,aprawdtlres2 " +
                "FROM (SELECT 0 AS aprawdtlseq, regm.porawmstoid, regm.porawno, regd.porawdtloid, mrm.mrrawmstoid, mrrawno, mrrawdtloid, mrd.matrawoid, matrawcode, matrawlongdesc, (mrrawqty - ISNULL((SELECT SUM(pretrawqty) FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretrawmstoid = pretd.pretrawmstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mrrawdtloid = mrd.mrrawdtloid AND pretrawmststatus <> 'Rejected'), 0.0)) AS aprawqty, mrrawunitoid AS aprawunitoid, gendesc AS aprawunit, porawprice aprawprice, porawdtldisctype aprawdtldisctype, (CASE porawdtldisctype WHEN 'A' THEN(porawdtldiscvalue / porawqty) ELSE porawdtldiscvalue END) AS aprawdtldiscvalue, porawtaxtype, porawtaxamt, '' aprawdtlnote, '" + aprawdtlres1 + "' AS aprawdtlres1, '" + aprawdtlres2 + "' AS aprawdtlres2 FROM QL_trnporawdtl regd INNER JOIN QL_trnporawmst regm ON regm.cmpcode = regd.cmpcode AND regm.porawmstoid = regd.porawmstoid INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode = regd.cmpcode  AND mrm.porawmstoid = regd.porawmstoid INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode = regd.cmpcode AND mrm.mrrawmstoid = mrd.mrrawmstoid  AND mrd.porawdtloid = regd.porawdtloid INNER JOIN QL_mstmatraw m ON m.matrawoid = mrd.matrawoid INNER JOIN QL_mstgen g ON genoid = matrawunitoid WHERE regd.cmpcode = '" + cmp + "' AND mrm.mrrawmstoid = " + porawmstoid + " AND ISNULL(mrrawdtlres1, '')= '' AND ISNULL(mrrawmstres1, '')= '' AND mrrawmststatus = 'Post' AND mrrawdtlstatus = '') AS tbl";

            tbl = db.Database.SqlQuery<aprawdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<aprawdtl> dtDtl)
        {
            Session["QL_trnaprawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnaprawdtl"] == null)
            {
                Session["QL_trnaprawdtl"] = new List<aprawdtl>();
            }

            List<aprawdtl> dataDtl = (List<aprawdtl>)Session["QL_trnaprawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnaprawmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            //ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnaprawmst WHERE cmpcode='" + tbl.cmpcode + "' AND aprawmstoid=" + tbl.aprawmstoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: APRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "apm", divgroupoid, "divgroupoid");
            sSql = "SELECT aprawmstoid, aprawno, aprawdate, suppname, aprawmststatus, aprawmstnote, divname, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'raw' AS formtype, 'False' AS checkvalue, aprawgrandtotal , aprawdatetakegiro,  CONVERT(DATETIME,DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g1.gendesc AS INT) END),aprawdate), 101) duedate FROM QL_trnaprawmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=apm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=aprawpaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "apm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "apm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND aprawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND aprawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND aprawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND aprawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND aprawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND aprawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND apm.createuser='" + Session["UserID"].ToString() + "'";

            sSql += "AND aprawmstoid>0 ORDER BY CONVERT(DATETIME, aprawdate) DESC, aprawmstoid DESC ";

            List<aprawmst> dt = db.Database.SqlQuery<aprawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnaprawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        [HttpPost]
        public ActionResult generateNo(string cmp, int apmstoid)
        {
            sSql = "SELECT aprawno FROM QL_trnaprawmst WHERE cmpcode='" + cmp + "' AND aprawmstoid=" + apmstoid;
            string sNoAP = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (string.IsNullOrEmpty(sNoAP))
            {

                string sNo = "APRM-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(aprawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnaprawmst WHERE cmpcode='" + cmp + "' AND aprawno LIKE '" + sNo + "%'";

                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

                sNo = sNo + sCounter;
                return Json(sNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(sNoAP, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: APRawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnaprawmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnaprawmst();
                tbl.cmpcode = CompnyCode;
                tbl.aprawmstoid = ClassFunction.GenerateID("QL_trnaprawmst");
                tbl.aprawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.aprawmststatus = "In Process";

                Session["QL_trnaprawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnaprawmst.Find(cmp, id);

                sSql = "SELECT aprawdtlseq, apd.porawmstoid, porawno, porawdtloid, apd.mrrawmstoid, mrrawno, mrrawdtloid, apd.matrawoid, matrawcode, matrawlongdesc, aprawqty, aprawunitoid, gendesc AS aprawunit, aprawprice, aprawdtlamt, aprawdtldisctype, aprawdtldiscvalue, aprawdtldiscamt, aprawdtlnetto, aprawdtltaxamt, aprawdtlnote, ISNULL(aprawdtlres1, '') AS aprawdtlres1, ISNULL(aprawdtlres2, '') AS aprawdtlres2 " +
                    "FROM QL_trnaprawdtl apd " +
                    "INNER JOIN QL_trnporawmst regm ON regm.cmpcode = apd.cmpcode AND regm.porawmstoid = apd.porawmstoid " +
                    "INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode = apd.cmpcode AND mrm.mrrawmstoid = apd.mrrawmstoid AND mrm.porawmstoid = regm.porawmstoid " +
                    "INNER JOIN QL_mstmatraw m ON m.matrawoid = apd.matrawoid " +
                    "INNER JOIN QL_mstgen g ON genoid = aprawunitoid " +
                    "WHERE aprawmstoid = " + id + " AND apd.cmpcode = '" + cmp + "' ORDER BY aprawdtlseq  ";
                Session["QL_trnaprawdtl"] = db.Database.SqlQuery<aprawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: APRawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnaprawmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.aprawno == null)
                tbl.aprawno = "";

            List<aprawdtl> dtDtl = (List<aprawdtl>)Session["QL_trnaprawdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnmrrawmst mrm WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + dtDtl[i].mrrawmstoid + " AND mrrawmststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND mrrawmstoid NOT IN (SELECT apd.mrrawmstoid FROM QL_trnaprawdtl apd WHERE apd.cmpcode=mrm.cmpcode AND aprawmstoid=" + tbl.aprawmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "PO No. " + dtDtl[i].porawno + " has been used by another data. Please cancel this transaction or use another PO No.!");
                    }
                }
            }

            var msg = "";

            cRate.SetRateValue(tbl.curroid, tbl.aprawdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.aprawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                //AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaprawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaprawmst' AND activeflag='ACTIVE'").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.aprawmststatus == "Revised")
                tbl.aprawmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnaprawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnaprawdtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.aprawtotalamtidr = (tbl.aprawtotalamt * decimal.Parse(tbl.aprawrate2toidr));
                tbl.aprawtotalamtusd = (tbl.aprawtotalamt * decimal.Parse(tbl.aprawrate2tousd));
                tbl.aprawtotaldiscidr = (tbl.aprawtotaldisc * decimal.Parse(tbl.aprawrate2toidr));
                tbl.aprawtotaldiscusd = (tbl.aprawtotaldisc * decimal.Parse(tbl.aprawrate2tousd));
                tbl.aprawtotaltaxidr = (tbl.aprawtotaltax * decimal.Parse(tbl.aprawrate2toidr));
                tbl.aprawtotaltaxusd = (tbl.aprawtotaltax * decimal.Parse(tbl.aprawrate2tousd));
                tbl.aprawgrandtotalidr = (tbl.aprawgrandtotal * decimal.Parse(tbl.aprawrate2toidr));
                tbl.aprawgrandtotalusd = (tbl.aprawgrandtotal * decimal.Parse(tbl.aprawrate2tousd));

                //tbl.cancelseq = 0;
                //tbl.apcloseuser = "";
                //tbl.apclosetime = DateTime.Parse("1/1/1900 00:00:00");
                if (tbl.aprawmstnote == null)
                    tbl.aprawmstnote = "";
                if (tbl.aprawtotaldisc == null)
                    tbl.aprawtotaldisc = 0;
                if (tbl.aprawtotaldiscidr == null)
                    tbl.aprawtotaldiscidr = 0;
                if (tbl.aprawtotaldiscusd == null)
                    tbl.aprawtotaldiscusd = 0;
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime= DateTime.Parse("1/1/1900 00:00:00");
                tbl.rejecttime= DateTime.Parse("1/1/1900 00:00:00");


                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnaprawmst.Find(tbl.cmpcode, tbl.aprawmstoid) != null)
                                tbl.aprawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.aprawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnaprawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.aprawmstoid + " WHERE tablename='QL_trnaprawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE MRDTL SET mrrawdtlstatus='' FROM (SELECT mrrawdtlstatus FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=mrd.cmpcode AND apd.mrrawmstoid=mrd.mrrawmstoid AND apd.mrrawdtloid=mrd.mrrawdtloid  WHERE apd.cmpcode='" + tbl.cmpcode + "' AND apd.aprawmstoid=" + tbl.aprawmstoid + ") AS MRDTL ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrrawmst SET mrrawmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnaprawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aprawmstoid=" + tbl.aprawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnaprawdtl.Where(a => a.aprawmstoid == tbl.aprawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnaprawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnaprawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].aprawdtlnote == null)
                                dtDtl[i].aprawdtlnote = "";

                            tbldtl = new QL_trnaprawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.aprawdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.aprawmstoid = tbl.aprawmstoid;
                            tbldtl.aprawdtlseq = i + 1;
                            tbldtl.porawdtloid = dtDtl[i].porawdtloid;
                            tbldtl.porawmstoid = dtDtl[i].porawmstoid;
                            tbldtl.mrrawdtloid = dtDtl[i].mrrawdtloid;
                            tbldtl.mrrawmstoid = dtDtl[i].mrrawmstoid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.aprawqty = dtDtl[i].aprawqty;
                            tbldtl.aprawunitoid = dtDtl[i].aprawunitoid;
                            tbldtl.aprawprice = dtDtl[i].aprawprice;
                            tbldtl.aprawdtlamt = dtDtl[i].aprawdtlamt;
                            tbldtl.aprawdtldisctype = dtDtl[i].aprawdtldisctype;
                            tbldtl.aprawdtldiscvalue = dtDtl[i].aprawdtldiscvalue;
                            tbldtl.aprawdtldiscamt = dtDtl[i].aprawdtldiscamt;
                            tbldtl.aprawdtlnetto = dtDtl[i].aprawdtlnetto;
                            tbldtl.aprawdtltaxamt = dtDtl[i].aprawdtltaxamt;
                            tbldtl.aprawdtlnote = dtDtl[i].aprawdtlnote;
                            tbldtl.aprawdtlstatus = "";
                            tbldtl.aprawdtlres1 = dtDtl[i].aprawdtlres1;
                            tbldtl.aprawdtlres2 = dtDtl[i].aprawdtlres2;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnaprawdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawdtloid=" + dtDtl[i].mrrawdtloid + " AND mrrawmstoid=" + dtDtl[i].mrrawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnmrrawmst SET mrrawmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + dtDtl[i].mrrawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnaprawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.aprawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AP-RAW" + tbl.aprawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnaprawmst";
                                tblApp.oid = tbl.aprawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.aprawmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            tbl.aprawmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: APRawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaprawmst tbl = db.QL_trnaprawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnaprawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aprawmstoid=" + tbl.aprawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmrrawmst SET mrrawmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnaprawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aprawmstoid=" + tbl.aprawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        

                        var trndtl = db.QL_trnaprawdtl.Where(a => a.aprawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnaprawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnaprawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPTrn.rpt"));

            //report.SetParameterValue("sRegister", "(SELECT registerno FROM QL_trnregistermst regm WHERE regm.cmpcode=apd.cmpcode AND regm.registermstoid=apd.registermstoid )");
            report.SetParameterValue("sRegister", "(SELECT porawno FROM QL_trnporawrmst regm WHERE regm.cmpcode=apd.cmpcode AND regm.porawmstoid=apd.porawmstoid )");
            report.SetParameterValue("sMatCode", "(SELECT matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=apd.matrawoid)");
            report.SetParameterValue("sMatDesc", "(SELECT matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=apd.matrawoid)");
            report.SetParameterValue("sType", "raw");
            report.SetParameterValue("sHeader", "A/P RAW MATERIAL PRINT OUT");
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + cmp + "' AND apm.aprawmstoid IN (" + id + ")");


            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "APRawMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}