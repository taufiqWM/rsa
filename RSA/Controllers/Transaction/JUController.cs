﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Transaction
{
    public class JUController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = "RCA";
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public class trnglmst
        {
            public string cmpcode { get; set; }
            public int glmstoid { get; set; }
            public string divgroup { get; set; }
            public string glother1 { get; set; }
            public DateTime gldate { get; set; }
            public string glflag { get; set; }
            public string glother3 { get; set; }
        }

        public class trngldtl
        {
            public int glseq { get; set; }
            public int acctgoid { get; set; }
            public string divgroup { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string gldbcr { get; set; }
            public decimal glamt { get; set; }
            public decimal glamtdb { get; set; }
            public decimal glamtcr { get; set; }
            public string glnote { get; set; }
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
        }

        private void InitDDL(QL_trnglmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            //sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            //if (Session["CompnyCode"].ToString() != CompnyCode)
            //    sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            //sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var glother2 = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.glother2);
            ViewBag.glother2 = glother2;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", null);
            ViewBag.groupoid = groupoid;
        }

        [HttpPost]
        public ActionResult InitDDLDiv(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLCurr(string sFilter)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcurr> tbl = new List<QL_mstcurr>();
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' " + sFilter + " AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstcurr>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<trngldtl> tbl = new List<trngldtl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, 0.0 glamt, 0.0 glamtdb, 0.0 glamtcr, '' glnote, 0 groupoid, '' groupdesc, 'D' gldbcr FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string glother1, string cmpcode, string ratetype, int glmstoid)
        {
            return Json(ClassFunction.ShowCOAPosting(glother1, cmpcode, ratetype, "QL_trnglmst " + glmstoid), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trngldtl> dtDtl)
        {
            Session["QL_trngldtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trngldtl"] == null)
            {
                Session["QL_trngldtl"] = new List<trngldtl>();
            }
            List<trngldtl> dataDtl = (List<trngldtl>)Session["QL_trngldtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnglmst tbl)
        {
            ViewBag.isgl1side = 0;
            if (tbl.gl1side == "Y")
            {
                ViewBag.isgl1side = 1;
            }
        }

        private string GenerateMemoNo2(string cmp, string gldate)
        {
            var sNo = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(gldate));
            if (CompnyCode != "")
            {
                sNo = "MJ/" + sDate.ToString("yy") + "/" + sDate.ToString("MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(glother1, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnglmst WHERE cmpcode='" + CompnyCode + "' AND ISNULL(glother1,'') LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
                sNo = sNo + sCounter;
            }
            return sNo;
        }

        [HttpPost]
 
        public ActionResult GenerateMemoNo(string cmp, string gldate)
        {
            var sNo = "";
            DateTime sDate = DateTime.Parse((gldate));
            if (CompnyCode != "")
            {
                sNo = "MJ/" + sDate.ToString("yy") + "/" + sDate.ToString("MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(glother1, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnglmst WHERE cmpcode='" + CompnyCode + "' AND ISNULL(glother1,'') LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
                sNo = sNo + sCounter;
            }
            return Json(sNo, JsonRequestBehavior.AllowGet);
        }

        // GET: glmstMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "glm", divgroupoid, "divgroupoid");
            sSql = "SELECT * FROM (";
            sSql += "SELECT glm.cmpcode, select gendesc from ql_mstgen where genoid=glm.divgroupoid) divgroup,glmstoid, glother1, gldate, glflag, glother3, glm.createuser FROM QL_trnglmst glm WHERE (ISNULL(glother1, '')<>'') ";
           
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND gldate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";                     
                }
            }
            else
            {
                sSql += " AND glflag IN ('In Process')";
            }
            sSql += sqlplus;
            sSql += ") tblGL ORDER BY CONVERT(DATETIME, gldate) DESC, glmstoid DESC";
            List<trnglmst> dt = db.Database.SqlQuery<trnglmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: glmstMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnglmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnglmst();
                tbl.cmpcode = CompnyCode;
                tbl.glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                tbl.gldate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.glflag = "In Process";
                Session["QL_trngldtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnglmst.Find(CompnyCode, id);
                Session["History"] = "";
                if (tbl == null)
                {
                    tbl = db.Database.SqlQuery<QL_trnglmst>("SELECT * FROM QL_trnglmst_hist WHERE cmpcode='" + CompnyCode + "' AND glmstoid=" + id).FirstOrDefault();
                    Session["History"] = "Y";
                }

                sSql = "SELECT * FROM (";
                sSql += "SELECT glseq, acctggrp1 AS acctggroup, gld.acctgoid, acctgcode, acctgdesc, gldbcr, (CASE gldbcr WHEN 'D' THEN glamt ELSE 0.0 END) AS glamtdb, (CASE gldbcr WHEN 'D' THEN 0.0 ELSE glamt END) AS glamtcr, glnote, ISNULL(groupoid, 0) AS groupoid, ISNULL((SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dg WHERE dg.cmpcode=gld.cmpcode AND dg.groupoid=gld.groupoid), 'None') AS groupdesc FROM QL_trngldtl gld INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.cmpcode='" + CompnyCode + "' AND glmstoid=" + id + ") tblGL ORDER BY glseq";
                var tbldtl = db.Database.SqlQuery<trngldtl>(sSql).ToList();
                Session["QL_trngldtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: glmstMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnglmst tbl, string action, string closing, string tglmst, decimal totaldebit, decimal totalcredit, string gl1side)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.glother1 == null)
                tbl.glother1 = "";

            if (action == "Create")
            {
                tbl.glother1 = GenerateMemoNo2(CompnyCode, tglmst);
            }
                
            List<trngldtl> dtDtl = (List<trngldtl>)Session["QL_trngldtl"];
            if (tbl.glother2 == "")
                ModelState.AddModelError("", "Please select CURRENCY field!");
            if (totaldebit > 0 && totalcredit > 0)
            {
                if (totaldebit != totalcredit)
                    ModelState.AddModelError("", "TOTAL DEBIT must be equal with TOTAL CREDIT!");
            }
            else
            {
                ModelState.AddModelError("", "Both of TOTAL DEBIT dan TOTAL CREDIT must be more than 0!");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].acctgoid == 0)
                            ModelState.AddModelError("", "Please select COA field!");
                        //if (dtDtl[i].groupoid == 0)
                            //ModelState.AddModelError("", "Please select DIVISION field!");
                    }
                }
            }

            try
            {
                tbl.gldate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("aritemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            //var cRate = new ClassRate();
            DateTime sDatePost = Convert.ToDateTime("1/1/1900");
            if (tbl.glflag == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, tbl.gldate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(tbl.gldate.Month).ToString() + " " + tbl.gldate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.glflag = "In Process";
                }
                
                sDatePost = tbl.gldate;
                }

                if (!ModelState.IsValid)
                tbl.glflag = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnglmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trnglmst WHERE glmstoid=" + tbl.glmstoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trnglmst");
                    }
                }

                var dtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.rateoid = 0;
                tbl.rate2oid = 0;
                tbl.glrateidr = 1;
                tbl.glrateusd = 1;
                tbl.glrate2idr = 1;
                tbl.glrate2usd = 1;
                tbl.type = "";
                tbl.glres = "";
                tbl.postdate = sDatePost;
                tbl.glnote = "Memo|" + tbl.glother1 + "";
                tbl.gl1side = gl1side;
                string Penanda = "";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trnglmst.Find(tbl.cmpcode, tbl.glmstoid) != null)
                                tbl.glmstoid = mstoid;
                            Session["History"] = "";
                            tbl.cmpcode = CompnyCode;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.gldate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnglmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                           
                            var trndtl = db.QL_trngldtl.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trngldtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtDtl[i].gldbcr = (dtDtl[i].glamtdb > 0 ? "D" : "C");
                            if (Penanda == "")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, dtloid++, (i + 1), tbl.glmstoid, dtDtl[i].acctgoid, dtDtl[i].gldbcr, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb : dtDtl[i].glamtcr), tbl.glother1, dtDtl[i].glnote, tbl.glflag, tbl.upduser, tbl.updtime, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : 1) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) :1)), (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : 1) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : 1)), "QL_trnglmst " + tbl.glmstoid + "", "", "", "", dtDtl[i].groupoid,tbl.divgroupoid??0));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.glmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: glmstMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnglmst tbl = db.QL_trnglmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trngldtl.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                        db.QL_trngldtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnglmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMemoJournal.rpt"));
            report.SetParameterValue("sWhere", " WHERE glm.cmpcode='" + CompnyCode + "' AND ISNULL(glm.glother1,'')<>'' AND glm.glmstoid IN (" + id + ")");
            report.SetParameterValue("BU", CompnyName);
            report.SetParameterValue("PrintUserID", Session["UserID"]);
            report.SetParameterValue("PrintUserName", Session["UserID"]);
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "MemorialJournalPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}