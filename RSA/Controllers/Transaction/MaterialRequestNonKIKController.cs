﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class MaterialRequestNonKIKController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class approvalperson
        {
            public string matreqmstres3 { get; set; }
        }

        public class matreqmst
        {
            public string cmpcode { get; set; }
            public string divname { get; set; }
            public int matreqmstoid { get; set; }
            public string matreqno { get; set; }
            public DateTime matreqdate { get; set; }
            public string deptname { get; set; }
            public string matreqwh { get; set; }
            public string matreqmststatus { get; set; }
            public string matreqmstnote { get; set; }
            public string createuser { get; set; }
        }

        public class matreqdtl
        {
            public int matreqdtlseq { get; set; }
            public string matreqreftype { get; set; }
            public int matreqrefoid { get; set; }
            public string matreqrefcode { get; set; }
            public string matreqreflongdesc { get; set; }
            public decimal matreqqty { get; set; }
            public int matrequnitoid { get; set; }
            public string matrequnit { get; set; }
            public string matreqdtlnote { get; set; }        
        }

        public class mstperson
        {
            public string cmpcode { get; set; }
            public string personoid { get; set; }
            public string nip { get; set; }
            public string personname { get; set; }
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        private string generateNo(string cmp, DateTime tgl)
        {
            string sCode = "REQ-" + tgl.ToString("yyyy") + "." + tgl.ToString("MM") + "-";
            int formatCounter = 6;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matreqno, "+ formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmatreqmst WHERE cmpcode='" + cmp + "' AND matreqno LIKE '" + sCode + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        private void InitDDL(QL_trnmatreqmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_mstgen WHERE activeflag='ACTIVE' AND gengroup='MATERIAL LOCATION' ORDER BY gendesc";
            var matreqwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.matreqwhoid);
            ViewBag.matreqwhoid = matreqwhoid;

            sSql = "SELECT * FROM QL_mstprof WHERE activeflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND profoid = '" + Session["UserID"].ToString() + "' ORDER BY profname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstprof>(sSql).ToList(), "profoid", "profname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT p.* FROM QL_approvalperson ap INNER JOIN QL_mstprof p ON p.profoid = ap.approvaluser where tablename = 'QL_trnmatreqmst' ORDER BY p.profoid";
            var matreqmstres3 = new SelectList(db.Database.SqlQuery<QL_mstprof>(sSql).ToList(), "profoid", "profname", tbl.personoid);
            ViewBag.matreqmstres3 = matreqmstres3;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode, string mattype)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();

            var sSqlPlus = " AND genoid IN (select distinct matrawlocoid from QL_mstmatraw) ";
                        
            if (mattype.ToUpper() == "GENERAL")
                sSqlPlus = sSqlPlus.Replace("matrawlocoid", "matgenlocoid").Replace("QL_mstmatraw", "QL_mstmatgen");
            if (mattype.ToUpper() == "SPARE PART")
                sSqlPlus = sSqlPlus.Replace("matrawlocoid", "sparepartlocoid").Replace("QL_mstmatraw", "QL_mstsparepart");

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' AND gengroup='MATERIAL LOCATION' "+ sSqlPlus +" ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmpcode, string mattype)
        {
            List<matreqdtl> tbl = new List<matreqdtl>();
            string sType = ""; string sSql = "";
            var sSqlPlus = "";
            JsonResult js = null;
            if (mattype == "Raw")
            {
                sType = "matraw";
            }
            else if (mattype == "General")
            {
                sType = "matgen";
                //sSqlPlus += " AND m.cat3oid IN (select cat3oid from QL_mstcat3 where cat3longdesc IN ('Alat Tulis Kantor', 'Kebersihan', 'Bahan Bangunan')) "; //ATK & Alat Kebersihan & Bahan Bangunan
            }
            else if (mattype == "Spare Part")
            {
                sType = "sparepart";
            }           
            
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY " + sType + "code) AS INT) matreqdtlseq, m." + sType + "oid AS matreqrefoid, '' AS matreqrefno, m." + sType + "code AS matreqrefcode, m." + sType + "longdesc AS matreqreflongdesc, 0.0 AS matreqqty , m." + sType + "unitoid AS matrequnitoid, g.gendesc AS matrequnit, '' AS matreqdtlnote, m.createuser FROM QL_mst" + sType + " m INNER JOIN QL_mstgen g ON m." + sType + "unitoid=g.genoid WHERE m.activeflag='ACTIVE' "+ sSqlPlus +" /*AND ISNULL(m." + sType + "res2, 'Non Assets')='Non Assets'*/ ORDER BY m." + sType + "code";

            tbl = db.Database.SqlQuery<matreqdtl>(sSql).ToList();
            
            js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<matreqdtl> dtDtl)
        {
            Session["QL_trnmatreqdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmatreqdtl"] == null)
            {
                Session["QL_trnmatreqdtl"] = new List<matreqdtl>();
            }

            List<matreqdtl> dataDtl = (List<matreqdtl>)Session["QL_trnmatreqdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmatreqmst tbl)
        {
            //try
            //{
            //    ViewBag.personname = db.Database.SqlQuery<string>("SELECT personname FROM " + HRISServer + "tv_allperson WHERE cmpcode='" + tbl.personcmpcode + "' AND personoid='" + tbl.personoid + "'").FirstOrDefault();
            //}
            //catch
            //{
            //    ViewBag.personname = db.Database.SqlQuery<string>("SELECT personname FROM QL_mstperson WHERE cmpcode='" + tbl.cmpcode + "' AND personoid='" + tbl.personoid + "'").FirstOrDefault();
            //}
            ViewBag.personname = db.Database.SqlQuery<string>("SELECT profname [personname] FROM QL_mstprof WHERE cmpcode='" + tbl.cmpcode + "' AND profoid='" + tbl.personoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetRequestByData(string cmp)
        {
            List<mstperson> tbl = new List<mstperson>();

            try
            {
                //sSql = "SELECT p.cmpcode, p.personoid, p.nip, p.personname, (p.deptname + ' - ' + p.subdeptname + ' - ' + p.sectionname) deptname FROM " + HRISServer + "tv_allperson p INNER JOIN QL_mstdivision d ON p.cmpcode=d.divhris WHERE p.activeflag='ACTIVE' AND d.cmpcode='" + cmp + "' ORDER BY personname";
                sSql = "SELECT cmpcode, '' [nip], profoid [personoid], profname [personname], '' [deptname]  FROM QL_mstprof WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY profname";
                tbl = db.Database.SqlQuery<mstperson>(sSql).ToList();
            }
            catch
            {
                //sSql = "SELECT div.divhris cmpcode, p.personoid, p.nip, p.personname, deptname FROM QL_mstperson p INNER JOIN QL_mstdept d ON d.cmpcode=p.cmpcode AND d.deptoid=p.deptoid INNER JOIN QL_mstdivision div ON div.divoid=p.divisioid AND div.divcode=p.cmpcode WHERE p.cmpcode='" + cmp + "' AND p.activeflag='ACTIVE' ORDER BY personname";
                sSql = "SELECT cmpcode, '' [nip], profoid [personoid], profname [personname], '' [deptname]  FROM QL_mstprof WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY profname";
                tbl = db.Database.SqlQuery<mstperson>(sSql).ToList();
            }

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT reqm.cmpcode,div.divname, reqm.matreqmstoid, reqm.matreqno, reqm.matreqdate, de.deptname, g1.gendesc AS matreqwh, reqm.matreqmststatus, reqm.matreqmstnote, reqm.createuser FROM QL_trnmatreqmst reqm INNER JOIN QL_mstdept de ON de.cmpcode=reqm.cmpcode AND de.deptoid=reqm.deptoid INNER JOIN QL_mstgen g1 ON g1.genoid=reqm.matreqwhoid INNER JOIN QL_mstdivision div ON div.cmpcode=reqm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "reqm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "reqm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND matreqdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND matreqdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) /*AND matreqmststatus IN ('In Process', 'Revised')*/";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND matreqdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND matreqdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) /*AND matreqmststatus IN ('In Process', 'Revised')*/";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND matreqdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND matreqdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND matreqmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                //sSql += " AND matreqmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND reqm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND reqm.createuser='" + Session["UserID"].ToString() + "'";

            List<matreqmst> dt = db.Database.SqlQuery<matreqmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: PRRawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatreqmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnmatreqmst();
                tbl.cmpcode = CompnyCode;
                tbl.matreqmstoid = ClassFunction.GenerateID("QL_trnmatreqmst");
                tbl.matreqdate = ClassFunction.GetServerTime();
                tbl.matreqexpdate = ClassFunction.GetServerTime().AddMonths(3);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.matreqmststatus = "In Process";
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
                Session["QL_trnmatreqdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmatreqmst.Find(cmp, id);

                sSql = "SELECT reqd.matreqdtlseq, reqd.matreqreftype, reqd.matreqrefoid, (CASE reqd.matreqreftype WHEN 'Raw' THEN(SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid = reqd.matreqrefoid) WHEN 'General' THEN(SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid = reqd.matreqrefoid) WHEN 'Spare Part' THEN(SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid = reqd.matreqrefoid) WHEN 'Log' THEN(SELECT(cat1code + '.' + cat2code + '.' + cat3code) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode = c3.cmpcode AND c2.cat2oid = c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode = c3.cmpcode AND c1.cat1oid = c3.cat1oid WHERE cat3oid = reqd.matreqrefoid) WHEN 'Sawn Timber' THEN(SELECT(cat1code + '.' + cat2code) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode = c2.cmpcode AND c1.cat1oid = c2.cat1oid WHERE cat2oid = reqd.matreqrefoid) ELSE '' END) AS matreqrefcode, (CASE reqd.matreqreftype WHEN 'Raw' THEN(SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid = reqd.matreqrefoid) WHEN 'General' THEN(SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid = reqd.matreqrefoid) WHEN 'Spare Part' THEN(SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid = reqd.matreqrefoid) WHEN 'Log' THEN(SELECT RTRIM((CASE WHEN(LTRIM(cat1shortdesc) = 'None' OR LTRIM(cat1shortdesc) = '') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN(LTRIM(cat2shortdesc) = 'None' OR LTRIM(cat2shortdesc) = '') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN(LTRIM(cat3shortdesc) = 'None' OR LTRIM(cat3shortdesc) = '') THEN '' ELSE cat3shortdesc + ' ' END)) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode = c3.cmpcode AND c2.cat2oid = c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode = c3.cmpcode AND c1.cat1oid = c3.cat1oid WHERE cat3oid = reqd.matreqrefoid) WHEN 'Sawn Timber' THEN(SELECT RTRIM((CASE WHEN(LTRIM(cat1shortdesc) = 'None' OR LTRIM(cat1shortdesc) = '') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN(LTRIM(cat2shortdesc) = 'None' OR LTRIM(cat2shortdesc) = '') THEN '' ELSE cat2shortdesc + ' ' END)) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode = c2.cmpcode AND c1.cat1oid = c2.cat1oid WHERE cat2oid = reqd.matreqrefoid) ELSE '' END) AS matreqreflongdesc, reqd.matreqqty, reqd.matrequnitoid, g.gendesc AS matrequnit, reqd.matreqdtlnote FROM QL_trnmatreqdtl reqd INNER JOIN QL_mstgen g ON g.genoid = reqd.matrequnitoid WHERE reqd.cmpcode = '" + cmp + "' AND reqd.matreqmstoid = " + id +"";
                Session["QL_trnmatreqdtl"] = db.Database.SqlQuery<matreqdtl>(sSql).ToList();

                ViewBag.mattype = db.Database.SqlQuery<string>("SELECT distinct matreqreftype FROM QL_trnmatreqdtl where matreqmstoid = "+ id).FirstOrDefault();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRRawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmatreqmst tbl, string action, string closing, string matreqmstres3)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            else
            {
                if (tbl.matreqno == null)
                    tbl.matreqno = "";

                tbl.matreqmstres3 = matreqmstres3;
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            List<approvalperson> AppUser = null;
            if (ModelState.IsValid && tbl.matreqmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnmatreqmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form!");
                }
                else
                {
                    sSql = "SELECT '"+ matreqmstres3 + "' matreqmstres3 ";
                    if (matreqmstres3 != "agnesr")
                        sSql += " UNION ALL SELECT 'agnesr' matreqmstres3 ";
                    AppUser = db.Database.SqlQuery<approvalperson>(sSql).ToList();
                }               
            }
            // END VAR UTK APPROVAL

            List<matreqdtl> dtDtl = (List<matreqdtl>)Session["QL_trnmatreqdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmatreqmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmatreqdtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmatreqmst.Find(tbl.cmpcode, tbl.matreqmstoid) != null)
                                tbl.matreqmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.matreqdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnmatreqmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.matreqmstoid + " WHERE tablename='QL_trnmatreqmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnmatreqdtl.Where(a => a.matreqmstoid == tbl.matreqmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmatreqdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmatreqdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnmatreqdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.matreqdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.matreqmstoid = tbl.matreqmstoid;
                            tbldtl.matreqdtlseq = i + 1;
                            tbldtl.matreqreftype = dtDtl[i].matreqreftype;
                            tbldtl.matreqrefoid = dtDtl[i].matreqrefoid;
                            tbldtl.matreqqty = dtDtl[i].matreqqty;
                            tbldtl.matrequnitoid = dtDtl[i].matrequnitoid;
                            tbldtl.matreqdtlstatus = "";
                            tbldtl.matreqdtlnote = dtDtl[i].matreqdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnmatreqdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmatreqdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.matreqmststatus == "In Approval")
                        {                            
                            for (int i=0; i < AppUser.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "REQ" + tbl.matreqmstoid + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnmatreqmst";
                                tblApp.oid = tbl.matreqmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppUser[i].matreqmstres3;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();                               
                            }
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.matreqmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        tbl.matreqmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.matreqmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRRawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatreqmst tbl = db.QL_trnmatreqmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnmatreqdtl.Where(a => a.matreqmstoid == id && a.cmpcode == cmp);
                        db.QL_trnmatreqdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmatreqmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmatreqmst.Find(cmp, id);
            if (tbl == null)
                return null;

            var rptname = "rptMatRequest";
            if (printtype == "A5")
            {
                rptname += "A5";
            }
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            //sSql = "SELECT reqm.matreqmstoid, CONVERT(VARCHAR(20), reqm.matreqmstoid) AS [Draft No.], reqm.matreqno AS [Request No.], reqm.matreqdate AS [Request Date], de.deptname AS [Department], g1.gendesc AS [Warehouse], reqm.matreqmststatus AS [Status], reqm.matreqmstnote AS [Header Note], reqd.matreqdtloid, reqd.matreqdtlseq AS [No.], reqd.matreqreftype AS [Type], reqd.matreqrefoid, (CASE reqd.matreqreftype WHEN 'Raw' THEN (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=reqd.matreqrefoid) WHEN 'General' THEN (SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid=reqd.matreqrefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid=reqd.matreqrefoid) WHEN 'Log' THEN (SELECT (cat1code + '.' + cat2code + '.' + cat3code) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE cat3oid=reqd.matreqrefoid) WHEN 'Sawn Timber' THEN (SELECT (cat1code + '.' + cat2code) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=reqd.matreqrefoid) ELSE '' END) AS [Code], (CASE reqd.matreqreftype WHEN 'Raw' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=reqd.matreqrefoid) WHEN 'General' THEN (SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=reqd.matreqrefoid) WHEN 'Log' THEN (SELECT RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE cat3oid=reqd.matreqrefoid) WHEN 'Sawn Timber' THEN (SELECT RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=reqd.matreqrefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid=reqd.matreqrefoid) ELSE '' END) AS [Description], reqd.matreqqty AS [Qty], g2.gendesc AS [Unit], reqd.matreqdtlnote AS [Detail Note], di.divname AS [Business Unit], reqm.createuser [crtuser], reqm.createtime [crttime], CASE reqm.matreqmststatus WHEN 'In Process' THEN  '' ELSE reqm.upduser END [upduser], CASE reqm.matreqmststatus WHEN 'In Process' THEN  NULL ELSE reqm.updtime END [updtime], reqm.matreqexpdate [expdate], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) AS [BU Name] , (SELECT groupcode+' - '+groupdesc FROM QL_mstdeptgroupdtl deptd INNER JOIN QL_mstdeptgroup deptm ON deptm.cmpcode=deptd.cmpcode AND deptd.groupoid=deptm.groupoid WHERE deptd.cmpcode=reqm.cmpcode AND deptd.deptoid=reqm.deptoid) AS [Division], ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=reqm.cmpcode AND loc.mtrwhoid=reqm.matreqwhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)=reqd.matreqreftype AND loc.matoid=reqd.matreqrefoid),'') AS [Detail Location], emp.nip, emp.personname FROM QL_trnmatreqmst reqm INNER JOIN QL_mstdept de ON de.cmpcode=reqm.cmpcode AND de.deptoid=reqm.deptoid INNER JOIN QL_mstgen g1 ON g1.genoid=reqm.matreqwhoid INNER JOIN QL_trnmatreqdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.matreqmstoid=reqm.matreqmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=reqd.matrequnitoid INNER JOIN QL_mstdivision di ON di.cmpcode=reqm.cmpcode LEFT JOIN " + HRISServer +"QL_mstperson emp ON /*emp.cmpcode=reqm.personcmpcode AND*/ emp.personoid=reqm.personoid WHERE reqm.matreqmstoid="+ id +" ORDER BY reqm.matreqmstoid, reqd.matreqdtlseq ";
            sSql = "SELECT reqm.matreqmstoid, CONVERT(VARCHAR(20), reqm.matreqmstoid) AS [Draft No.], reqm.matreqno AS [Request No.], reqm.matreqdate AS [Request Date], de.deptname AS [Department], g1.gendesc AS [Warehouse], reqm.matreqmststatus AS [Status], reqm.matreqmstnote AS [Header Note], reqd.matreqdtloid, reqd.matreqdtlseq AS [No.], reqd.matreqreftype AS [Type], reqd.matreqrefoid, (CASE reqd.matreqreftype WHEN 'Raw' THEN (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=reqd.matreqrefoid) WHEN 'General' THEN (SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid=reqd.matreqrefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid=reqd.matreqrefoid) WHEN 'Log' THEN (SELECT (cat1code + '.' + cat2code + '.' + cat3code) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE cat3oid=reqd.matreqrefoid) WHEN 'Sawn Timber' THEN (SELECT (cat1code + '.' + cat2code) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=reqd.matreqrefoid) ELSE '' END) AS [Code], (CASE reqd.matreqreftype WHEN 'Raw' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=reqd.matreqrefoid) WHEN 'General' THEN (SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=reqd.matreqrefoid) WHEN 'Log' THEN (SELECT RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE cat3oid=reqd.matreqrefoid) WHEN 'Sawn Timber' THEN (SELECT RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=reqd.matreqrefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid=reqd.matreqrefoid) ELSE '' END) AS [Description], reqd.matreqqty AS [Qty], g2.gendesc AS [Unit], reqd.matreqdtlnote AS [Detail Note], di.divname AS [Business Unit], reqm.createuser [crtuser], reqm.createtime [crttime], CASE reqm.matreqmststatus WHEN 'In Process' THEN  '' ELSE reqm.upduser END [upduser], CASE reqm.matreqmststatus WHEN 'In Process' THEN  NULL ELSE reqm.updtime END [updtime], reqm.matreqexpdate [expdate], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) AS [BU Name] , (SELECT groupcode+' - '+groupdesc FROM QL_mstdeptgroupdtl deptd INNER JOIN QL_mstdeptgroup deptm ON deptm.cmpcode=deptd.cmpcode AND deptd.groupoid=deptm.groupoid WHERE deptd.cmpcode=reqm.cmpcode AND deptd.deptoid=reqm.deptoid) AS [Division], '' AS [Detail Location], '' [nip], profname [personname], ISNULL((SELECT profname FROM QL_mstprof where profoid = reqm.createuser),'') [CreteUser], ISNULL((SELECT profname FROM QL_mstprof where profoid = reqm.approvaluser),'') [ApprovalUser] " +
                "FROM QL_trnmatreqmst reqm  " +
                "INNER JOIN QL_mstdept de ON de.cmpcode=reqm.cmpcode AND de.deptoid=reqm.deptoid " +
                "INNER JOIN QL_mstgen g1 ON g1.genoid=reqm.matreqwhoid " +
                "INNER JOIN QL_trnmatreqdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.matreqmstoid=reqm.matreqmstoid " +
                "INNER JOIN QL_mstgen g2 ON g2.genoid=reqd.matrequnitoid " +
                "INNER JOIN QL_mstdivision di ON di.cmpcode=reqm.cmpcode " +
                "LEFT JOIN QL_mstprof emp ON emp.profoid=reqm.personoid " + 
                "WHERE reqm.matreqmstoid=" + id + " ORDER BY reqm.matreqmstoid, reqd.matreqdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            sSql = "SELECT " + id + " AS matreqmstoid, '' AS code, '' AS description";
            ClassConnection cConn2 = new ClassConnection();
            DataTable subdt1 = cConn2.GetDataTable(sSql, "QL_subdt1");
            report.Subreports[0].SetDataSource(subdt1);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("diPrint", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "MatRequestPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}