﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class TransformSheetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();

        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string HRISServer = System.Configuration.ConfigurationManager.AppSettings["HRISServer"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public TransformSheetController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class usagemst
        {
            public string cmpcode { get; set; }
            public int transformmstoid { get; set; }
            public string divgroup { get; set; }
            public string transformno { get; set; }
            public DateTime transformdate { get; set; }
            public string transformmststatus { get; set; }
            public string transformmstnote { get; set; }
         
        }

     

     
        public static string GetQueryInitDDLWH(string cmpcode, string action)
        {
            var cmp = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            var res = "SELECT * FROM QL_mstgen WHERE cmpcode='" + cmp + "' AND gengroup='ITEM LOCATION' AND genoid>0";
            if (action == "New Data")
                res += " AND activeflag='ACTIVE'";
            res += " ORDER BY gendesc";
            return res;
        }

        public class COA
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }
        private void InitDDL(QL_trntransformdivmst tbl, string action)
        {

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            var cmp = cmpcode.First().Value;
            if (!string.IsNullOrEmpty(tbl.cmpcode) && cmpcode.Count() > 0)
                cmp = tbl.cmpcode;

            var transformwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(GetQueryInitDDLWH(cmp, action)).ToList(), "genoid", "gendesc", tbl.transformwhoid);
            ViewBag.transformwhoid = transformwhoid;

        }

        private string GenerateNo(string cmp)
        {
            var usageno = "";
            if (!string.IsNullOrEmpty(cmp))
            {
                string sNo = "TFMST-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransformdivmst WHERE cmpcode='" + cmp + "' AND transformno LIKE '%" + sNo + "%'";
                usageno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return usageno;
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode, string action)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = GetQueryInitDDLWH(cmpcode, action);
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public class cust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
        }
        [HttpPost]
        public ActionResult GetCustData(string cmp, int divgroupoid)
        {
            List<cust> tbl = new List<cust>();

            sSql = "SELECT DISTINCT w.custoid, c.custcode, c.custname from QL_conmat con inner join QL_trnsorawdtl i on i.sorawdtloid = con.refoid and con.refname = 'SHEET' inner join ql_trnwomst w on w.itemoid=i.sorawdtloid and w.sotype='QL_trnsorawmst' Inner Join ql_mstcust c on c.custoid=w.custoid WHERE i.divgroupoid=" + divgroupoid + " AND con.cmpcode='" + cmp + "' group by  con.refoid, w.custoid,c.custoid, custcode, custname, refname, con.mtrwhoid having  sum(qtyin-qtyout)>0  ORDER BY custname";
            tbl = db.Database.SqlQuery<cust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public class sheet
        {
            public int sheetoid { get; set; }
            public string sheetdesc { get; set; }
        }
        [HttpPost]
        public ActionResult GetSheetData(string cmp, int divgroupoid,int custoid)
        {
            List<sheet> tbl = new List<sheet>();

            sSql = "SELECT DISTINCT con.refoid sheetoid, i.sodtldesc sheetdesc from QL_conmat con inner join QL_trnsorawdtl i on i.sorawdtloid = con.refoid and con.refname = 'SHEET' inner join ql_trnwomst w on w.itemoid=i.sorawdtloid and w.sotype='QL_trnsorawmst' WHERE i.divgroupoid=" + divgroupoid + " AND con.cmpcode='" + cmp + "' and w.custoid="+custoid+ " group by  con.refoid, sodtldesc, refname, con.mtrwhoid having  sum(qtyin-qtyout)>0  ORDER BY sodtldesc";
            tbl = db.Database.SqlQuery<sheet>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public class transformdtl1
        {
            public int transformdtl1seq { get; set; }
            public int sheetoid { get; set; }
            public string sheetdesc { get; set; }
            public string refno { get; set; }
            public string refno2 { get; set; }
            public int transformunitoid { get; set; }
            public string transformunit { get; set; }
            public string transformdtlnote { get; set; }
            public decimal transformdtlvalue { get; set; }
            public decimal transformqty { get; set; }
            public decimal stock { get; set; }
            public decimal berat { get; set; }
            public decimal totalberat { get; set; }
        }

        [HttpPost]
        public ActionResult GetMatData(string cmp, int divgroupoid, int whoid,int refoid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl1> tbl = new List<transformdtl1>();
            try
            {
                sSql = "SELECT DISTINCT 0 transformdtl1seq, sum(qtyin - qtyout) transformqty,con.refname mattype ,con.refoid sheetoid,i.sodtldesc sheetdesc, con.refno, isnull((select conx.refno2 from QL_conmat conx where conx.conmtroid = (select max(x.conmtroid) from ql_conmat x where x.refoid = con.refoid and x.refname = con.refname)),'')  refno2, sum(qtyin - qtyout) stock, i.sorawunitoid transformunitoid, (select x.gendesc from ql_mstgen x where genoid=i.sorawunitoid) transformunit,'' transformdtlnote,w.wsheet berat from QL_conmat con inner join QL_trnsorawdtl i on i.sorawdtloid = con.refoid and con.refname = 'SHEET' inner join ql_trnwomst w on w.itemoid=i.sorawdtloid and w.sotype='QL_trnsorawmst' WHERE i.divgroupoid=" + divgroupoid+ " AND con.cmpcode='" + cmp + "' and con.refoid="+refoid+" and con.mtrwhoid=" + whoid+ " group by  con.refname,con.refoid, sodtldesc, sorawunitoid, wsheet, con.mtrwhoid, con.refno having  sum(qtyin-qtyout)>0  ORDER BY sodtldesc";
                tbl = db.Database.SqlQuery<transformdtl1>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].transformdtl1seq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch(Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl1> dtl1 = new List<transformdtl1>();

            try
            {
                sSql = "SELECT 0 transformdtl1seq,* from QL_trntransformdivdtl u WHERE u.cmpcode='" + cmp + "' and u.transformmstoid=" + oid;
                dtl1 = db.Database.SqlQuery<transformdtl1>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].transformdtl1seq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class transformdtl2
        {
            public int transformdtl2seq { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public string refno { get; set; }
            public string transformdtl2note { get; set; }
            public int transform2unitoid { get; set; }
            public string transform2unit { get; set; }
            public decimal transformdtl2value { get; set; }
            public decimal transform2qty { get; set; }
            public decimal berat { get; set; }
            public decimal transformvalueperkg { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int divgroupoid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl2> tbl = new List<transformdtl2>();

            try
            {
                sSql = "SELECT 0 transformdtl2seq, matrawoid, matrawcode, matrawlongdesc, '' refno, 0.0 transform2qty, matrawunitoid transform2unitoid, (select x.gendesc from ql_mstgen x where x.genoid=i.matrawunitoid) transform2unit, '' transformdtl2note, isnull(beratpcs,0.0) berat, 0.0 transformvalueperkg FROM QL_mstmatraw i WHERE i.cmpcode='" + cmp + "' AND i.divgroupoid=" + divgroupoid + " and matrawres3='Sheet'";
                tbl = db.Database.SqlQuery<transformdtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].transformdtl2seq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        [HttpPost]
        public ActionResult FillDetailData2(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl2> dtl1 = new List<transformdtl2>();

            try
            {
                sSql = "SELECT 0 transformdtl2seq, * FROM QL_trntransformdivdtl2 u WHERE u.cmpcode='" + cmp + "' and transformmstoid='" + oid+"'";
                dtl1 = db.Database.SqlQuery<transformdtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].transformdtl2seq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string generateBatchNo()
        {
            var tgl = ClassFunction.GetServerTime();
            var sNo = tgl.ToString("yyyy.MM.dd") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            sNo = sNo + sCounter;

            return sNo;
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: RawMaterialUsage
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "usagem", divgroupoid, "divgroupoid");
            sSql = "SELECT usagem.*,(select gendesc from ql_mstgen where genoid=usagem.divgroupfromoid) divgroup FROM QL_trntransformdivmst usagem INNER JOIN QL_mstgen g1 ON g1.genoid=usagem.transformwhoid INNER JOIN QL_mstdivision div ON div.cmpcode=usagem.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "usagem.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "usagem.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND transformmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND transformmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND transformmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND transformdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND transformdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND transformmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND usagem.createuser='" + Session["UserID"].ToString() + "'";

            List<usagemst> dt = db.Database.SqlQuery<usagemst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trntransformdivmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }


        // GET: RawMaterialUsage/PrintReport/id/cmp?type1=&type2=
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            ids = ClassFunction.Left(ids, ids.Length - 1);
            ids = ids.Replace("x", ",");

            var rptname = "rptUsageByWO";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT mum.usagemstoid AS [Oid], CONVERT(VARCHAR(10), mum.usagemstoid) AS [Draft No.], usageno AS [Usage No.], usagedate AS [Usage Date], d.routename AS [Department], '' AS [Request No.], gwh.gendesc AS [Warehouse], usagemstnote AS [Header Note], usagemststatus AS [Status], usagereftype AS [Type], matrawcode AS [Code], matrawlongdesc AS [Description], usageqty AS [Qty], gunit.gendesc AS [Unit], usagedtlnote AS [Detail Note], divname AS [BU Name], divaddress AS [BU Address], gcity.gendesc AS [BU City], gprov.gendesc AS [BU Province], gcont.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Telp. 1], ISNULL(divphone2, '') AS [BU Telp. 2], ISNULL(divfax1, '') AS [BU Fax 1], ISNULL(divfax2, '') AS [BU Fax 2], ISNULL(divemail, '') AS [BU Email], ISNULL(divpostcode, '') AS [BU Post Code], '' AS [Division], som.wono [SO No.], ''  [Person Name], ''  [NIP] FROM QL_trnusagemst mum INNER JOIN QL_trnusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagemstoid=mum.usagemstoid INNER JOIN QL_trnwomst som ON som.cmpcode=mum.cmpcode AND mum.womstoid=som.womstoid INNER JOIN QL_mstroute d ON d.cmpcode=mum.cmpcode AND d.routeoid=mum.routeoid INNER JOIN QL_mstmatraw m ON m.matrawoid=mud.usagerefoid INNER JOIN QL_mstgen gwh ON gwh.genoid=usagewhoid INNER JOIN QL_mstgen gunit ON gunit.genoid=usageunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode INNER JOIN QL_mstgen gcity ON gcity.genoid=divcityoid INNER JOIN QL_mstgen gprov ON gprov.genoid=CAST(gcity.genother1 AS INT) INNER JOIN QL_mstgen gcont ON gcont.genoid=CAST(gcity.genother2 AS INT) WHERE mum.usagemstoid IN (" + ids + ") ORDER BY mum.usagemstoid, usagedtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", Session["UserName"].ToString());
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "MaterialUsagePrintOut.pdf");
        }
        private void FillAdditionalField(QL_trntransformdivmst tbl)
        {
            ViewBag.divgroupfrom = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE genoid=" + tbl.divgroupfromoid + "").FirstOrDefault();
            ViewBag.divgroupto = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE genoid=" + tbl.divgrouptooid + "").FirstOrDefault();
        }
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trntransformdivmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trntransformdivmst();
                tbl.transformdate = ClassFunction.GetServerTime();
                tbl.transformmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.divgroupfromoid = 1781;
                tbl.divgrouptooid = 1782;
                tbl.custoid = 0;
                tbl.sheetoid = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trntransformdivmst.Find(cmp, id);
              
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }
    
      
        // POST: POService/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trntransformdivmst tbl, List<transformdtl1> dtl1, List<transformdtl2> dtl2, string action, string closing, string batch)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var iStockValOid = ClassFunction.GenerateID("QL_stockvalue");
            var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
            var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
            var msg = ""; var result = "failed"; var hdrid = "0";decimal stock = 0;  
            int iConOid = ClassFunction.GenerateID("QL_CONMAT");
            decimal totalvaluemat1 = 0;decimal qty1 = 0;
            decimal qtyfg = 0; decimal beratdtl2 = 0;
            if (string.IsNullOrEmpty(tbl.cmpcode))
                msg += "Please select BUSINESS UNIT!<br />";

            if (string.IsNullOrEmpty(tbl.transformmstnote))
                tbl.transformmstnote = "";
            
       
            if (tbl.transformwhoid == 0)
                msg += "Please select WAREHOUSE field!<br />";

            if (dtl1 == null)
                msg += "Please fill detail Sheet!<br />";
            else if (dtl1.Count <= 0)
                msg += "Please fill detail Sheet!<br />";
            else
            {
                if (dtl1.Count > 0)
                {
                    for (int i = 0; i < dtl1.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl1[i].transformdtlnote))
                        {
                            dtl1[i].transformdtlnote = "";
                        }
                        if (dtl1[i].transformqty <= 0)
                        {
                            msg += "Transform Material Must be more than 0 !<BR>";
                        }
                        else
                        {
                            sSql = "select sum(qtyin-qtyout) from ql_conmat where refoid=" + dtl1[i].sheetoid + " and refname='SHEET' and refno='"+dtl1[i].refno+"'";
                            stock = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dtl1[i].stock != stock)
                            {
                                dtl1[i].stock = stock;
                            }
                            if (dtl1[i].stock < dtl1[i].transformqty)
                            {
                                msg += "Usage Transform Must be equal or less than Stock QTY !<BR>";
                            }
                        }
                        dtl1[i].transformdtlvalue = ClassFunction.GetStockValueIDR(db, CompnyCode, "", "SHEET", dtl1[i].sheetoid);
                        dtl1[i].totalberat = dtl1[i].berat * dtl1[i].transformqty;
                        totalvaluemat1 += dtl1[i].transformdtlvalue * dtl1[i].totalberat;
                        qty1 += dtl1[i].transformqty;
                    }
                }
            }

            if (dtl2 == null)
                msg += "Please fill detail Material data!<br />";
            else if (dtl2.Count <= 0)
                msg += "Please fill detail Material data!<br />";
            else
            {
                if (dtl2.Count > 0)
                {
                    for (int i = 0; i < dtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl2[i].transformdtl2note))
                        {
                            dtl2[i].transformdtl2note = "";
                        }
                        if(dtl2[i].transform2qty <= 0)
                        {
                            msg += "QTY Raw Seet Must be more than 0 !<BR>";
                        }
                        else
                        {
                            qtyfg += dtl2[i].transform2qty;
                            beratdtl2 += dtl2[i].transform2qty * dtl2[i].berat;
                        }
                        
                    }
                }
            }
            if (qty1 != qtyfg)
            {
                msg+="Total QTY Sheet Harus sama Dengan Total QTY Raw Sheet !</BR>";
            }
            tbl.sheetvalue = totalvaluemat1 / beratdtl2;
            tbl.transformtotalvalue = totalvaluemat1;
            int iAcctgoidRM = 0;int iAcctgoidST = 0;

            tbl.transformno = "";
            if (tbl.transformmststatus == "Post")
            {
                tbl.transformno = GenerateNo(CompnyCode);


                var divisifrom = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupfromoid).FirstOrDefault();
                var divisito = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgrouptooid).FirstOrDefault();
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgrouptooid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupfromoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }

                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgrouptooid));

                iAcctgoidST = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupfromoid));
              
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.transformdate);
               
    
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                      
                        if (action == "New Data")
                        {
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trntransformdivmst.Add(tbl);
                            db.SaveChanges();

                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            var trndtl = db.QL_trntransformdivdtl.Where(a => a.transformmstoid == tbl.transformmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trntransformdivdtl.RemoveRange(trndtl);
                            var trndtl2 = db.QL_trntransformdivdtl2.Where(a => a.transformmstoid == tbl.transformmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trntransformdivdtl2.RemoveRange(trndtl2);
                        }

                        QL_trntransformdivdtl tbldtl;

                        for (int i = 0; i < dtl1.Count(); i++)
                        {
                            tbldtl = new QL_trntransformdivdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupfromoid;
                            tbldtl.transformmstoid = tbl.transformmstoid;
                            tbldtl.sheetoid = dtl1[i].sheetoid;
                            tbldtl.sheetdesc = dtl1[i].sheetdesc;
                            tbldtl.refno = dtl1[i].refno;
                            tbldtl.refno2 = dtl1[i].refno2;
                            tbldtl.transformqty = dtl1[i].transformqty;
                            tbldtl.berat = dtl1[i].berat;
                            tbldtl.totalberat = dtl1[i].transformqty * dtl1[i].berat;
                            tbldtl.transformunitoid = dtl1[i].transformunitoid;
                            tbldtl.transformunit = dtl1[i].transformunit;
                            tbldtl.transformdtlnote = dtl1[i].transformdtlnote;
                            tbldtl.transformdtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.transformdtlvalue = dtl1[i].transformdtlvalue;
                            tbldtl.transformdtlvalueidr = dtl1[i].transformdtlvalue;
                            tbldtl.transformdtlvalueusd = 0;
                            db.QL_trntransformdivdtl.Add(tbldtl);
                            db.SaveChanges();
                         
                         
                            if (tbl.transformmststatus == "Post")
                            {
                                // STOCK OUT
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "TRFMP", "QL_trntransformdivmst", tbl.transformmstoid, tbldtl.sheetoid, "SHEET", tbl.transformwhoid, tbldtl.transformqty * -1, "Transform SHEET", tbl.transformno, tbl.upduser, tbldtl.refno, tbldtl.refno2, tbldtl.transformqty*tbldtl.transformdtlvalueidr,tbldtl.transformdtlvalueusd* tbldtl.transformqty, 0, null, tbldtl.transformdtloid, tbl.divgroupfromoid, ""));
                                db.SaveChanges();
                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(tbldtl.transformqty, tbldtl.transformdtlvalueidr, tbldtl.transformdtlvalueusd, "QL_trntransformdivmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl1[i].sheetoid, "SHEET");
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(tbldtl.transformqty, tbldtl.transformdtlvalueidr, tbldtl.transformdtlvalueusd, "QL_trntransformdivmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl1[i].sheetoid, "SHEET", iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                         
                        }
                        QL_trntransformdivdtl2 tbldtl2;

                        for (int i = 0; i < dtl2.Count(); i++)
                        {
                            tbldtl2 = new QL_trntransformdivdtl2();
                            tbldtl2.cmpcode = tbl.cmpcode;
                            tbldtl2.divgroupoid = tbl.divgrouptooid;
                            tbldtl2.transformmstoid = tbl.transformmstoid;
                            tbldtl2.matrawoid = dtl2[i].matrawoid;
                            tbldtl2.matrawcode = dtl2[i].matrawcode;
                            tbldtl2.matrawlongdesc = dtl2[i].matrawlongdesc;
                            tbldtl2.transform2qty = dtl2[i].transform2qty;
                            tbldtl2.berat = dtl2[i].berat;
                            tbldtl2.transform2unitoid = dtl2[i].transform2unitoid;
                            tbldtl2.transform2unit = dtl2[i].transform2unit;
                            tbldtl2.transformvalueperkg = tbl.sheetvalue;
                            tbldtl2.transformdtl2value = tbl.sheetvalue*tbldtl2.berat;
                            tbldtl2.transformdtl2valueidr = tbldtl2.transformdtl2value;
                            tbldtl2.transformdtl2valueusd = 0;
                            tbldtl2.transformdtl2note = dtl2[i].transformdtl2note;
                            tbldtl2.transformdtl2status = "";
                            tbldtl2.upduser = tbl.upduser;
                            tbldtl2.updtime = tbl.updtime;
                            if (tbl.transformmststatus == "Post")
                            {
                                tbldtl2.refno = generateBatchNo();
                            }
                            else
                            {
                                tbldtl2.refno = "";
                            }

                            db.QL_trntransformdivdtl2.Add(tbldtl2);
                            db.SaveChanges();

                            if (tbl.transformmststatus == "Post")
                            {
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "TRFMP", "QL_trntransformdivmst", tbl.transformmstoid, dtl2[i].matrawoid, "SHEET", tbl.transformwhoid, tbldtl2.transform2qty, "RECEIVE TRANSFORM SHEET", tbl.transformno, tbl.upduser, tbldtl2.refno, "R-001-"+tbldtl2.refno, tbldtl2.transformdtl2valueidr*tbldtl2.transform2qty, tbldtl2.transform2qty * tbldtl2.transformdtl2valueusd, 0, null, tbldtl2.transformdtl2oid, tbl.divgrouptooid,""));
                                db.SaveChanges();

                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(tbldtl2.transform2qty, tbldtl2.transformdtl2valueidr, tbldtl2.transformdtl2valueusd, "QL_trntransformdivmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].matrawoid, "SHEET");
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(tbldtl2.transform2qty, tbldtl2.transformdtl2valueidr, tbldtl2.transformdtl2valueusd, "QL_trntransformdivmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].matrawoid, "SHEET", iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }

                            }
                        }

                        if (tbl.transformmststatus == "Post")
                        {
                         
               
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, servertime, sPeriod, "TRANSFORM SHEET|No. " + tbl.transformno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupfromoid));
                            db.SaveChanges();

                            int iSeq = 1;

                            // Persediaan Ke;luar
                            
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidST, "C", tbl.transformtotalvalue, tbl.transformno, "TRANSFORM SHEET|No. " + tbl.transformno, "Post", tbl.upduser, servertime, tbl.transformtotalvalue, 0, "QL_trntransformdivmst " + tbl.transformmstoid.ToString(), null, null, null, 0, tbl.divgroupfromoid));
                            db.SaveChanges();
                            
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "D", tbl.transformtotalvalue, tbl.transformno, "TRANSFORM SHEET|No. " + tbl.transformno, "Post", tbl.upduser, servertime, tbl.transformtotalvalue, 0, "QL_trntransformdivmst " + tbl.transformmstoid.ToString(), null, null, null, 0, tbl.divgrouptooid));
                            db.SaveChanges();
                            
                          
                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + iConOid + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_stockvalue'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        hdrid = tbl.transformmstoid.ToString();
                        msg = "Data saved using Draft No. " + hdrid + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: POService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransformdivmst tbl = db.QL_trntransformdivmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        var trndtl = db.QL_trntransformdivdtl.Where(a => a.transformmstoid == id && a.cmpcode == cmp);
                        db.QL_trntransformdivdtl.RemoveRange(trndtl);

                        var trndtl2 = db.QL_trntransformdivdtl2.Where(a => a.transformmstoid == id && a.cmpcode == cmp);
                        db.QL_trntransformdivdtl2.RemoveRange(trndtl2);

                        db.QL_trntransformdivmst.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}