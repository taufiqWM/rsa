﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ProdResConfirmSheetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        //private string DefaultFormatCounter = "6";
        private string sSql = "";

        public ProdResConfirmSheetController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class confirmmst
        {
            public int confirmmstoid { get; set; }
            public string confirmno { get; set; }
            public string divgroup { get; set; }
            public DateTime confirmdate { get; set; }
            public string confirmmststatus { get; set; }
            public string confirmmstnote { get; set; }
            public string confirmwh { get; set; }
            public string cmpcode { get; set; }
        }

        public class confirmdtl
        {
            public int confirmdtlseq { get; set; }
            public int prodresmstoid { get; set; }
            public string prodresno { get; set; }
            public int womstoid { get; set; }
            public string wono { get; set; }
            public string deptname { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal prodresqty { get; set; }
            public decimal confirmqty { get; set; }
            public decimal rejectqty { get; set; }
            public int confirmunitoid { get; set; }
            public string confirmunit { get; set; }
            public string confirmdtlnote { get; set; }
            public decimal confirmdm { get; set; }
            public decimal confirmdl { get; set; }
            public decimal confirmfoh { get; set; }
            public decimal confirmvalue { get; set; }
        }

        public class prodresmst
        {
            public int prodresmstoid { get; set; }
            public string prodresno { get; set; }
            public string prodresmstnote { get; set; }
            public int itemoid { get; set; }
            public int womstoid { get; set; }
            public string itemlongdesc { get; set; }
            public decimal prodresqty { get; set; }
        }

        [HttpPost]
        public ActionResult GetProdresData(string cmp, int divgroupoid)
        {
            List<prodresmst> tbl = new List<prodresmst>();

            sSql = "SELECT pm.prodresmstoid, prodresno, pm.womstoid,ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=pm.cmpcode AND wom.womstoid=pm.womstoid), '') wono, pm.itemoid, 'Sheet' itemcode, sodtldesc itemlongdesc, (resultqty - ISNULL((SELECT SUM(confirmqty) FROM QL_trnconfirmdtl cd WHERE cd.cmpcode=pm.cmpcode AND cd.prodresmstoid=pm.prodresmstoid), 0)) prodresqty FROM QL_trnprodresmst pm INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=pm.itemoid inner join ql_mstgen ep on ep.genoid=pm.processoid WHERE pm.cmpcode='" + CompnyCode + "' AND ISNULL(pm.prodresmstres1,'')='Sheet' AND prodresmststatus='Post' AND UPPER(ep.gendesc) IN('CORRUGATOR', 'FINISHING') and pm.divgroupoid=" + divgroupoid + " ORDER BY prodresno";
            tbl = db.Database.SqlQuery<prodresmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cm", divgroupoid, "divgroupoid");
            sSql = "SELECT confirmmstoid, (select gendesc from ql_mstgen where genoid=cm.divgroupoid) divgroup, confirmno, confirmdate, confirmmststatus, confirmmstnote, gendesc confirmwh, cm.cmpcode FROM QL_trnconfirmmst cm INNER JOIN QL_mstgen g ON genoid=confirmwhoid WHERE isnull(transtype,'')='Sheet' AND ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "cm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "cm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND confirmmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND confirmmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND confirmmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND confirmdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND confirmdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND confirmmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cm.createuser='" + Session["UserID"].ToString() + "'";

            List<confirmmst> dt = db.Database.SqlQuery<confirmmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnconfirmmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________

            return View(dt);
        }
        
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnconfirmmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnconfirmmst();
                tbl.cmpcode = CompnyCode;
                tbl.confirmdate = ClassFunction.GetServerTime();
                tbl.confirmmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnconfirmmst.Find(cmp, id);
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_trnconfirmmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            ViewBag.confirmwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>("SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc").ToList(), "genoid", "gendesc", tbl.confirmwhoid);

            ViewBag.prodresno = db.Database.SqlQuery<string>("select prodresno from ql_trnprodresmst where prodresmstoid='" + tbl.prodresmstoid + "'").FirstOrDefault();

        }

        public class listspk
        {
            public string prodresno { get; set; }
            public string wono { get; set; }
            public string sono { get; set; }
            public string itemlongdesc { get; set; }
            public string custname { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetailSPK(int womstoid)
        {
            var result = "";
            JsonResult js = null;
            List<listspk> tbl = new List<listspk>();

            try
            {
                sSql = "SELECT prodresno, wono, sorawno sono, sodtldesc itemlongdesc, custname FROM QL_trnprodresmst pm INNER JOIN QL_trnwomst wo ON wo.womstoid=pm.womstoid  inner join QL_mstcust c on c.custoid=wo.custoid inner join QL_trnsorawdtl sod on sod.sorawdtloid=wo.sodtloid inner join QL_trnsorawmst so on so.sorawmstoid=sod.sorawmstoid WHERE isnull(wo.womstres1,'')='Sheet' AND wo.womstoid='" + womstoid + "' ORDER BY prodresno";
                tbl = db.Database.SqlQuery<listspk>(sSql).ToList();
                if (tbl.Count == 0)
                    result = "Data tidak ditemukan";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prodresmstoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 confirmdtlseq, pm.prodresmstoid, prodresno, pm.womstoid,ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=pm.cmpcode AND wom.womstoid=pm.womstoid), '') wono, pm.itemoid, '' itemcode, sodtldesc itemlongdesc, (resultqty - ISNULL((SELECT SUM(confirmqty) FROM QL_trnconfirmdtl cd WHERE cd.cmpcode=pm.cmpcode AND cd.prodresmstoid=pm.prodresmstoid), 0)) prodresqty, (resultqty - ISNULL((SELECT SUM(confirmqty) FROM QL_trnconfirmdtl cd WHERE cd.cmpcode=pm.cmpcode AND cd.prodresmstoid=pm.prodresmstoid), 0)) confirmqty, 0.0 rejectqty, 0 confirmunitoid, 'PCS' confirmunit, '' confirmdtlnote, 0.0 confirmdm, 0.0 confirmdl, 0.0 confirmfoh, 0.0 confirmvalue FROM QL_trnprodresmst pm INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=pm.itemoid  inner join ql_mstgen ep on ep.genoid=pm.processoid WHERE pm.cmpcode='" + cmp + "' and pm.prodresmstoid='" + prodresmstoid + "' AND prodresmststatus='Post' AND UPPER(ep.gendesc) IN('CORRUGATOR', 'FINISHING') and isnull(prodresmstres1,'')='Sheet' ORDER BY prodresno";
                var tbl = db.Database.SqlQuery<confirmdtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class conwip : QL_conwip
        {
            public string prodresno { get; set; }
            public string itemlongdesc { get; set; }
            public string matrawlongdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails2(List<confirmdtl> dtl)
        {
            JsonResult js = null;
            try
            {
                if (dtl.Any())
                {
                    var tbl = new List<conwip>();
                    foreach (var d in dtl)
                    {
                        var flute = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == d.womstoid)?.flute ?? "";
                        var lsheet = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == d.womstoid)?.lsheet ?? 0;
                        if (!string.IsNullOrEmpty(flute))
                        {
                            decimal price_avg = 0; decimal std = 1/1000;
                            var bps = db.QL_mstbpsheet.FirstOrDefault(x => x.flute.ToLower() == flute.ToLower());
                            if (bps != null)
                            {
                                //borax
                                price_avg = db.QL_trnporawdtl.Where(x=> x.matrawoid == bps.borax_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                decimal qty = lsheet * d.confirmqty * bps.borax_price;
                                decimal price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                decimal amt = qty * price;
                                if(amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.borax_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.borax_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.borax_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //soda
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.soda_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.soda_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if (amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.soda_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.soda_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.soda_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //tapioka_rb
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.tapioka_rb_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.tapioka_rb_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if (amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.tapioka_rb_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.tapioka_rb_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.tapioka_rb_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //tapioka_rb2
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.tapioka_rb2_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.tapioka_rb2_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if (amt > 0){
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.tapioka_rb2_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.tapioka_rb2_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.tapioka_rb2_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //tapioka_sg
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.tapioka_sg_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.tapioka_sg_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if (amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.tapioka_sg_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.tapioka_sg_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.tapioka_sg_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //soda_fl
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.soda_fl_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.soda_fl_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if (amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.soda_fl_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.soda_fl_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.soda_fl_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                                //cms
                                price_avg = db.QL_trnporawdtl.Where(x => x.matrawoid == bps.cms_matrawoid).OrderByDescending(r => r.porawdtloid).FirstOrDefault()?.porawprice ?? 0;
                                qty = lsheet * d.confirmqty * bps.cms_price;
                                price = price_avg * std * (flute == "BCF" ? 2 : 1);
                                amt = qty * price;
                                if(amt > 0)
                                {
                                    tbl.Add(new conwip()
                                    {
                                        conwipseq = 0,
                                        prodresmstoid = d.prodresmstoid,
                                        prodresno = d.prodresno,
                                        confirmmstoid = 0,
                                        confirmdtloid = 0,
                                        flute = flute,
                                        refname = "Sheet",
                                        refoid = d.itemoid,
                                        itemlongdesc = d.itemlongdesc,
                                        bpsheetoid = bps.bpsheetoid,
                                        matrawoid = bps.cms_matrawoid,
                                        matrawlongdesc = db.QL_mstmatraw.FirstOrDefault(x => x.matrawoid == bps.cms_matrawoid)?.matrawlongdesc ?? "",
                                        acctgoid = bps.cms_acctgoid,
                                        qty = qty,
                                        price = price,
                                        amt = amt,
                                    });
                                }
                               
                            }
                        } 
                    }
                    if (tbl == null || tbl.Count <= 0)
                        js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                        js.MaxJsonLength = Int32.MaxValue;
                    }
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<confirmdtl>();
            var dtl2 = new List<conwip>();
            try
            {
                sSql = $"SELECT confirmdtlseq, cd.prodresmstoid, pm.womstoid,prodresno, ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=cd.cmpcode AND wom.womstoid=pm.womstoid), '') wono, '' deptname, pm.itemoid, '' itemcode, sodtldesc itemlongdesc, (resultqty - ISNULL((SELECT SUM(cdx.confirmqty) FROM QL_trnconfirmdtl cdx WHERE cdx.cmpcode=cd.cmpcode AND cdx.prodresmstoid=cd.prodresmstoid AND cdx.confirmmstoid<>cd.confirmmstoid), 0)) prodresqty, confirmqty, cd.rejectqty, 0 confirmunitoid, 'PCS' confirmunit, ISNULL(confirmdtlnote, '') confirmdtlnote, confirmdm, confirmdl, confirmfoh, confirmvalue FROM QL_trnconfirmdtl cd INNER JOIN QL_trnprodresmst pm ON pm.cmpcode=cd.cmpcode AND pm.prodresmstoid=cd.prodresmstoid INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=pm.itemoid WHERE cd.cmpcode='" + CompnyCode + "' AND cd.confirmmstoid=" + id + " ORDER BY confirmdtlseq";
                dtl = db.Database.SqlQuery<confirmdtl>(sSql).ToList();
                sSql = $"SELECT cd.*, isnull((select x.prodresno from QL_trnprodresmst x where x.prodresmstoid=cd.prodresmstoid),'') prodresno, isnull((select x.sodtldesc from QL_trnsorawdtl x where x.sorawdtloid=cd.refoid),'') itemlongdesc, isnull((select x.matrawlongdesc from QL_mstmatraw x where x.matrawoid=cd.matrawoid),'') matrawlongdesc FROM QL_conwip cd WHERE cd.cmpcode='" + CompnyCode + "' AND cd.confirmmstoid=" + id + " ORDER BY conwipseq";
                dtl2 = db.Database.SqlQuery<conwip>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl, dtl2 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string generateBatchNo(int i)
        {
            var tgl = ClassFunction.GetServerTime();
            var sNo = tgl.ToString("yyyy.MM.dd") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault() + i, 6);
            sNo = sNo + sCounter;

            return sNo;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnconfirmmst tbl, List<confirmdtl> dtl, List<conwip> dtl2, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed"; var hdrid = "0";decimal qtyprod = 0M;
            
            if (tbl.confirmdate == null || tbl.confirmdate <= new DateTime(1900, 1, 1))
                msg += "Silahkan isi Tgl Serah Terima!<br />";
            if (tbl.confirmwhoid == 0)
                msg += "Silahkan pilih Gudang!<br />";
            if (dtl == null || dtl.Count <= 0)
                msg += "Silahkan isi data detail!<br />";
            if (dtl2 == null || dtl2.Count <= 0)
                msg += "Silahkan Generate Bahan Penolong!<br />";
            if (tbl.womstoid == 0)
                msg += "Pilih WO !<BR/>";
            else
            {
                for (int i = 0; i < dtl.Count(); i++)
                {
                    sSql = "select resultqty - isnull((select sum(confirmqty+rejectqty) from ql_trnconfirmdtl x where x.prodresmstoid=pm.prodresmstoid and x.confirmmstoid<>" + tbl.confirmmstoid +"),0.0) qty from ql_trnprodresmst pm where pm.prodresmstoid=" + dtl[i].prodresmstoid + "";
                    qtyprod = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                    if (qtyprod != dtl[i].prodresqty)
                        dtl[i].prodresqty = qtyprod;
                    if (dtl[i].confirmqty <= 0)
                        msg += "Qty untuk Item " + dtl[i].itemlongdesc + " harus lebih besar dari 0!<br />";
                    //else if (dtl[i].confirmqty > dtl[i].prodresqty)
                    //msg += "Qty untuk Item " + dtl[i].itemlongdesc + " harus lebih kecil dari Qty Produksi!<br />";

                }
            }
            

            if (msg == "")
            {
                int mstoid = ClassFunction.GenerateID("QL_trnconfirmmst");
                int dtloid = ClassFunction.GenerateID("QL_trnconfirmdtl");
                int iConOid = ClassFunction.GenerateID("QL_CONMAT");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var servertime = ClassFunction.GetServerTime();
                var sDateGL = servertime.ToString("MM/dd/yyyy").ToDateTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
                tbl.periodacctg = sPeriod;
                tbl.confirmno = "";
                if (tbl.confirmmststatus == "Post")
                {
                    tbl.confirmno = GenerateNo(tbl.cmpcode);
                    tbl.postappuser = Session["UserID"].ToString();
                    tbl.postapptime = servertime;
                }
                tbl.transtype = "Sheet";
                var processoid = db.QL_trnprodresmst.FirstOrDefault(x => x.prodresmstoid == tbl.prodresmstoid).processoid;
                var cek_wo = db.QL_trnwodtl.FirstOrDefault(x => x.womstoid == tbl.womstoid && x.procesoid == processoid).wodtlseq;

                var iAcctgoidBOP = 0; var iAcctgoidFG = 0;
                if (tbl.confirmmststatus.ToLower() == "post")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_BOP_BEBAN", tbl.cmpcode))
                    {
                        msg += ClassFunction.GetInterfaceWarning("VAR_BOP_BEBAN") + "<BR>";
                    }
                    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid.Value))
                    {
                        msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_FG") + "<BR>";
                    }
                    iAcctgoidBOP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_BOP_BEBAN", tbl.cmpcode));
                    iAcctgoidFG = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid.Value));
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updtime = tbl.createtime;
                            tbl.confirmmstoid = mstoid;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnconfirmmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.confirmmstoid + " WHERE tablename='QL_trnconfirmmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            sSql = "UPDATE QL_trnprodresmst SET prodresmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND prodresmstoid IN (SELECT DISTINCT prodresmstoid FROM QL_trnconfirmdtl WHERE cmpcode='" + tbl.cmpcode + "' AND confirmmstoid=" + tbl.confirmmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            //sSql = "UPDATE QL_trnwomst SET womstres2='' WHERE cmpcode='" + tbl.cmpcode + "' AND womstoid IN (SELECT DISTINCT womstoid FROM QL_trnconfirmdtl WHERE cmpcode='" + tbl.cmpcode + "' AND confirmmstoid=" + tbl.confirmmstoid + ")";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();

                            var trndtl = db.QL_trnconfirmdtl.Where(a => a.confirmmstoid == tbl.confirmmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnconfirmdtl.RemoveRange(trndtl);

                            var trndtl2 = db.QL_conwip.Where(a => a.confirmmstoid == tbl.confirmmstoid);
                            db.QL_conwip.RemoveRange(trndtl2);
                        }    

                        QL_trnconfirmdtl tbldtl1; QL_conwip tbldtl2;
                        for (int i = 0; i < dtl.Count(); i++)
                        {
                            tbldtl1 = new QL_trnconfirmdtl();
                            tbldtl1.cmpcode = tbl.cmpcode;
                            tbldtl1.divgroupoid = tbl.divgroupoid;
                            tbldtl1.confirmmstoid = tbl.confirmmstoid;
                            tbldtl1.confirmdtloid = dtloid++;
                            tbldtl1.confirmdtlseq = dtl[i].confirmdtlseq;
                            tbldtl1.prodresmstoid = dtl[i].prodresmstoid;
                            tbldtl1.itemoid = dtl[i].itemoid;
                            tbldtl1.confirmqty = dtl[i].confirmqty;
                            tbldtl1.rejectqty = dtl[i].rejectqty;
                            tbldtl1.confirmunitoid = dtl[i].confirmunitoid;
                            tbldtl1.confirmdm = dtl[i].confirmdm;
                            tbldtl1.confirmdl = dtl[i].confirmdl;
                            tbldtl1.confirmfoh = dtl[i].confirmfoh;
                            tbldtl1.confirmvalue = dtl[i].confirmvalue;
                            tbldtl1.confirmdtlnote = dtl[i].confirmdtlnote;
                            tbldtl1.womstoid = dtl[i].womstoid;
                            tbldtl1.upduser = tbl.upduser;
                            tbldtl1.updtime = tbl.updtime;

                            db.QL_trnconfirmdtl.Add(tbldtl1);
                            db.SaveChanges();

                            foreach (var d in dtl2.Where(x => x.prodresmstoid == tbldtl1.prodresmstoid))
                            {
                                tbldtl2 = (QL_conwip)ClassFunction.MappingTable(new QL_conwip(), d);
                                tbldtl2.cmpcode = tbl.cmpcode;
                                tbldtl2.conwipdate = sDateGL;
                                tbldtl2.confirmmstoid = tbl.confirmmstoid;
                                tbldtl2.confirmdtloid = tbldtl1.confirmdtloid;
                                tbldtl2.upduser = tbl.upduser;
                                tbldtl2.updtime = tbl.updtime;

                                db.QL_conwip.Add(tbldtl2);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_trnprodresmst SET prodresmststatus='Closed' WHERE prodresmstoid=" + dtl[i].prodresmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //if(dtl[i].prodresqty<=(dtl[i].confirmqty+ dtl[i].rejectqty))
                            //{
                            //sSql = "UPDATE QL_trnprodresmst SET prodresmststatus='Closed' WHERE prodresmstoid="+ dtl[i].prodresmstoid + "";
                            //    db.Database.ExecuteSqlCommand(sSql);
                            //    db.SaveChanges();
                            //sSql = "UPDATE QL_trnwomst SET womstres2='Closed' WHERE womstoid=" + dtl[i].womstoid + "";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();
                            //}

                            if (tbl.confirmmststatus == "Post" && cek_wo == 1)
                            {
                                int divgroupoid = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == tbldtl1.womstoid)?.divgroupoid ?? 0;
                                int cust_id = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == tbldtl1.womstoid)?.custoid ?? 0;
                                decimal value_final = dtl2.Where(x => x.prodresmstoid == tbldtl1.prodresmstoid).Sum(s => s.amt) / tbldtl1.confirmqty;
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "CFM", "QL_trnconfirmdtl", tbl.confirmmstoid, tbldtl1.itemoid, "SHEET", tbl.confirmwhoid, tbldtl1.confirmqty, "Prodres Confirmation", tbl.confirmno, tbl.upduser, generateBatchNo(i), "", value_final, 0, tbldtl1.womstoid, null, tbldtl1.confirmdtloid, divgroupoid, "", cust_id: cust_id));
                                db.SaveChanges();
                            }
                        }

                        if (tbl.confirmmststatus == "Post" && cek_wo == 1)
                        {
                            decimal amt_final = dtl2.Sum(s => s.amt);
                            // Insert QL_trnglmst BP
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDateGL, tbl.periodacctg, "BP Sheet Confirmation|No. " + tbl.confirmno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid.Value));
                            db.SaveChanges();

                            int iSeq = 1;
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidFG, "D", amt_final, tbl.confirmno, "BP Sheet Confirmation|No. " + tbl.confirmno + "", "Post", tbl.upduser, servertime, amt_final, 0, "QL_trnconfirmmst " + tbl.confirmmstoid.ToString(), null, null, null, 0, tbl.divgroupoid.Value));
                            db.SaveChanges();
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidBOP, "C", amt_final, tbl.confirmno, "BP Sheet Confirmation|No. " + tbl.confirmno + "", "Post", tbl.upduser, servertime, amt_final, 0, "QL_trnconfirmmst " + tbl.confirmmstoid.ToString(), null, null, null, 0, tbl.divgroupoid.Value));
                            db.SaveChanges();

                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={glmstoid} WHERE tablename='QL_trnglmst'");
                            db.SaveChanges();
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={gldtloid - 1} WHERE tablename='QL_trngldtl'");
                            db.SaveChanges();
                        } 

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnconfirmdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstoid SET lastoid=" + iConOid + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        result = "success";
                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.confirmmstoid.ToString();
                        msg = "Data telah disimpan dengan No. Draft " + hdrid + "<br />";
                        if (tbl.confirmmststatus == "Post")
                            msg += "Data telah diposting dengan No. Serah Terima " + tbl.confirmno + "<br />";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string GenerateNo(string cmp)
        {
            string sNo = "CFM-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(confirmno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnconfirmmst WHERE cmpcode='" + cmp + "' AND confirmno LIKE '" + sNo + "%'";
            return sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnconfirmmst tbl = db.QL_trnconfirmmst.Find(cmp, id);

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnprodresmst SET prodresmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND prodresmstoid IN (SELECT DISTINCT prodresmstoid FROM QL_trnconfirmdtl WHERE cmpcode='" + CompnyCode + "' AND confirmmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnwomst SET womstres2='' WHERE cmpcode='" + CompnyCode + "' AND womstoid IN (SELECT DISTINCT womstoid FROM QL_trnconfirmdtl WHERE cmpcode='" + CompnyCode + "' AND confirmmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnconfirmdtl.Where(a => a.confirmmstoid == id && a.cmpcode == cmp);
                        db.QL_trnconfirmdtl.RemoveRange(trndtl);

                        var trndtl2 = db.QL_conwip.Where(a => a.confirmmstoid == tbl.confirmmstoid);
                        db.QL_conwip.RemoveRange(trndtl2);

                        db.QL_trnconfirmmst.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            var rptname = "rptProdResConfirm";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT cm.confirmmstoid [No. Draft], confirmno [No. Serah Terima], confirmdate [Tgl Serah Terima], g.gendesc [Gudang], confirmmstnote [Catatan Header], confirmmststatus [Status], cm.createuser [Dibuat Oleh], cm.createtime [Dibuat Pada], cm.upduser [Terakhir Diedit Oleh], cm.updtime [Terakhir Diedit Pada], cm.postappuser [Diposting Oleh], cm.postapptime [Diposting Pada], confirmdtlseq [No.], prodresno [No. Produksi], wono [No. PP], deptname [Departemen], itemcode [Kode Obat], itemlongdesc [Nama Obat], confirmqty [Qty], gu.gendesc [Satuan], confirmdtlnote [Catatan] FROM QL_trnconfirmmst cm INNER JOIN QL_mstgen g ON g.genoid=confirmwhoid INNER JOIN QL_trnconfirmdtl cd ON cd.cmpcode=cm.cmpcode AND cd.confirmmstoid=cm.confirmmstoid INNER JOIN QL_trnprodresmst pm ON pm.cmpcode=cd.cmpcode AND pm.prodresmstoid=cd.prodresmstoid INNER JOIN QL_trnwomst wom ON wom.cmpcode=pm.cmpcode AND wom.womstoid=pm.womstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=pm.cmpcode AND wod2.wodtl2oid=pm.wodtl2oid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.deptoid INNER JOIN QL_mstitem i ON i.itemoid=cd.itemoid INNER JOIN QL_mstgen gu ON gu.genoid=confirmunitoid WHERE cm.cmpcode='" + cmp + "' AND cm.confirmmstoid=" + id + " ORDER BY confirmdtlseq";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", Session["UserName"].ToString());
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            report.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            var conttype = "application/pdf"; var fileext = "pdf"; var crysdes = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
            Stream stream = report.ExportToStream(crysdes);
            stream.Seek(0, SeekOrigin.Begin);
            report.Dispose(); report.Close();
            return File(stream, conttype, "PrintOutSPOJ." + fileext);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}