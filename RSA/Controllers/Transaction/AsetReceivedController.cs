﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class AsetReceivedController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnmrassetmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int mrassetmstoid { get; set; }
            public int mrassetwhoid { get; set; }
            public string mrassetno { get; set; }
            public string poassetno { get; set; }
            public DateTime mrassetdate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string mrassetmststatus { get; set; }
            public string mrassetmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnmrassetdtl
        {
            public int mrassetdtlseq { get; set; }
            public int poassetdtloid { get; set; }
            public string poassetno { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal poassetdtlqty { get; set; }
            public decimal mrassetqty { get; set; }
            public int mrassetunitoid { get; set; }
            public string mrassetunit { get; set; }
            public string mrassetdtlnote { get; set; }
            public decimal poqty { get; set; }
            public decimal mrassetvalue { get; set; }
            public decimal mrassetamt { get; set; }
            public string location { get; set; }

        }

        private void InitDDL(QL_trnmrassetmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genother1='" + tbl.cmpcode + "' AND genoid>0";
            var mrassetwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.mrassetwhoid = mrassetwhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genother1='" + cmp + "' AND genoid>0";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int poassetmstoid, int suppoid, int mrassetmstoid, int mrassetwhoid)
        {
            List<trnmrassetdtl> tbl = new List<trnmrassetdtl>();

            sSql = "SELECT 0 AS mrassetdtlseq, regd.poassetdtloid, poassetno, regd.poassetrefoid AS matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (poassetqty - ISNULL((SELECT SUM(mrassetqty) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrassetmstoid = mrd.mrassetmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.poassetdtloid = regd.poassetdtloid AND mrm2.registermstoid = regm.poassetmstoid AND mrd.mrassetmstoid <> " + mrassetmstoid + "), 0) ) AS poassetdtlqty, 0.0 AS mrassetqty, poassetunitoid AS mrassetunitoid, gendesc AS mrassetunit, '' AS mrassetdtlnote, (CASE poassetdtldiscvalue WHEN 0 THEN poassetprice ELSE(poassetdtlnetto / poassetqty) END) AS mrassetvalue, ''[location], poassetqty AS poqty, poassetdtlseq, regm.poassetmstoid " +
                "FROM QL_trnpoassetdtl regd " +
                "INNER JOIN QL_trnpoassetmst regm ON regm.cmpcode = regd.cmpcode AND regd.poassetmstoid = regm.poassetmstoid " +
                "INNER JOIN QL_mstmatgen m ON m.matgenoid = regd.poassetrefoid " +
                "INNER JOIN QL_mstgen g ON genoid = regd.poassetunitoid " +
                "WHERE regd.cmpcode = '" + CompnyCode + "' AND regd.poassetmstoid = " + poassetmstoid + " AND poassetdtlstatus = '' AND ISNULL(regm.poassetmstres2, '')<> 'Complete' ORDER BY poassetdtlseq";

            tbl = db.Database.SqlQuery<trnmrassetdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmrassetdtl> dtDtl)
        {
            Session["QL_trnmrassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmrassetdtl"] == null)
            {
                Session["QL_trnmrassetdtl"] = new List<trnmrassetdtl>();
            }

            List<trnmrassetdtl> dataDtl = (List<trnmrassetdtl>)Session["QL_trnmrassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmrassetmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poassetno = db.Database.SqlQuery<string>("SELECT poassetno FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnpoassetmst WHERE cmpcode='" + cmp + "' AND poassetmststatus='Approved' AND (CASE poassettype WHEN 'IMPORT' THEN poassetmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(poassetmstres2, '')<>'Closed' and divgroupoid=" + divgroupoid + ")  ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int poassetmstoid { get; set; }
            public string poassetno { get; set; }
            public DateTime poassetdate { get; set; }
            public string poassetdocrefno { get; set; }
            public DateTime poassetdocrefdate { get; set; }
            public string poassetmstnote { get; set; }
            public string poassettype { get; set; }
            public int curroid { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int divgroupoid)
        {
            List<pomst> tbl = new List<pomst>();

            sSql = "SELECT poassetmstoid, poassetno, poassetdate, '' [poassetdocrefno], poassetmstnote, poassettype, curroid FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + suppoid + " AND poassetmststatus='Approved' AND (CASE poassettype WHEN 'IMPORT' THEN poassetmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(poassetmstres2, '')<>'Closed' and divgroupoid=" + divgroupoid + " ORDER BY poassetmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "mrm", divgroupoid, "divgroupoid");
            sSql = " SELECT mrm.cmpcode, (select gendesc from ql_mstgen where genoid=mrm.divgroupoid) divgroup, mrassetmstoid, mrassetno, mrassetdate, suppname, poassetno, mrassetmststatus, ISNULL(mrassetmstnote, '') [mrassetmstnote], divname FROM QL_trnmrassetmst mrm INNER JOIN QL_trnpoassetmst reg ON reg.cmpcode = mrm.cmpcode AND reg.poassetmstoid = mrm.poassetmstoid INNER JOIN QL_mstsupp s ON s.suppoid = mrm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode = mrm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "mrm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND mrassetmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND mrassetmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND mrassetmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND mrassetdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrassetdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND mrassetmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND mrm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnmrassetmst> dt = db.Database.SqlQuery<trnmrassetmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnmrassetmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmrassetmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnmrassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.mrassetmstoid = ClassFunction.GenerateID("QL_trnmrassetmst");
                tbl.mrassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrassetmststatus = "In Process";


                Session["QL_trnmrassetdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmrassetmst.Find(cmp, id);
                sSql = "SELECT mrassetdtlseq, mrd.poassetdtloid, poassetno, mrd.mrassetrefoid matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (poassetqty - ISNULL((SELECT SUM(x.mrassetqty) FROM QL_trnmrassetdtl x INNER JOIN QL_trnmrassetmst y ON y.cmpcode=x.cmpcode AND y.mrassetmstoid=x.mrassetmstoid WHERE x.cmpcode=mrd.cmpcode AND x.poassetdtloid=mrd.poassetdtloid AND reg.poassetmstoid=y.poassetmstoid AND x.mrassetmstoid<>mrd.mrassetmstoid), 0) /*- ISNULL((SELECT SUM(retgenqty) FROM QL_trnretgendtl x WHERE x.cmpcode=mrd.cmpcode AND x.poassetdtloid=mrd.poassetdtloid), 0)*/) AS poassetqty, (mrassetqty + mrassetbonusqty) AS mrqty, mrassetqty, mrassetbonusqty, mrassetunitoid, gendesc AS mrassetunit, ISNULL(mrassetdtlnote, '') [mrassetdtlnote], mrassetvalue, (mrassetvalue * mrassetqty) AS mrassetamt, '' AS location, poassetqty AS regqty " +
                    "FROM QL_trnmrassetdtl mrd " +
                    "INNER JOIN QL_mstmatgen m ON m.matgenoid = mrd.mrassetrefoid " +
                    "INNER JOIN QL_mstgen g ON genoid = mrassetunitoid " +
                    "INNER JOIN QL_trnpoassetdtl reg ON reg.cmpcode = mrd.cmpcode AND reg.poassetdtloid = mrd.poassetdtloid AND poassetmstoid IN(SELECT poassetmstoid FROM QL_trnmrassetmst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mrassetmstoid = mrd.mrassetmstoid) " +
                    "INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = reg.cmpcode AND pom.poassetmstoid = reg.poassetmstoid " +
                    "WHERE mrassetmstoid = " + id + "  AND mrd.cmpcode = '" + cmp + "' ORDER BY mrassetdtlseq";
                Session["QL_trnmrassetdtl"] = db.Database.SqlQuery<trnmrassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private Boolean IsRegisterWillBeClosed(string cmp, int registermstoid)
        {
            List<trnmrassetdtl> dtDtl = (List<trnmrassetdtl>)Session["QL_trnmrassetdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrassetqty >= dtDtl[i].poqty)
                {
                    sOid += dtDtl[i].poassetdtlqty + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" + cmp + "' AND registermstoid=" + registermstoid + " AND registerdtlstatus='' AND registerdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }

        private Boolean IsPOrWillBeClosed(string cmp, int poitemmstoid)
        {
            List<trnmrassetdtl> dtDtl = (List<trnmrassetdtl>)Session["QL_trnmrassetdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrassetqty >= dtDtl[i].poqty)
                {
                    sOid += dtDtl[i].poassetdtlqty + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnpoitemdtl WHERE cmpcode='" + cmp + "' AND poitemmstoid=" + poitemmstoid + " AND poitemdtlstatus='' AND poitemdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }


        public class import
        {
            public int curroid { get; set; }
            public decimal importvalue { get; set; }
        }

        private decimal GetImportCost(string cmp, int registermstoid)
        {
            decimal dImp = 0;
            List<import> tbl = new List<import>();
            sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" + cmp + "' AND icm.registermstoid=" + registermstoid + " GROUP BY icd.curroid ";
            tbl = db.Database.SqlQuery<import>(sSql).ToList();
            for (int i = 0; i < tbl.Count(); i++)
            {
                //cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
                dImp += tbl[i].importvalue;// * cRateTmp.GetRateMonthlyIDRValue
            }

            return dImp;
        }

        public class dtHdr
        {
            public int mrassetmstoid { get; set; }
            public DateTime mrassetdate { get; set; }
            public string mrassetno { get; set; }
            public int mrassetwhoid { get; set; }
        }

        public class dtDtl
        {
            public int mrassetmstoid { get; set; }
            public int mrassetdtloid { get; set; }
            public int mrassetdtlseq { get; set; }
            public int matgenoid { get; set; }
            public int mrassetqty { get; set; }
            public decimal mrassetvalue { get; set; }
            public decimal mrassetamt { get; set; }
        }


        private void GetLastData(string cmp, int registermstoid, int mrassetmstoid, out List<dtHdr> dtLastHdrData, out List<dtDtl> dtLastDtlData)
        {
            dtLastHdrData = new List<dtHdr>();
            sSql = "SELECT mrassetmstoid, mrassetdate, mrassetno, mrassetwhoid FROM QL_trnmrassetmst WHERE cmpcode='" + cmp + "' AND mrassetmststatus='Post' AND mrassetno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrassetmstoid<>" + mrassetmstoid;
            sSql += " ORDER BY mrassetmstoid ";
            dtLastHdrData = db.Database.SqlQuery<dtHdr>(sSql).ToList();

            dtLastDtlData = new List<dtDtl>();
            sSql = "SELECT mrm.mrassetmstoid, mrassetdtloid, mrassetdtlseq, matgenoid, mrassetqty, mrassetvalue, (mrassetqty * mrassetvalue) AS mrassetamt FROM QL_trnmrassetmst mrm INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrassetmstoid=mrm.mrassetmstoid WHERE mrm.cmpcode='" + cmp + "' AND mrassetmststatus='Post' AND mrassetno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrm.mrassetmstoid<>" + mrassetmstoid;
            sSql += " ORDER BY mrm.mrassetmstoid, mrassetdtlseq ";
            dtLastDtlData = db.Database.SqlQuery<dtDtl>(sSql).ToList();
        }

        private string generateNo(string cmp)
        {
            var sNo = "GMR-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrassetmst WHERE cmpcode='" + cmp + "' AND mrassetno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrassetmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();

            if (tbl.mrassetno == null)
                tbl.mrassetno = "";

            List<trnmrassetdtl> dtDtl = (List<trnmrassetdtl>)Session["QL_trnmrassetdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mrassetdtlnote == null)
                            dtDtl[i].mrassetdtlnote = "";
                        if (dtDtl[i].mrassetqty <= 0)
                            ModelState.AddModelError("", "MR QTY for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        sSql = "SELECT (poassetqty - ISNULL((SELECT SUM(mrassetqty) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrassetmstoid=mrd.mrassetmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.poassetdtloid=regd.poassetdtloid AND mrm2.poassetmstoid=regd.poassetmstoid  AND mrd.mrassetmstoid<>" + tbl.mrassetmstoid + "), 0) ) AS poqty  FROM QL_trnpoassetdtl regd WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.poassetdtloid=" + dtDtl[i].poassetdtloid + " AND regd.poassetmstoid=" + tbl.poassetmstoid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].poqty)
                            dtDtl[i].poqty = dQty;
                        if (dQty < dtDtl[i].mrassetqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PO Qty!");

                        dtDtl[i].mrassetamt = dtDtl[i].mrassetqty * dtDtl[i].mrassetvalue;
                    }
                }
            }

            if (tbl.mrassetmststatus.ToUpper() == "POST")
            {
                var sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid))
                {
                    sVarErr = "VAR_PURC_RECEIVED";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                var msg = "";
                var regdate = db.Database.SqlQuery<DateTime>("SELECT poassetdate FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid).FirstOrDefault();

                cRate.SetRateValue(tbl.curroid, regdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;

                if (msg != "")
                    ModelState.AddModelError("", msg);
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmrassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrassetdtl");
                
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                var regflag = db.Database.SqlQuery<string>("SELECT poassettype FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid).FirstOrDefault();
                
                var iGLMstOid = 0;
                var iGLDtlOid = 0;
                var iRecAcctgOid = 0;
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

                if (tbl.mrassetmststatus.ToUpper() == "POST")
                {
                    iGLMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                    iGLDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                    iRecAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid));
                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                    tbl.mrassetno = generateNo(tbl.cmpcode);
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmrassetmst.Find(tbl.cmpcode, tbl.mrassetmstoid) != null)
                                tbl.mrassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mrassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnmrassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.mrassetmstoid + " WHERE tablename='QL_trnmrassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid IN (SELECT poassetdtloid FROM QL_trnmrassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + tbl.mrassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmrassetdtl.Where(a => a.mrassetmstoid == tbl.mrassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmrassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmrassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnmrassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.mrassetdtloid = dtloid++;
                            tbldtl.mrassetmstoid = tbl.mrassetmstoid;
                            tbldtl.mrassetdtlseq = i + 1;
                            tbldtl.poassetdtloid = dtDtl[i].poassetdtloid;
                            tbldtl.mrassetreftype ="QL_mstmatgen";
                            tbldtl.mrassetrefoid = dtDtl[i].matgenoid;
                            tbldtl.mrassetqty = dtDtl[i].mrassetqty;
                            tbldtl.mrassetbonusqty = 0;
                            tbldtl.mrassetunitoid = dtDtl[i].mrassetunitoid;
                            tbldtl.mrassetvalueidr = dtDtl[i].mrassetvalue * cRate.GetRateMonthlyIDRValue; 
                            tbldtl.mrassetvalueusd = dtDtl[i].mrassetvalue * cRate.GetRateMonthlyUSDValue;
                            tbldtl.mrassetdtlstatus = "";
                            tbldtl.mrassetdtlnote = dtDtl[i].mrassetdtlnote;
                            tbldtl.mrassetvalue = dtDtl[i].mrassetvalue;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnmrassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].mrassetqty >= dtDtl[i].poqty)
                            {
                                sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetdtloid=" + dtDtl[i].poassetdtloid + " AND poassetmstoid=" + tbl.poassetmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND (SELECT COUNT(*) FROM QL_trnpoassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid<>" + dtDtl[i].poassetdtloid + " AND poassetdtlstatus='')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnmrassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.mrassetmststatus.ToUpper() == "POST")
                        {
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, iGLMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "MR Aset|No. " + tbl.mrassetno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid));
                            db.SaveChanges();

                            int seq = 1;
                            foreach (var d in dtDtl)
                            {
                                int iAssetAcctgOid = db.QL_mstmatgen.FirstOrDefault(x => x.matgenoid == d.matgenoid)?.acctgoid ?? 0;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, seq, iGLMstOid, iAssetAcctgOid, "D", d.mrassetamt, tbl.mrassetno, "MR Aset|No. " + tbl.mrassetno, "Post", Session["UserID"].ToString(), servertime, d.mrassetamt * cRate.GetRateMonthlyIDRValue, d.mrassetamt * cRate.GetRateMonthlyUSDValue, "QL_trnmrassetmst " + tbl.mrassetmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                                seq++;
                            }
                            decimal totMR = dtDtl.Sum(x => x.mrassetamt);

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, seq, iGLMstOid, iRecAcctgOid, "C", totMR, tbl.mrassetno, "MR Aset|No. " + tbl.mrassetno, "Post", Session["UserID"].ToString(), servertime, totMR * cRate.GetRateMonthlyIDRValue, totMR * cRate.GetRateMonthlyUSDValue, "QL_trnmrassetmst " + tbl.mrassetmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + iGLMstOid + " WHERE tablename='QL_TRNGLMST' AND cmpcode='" + CompnyCode + "'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" + CompnyCode + "'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.mrassetmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.mrassetno = "";
            tbl.mrassetmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrassetmst tbl = db.QL_trnmrassetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid IN (SELECT poassetdtloid FROM QL_trnmrassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + tbl.mrassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmrassetdtl.Where(a => a.mrassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnmrassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmrassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmrassetmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMR.rpt"));
            //sSql = "SELECT mrm.mrassetmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrassetmstoid)) AS [Draft No.], mrm.mrassetno AS [MR No.], mrm.mrassetdate AS [MR Date], mrm.mrassetmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrassetmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrassetmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrassetmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrassetdtlseq AS [No.] , m.matgencode AS [Code], m.matgenlongdesc AS [Description], mrd.mrassetqty AS [Qty], mrassetbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrassetdtlnote AS [Note], pom.poassetno AS [PO No.], pom.poassetdate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg PostDate], registerdocrefdate AS [Tgl. SJ], registerdocrefno [No SJ],registernopol [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] " +
            //    ", ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=mrm.cmpcode AND loc.mtrwhoid=mrm.mrassetwhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)='Raw' AND loc.matoid=mrd.matgenoid),'') AS [Detail Location] " +
            //    " FROM QL_trnmrassetmst mrm INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrassetmstoid=mrm.mrassetmstoid INNER JOIN QL_mstmatgen m ON  m.matgenoid=mrd.matgenoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrassetunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrassetwhoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=mrm.cmpcode AND  pom.poassetmstoid=regd.porefmstoid INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode=mrm.cmpcode AND pod.poassetdtloid=regd.porefdtloid INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='GEN' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrassetmstoid IN (" + id + ") ORDER BY mrm.mrassetmstoid, mrassetdtlseq ";
            sSql = "SELECT mrm.mrassetmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrassetmstoid)) AS [Draft No.], mrm.mrassetno AS [MR No.], mrm.mrassetdate AS [MR Date], mrm.mrassetmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrassetmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrassetmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrassetmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrassetdtlseq AS [No.] , m.matgencode AS [Code], m.matgenlongdesc AS [Description], mrd.mrassetqty AS [Qty], mrassetbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrassetdtlnote AS [Note], regm.poassetno AS [PO No.], regm.poassetdate AS [PO Date], regm.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE regm.approvaluser=p2.profoid) AS [PO UserApp] ,regm.poassetno [Reg No.],regm.poassetdate [Reg Date], regm.updtime AS [Reg PostDate], CAST('01/01/1900' AS date) AS [Tgl. SJ], '' [No SJ], '' [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost], '' AS [Detail Location] " +
                " FROM QL_trnmrassetmst mrm  " +
                "INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrassetmstoid=mrm.mrassetmstoid  " +
                "INNER JOIN QL_mstmatgen m ON  m.matgenoid=mrd.mrassetrefoid  " +
                "INNER JOIN QL_mstgen g ON g.genoid=mrd.mrassetunitoid  " +
                "INNER JOIN QL_mstgen g1 ON g1.genoid=mrassetwhoid  " +
                "INNER JOIN QL_trnpoassetdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.poassetmstoid=mrm.poassetmstoid AND regd.poassetdtloid=mrd.poassetdtloid " +
                "INNER JOIN QL_trnpoassetmst regm ON mrm.cmpcode = regm.cmpcode AND regm.poassetmstoid = mrm.poassetmstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid  " +
                "WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrassetmstoid IN (" + id + ") ORDER BY mrm.mrassetmstoid, mrassetdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintmrasset");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("Header", "GENERAL MATERIAL RECEIVED PRINT OUT");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "GeneralMaterialReceivedPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
