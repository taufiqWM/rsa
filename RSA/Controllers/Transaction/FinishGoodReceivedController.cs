﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class FinishGoodReceivedController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnmritemmst
        {
            public string cmpcode { get; set; }
            public int mritemmstoid { get; set; }
            public int mritemwhoid { get; set; }
            public string mritemno { get; set; }
            public string poitemno { get; set; }
            public DateTime mritemdate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string mritemmststatus { get; set; }
            public string mritemmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnmritemdtl
        {
            public int mritemdtlseq { get; set; }
            public int poitemdtloid { get; set; }
            public string poitemno { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal itemlimitqty { get; set; }
            public decimal poitemqty { get; set; }
            public decimal mritemqty { get; set; }
            public int mritemunitoid { get; set; }
            public string mritemunit { get; set; }
            public string mritemdtlnote { get; set; }
            public decimal poqty { get; set; }
            public decimal mritemvalue { get; set; }
            public decimal mritemamt { get; set; }
            public string location { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnmritemmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genother1='" + tbl.cmpcode + "' AND genoid>0";
            var mritemwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.mritemwhoid = mritemwhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genother1='" + cmp + "' AND genoid>0";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int poitemmstoid, int suppoid, int mritemmstoid, int mritemwhoid)
        {
            List<trnmritemdtl> tbl = new List<trnmritemdtl>();


            //sSql = "SELECT 0 AS mritemdtlseq, regd.registerdtloid, poitemno, regd.matrefoid AS itemoid, itemcode, itemlongdesc, itemlimitqty, (poitemqty - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mritemmstoid = mrd.mritemmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.registerdtloid = regd.registerdtloid AND mrm2.registermstoid = regm.registermstoid AND mrd.mritemmstoid<>" + mritemmstoid + "), 0) -ISNULL((SELECT SUM(retd.retitemqty) FROM QL_trnretitemdtl retd WHERE retd.cmpcode = regd.cmpcode AND retd.registerdtloid = regd.registerdtloid), 0)) AS poitemqty, 0.0 AS mritemqty, registerunitoid AS mritemunitoid, gendesc AS mritemunit, '' AS mritemdtlnote, (CASE poitemdtldiscvalue WHEN 0 THEN poitemprice ELSE(poitemdtlnetto / poitemqty) END) AS mritemvalue  , ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode='" + cmp + "' AND loc.mtrwhoid=" + mritemwhoid + " AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' WHEN 'item' THEN 'Finish Good' ELSE '' END)='Raw' AND loc.matoid=m.itemoid),'') AS location, poitemqty AS regqty /*, 0.0 AS mritemamt */, registerdtlseq, regm.registermstoid  FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regd.registermstoid=regm.registermstoid INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=regd.cmpcode AND poitemmstoid=porefmstoid INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode=regd.cmpcode AND pod.poitemmstoid=pom.poitemmstoid AND poitemdtloid=porefdtloid INNER JOIN QL_mstitem m ON m.itemoid=regd.matrefoid INNER JOIN QL_mstgen g ON genoid=registerunitoid WHERE regd.cmpcode='" + cmp + "' AND regd.registermstoid=" + registermstoid + " AND registerdtlstatus='' AND ISNULL(registerdtlres2, '')<>'Complete' ORDER BY registerdtlseq";

            sSql = "SELECT 0 AS mritemdtlseq, regd.poitemdtloid, poitemno, regd.itemoid AS itemoid, itemcode, itemlongdesc, itemlimitqty, (poitemqty - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mritemmstoid = mrd.mritemmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.poitemdtloid = regd.poitemdtloid AND mrm2.poitemmstoid = regm.poitemmstoid AND mrd.mritemmstoid <> " + mritemmstoid + "), 0) - ISNULL((SELECT SUM(retd.retitemqty) FROM QL_trnretitemdtl retd WHERE retd.cmpcode = regd.cmpcode AND retd.poitemdtloid = regd.poitemdtloid), 0)) AS poitemqty, 0.0 AS mritemqty, poitemunitoid AS mritemunitoid, gendesc AS mritemunit, '' AS mritemdtlnote, (CASE poitemdtldiscvalue WHEN 0 THEN poitemprice ELSE(poitemdtlnetto / poitemqty) END) AS mritemvalue, '' AS location, poitemqty AS poqty, poitemdtlseq, regm.poitemmstoid " +
                "FROM QL_trnpoitemdtl regd " +
                "INNER JOIN QL_trnpoitemmst regm ON regm.cmpcode = regd.cmpcode AND regd.poitemmstoid = regm.poitemmstoid " +
                "INNER JOIN QL_mstitem m ON m.itemoid = regd.itemoid " +
                "INNER JOIN QL_mstgen g ON genoid = regd.poitemunitoid " +
                "WHERE regd.cmpcode = '" + cmp + "' AND regd.poitemmstoid = " + poitemmstoid + " AND poitemdtlstatus = '' AND ISNULL(poitemdtlres2, '')<> 'Complete' ORDER BY poitemdtlseq";

            tbl = db.Database.SqlQuery<trnmritemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmritemdtl> dtDtl)
        {
            Session["QL_trnmritemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmritemdtl"] == null)
            {
                Session["QL_trnmritemdtl"] = new List<trnmritemdtl>();
            }

            List<trnmritemdtl> dataDtl = (List<trnmritemdtl>)Session["QL_trnmritemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmritemmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poitemno = db.Database.SqlQuery<string>("SELECT poitemno FROM QL_trnpoitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<mstsupp> tbl = new List<mstsupp>();
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnpoitemmst WHERE cmpcode='" + cmp + "' AND poitemmststatus='Approved' AND (CASE poitemtype WHEN 'IMPORT' THEN poitemmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(poitemmstres2, '')<>'Closed') ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public DateTime poitemdate { get; set; }
            public string poitemdocrefno { get; set; }
            public DateTime poitemdocrefdate { get; set; }
            public string poitemmstnote { get; set; }
            public string poitemtype { get; set; }
            public int curroid { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid)
        {
            List<pomst> tbl = new List<pomst>();
            sSql = "SELECT poitemmstoid, poitemno, poitemdate, '' [poitemdocrefno], poitemmstnote, poitemtype, curroid  FROM QL_trnpoitemmst WHERE cmpcode='" + cmp + "' AND suppoid=" + suppoid + " AND poitemmststatus='Approved' AND (CASE poitemtype WHEN 'IMPORT' THEN poitemmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(poitemmstres2, '')<>'Closed' ORDER BY poitemmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = " SELECT mrm.cmpcode, mritemmstoid, mritemno, mritemdate, suppname, poitemno, mritemmststatus, mritemmstnote, divname FROM QL_trnmritemmst mrm INNER JOIN QL_trnpoitemmst reg ON reg.cmpcode=mrm.cmpcode AND reg.poitemmstoid=mrm.poitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=mrm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "mrm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND mritemdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mritemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND mritemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND mritemdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mritemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND mritemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND mritemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mritemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND mritemmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND mritemmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND mrm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnmritemmst> dt = db.Database.SqlQuery<trnmritemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmritemmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnmritemmst();
                tbl.cmpcode = CompnyCode;
                tbl.mritemmstoid = ClassFunction.GenerateID("QL_trnmritemmst");
                tbl.mritemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mritemmststatus = "In Process";


                Session["QL_trnmritemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmritemmst.Find(cmp, id);
                sSql = "SELECT mritemdtlseq, mrd.poitemdtloid, poitemno, mrd.itemoid, itemcode, itemlongdesc, itemlimitqty, (poitemqty - ISNULL((SELECT SUM(x.mritemqty) FROM QL_trnmritemdtl x INNER JOIN QL_trnmritemmst y ON y.cmpcode = x.cmpcode AND y.mritemmstoid = x.mritemmstoid WHERE x.cmpcode = mrd.cmpcode AND x.poitemdtloid = mrd.poitemdtloid AND reg.poitemmstoid = y.poitemmstoid AND x.mritemmstoid <> mrd.mritemmstoid), 0) - ISNULL((SELECT SUM(retitemqty) FROM QL_trnretitemdtl x WHERE x.cmpcode = mrd.cmpcode AND x.poitemdtloid = mrd.poitemdtloid), 0)) AS poitemqty, (mritemqty + mritembonusqty) AS mrqty, mritemqty, mritembonusqty, mritemunitoid, gendesc AS mritemunit, mritemdtlnote, mritemvalue, (mritemvalue * mritemqty) AS mritemamt, '' AS location, poitemqty AS poqty  " +
                    "FROM QL_trnmritemdtl mrd " +
                    "INNER JOIN QL_mstitem m ON m.itemoid = mrd.itemoid " +
                    "INNER JOIN QL_mstgen g ON genoid = mritemunitoid " +
                    "INNER JOIN QL_trnpoitemdtl reg ON reg.cmpcode = mrd.cmpcode AND reg.poitemdtloid = mrd.poitemdtloid AND poitemmstoid IN(SELECT poitemmstoid FROM QL_trnmritemmst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mritemmstoid = mrd.mritemmstoid) " +
                    "INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode = reg.cmpcode AND pom.poitemmstoid = reg.poitemmstoid " +
                    "WHERE mritemmstoid=" + id + " AND mrd.cmpcode='" + cmp + "' ORDER BY mritemdtlseq";
                Session["QL_trnmritemdtl"] = db.Database.SqlQuery<trnmritemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private Boolean IsRegisterWillBeClosed(string cmp, int registermstoid)
        {
            List<trnmritemdtl> dtDtl = (List<trnmritemdtl>)Session["QL_trnmritemdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mritemqty >= dtDtl[i].poitemqty)
                {
                    sOid += dtDtl[i].poitemdtloid + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" + cmp + "' AND registermstoid=" + registermstoid + " AND registerdtlstatus='' AND registerdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }

        private Boolean IsPOWillBeClosed(string cmp, int poitemmstoid)
        {
            List<trnmritemdtl> dtDtl = (List<trnmritemdtl>)Session["QL_trnmritemdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mritemqty >= dtDtl[i].poitemqty)
                {
                    sOid += dtDtl[i].poitemdtloid + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnpoitemdtl WHERE cmpcode='" + cmp + "' AND poitemmstoid=" + poitemmstoid + " AND poitemdtlstatus='' AND poitemdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }


        public class import
        {
            public int curroid { get; set; }
            public decimal importvalue { get; set; }
        }

        private decimal GetImportCost(string cmp, int registermstoid)
        {
            decimal dImp = 0;
            List<import> tbl = new List<import>();
            sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" + cmp + "' AND icm.registermstoid=" + registermstoid + " GROUP BY icd.curroid ";
            tbl = db.Database.SqlQuery<import>(sSql).ToList();
            for (int i = 0; i < tbl.Count(); i++)
            {
                //cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
                dImp += tbl[i].importvalue;// * cRateTmp.GetRateMonthlyIDRValue
            }

            return dImp;
        }

        public class dtHdr
        {
            public int mritemmstoid { get; set; }
            public DateTime mritemdate { get; set; }
            public string mritemno { get; set; }
            public int mritemwhoid { get; set; }
        }

        public class dtDtl
        {
            public int mritemmstoid { get; set; }
            public int mritemdtloid { get; set; }
            public int mritemdtlseq { get; set; }
            public int itemoid { get; set; }
            public int mritemqty { get; set; }
            public decimal mritemvalue { get; set; }
            public decimal mritemamt { get; set; }
        }


        private void GetLastData(string cmp, int registermstoid, int mritemmstoid, out List<dtHdr> dtLastHdrData, out List<dtDtl> dtLastDtlData)
        {
            dtLastHdrData = new List<dtHdr>();
            sSql = "SELECT mritemmstoid, mritemdate, mritemno, mritemwhoid FROM QL_trnmritemmst WHERE cmpcode='" + cmp + "' AND mritemmststatus='Post' AND mritemno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'FGR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'FGR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mritemmstoid<>" + mritemmstoid;
            sSql += " ORDER BY mritemmstoid ";
            dtLastHdrData = db.Database.SqlQuery<dtHdr>(sSql).ToList();

            dtLastDtlData = new List<dtDtl>();
            sSql = "SELECT mrm.mritemmstoid, mritemdtloid, mritemdtlseq, itemoid, mritemqty, mritemvalue, (mritemqty * mritemvalue) AS mritemamt FROM QL_trnmritemmst mrm INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mritemmstoid=mrm.mritemmstoid WHERE mrm.cmpcode='" + cmp + "' AND mritemmststatus='Post' AND mritemno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'FGR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'FGR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrm.mritemmstoid<>" + mritemmstoid;
            sSql += " ORDER BY mrm.mritemmstoid, mritemdtlseq ";
            dtLastDtlData = db.Database.SqlQuery<dtDtl>(sSql).ToList();
        }

        private string generateNo(string cmp)
        {
            var sNo = "FGR-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mritemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmritemmst WHERE cmpcode='" + cmp + "' AND mritemno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmritemmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.mritemno == null)
                tbl.mritemno = "";

            List<trnmritemdtl> dtDtl = (List<trnmritemdtl>)Session["QL_trnmritemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mritemqty <= 0)
                        {
                            ModelState.AddModelError("", "MR QTY for Material " + dtDtl[i].itemlongdesc + " must be more than 0");
                        }
                        sSql = "SELECT (poitemqty - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mritemmstoid=mrd.mritemmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.poitemdtloid=regd.poitemdtloid AND mrm2.poitemmstoid=regd.poitemmstoid  AND mrd.mritemmstoid<>" + tbl.mritemmstoid + "), 0) - ISNULL((SELECT SUM(retd.retitemqty) FROM QL_trnretitemdtl retd WHERE retd.cmpcode=regd.cmpcode AND retd.poitemdtloid=regd.poitemdtloid), 0)) AS poitemqty FROM QL_trnpoitemdtl regd  WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.poitemdtloid=" + dtDtl[i].poitemdtloid + " AND regd.poitemmstoid=" + tbl.poitemmstoid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].poitemqty)
                            dtDtl[i].poitemqty = dQty;
                        if (dQty < dtDtl[i].mritemqty)
                            ModelState.AddModelError("", "Some Reg Qty has been updated by another user. Please check that every Qty must be less than Reg Qty!");

                        dtDtl[i].mritemamt = dtDtl[i].mritemqty * dtDtl[i].mritemvalue;
                    }
                }
            }

            if (tbl.mritemmststatus.ToUpper() == "POST")
            {
                var sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", tbl.cmpcode))
                {
                    sVarErr = "VAR_STOCK_FG";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", tbl.cmpcode))
                {
                    sVarErr = "VAR_PURC_RECEIVED";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }

                var msg = "";
                var podate = db.Database.SqlQuery<DateTime>("SELECT poitemdate FROM QL_trnpoitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid).FirstOrDefault();

                cRate.SetRateValue(tbl.curroid, podate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;

                if (msg != "")
                    ModelState.AddModelError("", msg);
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmritemdtl");
                var dtloid2 = ClassFunction.GenerateID("QL_trnmritemdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                var regflag = db.Database.SqlQuery<string>("SELECT poitemtype FROM QL_trnpoitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid).FirstOrDefault();

                Boolean isRegClosed = false;
                decimal dImportCost = 0;
                decimal dSumMR = 0;
                decimal dSumMR_IDR = 0;
                decimal dSumMR_USD = 0;
                List<dtHdr> dtLastHdrData = null;
                List<dtDtl> dtLastDtlData = null;
                var iConMatOid = 0;
                var iCrdMtrOid = 0;
                var iGLMstOid = 0;
                var iGLDtlOid = 0;
                var iStockValOid = 0;
                var iStockAcctgOid = 0;
                var iRecAcctgOid = 0;
                //DateTime sDate = ClassFunction.GetServerTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                var sDateGRIR = "";

                if (tbl.mritemmststatus.ToUpper() == "POST")
                {
                    //    cRate.SetRateValue(CInt(curroid.Text), registerdate.Text)
                    //If cRate.GetRateDailyLastError <> "" Then
                    //    showMessage(cRate.GetRateDailyLastError, 2)
                    //    mritemmststatus.Text = "In Process"
                    //    Exit Sub
                    //End If
                    //If cRate.GetRateMonthlyLastError <> "" Then
                    //    showMessage(cRate.GetRateMonthlyLastError, 2)
                    //    mritemmststatus.Text = "In Process"
                    //    Exit Sub
                    //End If
                    if (regflag.ToUpper() == "IMPORT")
                    {
                        isRegClosed = IsPOWillBeClosed(tbl.cmpcode, tbl.poitemmstoid);
                    }
                    if (regflag.ToUpper() == "IMPORT" && isRegClosed == true)
                    {
                        dImportCost = GetImportCost(tbl.cmpcode, tbl.poitemmstoid) / cRate.GetRateMonthlyIDRValue;
                        GetLastData(tbl.cmpcode, tbl.poitemmstoid, tbl.mritemmstoid, out dtLastHdrData, out dtLastDtlData);
                        dSumMR += dtLastDtlData.Sum(x => x.mritemamt);
                    }
                    if ((regflag.ToUpper() == "LOCAL") || (regflag.ToUpper() == "IMPORT" & isRegClosed == true))
                    {
                        iConMatOid = ClassFunction.GenerateID("QL_CONMAT");
                        iCrdMtrOid = ClassFunction.GenerateID("QL_CRDMTR");
                        iGLMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                        iGLDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                        iStockValOid = ClassFunction.GenerateID("QL_STOCKVALUE");
                        iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tbl.cmpcode));
                        iRecAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", tbl.cmpcode));
                        sDateGRIR = ClassFunction.GetServerTime().ToString("MM/dd/yyyy  hh:mm:ss");
                        //tbl.grirposttime = ClassFunction.GetServerTime();
                    }
                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                    tbl.mritemno = generateNo(tbl.cmpcode);
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmritemmst.Find(tbl.cmpcode, tbl.mritemmstoid) != null)
                                tbl.mritemmstoid = mstoid;

                            tbl.registermstoid = 0;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mritemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            //tbl.grirposttime = new DateTime(1900, 01, 01);
                            db.QL_trnmritemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.mritemmstoid + " WHERE tablename='QL_trnmritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + " AND poitemdtloid IN (SELECT poitemdtloid FROM QL_trnmritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mritemmstoid=" + tbl.mritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmritemdtl.Where(a => a.mritemmstoid == tbl.mritemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmritemdtl tbldtl;
                        dSumMR += dtDtl.Sum(x => x.mritemamt);
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dMRVal = 0;
                            if (dSumMR > 0)
                            {
                                dMRVal = (dtDtl[i].mritemvalue + ((dtDtl[i].mritemvalue / dSumMR) * dImportCost));
                            }
                            if (dtDtl[i].mritemdtlnote == null)
                                dtDtl[i].mritemdtlnote = "";
                            dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * (dtDtl[i].mritemqty));
                            dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mritemqty));
                            //dSumMR_IDR += Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * ToDouble(objTable.Rows(C1)("mritemqty").ToString)
                            //dSumMR_USD += Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * ToDouble(objTable.Rows(C1)("mritemqty").ToString)

                            tbldtl = new QL_trnmritemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.mritemdtloid = dtloid++;
                            tbldtl.mritemmstoid = tbl.mritemmstoid;
                            tbldtl.mritemdtlseq = i + 1;
                            tbldtl.poitemdtloid = dtDtl[i].poitemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.mritemqty = dtDtl[i].mritemqty;
                            tbldtl.mritembonusqty = 0;
                            tbldtl.mritemunitoid = dtDtl[i].mritemunitoid;
                            tbldtl.mritemvalueidr = dMRVal * cRate.GetRateMonthlyIDRValue; //* cRate.GetRateMonthlyIDRValue;
                            tbldtl.mritemvalueusd = dMRVal * cRate.GetRateMonthlyUSDValue; //* cRate.GetRateMonthlyUSDValue;
                            tbldtl.mritemdtlstatus = "";
                            tbldtl.mritemdtlnote = dtDtl[i].mritemdtlnote;
                            tbldtl.mritemvalue = dMRVal;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnmritemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].mritemqty >= dtDtl[i].poitemqty)
                            {
                                sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemdtloid=" + dtDtl[i].poitemdtloid + " AND poitemmstoid=" + tbl.poitemmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnpoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + " AND poitemdtloid<>" + dtDtl[i].poitemdtloid + " AND poitemdtlstatus='')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnmritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.mritemmststatus.ToUpper() == "POST")
                        {
                            if (regflag.ToUpper() == "IMPORT" && isRegClosed == true)
                            {
                                for (int i = 0; i < dtLastHdrData.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnmritemmst SET grirposttime='" + sDateGRIR + "' WHERE cmpcode='" + tbl.cmpcode + "' AND mritemmstoid=" + dtLastHdrData[i].mritemmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    var a = dtLastDtlData.Where(x => x.mritemmstoid == dtLastHdrData[i].mritemmstoid);
                                    for (int k = 0; k < a.Count(); k++)
                                    {
                                        decimal dMRVal = 0;
                                        if (dSumMR > 0)
                                        {
                                            dMRVal = (dtLastDtlData[k].mritemvalue + ((dtLastDtlData[k].mritemvalue / dSumMR) * dImportCost));
                                        }
                                        dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * (dtDtl[i].mritemqty));
                                        dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mritemqty));

                                        sSql = "UPDATE QL_trnmritemdtl SET mritemvalue=" + dMRVal + ", mritemvalueidr=" + (dMRVal * cRate.GetRateMonthlyIDRValue) + ", mritemvalueusd=" + (dMRVal * cRate.GetRateMonthlyUSDValue) + " WHERE cmpcode='" + tbl.cmpcode + "' AND mritemdtloid=" + dtLastDtlData[k].mritemdtloid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();

                                        // Insert QL_conmat
                                        db.QL_conmat.Add(ClassFunction.InsertConMat(tbl.cmpcode, iConMatOid++, "FGR", "QL_trnmritemdtl", dtLastHdrData[i].mritemmstoid, dtLastDtlData[k].itemoid, "FINISH GOOD", dtLastHdrData[i].mritemwhoid, dtLastDtlData[k].mritemqty, "Finish Good Received", dtLastHdrData[i].mritemno, Session["UserID"].ToString(), "","", (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), 0, null, dtLastDtlData[k].mritemdtloid,  0));
                                        db.SaveChanges();

                               

                                        //Insert QL_stockvalue
                                        sSql = ClassFunction.GetQueryUpdateStockValue(dtLastDtlData[k].mritemqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmritemdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtLastDtlData[k].itemoid, "FINISH GOOD");
                                        if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                        {
                                            db.SaveChanges();
                                            sSql = sSql = ClassFunction.GetQueryInsertStockValue(dtLastDtlData[k].mritemqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmritemdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtLastDtlData[k].itemoid, "FINISH GOOD", iStockValOid++);
                                            db.Database.ExecuteSqlCommand(sSql);
                                        }
                                        db.SaveChanges();

                                    }// for Dtl
                                } //for Hdr
                            } // import

                            if ((regflag.ToUpper() == "LOCAL") || (regflag.ToUpper() == "IMPORT" & isRegClosed == true))
                            {
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    decimal dMRVal = 0;
                                    if (dSumMR > 0)
                                    {
                                        dMRVal = (dtDtl[i].mritemvalue + ((dtDtl[i].mritemvalue / dSumMR) * dImportCost));
                                    }

                                    // Insert QL_conmat
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(tbl.cmpcode, iConMatOid++, "FGR", "QL_trnmritemdtl", tbl.mritemmstoid, dtDtl[i].itemoid, "FINISH GOOD", tbl.mritemwhoid, dtDtl[i].mritemqty, "Finish Good Received", tbl.mritemno, Session["UserID"].ToString(), "","", (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), 0, null, dtloid2++,0));
                                    db.SaveChanges();

                                    sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd, formdtloid) VALUES ('" + tbl.cmpcode + "', " + iConMatOid + ", 'FGR', 1, '" + servertime + "', '" + sPeriod + "', 'QL_trnmritemdtl', " + tbl.mritemmstoid + ", " + dtDtl[i].itemoid + ", 'FINISH GOOD', " + tbl.mritemwhoid + ", " + dtDtl[i].mritemqty + ", 0, 'Finish Good Received', '" + tbl.mritemno + "', '" + Session["UserID"].ToString() + "', CURRENT_TIMESTAMP, " + (dMRVal * cRate.GetRateMonthlyIDRValue) + ", " + (dMRVal * cRate.GetRateMonthlyUSDValue) + ", " + (dtloid2++) + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    iConMatOid += 1;

                                  
                                    //Insert QL_stockvalue
                                    sSql = ClassFunction.GetQueryUpdateStockValue(dtDtl[i].mritemqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmritemdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].itemoid, "FINISH GOOD");
                                    if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                    {
                                        db.SaveChanges();
                                        sSql = sSql = ClassFunction.GetQueryInsertStockValue(dtDtl[i].mritemqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmritemdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].itemoid, "FINISH GOOD", iStockValOid++);
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    db.SaveChanges();
                                }
                                dSumMR += dImportCost;
                                if (dSumMR > 0)
                                {
                                    // Insert QL_trnglmst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, iGLMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "MR FG|No. " + tbl.mritemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 1, iGLMstOid, iStockAcctgOid, "D", dSumMR, tbl.mritemno, "MR FG|No. " + tbl.mritemno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmritemmst " + tbl.mritemmstoid.ToString(), null, null, null, 0, 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 2, iGLMstOid, iRecAcctgOid, "C", dSumMR, tbl.mritemno, "MR FG|No. " + tbl.mritemno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmritemmst " + tbl.mritemmstoid.ToString(), null, null, null, 0,  0));
                                    db.SaveChanges();

                                    iGLMstOid += 1;
                                }

                                sSql = " UPDATE QL_mstoid SET lastoid=" + (iConMatOid - 1) + " WHERE tablename='QL_CONMAT' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iCrdMtrOid - 1) + " WHERE tablename='QL_CRDMTR' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLMstOid - 1) + " WHERE tablename='QL_TRNGLMST' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } //update Stock & GL
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.mritemmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.mritemno = "";
            tbl.mritemmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tbl = db.QL_trnmritemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + " AND poitemdtloid IN (SELECT poitemdtloid FROM QL_trnmritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mritemmstoid=" + tbl.mritemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmritemdtl.Where(a => a.mritemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnmritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmritemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult PrintReport(int id, string cmp)
        //{
        //    if (Session["UserID"] == null)
        //        return RedirectToAction("Login", "Account");
        //    if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
        //        return RedirectToAction("NotAuthorize", "Account");

        //    ReportDocument report = new ReportDocument();
        //    var tbl = db.QL_trnmritemmst.Find(cmp, id);
        //    if (tbl == null)
        //        return null;

        //    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMR.rpt"));
        //    sSql = "SELECT mrm.mritemmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mritemmstoid)) AS [Draft No.], mrm.mritemno AS [MR No.], mrm.mritemdate AS [MR Date], mrm.mritemmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mritemmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mritemmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mritemmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mritemdtlseq AS [No.] , m.itemcode AS [Code], m.itemlongdesc AS [Description], mrd.mritemqty AS [Qty], mritembonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mritemdtlnote AS [Note], pom.poitemno AS [PO No.], pom.poitemdate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg PostDate], registerdocrefdate AS [Tgl. SJ], registerdocrefno [No SJ],registernopol [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] " +
        //        ", ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=mrm.cmpcode AND loc.mtrwhoid=mrm.mritemwhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)='Raw' AND loc.matoid=mrd.itemoid),'') AS [Detail Location] " +
        //        " FROM QL_trnmritemmst mrm INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mritemmstoid=mrm.mritemmstoid INNER JOIN QL_mstitem m ON  m.itemoid=mrd.itemoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mritemunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=mritemwhoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=mrm.cmpcode AND  pom.poitemmstoid=regd.porefmstoid INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode=mrm.cmpcode AND pod.poitemdtloid=regd.porefdtloid INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='ITEM' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + cmp + "' AND mrm.mritemmstoid IN (" + id + ") ORDER BY mrm.mritemmstoid, mritemdtlseq ";

        //    ClassConnection cConn = new ClassConnection();
        //    DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintmritem");

        //    report.SetDataSource(dtRpt);
        //    report.SetParameterValue("Header", "FINISH GOOD RECEIVED PRINT OUT");
        //    report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
        //    report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
        //    Response.Buffer = false;
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    return File(stream, "application/pdf", "FinishGoodReceivedPrintOut.pdf");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
