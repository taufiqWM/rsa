﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class DOCancellationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class domst
        {
            public string cmpcode { get; set; }
            public int domstoid { get; set; }
            public int custoid { get; set; }
            public string dono { get; set; }
            public string sType { get; set; }
            public string dodate { get; set; }
            public string domststatus { get; set; }
            public string domstnote { get; set; }
            public string custname { get; set; }
            public decimal dograndtotalamt { get; set; }
        }

        public class dodtl
        {
            public string cmpcode { get; set; }
            public int dodtloid { get; set; }
            public int domstoid { get; set; }
            public string dotype { get; set; }
            public int dodtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal doqty { get; set; }
            public string dounit { get; set; }
            public string dodtlnote { get; set; }
            public string dodtlstatus { get; set; }
            public string domststatus { get; set; }
            public decimal doprice { get; set; }
            public int assetmstoid { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetDeliveryOrderData(string cmp, string sType)
        {
            List<domst> tbl = new List<domst>();
            sSql = "SELECT do" + sType + "mstoid AS domstoid, do" + sType + "no AS dono, CONVERT(VARCHAR(10),do" + sType + "date,101) AS dodate, do" + sType + "mststatus AS domststatus, do" + sType + "mstnote AS domstnote, c.custname FROM QL_trndo" + sType + "mst dom INNER JOIN QL_mstcust C ON dom.custoid=c.custoid WHERE dom.cmpcode='" + cmp + "' AND do" + sType + "mststatus='Post' AND do" + sType + "mstoid NOT IN (SELECT do" + sType + "mstoid FROM QL_trnshipment" + sType + "dtl WHERE cmpcode='" + cmp + "') ORDER BY CAST (do" + sType + "date AS DATETIME) DESC, domstoid DESC";
            tbl = db.Database.SqlQuery<domst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<dodtl> tbl = new List<dodtl>();
            if (sType.ToUpper() == "RAW")
            {
                sSql = "SELECT dod.dorawdtloid AS dodtloid, dod.dorawdtlseq AS dodtlseq, dod.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, dod.dorawqty AS doqty, g.gendesc AS dounit, dod.dorawdtlnote AS dodtlnote, dod.dorawdtlstatus AS dodtlstatus, dorawprice as doprice, 0 assetmstoid FROM QL_trndorawdtl dod INNER JOIN QL_mstmatraw m ON dod.matrawoid=m.matrawoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON dod.dorawunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.dorawmstoid=" + iOid;
            } else if (sType.ToUpper() == "GEN")
            {
                sSql = "SELECT dod.dogendtloid AS dodtloid, dod.dogendtlseq AS dodtlseq, dod.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, dod.dogenqty AS doqty, g.gendesc AS dounit, dod.dogendtlnote AS dodtlnote, dod.dogendtlstatus AS dodtlstatus, dogenprice as doprice, 0 assetmstoid FROM QL_trndogendtl dod INNER JOIN QL_mstmatgen m ON dod.matgenoid=m.matgenoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON dod.dogenunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.dogenmstoid=" + iOid;
            } else if (sType.ToUpper() == "SP")
            {
                sSql = "SELECT dod.dospdtloid AS dodtloid, dod.dospdtlseq AS dodtlseq, dod.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, dod.dospqty AS doqty, g.gendesc AS dounit, dod.dospdtlnote AS dodtlnote, dod.dospdtlstatus AS dodtlstatus, dospprice as doprice, 0 assetmstoid FROM QL_trndospdtl dod INNER JOIN QL_mstsparepart m ON dod.sparepartoid=m.sparepartoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON dod.dospunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.dospmstoid=" + iOid;
            } else if (sType.ToUpper() == "ITEM")
            {
                sSql = "SELECT dod.doitemdtloid AS dodtloid, dod.doitemdtlseq AS dodtlseq, dod.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, dod.doitemqty AS doqty, g.gendesc AS dounit, dod.doitemdtlnote AS dodtlnote, dod.doitemdtlstatus AS dodtlstatus, doitemprice as doprice, 0 assetmstoid FROM QL_trndoitemdtl dod INNER JOIN QL_mstitem m ON dod.itemoid=m.itemoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON dod.doitemunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.doitemmstoid=" + iOid;
            } else if (sType.ToUpper() == "ASSET")
            {
                sSql = "SELECT dod.doassetdtloid AS dodtloid, dod.doassetdtlseq AS dodtlseq, fam.refoid AS matoid, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matcode, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matlongdesc, dod.doassetqty AS doqty, g.gendesc AS dounit, dod.doassetdtlnote AS dodtlnote, dod.doassetdtlstatus AS dodtlstatus, dod.doassetprice as doprice, fam.assetmstoid FROM QL_trndoassetdtl dod INNER JOIN QL_assetmst fam ON fam.assetmstoid=dod.assetmstoid INNER JOIN QL_mstgen g ON dod.doassetunitoid=g.genoid WHERE dod.cmpcode='" + cmp + "' AND dod.doassetmstoid=" + iOid;
            } else if (sType.ToUpper() == "LOG")
            {
                sSql = "SELECT dod.dologdtloid AS dodtloid, dod.dologdtlseq AS dodtlseq, dod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, dod.dologqty AS doqty, g.gendesc AS dounit, dod.dologdtlnote AS dodtlnote, dod.dologdtlstatus AS dodtlstatus, dologprice as doprice FROM QL_trndologdtl dod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=dod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON dod.dologunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.dologmstoid=" + iOid;
            } else if (sType.ToUpper() == "SAWN")
            {
                sSql = "SELECT dod.dosawndtloid AS dodtloid, dod.dosawndtlseq AS dodtlseq, dod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, dod.dosawnqty AS doqty, g.gendesc AS dounit, dod.dosawndtlnote AS dodtlnote, dod.dosawndtlstatus AS dodtlstatus, dosawnprice as doprice FROM QL_trndosawndtl dod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=dod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON dod.dosawnunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE dod.cmpcode='" + cmp + "' AND dod.dosawnmstoid=" + iOid;
            }
            tbl = db.Database.SqlQuery<dodtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<dodtl> dtDtl)
        {
            Session["QL_trndodtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            List<dodtl> dtDtl = (List<dodtl>)Session["QL_trndodtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].domstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select do Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    sSql = "SELECT COUNT(*) FROM QL_trndo" + dtDtl[0].dotype + "mst WHERE do" + dtDtl[0].dotype + "mstoid=" + dtDtl[0].domstoid + " AND do" + dtDtl[0].dotype + "mststatus='Cancel'";
                    var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck > 0)
                    {
                        ModelState.AddModelError("", "This data has been canceled by another user. Please CANCEL this transaction and try again!");
                    }
                    else
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnshipment" + dtDtl[0].dotype + "dtl dod WHERE do" + dtDtl[0].dotype + "mstoid=" + dtDtl[0].domstoid;
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "This data has been created Shipment by another user. Please CANCEL this transaction and try again!");
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            sSql = "UPDATE QL_trndo" + dtDtl[0].dotype + "mst SET do" + dtDtl[0].dotype + "mststatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND do" + dtDtl[0].dotype + "mstoid=" + dtDtl[0].domstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnso" + dtDtl[0].dotype + "dtl SET so" + dtDtl[0].dotype + "dtlstatus='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].dotype + "dtloid IN (SELECT so" + dtDtl[0].dotype + "dtloid FROM QL_trndo" + dtDtl[0].dotype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND do" + dtDtl[0].dotype + "mstoid=" + dtDtl[0].domstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnso"+ dtDtl[0].dotype + "mst SET so" + dtDtl[0].dotype + "mststatus='Approved' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].dotype + "mstoid IN (SELECT so" + dtDtl[0].dotype + "mstoid FROM QL_trndo" + dtDtl[0].dotype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND do" + dtDtl[0].dotype + "mstoid=" + dtDtl[0].domstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            objTrans.Commit();
                            Session["QL_trndodtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}