﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ShipmentFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class shipmentitemmst
        {
            public string cmpcode { get; set; }
            public int shipmentitemmstoid { get; set; }
            public string divgroup { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public string custname { get; set; }
            public string shipmentitemcontno { get; set; }
            public string shipmentitemmststatus { get; set; }
            public string shipmentitemmstnote { get; set; }
            public string divname { get; set; }
        }

        public class shipmentitemdtl
        {
            public int shipmentitemdtlseq { get; set; }
            public int doitemmstoid { get; set; }
            public string soitemno { get; set;}
            public string doitemno { get; set; }
            public string doitemdate { get; set; }
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
            public int doitemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public int doitemunitoid { get; set; }
            public string doitemunit { get; set; }
            public decimal stockqty { get; set; }
            public decimal doitemqty { get; set; }
            public decimal shipmentitemqty { get; set; }
            public int shipmentitemunitoid { get; set; }
            public string shipmentitemunit { get; set; }
            public string shipmentitemdtlnote { get; set; }
            public decimal itemlimitqty { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custtype { get; set; }
            public string custaddr { get; set; }
            public string doitemcustpono { get; set; }
        }

        public class warehouse
        {
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
        }

        private void InitDDL(QL_trnshipmentitemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;



            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0";
            var shipmentitemwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.shipmentitemwhoid = shipmentitemwhoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT cust.custoid, cust.custcode, cust.custname, cust.custtype, cust.custaddr, ISNULL(drm.doitemcustpono,'') doitemcustpono FROM QL_mstcust cust LEFT JOIN QL_trndoitemmst drm ON cust.cmpcode=drm.cmpcode AND cust.custoid=drm.custoid WHERE cust.cmpcode='" + CompnyCode + "' AND cust.divgroupoid=" + divgroupoid + " AND cust.custoid IN (SELECT DISTINCT custoid FROM QL_trndoitemmst WHERE cmpcode='" + cmp + "' AND doitemmststatus='Post') ORDER BY custname";

            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDOData(string cmp, int custoid, string action, int shipmentitemmstoid, int divgroupoid)
        {
            List<QL_trndoitemmst> tbl = new List<QL_trndoitemmst>();
            sSql = "SELECT * FROM QL_trndoitemmst dom WHERE dom.cmpcode='" + cmp + "' AND custoid=" + custoid + " and dom.divgroupoid='"+divgroupoid+"'";
            if (action == "Update Data")
            {
                sSql += " AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + shipmentitemmstoid + ")";
            } else
            {
                sSql += " AND doitemmststatus='Post'";
            }
            sSql += " AND dom.divgroupoid=" + divgroupoid + " ORDER BY doitemmstoid";

            tbl = db.Database.SqlQuery<QL_trndoitemmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int custoid, int doitemmstoid, int shipmentitemwhoid, int shipmentitemmstoid, string action)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<shipmentitemdtl> tbl = new List<shipmentitemdtl>();
            sSql = "SELECT som.soitemno, dom.doitemno, CONVERT(VARCHAR(10), dom.doitemdate, 101) AS doitemdate, dod.doitemmstoid, dod.doitemdtloid, dod.doitemdtlseq, dod.itemoid, m.itemcode, m.itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND crd.refname='FINISH GOOD' AND  crd.refoid=dod.itemoid AND crd.mtrwhoid=" + shipmentitemwhoid + "), 0.0) AS stockqty, (dod.doitemqty - ISNULL((SELECT SUM(sd.shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND sm.shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dod.doitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0)) AS doitemqty, 0.0 AS shipmentitemqty, dod.doitemunitoid shipmentitemunitoid, g.gendesc AS shipmentitemunit, dod.doitemdtlnote AS shipmentitemdtlnote, 1.0 itemlimitqty FROM QL_trndoitemdtl dod INNER JOIN QL_mstitem m ON m.itemoid=dod.itemoid INNER JOIN QL_mstgen g ON g.genoid=dod.doitemunitoid INNER JOIN QL_trnsoitemdtl sod ON sod.soitemdtloid=dod.soitemdtloid AND sod.soitemmstoid=dod.soitemmstoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_trndoitemmst dom ON dom.cmpcode=dod.cmpcode AND dom.doitemmstoid=dod.doitemmstoid WHERE dod.cmpcode='" + cmp + "' AND dod.doitemmstoid=" + doitemmstoid + "";
            if (action == "New Data") sSql += " AND dod.doitemdtlstatus=''";
            sSql += " ORDER BY doitemdtlseq";

            tbl = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();
            if (tbl.Count > 0)
            {
                for(var i = 0; i < tbl.Count(); i++)
                {
                    tbl[i].shipmentitemqty = tbl[i].doitemqty;
                }
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentitemdtl> dtDtl)
        {
            Session["QL_trnshipmentitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnshipmentitemdtl"] == null)
            {
                Session["QL_trnshipmentitemdtl"] = new List<shipmentitemdtl>();
            }

            List<shipmentitemdtl> dataDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnshipmentitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();
            ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnshipmentitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid='" + tbl.shipmentitemmstoid + "'").FirstOrDefault();
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: shipmentitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dom", divgroupoid, "divgroupoid");
            sSql = "SELECT som.cmpcode, (select gendesc from ql_mstgen where genoid=som.divgroupoid) divgroup, som.shipmentitemmstoid, som.shipmentitemno, som.shipmentitemdate, c.custname, som.shipmentitemmststatus, som.shipmentitemmstnote, div.divname FROM QL_TRNshipmentitemMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND shipmentitemmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND shipmentitemmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND shipmentitemmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND shipmentitemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND shipmentitemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND shipmentitemmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY som.shipmentitemdate DESC, som.shipmentitemmstoid DESC";

            List<shipmentitemmst> dt = db.Database.SqlQuery<shipmentitemmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnshipmentitemmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: shipmentitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            var cmp = CompnyCode;
            QL_trnshipmentitemmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnshipmentitemmst();
                tbl.shipmentitemmstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                tbl.shipmentitemdate = ClassFunction.GetServerTime();
                tbl.shipmentitemetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.shipmentitemeta = ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.shipmentitemmststatus = "In Process";
                
                Session["QL_trnshipmentitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnshipmentitemmst.Find(cmp, id);

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSql = "SELECT sd.shipmentitemdtlseq, sd.doitemmstoid, dom.doitemno, CONVERT(VARCHAR(10), dom.doitemdate, 101) AS doitemdate, sd.shipmentitemwhoid, g1.gendesc AS shipmentitemwh, sd.doitemdtloid, sd.itemoid, m.itemcode, m.itemlongdesc, 1.0 itemlimitqty, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=sd.cmpcode AND crd.refname='FINISH GOOD'  AND crd.refoid=sd.itemoid AND crd.mtrwhoid=sd.shipmentitemwhoid ), 0.0) AS stockqty, (dod.doitemqty - ISNULL((SELECT SUM(sdx.shipmentitemqty) FROM QL_trnshipmentitemdtl sdx INNER JOIN QL_trnshipmentitemmst smx ON smx.cmpcode=sdx.cmpcode AND smx.shipmentitemmstoid=sdx.shipmentitemmstoid WHERE sdx.cmpcode=sd.cmpcode AND smx.shipmentitemmststatus<>'Rejected' AND sdx.doitemdtloid=sd.doitemdtloid AND sdx.shipmentitemmstoid<>sd.shipmentitemmstoid), 0.0)) AS doitemqty, sd.shipmentitemqty, sd.shipmentitemunitoid, g2.gendesc AS shipmentitemunit, sd.shipmentitemdtlnote FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trndoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.doitemmstoid=sd.doitemmstoid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid INNER JOIN QL_mstitem m ON m.itemoid=sd.itemoid INNER JOIN QL_mstgen g1 ON g1.genoid=sd.shipmentitemwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=sd.shipmentitemunitoid WHERE sd.shipmentitemmstoid=" + id + " ORDER BY sd.shipmentitemdtlseq";
                Session["QL_trnshipmentitemdtl"] = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            if (tbl.shipmentitemno == null)
                tbl.shipmentitemno = "";
            
            List<shipmentitemdtl> dtDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].shipmentitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY must be more than 0!");
                        }
                        else
                        {
                            if (dtDtl[i].shipmentitemqty > dtDtl[i].doitemqty)
                            {
                                ModelState.AddModelError("", "QTY must be less than DO QTY!");
                            }
                            else
                            {
 
                            }
                        }

                        sSql = "SELECT (doitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dod.doitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0)) AS doitemqty FROM QL_trndoitemdtl dod WHERE dod.cmpcode='" + tbl.cmpcode + "' AND doitemdtloid=" + dtDtl[i].doitemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].doitemqty)
                            dtDtl[i].doitemqty = dQty;
                        if (dQty < dtDtl[i].shipmentitemqty)
                            ModelState.AddModelError("", "Some DO Qty has been updated by another user. Please check that every Qty must be less than DO Qty!");
                        if (!ClassFunction.IsStockAvailable(tbl.cmpcode, sPeriod, dtDtl[i].itemoid, dtDtl[i].shipmentitemwhoid, dtDtl[i].shipmentitemqty, "FINISH GOOD"))
                            if (tbl.shipmentitemmststatus == "In Approval")
                                ModelState.AddModelError("", "Every Qty must be less than Stock Qty before sending this data for approval!");
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.shipmentitemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.shipmentitemmststatus == "Revised")
                tbl.shipmentitemmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.shipmentitemmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentitemdtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.shipmentitemetd = Convert.ToDateTime("1/1/1900 00:00:00");
                tbl.shipmentitemeta = Convert.ToDateTime("1/1/1900 00:00:00");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnshipmentitemmst.Find(tbl.cmpcode, tbl.shipmentitemmstoid) != null)
                                tbl.shipmentitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnshipmentitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.shipmentitemmstoid + " WHERE tablename='QL_trnshipmentitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == tbl.shipmentitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnshipmentitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.shipmentitemdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.shipmentitemmstoid = tbl.shipmentitemmstoid;
                            tbldtl.shipmentitemdtlseq = i + 1;
                            tbldtl.doitemmstoid = dtDtl[i].doitemmstoid;
                            tbldtl.doitemdtloid = dtDtl[i].doitemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.shipmentitemqty = dtDtl[i].shipmentitemqty;
                            tbldtl.shipmentitemunitoid = dtDtl[i].shipmentitemunitoid;
                            tbldtl.shipmentitemwhoid = dtDtl[i].shipmentitemwhoid;
                            tbldtl.shipmentitemdtlstatus = "";
                            tbldtl.shipmentitemdtlnote = (dtDtl[i].shipmentitemdtlnote == null ? "" : dtDtl[i].shipmentitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentitemdtlres3 = "";
                            tbldtl.shipmentitemvalueidr = 0;
                            tbldtl.shipmentitemvalueusd = 0;

                            db.QL_trnshipmentitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid=" + dtDtl[i].doitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmststatus='Post' AND doitemmstoid=" + dtDtl[i].doitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnshipmentitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.shipmentitemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SHP-FG" + tbl.shipmentitemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnshipmentitemmst";
                                tblApp.oid = tbl.shipmentitemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.shipmentitemmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl = db.QL_trnshipmentitemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Post' WHERE cmpcode='" + cmp + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnshipmentitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnshipmentitemmst.Find(cmp, id);
            if (tbl == null)
                return null;
            var sReportName = "";
            if (printtype == "A4")
                sReportName = "rptShipmentFG_Trn.rpt";
            else
                sReportName = "rptShipmentFG_TrnA5.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            sSql = "SELECT sm.shipmentitemmstoid AS [MstOid], CONVERT(VARCHAR(10), sm.shipmentitemmstoid) AS [Draft No.], sm.shipmentitemno AS [Shipment No.], sm.shipmentitemdate AS [Shipment Date], sm.shipmentitemmstnote AS [Header Note], shipmentitemcontno [Container No.],  ISNULL((SELECT vhcdesc FROM QL_mstvehicle v WHERE v.vhcoid=sm.vhcoid),'') AS [Vehicle],  ISNULL((SELECT drivname FROM QL_mstdriver d WHERE d.drivoid=sm.drivoid),'') AS [Driver], shipmentitemnopol [Vehicle Police No.], sm.shipmentitemmststatus AS [Status], [Customer Type], [Customer], [Customer Address], [Customer City],[Customer Province], [Customer Country], [Customer Phone1], [Customer Phone2], [Customer Phone3], [Customer Fax], [Customer Website], [Customer CP1], [Customer CP2], [Customer CPPhone1], [Customer CPPhone2], [Customer CPEmail1], [Customer CPEmail2], [Customer Postal Code], ISNULL(custdtlcode,'') [Cust Ship To. Code], ISNULL(custdtlname,'') [Cust Ship To. Name], ISNULL(custdtladdr,'') [Cust Ship To. Address], ISNULL([City],'') [Cust Ship To. City], ISNULL([Province],'') [Cust Ship To. Province], ISNULL([Country],'') [Cust Ship To. Country], ISNULL(custdtlnote,'') [Cust Ship To. Note], ISNULL(custdtlpostcode,'')  AS [Cust Ship To. Post Code], sd.shipmentitemdtloid AS [DtlOid], sd.shipmentitemdtlseq AS [No.], m.itemoid AS [MatOid], m.itemcode AS [Code], m.itemlongdesc AS [Material], sd.shipmentitemqty AS [Qty], g2.gendesc AS [Unit], g3.gendesc AS [Warehouse], sd.shipmentitemdtlnote AS [Note], sm.cmpcode , dom.doitemno AS [DO no.], ISNULL(shipmentitemnopajak,'') [SJ Pajak], (SELECT divname FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Name],(SELECT divaddress FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Address],(SELECT g1.gendesc FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid=divcityoid AND gengroup='City' WHERE di.cmpcode=sm.cmpcode ) AS [BU City],(SELECT g2.gendesc FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid=divcityoid AND gengroup='City' INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) WHERE di.cmpcode=sm.cmpcode ) AS [BU Province],(SELECT g3.gendesc FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid=divcityoid AND gengroup='City' INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2) WHERE di.cmpcode=sm.cmpcode) AS [BU Country],(SELECT ISNULL(divphone, '') FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS[BU Telp. 2],(SELECT ISNULL(divfax1, '') FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Fax 1],(SELECT ISNULL(divfax2, '')  FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Fax 2],(SELECT ISNULL(divemail, '') FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Email],(SELECT ISNULL(divpostcode, '') FROM QL_mstdivision di WHERE di.cmpcode=sm.cmpcode) AS [BU Post Code], ISNULL(shipmentitemsealno, '') [Seal No.] FROM QL_trnshipmentitemmst sm INNER JOIN (SELECT custoid,custtype [Customer Type],custname [Customer],custaddr [Customer Address],custcityoid,g.gendesc [Customer City] ,g2.gendesc [Customer Province] ,g3.gendesc [Customer Country],custphone1 [Customer Phone1],custphone2 [Customer Phone2],custphone3 [Customer Phone3],custfax1 [Customer Fax],custwebsite [Customer Website],custcp1name [Customer CP1],custcp2name [Customer CP2],custcp1phone [Customer CPPhone1],custcp2phone [Customer CPPhone2],custcp1email [Customer CPEmail1],custcp2email [Customer CPEmail2], ISNULL(custpostcode,'') [Customer Postal Code] FROM QL_mstcust c INNER JOIN QL_mstgen g ON g.genoid=custcityoid AND g.gengroup='City' INNER JOIN QL_mstgen g2 ON g2.cmpcode=g.cmpcode AND g2.genoid=CONVERT(INT, g.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g.cmpcode AND g3.genoid=CONVERT(INT, g.genother2))AS TblCust ON TblCust.custoid=sm.custoid  INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentitemmstoid=sm.shipmentitemmstoid INNER JOIN QL_trndoitemmst dom ON sd.cmpcode=dom.cmpcode AND dom.doitemmstoid=sd.doitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=sd.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=sd.shipmentitemunitoid INNER JOIN QL_mstgen g3 ON g3.genoid=sd.shipmentitemwhoid LEFT JOIN (SELECT custoid, custdtloid, custdtlcode, custdtlname, custdtladdr, custdtlcityoid, custdtlnote, custdtlpostcode, g4.gendesc [City] ,g5.gendesc [Province],g6.gendesc [Country] FROM QL_mstcustdtl cdtl INNER JOIN QL_mstgen g4 ON g4.genoid=custdtlcityoid AND g4.gengroup='City' INNER JOIN QL_mstgen g5 ON g5.cmpcode=g4.cmpcode AND g5.genoid=CONVERT(INT, g4.genother1) INNER JOIN QL_mstgen g6 ON g6.cmpcode=g4.cmpcode AND g6.genoid=CONVERT(INT, g4.genother2))tblCustDtl ON sm.custoid=tblCustDtl.custoid AND dom.custdtloid=tblCustDtl.custdtloid WHERE sm.cmpcode='" + cmp + "' AND sm.shipmentitemmstoid="+ id +" ORDER BY sm.shipmentitemmstoid, sd.shipmentitemdtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptShipmentFG");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("Header", "SHIPMENT FINISH GOOD PRINT OUT");
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ShipmentFinishGoodReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}