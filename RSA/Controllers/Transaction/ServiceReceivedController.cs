﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ServiceReceivedController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnmrservicemst
        {
            public string cmpcode { get; set; }
            public int mrservicemstoid { get; set; }
            public int mrservicewhoid { get; set; }
            public string mrserviceno { get; set; }
            public string poserviceno { get; set; }
            public DateTime mrservicedate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string mrservicemststatus { get; set; }
            public string mrservicemstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnmrservicedtl
        {
            public int mrservicedtlseq { get; set; }
            public int poservicedtloid { get; set; }
            public string poserviceno { get; set; }
            public int serviceoid { get; set; }
            public string servicecode { get; set; }
            public string servicelongdesc { get; set; }
            public decimal servicelimitqty { get; set; }
            public decimal poservicedtlqty { get; set; }
            public decimal mrserviceqty { get; set; }
            public int mrserviceunitoid { get; set; }
            public string mrserviceunit { get; set; }
            public string mrservicedtlnote { get; set; }
            public decimal poqty { get; set; }
            public decimal mrservicevalue { get; set; }
            public decimal mrserviceamt { get; set; }
            public string location { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnmrservicemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0 AND gencode='GUM'";
            var mrservicewhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.mrservicewhoid = mrservicewhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0 AND gencode='GUM'";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int poservicemstoid, int mrservicemstoid)
        {
            var result = "";
            JsonResult js = null;
            List<trnmrservicedtl> tbl = new List<trnmrservicedtl>();
            try
            {
                sSql = "SELECT 0 AS mrservicedtlseq, regd.poservicedtloid, poserviceno, regd.serviceoid AS serviceoid, servicecode, servicelongdesc, 0.0 servicelimitqty, (cast(poserviceqty as decimal(18,2)) - ISNULL((SELECT SUM(mrserviceqty) FROM QL_trnmrservicedtl mrd INNER JOIN QL_trnmrservicemst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrservicemstoid = mrd.mrservicemstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.poservicedtloid = regd.poservicedtloid AND mrm2.poservicemstoid = regm.poservicemstoid AND mrd.mrservicemstoid <> 1), 0.0) - ISNULL((SELECT 0.0 /*SUM(retd.retserviceqty)*/ FROM QL_trnretservicedtl retd WHERE retd.cmpcode = regd.cmpcode AND retd.poservicedtloid = regd.poservicedtloid), 0)) AS poservicedtlqty, 0.0 AS mrserviceqty, poserviceunitoid AS mrserviceunitoid, gendesc AS mrserviceunit, '' AS mrservicedtlnote, (CASE poservicedtldiscvalue WHEN 0 THEN poserviceprice ELSE(poservicedtlnetto / poserviceqty) END) AS mrservicevalue, ''[location], cast(poserviceqty as decimal(18,2)) AS poqty, poservicedtlseq, regm.poservicemstoid  " +
                "FROM QL_trnposervicedtl regd " +
                "INNER JOIN QL_trnposervicemst regm ON regm.cmpcode = regd.cmpcode AND regd.poservicemstoid = regm.poservicemstoid " +
                "INNER JOIN QL_mstservice m ON m.serviceoid = regd.serviceoid " +
                "INNER JOIN QL_mstgen g ON genoid = regd.poserviceunitoid " +
                "WHERE regd.cmpcode = '" + CompnyCode + "' AND regd.poservicemstoid = " + poservicemstoid + " AND poservicedtlstatus = '' AND ISNULL(regm.poservicemstres2, '')<> 'Complete' ORDER BY poservicedtlseq";

                tbl = db.Database.SqlQuery<trnmrservicedtl>(sSql).ToList();
                for (int i = 0; i < tbl.Count; i++)
                {
                    tbl[i].mrservicedtlseq = i + 1;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmrservicedtl> dtDtl)
        {
            Session["QL_trnmrservicedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmrservicedtl"] == null)
            {
                Session["QL_trnmrservicedtl"] = new List<trnmrservicedtl>();
            }

            List<trnmrservicedtl> dataDtl = (List<trnmrservicedtl>)Session["QL_trnmrservicedtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmrservicemst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poserviceno = db.Database.SqlQuery<string>("SELECT poserviceno FROM QL_trnposervicemst WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp, int divgroupoid)
        {
            var result = "";
            JsonResult js = null;
            List<mstsupp> tbl = new List<mstsupp>();
            try
            {
                sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + cmp + "' and divgroupoid='" + divgroupoid + "' AND suppoid IN (SELECT suppoid FROM QL_trnposervicemst WHERE cmpcode='" + cmp + "' AND poservicemststatus='Approved' AND ISNULL(poservicemstres1, '')<>'Closed'  AND ISNULL(poservicemstres2, '')<>'Closed') ORDER BY suppcode, suppname";
                tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class pomst
        {
            public int poservicemstoid { get; set; }
            public string poserviceno { get; set; }
            public DateTime poservicedate { get; set; }
            public string poservicedocrefno { get; set; }
            public DateTime poservicedocrefdate { get; set; }
            public string poservicemstnote { get; set; }
            public string poservicetype { get; set; }
            public int curroid { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid)
        {
            List<pomst> tbl = new List<pomst>();
            sSql = "SELECT poservicemstoid, poserviceno, poservicedate, '' [poservicedocrefno], poservicemstnote, curroid FROM QL_trnposervicemst WHERE cmpcode='" + cmp + "' AND suppoid= " + suppoid + " AND poservicemststatus='Approved' AND ISNULL(poservicemstres1, '')<>'Closed' AND ISNULL(poservicemstres2, '')<>'Closed' ORDER BY poservicemstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = " SELECT mrm.cmpcode, mrservicemstoid, mrserviceno, mrservicedate, suppname, poserviceno, mrservicemststatus, ISNULL(mrservicemstnote, '') [mrservicemstnote], divname FROM QL_trnmrservicemst mrm INNER JOIN QL_trnposervicemst reg ON reg.cmpcode = mrm.cmpcode AND reg.poservicemstoid = mrm.poservicemstoid INNER JOIN QL_mstsupp s ON s.suppoid = mrm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode = mrm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "mrm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND mrservicedate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND mrservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND mrservicedate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND mrservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND mrservicedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrservicedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND mrservicemststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND mrservicemststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND mrm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnmrservicemst> dt = db.Database.SqlQuery<trnmrservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmrservicemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnmrservicemst();
                tbl.cmpcode = CompnyCode;
                tbl.mrservicemstoid = ClassFunction.GenerateID("QL_trnmrservicemst");
                tbl.mrservicedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrservicemststatus = "In Process";
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());

                Session["QL_trnmrservicedtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmrservicemst.Find(cmp, id);
                sSql = "SELECT mrservicedtlseq, mrd.poservicedtloid, poserviceno, mrd.serviceoid, servicecode, servicelongdesc, 0.0 servicelimitqty, (cast(poserviceqty as decimal(18,2)) - ISNULL((SELECT SUM(x.mrserviceqty) FROM QL_trnmrservicedtl x INNER JOIN QL_trnmrservicemst y ON y.cmpcode=x.cmpcode AND y.mrservicemstoid=x.mrservicemstoid WHERE x.cmpcode=mrd.cmpcode AND x.poservicedtloid=mrd.poservicedtloid AND pod.poservicemstoid=y.poservicemstoid AND x.mrservicemstoid<>mrd.mrservicemstoid), 0.0) - ISNULL((SELECT 0.0 /*SUM(retd.retserviceqty)*/ FROM QL_trnretservicedtl retd WHERE retd.cmpcode = pod.cmpcode AND retd.poservicedtloid = pod.poservicedtloid), 0.0)) AS poservicedtlqty, cast(mrserviceqty as decimal(18,2)) AS mrqty, cast(mrserviceqty as decimal(18,2)) mrserviceqty, 0.0 mrservicebonusqty, mrserviceunitoid, gendesc AS mrserviceunit, ISNULL(mrservicedtlnote, '') [mrservicedtlnote], mrservicevalue, (mrservicevalue * mrserviceqty) AS mrserviceamt, '' AS location, cast(poserviceqty as decimal(18,2)) AS poqty " +
                    "FROM QL_trnmrservicedtl mrd " +
                    "INNER JOIN QL_mstservice m ON m.serviceoid = mrd.serviceoid " +
                    "INNER JOIN QL_mstgen g ON genoid = mrserviceunitoid " +
                    "INNER JOIN QL_trnposervicedtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poservicedtloid = mrd.poservicedtloid AND poservicemstoid IN(SELECT poservicemstoid FROM QL_trnmrservicemst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mrservicemstoid = mrd.mrservicemstoid) " +
                    "INNER JOIN QL_trnposervicemst pom ON pom.cmpcode = pod.cmpcode AND pom.poservicemstoid = pod.poservicemstoid " +
                    "WHERE mrservicemstoid = " + id + "  AND mrd.cmpcode = '" + cmp + "' ORDER BY mrservicedtlseq";
                Session["QL_trnmrservicedtl"] = db.Database.SqlQuery<trnmrservicedtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        public class import
        {
            public int curroid { get; set; }
            public decimal importvalue { get; set; }
        }

        public class dtHdr
        {
            public int mrservicemstoid { get; set; }
            public DateTime mrservicedate { get; set; }
            public string mrserviceno { get; set; }
            public int mrservicewhoid { get; set; }
        }

        public class dtDtl
        {
            public int mrservicemstoid { get; set; }
            public int mrservicedtloid { get; set; }
            public int mrservicedtlseq { get; set; }
            public int serviceoid { get; set; }
            public decimal mrserviceqty { get; set; }
            public decimal mrservicevalue { get; set; }
            public decimal mrserviceamt { get; set; }
        }


        private void GetLastData(string cmp, int poservicemstoid, int mrservicemstoid, out List<dtHdr> dtLastHdrData, out List<dtDtl> dtLastDtlData)
        {
            dtLastHdrData = new List<dtHdr>();
            sSql = "SELECT mrservicemstoid, mrservicedate, mrserviceno, mrservicewhoid FROM QL_trnmrservicemst WHERE cmpcode='" + cmp + "' AND mrservicemststatus='Post' AND mrserviceno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND poservicemstoid=" + poservicemstoid;
            sSql += " AND mrservicemstoid<>" + mrservicemstoid;
            sSql += " ORDER BY mrservicemstoid ";
            dtLastHdrData = db.Database.SqlQuery<dtHdr>(sSql).ToList();

            dtLastDtlData = new List<dtDtl>();
            sSql = "SELECT mrm.mrservicemstoid, mrservicedtloid, mrservicedtlseq, serviceoid, mrserviceqty, mrservicevalue, (mrserviceqty * mrservicevalue) AS mrserviceamt FROM QL_trnmrservicemst mrm INNER JOIN QL_trnmrservicedtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrservicemstoid=mrm.mrservicemstoid WHERE mrm.cmpcode='" + cmp + "' AND mrservicemststatus='Post' AND mrserviceno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND poservicemstoid=" + poservicemstoid;
            sSql += " AND mrm.mrservicemstoid<>" + mrservicemstoid;
            sSql += " ORDER BY mrm.mrservicemstoid, mrservicedtlseq ";
            dtLastDtlData = db.Database.SqlQuery<dtDtl>(sSql).ToList();
        }

        private string generateNo(string cmp)
        {
            var sNo = "SVCR-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrserviceno, " + DefaultFormatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrservicemst WHERE cmpcode='" + cmp + "' AND mrserviceno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrservicemst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.mrserviceno == null)
                tbl.mrserviceno = "";

            List<trnmrservicedtl> dtDtl = (List<trnmrservicedtl>)Session["QL_trnmrservicedtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mrserviceqty <= 0)
                        {
                            ModelState.AddModelError("", "MR QTY for Material " + dtDtl[i].servicelongdesc + " must be more than 0");
                        }
                        sSql = "SELECT (cast(poserviceqty as decimal(18,2)) - ISNULL((SELECT SUM(mrserviceqty) FROM QL_trnmrservicedtl mrd INNER JOIN QL_trnmrservicemst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrservicemstoid=mrd.mrservicemstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.poservicedtloid=regd.poservicedtloid AND mrm2.poservicemstoid=regd.poservicemstoid  AND mrd.mrservicemstoid<>" + tbl.mrservicemstoid + "), 0.0) - ISNULL((SELECT /*SUM(retd.retserviceqty)*/ 0 FROM QL_trnretservicedtl retd WHERE retd.cmpcode=regd.cmpcode AND retd.poservicedtloid=regd.poservicedtloid), 0)) AS poqty  FROM QL_trnposervicedtl regd WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.poservicedtloid=" + dtDtl[i].poservicedtloid + " AND regd.poservicemstoid=" + tbl.poservicemstoid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].poqty)
                            dtDtl[i].poqty = dQty;
                        if (dQty < dtDtl[i].mrserviceqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PO Qty!");

                        dtDtl[i].mrserviceamt = dtDtl[i].mrserviceqty * dtDtl[i].mrservicevalue;
                    }
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmrservicemst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrservicedtl");
                var dtloid2 = ClassFunction.GenerateID("QL_trnmrservicedtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                decimal dImportCost = 0;
                decimal dSumMR = 0;
                decimal dSumMR_IDR = 0;
                decimal dSumMR_USD = 0;
                //List<dtHdr> dtLastHdrData = null;
                //List<dtDtl> dtLastDtlData = null;
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

                if (tbl.mrservicemststatus.ToUpper() == "POST")
                {                    
                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                    tbl.mrserviceno = generateNo(tbl.cmpcode);
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmrservicemst.Find(tbl.cmpcode, tbl.mrservicemstoid) != null)
                                tbl.mrservicemstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mrservicedate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnmrservicemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.mrservicemstoid + " WHERE tablename='QL_trnmrservicemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnposervicedtl SET poservicedtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + " AND poservicedtloid IN (SELECT poservicedtloid FROM QL_trnmrservicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid=" + tbl.mrservicemstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnposervicemst SET poservicemststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmrservicedtl.Where(a => a.mrservicemstoid == tbl.mrservicemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmrservicedtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmrservicedtl tbldtl;
                        dSumMR += dtDtl.Sum(x => x.mrserviceamt);
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dMRVal = 0;
                            if (dSumMR > 0)
                            {
                                dMRVal = (dtDtl[i].mrservicevalue + ((dtDtl[i].mrservicevalue / dSumMR) * dImportCost));
                            }
                            if (dtDtl[i].mrservicedtlnote == null)
                                dtDtl[i].mrservicedtlnote = "";
                            dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * (dtDtl[i].mrserviceqty));
                            dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mrserviceqty));

                            tbldtl = new QL_trnmrservicedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.mrservicedtloid = dtloid++;
                            tbldtl.mrservicemstoid = tbl.mrservicemstoid;
                            tbldtl.mrservicedtlseq = i + 1;
                            tbldtl.poservicedtloid = dtDtl[i].poservicedtloid;
                            tbldtl.serviceoid = dtDtl[i].serviceoid;
                            tbldtl.mrserviceqty = int.Parse(dtDtl[i].mrserviceqty.ToString());
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.mrserviceunitoid = dtDtl[i].mrserviceunitoid;
                            tbldtl.mrservicevalueidr = dMRVal * cRate.GetRateMonthlyIDRValue; //* cRate.GetRateMonthlyIDRValue;
                            tbldtl.mrservicevalueusd = dMRVal * cRate.GetRateMonthlyUSDValue; //* cRate.GetRateMonthlyUSDValue;
                            tbldtl.mrservicedtlstatus = "";
                            tbldtl.mrservicedtlnote = dtDtl[i].mrservicedtlnote;
                            tbldtl.mrservicevalue = dMRVal;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnmrservicedtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].mrserviceqty >= dtDtl[i].poqty)
                            {                                
                                sSql = "UPDATE QL_trnposervicedtl SET poservicedtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicedtloid=" + dtDtl[i].poservicedtloid + " AND poservicemstoid=" + tbl.poservicemstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnposervicemst SET poservicemststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + " AND (SELECT COUNT(*) FROM QL_trnposervicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + " AND poservicedtloid<>" + dtDtl[i].poservicedtloid + " AND poservicedtlstatus='')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmrservicedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();                        

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.mrservicemstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.mrserviceno = "";
            tbl.mrservicemststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrservicemst tbl = db.QL_trnmrservicemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnposervicedtl SET poservicedtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + " AND poservicedtloid IN (SELECT poservicedtloid FROM QL_trnmrservicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid=" + tbl.mrservicemstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnposervicemst SET poservicemststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmrservicedtl.Where(a => a.mrservicemstoid == id && a.cmpcode == cmp);
                        db.QL_trnmrservicedtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmrservicemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmrservicemst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMR.rpt"));
            //sSql = "SELECT mrm.mrservicemstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrservicemstoid)) AS [Draft No.], mrm.mrserviceno AS [MR No.], mrm.mrservicedate AS [MR Date], mrm.mrservicemststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrservicemstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrservicemststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrservicemststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrservicedtlseq AS [No.] , m.servicecode AS [Code], m.servicelongdesc AS [Description], mrd.mrserviceqty AS [Qty], mrservicebonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrservicedtlnote AS [Note], pom.poserviceno AS [PO No.], pom.poservicedate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg PostDate], registerdocrefdate AS [Tgl. SJ], registerdocrefno [No SJ],registernopol [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] " +
            //    ", ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=mrm.cmpcode AND loc.mtrwhoid=mrm.mrservicewhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)='Raw' AND loc.matoid=mrd.serviceoid),'') AS [Detail Location] " +
            //    " FROM QL_trnmrservicemst mrm INNER JOIN QL_trnmrservicedtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrservicemstoid=mrm.mrservicemstoid INNER JOIN QL_mstservice m ON  m.serviceoid=mrd.serviceoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrserviceunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrservicewhoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnposervicemst pom ON pom.cmpcode=mrm.cmpcode AND  pom.poservicemstoid=regd.porefmstoid INNER JOIN QL_trnposervicedtl pod ON pod.cmpcode=mrm.cmpcode AND pod.poservicedtloid=regd.porefdtloid INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='GEN' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrservicemstoid IN (" + id + ") ORDER BY mrm.mrservicemstoid, mrservicedtlseq ";
            sSql = "SELECT mrm.mrservicemstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrservicemstoid)) AS [Draft No.], mrm.mrserviceno AS [MR No.], mrm.mrservicedate AS [MR Date], mrm.mrservicemststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrservicemstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrservicemststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrservicemststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrservicedtlseq AS [No.] , m.servicecode AS [Code], m.servicelongdesc AS [Description], mrd.mrserviceqty AS [Qty], mrservicebonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrservicedtlnote AS [Note], regm.poserviceno AS [PO No.], regm.poservicedate AS [PO Date], regm.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE regm.approvaluser=p2.profoid) AS [PO UserApp] ,regm.poserviceno [Reg No.],regm.poservicedate [Reg Date], regm.updtime AS [Reg PostDate], CAST('01/01/1900' AS date) AS [Tgl. SJ], '' [No SJ], '' [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost], '' AS [Detail Location] " +
                " FROM QL_trnmrservicemst mrm  " +
                "INNER JOIN QL_trnmrservicedtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrservicemstoid=mrm.mrservicemstoid  " +
                "INNER JOIN QL_mstservice m ON  m.serviceoid=mrd.serviceoid  " +
                "INNER JOIN QL_mstgen g ON g.genoid=mrd.mrserviceunitoid  " +
                "INNER JOIN QL_mstgen g1 ON g1.genoid=mrservicewhoid  " +
                "INNER JOIN QL_trnposervicedtl regd ON regd.cmpcode=mrm.cmpcode AND regd.poservicemstoid=mrm.poservicemstoid AND regd.poservicedtloid=mrd.poservicedtloid " +
                "INNER JOIN QL_trnposervicemst regm ON mrm.cmpcode = regm.cmpcode AND regm.poservicemstoid = mrm.poservicemstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid  " +
                "WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrservicemstoid IN (" + id + ") ORDER BY mrm.mrservicemstoid, mrservicedtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintmrservice");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("Header", "GENERAL MATERIAL RECEIVED PRINT OUT");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "GeneralMaterialReceivedPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
