﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;
using RSA.Controllers;

namespace RSA.Controllers
{
    public class APServiceController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class apservicemst
        {
            public string cmpcode { get; set; }
            public int apservicemstoid { get; set; }
            public string apserviceno { get; set; }
            public DateTime apservicedate { get; set; }
            public DateTime apservicedatetakegiro { get; set; }
            public DateTime duedate { get; set; }
            public string suppname { get; set; }
            public string apservicemststatus { get; set; }
            public string apservicemstnote { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apservicegrandtotal { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apservicegrandtotalsupp { get; set; }
            public string divname { get; set; }
        }

        public class apservicedtl
        {
            public int apservicedtlseq { get; set; }
            public int poservicemstoid { get; set; }
            public string poserviceno { get; set; }
            public int poservicedtloid { get; set; }
            public int mrservicemstoid { get; set; }
            public string mrserviceno { get; set; }
            public string porawno { get; set; }
            public int mrservicedtloid { get; set; }
            public int serviceoid { get; set; }
            public string servicecode { get; set; }
            public string servicelongdesc { get; set; }
            public decimal apserviceqty { get; set; }
            public int apserviceunitoid { get; set; }
            public string apserviceunit { get; set; }
            public decimal apserviceprice { get; set; }
            public decimal apservicedtlamt { get; set; }
            public string apservicedtldisctype { get; set; }
            public decimal apservicedtldiscvalue { get; set; }
            public decimal apservicedtldiscamt { get; set; }
            public decimal apservicedtlnetto { get; set; }
            public decimal apservicedtltaxamt { get; set; }
            public string apservicedtlnote { get; set; }
            public string apservicedtlres1 { get; set; }
            public string apservicedtlres2 { get; set; }
        }

        public class poservicemst
        {
            public int poservicemstoid { get; set; }
            public string poserviceno { get; set; }
            public DateTime poservicedate { get; set; }
            public string poservicedocrefno { get; set; }
            public DateTime poservicedocrefdate { get; set; }
            public string poservicemstnote { get; set; }
            public string poserviceflag { get; set; }
        }
        private void InitDDL(QL_trnapservicemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var apservicepaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.apservicepaytypeoid);
            ViewBag.apservicepaytypeoid = apservicepaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapservicemst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapservicemst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp, int apservicemstoid, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT suppoid, suppcode, suppname, suppaddr, supppaymentoid FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND divgroupoid='"+divgroupoid+"' AND suppoid IN (SELECT suppoid FROM QL_trnposervicemst WHERE cmpcode='" + cmp + "' /*AND poservicemststatus Like '%os%' AND poservicemstoid NOT IN (SELECT poservicemstoid FROM QL_trnapservicedtl apd INNER JOIN QL_trnapservicemst apm ON apm.cmpcode=apd.cmpcode AND apm.apservicemstoid=apd.apservicemstoid WHERE apd.cmpcode='" + cmp + "' AND apservicemststatus<>'Rejected' AND apm.apservicemstoid <> " + apservicemstoid + ")*/ AND poservicemstoid IN (SELECT mrm.poservicemstoid FROM QL_trnmrservicemst mrm WHERE mrm.cmpcode='" + cmp + "' AND ISNULL(mrservicemstres1, '')='' AND mrservicemststatus='Post')) ORDER BY suppcode";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetposerviceData(string cmp, int suppoid, int apservicemstoid, int curroid)
        {
            List<poservicemst> tbl = new List<poservicemst>();

            sSql = "SELECT DISTINCT regm.poservicemstoid, poserviceno, poservicedate, poservicemstnote FROM QL_trnposervicemst regm INNER JOIN QL_trnposervicedtl regd ON regd.cmpcode=regm.cmpcode AND regd.poservicemstoid=regm.poservicemstoid WHERE regm.cmpcode='" + cmp + "' AND regm.suppoid=" + suppoid + "  /*AND regm.poservicemstoid NOT IN (SELECT poservicemstoid FROM QL_trnapservicedtl apd INNER JOIN QL_trnapservicemst apm ON apm.cmpcode=apd.cmpcode AND apm.apservicemstoid=apd.apservicemstoid WHERE apd.cmpcode='" + cmp + "' AND apservicemststatus<>'Rejected' AND apm.apservicemstoid <> " + apservicemstoid + ")*/ AND regm.poservicemstoid IN (SELECT mrm.poservicemstoid FROM QL_trnmrservicemst mrm WHERE mrm.cmpcode='" + cmp + "' AND ISNULL(mrservicemstres1, '')='' AND mrservicemststatus='Post') AND regm.poservicemstoid NOT IN (SELECT pretm.poservicemstoid FROM QL_trnpretservicemst pretm WHERE pretm.cmpcode='" + cmp + "' AND pretservicemststatus IN ('In Process', 'Revised', 'In Approval')) AND curroid=" + curroid + " ORDER BY regm.poservicemstoid";

            tbl = db.Database.SqlQuery<poservicemst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int poservicemstoid, string apservicedtlres1, string apservicedtlres2)
        {

            List<apservicedtl> tbl = new List<apservicedtl>();
           
            sSql = "SELECT apservicedtlseq, poservicemstoid, poserviceno, poservicedtloid, mrservicemstoid, mrserviceno, mrservicedtloid, serviceoid, servicecode, servicelongdesc, apserviceqty, apserviceunitoid, apserviceunit, apserviceprice,(apserviceqty * apserviceprice) AS apservicedtlamt, apservicedtldisctype, apservicedtldiscvalue, (CASE apservicedtldisctype WHEN 'P' THEN(apserviceqty * apserviceprice) * (apservicedtldiscvalue / 100) ELSE(apservicedtldiscvalue * apserviceqty) END) apservicedtldiscamt, ((apserviceqty * apserviceprice) - (CASE apservicedtldisctype WHEN 'P' THEN(apserviceqty * apserviceprice) * (apservicedtldiscvalue / 100) ELSE(apservicedtldiscvalue * apserviceqty) END)) AS apservicedtlnetto, (CASE poservicetaxtype WHEN 'TAX' THEN(((apserviceqty * apserviceprice) - (CASE apservicedtldisctype WHEN 'P' THEN(apserviceqty * apserviceprice) * (apservicedtldiscvalue / 100) ELSE(apservicedtldiscvalue * apserviceqty) END)) *poservicetaxamt) / 100 ELSE 0 END) apservicedtltaxamt, apservicedtlnote ,apservicedtlres1 ,apservicedtlres2 FROM (SELECT 0 AS apservicedtlseq, pom.poservicemstoid, pom.poserviceno, pod.poservicedtloid, mrm.mrservicemstoid, mrserviceno, mrservicedtloid, mrd.serviceoid, servicecode, servicelongdesc, (cast(mrserviceqty as decimal(18,2)) - ISNULL((SELECT SUM(pretserviceqty) FROM QL_trnpretservicedtl pretd INNER JOIN QL_trnpretservicemst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretservicemstoid = pretd.pretservicemstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mrservicedtloid = mrd.mrservicedtloid AND pretservicemststatus <> 'Rejected'), 0.0)) AS apserviceqty, mrserviceunitoid AS apserviceunitoid, gendesc AS apserviceunit, poserviceprice apserviceprice, poservicedtldisctype apservicedtldisctype, (CASE poservicedtldisctype WHEN 'A' THEN isnull((poservicedtldiscvalue / poserviceqty),0.0) ELSE isnull(poservicedtldiscvalue,0.0) END) AS apservicedtldiscvalue,  poservicetaxtype, isnull(poservicetaxamt,0.0) poservicetaxamt, '' apservicedtlnote, '" + apservicedtlres1 + "' AS apservicedtlres1, '" + apservicedtlres2 + "' AS apservicedtlres2 FROM QL_trnmrservicedtl mrd INNER JOIN QL_trnmrservicemst mrm ON mrm.cmpcode = mrd.cmpcode  AND mrm.mrservicemstoid = mrd.mrservicemstoid INNER JOIN QL_trnposervicemst pom ON pom.cmpcode = mrd.cmpcode AND pom.poservicemstoid = mrm.poservicemstoid INNER JOIN QL_trnposervicedtl pod ON mrd.cmpcode = pod.cmpcode AND pod.poservicedtloid = mrd.poservicedtloid AND pom.poservicemstoid = mrm.poservicemstoid INNER JOIN QL_mstservice m ON m.cmpcode = mrd.cmpcode AND m.serviceoid = mrd.serviceoid INNER JOIN QL_mstgen g ON g.cmpcode = mrd.cmpcode AND genoid = serviceunitoid WHERE pod.cmpcode = '" + cmp + "' AND pom.poservicemstoid = " + poservicemstoid + " AND ISNULL(mrservicemstres1, '')= '' AND mrm.mrservicemststatus IN ('Post') AND mrd.mrservicedtlstatus IN('')) AS tbl";

            tbl = db.Database.SqlQuery<apservicedtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<apservicedtl> dtDtl)
        {
            Session["QL_trnapservicedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnapservicedtl"] == null)
            {
                Session["QL_trnapservicedtl"] = new List<apservicedtl>();
            }

            List<apservicedtl> dataDtl = (List<apservicedtl>)Session["QL_trnapservicedtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnapservicemst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            //ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnapservicemst WHERE cmpcode='" + tbl.cmpcode + "' AND apservicemstoid=" + tbl.apservicemstoid + "").FirstOrDefault();
        }

        // GET: apserviceMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT apservicemstoid, apserviceno, apservicedate, suppname, apservicemststatus, apservicemstnote, divname, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'raw' AS formtype, 'False' AS checkvalue, apservicegrandtotal , apservicedatetakegiro,  CONVERT(DATETIME,DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g1.gendesc AS INT) END),apservicedate), 101) duedate FROM QL_trnapservicemst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=apm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=apservicepaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "apm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "apm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND apservicedate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND apservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND apservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND apservicedate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND apservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND apservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND apservicedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND apservicedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND apservicemststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else

            {
                sSql += " AND apservicemststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND apm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND apm.createuser='" + Session["UserID"].ToString() + "'";
            sSql += "AND apservicemstoid>0 ORDER BY CONVERT(DATETIME, apservicedate) DESC, apservicemstoid DESC ";

            List<apservicemst> dt = db.Database.SqlQuery<apservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        [HttpPost]
        public ActionResult generateNo(string cmp, int apmstoid)
        {
            sSql = "SELECT apserviceno FROM QL_trnapservicemst WHERE cmpcode='" + cmp + "' AND apservicemstoid=" + apmstoid;
            string sNoAP = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (string.IsNullOrEmpty(sNoAP))
            {

                string sNo = "APSRV-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apserviceno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapservicemst WHERE cmpcode='" + cmp + "' AND apserviceno LIKE '" + sNo + "%'";

                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

                sNo = sNo + sCounter;
                return Json(sNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(sNoAP, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: apserviceMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnapservicemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnapservicemst();
                tbl.cmpcode = CompnyCode;
                tbl.apservicemstoid = ClassFunction.GenerateID("QL_trnapservicemst");
                tbl.apservicedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.apservicemststatus = "In Process";
                //tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                //tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());

                Session["QL_trnapservicedtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnapservicemst.Find(cmp, id);

                sSql = "SELECT apservicedtlseq, apd.poservicemstoid, poserviceno, poservicedtloid, apd.mrservicemstoid, mrserviceno, mrservicedtloid, apd.serviceoid, servicecode, servicelongdesc, apserviceqty, apserviceunitoid, gendesc AS apserviceunit, apserviceprice, apservicedtlamt, apservicedtldisctype, apservicedtldiscvalue, apservicedtldiscamt, apservicedtlnetto, apservicedtltaxamt, apservicedtlnote, ISNULL(apservicedtlres1, '') AS apservicedtlres1, ISNULL(apservicedtlres2, '') AS apservicedtlres2 FROM QL_trnapservicedtl apd INNER JOIN QL_trnposervicemst regm ON regm.cmpcode=apd.cmpcode AND regm.poservicemstoid=apd.poservicemstoid INNER JOIN QL_trnmrservicemst mrm ON mrm.cmpcode=apd.cmpcode AND mrm.mrservicemstoid=apd.mrservicemstoid INNER JOIN QL_mstservice m ON m.serviceoid=apd.serviceoid INNER JOIN QL_mstgen g ON genoid=apserviceunitoid WHERE apservicemstoid=" + id + " AND apd.cmpcode='" + cmp + "' ORDER BY apservicedtlseq";
                Session["QL_trnapservicedtl"] = db.Database.SqlQuery<apservicedtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apserviceMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapservicemst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.apserviceno == null)
                tbl.apserviceno = "";

            List<apservicedtl> dtDtl = (List<apservicedtl>)Session["QL_trnapservicedtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnmrservicemst mrm WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid=" + dtDtl[i].mrservicemstoid + " AND mrservicemststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND mrservicemstoid NOT IN (SELECT apd.mrservicemstoid FROM QL_trnapservicedtl apd WHERE apd.cmpcode=mrm.cmpcode AND apservicemstoid=" + tbl.apservicemstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "poservice No. " + dtDtl[i].poserviceno + " has been used by another data. Please cancel this transaction or use another poservice No.!");
                    }
                }
            }

            var msg = "";

            cRate.SetRateValue(tbl.curroid, tbl.apservicedate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.apservicemststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapservicemst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.apservicemststatus == "Revised")
                tbl.apservicemststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnapservicemst");
                var dtloid = ClassFunction.GenerateID("QL_trnapservicedtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.apservicetotalamtidr = (tbl.apservicetotalamt * decimal.Parse(tbl.apservicerate2toidr));
                tbl.apservicetotalamtusd = (tbl.apservicetotalamt * decimal.Parse(tbl.apservicerate2tousd));
                tbl.apservicetotaldiscidr = (tbl.apservicetotaldisc * decimal.Parse(tbl.apservicerate2toidr));
                tbl.apservicetotaldiscusd = (tbl.apservicetotaldisc * decimal.Parse(tbl.apservicerate2tousd));
                tbl.apservicetotaltaxidr = (tbl.apservicetotaltax * decimal.Parse(tbl.apservicerate2toidr));
                tbl.apservicetotaltaxusd = (tbl.apservicetotaltax * decimal.Parse(tbl.apservicerate2tousd));
                tbl.apservicegrandtotalidr = (tbl.apservicegrandtotal * decimal.Parse(tbl.apservicerate2toidr));
                tbl.apservicegrandtotalusd = (tbl.apservicegrandtotal * decimal.Parse(tbl.apservicerate2tousd));

                tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                //tbl.cancelseq = 0;
                //tbl.apcloseuser = "";
                //tbl.apclosetime = DateTime.Parse("1/1/1900 00:00:00");
                if (tbl.apservicemstnote == null)
                    tbl.apservicemstnote = "";
                if (tbl.apservicetotaldisc == null)
                    tbl.apservicetotaldisc = 0;
                if (tbl.apservicetotaldiscidr == null)
                    tbl.apservicetotaldiscidr = 0;
                if (tbl.apservicetotaldiscusd == null)
                    tbl.apservicetotaldiscusd = 0;


                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnapservicemst.Find(tbl.cmpcode, tbl.apservicemstoid) != null)
                                tbl.apservicemstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.apservicedate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnapservicemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.apservicemstoid + " WHERE tablename='QL_trnapservicemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE MRDTL SET mrservicedtlstatus='' FROM (SELECT mrservicedtlstatus FROM QL_trnmrservicedtl mrd INNER JOIN QL_trnapservicedtl apd ON apd.cmpcode=mrd.cmpcode AND apd.mrservicemstoid=mrd.mrservicemstoid AND apd.mrservicedtloid=mrd.mrservicedtloid  WHERE apd.cmpcode='" + tbl.cmpcode + "' AND apd.apservicemstoid=" + tbl.apservicemstoid + ") AS MRDTL ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrservicemst SET mrservicemststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid IN (SELECT mrservicemstoid FROM QL_trnapservicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND apservicemstoid=" + tbl.apservicemstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapservicedtl.Where(a => a.apservicemstoid == tbl.apservicemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapservicedtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnapservicedtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].apservicedtlnote == null)
                                dtDtl[i].apservicedtlnote = "";

                            tbldtl = new QL_trnapservicedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.apservicedtloid = dtloid++;
                            tbldtl.apservicemstoid = tbl.apservicemstoid;
                            tbldtl.apservicedtlseq = i + 1;
                            tbldtl.poservicedtloid = dtDtl[i].poservicedtloid;
                            tbldtl.poservicemstoid = dtDtl[i].poservicemstoid;
                            tbldtl.mrservicedtloid = dtDtl[i].mrservicedtloid;
                            tbldtl.mrservicemstoid = dtDtl[i].mrservicemstoid;
                            tbldtl.serviceoid = dtDtl[i].serviceoid;
                            tbldtl.apserviceqty = Convert.ToInt16(dtDtl[i].apserviceqty);
                            tbldtl.apserviceunitoid = dtDtl[i].apserviceunitoid;
                            tbldtl.apserviceprice = dtDtl[i].apserviceprice;
                            tbldtl.apservicedtlamt = dtDtl[i].apservicedtlamt;
                            tbldtl.apservicedtldisctype = dtDtl[i].apservicedtldisctype;
                            tbldtl.apservicedtldiscvalue = dtDtl[i].apservicedtldiscvalue;
                            tbldtl.apservicedtldiscamt = dtDtl[i].apservicedtldiscamt;
                            tbldtl.apservicedtlnetto = dtDtl[i].apservicedtlnetto;
                            tbldtl.apservicedtltaxamt = dtDtl[i].apservicedtltaxamt;
                            tbldtl.apservicedtlnote = dtDtl[i].apservicedtlnote;
                            tbldtl.apservicedtlstatus = "";
                            tbldtl.apservicedtlres1 = dtDtl[i].apservicedtlres1;
                            tbldtl.apservicedtlres2 = dtDtl[i].apservicedtlres2;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnapservicedtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrservicedtl SET mrservicedtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicedtloid=" + dtDtl[i].mrservicedtloid + " AND mrservicemstoid=" + dtDtl[i].mrservicemstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnmrservicemst SET mrservicemststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid=" + dtDtl[i].mrservicemstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnapservicedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.apservicemststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AP-Service" + tbl.apservicemstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnapservicemst";
                                tblApp.oid = tbl.apservicemstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.apservicemstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        //return View(ex.ToString());
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.apservicemststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apserviceMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapservicemst tbl = db.QL_trnapservicemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmrservicedtl SET mrservicedtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicedtloid IN (SELECT mrservicedtloid FROM QL_trnapservicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND apservicemstoid=" + tbl.apservicemstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmrservicemst SET mrservicemststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrservicemstoid IN (SELECT mrservicemstoid FROM QL_trnapservicedtl WHERE cmpcode='" + tbl.cmpcode + "' AND apservicemstoid=" + tbl.apservicemstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();



                        var trndtl = db.QL_trnapservicedtl.Where(a => a.apservicemstoid == id && a.cmpcode == cmp);
                        db.QL_trnapservicedtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapservicemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPTrn.rpt"));

            sSql = "SELECT DISTINCT apm.apservicemstoid  [Oid], CONVERT(VARCHAR(10), apm.apservicemstoid) AS draftno, apserviceno [AP No.], apservicedate  [AP Date], DATEADD(day, (CASE g1.gendesc WHEN '-' THEN 0 ELSE CAST(g1.gendesc AS INT) END),apservicedate) [Due Date], apservicemstnote[Header Note], apservicemststatus[Status], suppname[Supplier], g1.gendesc AS[Payment Type], currcode[Curr Code], apserviceratetoidr[Rate to IDR], CAST(apserviceratetousd AS REAL) AS[Rate to USD], apservicerate2toidr[Rate2 to IDR], CAST(apservicerate2tousd AS REAL) [Rate2 to USD], apservicemstres1[Res1], apservicetotalamt[Total Amount], apservicetotaldisc[Total Disc], apservicetotaltax[Total Tax], apservicegrandtotal[Grand Total], apservicedtloid[Dtl Oid], apservicedtlseq AS[No.],'' AS[Reg.No.], mrserviceno AS[MR No.], CAST((SELECT servicecode FROM QL_mstservice m WHERE m.serviceoid = apd.serviceoid) AS VARCHAR(500)) AS[Code], CAST((SELECT servicelongdesc FROM QL_mstservice m WHERE m.serviceoid= apd.serviceoid) AS VARCHAR(500)) AS[Description], apserviceqty AS[Qty], g2.gendesc AS[Unit], apserviceprice AS[Price], apservicedtlamt AS[Amount], ISNULL(apservicedtldiscamt, 0.0) AS[Disc.], apservicedtlnetto AS[Netto], apservicedtlnote AS[Note], divname[Bussiness Unit], apservicedtlres1 AS[Faktur No], apservicedtlres2 AS[Faktur Pajak No] FROM QL_trnapservicemst apm INNER JOIN QL_mstsupp s ON s.suppoid = apm.suppoid INNER JOIN QL_mstcurr c ON c.curroid = apm.curroid INNER JOIN QL_mstgen g1 ON g1.genoid = apservicepaytypeoid INNER JOIN QL_trnapservicedtl apd ON apd.cmpcode = apm.cmpcode AND apd.apservicemstoid = apm.apservicemstoid INNER JOIN QL_trnmrservicemst mrm ON mrm.cmpcode = apd.cmpcode AND mrm.mrservicemstoid = apd.mrservicemstoid INNER JOIN QL_mstgen g2 ON g2.genoid = apserviceunitoid INNER JOIN QL_mstdivision div ON div.cmpcode = apm.cmpcode WHERE apm.cmpcode = '" + cmp + "' AND apm.apservicemstoid IN (" + id + ") ORDER BY apm.apservicemstoid, apservicedtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintAPService");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sHeader", "HUTANG GENERAL MATERIAL PRINT OUT");

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "APServiceReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}