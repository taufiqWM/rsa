﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class POServiceController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            public string supptaxable { get; set; }
            public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

        public class poservicemst
        {
            public int poservicemstoid { get; set; }
            public string poserviceno { get; set; }
            public DateTime poservicedate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string poservicemststatus { get; set; }
            public string poservicemstnote { get; set; }
            public string divname { get; set; }
            public string cmpcode { get; set; }
        }

        public class poservicedtl
        {
            public int poservicedtlseq { get; set; }
            public int serviceoid { get; set; }
            public string servicecode { get; set; }
            public string servicelongdesc { get; set; }
            public int poserviceqty { get; set; }
            public int poserviceunitoid { get; set; }
            public string poserviceunit { get; set; }
            public decimal poserviceprice { get; set; }
            public decimal poservicedtlnetto { get; set; }
            public string poservicedtlnote { get; set; }
            public string poservicedtlnote2 { get; set; }
            public decimal lastpoprice { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<poservicedtl> tbl)
        {
            Session["QL_trnposervicedtl"] = tbl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnposervicedtl"] == null)
            {
                Session["QL_trnposervicedtl"] = new List<poservicedtl>();
            }

            List<poservicedtl> tbl = (List<poservicedtl>)Session["QL_trnposervicedtl"];
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: POService
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT pom.poservicemstoid, pom.poserviceno, poservicedate, s.suppname, (CASE ISNULL(pom.poservicemstres1, '') WHEN '' THEN pom.poservicemststatus ELSE pom.poservicemstres1 END) poservicemststatus, pom.poservicemstnote, div.divname, pom.cmpcode FROM QL_trnposervicemst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND poservicedate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND poservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND poservicedate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND poservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil != null)
                {
                    sSql += " AND poservicedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poservicedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND poservicemststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND poservicemststatus IN ('In Process', 'Revised')";
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<poservicemst> dt = db.Database.SqlQuery<poservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: POService/PrintReport/id/cmp
        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnposervicemst.Find(cmp, id);
            if (tbl == null)
                return null;

            var rptname = "rptPOService_Trn";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.poservicemstoid [Oid], CONVERT(VARCHAR(10), pom.poservicemstoid) AS [Draft No.], pom.poserviceno2 [PO No.], pom.poservicedate [PO Date], pom.poservicemstnote [Header Note], pom.poservicemststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], (SELECT s.suppaddr FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Address], g1.gendesc AS [Payment Type], pom.poservicemstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.poservicemstdiscamt, 0) AS [Disc Amount], pom.poservicetotalnetto [Total Netto], pom.poservicetaxtype [Tax Type], CONVERT(VARCHAR(10), pom.poservicetaxamt) AS [Tax Amount], ISNULL(pom.poservicevat, 0) AS [VAT], 0.0 AS [Delivery Cost], 0.0 AS [Other Cost], pom.poservicegrandtotalamt [Grand Total], pod.poservicedtloid [DtlOid], m.servicecode AS [Code], m.servicelongdesc AS [Material], '' AS [PR No.], CAST('01/01/1900' AS DATETIME) AS [PR Date], pod.poserviceqty AS [Qty], g2.gendesc AS [Unit], pod.poserviceprice AS [Price], (pod.poservicedtlnetto + ISNULL(pod.poservicedtldiscamt, 0)) AS [Amount], ISNULL(pod.poservicedtldiscamt, 0) AS [Detail Disc.], pod.poservicedtlnetto AS [Netto], pod.poservicedtlnote AS [Note], 'Local' AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], '' AS [SuppReff], pod.poservicedtldisctype AS [DtlDiscType], ISNULL(pod.poservicedtldiscvalue,0) AS [DtlDiscValue], pod.poservicedtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], CAST('01/01/1900' AS DATETIME) AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnposervicemst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.poservicepaytypeoid INNER JOIN QL_trnposervicedtl pod ON pod.cmpcode=pom.cmpcode AND pod.poservicemstoid=pom.poservicemstoid INNER JOIN QL_mstservice m ON m.serviceoid=pod.serviceoid INNER JOIN QL_mstgen g2 ON g2.genoid=m.serviceunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.poservicemstoid=" + id + " ORDER BY pom.cmpcode, pom.poservicemstoid, pod.poservicedtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", Session["UserName"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "POServicePrintOut.pdf");
        }

        // GET: POService/Form/id/cmp
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnposervicemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnposervicemst();
                tbl.cmpcode = CompnyCode;
                tbl.poservicemstoid = ClassFunction.GenerateID("QL_trnposervicemst");
                tbl.poservicedate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.poservicemststatus = "In Process";
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
                Session["QL_trnposervicedtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnposervicemst.Find(cmp, id);
                FillAdditionalField(tbl);

                sSql = "SELECT poservicedtlseq, pod.serviceoid, servicecode, servicelongdesc,s.serviceunitoid AS poserviceunitoid, gendesc poserviceunit, poserviceqty, poserviceprice, poservicedtlnetto, poservicedtlnote,'' poservicedtlnote2, ISNULL((SELECT TOP 1 crd.lastprice FROM QL_crdservice crd WHERE crd.cmpcode=pod.cmpcode AND crd.serviceoid=pod.serviceoid AND crd.curroid=" + tbl.curroid + " ORDER BY crd.updtime DESC), 0.0) lastpoprice FROM QL_trnposervicedtl pod INNER JOIN QL_mstservice s ON s.serviceoid=pod.serviceoid INNER JOIN QL_mstgen g1 ON genoid=s.serviceunitoid WHERE pod.cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + " ORDER BY poservicedtlseq";
                var tbldtl = db.Database.SqlQuery<poservicedtl>(sSql).ToList();

                Session["QL_trnposervicedtl"] = tbldtl;
            }
            
            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        // POST: POService/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnposervicemst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (string.IsNullOrEmpty(tbl.cmpcode))
                ModelState.AddModelError("cmpcode", "Please select BUSINESS UNIT!");
            if (string.IsNullOrEmpty(tbl.poserviceno))
                tbl.poserviceno = "";
            if (tbl.suppoid == 0)
                ModelState.AddModelError("suppoid", "Please select SUPPLIER!");
            if (tbl.poservicepaytypeoid == 0)
                ModelState.AddModelError("poservicepaytypeoid", "Please select PAYMENT TYPE!");
            if (tbl.curroid == 0)
                ModelState.AddModelError("curroid", "Please select CURRENCY!");            

            List<poservicedtl> dtDtl = (List<poservicedtl>)Session["QL_trnposervicedtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    if (dtDtl[i].poserviceqty <= 0)
                        ModelState.AddModelError("", "PO Qty for Service " + dtDtl[i].servicelongdesc + " must be more than 0!");
                    if (dtDtl[i].poserviceprice <= 0)
                        ModelState.AddModelError("", "PO Price for Service " + dtDtl[i].servicelongdesc + " must be more than 0!");
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.poservicemststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnposervicemst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' /*OR approvaluser='" + tbl.approvalcode + "'*/)").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                    tbl.poservicemststatus = "In Process";
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.poservicemststatus == "Revised")
                tbl.poservicemststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnposervicemst");
                var dtloid = ClassFunction.GenerateID("QL_trnposervicedtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.curroid = 1;
                        tbl.rateoid = 0;
                        tbl.rate2oid = 0;
                        tbl.poserviceratetoidr = 0;
                        tbl.poserviceratetousd = 0;
                        tbl.poservicerate2toidr = 0;
                        tbl.poservicerate2tousd = 0;
                        tbl.poservicetotalamtidr = tbl.poservicetotalamt;
                        tbl.poservicetotalnettoidr = tbl.poservicetotalamt;
                        tbl.poservicetotalnettousd = 0;
                        tbl.poservicegrandtotalamtidr = tbl.poservicegrandtotalamt;
                        tbl.poservicegrandtotalamtusd = 0;
                        tbl.poserviceratetoidrchar = "";
                        tbl.poserviceratetousdchar = "";
                        tbl.poserviceratetousdchar = "";
                        tbl.poservicerate2toidrchar = "";
                        tbl.poservicerate2tousdchar = "";
                        tbl.revisereason = "";
                        tbl.reviseuser = "";
                        tbl.revisetime = new DateTime(1900, 01, 01);
                        tbl.rejectreason = "";
                        tbl.rejectuser = "";
                        tbl.rejecttime = new DateTime(1900, 01, 01);

                        if (action == "New Data")
                        {
                            if (db.QL_trnposervicemst.Find(tbl.cmpcode, tbl.poservicemstoid) != null)
                                tbl.poservicemstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.poservicedate);
                            tbl.poservicetotalnetto = tbl.poservicetotalamt;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnposervicemst.Add(tbl);

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.poservicemstoid + " WHERE tablename='QL_trnposervicemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            var trndtl = db.QL_trnposervicedtl.Where(a => a.poservicemstoid == tbl.poservicemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnposervicedtl.RemoveRange(trndtl);
                        }

                        QL_trnposervicedtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnposervicedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.poservicedtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.poservicemstoid = tbl.poservicemstoid;
                            tbldtl.poservicedtlseq = i + 1;
                            tbldtl.serviceoid = dtDtl[i].serviceoid;
                            tbldtl.poserviceprice = dtDtl[i].poserviceprice;
                            tbldtl.poservicedtlnetto = dtDtl[i].poservicedtlnetto;
                            tbldtl.poservicedtlnote = dtDtl[i].poservicedtlnote;
                            tbldtl.poservicedtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.poserviceqty = dtDtl[i].poserviceqty;
                            tbldtl.poserviceunitoid = dtDtl[i].poserviceunitoid;
                            tbldtl.unittolerance = "";

                            db.QL_trnposervicedtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnposervicedtl'";
                        db.Database.ExecuteSqlCommand(sSql);

                        // INSERT APPROVAL
                        if (tbl.poservicemststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-SVC" + tbl.poservicemstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnposervicemst";
                                tblApp.oid = tbl.poservicemstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                        }
                        // END INSERT APPROVAL

                        db.SaveChanges();
                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.poservicemstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }
            }
            tbl.poservicemststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: POService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnposervicemst tbl = db.QL_trnposervicemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnposervicedtl.Where(a => a.poservicemstoid == id && a.cmpcode == cmp);
                        db.QL_trnposervicedtl.RemoveRange(trndtl);

                        db.QL_trnposervicemst.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitDDL(QL_trnposervicemst tbl, string action)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var poservicepaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.poservicepaytypeoid);
            ViewBag.poservicepaytypeoid = poservicepaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            //if (action == "New Data" && cmpcode.Count() > 0)
            //{
            //    var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(GetQueryApprovalUser(cmpcode.First().Value, "QL_trnposervicemst", CompnyCode)).ToList(), "approvaluser", "approvaluser");
            //    ViewBag.approvalcode = approvalcode;
            //}
            //else
            //{
            //    var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(GetQueryApprovalUser(tbl.cmpcode, "QL_trnposervicemst", CompnyCode)).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //    ViewBag.approvalcode = approvalcode;
            //}
        }

        public static string GetQueryApprovalUser(string cmp, string tbl, string cmpcorp)
        {
            var result = "SELECT * FROM QL_approvalperson WHERE tablename='" + tbl + "' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + cmpcorp + "') ORDER BY approvaluser";
            return result;
        }

        private void FillAdditionalField(QL_trnposervicemst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetSuppData(int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, (CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 11 ELSE 0 END) supptaxamt, /*c.currcode, suppcurroid,*/ gp.gendesc supppayment  FROM QL_mstsupp s /*INNER JOIN QL_mstcurr c ON suppcurroid=c.curroid*/ INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' And s.divgroupoid='"+divgroupoid+"' AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp)
        {
            var result = "";
            JsonResult js = null;
            List<poservicedtl> tbl = new List<poservicedtl>();

            try
            {
                sSql = "SELECT 0 poservicedtlseq, serviceoid, servicecode, servicelongdesc, 0 AS poserviceqty, s.serviceunitoid poserviceunitoid, g1.gendesc poserviceunit, 0.0 AS poserviceprice, 0.0 poservicedtlnetto, '' AS poservicedtlnote, '' AS poservicedtlnote2, ISNULL((SELECT TOP 1 crd.lastprice FROM QL_crdservice crd WHERE crd.serviceoid=s.serviceoid ORDER BY crd.updtime DESC), 0.0) AS lastpoprice FROM QL_mstservice s INNER JOIN QL_mstgen g1 ON g1.genoid=s.serviceunitoid AND gengroup='MATERIAL UNIT' WHERE s.cmpcode='" + CompnyCode + "' AND s.activeflag='ACTIVE'";
                sSql += " ORDER BY servicecode";
                tbl = db.Database.SqlQuery<poservicedtl>(sSql).ToList();
                for (int i = 0; i < tbl.Count; i++)
                {
                    tbl[i].poservicedtlseq = i + 1;
                    tbl[i].poserviceprice = tbl[i].lastpoprice;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp, string tblname)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            tbl = db.Database.SqlQuery<QL_approvalperson>(GetQueryApprovalUser(cmp, tblname, CompnyCode)).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
    }
}