﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PurchaseReturnGenMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnpretgenmst
        {
            public string cmpcode { get; set; }
            public int pretgenmstoid { get; set; }
            public string pretgenno { get; set; }
            public string pogenno { get; set; }
            public DateTime pretgendate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string pretgenmststatus { get; set; }
            public string pretgenmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnpretgendtl
        {
            public int pretgendtlseq { get; set; }
            public int mrgenmstoid { get; set; }
            public int mrgendtloid { get; set; }
            public string mrgenno { get; set; }
            public int pretgenwhoid { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal mrgenqty { get; set; }
            public decimal pretgenqty { get; set; }
            public int pretgenunitoid { get; set; }
            public string pretgenunit { get; set; }
            public string pretgendtlnote { get; set; }
            public decimal mrqty { get; set; }
            public int reasonoid { get; set; }

        }

        private void InitDDL(QL_trnpretgenmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretgenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;

            List<QL_mstgen> tblgen = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PURCHASE RETURN REASON' AND activeflag='ACTIVE'";
            tblgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            var reason = "";
            for (int i = 0;i < tblgen.Count; i++)
            {
                reason += "<option value=\'" + tblgen[i].genoid + "\'>" + tblgen[i].gendesc + "</option>";
            }
            ViewBag.reason = reason;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretgenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int mrgenmstoid, int pretgenmstoid)
        {
            List<trnpretgendtl> tbl = new List<trnpretgendtl>();

            sSql = "SELECT DISTINCT 0 AS pretgendtlseq, mrgenno, mrd.mrgenmstoid, mrgendtloid, mrd.matgenoid, matgencode, matgenlongdesc, matgenlimitqty, mrgenunitoid AS pretgenunitoid, gendesc AS pretgenunit, (mrgenqty - ISNULL((SELECT SUM(pretgenqty) FROM QL_trnpretgendtl pretd INNER JOIN QL_trnpretgenmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretgenmstoid=pretd.pretgenmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrgendtloid=mrd.mrgendtloid AND pretgenmststatus<>'Rejected' AND pretd.pretgenmstoid<>" + pretgenmstoid + "), 0)) AS mrgenqty, 0.0 AS pretgenqty, '' AS pretgendtlnote, mrgendtlseq, 0 AS reasonoid, mrgenwhoid AS pretgenwhoid FROM QL_trnmrgendtl mrd INNER JOIN QL_trnmrgenmst mrm ON mrd.cmpcode=mrm.cmpcode AND mrd.mrgenmstoid=mrm.mrgenmstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=mrd.matgenoid INNER JOIN QL_mstgen g ON genoid=mrgenunitoid WHERE mrd.cmpcode='" + cmp + "' AND mrd.mrgenmstoid=" + mrgenmstoid + " AND ISNULL(mrgendtlres1, '')<>'Complete' ORDER BY mrgendtlseq ";

            tbl = db.Database.SqlQuery<trnpretgendtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpretgendtl> dtDtl)
        {
            Session["QL_trnpretgendtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpretgendtl"] == null)
            {
                Session["QL_trnpretgendtl"] = new List<trnpretgendtl>();
            }

            List<trnpretgendtl> dataDtl = (List<trnpretgendtl>)Session["QL_trnpretgendtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpretgenmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.pogenno = db.Database.SqlQuery<string>("SELECT pogenno FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnpogenmst WHERE cmpcode='" + cmp + "' AND pogenmstoid IN (SELECT pogenmstoid FROM QL_trnmrgenmst WHERE cmpcode='" + cmp + "' AND mrgenmststatus='Post' AND ISNULL(mrgenmstres1, '')<>'Closed') AND pogenmstoid NOT IN (SELECT pogenmstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode=apd.cmpcode AND apm.apgenmstoid=apd.apgenmstoid WHERE apd.cmpcode='" + cmp + "' AND apgenmststatus<>'Rejected') AND (CASE pogentype WHEN 'Local' THEN 'Closed' ELSE pogenmststatus END)='Closed')  ORDER BY suppcode, suppname";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int pogenmstoid { get; set; }
            public string pogenno { get; set; }
            public DateTime pogendate { get; set; }
            public string pogendocrefno { get; set; }
            public string pogenmstnote { get; set; }
            public string pogenflag { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid)
        {
            List<pomst> tbl = new List<pomst>();

            //sSql = "SELECT registermstoid, registerno, registerdate, registerdocrefno, registerdocrefdate, registermstnote, registerflag FROM QL_trnregistermst WHERE cmpcode='" + cmp + "' AND suppoid=" + suppoid + " AND registertype='GEN' AND registermstoid IN (SELECT registermstoid FROM QL_trnmrgenmst WHERE cmpcode='" + cmp + "' AND mrgenmststatus='Post' AND ISNULL(mrgenmstres1, '')<>'Closed') AND registermstoid NOT IN (SELECT registermstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode=apd.cmpcode AND apm.apgenmstoid=apd.apgenmstoid WHERE apd.cmpcode='" + cmp + "' AND apgenmststatus<>'Rejected') AND (CASE registerflag WHEN 'Local' THEN 'Closed' ELSE registermststatus END)='Closed' ORDER BY registermstoid ";
            sSql = "SELECT pogenmstoid, pogenno, pogendate, '' [pogendocrefno], ISNULL(pogenmstnote, '') [pogenmstnote], pogentype " +
                "FROM QL_trnpogenmst " +
                "WHERE cmpcode = '" + cmp + "' AND suppoid = " + suppoid + " AND pogenmstoid IN(SELECT pogenmstoid FROM QL_trnmrgenmst WHERE cmpcode = '" + cmp + "' AND mrgenmststatus = 'Post' AND ISNULL(mrgenmstres1, '') <> 'Closed') AND pogenmstoid NOT IN (SELECT pogenmstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode = apd.cmpcode AND apm.apgenmstoid = apd.apgenmstoid WHERE apd.cmpcode = '" + cmp + "' AND apgenmststatus <> 'Rejected') AND(CASE pogentype WHEN 'Local' THEN 'Closed' ELSE pogenmststatus END) = 'Closed' " +
                "ORDER BY pogenmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class mrmst
        {
            public int mrgenmstoid { get; set; }
            public string mrgenno { get; set; }
            public DateTime mrgendate { get; set; }
            public string mrgenwh { get; set; }
            public string mrgenmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetMRData(string cmp, int pogenmstoid)
        {
            List<mrmst> tbl = new List<mrmst>();
            sSql = "SELECT mrgenmstoid, mrgenno, mrgendate, mrgenwhoid, gendesc AS mrgenwh, mrgenmstnote FROM QL_trnmrgenmst mrm INNER JOIN QL_mstgen ON genoid=mrgenwhoid WHERE mrm.cmpcode='" + cmp + "' AND pogenmstoid=" + pogenmstoid + " AND mrgenmststatus='Post' ORDER BY mrgenmstoid";

            tbl = db.Database.SqlQuery<mrmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = " SELECT pretm.cmpcode, pretgenmstoid, pretm.pretgenno, pretm.pretgendate, suppname, pogenno, pretgenmststatus, pretgenmstnote, divname FROM QL_trnpretgenmst pretm INNER JOIN QL_trnpogenmst reg ON reg.cmpcode = pretm.cmpcode AND reg.pogenmstoid = pretm.pogenmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pretm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pretm.cmpcode WHERE ";
            
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pretm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND pretgendate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretgendate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pretgenmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND pretgendate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretgendate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pretgenmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pretgendate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretgendate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND pretgenmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND pretgenmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND pretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pretmm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpretgenmst> dt = db.Database.SqlQuery<trnpretgenmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretgenmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnpretgenmst();
                tbl.cmpcode = CompnyCode;
                tbl.pretgenmstoid = ClassFunction.GenerateID("QL_trnpretgenmst");
                tbl.pretgendate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.pretgenmststatus = "In Process";
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);

                Session["QL_trnpretgendtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpretgenmst.Find(cmp, id);

                sSql = "SELECT pretgendtlseq, pretd.mrgendtloid, pretd.mrgenmstoid, mrgenno, pretgenwhoid, pretd.mrgendtloid, pretd.matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (mrgenqty - ISNULL((SELECT SUM(x.pretgenqty) FROM QL_trnpretgendtl x INNER JOIN QL_trnpretgenmst xm ON xm.cmpcode=x.cmpcode AND xm.pretgenmstoid=x.pretgenmstoid WHERE x.cmpcode=pretd.cmpcode AND x.mrgendtloid=pretd.mrgendtloid AND x.pretgenmstoid<>pretd.pretgenmstoid AND xm.pretgenmststatus<>'Rejected'), 0)) AS mrgenqty, pretgenqty, pretgenunitoid, g.gendesc AS pretgenunit, ISNULL(pretgendtlnote, '') [pretgendtlnote], reasonoid, mrgenqty AS mrqty FROM QL_trnpretgendtl pretd INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode=pretd.cmpcode AND mrm.mrgenmstoid=pretd.mrgenmstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pretd.matgenoid INNER JOIN QL_mstgen g ON g.genoid=pretgenunitoid INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrgendtloid=pretd.mrgendtloid LEFT JOIN QL_mstgen gres ON gres.genoid=reasonoid WHERE pretd.cmpcode='" + cmp + "' AND pretd.pretgenmstoid=" + id + " ORDER BY pretgendtlseq ";

                Session["QL_trnpretgendtl"] = db.Database.SqlQuery<trnpretgendtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }
        
        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpretgenmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.pretgenno == null)
                tbl.pretgenno = "";

            List<trnpretgendtl> dtDtl = (List<trnpretgendtl>)Session["QL_trnpretgendtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].pretgenqty <= 0)
                        {
                            ModelState.AddModelError("", "PRET QTY for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }
                        //rounded qty

                        sSql = "SELECT (mrgenqty - ISNULL((SELECT SUM(pretgenqty) FROM QL_trnpretgendtl pretd INNER JOIN QL_trnpretgenmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretgenmstoid=pretd.pretgenmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrgendtloid=mrd.mrgendtloid AND pretd.pretgenmstoid<>" + tbl.pretgenmstoid + " AND pretgenmststatus<>'Rejected'), 0)) AS mrgenqty FROM QL_trnmrgendtl mrd WHERE mrd.cmpcode='" + tbl.cmpcode + "' AND mrd.mrgenmstoid=" + dtDtl[i].mrgenmstoid + " AND mrd.mrgendtloid=" + dtDtl[i].mrgendtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].mrgenqty)
                            dtDtl[i].mrgenqty = dQty;
                        if (dQty < dtDtl[i].pretgenqty)
                            ModelState.AddModelError("", "MR QTY for some detail data has been updated by another user. Please check that every detail MR QTY must be more or equal than RETURN QTY!");

                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.pretgenmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                //AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretgenmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretgenmst' AND activeflag='ACTIVE'").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpretgenmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpretgendtl");
                var servertime = ClassFunction.GetServerTime();
                //tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpretgenmst.Find(tbl.cmpcode, tbl.pretgenmstoid) != null)
                                tbl.pretgenmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pretgendate);
                            tbl.registermstoid = 0;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnpretgenmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.pretgenmstoid + " WHERE tablename='QL_trnpretgenmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();


                            sSql = "UPDATE QL_trnmrgendtl SET mrgendtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnpretgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretgenmstoid=" + tbl.pretgenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrgenmst SET mrgenmstres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnpretgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretgenmstoid=" + tbl.pretgenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpretgendtl.Where(a => a.pretgenmstoid == tbl.pretgenmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpretgendtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpretgendtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {

                            tbldtl = new QL_trnpretgendtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.pretgendtloid = dtloid++;
                            tbldtl.pretgenmstoid = tbl.pretgenmstoid;
                            tbldtl.pretgendtlseq = i + 1;
                            tbldtl.mrgenmstoid = dtDtl[i].mrgenmstoid;
                            tbldtl.mrgendtloid = dtDtl[i].mrgendtloid;
                            tbldtl.pretgenwhoid = dtDtl[i].pretgenwhoid;
                            tbldtl.matgenoid = dtDtl[i].matgenoid;
                            tbldtl.pretgenqty = dtDtl[i].pretgenqty;
                            tbldtl.pretgenunitoid = dtDtl[i].pretgenunitoid;
                            tbldtl.pretgendtlstatus = "";
                            tbldtl.pretgendtlnote = dtDtl[i].pretgendtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.pretgenvalueidr = 0;
                            tbldtl.pretgenvalueusd = 0;
                            tbldtl.reasonoid = dtDtl[i].reasonoid;
                            //tbldtl.pretgenvalueidr_stock = 0;
                            //tbldtl.pretgenvalueusd_stock = 0;
                            db.QL_trnpretgendtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].pretgenqty >= dtDtl[i].mrgenqty)
                            {
                                sSql = "UPDATE QL_trnmrgendtl SET mrgendtlres1='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid=" + dtDtl[i].mrgendtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmrgenmst SET mrgenmstres1='Closed' WHERE cmpcode='" + tbl.cmpcode +  "' AND mrgenmstoid=" + dtDtl[i].mrgenmstoid + " AND (SELECT COUNT(*) FROM QL_trnmrgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + dtDtl[i].mrgenmstoid + " AND mrgendtloid<>" + dtDtl[i].mrgendtloid + " AND ISNULL(mrgendtlres1, '')<>'Complete')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpretgendtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.pretgenmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PRET-GEN" + tbl.pretgenmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpretgenmst";
                                tblApp.oid = tbl.pretgenmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.pretgenmstoid + "?cmp=" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.pretgenno = "";
            tbl.pretgenmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretgenmst tbl = db.QL_trnpretgenmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnmrgendtl SET mrgendtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnpretgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretgenmstoid=" + tbl.pretgenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnmrgenmst SET mrgenmstres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnpretgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretgenmstoid=" + tbl.pretgenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpretgendtl.Where(a => a.pretgenmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpretgendtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpretgenmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpretgenmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPRet_PrintOut.rpt"));
            //sSql = "SELECT pretm.pretgenmstoid [Oid], CONVERT(VARCHAR(10), pretm.pretgenmstoid) AS [Draft No.], pretgenno [Return No.], pretgendate [Return Date], registerno [Register No.], pretgenmstnote [Header Note], pretgenmststatus [Status], suppname [Supplier], matgencode [Code], matgenlongdesc [Material], pretgendtlnote [Detail Note], pretgendtlseq [No.], pretgenqty [Qty], gendesc AS [Unit], divname [Business Unit] FROM QL_trnpretgenmst pretm INNER JOIN QL_trnpretgendtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretd.pretgenmstoid=pretm.pretgenmstoid INNER JOIN QL_trnregistermst reg ON reg.cmpcode=pretm.cmpcode AND reg.registermstoid=pretm.registermstoid INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pretd.matgenoid INNER JOIN QL_mstgen ON genoid=pretgenunitoid INNER JOIN QL_mstdivision div ON div.cmpcode=pretm.cmpcode WHERE pretm.cmpcode='" + cmp + "' AND pretm.pretgenmstoid IN (" + id + ") ORDER BY pretm.pretgenmstoid, pretgendtlseq ";
            sSql = "SELECT pretm.pretgenmstoid [Oid], CONVERT(VARCHAR(10), pretm.pretgenmstoid) AS [Draft No.], pretgenno [Return No.], pretgendate [Return Date], pogenno [pogen No.], pretgenmstnote [Header Note], pretgenmststatus [Status], suppname [Supplier], matgencode [Code], matgenlongdesc [Material], pretgendtlnote [Detail Note], pretgendtlseq [No.], pretgenqty [Qty], gendesc AS [Unit], divname [Business Unit] " +
                "FROM QL_trnpretgenmst pretm " +
                "INNER JOIN QL_trnpretgendtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretd.pretgenmstoid = pretm.pretgenmstoid " +
                "INNER JOIN QL_trnpogenmst reg ON reg.cmpcode = pretm.cmpcode AND reg.pogenmstoid = pretm.pogenmstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid = pretm.suppoid INNER JOIN QL_mstmatgen m ON m.matgenoid = pretd.matgenoid " +
                "INNER JOIN QL_mstgen ON genoid = pretgenunitoid " +
                "INNER JOIN QL_mstdivision div ON div.cmpcode = pretm.cmpcode " +
                "WHERE pretm.cmpcode = '" + cmp + "' AND pretm.pretgenmstoid IN (" + id + ") ORDER BY pretm.pretgenmstoid, pretgendtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPRetRaw");

            report.SetDataSource(dtRpt);
            //report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            //report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "PurchaseReturnRawMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
