﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class GeneralMaterialReceivedController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnmrgenmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int mrgenmstoid { get; set; }
            public int mrgenwhoid { get; set; }
            public string mrgenno { get; set; }
            public string pogenno { get; set; }
            public DateTime mrgendate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string mrgenmststatus { get; set; }
            public string mrgenmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnmrgendtl
        {
            public int mrgendtlseq { get; set; }
            public int pogendtloid { get; set; }
            public string pogenno { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal pogendtlqty { get; set; }
            public decimal mrgenqty { get; set; }
            public int mrgenunitoid { get; set; }
            public string mrgenunit { get; set; }
            public string mrgendtlnote { get; set; }
            public decimal poqty { get; set; }
            public decimal mrgenvalue { get; set; }
            public decimal mrgenamt { get; set; }
            public string location { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnmrgenmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            var mrgenwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.mrgenwhoid = mrgenwhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int pogenmstoid, int suppoid, int mrgenmstoid, int mrgenwhoid)
        {
            List<trnmrgendtl> tbl = new List<trnmrgendtl>();


            //sSql = "SELECT 0 AS mrgendtlseq, regd.registerdtloid, pogenno, regd.matrefoid AS matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (registerqty - ISNULL((SELECT SUM(mrgenqty) FROM QL_trnmrgendtl mrd INNER JOIN QL_trnmrgenmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrgenmstoid = mrd.mrgenmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.registerdtloid = regd.registerdtloid AND mrm2.registermstoid = regm.registermstoid AND mrd.mrgenmstoid<>" + mrgenmstoid + "), 0) -ISNULL((SELECT SUM(retd.retgenqty) FROM QL_trnretgendtl retd WHERE retd.cmpcode = regd.cmpcode AND retd.registerdtloid = regd.registerdtloid), 0)) AS registerqty, 0.0 AS mrgenqty, registerunitoid AS mrgenunitoid, gendesc AS mrgenunit, '' AS mrgendtlnote, (CASE pogendtldiscvalue WHEN 0 THEN pogenprice ELSE(pogendtlnetto / pogenqty) END) AS mrgenvalue  , ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode='" + cmp + "' AND loc.mtrwhoid=" + mrgenwhoid + " AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' WHEN 'item' THEN 'Finish Good' ELSE '' END)='Raw' AND loc.matoid=m.matgenoid),'') AS location, registerqty AS regqty /*, 0.0 AS mrgenamt */, registerdtlseq, regm.registermstoid  FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regd.registermstoid=regm.registermstoid INNER JOIN QL_trnpogenmst pom ON pom.cmpcode=regd.cmpcode AND pogenmstoid=porefmstoid INNER JOIN QL_trnpogendtl pod ON pod.cmpcode=regd.cmpcode AND pod.pogenmstoid=pom.pogenmstoid AND pogendtloid=porefdtloid INNER JOIN QL_mstmatgen m ON m.matgenoid=regd.matrefoid INNER JOIN QL_mstgen g ON genoid=registerunitoid WHERE regd.cmpcode='" + cmp + "' AND regd.registermstoid=" + pogenmstoid + " AND registerdtlstatus='' AND ISNULL(registerdtlres2, '')<>'Complete' ORDER BY registerdtlseq";

            sSql = "SELECT 0 AS mrgendtlseq, regd.pogendtloid, pogenno, regd.matgenoid AS matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (pogenqty - ISNULL((SELECT SUM(mrgenqty) FROM QL_trnmrgendtl mrd INNER JOIN QL_trnmrgenmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrgenmstoid = mrd.mrgenmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.pogendtloid = regd.pogendtloid AND mrm2.registermstoid = regm.pogenmstoid AND mrd.mrgenmstoid <> " + mrgenmstoid + "), 0) ) AS pogendtlqty, 0.0 AS mrgenqty, pogenunitoid AS mrgenunitoid, gendesc AS mrgenunit, '' AS mrgendtlnote, (CASE pogendtldiscvalue WHEN 0 THEN pogenprice ELSE(pogendtlnetto / pogenqty) END) AS mrgenvalue, ''[location], pogenqty AS poqty, pogendtlseq, regm.pogenmstoid " +
                "FROM QL_trnpogendtl regd " +
                "INNER JOIN QL_trnpogenmst regm ON regm.cmpcode = regd.cmpcode AND regd.pogenmstoid = regm.pogenmstoid " +
                "INNER JOIN QL_mstmatgen m ON m.matgenoid = regd.matgenoid " +
                "INNER JOIN QL_mstgen g ON genoid = regd.pogenunitoid " +
                "WHERE regd.cmpcode = '" + CompnyCode + "' AND regd.pogenmstoid = " + pogenmstoid + " AND pogendtlstatus = '' AND ISNULL(regm.pogenmstres2, '')<> 'Complete' ORDER BY pogendtlseq";

            tbl = db.Database.SqlQuery<trnmrgendtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmrgendtl> dtDtl)
        {
            Session["QL_trnmrgendtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmrgendtl"] == null)
            {
                Session["QL_trnmrgendtl"] = new List<trnmrgendtl>();
            }

            List<trnmrgendtl> dataDtl = (List<trnmrgendtl>)Session["QL_trnmrgendtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmrgenmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.pogenno = db.Database.SqlQuery<string>("SELECT pogenno FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnpogenmst WHERE cmpcode='" + cmp + "' AND pogenmststatus='Approved' AND (CASE pogentype WHEN 'IMPORT' THEN pogenmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(pogenmstres2, '')<>'Closed' and divgroupoid=" + divgroupoid + ")  ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int pogenmstoid { get; set; }
            public string pogenno { get; set; }
            public DateTime pogendate { get; set; }
            public string pogendocrefno { get; set; }
            public DateTime pogendocrefdate { get; set; }
            public string pogenmstnote { get; set; }
            public string pogentype { get; set; }
            public int curroid { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int divgroupoid)
        {
            List<pomst> tbl = new List<pomst>();
           
            sSql = "SELECT pogenmstoid, pogenno, pogendate, '' [pogendocrefno], pogenmstnote, pogentype, curroid FROM QL_trnpogenmst WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + suppoid + " AND pogenmststatus='Approved' AND (CASE pogentype WHEN 'IMPORT' THEN pogenmstres1 ELSE 'Closed' END)='Closed' AND ISNULL(pogenmstres2, '')<>'Closed' and divgroupoid="+divgroupoid+" ORDER BY pogenmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "mrm", divgroupoid, "divgroupoid");
            sSql = " SELECT mrm.cmpcode, (select gendesc from ql_mstgen where genoid=mrm.divgroupoid) divgroup, mrgenmstoid, mrgenno, mrgendate, suppname, pogenno, mrgenmststatus, ISNULL(mrgenmstnote, '') [mrgenmstnote], divname FROM QL_trnmrgenmst mrm INNER JOIN QL_trnpogenmst reg ON reg.cmpcode = mrm.cmpcode AND reg.pogenmstoid = mrm.pogenmstoid INNER JOIN QL_mstsupp s ON s.suppoid = mrm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode = mrm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "mrm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND mrgenmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND mrgenmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND mrgenmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND mrgendate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrgendate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND mrgenmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND mrm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnmrgenmst> dt = db.Database.SqlQuery<trnmrgenmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnmrgenmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmrgenmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnmrgenmst();
                tbl.cmpcode = CompnyCode;
                tbl.mrgenmstoid = ClassFunction.GenerateID("QL_trnmrgenmst");
                tbl.mrgendate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrgenmststatus = "In Process";


                Session["QL_trnmrgendtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmrgenmst.Find(cmp, id);
                sSql = "SELECT mrgendtlseq, mrd.pogendtloid, pogenno, mrd.matgenoid, matgencode, matgenlongdesc, matgenlimitqty, (pogenqty - ISNULL((SELECT SUM(x.mrgenqty) FROM QL_trnmrgendtl x INNER JOIN QL_trnmrgenmst y ON y.cmpcode=x.cmpcode AND y.mrgenmstoid=x.mrgenmstoid WHERE x.cmpcode=mrd.cmpcode AND x.pogendtloid=mrd.pogendtloid AND reg.pogenmstoid=y.pogenmstoid AND x.mrgenmstoid<>mrd.mrgenmstoid), 0) /*- ISNULL((SELECT SUM(retgenqty) FROM QL_trnretgendtl x WHERE x.cmpcode=mrd.cmpcode AND x.pogendtloid=mrd.pogendtloid), 0)*/) AS pogenqty, (mrgenqty + mrgenbonusqty) AS mrqty, mrgenqty, mrgenbonusqty, mrgenunitoid, gendesc AS mrgenunit, ISNULL(mrgendtlnote, '') [mrgendtlnote], mrgenvalue, (mrgenvalue * mrgenqty) AS mrgenamt, '' AS location, pogenqty AS regqty " +
                    "FROM QL_trnmrgendtl mrd " +
                    "INNER JOIN QL_mstmatgen m ON m.matgenoid = mrd.matgenoid " +
                    "INNER JOIN QL_mstgen g ON genoid = mrgenunitoid " +
                    "INNER JOIN QL_trnpogendtl reg ON reg.cmpcode = mrd.cmpcode AND reg.pogendtloid = mrd.pogendtloid AND pogenmstoid IN(SELECT pogenmstoid FROM QL_trnmrgenmst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mrgenmstoid = mrd.mrgenmstoid) " +
                    "INNER JOIN QL_trnpogenmst pom ON pom.cmpcode = reg.cmpcode AND pom.pogenmstoid = reg.pogenmstoid " +
                    "WHERE mrgenmstoid = " + id + "  AND mrd.cmpcode = '" + cmp + "' ORDER BY mrgendtlseq";
                Session["QL_trnmrgendtl"] = db.Database.SqlQuery<trnmrgendtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private Boolean IsRegisterWillBeClosed(string cmp, int registermstoid)
        {
            List<trnmrgendtl> dtDtl = (List<trnmrgendtl>)Session["QL_trnmrgendtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrgenqty >= dtDtl[i].poqty)
                {
                    sOid += dtDtl[i].pogendtlqty + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" + cmp + "' AND registermstoid=" + registermstoid + " AND registerdtlstatus='' AND registerdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }

        private Boolean IsPOrWillBeClosed(string cmp, int poitemmstoid)
        {
            List<trnmrgendtl> dtDtl = (List<trnmrgendtl>)Session["QL_trnmrgendtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrgenqty >= dtDtl[i].poqty)
                {
                    sOid += dtDtl[i].pogendtlqty + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnpoitemdtl WHERE cmpcode='" + cmp + "' AND poitemmstoid=" + poitemmstoid + " AND poitemdtlstatus='' AND poitemdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }


        public class import
        {
            public int curroid { get; set; }
            public decimal importvalue { get; set; }
        }

        private decimal GetImportCost(string cmp, int registermstoid)
        {
            decimal dImp = 0;
            List<import> tbl = new List<import>();
            sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" + cmp + "' AND icm.registermstoid=" + registermstoid + " GROUP BY icd.curroid ";
            tbl = db.Database.SqlQuery<import>(sSql).ToList();
            for (int i = 0; i < tbl.Count(); i++)
            {
                //cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
                dImp += tbl[i].importvalue;// * cRateTmp.GetRateMonthlyIDRValue
            }

            return dImp;
        }

        public class dtHdr
        {
            public int mrgenmstoid { get; set; }
            public DateTime mrgendate { get; set; }
            public string mrgenno { get; set; }
            public int mrgenwhoid { get; set; }
        }

        public class dtDtl
        {
            public int mrgenmstoid { get; set; }
            public int mrgendtloid { get; set; }
            public int mrgendtlseq { get; set; }
            public int matgenoid { get; set; }
            public int mrgenqty { get; set; }
            public decimal mrgenvalue { get; set; }
            public decimal mrgenamt { get; set; }
        }


        private void GetLastData(string cmp, int registermstoid, int mrgenmstoid, out List<dtHdr> dtLastHdrData, out List<dtDtl> dtLastDtlData)
        {
            dtLastHdrData = new List<dtHdr>();
            sSql = "SELECT mrgenmstoid, mrgendate, mrgenno, mrgenwhoid FROM QL_trnmrgenmst WHERE cmpcode='" + cmp + "' AND mrgenmststatus='Post' AND mrgenno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrgenmstoid<>" + mrgenmstoid;
            sSql += " ORDER BY mrgenmstoid ";
            dtLastHdrData = db.Database.SqlQuery<dtHdr>(sSql).ToList();

            dtLastDtlData = new List<dtDtl>();
            sSql = "SELECT mrm.mrgenmstoid, mrgendtloid, mrgendtlseq, matgenoid, mrgenqty, mrgenvalue, (mrgenqty * mrgenvalue) AS mrgenamt FROM QL_trnmrgenmst mrm INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrgenmstoid=mrm.mrgenmstoid WHERE mrm.cmpcode='" + cmp + "' AND mrgenmststatus='Post' AND mrgenno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'GMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrm.mrgenmstoid<>" + mrgenmstoid;
            sSql += " ORDER BY mrm.mrgenmstoid, mrgendtlseq ";
            dtLastDtlData = db.Database.SqlQuery<dtDtl>(sSql).ToList();
        }

        private string generateNo(string cmp)
        {
            var sNo = "GMR-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrgenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrgenmst WHERE cmpcode='" + cmp + "' AND mrgenno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrgenmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.mrgenno == null)
                tbl.mrgenno = "";

            List<trnmrgendtl> dtDtl = (List<trnmrgendtl>)Session["QL_trnmrgendtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mrgenqty <= 0)
                        {
                            ModelState.AddModelError("", "MR QTY for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }
                        sSql = "SELECT (pogenqty - ISNULL((SELECT SUM(mrgenqty) FROM QL_trnmrgendtl mrd INNER JOIN QL_trnmrgenmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrgenmstoid=mrd.mrgenmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.pogendtloid=regd.pogendtloid AND mrm2.pogenmstoid=regd.pogenmstoid  AND mrd.mrgenmstoid<>" + tbl.mrgenmstoid + "), 0) ) AS poqty  FROM QL_trnpogendtl regd WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.pogendtloid=" + dtDtl[i].pogendtloid + " AND regd.pogenmstoid=" + tbl.pogenmstoid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].poqty)
                            dtDtl[i].poqty = dQty;
                        if (dQty < dtDtl[i].mrgenqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PO Qty!");

                        dtDtl[i].mrgenamt = dtDtl[i].mrgenqty * dtDtl[i].mrgenvalue;
                    }
                }
            }

            if (tbl.mrgenmststatus.ToUpper() == "POST")
            {
                var sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    sVarErr = "VAR_STOCK_GM";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    sVarErr = "VAR_PURC_RECEIVED";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                var msg = "";
                var regdate  = db.Database.SqlQuery<DateTime>("SELECT pogendate FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid).FirstOrDefault();
                
                cRate.SetRateValue(tbl.curroid, regdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
               
                if (msg != "")
                    ModelState.AddModelError("", msg);
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmrgenmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrgendtl");
                var dtloid2 = ClassFunction.GenerateID("QL_trnmrgendtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                var regflag = db.Database.SqlQuery<string>("SELECT pogentype FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid).FirstOrDefault();

                Boolean isRegClosed = false;
                decimal dImportCost = 0;
                decimal dSumMR = 0;
                decimal dSumMR_IDR = 0;
                decimal dSumMR_USD = 0;
                List<dtHdr> dtLastHdrData = null;
                List<dtDtl> dtLastDtlData = null;
                var iConMatOid = 0;
                var iCrdMtrOid = 0;
                var iGLMstOid = 0;
                var iGLDtlOid = 0;
                var iStockValOid = 0;
                var iStockAcctgOid = 0;
                var iRecAcctgOid = 0;
                //DateTime sDate = ClassFunction.GetServerTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                var sDateGRIR = "";

                if (tbl.mrgenmststatus.ToUpper() == "POST")
                {
                    //    cRate.SetRateValue(CInt(curroid.Text), registerdate.Text)
                    //If cRate.GetRateDailyLastError <> "" Then
                    //    showMessage(cRate.GetRateDailyLastError, 2)
                    //    mrgenmststatus.Text = "In Process"
                    //    Exit Sub
                    //End If
                    //If cRate.GetRateMonthlyLastError <> "" Then
                    //    showMessage(cRate.GetRateMonthlyLastError, 2)
                    //    mrgenmststatus.Text = "In Process"
                    //    Exit Sub
                    //End If
                    if (regflag.ToUpper() == "IMPORT")
                    {
                        isRegClosed = IsPOrWillBeClosed(tbl.cmpcode, tbl.pogenmstoid);
                    }
                    if (regflag.ToUpper() == "IMPORT" && isRegClosed == true)
                    {
                        dImportCost = GetImportCost(tbl.cmpcode, tbl.pogenmstoid) / cRate.GetRateMonthlyIDRValue; // /cRate.GetRateMonthlyIDRValue 
                        GetLastData(tbl.cmpcode, tbl.pogenmstoid, tbl.mrgenmstoid, out dtLastHdrData, out dtLastDtlData);
                        dSumMR += dtLastDtlData.Sum(x => x.mrgenamt);
                    }
                    if ((regflag.ToUpper() == "LOCAL") || (regflag.ToUpper() == "IMPORT" & isRegClosed == true))
                    {
                        iConMatOid = ClassFunction.GenerateID("QL_CONMAT");
                        iCrdMtrOid = ClassFunction.GenerateID("QL_CRDMTR");
                        iGLMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                        iGLDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                        iStockValOid = ClassFunction.GenerateID("QL_STOCKVALUE");
                        iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid.Value));
                        iRecAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid.Value));
                        sDateGRIR = ClassFunction.GetServerTime().ToString("MM/dd/yyyy  hh:mm:ss");
                        //tbl.grirposttime = ClassFunction.GetServerTime();
                    }
                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                    tbl.mrgenno = generateNo(tbl.cmpcode);
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmrgenmst.Find(tbl.cmpcode, tbl.mrgenmstoid) != null)
                                tbl.mrgenmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mrgendate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            //tbl.grirposttime = new DateTime(1900, 01, 01);
                            db.QL_trnmrgenmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.mrgenmstoid + " WHERE tablename='QL_trnmrgenmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid + " AND registerdtloid IN (SELECT registerdtloid FROM QL_trnmrgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + tbl.mrgenmstoid + ")";
                            sSql = "UPDATE QL_trnpogendtl SET pogendtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + " AND pogendtloid IN (SELECT pogendtloid FROM QL_trnmrgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + tbl.mrgenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //sSql = "UPDATE QL_trnregistermst SET registermststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid;
                            sSql = "UPDATE QL_trnpogenmst SET pogenmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmrgendtl.Where(a => a.mrgenmstoid == tbl.mrgenmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmrgendtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmrgendtl tbldtl;
                        dSumMR += dtDtl.Sum(x => x.mrgenamt);
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dMRVal = 0;
                            if (dSumMR > 0)
                            {
                                dMRVal = (dtDtl[i].mrgenvalue + ((dtDtl[i].mrgenvalue / dSumMR) * dImportCost));
                            }
                            if (dtDtl[i].mrgendtlnote == null)
                                dtDtl[i].mrgendtlnote = "";
                            dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue,4 ) * (dtDtl[i].mrgenqty));
                            dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mrgenqty));
                            //dSumMR_IDR += Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * ToDouble(objTable.Rows(C1)("mrgenqty").ToString)
                            //dSumMR_USD += Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * ToDouble(objTable.Rows(C1)("mrgenqty").ToString)

                            tbldtl = new QL_trnmrgendtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.mrgendtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.mrgenmstoid = tbl.mrgenmstoid;
                            tbldtl.mrgendtlseq = i + 1;
                            tbldtl.pogendtloid = dtDtl[i].pogendtloid;
                            tbldtl.matgenoid = dtDtl[i].matgenoid;
                            tbldtl.mrgenqty = dtDtl[i].mrgenqty;
                            tbldtl.mrgenbonusqty = 0;
                            tbldtl.mrgenunitoid = dtDtl[i].mrgenunitoid;
                            tbldtl.mrgenvalueidr = dMRVal * cRate.GetRateMonthlyIDRValue; //* cRate.GetRateMonthlyIDRValue;
                            tbldtl.mrgenvalueusd = dMRVal * cRate.GetRateMonthlyUSDValue; //* cRate.GetRateMonthlyUSDValue;
                            tbldtl.mrgendtlstatus = "";
                            tbldtl.mrgendtlnote = dtDtl[i].mrgendtlnote;
                            tbldtl.mrgenvalue = dMRVal;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnmrgendtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].mrgenqty >= dtDtl[i].poqty)
                            {
                                //sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND registerdtloid=" + dtDtl[i].pogendtloid + " AND registermstoid=" + tbl.registermstoid;
                                sSql = "UPDATE QL_trnpogendtl SET pogendtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND pogendtloid=" + dtDtl[i].pogendtloid + " AND pogenmstoid=" + tbl.pogenmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                //sSql = "UPDATE QL_trnregistermst SET registermststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid + " AND (SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid + " AND registerdtloid<>" + dtDtl[i].pogendtloid + " AND registerdtlstatus='')=0";
                                sSql = "UPDATE QL_trnpogenmst SET pogenmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + " AND (SELECT COUNT(*) FROM QL_trnpogendtl WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + " AND pogendtloid<>" + dtDtl[i].pogendtloid + " AND pogendtlstatus='')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnmrgendtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.mrgenmststatus.ToUpper() == "POST")
                        {
                            if (regflag.ToUpper() == "IMPORT" && isRegClosed == true)
                            {
                                for (int i = 0; i < dtLastHdrData.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnmrgenmst SET grirposttime='" + sDateGRIR + "' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + dtLastHdrData[i].mrgenmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    var a = dtLastDtlData.Where(x => x.mrgenmstoid == dtLastHdrData[i].mrgenmstoid);
                                    for (int k = 0; k < a.Count(); k++)
                                    {
                                        decimal dMRVal = 0;
                                        if (dSumMR > 0)
                                        {
                                            dMRVal = (dtLastDtlData[k].mrgenvalue + ((dtLastDtlData[k].mrgenvalue / dSumMR) * dImportCost));
                                        }
                                        dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * (dtDtl[i].mrgenqty));
                                        dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mrgenqty));

                                        sSql = "UPDATE QL_trnmrgendtl SET mrgenvalue=" + dMRVal + ", mrgenvalueidr=" + (dMRVal * cRate.GetRateMonthlyIDRValue) + ", mrgenvalueusd=" + (dMRVal * cRate.GetRateMonthlyUSDValue) + " WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid=" + dtLastDtlData[k].mrgendtloid;
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();

                                        // Insert QL_conmat
                                        db.QL_conmat.Add(ClassFunction.InsertConMat(tbl.cmpcode, iConMatOid++, "GMR", "QL_trnmrgendtl", dtLastHdrData[i].mrgenmstoid, dtLastDtlData[k].matgenoid, "GENERAL MATERIAL", dtLastHdrData[i].mrgenwhoid, dtLastDtlData[k].mrgenqty, "General Material Received", dtLastHdrData[i].mrgenno, Session["UserID"].ToString(), "","", (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), 0, null, dtLastDtlData[k].mrgendtloid, tbl.divgroupoid ?? 0,""));
                                        db.SaveChanges();

                                       

                                        //Insert QL_stockvalue
                                        sSql = ClassFunction.GetQueryUpdateStockValue(dtLastDtlData[k].mrgenqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrgendtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtLastDtlData[k].matgenoid, "GENERAL MATERIAL");
                                        if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                        {
                                            db.SaveChanges();
                                            sSql = sSql = ClassFunction.GetQueryInsertStockValue(dtLastDtlData[k].mrgenqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrgendtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtLastDtlData[k].matgenoid, "GENERAL MATERIAL", iStockValOid++);
                                            db.Database.ExecuteSqlCommand(sSql);
                                        }
                                        db.SaveChanges();


                                    }// for Dtl
                                } //for Hdr
                            } // import

                            if ((regflag.ToUpper() == "LOCAL") || (regflag.ToUpper() == "IMPORT" & isRegClosed == true))
                            {
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    decimal dMRVal = 0;
                                    if (dSumMR > 0)
                                    {
                                        dMRVal = (dtDtl[i].mrgenvalue + ((dtDtl[i].mrgenvalue / dSumMR) * dImportCost));
                                    }

                                    // Insert QL_conmat
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(tbl.cmpcode, iConMatOid++, "GMR", "QL_trnmrgendtl", tbl.mrgenmstoid, dtDtl[i].matgenoid, "GENERAL MATERIAL", tbl.mrgenwhoid, dtDtl[i].mrgenqty, "General Material Received", tbl.mrgenno, Session["UserID"].ToString(), "","", (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), 0, null, dtloid2++, tbl.divgroupoid ?? 0,""));
                                    db.SaveChanges();

                                  

                                    //Insert QL_stockvalue
                                    sSql = ClassFunction.GetQueryUpdateStockValue(dtDtl[i].mrgenqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrgendtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matgenoid, "GENERAL MATERIAL");
                                    if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                    {
                                        db.SaveChanges();
                                        sSql = sSql = ClassFunction.GetQueryInsertStockValue(dtDtl[i].mrgenqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrgendtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matgenoid, "GENERAL MATERIAL", iStockValOid++);
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    db.SaveChanges();

                                }
                                dSumMR += dImportCost;
                                if (dSumMR > 0)
                                {
                                    // Insert QL_trnglmst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, iGLMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "MR Gen|No. " + tbl.mrgenno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 1, iGLMstOid, iStockAcctgOid, "D", dSumMR, tbl.mrgenno, "MR Gen|No. " + tbl.mrgenno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmrgenmst " + tbl.mrgenmstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 2, iGLMstOid, iRecAcctgOid, "C", dSumMR, tbl.mrgenno, "MR Gen|No. " + tbl.mrgenno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmrgenmst " + tbl.mrgenmstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    iGLMstOid += 1;
                                }

                                sSql = " UPDATE QL_mstoid SET lastoid=" + (iConMatOid - 1) + " WHERE tablename='QL_CONMAT' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iCrdMtrOid - 1) + " WHERE tablename='QL_CRDMTR' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLMstOid - 1) + " WHERE tablename='QL_TRNGLMST' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } //update Stock & GL
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.mrgenmstoid );
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.mrgenno = "";
            tbl.mrgenmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrgenmst tbl = db.QL_trnmrgenmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnpogendtl SET pogendtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + " AND pogendtloid IN (SELECT pogendtloid FROM QL_trnmrgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + tbl.mrgenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpogenmst SET pogenmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmrgendtl.Where(a => a.mrgenmstoid == id && a.cmpcode == cmp);
                        db.QL_trnmrgendtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmrgenmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmrgenmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMR.rpt"));
            //sSql = "SELECT mrm.mrgenmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrgenmstoid)) AS [Draft No.], mrm.mrgenno AS [MR No.], mrm.mrgendate AS [MR Date], mrm.mrgenmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrgenmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrgenmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrgenmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrgendtlseq AS [No.] , m.matgencode AS [Code], m.matgenlongdesc AS [Description], mrd.mrgenqty AS [Qty], mrgenbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrgendtlnote AS [Note], pom.pogenno AS [PO No.], pom.pogendate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg PostDate], registerdocrefdate AS [Tgl. SJ], registerdocrefno [No SJ],registernopol [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] " +
            //    ", ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=mrm.cmpcode AND loc.mtrwhoid=mrm.mrgenwhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)='Raw' AND loc.matoid=mrd.matgenoid),'') AS [Detail Location] " +
            //    " FROM QL_trnmrgenmst mrm INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrgenmstoid=mrm.mrgenmstoid INNER JOIN QL_mstmatgen m ON  m.matgenoid=mrd.matgenoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrgenunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrgenwhoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnpogenmst pom ON pom.cmpcode=mrm.cmpcode AND  pom.pogenmstoid=regd.porefmstoid INNER JOIN QL_trnpogendtl pod ON pod.cmpcode=mrm.cmpcode AND pod.pogendtloid=regd.porefdtloid INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='GEN' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrgenmstoid IN (" + id + ") ORDER BY mrm.mrgenmstoid, mrgendtlseq ";
            sSql = "SELECT mrm.mrgenmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrgenmstoid)) AS [Draft No.], mrm.mrgenno AS [MR No.], mrm.mrgendate AS [MR Date], mrm.mrgenmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrgenmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrgenmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrgenmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrgendtlseq AS [No.] , m.matgencode AS [Code], m.matgenlongdesc AS [Description], mrd.mrgenqty AS [Qty], mrgenbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrgendtlnote AS [Note], regm.pogenno AS [PO No.], regm.pogendate AS [PO Date], regm.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE regm.approvaluser=p2.profoid) AS [PO UserApp] ,regm.pogenno [Reg No.],regm.pogendate [Reg Date], regm.updtime AS [Reg PostDate], CAST('01/01/1900' AS date) AS [Tgl. SJ], '' [No SJ], '' [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost], '' AS [Detail Location] " +
                " FROM QL_trnmrgenmst mrm  " +
                "INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrgenmstoid=mrm.mrgenmstoid  " +
                "INNER JOIN QL_mstmatgen m ON  m.matgenoid=mrd.matgenoid  " +
                "INNER JOIN QL_mstgen g ON g.genoid=mrd.mrgenunitoid  " +
                "INNER JOIN QL_mstgen g1 ON g1.genoid=mrgenwhoid  " +
                "INNER JOIN QL_trnpogendtl regd ON regd.cmpcode=mrm.cmpcode AND regd.pogenmstoid=mrm.pogenmstoid AND regd.pogendtloid=mrd.pogendtloid " + 
                "INNER JOIN QL_trnpogenmst regm ON mrm.cmpcode = regm.cmpcode AND regm.pogenmstoid = mrm.pogenmstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid  " + 
                "WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrgenmstoid IN (" + id + ") ORDER BY mrm.mrgenmstoid, mrgendtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintmrgen");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("Header", "GENERAL MATERIAL RECEIVED PRINT OUT");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "GeneralMaterialReceivedPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
