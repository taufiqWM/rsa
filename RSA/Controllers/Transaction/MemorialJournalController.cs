﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.Transaction
{
    public class MemorialJournalController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class trnglmst
        {
            public string cmpcode { get; set; }
            public int glmstoid { get; set; }
            public string glother1 { get; set; }
            public DateTime gldate { get; set; }
            public string glflag { get; set; }
            public string glother3 { get; set; }
        }

        public class trngldtl
        {
            public int glseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string gldbcr { get; set; }
            public decimal glamt { get; set; }
            public decimal glamtdb { get; set; }
            public decimal glamtcr { get; set; }
            public string glnote { get; set; }
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
        }

        private void InitDDL(QL_trnglmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var glother2 = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.glother2);
            ViewBag.glother2 = glother2;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", null);
            ViewBag.groupoid = groupoid;
        }

        [HttpPost]
        public ActionResult InitDDLDiv(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLCurr(string sFilter)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcurr> tbl = new List<QL_mstcurr>();
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' " + sFilter + " AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstcurr>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string[] sVar)
        {
            List<trngldtl> tbl = new List<trngldtl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, 0.0 glamt, 0.0 glamtdb, 0.0 glamtcr, '' glnote, 0 groupoid, '' groupdesc, 'D' gldbcr FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string glother1, string cmpcode, string ratetype, int glmstoid)
        {
            return Json(ClassFunction.ShowCOAPosting(glother1, cmpcode, ratetype, "QL_trnglmst " + glmstoid), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trngldtl> dtDtl)
        {
            Session["QL_trngldtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trngldtl"] == null)
            {
                Session["QL_trngldtl"] = new List<trngldtl>();
            }

            List<trngldtl> dataDtl = (List<trngldtl>)Session["QL_trngldtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnglmst tbl)
        {
            ViewBag.isgl1side = 0;
            if (tbl.gl1side == "Y")
            {
                ViewBag.isgl1side = 1;
            }
        }

        private string GenerateMemoNo2(string cmp, DateTime gldate)
        {
            var glother1 = "";
            if (cmp != "")
            {
                string sNo = "MJ-" + gldate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(glother1, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnglmst WHERE cmpcode='" + cmp + "' AND glother1 LIKE '%" + sNo + "%'";
                glother1 = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
            }
            return glother1;
        }

        [HttpPost]
        public ActionResult GenerateMemoNo(string cmp, DateTime gldate)
        {
            var glother1 = "";
            if (cmp != "")
            {
                string sNo = "MJ-" + gldate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(glother1, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnglmst WHERE cmpcode='" + cmp + "' AND glother1 LIKE '%" + sNo + "%'";
                glother1 = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
            }
            return Json(glother1, JsonRequestBehavior.AllowGet);
        }

        // GET: glmstMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT * FROM (";
            sSql += "SELECT glm.cmpcode, glmstoid, glother1, gldate, glflag, glother3, glm.createuser FROM QL_trnglmst glm WHERE (ISNULL(glother1, '')<>'') ";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "AND glm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "AND glm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND gldate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND glflag IN ('In Process')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND gldate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND glflag IN ('In Process')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND gldate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND glflag='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post")
                        DisplayCol = "normal";
                }
            }
            else

            {
                sSql += " AND glflag IN ('In Process')";
            }
            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND glm.createuser='" + Session["UserID"].ToString() + "'";
            //sSql += " UNION ALL ";
            //sSql += "SELECT glm.cmpcode, glmstoid, glother1, gldate, glflag, glother3, glm.createuser FROM QL_trnglmst_hist glm WHERE (ISNULL(glother1, '')<>'') ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "AND glm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "AND glm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND gldate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND glflag IN ('In Process')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND gldate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND glflag IN ('In Process')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND gldate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND gldate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND glflag='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post")
                        DisplayCol = "normal";
                }
            }
            else

            {
                sSql += " AND glflag IN ('In Process')";
            }
            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND glm.createuser='" + Session["UserID"].ToString() + "'";

            sSql += ") tblGL ";

            //sSql += " ORDER BY CONVERT(DATETIME, gldate) DESC, glmstoid DESC";

            List<trnglmst> dt = db.Database.SqlQuery<trnglmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: glmstMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnglmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnglmst();
                tbl.glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                tbl.gldate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.glflag = "In Process";
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
                Session["QL_trngldtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnglmst.Find(cmp, id);
                Session["History"] = "";
                if (tbl == null)
                {
                    tbl = db.Database.SqlQuery<QL_trnglmst>("SELECT * FROM QL_trnglmst_hist WHERE cmpcode='" + cmp + "' AND glmstoid=" + id).FirstOrDefault();
                    Session["History"] = "Y";
                }

                sSql = "SELECT * FROM (";
                sSql += "SELECT glseq, acctggrp1 AS acctggroup, gld.acctgoid, acctgcode, acctgdesc, gldbcr, (CASE gldbcr WHEN 'D' THEN glamt ELSE 0.0 END) AS glamtdb, (CASE gldbcr WHEN 'D' THEN 0.0 ELSE glamt END) AS glamtcr, glnote, ISNULL(groupoid, 0) AS groupoid, ISNULL((SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dg WHERE dg.cmpcode=gld.cmpcode AND dg.groupoid=gld.groupoid), 'None') AS groupdesc FROM QL_trngldtl gld INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.cmpcode='" + cmp + "' AND glmstoid=" + id + "";
                //sSql += " UNION ALL ";
                //sSql += "SELECT glseq, acctggrp1 AS acctggroup, gld.acctgoid, acctgcode, acctgdesc, gldbcr, (CASE gldbcr WHEN 'D' THEN glamt ELSE 0.0 END) AS glamtdb, (CASE gldbcr WHEN 'D' THEN 0.0 ELSE glamt END) AS glamtcr, glnote, ISNULL(groupoid, 0) AS groupoid, ISNULL((SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dg WHERE dg.cmpcode=gld.cmpcode AND dg.groupoid=gld.groupoid), 'None') AS groupdesc FROM QL_trngldtl_hist gld INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.cmpcode='" + cmp + "' AND glmstoid=" + id + "";
                sSql += ") tblGL ORDER BY glseq";
                var tbldtl = db.Database.SqlQuery<trngldtl>(sSql).ToList();
                Session["QL_trngldtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: glmstMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnglmst tbl, string action, string closing, decimal totaldebit, decimal totalcredit, string gl1side)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.glother1 == null)
                tbl.glother1 = "";

            List<trngldtl> dtDtl = (List<trngldtl>)Session["QL_trngldtl"];
            if (tbl.glother2 == "")
                ModelState.AddModelError("", "Please select CURRENCY field!");
            if (totaldebit > 0 && totalcredit > 0)
            {
                if (totaldebit != totalcredit)
                    ModelState.AddModelError("", "TOTAL DEBIT must be equal with TOTAL CREDIT!");
            }
            else
            {
                ModelState.AddModelError("", "Both of TOTAL DEBIT dan TOTAL CREDIT must be more than 0!");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].acctgoid == 0)
                            ModelState.AddModelError("", "Please select COA field!");
                        //if (dtDtl[i].groupoid == 0)
                        //    ModelState.AddModelError("", "Please select DIVISION field!");
                        dtDtl[i].groupoid = 0;
                    }
                }
            }

            var cRate = new ClassRate();
            DateTime sDatePost = Convert.ToDateTime("1/1/1900");
            if (tbl.glflag == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, tbl.gldate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(tbl.gldate.Month).ToString() + " " + tbl.gldate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.glflag = "In Process";
                }
                cRate.SetRateValue(Convert.ToInt32(tbl.glother2), tbl.gldate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.glflag = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.glflag = "In Process";
                }
                sDatePost = tbl.gldate;
            }
            if (!ModelState.IsValid)
                tbl.glflag = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnglmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trnglmst WHERE glmstoid=" + tbl.glmstoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trnglmst");
                        tbl.glother1 = GenerateMemoNo2(tbl.cmpcode, tbl.gldate);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.rateoid = cRate.GetRateDailyOid;
                tbl.rate2oid = cRate.GetRateMonthlyOid;
                tbl.glrateidr = cRate.GetRateDailyIDRValue;
                tbl.glrateusd = cRate.GetRateDailyUSDValue;
                tbl.glrate2idr = cRate.GetRateMonthlyIDRValue;
                tbl.glrate2usd = cRate.GetRateMonthlyUSDValue;
                tbl.type = "";
                tbl.glres = "";
                tbl.postdate = sDatePost;
                tbl.glnote = "Memo|" + tbl.glother1 + "";
                tbl.gl1side = gl1side;
                string Penanda = "";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnglmst.Find(tbl.cmpcode, tbl.glmstoid) != null)
                                tbl.glmstoid = mstoid;
                            Session["History"] = "";

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.gldate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnglmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            if (Session["History"].ToString() == "")
                            {
                                tbl.updtime = servertime;
                                tbl.upduser = Session["UserID"].ToString();
                                db.Entry(tbl).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                //sSql = "UPDATE QL_trnglmst_hist SET gldate='" + tbl.gldate + "', periodacctg='" + tbl.periodacctg + "', glnote='Memo|" + tbl.glother1 + "', glflag='" + tbl.glflag + "', postdate='" + sDatePost + "', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP, glother1='" + tbl.glother1 + "', glother2='" + tbl.glother2 + "', glother3='" + tbl.glother3 + "', rateoid=" + cRate.GetRateDailyOid + ", rate2oid=" + cRate.GetRateMonthlyOid + ", glrateidr=" + cRate.GetRateDailyIDRValue + ", glrate2idr=" + cRate.GetRateMonthlyIDRValue + ", glrateusd=" + cRate.GetRateDailyUSDValue + ", glrate2usd=" + cRate.GetRateMonthlyUSDValue + ", gl1side='" + tbl.gl1side + "' WHERE cmpcode='" + tbl.cmpcode + "' AND glmstoid=" + tbl.glmstoid;
                                //if (db.Database.ExecuteSqlCommand(sSql) > 0)
                                //{
                                //    Penanda = "Hist";
                                //    db.SaveChanges();
                                //}
                            }

                            var trndtl = db.QL_trngldtl.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trngldtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            //var trndtl_hist = db.QL_trngldtl_hist.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                            //db.QL_trngldtl_hist.RemoveRange(trndtl_hist);
                            //db.SaveChanges();
                        }

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtDtl[i].gldbcr = (dtDtl[i].glamtdb > 0 ? "D" : "C");
                            if (Penanda == "")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, dtloid++, (i + 1), tbl.glmstoid, dtDtl[i].acctgoid, dtDtl[i].gldbcr, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb : dtDtl[i].glamtcr), tbl.glother1, dtDtl[i].glnote, tbl.glflag, tbl.upduser, tbl.updtime, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyIDRValue) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyIDRValue)), (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyUSDValue) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyUSDValue)), "QL_trnglmst " + tbl.glmstoid + "", "", "", "", dtDtl[i].groupoid,tbl.divgroupoid??0));
                                db.SaveChanges();
                            }
                            else
                            {
                                //db.QL_trngldtl_hist.Add(ClassFunction.InsertGLDtlHist(tbl.cmpcode, dtloid++, (i + 1), tbl.glmstoid, dtDtl[i].acctgoid, dtDtl[i].gldbcr, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb : dtDtl[i].glamtcr), tbl.glother1, dtDtl[i].glnote, tbl.glflag, tbl.upduser, tbl.updtime, (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyIDRValue) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyIDRValue)), (dtDtl[i].gldbcr == "D" ? dtDtl[i].glamtdb * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyUSDValue) : dtDtl[i].glamtcr * (tbl.gl1side == "Y" ? (tbl.glother2 == "1" ? 1 : 0) : cRate.GetRateMonthlyUSDValue)), "QL_trnglmst " + tbl.glmstoid + "", "", "", "", dtDtl[i].groupoid));
                                //db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.glmstoid + "?cmp=" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: glmstMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnglmst tbl = db.QL_trnglmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trngldtl.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                        db.QL_trngldtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        //var trndtl_hist = db.QL_trngldtl_hist.Where(a => a.glmstoid == tbl.glmstoid && a.cmpcode == tbl.cmpcode);
                        //db.QL_trngldtl_hist.RemoveRange(trndtl_hist);
                        //db.SaveChanges();

                        db.QL_trnglmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMemoJournal.rpt"));
            report.SetParameterValue("sWhere", " WHERE glm.cmpcode='" + cmp + "' AND ISNULL(glm.glother1,'')<>'' AND glm.glmstoid IN (" + id + ")");
            report.SetParameterValue("PrintUserID", Session["UserID"]);
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "MemorialJournalPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}