﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ShipmentRawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class shipmentrawmst
        {
            public string cmpcode { get; set; }
            public int shipmentrawmstoid { get; set; }
            public string divgroup { get; set; }
            public string shipmentrawno { get; set; }
            public DateTime shipmentrawdate { get; set; }
            public string custname { get; set; }
            public string shipmentrawcontno { get; set; }
            public string shipmentrawmststatus { get; set; }
            public string shipmentrawmstnote { get; set; }
            public string divname { get; set; }
        }

        public class shipmentrawdtl
        {
            public int shipmentrawdtlseq { get; set; }
            public int sorawmstoid { get; set; }
            public int dorawmstoid { get; set; }
            public int dorawdtloid { get; set; }
            public string dorawno { get; set;}
            public string dorawdate { get; set; }
            public int shipmentrawwhoid { get; set; }
            public string shipmentrawwh { get; set; }
            public int sorawdtloid { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public int dorawunitoid { get; set; }
            public string dorawunit { get; set; }
            public decimal stockqty { get; set; }
            public decimal dorawqty { get; set; }
            public decimal shipmentrawqty { get; set; }
            public int shipmentrawunitoid { get; set; }
            public string shipmentrawunit { get; set; }
            public string shipmentrawdtlnote { get; set; }
            public decimal shipmentrawvalueidr { get; set; }
            public decimal shipmentrawvalueusd { get; set; }
            public decimal rawlimitqty { get; set; }
            public string sn { get; set; }
            public DateTime expdate { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custtype { get; set; }
            public string custaddr { get; set; }
            public string sorawcustpono { get; set; }
        }

        public class listalamat
        {
            public int custdtloid { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
            public int custdtlcityoid { get; set; }
            public string custdtlcity { get; set; }
        }

        public class warehouse
        {
            public int shipmentrawwhoid { get; set; }
            public string shipmentrawwh { get; set; }
        }

        private string generateNo(string cmp)
        {
            var sNo = "SJFG-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            var DefaultFormatCounter = 4;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentrawno, " + DefaultFormatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentrawmst WHERE cmpcode='" + cmp + "' AND shipmentrawno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        private void InitDDL(QL_trnshipmentrawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;



            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0";
            var shipmentrawwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.shipmentrawwhoid = shipmentrawwhoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0"; //AND genother1='" + cmp +"'
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT DISTINCT cust.custoid, cust.custcode, cust.custname, cust.custtype, cust.custaddr, '' sorawcustpono FROM QL_mstcust cust INNER JOIN QL_trnsorawmst drm ON cust.cmpcode=drm.cmpcode AND cust.custoid=drm.custoid WHERE cust.cmpcode='" + CompnyCode + "' AND cust.divgroupoid=" + divgroupoid + " AND cust.custoid IN (SELECT DISTINCT custoid FROM QL_trndorawmst WHERE cmpcode='" + cmp + "' AND dorawmststatus='Post') ORDER BY custname";

            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtloid, custdtlname, custdtladdr , custdtlcityoid , g.gendesc custdtlcity FROM QL_mstcustdtl c Left JOIN QL_mstgen g ON g.genoid=c.custdtlcityoid WHERE c.custoid = " + custoid + " ORDER BY custdtloid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDOData(string cmp, int custoid, string action, int shipmentrawmstoid, int divgroupoid)
        {
            List<QL_trndorawmst> tbl = new List<QL_trndorawmst>();
            sSql = "SELECT * FROM QL_trndorawmst dom WHERE dom.cmpcode='" + cmp + "' AND custoid=" + custoid;
            if (action == "Update Data")
            {
                sSql += " AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + shipmentrawmstoid + ")";
            } else
            {
                sSql += " AND dorawmststatus='Post'";
            }
            sSql += " AND dom.divgroupoid=" + divgroupoid + " ORDER BY dorawmstoid";

            tbl = db.Database.SqlQuery<QL_trndorawmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int dorawmstoid, int shipmentrawwhoid, int shipmentrawmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<shipmentrawdtl> tbl = new List<shipmentrawdtl>();

            sSql = "SELECT som.dorawno, CONVERT(VARCHAR(10), som.dorawdate, 101) AS dorawdate, sod.dorawmstoid, sod.dorawdtloid, sod.dorawdtlseq, sod.sorawdtloid matrawoid, 'SHEET' matrawcode, sodtldesc matrawlongdesc, " +
                "ISNULL(con.qty, 0.0) AS stockqty, " +
                "(sod.dorawqty - ISNULL((SELECT SUM(sd.shipmentrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentrawmstoid = sd.shipmentrawmstoid WHERE sd.cmpcode = sod.cmpcode AND sm.shipmentrawmststatus <> 'Rejected' AND sd.dorawdtloid = sod.sorawdtloid AND sd.shipmentrawmstoid <> " + shipmentrawmstoid + "), 0.0)) AS dorawqty, " +
                "0.0 AS shipmentrawqty, sod.dorawunitoid [shipmentrawunitoid], g.gendesc[shipmentrawunit], ISNULL(sod.dorawdtlnote, '') [shipmentrawdtlnote], 1.0 rawlimitqty, con.sn " +
                "FROM QL_trndorawdtl sod " +
                "INNER JOIN QL_trndorawmst som ON som.cmpcode=sod.cmpcode AND som.dorawmstoid = sod.dorawmstoid inner join ql_trnsorawdtl sd on sod.sorawdtloid=sd.sorawdtloid " +
                "INNER JOIN QL_mstgen g ON g.cmpcode=sod.cmpcode AND g.genoid = sod.dorawunitoid " +
                "INNER JOIN (SELECT refoid, refname, refno sn, SUM(qtyin-qtyout) qty FROM QL_conmat WHERE cmpcode='" + cmp + "' AND refname='SHEET' AND mtrwhoid = "+  shipmentrawwhoid+" GROUP BY refoid, refname, refno  HAVING sum(qtyin-qtyout) > 0) con ON con.refoid=sod.matrawoid " +
                "WHERE sod.cmpcode = '" + cmp + "' AND sod.dorawmstoid = " + dorawmstoid;
            //if (action == "New Data") sSql += " AND sod.sorawdtlstatus=''";
            sSql += " ORDER BY dorawdtlseq ASC";

            decimal dSisa = 0;
            tbl = db.Database.SqlQuery<shipmentrawdtl>(sSql).ToList();
            if (tbl.Count > 0)
            {
                var a = tbl.GroupBy(x => x.matrawoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentrawqty), sorawqty = x.Select(y => y.dorawqty) }).ToList();

                for (var i = 0; i < tbl.Count(); i++)
                {
                    var tbldtl = tbl.Where(d => d.matrawoid == tbl[i].matrawoid).ToList();
                    for (var j = 0; j < tbldtl.Count(); j++)
                    {
                        if (dSisa == 0)
                            dSisa = tbl[i].dorawqty;

                        if (dSisa > tbl[i].stockqty)
                        {
                            tbl[i].shipmentrawqty = tbl[i].stockqty;
                            dSisa += tbl[i].dorawqty - tbl[i].stockqty;
                        }
                        else
                        {
                            tbl[i].shipmentrawqty = dSisa;
                        }
                    }                    
                }
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentrawdtl> dtDtl)
        {
            Session["QL_trnshipmentrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(int oid)
        {
            var result = "";
            JsonResult js = null;
            List<shipmentrawdtl> dtl = new List<shipmentrawdtl>();

            try
            {
                sSql = "SELECT sd.shipmentrawdtlseq, sd.dorawmstoid, dom.dorawno, CONVERT(VARCHAR(10), dom.dorawdate, 101) AS dorawdate, sd.shipmentrawwhoid, g1.gendesc AS shipmentrawwh, sd.dorawdtloid, sod.sorawdtloid matrawoid, 'Sheet' matrawcode, sodtldesc matrawlongdesc, 0.0 rawlimitqty, " +
                    "ISNULL((SELECT SUM(qtyin-qtyout) qty FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refoid=sd.matrawoid AND refname='FINISH GOOD' GROUP BY refoid), 0.0) AS stockqty, " +
                    "(dod.dorawqty - ISNULL((SELECT SUM(sdx.shipmentrawqty) FROM QL_trnshipmentrawdtl sdx INNER JOIN QL_trnshipmentrawmst smx ON smx.cmpcode = sdx.cmpcode AND smx.shipmentrawmstoid = sdx.shipmentrawmstoid WHERE sdx.cmpcode = sd.cmpcode AND smx.shipmentrawmststatus <> 'Rejected' AND sdx.dorawdtloid = sd.dorawdtloid AND sdx.shipmentrawmstoid <> sd.shipmentrawmstoid), 0.0)) AS dorawqty, " +
                    "sd.shipmentrawqty, sd.shipmentrawunitoid, g2.gendesc AS shipmentrawunit, sd.shipmentrawdtlnote, sd.sn " +
                    "FROM QL_trnshipmentrawdtl sd " +
                    "INNER JOIN QL_trndorawmst dom ON dom.cmpcode = sd.cmpcode AND dom.dorawmstoid = sd.dorawmstoid " +
                    "INNER JOIN QL_trndorawdtl dod ON dod.cmpcode = sd.cmpcode AND dod.dorawdtloid = sd.dorawdtloid " +
                    " Inner Join ql_trnsorawdtl sod on sod.sorawdtloid = dod.sorawdtloid " +
                    "INNER JOIN QL_mstgen g1 ON g1.genoid = sd.shipmentrawwhoid " +
                    "INNER JOIN QL_mstgen g2 ON g2.genoid = sd.shipmentrawunitoid " +
                    "WHERE sd.shipmentrawmstoid =" + oid + " ORDER BY sd.shipmentrawdtlseq";
                dtl = db.Database.SqlQuery<shipmentrawdtl>(sSql).ToList();
                Session["QL_trnshipmentrawdtl"] = dtl;
            }
            catch(Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private void FillAdditionalField(QL_trnshipmentrawmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnshipmentrawmst WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + "").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtladdr FROM QL_mstcustdtl a WHERE a.custdtloid ='" + tbl.custdtloid + "'").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: shipmentrawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dom", divgroupoid, "divgroupoid");
            sSql = "SELECT som.cmpcode, (select gendesc from ql_mstgen where genoid=som.divgroupoid) divgroup, som.shipmentrawmstoid, som.shipmentrawno, som.shipmentrawdate, c.custname, som.shipmentrawmststatus, som.shipmentrawmstnote, div.divname FROM QL_TRNshipmentrawMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND shipmentrawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND shipmentrawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND shipmentrawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND shipmentrawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND shipmentrawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND shipmentrawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY som.shipmentrawdate DESC, som.shipmentrawmstoid DESC";

            List<shipmentrawmst> dt = db.Database.SqlQuery<shipmentrawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnshipmentrawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: shipmentrawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnshipmentrawmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnshipmentrawmst();
                tbl.cmpcode = CompnyCode;
                tbl.shipmentrawmstoid = ClassFunction.GenerateID("QL_trnshipmentrawmst");
                tbl.shipmentrawdate = ClassFunction.GetServerTime();
                tbl.shipmentrawetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.shipmentraweta = ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";

                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.shipmentrawmststatus = "In Process";
                
                Session["QL_trnshipmentrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnshipmentrawmst.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentrawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentrawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.shipmentrawno == null)
                tbl.shipmentrawno = "";

            List<shipmentrawdtl> dtDtl = (List<shipmentrawdtl>)Session["QL_trnshipmentrawdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (Convert.ToDateTime(tbl.shipmentrawetd.ToString("MM/dd/yyyy")) > Convert.ToDateTime(tbl.shipmentraweta.ToString("MM/dd/yyyy")))
            {
                ModelState.AddModelError("", "ETD must be less than ETA!");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].shipmentrawqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY must be more than 0!");
                        }
                        else
                        {
                            var totalqty = dtDtl.Where(x => x.matrawoid == dtDtl[i].matrawoid && x.shipmentrawwhoid == dtDtl[i].shipmentrawwhoid && x.dorawdtloid==dtDtl[i].dorawdtloid).Sum(x => x.shipmentrawqty);
                            if (totalqty > dtDtl[i].dorawqty)
                            {
                                ModelState.AddModelError("", "QTY FG"+dtDtl[i].matrawlongdesc +" Batch. "+ dtDtl[i].sn +" must be less than DO QTY!");
                            }
                           
                        }

                        sSql = "SELECT (dorawqty - ISNULL((SELECT SUM(shipmentrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentrawmstoid = sd.shipmentrawmstoid WHERE sd.cmpcode = dod.cmpcode AND shipmentrawmststatus<> 'Rejected' AND sd.dorawdtloid = dod.sorawdtloid AND sd.shipmentrawmstoid <> " + tbl.shipmentrawmstoid + "), 0.0)) AS dorawqty FROM QL_trndorawdtl dod WHERE dod.cmpcode = '" + tbl.cmpcode + "' AND dorawdtloid = " + dtDtl[i].dorawdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].dorawqty)
                            dtDtl[i].dorawqty = dQty;
                        if (dQty < dtDtl[i].shipmentrawqty)
                            ModelState.AddModelError("", "Some DO Qty has been updated by another user. Please check that every Qty must be less than DO Qty!");

                        if (tbl.shipmentrawmststatus == "Post")
                        {
                            if (!ClassFunction.IsStockAvailableWithBatch(tbl.cmpcode, ClassFunction.GetServerTime(), dtDtl[i].matrawoid, dtDtl[i].shipmentrawwhoid, dtDtl[i].shipmentrawqty, "SHEET", dtDtl[i].sn))
                                ModelState.AddModelError("", "Every Qty must be less than Stock Qty before Post this data!");

                            dtDtl[i].shipmentrawvalueidr = ClassFunction.GetStockValueWithBatch(CompnyCode, "FINISH GOOD", dtDtl[i].matrawoid, dtDtl[i].sn);
                        }
                        else
                        {
                            dtDtl[i].shipmentrawvalueidr = 0;
                            dtDtl[i].shipmentrawvalueusd = 0;
                        }
                    }
                }
            }

            // VAR UTK APPROVAL
            /*var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.shipmentrawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentrawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }*/
            // END VAR UTK APPROVAL

            if (tbl.shipmentrawmststatus == "Revised")
                tbl.shipmentrawmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.shipmentrawmststatus = "In Process";

            if (tbl.shipmentrawmststatus == "Post")
            {
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode, tbl.divgroupoid.Value))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", CompnyCode, tbl.divgroupoid.Value))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_RM"));                
            }                

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentrawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentrawdtl");
                var iConOid = ClassFunction.GenerateID("QL_conmat");
                var iStockValOid = ClassFunction.GenerateID("QL_stockvalue");
                var iGlMstOid = ClassFunction.GenerateID("QL_trnglmst");
                var iGlDtlOid = ClassFunction.GenerateID("QL_trngldtl");
                var iDelAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode, tbl.divgroupoid.Value));
                var iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", CompnyCode, tbl.divgroupoid.Value));
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;
                        

                        if (tbl.shipmentrawmststatus == "Post")
                            tbl.shipmentrawno = generateNo(CompnyCode);


                        if (action == "New Data")
                        {
                            if (db.QL_trnshipmentrawmst.Find(tbl.cmpcode, tbl.shipmentrawmstoid) != null)
                                tbl.shipmentrawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentrawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnshipmentrawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.shipmentrawmstoid + " WHERE tablename='QL_trnshipmentrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndorawmst SET dorawmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentrawdtl.Where(a => a.shipmentrawmstoid == tbl.shipmentrawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnshipmentrawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.shipmentrawdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.shipmentrawmstoid = tbl.shipmentrawmstoid;
                            tbldtl.shipmentrawdtlseq = i + 1;
                            tbldtl.dorawmstoid = dtDtl[i].dorawmstoid;
                            tbldtl.dorawdtloid = dtDtl[i].dorawdtloid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.shipmentrawqty = dtDtl[i].shipmentrawqty;
                            tbldtl.shipmentrawunitoid = dtDtl[i].shipmentrawunitoid;
                            tbldtl.shipmentrawwhoid = dtDtl[i].shipmentrawwhoid;
                            tbldtl.shipmentrawdtlstatus = "";
                            tbldtl.shipmentrawdtlnote = (dtDtl[i].shipmentrawdtlnote == null ? "" : dtDtl[i].shipmentrawdtlnote);
                            tbldtl.sn = dtDtl[i].sn;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentrawdtlres3 = "";
                            tbldtl.shipmentrawvalueidr = 0;
                            tbldtl.shipmentrawvalueusd = 0;

                            //tbldtl.custpo = "";
                            //tbldtl.containerno = "";
                            //tbldtl.mattype = "";
                            //tbldtl.shipmentextname = "";
                            //tbldtl.dotype = "";

                            db.QL_trnshipmentrawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.shipmentrawmststatus == "Post")
                            {

                                if (dtDtl[i].dorawqty > dtDtl[i].shipmentrawqty)
                                {
                                    sSql = "update ql_trnsorawdtl set sorawdtlstatus='' where sorawdtloid= (select sorawdtloid from ql_trndorawdtl where dorawdtloid=" + dtDtl[i].dorawdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "update ql_trnsorawmst set sorawmststatus='Approved' where sorawmstoid= (select distinct sorawmstoid from ql_trndorawdtl where dorawdtloid=" + dtDtl[i].dorawdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                }
                            }


                            sSql = "UPDATE QL_trndorawdtl SET dorawdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND dorawmstoid=" + dtDtl[i].dorawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndorawmst SET dorawmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND dorawmstoid=" + dtDtl[i].dorawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.shipmentrawmststatus == "Post")
                            {
                                sSql = "SELECT ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refoid=" + dtDtl[i].matrawoid + " AND refname = 'SHEET' AND refno = '" + dtDtl[i].sn + "'),0.0) ";
                                var stockAkhir = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                                //Insert conmat
                                int dodtl_id = dtDtl[i].dorawdtloid;
                                int so_id = db.QL_trndorawdtl.FirstOrDefault(x => x.dorawdtloid == dodtl_id)?.sorawmstoid ?? 0;
                                int wo_id = db.QL_trnwomst.FirstOrDefault(x => x.somstoid == so_id && x.sotype == "QL_trnsorawmst")?.womstoid ?? 0;
                                db.QL_conmat.Add(ClassFunction.InsertConMatWithBatch(CompnyCode, iConOid++, "SJSH", "QL_trnshipmentrawdtl", tbl.shipmentrawmstoid, dtDtl[i].matrawoid, "SHEET", dtDtl[i].shipmentrawwhoid, (dtDtl[i].shipmentrawqty * -1), "Shipment SHEET", "SJSH | No. "+ tbl.shipmentrawno + " | Note. "+ dtDtl[i].shipmentrawdtlnote +"", Session["UserID"].ToString(), dtDtl[i].sn, dtDtl[i].shipmentrawvalueidr, dtDtl[i].shipmentrawvalueusd, wo_id, null, tbldtl.shipmentrawdtloid,tbl.divgroupoid??0, dtDtl[i].sn, dtDtl[i].expdate.ToString(), cust_id: tbl.custoid));


                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(dtDtl[i].shipmentrawqty * -1, dtDtl[i].shipmentrawvalueidr, dtDtl[i].shipmentrawvalueusd, "QL_trnshipmentrawdtl", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtDtl[i].matrawoid, "SHEET", dtDtl[i].sn);
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(dtDtl[i].shipmentrawqty * -1, dtDtl[i].shipmentrawvalueidr, dtDtl[i].shipmentrawvalueusd, "QL_trnshipmentrawdtl", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtDtl[i].matrawoid, "SHEET", iStockValOid++, dtDtl[i].sn, dtDtl[i].expdate.ToString(), stockAkhir);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                        }

                        if (tbl.shipmentrawmststatus == "Post")
                        {
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, iGlMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Shipment FG|No. " + tbl.shipmentrawno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 1, 1, 1, 1, 1, 1,tbl.divgroupoid??0));
                            db.SaveChanges();

                            decimal glamt = dtDtl.Sum(x => x.shipmentrawqty * x.shipmentrawvalueidr);
                            decimal glamtidr = dtDtl.Sum(x => x.shipmentrawqty * x.shipmentrawvalueidr);
                            decimal glamtusd = dtDtl.Sum(x => x.shipmentrawqty * x.shipmentrawvalueusd);
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid++, glseq++, iGlMstOid, iDelAcctgOid, "D", glamt, tbl.shipmentrawno, "Shipment SHEET|No. " + tbl.shipmentrawno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnshipmentrawmst " + tbl.shipmentrawmstoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid++, glseq++, iGlMstOid, iStockAcctgOid, "C", glamt, tbl.shipmentrawno, "Shipment SHEET|No. " + tbl.shipmentrawno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnshipmentrawmst " + tbl.shipmentrawmstoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iGlMstOid++;

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iConOid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + iGlMstOid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlDtlOid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }                        

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnshipmentrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        

                        // INSERT APPROVAL
                        /*if (tbl.shipmentrawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SHP-RM" + tbl.shipmentrawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnshipmentrawmst";
                                tblApp.oid = tbl.shipmentrawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }*/
                        // END INSERT APPROVAL

                        objTrans.Commit();

                        using (var objTrans2 = db.Database.BeginTransaction())
                        {
                            try
                            {
                                if (tbl.shipmentrawmststatus == "Post")
                                {
                                    sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND sorawmstoid IN (SELECT sorawmstoid FROM QL_trndorawdtl WHERE cmpcode='" + CompnyCode + "' AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + ") AND dorawdtloid NOT IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trndorawdtl WHERE cmpcode='" + CompnyCode + "' AND dorawmstoid IN (SELECT dorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + ") AND dorawdtloid NOT IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentrawmstoid=" + tbl.shipmentrawmstoid + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    objTrans2.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                objTrans2.Rollback();
                                tbl.shipmentrawmststatus = "In Process";
                                return View(ex.ToString());
                            }
                        }

                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.shipmentrawmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentrawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentrawmst tbl = db.QL_trnshipmentrawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + cmp + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + cmp + "' AND sorawmstoid IN (SELECT sorawmstoid FROM QL_trnshipmentrawdtl WHERE cmpcode='" + cmp + "' AND shipmentrawmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnshipmentrawdtl.Where(a => a.shipmentrawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnshipmentrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnshipmentrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnshipmentrawmst.Find(cmp, id);
            if (tbl == null)
                return null;
            var sReportName = "";
            if (printtype == "A4")
                sReportName = "rptShipment_Trn.rpt";
            else
                sReportName = "rptShipment_TrnA5.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            sSql = "SELECT sm.shipmentrawmstoid AS [MstOid], CONVERT(VARCHAR(10), sm.shipmentrawmstoid) AS [Draft No.], sm.shipmentrawno AS [Shipment No.], sm.shipmentrawdate AS [Shipment Date], sm.shipmentrawmstnote AS [Header Note], sm.shipmentrawmststatus AS [Status], [Customer] AS [Customer], sd.shipmentrawdtloid AS [DtlOid], sd.shipmentrawdtlseq AS [No.], m.sorawdtloid AS [MatOid], 'Sheet' AS [Code], m.sodtldesc AS [Material], sd.shipmentrawqty AS [Qty], g2.gendesc AS [Unit], g3.gendesc AS [Warehouse], sd.shipmentrawdtlnote AS [Note], sm.cmpcode, [BU Name], [BU Address], [BU City], [BU Province], [BU Country], [BU Telp. 1], [BU Telp. 2], [BU Fax 1], [BU Fax 2], [BU Email], [BU Post Code], shipmentraweta [ETA], shipmentrawetd [ETD], shipmentrawcontno [Container No.], shipmentrawportship [Port Ship], shipmentrawportdischarge [Port Discharge], shipmentrawnopol [Vehicle Police No.], [Customer Type], [Customer], [Customer Address], [Customer City],[Customer Province], [Customer Country], [Customer Phone1], [Customer Phone2], [Customer Phone3], [Customer Fax], [Customer Website], [Customer CP1], [Customer CP2], [Customer CPPhone1], [Customer CPPhone2], [Customer CPEmail1], [Customer CPEmail2], sd.sn AS [BATCH NO] FROM QL_trnshipmentrawmst sm  INNER JOIN (SELECT custoid, custcode [Customer Code],custtype [Customer Type],custname [Customer],custaddr [Customer Address],custcityoid,g.gendesc [Customer City],g2.gendesc [Customer Province] ,g3.gendesc [Customer Country],custphone1 [Customer Phone1],custphone2 [Customer Phone2],custphone3 [Customer Phone3],custfax1 [Customer Fax],custwebsite [Customer Website],custcp1name [Customer CP1],custcp2name [Customer CP2],custcp1phone [Customer CPPhone1],custcp2phone [Customer CPPhone2],custcp1email [Customer CPEmail1],custcp2email [Customer CPEmail2] FROM QL_mstcust c  " +
            "INNER JOIN QL_mstgen g ON g.genoid = custcityoid AND g.gengroup = 'City' " +
            "INNER JOIN QL_mstgen g2 ON g2.cmpcode = g.cmpcode AND g2.genoid = CONVERT(INT, g.genother1) " +
            "INNER JOIN QL_mstgen g3 ON g3.cmpcode = g.cmpcode AND g3.genoid = CONVERT(INT, g.genother2))AS TblCust ON TblCust.custoid = sm.custoid " +
            "INNER JOIN QL_trnshipmentrawdtl sd ON sd.cmpcode = sm.cmpcode AND sd.shipmentrawmstoid = sm.shipmentrawmstoid " +
            "INNER JOIN QL_trnsorawdtl m ON m.sorawdtloid = sd.matrawoid " +
            "INNER JOIN QL_mstgen g2 ON g2.genoid = sd.shipmentrawunitoid " +
            "INNER JOIN QL_mstgen g3 ON g3.genoid = sd.shipmentrawwhoid " +
            "INNER JOIN (SELECT di.cmpcode, divname AS[BU Name], divaddress AS[BU Address], g1.gendesc AS[BU City], g2.gendesc AS[BU Province], g3.gendesc AS[BU Country], ISNULL(divphone, '') AS[BU Telp. 1], ISNULL(divphone2, '') AS[BU Telp. 2], ISNULL(divfax1, '') AS[BU Fax 1], ISNULL(divfax2, '') AS[BU Fax 2], ISNULL(divemail, '') AS[BU Email], ISNULL(divpostcode, '') AS[BU Post Code]  FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid = divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode = g1.cmpcode AND g2.genoid = CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode = g1.cmpcode AND g3.genoid = CONVERT(INT, g1.genother2)  " +
            ") tblDiv ON tblDiv.cmpcode = sm.cmpcode " +
            "WHERE sm.cmpcode = '" + cmp + "' AND sm.shipmentrawmstoid = "+ id +" ORDER BY sm.shipmentrawmstoid, sd.shipmentrawdtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptShipmentFG");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("Header", "PRINT OUT SURAT JALAN");
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ShipmentrawMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}