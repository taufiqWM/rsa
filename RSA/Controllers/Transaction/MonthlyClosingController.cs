﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.Transaction
{
    public class MonthlyClosingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnmonthly
        {
            public string cmpcode { get; set; }
            public int seq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal amtopenidr { get; set; }
            public decimal amtdebetidr { get; set; }
            public decimal amtcreditidr { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtopenusd { get; set; }
            public decimal amtdebetusd { get; set; }
            public decimal amtcreditusd { get; set; }
            public decimal amtbalanceusd { get; set; }
            public string acctggrp1 { get; set; }
            public decimal ratetoidr { get; set; }
            public decimal selisihamt { get; set; }
        }

        public class trnmonthly2
        {
            public int seq { get; set; }
            public int jml { get; set; }
            public string tipe { get; set; }
        }

        public class listyear
        {
            public int iyear { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "divcode", "divname", null);
            ViewBag.cmpcode = cmpcode;

            var lMonth = new List<SelectListItem>();
            //List<int> iMonth = new List<int>();

            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            for (var i = 1; i <= 12; i++)
            {
                lMonth.Add(new SelectListItem() { Value = i.ToString(), Text = mfi.GetMonthName(i).ToString() });
            }
            ViewBag.iMonth = lMonth;

            //var endYear = (DateTime.Now.Year) + 1;
            //var startYear = (DateTime.Now.Year) - 5; ;

            //var lYear = new List<SelectListItem>();
            //for (var i = startYear; i <= endYear; i++)
            //{
            //    lYear.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString() });
            //}
            //ViewBag.iYear = lYear;
            var servertime = ClassFunction.GetServerTime().ToString("yyyy");
            sSql = "SELECT DISTINCT YEAR(gldate) iyear FROM QL_trnglmst";
            var iYear = new SelectList(db.Database.SqlQuery<listyear>(sSql).ToList(), "iyear", "iyear", int.Parse(servertime));
            ViewBag.iYear = iYear;
        }

        [HttpPost]
        public ActionResult BindClosingData(int iYear, int iMonth)
        {
            string sMsg = ""; string BindClosingData = "";
            List<trnmonthly> tbl = new List<trnmonthly>();
            List<trnmonthly2> tbl2 = new List<trnmonthly2>();

            sMsg += PostAssetDepreciation(CompnyCode, iYear, iMonth);

            var history = db.Database.SqlQuery<string>("SELECT ISNULL(closingperiod, '') FROM QL_acctgclosinghistory WHERE cmpcode='" + CompnyCode + "' AND historyclosingoid=(SELECT MAX(historyclosingoid) FROM QL_acctgclosinghistory WHERE cmpcode='" + CompnyCode + "' AND closinggroup='MONTHLY' AND closingstatus='CLOSED')").FirstOrDefault();

            int lastcloseyear = 0;
            int selectedyear = iYear;
            int lastclosemonth = 0;
            int selectedmonth = iMonth;
            var lastclosing = "";
            var nextclosing = "";
            int nextclosemonth = 0;
            int nextcloseyear = 0;
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();

            if (!string.IsNullOrEmpty(history))
            {
                lastcloseyear = int.Parse(ClassFunction.Left(history, 4));
                lastclosemonth = int.Parse(ClassFunction.Right(history, 2));
                lastclosing = mfi.GetMonthName((lastclosemonth)).ToString() + " " + lastcloseyear;

                if (lastclosemonth == 12)
                {
                    selectedmonth = 1;
                    selectedyear = lastcloseyear + 1;
                    nextclosemonth = 1;
                    nextcloseyear = lastcloseyear + 1;
                }
                else
                {
                    selectedmonth = lastclosemonth + 1;
                    selectedyear = lastcloseyear;
                    nextclosemonth = lastclosemonth + 1;
                    nextcloseyear = lastcloseyear;
                }
                nextclosing = mfi.GetMonthName((nextclosemonth)).ToString() + " " + nextcloseyear;
            }
            else
            {
                var sLastClose = db.Database.SqlQuery<string>("SELECT ISNULL(MIN(periodacctg), '') FROM QL_crdgl WHERE cmpcode='" + CompnyCode + "' AND crdglflag='OPEN'").FirstOrDefault();
                lastcloseyear = 0; lastclosemonth = 0; lastclosing = "-";

                if (sLastClose != "")
                {
                    nextclosemonth = int.Parse(ClassFunction.Right(sLastClose, 2));
                    nextcloseyear = int.Parse(ClassFunction.Left(sLastClose, 4));
                    nextclosing = mfi.GetMonthName((nextclosemonth)).ToString() + " " + nextcloseyear; ;
                    selectedmonth = int.Parse(ClassFunction.Right(sLastClose, 2));
                    selectedyear = int.Parse(ClassFunction.Left(sLastClose, 4));
                }
                else
                {
                    lastcloseyear = 0; lastclosemonth = 0; lastclosing = "-";
                    nextclosemonth = 0; nextcloseyear = 0; nextclosing = "-";
                    selectedmonth = iMonth;
                    selectedyear = iYear;
                }
            }

            //Action Result
            if (string.IsNullOrEmpty(sMsg))
            {
                var tempDate = new DateTime(selectedyear, selectedmonth, 1);
                sSql = "SELECT TOP 1 ISNULL(crdglflag, '') AS t FROM QL_crdgl WHERE cmpcode='" + CompnyCode + "' AND periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear, iMonth, 1)) + "'";
                BindClosingData = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                if (string.IsNullOrEmpty(BindClosingData))
                {
                    BindClosingData = "OPEN";
                }

                if (BindClosingData.ToUpper() == "OPEN")
                {
                    sSql = "DECLARE @cmpcode VARCHAR(10);" +
                    "DECLARE @periode VARCHAR(10);" +
                    "DECLARE @month INTEGER;" +
                    "DECLARE @year INTEGER;" +
                    "SET @cmpcode='" + CompnyCode + "';" +
                    "SET @periode='" + ClassFunction.GetDateToPeriodAcctg(tempDate) + "';" +
                    "SET @month=" + iMonth + ";" +
                    "SET @year=" + iYear + ";" +
                    "SELECT @cmpcode cmpcode, acctgoid, acctgcode, acctgdesc, amtopenidr, SUM(debetidr) AS amtdebetidr, SUM(creditidr) AS amtcreditidr, (amtopenidr + SUM(debetidr) - SUM(creditidr)) AS amtbalanceidr, amtopenusd, SUM(debetusd) AS amtdebetusd, SUM(creditusd) AS amtcreditusd, (amtopenusd + SUM(debetusd) - SUM(creditusd)) AS amtbalanceusd, seq, acctggrp1 FROM (" +
                    "SELECT a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0) AS amtopenidr, ISNULL(crd.amtopenusd, 0) AS amtopenusd, 'debetidr'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'creditidr'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'debetusd'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN (CASE WHEN gd.glamt<>gd.glamtidr THEN gd.glamt ELSE 0.0 END) ELSE 0 END), 'creditusd'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN (CASE WHEN gd.glamt<>gd.glamtidr THEN gd.glamt ELSE 0.0 END) ELSE 0 END), 0.00 AS amtbalanceidr, 0.00 AS amtbalanceusd, 0 AS seq, a1.acctggrp1 " +
                    "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON crd.cmpcode=@cmpcode AND crd.acctgoid=a1.acctgoid AND crd.periodacctg=@periode LEFT JOIN QL_trngldtl gd ON gd.cmpcode=@cmpcode AND a1.acctgoid=gd.acctgoid AND gd.glflag='Post' AND gd.glmstoid>0 LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' AND gm.glmstoid>0 " +
                    "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode)" +
                    ") AS crd GROUP BY acctgoid, acctgcode, acctgdesc, amtopenidr, amtopenusd, seq, acctggrp1 ORDER BY acctgcode";
                }
                else if (BindClosingData.ToUpper() == "CLOSED")
                {
                    sSql = "SELECT '" + CompnyCode + "' cmpcode, a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0.0) amtopenidr, ISNULL(crd.amtdebit, 0.0) amtdebetidr, ISNULL(crd.amtcredit, 0.0) amtcreditidr, ISNULL(crd.amtbalance, 0.0) amtbalanceidr, ISNULL(crd.amtopenusd, 0.0) amtopenusd, ISNULL(crd.amtdebitusd, 0.0) amtdebetusd, ISNULL(crd.amtcreditusd, 0.0) amtcreditusd, ISNULL(crd.amtbalanceusd, 0.0) amtbalanceusd, 0 seq, a1.acctggrp1 " +
                    "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON a1.acctgoid=crd.acctgoid AND crd.cmpcode='" + CompnyCode + "' AND crd.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(tempDate) + "' " +
                    "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode) ORDER BY a1.acctgcode";
                }
                tbl = db.Database.SqlQuery<trnmonthly>(sSql).ToList();
                Session["queryclosing"] = sSql;

                //VAR Utk LABA/RUGI Bulanan, Tahunan, Ditahan
                string varLabaBerjalan = ClassFunction.GetVarInterface("VAR_LR_MONTH", CompnyCode);
                int iLabaBerjalanOid = 0;
                if (!string.IsNullOrEmpty(varLabaBerjalan))
                {
                    iLabaBerjalanOid = ClassFunction.GetAcctgOID(varLabaBerjalan);
                }

                string varLabaTahunBerjalan = ClassFunction.GetVarInterface("VAR_LR_YEAR", CompnyCode);
                int iLabaTahunBerjalanOid = 0;
                if (!string.IsNullOrEmpty(varLabaTahunBerjalan))
                {
                    iLabaTahunBerjalanOid = ClassFunction.GetAcctgOID(varLabaTahunBerjalan);
                }

                string varLabaDitahan = ClassFunction.GetVarInterface("VAR_LR_ONHAND", CompnyCode);
                int iLabaDitahan = 0;
                if (!string.IsNullOrEmpty(varLabaDitahan))
                {
                    iLabaDitahan = ClassFunction.GetAcctgOID(varLabaDitahan);
                }

                //Untuk laba (rugi) berjalan
                decimal totPendapatanIDR = 0; decimal totPendapatanUSD = 0;
                //decimal totBiayaIDR = 0; decimal totBiayaUSD = 0;
                decimal totPendapatanIDR2 = 0; decimal totPendapatanUSD2 = 0;
                //decimal totBiayaIDR2 = 0; decimal totBiayaUSD2 = 0;

                if (tbl != null)
                {
                    if (tbl.Count() > 0)
                    {
                        for (int i = 0; i < tbl.Count(); i++)
                        {
                            if (tbl[i].acctggrp1.ToUpper() == "PENDAPATAN USAHA" || tbl[i].acctggrp1.ToUpper() == "HARGA POKOK" || tbl[i].acctggrp1.ToUpper() == "BIAYA USAHA" || tbl[i].acctggrp1.ToUpper() == "PENDAPATAN DAN BIAYA LAIN-LAIN")
                            {
                                totPendapatanIDR += (tbl[i].amtdebetidr - tbl[i].amtcreditidr);
                                totPendapatanUSD += (tbl[i].amtdebetusd - tbl[i].amtcreditusd);
                                //totBiayaIDR += (tbl[i].amtdebetidr - tbl[i].amtcreditidr);
                                //totBiayaUSD += (tbl[i].amtdebetusd - tbl[i].amtcreditusd);
                                totPendapatanIDR2 += tbl[i].amtopenidr;
                                totPendapatanUSD2 += tbl[i].amtopenusd;
                                //totBiayaIDR2 += tbl[i].amtopenidr;
                                //totBiayaUSD2 += tbl[i].amtopenusd;
                            }
                        }

                        //Membalik Pendapatan 
                        //totPendapatanIDR *= -1; totPendapatanUSD *= -1;
                        //totPendapatanIDR2 *= -1; totPendapatanUSD2 *= -1;

                        for (int i = 0; i < tbl.Count(); i++)
                        {
                            if (iLabaBerjalanOid != 0)
                            {
                                if (tbl[i].acctgoid == iLabaBerjalanOid)
                                {
                                    if (totPendapatanIDR < 0 )
                                    {
                                        tbl[i].amtcreditidr = totPendapatanIDR * (-1);
                                    }
                                    else
                                    {
                                        tbl[i].amtdebetidr =  totPendapatanIDR;
                                    }
                                    tbl[i].amtbalanceidr = tbl[i].amtopenidr + tbl[i].amtdebetidr - tbl[i].amtcreditidr;

                                    if (totPendapatanUSD < 0)
                                    {
                                        tbl[i].amtcreditusd = totPendapatanUSD * (-1);
                                    }
                                    else
                                    {
                                        tbl[i].amtdebetusd = totPendapatanUSD;
                                    }
                                    tbl[i].amtbalanceusd = tbl[i].amtopenusd + tbl[i].amtdebetusd - tbl[i].amtcreditusd;
                                }
                            }
                            if (iLabaTahunBerjalanOid != 0)
                            {
                                if (tbl[i].acctgoid == iLabaTahunBerjalanOid)
                                {
                                    if (totPendapatanIDR2 < 0)
                                    {
                                        tbl[i].amtcreditidr = totPendapatanIDR2 * (-1);
                                    }
                                    else
                                    {
                                        tbl[i].amtdebetidr = totPendapatanIDR2;
                                    }
                                    tbl[i].amtbalanceidr = tbl[i].amtopenidr + tbl[i].amtdebetidr - tbl[i].amtcreditidr;

                                    if (totPendapatanUSD2 < 0)
                                    {
                                        tbl[i].amtcreditusd = totPendapatanUSD2 * (-1);
                                    }
                                    else
                                    {
                                        tbl[i].amtdebetusd = totPendapatanUSD2;
                                    }
                                    tbl[i].amtbalanceusd = tbl[i].amtopenusd + tbl[i].amtdebetusd - tbl[i].amtcreditusd;
                                }
                            }
                        }
                    }
                }
                Session["QL_trnclosing"] = tbl;

                //CHECK Not POST Transaction
                sSql = "DECLARE @cmpcode VARCHAR(10);" +
                    "DECLARE @month INT; " +
                    "DECLARE @year INT; " +
                    "SET @cmpcode='" + CompnyCode + "'; " +
                    "SET @month=" + iMonth + "; " +
                    "SET @year=" + iYear + "; " +
                    "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/P' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trndpap dp ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup='DPAP' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/R' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trndpar dr ON cb.cmpcode=dr.cmpcode AND cb.cashbankoid=dr.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup='DPAR' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'A/P PAYMENT' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup LIKE 'AP%' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'A/R PAYMENT' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup LIKE 'AR%' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'INCOMING GIRO REALIZATION' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(cashbankdate)=@month " +
                    "AND YEAR(cashbankdate)=@year " +
                    "AND cashbankgroup='GIRO IN' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'CASH/BANK EXPENSE' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup='EXPENSE' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'CASH/BANK RECEIPT' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup='RECEIPT' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'CASH/BANK OVERBOOKING' AS tipe, 0 AS seq " +
                    "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " +
                    "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " +
                    "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " +
                    "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " +
                    "AND cashbankgroup='MUTATION' HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'MEMORIAL JOURNAL' AS tipe, 0 AS seq FROM QL_trnglmst WHERE cmpcode=@cmpcode " +
                    "AND glflag='IN PROCESS' AND MONTH(gldate)=@month AND YEAR(gldate)=@year HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'FIXED ASSETS CONFIRMATION' AS tipe, 0 AS seq FROM QL_trnassetrecmst WHERE cmpcode=@cmpcode " +
                    "AND assetrecmststatus='IN PROCESS' AND MONTH(assetrecdate)=@month AND YEAR(assetrecdate)=@year HAVING COUNT(-1)>0 " +
                    "UNION ALL " +
                    "SELECT COUNT(-1) AS jml, 'FIXED ASSETS PERIOD POSTING' AS tipe, 0 AS seq FROM QL_assetmst am INNER JOIN QL_assetdtl ad ON ad.cmpcode=am.cmpcode AND ad.assetmstoid=am.assetmstoid WHERE am.cmpcode=@cmpcode " +
                    "AND assetmststatus='POST' AND assetdtlstatus='' AND MONTH(assetdate)=@month AND YEAR(assetdate)=@year HAVING COUNT(-1)>0 ";
                tbl2 = db.Database.SqlQuery<trnmonthly2>(sSql).ToList();
            }

            JsonResult js = Json(new { tbl, tbl2, nextcloseyear, nextclosemonth, lastcloseyear, lastclosemonth, nextclosing, lastclosing, BindClosingData, sMsg }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetLastAndNextMonthlyClosing()
        {
            JsonResult js = null;
            var history = db.Database.SqlQuery<string>("SELECT ISNULL(closingperiod, '') FROM QL_acctgclosinghistory WHERE cmpcode='" + CompnyCode + "' AND historyclosingoid=(SELECT MAX(historyclosingoid) FROM QL_acctgclosinghistory WHERE cmpcode='" + CompnyCode + "' AND closinggroup='MONTHLY' AND closingstatus='CLOSED')").FirstOrDefault();

            int lastcloseyear = 0;
            int selectedyear = 0;
            int lastclosemonth = 0;
            int selectedmonth = 0;
            var lastclosing = "";
            var nextclosing = "";
            int nextclosemonth = 0;
            int nextcloseyear = 0;
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();

            if (!string.IsNullOrEmpty(history))
            {
                lastcloseyear = int.Parse(ClassFunction.Left(history, 4));
                lastclosemonth = int.Parse(ClassFunction.Right(history, 2));
                lastclosing = mfi.GetMonthName((lastclosemonth)).ToString() + " " + lastcloseyear;

                if (lastclosemonth == 12)
                {
                    selectedmonth = 1;
                    selectedyear = lastcloseyear + 1;
                    nextclosemonth = 1;
                    nextcloseyear = lastcloseyear + 1;
                }
                else
                {
                    selectedmonth = lastclosemonth + 1;
                    selectedyear = lastcloseyear;
                    nextclosemonth = lastclosemonth + 1;
                    nextcloseyear = lastcloseyear;
                }
                nextclosing = mfi.GetMonthName((nextclosemonth)).ToString() + " " + nextcloseyear;
            }
            else
            {
                var sLastClose = db.Database.SqlQuery<string>("SELECT ISNULL(MIN(periodacctg), '') FROM QL_crdgl WHERE cmpcode='" + CompnyCode + "' AND crdglflag='OPEN'").FirstOrDefault();
                lastcloseyear = 0; lastclosemonth = 0; lastclosing = "-";

                if (sLastClose != "")
                {
                    nextclosemonth = int.Parse(ClassFunction.Right(sLastClose, 2));
                    nextcloseyear = int.Parse(ClassFunction.Left(sLastClose, 4));
                    nextclosing = mfi.GetMonthName((nextclosemonth)).ToString() + " " + nextcloseyear; ;
                    selectedmonth = int.Parse(ClassFunction.Right(sLastClose, 2));
                    selectedyear = int.Parse(ClassFunction.Left(sLastClose, 4));
                }
                else
                {
                    lastcloseyear = 0; lastclosemonth = 0; lastclosing = "-";
                    nextclosemonth = 0; nextcloseyear = 0; nextclosing = "-";
                    selectedmonth = (DateTime.Now.Month);
                    selectedyear = (DateTime.Now.Year);
                }
            }

            var tempDate = new DateTime(selectedyear, selectedmonth, 1);
            sSql = "SELECT TOP 1 ISNULL(crdglflag, '') AS t FROM QL_crdgl WHERE cmpcode='" + CompnyCode + "' AND periodacctg='" + ClassFunction.GetDateToPeriodAcctg(tempDate) + "'";
            string BindClosingData = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (string.IsNullOrEmpty(BindClosingData))
            {
                BindClosingData = "OPEN";
            }

            js = Json(new { selectedyear, selectedmonth, nextcloseyear, nextclosemonth, nextclosing, lastclosing, BindClosingData }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmonthly> dtDtl)
        {
            Session["QL_trnclosing"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public class tblPost
        {
            public int dtloid { get; set; }
            public string mstoid { get; set; }
            public string trnno { get; set; }
            public decimal value { get; set; }
            public decimal valueidr { get; set; }
            public decimal valueusd { get; set; }
            public int acctgoid { get; set; }
            public int depacctgoid { get; set; }
            public int rateoid { get; set; }
            public int rate2oid { get; set; }
            public decimal ratetoidr { get; set; }
            public decimal ratetousd { get; set; }
            public decimal rate2toidr { get; set; }
            public decimal rate2tousd { get; set; }
        }

        private string PostAssetDepreciation(string cmpcode, int iYear, int iMonth)
        {
            var sMsg = "";
            // Posting Penyusutan Fixed Assets
            sSql = "SELECT fad.assetdtloid AS dtloid, fad.assetmstoid mstoid, fam.assetno AS trnno, fad.assetperiodvalue value, fad.assetperiodvalueidr valueidr, fad.assetperiodvalueusd valueusd, fad.periodacctgoid acctgoid, fad.perioddepacctgoid depacctgoid, fam.rateoid rateoid, fam.rate2oid rate2oid, /*CAST(rateres1 AS REAL)*/ 1 AS ratetoidr, /*CAST(rateres2 AS REAL)*/ 1 AS ratetousd, /*CAST(rate2res1 AS REAL)*/ 1 AS rate2toidr, /*CAST(rate2res2 AS REAL)*/ 1 AS rate2tousd FROM QL_assetdtl fad INNER JOIN QL_assetmst fam ON fam.cmpcode=fad.cmpcode AND fam.assetmstoid=fad.assetmstoid /*INNER JOIN QL_mstrate2 r2 ON r2.rate2oid=fam.rate2oid INNER JOIN QL_mstrate r ON r.rateoid=fam.rateoid*/ WHERE fad.cmpcode='" + cmpcode + "' AND fam.assetmststatus<>'In Process' AND fam.assetmstflag='' AND fad.assetdtlstatus='' AND assetperiod='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear, iMonth, 1)) + "' ORDER BY fam.assetno ";
            List<tblPost> dtPostAsset = db.Database.SqlQuery<tblPost>(sSql).ToList();

            if (dtPostAsset.Count > 0)
            {
                var iGLMstOid = ClassFunction.GenerateID("QL_trnglmst");
                var iGLDtlOid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        for (int i = 0; i < dtPostAsset.Count(); i++)
                        {
                            sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Complete', postuser='" + Session["UserID"].ToString() + "', postdate=CURRENT_TIMESTAMP WHERE cmpcode='" + cmpcode + "' AND assetdtloid=" + dtPostAsset[i].dtloid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_assetmst SET assetaccumdep=assetaccumdep + " + dtPostAsset[i].value + ", assetaccumdepidr=assetaccumdepidr + " + dtPostAsset[i].valueidr + ", assetaccumdepusd=assetaccumdepusd + " + dtPostAsset[i].valueusd + ", assetmststatus='Closed' WHERE cmpcode='" + cmpcode + "' AND assetmstoid=" + dtPostAsset[i].mstoid + " AND (SELECT COUNT(*) FROM QL_assetdtl WHERE cmpcode='" + cmpcode + "' AND assetmstoid=" + dtPostAsset[i].mstoid + " AND assetdtlstatus='' AND assetdtloid<>" + dtPostAsset[i].dtloid + ")=0";
                            if (db.Database.ExecuteSqlCommand(sSql) == 0)
                            {
                                sSql = "UPDATE QL_assetmst SET assetaccumdep=assetaccumdep + " + dtPostAsset[i].value + ", assetaccumdepidr=assetaccumdepidr + " + dtPostAsset[i].valueidr + ", assetaccumdepusd=assetaccumdepusd + " + dtPostAsset[i].valueusd + " WHERE cmpcode='" + cmpcode + "' AND assetmstoid=" + dtPostAsset[i].mstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                                db.SaveChanges();
                            
                            //' Auto Jurnal Accounting
                            //' Biaya Penyusutan
                            //' Akumulasi Penyusutan
                            
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(cmpcode, iGLMstOid++, new DateTime(iYear, iMonth, DateTime.DaysInMonth(iYear, iMonth)), ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear, iMonth, 1)), "'Penyusutan Asset|No=" + dtPostAsset[i].trnno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, dtPostAsset[i].rateoid, dtPostAsset[i].rate2oid, dtPostAsset[i].ratetoidr, dtPostAsset[i].rate2toidr, dtPostAsset[i].ratetousd, dtPostAsset[i].rate2tousd,0));
                            db.SaveChanges();

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmpcode, iGLDtlOid++, 1, iGLMstOid, dtPostAsset[i].acctgoid, "D", dtPostAsset[i].value, dtPostAsset[i].trnno, "Penyusutan Asset|No=" + dtPostAsset[i].trnno, "Post", Session["UserID"].ToString(), servertime, dtPostAsset[i].valueidr, dtPostAsset[i].valueusd, "QL_assetdtl " + dtPostAsset[i].dtloid, null, null, null, 0,0));
                            db.SaveChanges();

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(cmpcode, iGLDtlOid++, 2, iGLMstOid, dtPostAsset[i].depacctgoid, "C", dtPostAsset[i].value, dtPostAsset[i].trnno, "Penyusutan Asset|No=" + dtPostAsset[i].trnno, "Post", Session["UserID"].ToString(), servertime, dtPostAsset[i].valueidr, dtPostAsset[i].valueusd, "QL_assetdtl " + dtPostAsset[i].dtloid, null, null, null, 0,0));
                            db.SaveChanges();
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (iGLMstOid - 1) + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_ID SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        sMsg = "DBInfo," + ex.ToString();
                    }
                }

            }
            return sMsg;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int iMonth, int iYear)
        {
            JsonResult js = null;
            try
            {
                var sError = PostAssetDepreciation(cmp, iMonth, iYear);
                if (sError != "")
                {
                    js = Json(sError.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string action, string action2, string closingstatus, int? iYear, int? iMonth, int? lastcloseyear, int? lastclosemonth, int? nextcloseyear, int? nextclosemonth, decimal? ratebi)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            if (Session["QL_trnclosing"] == null)
            {
                Session["QL_trnclosing"] = new List<trnmonthly>();
            }
            List<trnmonthly> dtDtl = (List<trnmonthly>)Session["QL_trnclosing"];

            if (!string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(action2))
            {
                var tempDate = new DateTime(iYear.Value, iMonth.Value, 1);
                if (closingstatus.ToUpper() == "OPEN")
                {
                    sSql = "DECLARE @cmpcode VARCHAR(10);" +
                    "DECLARE @periode VARCHAR(10);" +
                    "DECLARE @month INTEGER;" +
                    "DECLARE @year INTEGER;" +
                    "SET @cmpcode='" + CompnyCode + "';" +
                    "SET @periode='" + ClassFunction.GetDateToPeriodAcctg(tempDate) + "';" +
                    "SET @month=" + iMonth + ";" +
                    "SET @year=" + iYear + ";" +
                    "SELECT @cmpcode cmpcode, acctgoid, acctgcode, acctgdesc, amtopenidr, SUM(debetidr) AS amtdebetidr, SUM(creditidr) AS amtcreditidr, (amtopenidr + SUM(debetidr) - SUM(creditidr)) AS amtbalanceidr, amtopenusd, SUM(debetusd) AS amtdebetusd, SUM(creditusd) AS amtcreditusd, (amtopenusd + SUM(debetusd) - SUM(creditusd)) AS amtbalanceusd, seq, acctggrp1 FROM (" +
                    "SELECT a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0) AS amtopenidr, ISNULL(crd.amtopenusd, 0) AS amtopenusd, 'debetidr'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'creditidr'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'debetusd'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN (CASE WHEN gd.glamt<>gd.glamtidr THEN gd.glamt ELSE 0.0 END) ELSE 0 END), 'creditusd'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN (CASE WHEN gd.glamt<>gd.glamtidr THEN gd.glamt ELSE 0.0 END) ELSE 0 END), 0.00 AS amtbalanceidr, 0.00 AS amtbalanceusd, 0 AS seq, a1.acctggrp1 " +
                    "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON crd.cmpcode=@cmpcode AND crd.acctgoid=a1.acctgoid AND crd.periodacctg=@periode LEFT JOIN QL_trngldtl gd ON gd.cmpcode=@cmpcode AND a1.acctgoid=gd.acctgoid AND gd.glflag='Post' AND gd.glmstoid>0 LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' AND gm.glmstoid>0 " +
                    "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode)" +
                    ") AS crd GROUP BY acctgoid, acctgcode, acctgdesc, amtopenidr, amtopenusd, seq, acctggrp1 ORDER BY acctgcode";
                }
                else if (closingstatus.ToUpper() == "CLOSED")
                {
                    sSql = "SELECT '" + CompnyCode + "' cmpcode, a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0.0) amtopenidr, ISNULL(crd.amtdebit, 0.0) amtdebetidr, ISNULL(crd.amtcredit, 0.0) amtcreditidr, ISNULL(crd.amtbalance, 0.0) amtbalanceidr, ISNULL(crd.amtopenusd, 0.0) amtopenusd, ISNULL(crd.amtdebitusd, 0.0) amtdebetusd, ISNULL(crd.amtcreditusd, 0.0) amtcreditusd, ISNULL(crd.amtbalanceusd, 0.0) amtbalanceusd, 0 seq, a1.acctggrp1 " +
                    "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON a1.acctgoid=crd.acctgoid AND crd.cmpcode='" + CompnyCode + "' AND crd.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(tempDate) + "' " +
                    "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode) ORDER BY a1.acctgcode";
                }
                dtDtl = db.Database.SqlQuery<trnmonthly>(sSql).ToList();

                if (dtDtl == null)
                    ModelState.AddModelError("", "Fill Detail Data!");
                else if (dtDtl.Count <= 0)
                    ModelState.AddModelError("", "Fill Detail Data!");

                if (lastclosemonth != 0 && lastcloseyear != 0)
                {
                    if (iMonth != nextclosemonth || iYear != nextcloseyear)
                    {
                        ModelState.AddModelError("", "Next Monthly Closing period is : " + nextclosemonth + " " + nextcloseyear + " !");
                    }
                }
                else
                {
                    if (ClassFunction.GetServerTime() < new DateTime(iYear.Value, iMonth.Value, DateTime.DaysInMonth(iYear.Value, iMonth.Value)))
                    {
                        ModelState.AddModelError("", "Monthly Closing can be executed minimum at the end of the month period!");
                    }
                }

                //VAR Utk LABA/RUGI Bulanan, Tahunan, Ditahan
                string varLabaBerjalan = ClassFunction.GetVarInterface("VAR_LR_MONTH", CompnyCode);
                int iLabaBerjalanOid = 0;
                if (!string.IsNullOrEmpty(varLabaBerjalan))
                {
                    iLabaBerjalanOid = ClassFunction.GetAcctgOID(varLabaBerjalan);
                }

                string varLabaTahunBerjalan = ClassFunction.GetVarInterface("VAR_LR_YEAR", CompnyCode);
                int iLabaTahunBerjalanOid = 0;
                if (!string.IsNullOrEmpty(varLabaTahunBerjalan))
                {
                    iLabaTahunBerjalanOid = ClassFunction.GetAcctgOID(varLabaTahunBerjalan);
                }

                string varLabaDitahan = ClassFunction.GetVarInterface("VAR_LR_ONHAND", CompnyCode);
                int iLabaDitahan = 0;
                if (!string.IsNullOrEmpty(varLabaDitahan))
                {
                    iLabaDitahan = ClassFunction.GetAcctgOID(varLabaDitahan);
                }
                if (iLabaBerjalanOid == 0)
                {
                    ModelState.AddModelError("", "Please Setting VAR_LR_MONTH in Interface!");
                }
                if (iLabaTahunBerjalanOid == 0)
                {
                    ModelState.AddModelError("", "Please Setting VAR_LR_YEAR in Interface!");
                }
                if (iLabaDitahan == 0)
                {
                    ModelState.AddModelError("", "Please Setting VAR_LR_ONHAND in Interface!");
                }
                var iAcctgOidDiffDebet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", CompnyCode));
                var iAcctgOidDiffCredit = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_BIAYA_CURR_IDR", CompnyCode));

                //Untuk laba (rugi) berjalan
                decimal totPendapatanIDR = 0; decimal totPendapatanUSD = 0;
                //decimal totBiayaIDR = 0; decimal totBiayaUSD = 0;
                decimal totPendapatanIDR2 = 0; decimal totPendapatanUSD2 = 0;
                //decimal totBiayaIDR2 = 0; decimal totBiayaUSD2 = 0;

                decimal dLRBulanBerjalanIDR = 0; decimal dLRBulanBerjalanUSD = 0;
                decimal dLRTahunBerjalanIDR = 0; decimal dLRTahunBerjalanUSD = 0;
                if (dtDtl != null)
                {
                    if (dtDtl.Count() > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            //Validasi Revaluasi Selisih Kurs
                            if (dtDtl[i].amtbalanceusd != 0)
                            {
                                if (ratebi == null || ratebi == 0 || ratebi == 1)
                                {
                                    ModelState.AddModelError("", "Please Input Rate BI, Because already transaction non IDR!");
                                }
                                else
                                {
                                    dtDtl[i].ratetoidr = ratebi.Value;
                                }
                            }
                            else
                            {
                                dtDtl[i].ratetoidr = 1;
                            }

                            //Revaluasi Selisih Kurs USD
                            if (dtDtl[i].amtbalanceusd != 0 && dtDtl[i].ratetoidr != 0)
                            {
                                decimal amtreal = dtDtl[i].amtbalanceusd * dtDtl[i].ratetoidr;
                                decimal amtidr = dtDtl[i].amtbalanceidr;
                                decimal balanceamt = amtidr - amtreal;

                                dtDtl[i].selisihamt = balanceamt;
                                if (dtDtl[i].selisihamt < 0)
                                {
                                    dtDtl[i].amtdebetidr = dtDtl[i].amtdebetidr + Math.Abs(dtDtl[i].selisihamt);
                                    dtDtl[i].amtbalanceidr = dtDtl[i].amtopenidr + dtDtl[i].amtdebetidr - dtDtl[i].amtcreditidr;
                                    dtDtl[i].amtbalanceusd = dtDtl[i].amtopenusd + dtDtl[i].amtdebetusd - dtDtl[i].amtcreditusd;
                                }
                                else if (dtDtl[i].selisihamt > 0)
                                {
                                    dtDtl[i].amtcreditidr = dtDtl[i].amtcreditidr + Math.Abs(dtDtl[i].selisihamt);
                                    dtDtl[i].amtbalanceidr = dtDtl[i].amtopenidr + dtDtl[i].amtdebetidr - dtDtl[i].amtcreditidr;
                                    dtDtl[i].amtbalanceusd = dtDtl[i].amtopenusd + dtDtl[i].amtdebetusd - dtDtl[i].amtcreditusd;
                                }
                            }

                            if (dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN USAHA" || dtDtl[i].acctggrp1.ToUpper() == "HARGA POKOK" || dtDtl[i].acctggrp1.ToUpper() == "BIAYA USAHA" || dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN DAN BIAYA LAIN-LAIN")
                            {
                                totPendapatanIDR += (dtDtl[i].amtdebetidr - dtDtl[i].amtcreditidr);
                                totPendapatanUSD += (dtDtl[i].amtdebetusd - dtDtl[i].amtcreditusd);
                                //totBiayaIDR += (tbl[i].amtdebetidr - tbl[i].amtcreditidr);
                                //totBiayaUSD += (tbl[i].amtdebetusd - tbl[i].amtcreditusd);
                                totPendapatanIDR2 += dtDtl[i].amtopenidr;
                                totPendapatanUSD2 += dtDtl[i].amtopenusd;
                                //totBiayaIDR2 += tbl[i].amtopenidr;
                                //totBiayaUSD2 += tbl[i].amtopenusd;
                            }
                        }

                        //Membalik Pendapatan 
                        //totPendapatanIDR *= -1; totPendapatanUSD *= -1;
                        //totPendapatanIDR2 *= -1; totPendapatanUSD2 *= -1;

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (iLabaBerjalanOid != 0)
                            {
                                if (dtDtl[i].acctgoid == iLabaBerjalanOid)
                                {
                                    if (totPendapatanIDR < 0)
                                    {
                                        dtDtl[i].amtcreditidr = totPendapatanIDR * (-1);
                                    }
                                    else
                                    {
                                        dtDtl[i].amtdebetidr = totPendapatanIDR;
                                    }
                                    dtDtl[i].amtbalanceidr = dtDtl[i].amtopenidr + dtDtl[i].amtdebetidr - dtDtl[i].amtcreditidr;

                                    if (totPendapatanUSD < 0)
                                    {
                                        dtDtl[i].amtcreditusd = totPendapatanUSD * (-1);
                                    }
                                    else
                                    {
                                        dtDtl[i].amtdebetusd = totPendapatanUSD;
                                    }
                                    dtDtl[i].amtbalanceusd = dtDtl[i].amtopenusd + dtDtl[i].amtdebetusd - dtDtl[i].amtcreditusd;
                                }
                            }
                            if (iLabaTahunBerjalanOid != 0)
                            {
                                if (dtDtl[i].acctgoid == iLabaTahunBerjalanOid)
                                {
                                    if (totPendapatanIDR2 < 0)
                                    {
                                        dtDtl[i].amtcreditidr = totPendapatanIDR2 * (-1);
                                    }
                                    else
                                    {
                                        dtDtl[i].amtdebetidr = totPendapatanIDR2;
                                    }
                                    dtDtl[i].amtbalanceidr = dtDtl[i].amtopenidr + dtDtl[i].amtdebetidr - dtDtl[i].amtcreditidr;

                                    if (totPendapatanUSD2 < 0)
                                    {
                                        dtDtl[i].amtcreditusd = totPendapatanUSD2 * (-1);
                                    }
                                    else
                                    {
                                        dtDtl[i].amtdebetusd = totPendapatanUSD2;
                                    }
                                    dtDtl[i].amtbalanceusd = dtDtl[i].amtopenusd + dtDtl[i].amtdebetusd - dtDtl[i].amtcreditusd;
                                }
                            }
                        }

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].acctgoid == iLabaBerjalanOid)
                            {
                                dLRBulanBerjalanIDR += dtDtl[i].amtbalanceidr; dLRBulanBerjalanUSD += dtDtl[i].amtbalanceusd;
                            }
                            if (dtDtl[i].acctgoid == iLabaTahunBerjalanOid)
                            {
                                dLRTahunBerjalanIDR += dtDtl[i].amtbalanceidr; dLRTahunBerjalanUSD += dtDtl[i].amtbalanceusd;
                            }
                        }    
                    }
                }

                if (ModelState.IsValid)
                {
                    int vIDCH = ClassFunction.GenerateID("QL_acctgclosinghistory");
                    int vIDCRD = ClassFunction.GenerateID("QL_crdgl");
                    int vIDGLMst = ClassFunction.GenerateID("QL_trnglmst");
                    int vIDGLDtl = ClassFunction.GenerateID("QL_trngldtl");
                    DateTime first = new DateTime(iYear.Value, iMonth.Value, 1);
                    DateTime last = new DateTime(iYear.Value, iMonth.Value, DateTime.DaysInMonth(iYear.Value, iMonth.Value));
                    int nextmonth = 0; int nextyear = 0;
                    if (iMonth == 12)
                    {
                        nextmonth = 1; nextyear = iYear.Value + 1;
                    }
                    else
                    {
                        nextmonth = iMonth.Value + 1; nextyear = iYear.Value;
                    }
                    DateTime nextfirst = new DateTime(nextyear, nextmonth, 1);
                    var servertime = ClassFunction.GetServerTime();

                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (dtDtl != null)
                            {
                                if (dtDtl.Count() > 0)
                                {
                                    if (closingstatus == "CLOSED")
                                    {
                                        for (int i = 0; i < dtDtl.Count(); i++)
                                        {
                                            //Revaluasi Selisih Kurs USD
                                            if (dtDtl[i].amtbalanceusd != 0 && dtDtl[i].ratetoidr != 0 && dtDtl[i].ratetoidr != 1)
                                            {
                                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, vIDGLMst, DateTime.Parse(last.ToString("MM/dd/yyyy")), ClassFunction.GetDateToPeriodAcctg(last), "Monthly Closing Auto Posting", "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1,0));
                                                db.SaveChanges();
                                                if (dtDtl[i].selisihamt < 0)
                                                {
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 1, vIDGLMst, dtDtl[i].acctgoid, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dtDtl[i].selisihamt), 0, "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 2, vIDGLMst, iAcctgOidDiffCredit, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dtDtl[i].selisihamt), 0, "", null, null, null, 0,0));
                                                    db.SaveChanges();

                                                }
                                                else if (dtDtl[i].selisihamt > 0)
                                                {
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 1, vIDGLMst, dtDtl[i].acctgoid, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dtDtl[i].selisihamt), 0, "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 2, vIDGLMst, iAcctgOidDiffDebet, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dtDtl[i].selisihamt), 0, "", null, null, null, 0,0));
                                                    db.SaveChanges();

                                                }
                                                vIDGLMst += 1;
                                                
                                            }
                                        }
                                    }

                                    for (int i = 0; i < dtDtl.Count(); i++)
                                    {
                                        int acctgdtl = dtDtl[i].acctgoid;
                                        string pr = ClassFunction.GetDateToPeriodAcctg(last);
                                        QL_crdgl tbl = db.QL_crdgl.Where(a => a.cmpcode == CompnyCode && a.acctgoid == acctgdtl && a.periodacctg == pr).FirstOrDefault();
                                        
                                        if (tbl == null)
                                        {
                                            if (!(dtDtl[i].amtopenidr == 0 && dtDtl[i].amtdebetidr == 0 && dtDtl[i].amtcreditidr == 0 && dtDtl[i].amtbalanceidr == 0))
                                            {
                                                tbl = new QL_crdgl();
                                                tbl.cmpcode = CompnyCode;
                                                tbl.crdgloid = vIDCRD++;
                                                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(last);
                                                tbl.crdgldate = first;
                                                tbl.acctgoid = dtDtl[i].acctgoid;
                                                tbl.upduser = Session["UserID"].ToString();
                                                tbl.updtime = servertime;
                                                tbl.postdate = (closingstatus == "CLOSED" ? servertime : DateTime.Parse("1900-01-01"));
                                                tbl.crdglflag = closingstatus;
                                                tbl.crdglnote = "MONTHLY CLOSING BALANCE - " + ClassFunction.GetDateToPeriodAcctg(last) + "";
                                                tbl.createuser = Session["UserID"].ToString();
                                                tbl.createtime = servertime;
                                                tbl.amtopen = dtDtl[i].amtopenidr;
                                                tbl.amtdebit = dtDtl[i].amtdebetidr;
                                                tbl.amtcredit = dtDtl[i].amtcreditidr;
                                                tbl.amtbalance = dtDtl[i].amtbalanceidr;
                                                tbl.amtopenusd = dtDtl[i].amtopenusd;
                                                tbl.amtdebitusd = dtDtl[i].amtdebetusd;
                                                tbl.amtcreditusd = dtDtl[i].amtcreditusd;
                                                tbl.amtbalanceusd = dtDtl[i].amtbalanceusd;
                                                tbl.ratetoidr = dtDtl[i].ratetoidr;
                                                db.QL_crdgl.Add(tbl);
                                                db.SaveChanges();
                                            }
                                            
                                        }
                                        else
                                        {
                                            tbl.postdate = (closingstatus == "CLOSED" ? servertime : DateTime.Parse("1900-01-01"));
                                            tbl.crdglflag = closingstatus;
                                            tbl.crdglnote = "MONTHLY CLOSING BALANCE - " + ClassFunction.GetDateToPeriodAcctg(last) + "";
                                            tbl.amtopen = dtDtl[i].amtopenidr;
                                            tbl.amtdebit = dtDtl[i].amtdebetidr;
                                            tbl.amtcredit = dtDtl[i].amtcreditidr;
                                            tbl.amtbalance = dtDtl[i].amtbalanceidr;
                                            tbl.amtopenusd = dtDtl[i].amtopenusd;
                                            tbl.amtdebitusd = dtDtl[i].amtdebetusd;
                                            tbl.amtcreditusd = dtDtl[i].amtcreditusd;
                                            tbl.ratetoidr = dtDtl[i].ratetoidr;
                                            db.Entry(tbl).State = EntityState.Modified;
                                            db.SaveChanges();

                                            if (dtDtl[i].amtopenidr == 0 && dtDtl[i].amtdebetidr == 0 && dtDtl[i].amtcreditidr == 0 && dtDtl[i].amtbalanceidr == 0)
                                            {
                                                db.QL_crdgl.Remove(tbl);
                                                db.SaveChanges();
                                            }
                                        }

                                        if (closingstatus == "CLOSED")
                                        {
                                            // Calculate Value utk Next Month
                                            decimal amtBalanceIDR = dtDtl[i].amtbalanceidr;
                                            decimal amtBalanceUSD = dtDtl[i].amtbalanceusd;

                                            if (dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN USAHA" || dtDtl[i].acctggrp1.ToUpper() == "HARGA POKOK" || dtDtl[i].acctggrp1.ToUpper() == "BIAYA USAHA" || dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN DAN BIAYA LAIN-LAIN")
                                            {
                                                amtBalanceIDR = 0; amtBalanceUSD = 0;
                                            }

                                            // Pada Closing Desember L/R Tahun Berjalan dan L/R Bulan Berjalan ditambahkan ke L/R Ditahan next year 
                                            if (iMonth == 12)
                                            {
                                                if (dtDtl[i].acctgoid == iLabaBerjalanOid || dtDtl[i].acctgoid == iLabaTahunBerjalanOid)
                                                {
                                                    amtBalanceIDR = 0; amtBalanceUSD = 0;
                                                }
                                                else if (dtDtl[i].acctgoid == iLabaDitahan)
                                                {
                                                    amtBalanceIDR += (dLRBulanBerjalanIDR + dLRTahunBerjalanIDR);
                                                    amtBalanceUSD += (dLRBulanBerjalanUSD + dLRTahunBerjalanUSD);
                                                }
                                            }
                                            else
                                            {
                                                // Selain Desember, L/R Bulan ini diakumulasikan ke L/R Tahun Berjalan next month
                                                if (dtDtl[i].acctgoid == iLabaBerjalanOid || dtDtl[i].acctgoid == iLabaTahunBerjalanOid)
                                                {
                                                    amtBalanceIDR = 0; amtBalanceUSD = 0;
                                                }
                                                else if (dtDtl[i].acctgoid == iLabaDitahan)
                                                {
                                                    amtBalanceIDR += dLRBulanBerjalanIDR;
                                                    amtBalanceUSD += dLRBulanBerjalanUSD;
                                                }
                                            }

                                            if (amtBalanceIDR != 0)
                                            {
                                                //INSERT SALDO AWAL BULAN SELANJUTNYA
                                                tbl = new QL_crdgl();
                                                tbl.cmpcode = CompnyCode;
                                                tbl.crdgloid = vIDCRD++;
                                                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(nextfirst);
                                                tbl.crdgldate = nextfirst;
                                                tbl.acctgoid = dtDtl[i].acctgoid;
                                                tbl.upduser = Session["UserID"].ToString();
                                                tbl.updtime = servertime;
                                                tbl.postdate = servertime;
                                                tbl.crdglflag = "OPEN";
                                                tbl.crdglnote = "OPENING BALANCE - " + ClassFunction.GetDateToPeriodAcctg(nextfirst) + "";
                                                tbl.createuser = Session["UserID"].ToString();
                                                tbl.createtime = servertime;
                                                tbl.amtopen = amtBalanceIDR;
                                                tbl.amtdebit = 0;
                                                tbl.amtcredit = 0;
                                                tbl.amtbalance = amtBalanceIDR;
                                                tbl.amtopenusd = amtBalanceUSD;
                                                tbl.amtdebitusd = 0;
                                                tbl.amtcreditusd = 0;
                                                tbl.amtbalanceusd = amtBalanceUSD;
                                                db.QL_crdgl.Add(tbl);
                                                db.SaveChanges();
                                            }

                                            // Insert GL terkait Laba/Rugi
                                            int iAcctgOid = 0; int iFixedAcctgOid = 0; decimal dAmtIDR = 0; decimal dAmtUSD = 0;
                                            if (dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN USAHA" || dtDtl[i].acctggrp1.ToUpper() == "HARGA POKOK" || dtDtl[i].acctggrp1.ToUpper() == "BIAYA USAHA" || dtDtl[i].acctggrp1.ToUpper() == "PENDAPATAN DAN BIAYA LAIN-LAIN")
                                            {
                                                iAcctgOid = dtDtl[i].acctgoid; iFixedAcctgOid = iLabaBerjalanOid;
                                                dAmtIDR = dtDtl[i].amtbalanceidr; dAmtUSD = dtDtl[i].amtbalanceusd;
                                            }
                                            else if (dtDtl[i].acctgoid == iLabaBerjalanOid)
                                            {
                                                iAcctgOid = iLabaBerjalanOid; iFixedAcctgOid = iLabaTahunBerjalanOid;
                                                dAmtIDR = dLRBulanBerjalanIDR; dAmtUSD = dLRBulanBerjalanUSD;
                                            }
                                            else if (dtDtl[i].acctgoid == iLabaTahunBerjalanOid && iMonth == 12)
                                            {
                                                iAcctgOid = iLabaTahunBerjalanOid; iFixedAcctgOid = iLabaDitahan;
                                                dAmtIDR = dLRBulanBerjalanIDR; dAmtUSD = dLRBulanBerjalanUSD;
                                            }
                                            if (dAmtIDR != 0)
                                            {
                                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, vIDGLMst, DateTime.Parse(last.ToString("MM/dd/yyyy")), ClassFunction.GetDateToPeriodAcctg(last), "Monthly Closing Auto Posting", "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1,0));
                                                db.SaveChanges();
                                                if (dAmtIDR < 0)
                                                {
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 1, vIDGLMst, iAcctgOid, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 2, vIDGLMst, iFixedAcctgOid, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                }
                                                else if (dAmtIDR > 0)
                                                {
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 1, vIDGLMst, iFixedAcctgOid, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, 2, vIDGLMst, iAcctgOid, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                    db.SaveChanges();
                                                }
                                                else
                                                {
                                                    int iSeq = 1;
                                                    int iAcctgOid_db = iAcctgOid; int iAcctgOid_cr = iFixedAcctgOid;
                                                    if (dAmtIDR > 0)
                                                    {
                                                        iAcctgOid_db = iFixedAcctgOid; iAcctgOid_cr = iAcctgOid;
                                                    }
                                                    if (dAmtIDR != 0)
                                                    {
                                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, iSeq++, vIDGLMst, iAcctgOid_db, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), 0, "", null, null, null, 0,0));
                                                        db.SaveChanges();
                                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, iSeq++, vIDGLMst, iAcctgOid_cr, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, Math.Abs(dAmtIDR), 0, "", null, null, null, 0,0));
                                                        db.SaveChanges();
                                                    }
                                                    iAcctgOid_db = iAcctgOid; iAcctgOid_cr = iFixedAcctgOid;
                                                    if (dAmtUSD > 0)
                                                    {
                                                        iAcctgOid_db = iFixedAcctgOid; iAcctgOid_cr = iAcctgOid;
                                                    }
                                                    if (dAmtUSD != 0)
                                                    {
                                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, iSeq++, vIDGLMst, iAcctgOid_db, "D", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                        db.SaveChanges();
                                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, vIDGLDtl++, iSeq++, vIDGLMst, iAcctgOid_cr, "C", 0, "CLOSING-" + ClassFunction.GetDateToPeriodAcctg(new DateTime(iYear.Value, iMonth.Value, 1)), "Monthly Closing Auto Posting", "Post", Session["UserID"].ToString(), servertime, 0, Math.Abs(dAmtUSD), "", null, null, null, 0,0));
                                                        db.SaveChanges();
                                                    }
                                                }
                                            }
                                            vIDGLMst += 1;

                                        }
                                    }
                                    sSql = "UPDATE QL_ID SET lastoid=" + (vIDCRD - 1) + " WHERE tablename='QL_crdgl'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_ID SET lastoid=" + (vIDGLMst - 1) + " WHERE tablename='QL_trnglmst'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_ID SET lastoid=" + (vIDGLDtl - 1) + " WHERE tablename='QL_trngldtl'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    //Simpan Data History Closing Bulanan
                                    string pr2 = ClassFunction.GetDateToPeriodAcctg(last);
                                    QL_acctgclosinghistory tblhist = db.QL_acctgclosinghistory.Where(a => a.cmpcode == CompnyCode && a.closingperiod == pr2).FirstOrDefault();
                                    if (tblhist == null)
                                    {
                                        tblhist = new QL_acctgclosinghistory();
                                        tblhist.cmpcode = CompnyCode;
                                        tblhist.historyclosingoid = vIDCH;
                                        tblhist.historyopendate = last;
                                        tblhist.historyopenuserid = Session["UserID"].ToString();
                                        tblhist.historyopentime = servertime;
                                        tblhist.historyclosingdate = servertime;
                                        tblhist.closingyear = iYear.Value;
                                        tblhist.createuser = Session["UserID"].ToString();
                                        tblhist.createtime = servertime;
                                        tblhist.upduser = Session["UserID"].ToString();
                                        tblhist.updtime = servertime;
                                        tblhist.closingmonth = iMonth.Value;
                                        tblhist.closingmonthname = DateTime.DaysInMonth(iYear.Value, iMonth.Value).ToString("MMMM").ToUpper();
                                        tblhist.historyclosinguserid = Session["UserID"].ToString();
                                        tblhist.historyclosingtime = servertime;
                                        tblhist.closingperiod = ClassFunction.GetDateToPeriodAcctg(last);
                                        tblhist.closingstatus = closingstatus;
                                        tblhist.closingnote = "MONTHLY CLOSING - " + ClassFunction.GetDateToPeriodAcctg(last);
                                        tblhist.closinggroup = "MONTHLY";
                                        db.QL_acctgclosinghistory.Add(tblhist);
                                        db.SaveChanges();

                                        sSql = "UPDATE QL_ID SET lastoid=" + vIDCH + " WHERE tablename='QL_acctgclosinghistory'";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        tblhist.historyclosingdate = last;
                                        tblhist.historyopenuserid = Session["UserID"].ToString();
                                        tblhist.historyclosingtime = servertime;
                                        tblhist.closingstatus = closingstatus;
                                        tblhist.closingnote = "MONTHLY CLOSING - " + ClassFunction.GetDateToPeriodAcctg(last);
                                        db.Entry(tblhist).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                            }

                            // Oh we are here, looks like everything is fine - save all the data permanently
                            objTrans.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            ViewBag.closingstatus = "OPEN"; ViewBag.ratebi = ratebi;
                            // roll back all database operations, if any thing goes wrong
                            objTrans.Rollback();
                            ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                        }
                    }
                }
                else
                {
                    ViewBag.closingstatus = "OPEN"; ViewBag.ratebi = ratebi;
                }
            }
            else
            {
                ViewBag.ratebi = 0;
            }
            InitDDL();
            return View(dtDtl);
        }

        [HttpPost, ActionName("unClose")]
        [ValidateAntiForgeryToken]
        public ActionResult unCloseConfirmed(string closingstatus, int iYear, int iMonth)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (Session["QL_trnclosing"] == null)
            {
                Session["QL_trnclosing"] = new List<trnmonthly>();
            }
            List<trnmonthly> dtDtl = (List<trnmonthly>)Session["QL_trnclosing"];
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (dtDtl == null)
                msg = "Silahkan isi detail data !!";
            else if (dtDtl.Count <= 0)
                msg = "Silahkan isi detail data !!";

            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (closingstatus == "CLOSED")
                        {
                            DateTime dDate = new DateTime(iYear, iMonth, 1);
                            string sPeriod = ClassFunction.GetDateToPeriodAcctg(dDate);

                            QL_acctgclosinghistory tbl = db.QL_acctgclosinghistory.Where(x => x.cmpcode == CompnyCode && x.closingperiod == sPeriod).FirstOrDefault();

                            if (tbl == null)
                            {
                                //no Action
                            }
                            else
                            {
                                sSql = "UPDATE QL_crdgl SET amtdebit=0, amtdebitusd=0, amtcredit=0, amtcreditusd=0, amtbalance=amtopen, amtbalanceusd=amtopenusd, crdglflag='OPEN', crdglnote='OPENING BALANCE - ' + '" + sPeriod + "' WHERE cmpcode='" + CompnyCode + "' AND periodacctg='" + sPeriod + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "DELETE QL_acctgclosinghistory WHERE cmpcode='" + CompnyCode + "' AND closingperiod>='" + sPeriod + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "DELETE QL_crdgl WHERE cmpcode='" + CompnyCode + "' AND periodacctg>'" + sPeriod + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "DELETE QL_trngldtl WHERE cmpcode='" + CompnyCode + "' AND glmstoid IN (SELECT glmstoid FROM QL_trnglmst WHERE cmpcode='" + CompnyCode + "' AND periodacctg>='" + sPeriod + "' AND glnote IN('Monthly Closing Auto Posting'))";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "DELETE QL_trnglmst WHERE cmpcode='" + CompnyCode + "' AND periodacctg>='" + sPeriod + "' AND glnote IN('Monthly Closing Auto Posting')";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int iYear, int iMonth)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            DataTable dtRpt = new ClassConnection().GetDataTable(Session["queryclosing"].ToString(), "closingbulanan");
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("sMonth", iMonth);
            rptparam.Add("sYear", iYear);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintUserName", Session["UserName"].ToString());

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMonthlyClosingXls.rpt"));
            report.SetDataSource(dvRpt.ToTable());
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "MonthlyClosing.xls");
        }
    }
}