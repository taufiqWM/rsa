﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class RawMaterialReceivedController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnmrrawmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int mrrawmstoid { get; set; }
            public int mrrawwhoid { get; set; }
            public string mrrawno { get; set; }
            public string porawno { get; set; }
            public DateTime mrrawdate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string mrrawmststatus { get; set; }
            public string mrrawmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnmrrawdtl
        {
            public int mrrawdtlseq { get; set; }
            public int porawdtloid { get; set; }
            public string porawno { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal matrawlimitqty { get; set; }
            public decimal porawdtlqty { get; set; }
            public decimal mrrawqty { get; set; }
            public int mrrawunitoid { get; set; }
            public string mrrawunit { get; set; }
            public string serialno { get; set; }
            public string mrrawdtlnote { get; set; }
            public decimal poqty { get; set; }
            public decimal mrrawvalue { get; set; }
            public decimal mrrawamt { get; set; }
            public string location { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnmrrawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            var mrrawwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.mrrawwhoid = mrrawwhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int porawmstoid, int suppoid, int mrrawmstoid, int mrrawwhoid)
        {
            List<trnmrrawdtl> tbl = new List<trnmrrawdtl>();            

            sSql = "SELECT 0 AS mrrawdtlseq, regd.porawdtloid, porawno, regd.matrawoid AS matrawoid, matrawcode, matrawlongdesc, matrawlimitqty,  (regd.porawqty - ISNULL((SELECT SUM(mrrawqty) FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnmrrawmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrrawmstoid = mrd.mrrawmstoid WHERE mrd.cmpcode = regd.cmpcode AND mrd.porawdtloid = regd.porawdtloid AND mrm2.porawmstoid = regm.porawmstoid AND mrd.mrrawmstoid <> " + mrrawmstoid + "), 0) ) AS porawdtlqty, 0.0 AS mrrawqty, regd.porawunitoid AS mrrawunitoid, gendesc AS mrrawunit, '' serialno,'' AS mrrawdtlnote, (CASE porawdtldiscvalue WHEN 0 THEN porawprice ELSE(porawdtlnetto / porawqty) END) AS mrrawvalue, '' [location], porawqty AS poqty, porawdtlseq, regm.porawmstoid " +
                "FROM QL_trnporawdtl regd " +
                "INNER JOIN QL_trnporawmst regm ON regm.cmpcode = regd.cmpcode AND regd.porawmstoid = regm.porawmstoid " +
                "INNER JOIN QL_mstmatraw m ON m.matrawoid = regd.matrawoid " +
                "INNER JOIN QL_mstgen g ON genoid = porawunitoid " +
                "WHERE regd.cmpcode = '" + CompnyCode + "' AND regd.porawmstoid = " + porawmstoid + " AND porawdtlstatus = '' AND ISNULL(porawdtlres2, '')<> 'Complete' " +
                "ORDER BY porawdtlseq";

            tbl = db.Database.SqlQuery<trnmrrawdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        } 

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmrrawdtl> dtDtl)
        {
            Session["QL_trnmrrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmrrawdtl"] == null)
            {
                Session["QL_trnmrrawdtl"] = new List<trnmrrawdtl>();
            }

            List<trnmrrawdtl> dataDtl = (List<trnmrrawdtl>)Session["QL_trnmrrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmrrawmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.porawno = db.Database.SqlQuery<string>("SELECT porawno FROM QL_trnporawmst WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();
            //sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnregistermst WHERE cmpcode='" + cmp + "' AND registertype='RAW' AND registermststatus='Post' AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed' AND ISNULL(registermstres2, '')<>'Closed') ORDER BY suppcode, suppname";
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT x.suppoid FROM QL_trnporawmst x WHERE x.cmpcode='" + CompnyCode + "' AND x.porawmststatus='Approved' AND x.divgroupoid='" + divgroupoid + "') ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int porawmstoid { get; set; }
            public string porawno { get; set; }
            public DateTime porawdate { get; set; }
            public string docrefno { get; set; }
            public DateTime docrefdate { get; set; }
            public string porawmstnote { get; set; }
            public string flag { get; set; }
            public int curroid { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int divgroupoid)
        {
            List<pomst> tbl = new List<pomst>();
            //sSql = "SELECT registermstoid, registerno, registerdate, registerdocrefno,  registerdocrefdate, registermstnote, registerflag, (SELECT TOP 1 curroid FROM QL_trnporawmst pom INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=pom.cmpcode AND porefmstoid=porawmstoid WHERE regd.cmpcode='" + cmp + "' AND regd.registermstoid=QL_trnregistermst.registermstoid) AS curroid FROM QL_trnregistermst WHERE cmpcode='" + cmp + "' AND suppoid=" + suppoid + " AND registertype='RAW' AND registermststatus='Post' AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed' AND ISNULL(registermstres2, '')<>'Closed' ORDER BY registermstoid";
            sSql = "SELECT porawmstoid, porawno, porawdate, '' [docrefno],  porawmstnote, '' [flag], curroid FROM QL_trnporawmst WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + suppoid + " AND porawmststatus='Approved' and divgroupoid="+divgroupoid+" ORDER BY porawmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "mrm", divgroupoid, "divgroupoid");
            sSql = " SELECT mrm.cmpcode, (select gendesc from ql_mstgen where genoid=mrm.divgroupoid) divgroup, mrrawmstoid, mrrawno, mrrawdate, suppname, porawno, mrrawmststatus, mrrawmstnote, '' [divname] FROM QL_trnmrrawmst mrm INNER JOIN QL_trnporawmst reg ON reg.cmpcode=mrm.cmpcode AND reg.porawmstoid=mrm.porawmstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "mrm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "mrm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND mrrawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND mrrawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND mrrawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND mrrawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND mrrawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND mrrawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND mrm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnmrrawmst> dt = db.Database.SqlQuery<trnmrrawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnmrrawmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmrrawmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnmrrawmst();
                tbl.cmpcode = CompnyCode;
                tbl.mrrawmstoid = ClassFunction.GenerateID("QL_trnmrrawmst");
                tbl.mrrawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrrawmststatus = "In Process";


                Session["QL_trnmrrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnmrrawmst.Find(cmp, id);
                sSql = "SELECT mrrawdtlseq, mrd.porawdtloid, porawno, mrd.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, (reg.porawqty - ISNULL((SELECT SUM(x.mrrawqty) FROM QL_trnmrrawdtl x INNER JOIN QL_trnmrrawmst y ON y.cmpcode = x.cmpcode AND y.mrrawmstoid = x.mrrawmstoid WHERE x.cmpcode = mrd.cmpcode AND x.porawdtloid = mrd.porawdtloid AND reg.porawmstoid = y.porawmstoid AND x.mrrawmstoid <> mrd.mrrawmstoid), 0) - ISNULL((SELECT SUM(retrawqty) FROM QL_trnretrawdtl x WHERE x.cmpcode = mrd.cmpcode AND x.porawdtloid = mrd.porawdtloid), 0)) AS porawdtlqty, (mrrawqty + mrrawbonusqty) AS mrqty, mrrawqty, mrrawbonusqty, mrrawunitoid, gendesc AS mrrawunit, mrd.serialno,mrrawdtlnote, mrrawvalue, (mrrawvalue * mrrawqty) AS mrrawamt, '' AS location, porawqty AS poqty " +
                    "FROM QL_trnmrrawdtl mrd " +
                    "INNER JOIN QL_mstmatraw m ON m.matrawoid = mrd.matrawoid " +
                    "INNER JOIN QL_mstgen g ON genoid = mrrawunitoid " +
                    "INNER JOIN QL_trnporawdtl reg ON reg.cmpcode = mrd.cmpcode AND reg.porawdtloid = mrd.porawdtloid AND porawmstoid IN(SELECT porawmstoid FROM QL_trnmrrawmst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mrrawmstoid = mrd.mrrawmstoid) " +
                    "INNER JOIN QL_trnporawmst pom ON pom.cmpcode = reg.cmpcode AND pom.porawmstoid = reg.porawmstoid " +
                    "WHERE mrrawmstoid = " + id + " AND mrd.cmpcode = '" + cmp + "' ORDER BY mrrawdtlseq";
                Session["QL_trnmrrawdtl"] = db.Database.SqlQuery<trnmrrawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

      

        private Boolean IsPOWillBeClosed(string cmp, int poitemmstoid)
        {
            List<trnmrrawdtl> dtDtl = (List<trnmrrawdtl>)Session["QL_trnmrrawdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrrawqty >= dtDtl[i].porawdtlqty)
                {
                    sOid += dtDtl[i].porawdtloid + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnpoitemdtl WHERE cmpcode='" + cmp + "' AND poitemmstoid=" + poitemmstoid + " AND poitemdtlstatus='' AND poitemdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }


        public class import
        {
            public int curroid { get; set; }
            public decimal importvalue { get; set; }
        }

        private decimal GetImportCost(string cmp, int registermstoid)
        {
            decimal dImp = 0;
            List<import> tbl = new List<import>();
            sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" + cmp + "' AND icm.registermstoid=" + registermstoid + " GROUP BY icd.curroid ";
            tbl = db.Database.SqlQuery<import>(sSql).ToList();
            for (int i = 0; i < tbl.Count(); i++)
            {
                //cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
                dImp += tbl[i].importvalue;// * cRateTmp.GetRateMonthlyIDRValue
            }

            return dImp;
        }

        public class dtHdr
        {
            public int mrrawmstoid { get; set; }
            public DateTime mrrawdate { get; set; }
            public string mrrawno { get; set; }
            public int mrrawwhoid { get; set; }
        }

        public class dtDtl
        {
            public int mrrawmstoid { get; set; }
            public int mrrawdtloid { get; set; }
            public int mrrawdtlseq { get; set; }
            public int matrawoid { get; set; }
            public int mrrawqty { get; set; }
            public decimal mrrawvalue { get; set; }
            public decimal mrrawamt { get; set; }
        }


        private void GetLastData(string cmp, int registermstoid, int mrrawmstoid, out List<dtHdr> dtLastHdrData, out List<dtDtl> dtLastDtlData)
        {
            dtLastHdrData = new List<dtHdr>();
            sSql = "SELECT mrrawmstoid, mrrawdate, mrrawno, mrrawwhoid FROM QL_trnmrrawmst WHERE cmpcode='" + cmp + "' AND mrrawmststatus='Post' AND mrrawno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'RMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'RMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrrawmstoid<>" + mrrawmstoid;
            sSql += " ORDER BY mrrawmstoid ";
            dtLastHdrData = db.Database.SqlQuery<dtHdr>(sSql).ToList();

            dtLastDtlData = new List<dtDtl>();
            sSql = "SELECT mrm.mrrawmstoid, mrrawdtloid, mrrawdtlseq, matrawoid, mrrawqty, mrrawvalue, (mrrawqty * mrrawvalue) AS mrrawamt FROM QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid WHERE mrm.cmpcode='" + cmp + "' AND mrrawmststatus='Post' AND mrrawno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" + cmp + "' AND noref LIKE 'RMR-%' UNION ALL SELECT noref FROM QL_trngldtl_hist WHERE cmpcode='" + cmp + "' AND noref LIKE 'RMR-%') AND registermstoid=" + registermstoid;
            sSql += " AND mrm.mrrawmstoid<>" + mrrawmstoid;
            sSql += " ORDER BY mrm.mrrawmstoid, mrrawdtlseq ";
            dtLastDtlData = db.Database.SqlQuery<dtDtl>(sSql).ToList();
        }

        private string generateNo(string cmp)
        {
            var sNo = "RMR-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrrawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrrawmst WHERE cmpcode='" + cmp + "' AND mrrawno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        private string generateBatchNo()
        {
            var tgl = ClassFunction.GetServerTime();
            var sNo = tgl.ToString("yyyy.MM.dd") + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            sNo = sNo + sCounter;

            return sNo;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrrawmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();
            var divisi = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupoid).FirstOrDefault();
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            if (tbl.mrrawno == null)
                tbl.mrrawno = "";
            var tolerance = db.Database.SqlQuery<decimal>("select isnull(porawtolerance,0.0) from ql_trnporawmst where porawmstoid=" + tbl.porawmstoid).FirstOrDefault();
            List<trnmrrawdtl> dtDtl = (List<trnmrrawdtl>)Session["QL_trnmrrawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mrrawqty <= 0)
                        {
                            ModelState.AddModelError("", "MR QTY for Material " + dtDtl[i].matrawlongdesc + " must be more than 0");
                        }
                        if (dtDtl[i].serialno == null)
                        {
                            dtDtl[i].serialno = "";
                        }
                        dtDtl[i].mrrawamt = dtDtl[i].mrrawqty * dtDtl[i].mrrawvalue;
                    }
                }
            }
            var a = dtDtl.GroupBy(x => x.porawdtloid).Select(x => new { poid = x.Key, mrqty = x.Sum(y => (y.mrrawqty)), }).ToList();
            for (int i = 0; i < a.Count(); i++)
            {
                sSql = "SELECT ((porawqty+(porawqty*(" + tolerance + "/100))) - ISNULL((SELECT SUM(mrrawqty) FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnmrrawmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrrawmstoid=mrd.mrrawmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.porawdtloid=regd.porawdtloid AND mrm2.porawmstoid=regd.porawmstoid  AND mrd.mrrawmstoid<>" + tbl.mrrawmstoid + "), 0)) AS poqtyqty FROM QL_trnporawdtl regd WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.porawdtloid=" + a[i].poid + " AND regd.porawmstoid=" + tbl.porawmstoid;
                var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                if (dQty < a[i].mrqty)
                    ModelState.AddModelError("", "Some PO Qty has been updated by another user. QTY MR Received is greater than PO QTY!");

                
            }

            if (tbl.mrrawmststatus.ToUpper() == "POST")
            { 
                var sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    sVarErr = "VAR_STOCK_RM";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    sVarErr = "VAR_PURC_RECEIVED";
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));
                }
                var msg = "";
                var regdate  = db.Database.SqlQuery<DateTime>("SELECT porawdate FROM QL_trnporawmst WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid).FirstOrDefault();
                
                cRate.SetRateValue(tbl.curroid, regdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
               
                if (msg != "")
                    ModelState.AddModelError("", msg);
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmrrawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrrawdtl");
                var dtloid2 = ClassFunction.GenerateID("QL_trnmrrawdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                var regflag = db.Database.SqlQuery<string>("SELECT porawtype FROM QL_trnporawmst WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid).FirstOrDefault();

                Boolean isRegClosed = false;
                decimal dImportCost = 0;
                decimal dSumMR = 0;
                decimal dSumMR_IDR = 0;
                decimal dSumMR_USD = 0;
                List<dtHdr> dtLastHdrData = null ;
                List<dtDtl> dtLastDtlData = null;
                var iConMatOid = 0;
                var iCrdMtrOid = 0;
                var iGLMstOid = 0;
                var iGLDtlOid = 0;
                var iStockValOid = 0;
                var iStockAcctgOid = 0;
                var iRecAcctgOid = 0;
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                var sDateGRIR = "";
                var cat1 = false;
                var batch = "";
            
                if (tbl.mrrawmststatus.ToUpper() == "POST")
                {
                
                    if (regflag.ToUpper() == "IMPORT")
                    {
                        isRegClosed = IsPOWillBeClosed(tbl.cmpcode, tbl.porawmstoid);
                    }
                    if (regflag.ToUpper() == "IMPORT" && isRegClosed == true)
                    {
                        dImportCost = GetImportCost(tbl.cmpcode, tbl.porawmstoid) / cRate.GetRateMonthlyIDRValue ; 
                        GetLastData(tbl.cmpcode, tbl.porawmstoid, tbl.mrrawmstoid, out dtLastHdrData, out dtLastDtlData);
                        dSumMR += dtLastDtlData.Sum(x => x.mrrawamt);
                    }
                    if ((regflag.ToUpper() == "LOCAL") || (regflag.ToUpper() == "IMPORT" & isRegClosed == true))
                    {
                        iConMatOid = ClassFunction.GenerateID("QL_CONMAT");
                        iCrdMtrOid = ClassFunction.GenerateID("QL_CRDMTR");
                        iGLMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                        iGLDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                        iStockValOid = ClassFunction.GenerateID("QL_STOCKVALUE");
                        iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value));
                        iRecAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", tbl.cmpcode, tbl.divgroupoid.Value));
                        sDateGRIR = ClassFunction.GetServerTime().ToString("MM/dd/yyyy  hh:mm:ss");
                        //tbl.grirposttime = ClassFunction.GetServerTime();
                    }
                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                    tbl.mrrawno = generateNo(tbl.cmpcode);
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmrrawmst.Find(tbl.cmpcode, tbl.mrrawmstoid) != null)
                                tbl.mrrawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mrrawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            //tbl.grirposttime = new DateTime(1900, 01, 01);
                            db.QL_trnmrrawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.mrrawmstoid + " WHERE tablename='QL_trnmrrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid + " AND registerdtloid IN (SELECT registerdtloid FROM QL_trnmrrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + tbl.mrrawmstoid + ")";
                            sSql = "UPDATE QL_trnporawdtl SET porawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + " AND porawdtloid IN (SELECT porawdtloid FROM QL_trnmrrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + tbl.mrrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //sSql = "UPDATE QL_trnregistermst SET registermststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND registermstoid=" + tbl.registermstoid;
                            sSql = "UPDATE QL_trnporawmst SET porawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmrrawdtl.Where(m => m.mrrawmstoid == tbl.mrrawmstoid && m.cmpcode == tbl.cmpcode);
                            db.QL_trnmrrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        for (int i = 0; i < a.Count(); i++)
                        {
                            sSql = "SELECT ((porawqty+(porawqty*(" + tolerance + "/100))) - ISNULL((SELECT SUM(mrrawqty) FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnmrrawmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrrawmstoid=mrd.mrrawmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.porawdtloid=regd.porawdtloid AND mrm2.porawmstoid=regd.porawmstoid  AND mrd.mrrawmstoid<>" + tbl.mrrawmstoid + "), 0)) AS poqtyqty FROM QL_trnporawdtl regd WHERE regd.cmpcode='" + tbl.cmpcode + "' AND regd.porawdtloid=" + a[i].poid + " AND regd.porawmstoid=" + tbl.porawmstoid;
                            var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if ((a[i].mrqty + (Math.Round(a[i].mrqty * (tolerance / 100)))) >= dQty)
                            {
                              
                                sSql = "UPDATE QL_trnporawdtl SET porawdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND porawdtloid=" + a[i].poid + " AND porawmstoid=" + tbl.porawmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnporawmst SET porawmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + " AND (SELECT COUNT(*) FROM QL_trnporawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + " AND porawdtloid<>" + a[i].poid + " AND porawdtlstatus='')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }


                        }

                        QL_trnmrrawdtl tbldtl;
                        dSumMR += dtDtl.Sum(x => x.mrrawamt);
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dMRVal = 0;
                            if (dSumMR > 0)
                            {
                                dMRVal = (dtDtl[i].mrrawvalue + ((dtDtl[i].mrrawvalue / dSumMR) * dImportCost));
                            }
                            if (dtDtl[i].mrrawdtlnote == null)
                                dtDtl[i].mrrawdtlnote = "";
                            dSumMR_IDR += (Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue,4 ) * (dtDtl[i].mrrawqty));
                            dSumMR_USD += (Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * (dtDtl[i].mrrawqty));
                            //dSumMR_IDR += Math.Round(dMRVal * cRate.GetRateMonthlyIDRValue, 4) * ToDouble(objTable.Rows(C1)("mrrawqty").ToString)
                            //dSumMR_USD += Math.Round(dMRVal * cRate.GetRateMonthlyUSDValue, 6) * ToDouble(objTable.Rows(C1)("mrrawqty").ToString)
                            cat1 = db.Database.SqlQuery<Boolean>("select flagbatch from ql_mstmatraw where matrawoid=" + dtDtl[i].matrawoid + "").FirstOrDefault();
                            if (cat1 == true)
                            {
                                batch = generateBatchNo();
                            }
                            else
                            {
                                batch = "";
                            }
                            tbldtl = new QL_trnmrrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.mrrawdtloid = dtloid++;
                            tbldtl.mrrawmstoid = tbl.mrrawmstoid;
                            tbldtl.mrrawdtlseq = i + 1;
                            tbldtl.registerdtloid = 0;
                            tbldtl.porawdtloid = dtDtl[i].porawdtloid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.mrrawqty = dtDtl[i].mrrawqty;
                            tbldtl.mrrawbonusqty = 0;
                            tbldtl.mrrawunitoid = dtDtl[i].mrrawunitoid;
                            tbldtl.mrrawvalueidr = dMRVal * cRate.GetRateMonthlyIDRValue;
                            tbldtl.mrrawvalueusd = dMRVal * cRate.GetRateMonthlyUSDValue;
                            tbldtl.mrrawdtlstatus = "";
                            tbldtl.mrrawdtlnote = dtDtl[i].mrrawdtlnote == null ? "" : dtDtl[i].mrrawdtlnote;
                            tbldtl.mrrawvalue = dMRVal;
                            tbldtl.mrrawdtldisctype = "0";
                            tbldtl.serialno = dtDtl[i].serialno;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.mrrawdtlres3 = batch;
                            db.QL_trnmrrawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.mrrawmststatus.ToUpper() == "POST")
                            {
                                // Insert QL_conmat
                                db.QL_conmat.Add(ClassFunction.InsertConMat(tbl.cmpcode, iConMatOid++, "RMR", "QL_trnmrrawdtl", tbl.mrrawmstoid, dtDtl[i].matrawoid, "RAW MATERIAL", tbl.mrrawwhoid, dtDtl[i].mrrawqty, "Raw Material Received", tbl.mrrawno, Session["UserID"].ToString(), tbl.mrrawno, "R-001-" + tbldtl.mrrawdtlres3, tbldtl.mrrawvalueidr, tbldtl.mrrawvalueusd ?? 0, 0, null, tbldtl.mrrawdtloid, tbl.divgroupoid ?? 0, tbldtl.serialno));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnmrrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.mrrawmststatus.ToUpper() == "POST")
                        {
    
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    decimal dMRVal = 0;
                                    if (dSumMR > 0)
                                    {
                                        dMRVal = (dtDtl[i].mrrawvalue + ((dtDtl[i].mrrawvalue / dSumMR) * dImportCost));
                                    }
                                    //var x = cRate.GetRateMonthlyIDRValue;
                                    //var y = cRate.GetRateMonthlyUSDValue;

                                  
                                   
                               

                                    //Insert QL_stockvalue
                                    sSql = ClassFunction.GetQueryUpdateStockValue(dtDtl[i].mrrawqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrrawdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matrawoid, "RAW MATERIAL");
                                    if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                    {
                                        db.SaveChanges();
                                        sSql = sSql = ClassFunction.GetQueryInsertStockValue(dtDtl[i].mrrawqty, (dMRVal * cRate.GetRateMonthlyIDRValue), (dMRVal * cRate.GetRateMonthlyUSDValue), "QL_trnmrrawdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matrawoid, "RAW MATERIAL", iStockValOid++);
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    db.SaveChanges();

                             
                                }
                                dSumMR += dImportCost;
                                if (dSumMR > 0)
                                {

                                    // Insert QL_trnglmst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, iGLMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "MR Raw|No. " + tbl.mrrawno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 1, iGLMstOid, iStockAcctgOid, "D", dSumMR, tbl.mrrawno, "MR Raw|No. " + tbl.mrrawno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmrrawmst " + tbl.mrrawmstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 2, iGLMstOid, iRecAcctgOid, "C", dSumMR, tbl.mrrawno, "MR Raw|No. " + tbl.mrrawno, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, dSumMR_USD, "QL_trnmrrawmst " + tbl.mrrawmstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();

                                    iGLMstOid += 1;
                                }

                                sSql = " UPDATE QL_mstoid SET lastoid=" + (iConMatOid - 1) + " WHERE tablename='QL_CONMAT' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iCrdMtrOid - 1) + " WHERE tablename='QL_CRDMTR' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLMstOid - 1) + " WHERE tablename='QL_TRNGLMST' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" + CompnyCode + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } //update Stock & GL
                       

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.mrrawmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.mrrawno = "";
            tbl.mrrawmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrrawmst tbl = db.QL_trnmrrawmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnporawdtl SET porawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + " AND porawdtloid IN (SELECT porawdtloid FROM QL_trnmrrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + tbl.mrrawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnporawmst SET porawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmrrawdtl.Where(a => a.mrrawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnmrrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmrrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmrrawmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMR.rpt"));
            //sSql = "SELECT mrm.mrrawmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrrawmstoid)) AS [Draft No.], mrm.mrrawno AS [MR No.], mrm.mrrawdate AS [MR Date], mrm.mrrawmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrrawmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrrawmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrrawdtlseq AS [No.] , m.matrawcode AS [Code], m.matrawlongdesc AS [Description], mrd.mrrawqty AS [Qty], mrrawbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrrawdtlnote AS [Note], pom.porawno AS [PO No.], pom.porawdate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg PostDate], registerdocrefdate AS [Tgl. SJ], registerdocrefno [No SJ],registernopol [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] , ISNULL((SELECT ISNULL(loc.detaillocation,'')  FROM QL_mstlocation loc WHERE loc.cmpcode=mrm.cmpcode AND loc.mtrwhoid=mrm.mrrawwhoid AND (CASE loc.refname WHEN 'raw' THEN 'Raw' WHEN 'gen' THEN 'General' WHEN 'sp' THEN 'Spare Part' ELSE '' END)='Raw' AND loc.matoid=mrd.matrawoid),'') AS [Detail Location] " +
            //    " FROM QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid INNER JOIN QL_mstmatraw m ON  m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrrawunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrrawwhoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrm.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnporawmst pom ON pom.cmpcode=mrm.cmpcode AND  pom.porawmstoid=regd.porefmstoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=mrm.cmpcode AND pod.porawdtloid=regd.porefdtloid INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='Raw' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrrawmstoid IN (" + id + ") ORDER BY mrm.mrrawmstoid, mrrawdtlseq ";
            sSql = "SELECT mrm.mrrawmstoid AS [Oid], (CONVERT(VARCHAR(10), mrm.mrrawmstoid)) AS [Draft No.], mrm.mrrawno AS [MR No.], mrm.mrrawdate AS [MR Date], mrm.mrrawmststatus AS [Status], g1.gendesc AS [Warehouse], mrm.cmpcode AS cmpcode, mrm.suppoid [Suppoid], s.suppcode [Supp Code], s.suppname [Supplier], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], (SELECT divaddress FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Address], (SELECT gc.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid  WHERE mrm.cmpcode = div.cmpcode) AS [BU City], (SELECT gp.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gp ON gp.cmpcode=gc.cmpcode AND gp.genoid=CONVERT(INT, gc.genother1)  WHERE mrm.cmpcode = div.cmpcode) AS [BU Province], (SELECT gco.gendesc FROM QL_mstdivision div INNER JOIN QL_mstgen gc ON gc.genoid=divcityoid INNER JOIN QL_mstgen gco ON gco.cmpcode=gc.cmpcode AND gco.genoid=CONVERT(INT, gc.genother2) WHERE mrm.cmpcode = div.cmpcode) AS [BU Country], (SELECT ISNULL(divphone, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 1], (SELECT ISNULL(divphone2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Telp. 2], (SELECT ISNULL(divfax1, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 1], (SELECT ISNULL(divfax2, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Fax 2], (SELECT ISNULL(divemail, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Email], (SELECT ISNULL(divpostcode, '') FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Post Code] , mrm.mrrawmstnote AS [Header Note], mrm.createuser, mrm.createtime, (CASE mrm.mrrawmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], mrrawdtlseq AS [No.] , m.matrawcode AS [Code], m.matrawlongdesc AS [Description], mrd.mrrawqty AS [Qty], mrrawbonusqty AS [Bonus Qty], g.gendesc AS [Unit], mrd.mrrawdtlnote AS [Note], regm.porawno AS [PO No.], regm.porawdate AS [PO Date], regm.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE regm.approvaluser=p2.profoid) AS [PO UserApp] ,regm.porawno [Reg No.],regm.porawdate [Reg Date], regm.updtime AS [Reg PostDate], CAST('01/01/1900' AS date) AS [Tgl. SJ], '' [No SJ], '' [No Pol.], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost], '' AS[Detail Location] " +
                "FROM QL_trnmrrawmst mrm  " +
                "INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode = mrm.cmpcode AND mrd.mrrawmstoid = mrm.mrrawmstoid " +
                "INNER JOIN QL_mstmatraw m ON m.matrawoid = mrd.matrawoid " +
                "INNER JOIN QL_mstgen g ON g.genoid = mrd.mrrawunitoid " +
                "INNER JOIN QL_mstgen g1 ON g1.genoid = mrrawwhoid " +
                "INNER JOIN QL_trnporawdtl regd ON regd.cmpcode = mrm.cmpcode AND regd.porawmstoid = mrm.porawmstoid AND regd.porawdtloid = mrd.porawdtloid " +
                "INNER JOIN QL_trnporawmst regm ON mrm.cmpcode = regm.cmpcode AND regm.porawmstoid = mrm.porawmstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid = mrm.suppoid   " + 
                "WHERE mrm.cmpcode='" + cmp + "' AND mrm.mrrawmstoid IN (" + id + ") ORDER BY mrm.mrrawmstoid, mrrawdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintMRRaw");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("Header", "RAW MATERIAL RECEIVED PRINT OUT");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "RawMaterialReceivedPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
