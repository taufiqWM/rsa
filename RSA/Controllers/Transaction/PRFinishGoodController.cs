﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PRFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class pritemmst
        {
            public string cmpcode { get; set; }
            public int pritemmstoid { get; set; }
            public string pritemno { get; set; }
            public DateTime pritemdate { get; set; }
            public string deptname { get; set; }
            public string pritemmststatus { get; set; }
            public string pritemmstnote { get; set; }
            public string divname { get; set; }
        }

        public class pritemdtl
        {
            public int pritemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal itemlimitqty { get; set; }
            public decimal pritemqty { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public DateTime pritemarrdatereq { get; set; }
            public string pritemdtlnote { get; set; }
            //public int requiredtloid { get; set; }
            //public decimal requireqty { get; set; }
        }

        public class matitem
        {
            public string cmpcode { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal itemlimitqty { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        private void InitDDL(QL_pritemmst tbl)
        {
            //sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            //if (Session["CompnyCode"].ToString() != CompnyCode)
            //    sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            //sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_pritemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataDetails()
        {
            //List<pritemdtl> tbl = new List<pritemdtl>();

            //if (reqoid == 0)
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matrawcode) AS INT) pritemdtlseq, matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, 0.0000 prrawqty, matrawunitoid prrawunitoid, gendesc prrawunit, '' pritemdtlnote, DATEADD(D, 7, GETDATE()) prrawarrdatereq, 0 requiredtloid, 0.0000 requireqty FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON genoid=matrawunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' AND ISNULL(matrawres2, '')='Non Assets' AND ISNULL(matrawres3, '') NOT IN ('Sawn Timber', 'Log') AND matrawoid IN (SELECT DISTINCT prd.matrawoid FROM QL_pritemdtl prd WHERE prd.cmpcode='" + cmp + "' AND prd.upduser='" + Session["UserID"].ToString() + "') ORDER BY matrawcode";
            //else
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matrawcode) AS INT) pritemdtlseq, m.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, requireqty prrawqty, requireunitoid prrawunitoid, gendesc prrawunit, '' pritemdtlnote, DATEADD(D, 7, GETDATE()) prrawarrdatereq, requiredtloid, requireqty FROM QL_trnrequiredtl req INNER JOIN QL_mstmatraw m ON matrawoid=requirerefoid INNER JOIN QL_mstgen g ON genoid=requireunitoid WHERE req.cmpcode='" + cmp + "' AND requiremstoid=" + reqoid + " AND requirereftype='Raw' AND requireqty>0 ORDER BY matrawcode";

            //tbl = db.Database.SqlQuery<pritemdtl>(sSql).ToList();
            if(Session["QL_pritemdtl"] == null)
            {
                Session["QL_pritemdtl"] = new List<pritemdtl>();
            }
            List<pritemdtl> tbl = (List<pritemdtl>)Session["QL_pritemdtl"];

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<pritemdtl> dtDtl)
        {
            Session["QL_pritemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_pritemdtl"] == null)
            {
                Session["QL_pritemdtl"] = new List<pritemdtl>();
            }

            List<pritemdtl> dataDtl = (List<pritemdtl>)Session["QL_pritemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_pritemmst tbl)
        {
            //ViewBag.requireno = db.Database.SqlQuery<string>("SELECT requireno FROM QL_trnrequiremst WHERE cmpcode='" + tbl.cmpcode + "' AND requiremstoid=" + tbl.requiremstoid + "").FirstOrDefault();
        }

        //[HttpPost]
        //public ActionResult GetRequireData(string cmp)
        //{
        //    List<QL_trnrequiremst> tbl = new List<QL_trnrequiremst>();

        //    sSql = "SELECT * FROM QL_trnrequiremst req WHERE req.cmpcode='" + cmp + "' AND requiremststatus='Post' ORDER BY requiredate DESC";
        //    tbl = db.Database.SqlQuery<QL_trnrequiremst>(sSql).ToList();

        //    return Json(tbl, JsonRequestBehavior.AllowGet);
        //}

        // GET: PRFinishGood
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, mdFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRFinishGood", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT prm.cmpcode, pritemmstoid, pritemno, pritemdate, deptname, pritemmststatus, pritemmstnote, divname FROM QL_pritemmst prm INNER JOIN QL_mstdept de ON de.cmpcode=prm.cmpcode AND de.deptoid=prm.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=prm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "prm.cmpcode IN (" + Session["CompnyCode"].ToString() + ")";
            else
                sSql += "prm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                sSql += "  ";
            }
            else
            {
                sSql += " AND (pritemdate BETWEEN '" + modfil.filterperiod1 + "' AND '" + modfil.filterperiod2 + "')";
                sSql += " AND pritemmststatus IN ('In Process', 'Revised')";
            }
            
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND prm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND prm.createuser='" + Session["UserID"].ToString() + "'";

            List<pritemmst> dt = db.Database.SqlQuery<pritemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: PRFinishGood/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRFinishGood", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_pritemmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_pritemmst();
                tbl.pritemmstoid = ClassFunction.GenerateID("QL_pritemmst");
                tbl.pritemdate = ClassFunction.GetServerTime();
                tbl.pritemexpdate = ClassFunction.GetServerTime().AddMonths(3);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.pritemmststatus = "In Process";
                //tbl.requiremstoid = 0;
                tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");

                Session["QL_pritemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_pritemmst.Find(cmp, id);

                sSql = "SELECT pritemdtlseq, prd.itemoid, itemcode, itemlongdesc, 1.0 itemlimitqty, pritemqty, itemunitoid, gendesc itemunit, ISNULL(pritemdtlnote, '') pritemdtlnote, pritemarrdatereq FROM QL_pritemdtl prd INNER JOIN QL_mstitem m ON m.itemoid=prd.itemoid INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE prd.cmpcode='" + cmp + "' AND prd.pritemmstoid=" + id + " ORDER BY pritemdtlseq";
                Session["QL_pritemdtl"] = db.Database.SqlQuery<pritemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRFinishGood/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_pritemmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRFinishGood", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.pritemno == null)
                tbl.pritemno = "";

            List<pritemdtl> dtDtl = (List<pritemdtl>)Session["QL_pritemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.pritemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_pritemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_pritemmst");
                var dtloid = ClassFunction.GenerateID("QL_pritemdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + CompnyCode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        //tbl.requiremstoid = 0;
                        tbl.pritemno = tbl.pritemno == null ? "" : tbl.pritemno;
                        tbl.pritemmstnote = tbl.pritemmstnote == null ? "" : tbl.pritemmstnote;
                        tbl.approvaluser = tbl.approvaluser == null ? "" : tbl.approvaluser;
                        tbl.approvalcode = tbl.approvalcode == null ? "" : tbl.approvalcode;
                        tbl.approvaldatetime = tbl.approvaldatetime == null ? DateTime.Parse("1900-01-01 00:00:00") : tbl.approvaldatetime;
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? servertime : tbl.revisetime; //DateTime.ParseExact("01/01/1900", "MM/dd/yyyy")
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? servertime : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_pritemmst.Find(tbl.cmpcode, tbl.pritemmstoid) != null)
                                tbl.pritemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pritemdate);
                            tbl.divoid = divoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                            tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");
                            db.QL_pritemmst.Add(tbl);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.pritemmstoid + " WHERE tablename='QL_pritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            
                            var trndtl = db.QL_pritemdtl.Where(a => a.pritemmstoid == tbl.pritemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_pritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_pritemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_pritemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.pritemdtloid = dtloid ++;
                            tbldtl.pritemmstoid = tbl.pritemmstoid;
                            tbldtl.pritemdtlseq = i + 1;
                            tbldtl.pritemarrdatereq = dtDtl[i].pritemarrdatereq;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.pritemqty = dtDtl[i].pritemqty;
                            tbldtl.pritemunitoid = dtDtl[i].itemunitoid;
                            tbldtl.pritemdtlstatus = "";
                            tbldtl.pritemdtlnote = dtDtl[i].pritemdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            //tbldtl.requireqty = dtDtl[i].requireqty;
                            //tbldtl.requiredtloid = dtDtl[i].requiredtloid;
                            db.QL_pritemdtl.Add(tbldtl);
                            db.SaveChanges();
                        }
                        
                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_pritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.pritemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PR-FG" + tbl.pritemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_pritemmst";
                                tblApp.oid = tbl.pritemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: PRFinishGood/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRFinishGood", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_pritemmst tbl = db.QL_pritemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_pritemdtl.Where(a => a.pritemmstoid == id && a.cmpcode == cmp);
                        db.QL_pritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();
                        
                        db.QL_pritemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetItem(string cmp)
        {
            List<matitem> mstitem = new List<matitem>();
            sSql = "SELECT m.cmpcode, itemoid, itemcode, itemlongdesc, 1.0 itemlimitqty, itemunitoid, g.gendesc [itemunit] FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.cmpcode = m.cmpcode AND g.genoid = m.itemunitoid WHERE m.cmpcode = '" + cmp + "' AND m.activeflag='ACTIVE'";
            mstitem = db.Database.SqlQuery<matitem>(sSql).ToList();

            return Json(mstitem, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_pritemmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPR_Trn.rpt"));
            sSql = "SELECT prm.pritemmstoid AS prmstoid, prm.cmpcode, [BU Name] BUName, [BU Address] BUAddress, [BU City] BUCity, [BU Province] BUProvince, [BU Phone] BUPhone, [BU Email] BUEmail, [BU Phone2] BUPhone2, [BU PostCode] BUPostCode, [BU Fax1] BUFax1, [BU Fax2] BUFax2, [BU Country] BUCountry, prm.pritemno AS prno, CONVERT(VARCHAR(10), prm.pritemmstoid) AS draftno, prm.pritemdate AS prdate, g1.deptname AS department, prm.pritemmstnote AS prmstnote, prm.pritemmststatus AS prmststatus, prd.pritemdtlseq AS prdtlseq, prd.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, prd.prrawqty AS prqty, prd.requireqty, ISNULL(requireno, '') RequireNo, ISNULL(requiredatefilter, CAST('1/1/1900' AS DATETIME)) RequireDate, ISNULL(requiremstnote, '') RequireHeaderNote, ISNULL(requiremststatus, '') RequireStatus, ISNULL(r.createtime, CAST('1/1/1900' AS DATETIME)) RequireCreateDate, ISNULL(r.createuser, '') RequireCreateUser, ISNULL(requiredatetype, '') RequireDateType, g4.gendesc as prunit, prd.pritemdtlnote AS prdtlnote, prd.prrawarrdatereq AS prarrdatereq, (SELECT ISNULL(pa.profname,'-') FROM QL_mstprof pa WHERE pa.profoid=prm.approvaluser) AS UserNameApproved, prm.approvaldatetime AS ApproveTime, (SELECT ISNULL(pc.profname,'-') FROM QL_mstprof pc WHERE pc.profoid=prm.createuser) AS CreateUserName, prm.createtime AS [CreateTime] FROM QL_pritemmst prm INNER JOIN QL_pritemdtl prd ON prm.cmpcode=prd.cmpcode AND prm.pritemmstoid=prd.pritemmstoid LEFT JOIN QL_trnrequiremst r ON r.cmpcode=prm.cmpcode AND r.requiremstoid=prm.requiremstoid INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid INNER JOIN QL_mstdept g1 ON prm.cmpcode=g1.cmpcode AND prm.deptoid=g1.deptoid INNER JOIN QL_mstgen g4 ON prd.prrawunitoid=g4.genoid INNER JOIN (SELECT d.cmpcode, d.divname AS [BU Name], d.divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], d.divphone AS [BU Phone], d.divemail AS [BU Email], d.divphone2 AS [BU Phone2], d.divpostcode AS [BU PostCode], d.divfax1	AS [BU Fax1], d.divfax2	AS [BU Fax2], g3.gendesc AS [BU Country] FROM QL_mstdivision d INNER JOIN QL_mstgen g1 ON g1.genoid=d.divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) AS BU ON BU.cmpcode=prm.cmpcode WHERE prm.cmpcode='" + cmp + "' AND prm.pritemmstoid=" + id + " ORDER BY prm.pritemmstoid";
            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPR_Trn");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sUserID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PRFG.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
