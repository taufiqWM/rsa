﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Transaction
{
    public class DPJualController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trndpar
        {
            public string cmpcode { get; set; }
            public int dparoid { get; set; }
            public string dparno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime dpardate { get; set; }
            public string custname { get; set; }
            public string divgroup { get; set; }
            public string acctgdesc { get; set; }
            public string dparpaytype { get; set; }
            public string dparstatus { get; set; }
            public string dparnote { get; set; }
            public string cashbankno { get; set; }
            //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dparamt { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custtaxable { get; set; }
            public decimal custtaxvalue { get; set; }
        }

        private void InitDDL(QL_trndpar tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";

            var dparpayacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dparpayacctgoid);
            ViewBag.dparpayacctgoid = dparpayacctgoid;
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
        }

        [HttpPost]
        public ActionResult BindCustomerData(int divgroupoid)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT custoid, custcode, custname, custaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS custtaxable, ISNULL((SELECT TOP 1 CAST (gendesc AS DECIMAL) FROM QL_mstgen WHERE cmpcode='"+ CompnyCode + "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC), 0) custtaxvalue FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' and divgroupoid='"+divgroupoid+"' ORDER BY custcode";
            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            if (sVar == "NONE")
            {
                sSql = "SELECT 'APIS' cmpcode, 0 acctgoid, '-' acctgcode, 'NONE' acctgdesc, '' acctgdbcr, '' acctggrp1, '' acctggrp2, '' acctggrp3, '' acctgnote, ''acctgres1, ''	 acctgres2, '' acctgres3, '' activeflag, '' createuser, getdate() createtime, '' upduser, getdate() updtime, 0 curroid, '' acctgcodeseq";
            }
            else
            {
                sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            }            
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string dparno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(dparno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndpar tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.dparpayrefno = db.Database.SqlQuery<string>("SELECT dparpayrefno FROM QL_trndpar WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.dparoid + "").FirstOrDefault();
            ViewBag.cashbankno = db.Database.SqlQuery<string>("SELECT cashbankno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            ViewBag.cashbankamt = tbl.dparamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;
        }

        private string GenerateNo(string dpardate)
        {
            var dparno = "";
            DateTime sDate = DateTime.Parse(dpardate);
            if (CompnyCode != "")
            {
                string sNo = "DPAR/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" + CompnyCode + "' AND dparno LIKE '%" + sNo + "%' ";
                dparno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return dparno;
        }

        private string GenerateExpenseNo2(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(cashbankdate);
            if (cmp != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%' AND acctgoid=" + acctgoid;
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(cashbankdate);
            if (CompnyCode != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' AND acctgoid=" + acctgoid;
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: dparMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dp", divgroupoid, "divgroupoid");
            sSql = "SELECT dp.cmpcode, dp.dparoid, (select gendesc from ql_mstgen where genoid=dp.divgroupoid) divgroup, dparno, dpardate, custname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE dparpaytype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'BANK' ELSE 'GIRO' END) AS dparpaytype, dparstatus, dparnote, 'False' AS checkvalue, cashbankno, dparamt FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid ";
            //if (cbVoucher)
            //    sSql += " INNER JOIN QL_mstaccount ma ON cb.acctgoid=ma.acctgoid INNER JOIN QL_mstbank mb ON ma.bankoid=mb.bankoid INNER JOIN QL_mstgen mg ON mg.genoid=mb.bankmstoid ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "Where dp.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "Where dp.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND dparstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND dparstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND dparstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND dpardate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND dpardate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND dparstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND dp.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " AND dp.dparoid>0 ORDER BY CONVERT(DATETIME, dp.dpardate) DESC, dp.dparoid DESC";

            List<trndpar> dt = db.Database.SqlQuery<trndpar>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trndpar", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: dparMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl;
            string action = "New Data";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trndpar();
                tbl.dparoid = ClassFunction.GenerateID("QL_trndpar");
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.dpardate = ClassFunction.GetServerTime();
                tbl.dparduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.dparstatus = "In Process";
                tbl.dpartakegiro = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trndpar.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dparMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndpar tbl, string action, string cashbankno)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.dparno == null)
                tbl.dparno = "";
            string sErrReply = "";
            if (tbl.custoid == 0)
                ModelState.AddModelError("", "Please select CUSTOMER field!");
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select DP ACCOUNT field");
            if (tbl.dparpaytype == "0")
                ModelState.AddModelError("", "Please select Payment Type field");
            if (tbl.dparpayacctgoid == 0)
                ModelState.AddModelError("", "Please select PAYMENT ACCOUNT field!");
            if (tbl.dparpaytype != "BKM")
            {
                if (tbl.dparpaytype == "BBM")
                {
                    if (tbl.dparpayrefno == "" || tbl.dparpayrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                }
                if (tbl.dpardate > tbl.dparduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than DP DATE");
                if (tbl.dparpaytype == "BGM")
                {
                    if (tbl.dpardate > tbl.dpartakegiro)
                        ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than DP DATE!");
                }
            }
            if (tbl.dparamt <= 0)
            {
                ModelState.AddModelError("", "DP AMOUNT must be more than 0!");
            }
            else
            {
                if (!ClassFunction.isLengthAccepted("dparamt", "QL_trndpar", tbl.dparamt, ref sErrReply))
                    ModelState.AddModelError("", "DP AMOUNT must be less than MAX DP AMOUNT (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.addacctgoid1 != 0)
                if (tbl.addacctgamt1 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid2)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                }
                if (tbl.addacctgoid2 != 0)
                {
                    if (tbl.addacctgoid2 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                }
            }
            //var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.dparpaytype == "BKM"|| tbl.dparpaytype == "BBM" || tbl.dparpaytype == "BLM")
                sDueDate = tbl.dpardate;
            else
                sDueDate = tbl.dparduedate??tbl.dpardate;
            DateTime sDate = tbl.dpardate;
       
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iGiroAcctgOid = 0;
            if (tbl.dparstatus == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, sDate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(sDate.Month).ToString() + " " + sDate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.dparstatus = "In Process";
                }
                tbl.dparno = GenerateNo(tbl.dpardate.ToString("MM/dd/yyyy"));
                //cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.dparstatus = "In Process";
                //}
                //if (cRate.GetRateMonthlyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.dparstatus = "In Process";
                //}
                string sVarErr = "";
                if (tbl.dparpaytype == "BGM")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO_IN";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", tbl.cmpcode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.dparstatus = "In Process";
                }
            }
            if (!ModelState.IsValid)
                tbl.dparstatus = "In Process";

            if (ModelState.IsValid)
            {
                tbl.cmpcode = CompnyCode;
                var mstoid = ClassFunction.GenerateID("QL_trndpar");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trndpar WHERE dparoid=" + tbl.dparoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trndpar");
                    }
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankno='" + cashbankno + "'";
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        cashbankno = GenerateExpenseNo2(CompnyCode, tbl.dpardate.ToString("MM/dd/yyyy"), tbl.dparpaytype, tbl.acctgoid);
                    }
                    cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                }
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.dparamtidr = tbl.dparamt * 1;
                tbl.dparamtusd = tbl.dparamt * 1;
                tbl.dpartakegiro = (tbl.dparpaytype == "BGM" ? tbl.dpartakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.dparduedate = sDueDate;
                tbl.dparnote = (tbl.dparnote == null ? "" : ClassFunction.Tchar(tbl.dparnote));
                if (tbl.custoid != 0)
                    ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.custoid).FirstOrDefault();
                decimal cashbankamt = tbl.dparamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_trncashbankmst tblcashbank;
                        if (action == "New Data")
                        {
                            tblcashbank = new QL_trncashbankmst();
                        }
                        else
                        {
                            tblcashbank = db.QL_trncashbankmst.Where(s => s.cmpcode == tbl.cmpcode && s.cashbankoid == tbl.cashbankoid).FirstOrDefault();
                        }
                        tblcashbank.cmpcode = tbl.cmpcode;
                        tblcashbank.cashbankno = cashbankno;
                        tblcashbank.divgroupoid = tbl.divgroupoid;
                        tblcashbank.cashbankdate = tbl.dpardate;
                        tblcashbank.cashbanktype = tbl.dparpaytype;
                        tblcashbank.cashbankgroup = "dpar";
                        tblcashbank.acctgoid = tbl.dparpayacctgoid;
                        tblcashbank.curroid = tbl.curroid;
                        tblcashbank.cashbankamt = cashbankamt;
                        tblcashbank.cashbankamtidr = cashbankamt * 1;
                        tblcashbank.cashbankamtusd = cashbankamt * 1;
                        tblcashbank.personoid = tbl.custoid;
                        tblcashbank.cashbankduedate = tbl.dparduedate;
                        tblcashbank.cashbankrefno = tbl.dparpayrefno;
                        tblcashbank.cashbanknote = tbl.dparnote;
                        tblcashbank.cashbankstatus = tbl.dparstatus;
                        tblcashbank.createuser = tbl.createuser;
                        tblcashbank.createtime = tbl.createtime;
                        tblcashbank.cashbanktakegiro = (tbl.dparpaytype == "BGM" ? tbl.dpartakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                        tblcashbank.giroacctgoid = iGiroAcctgOid;
                        tblcashbank.addacctgoid1 = tbl.addacctgoid1;
                        tblcashbank.addacctgamt1 = tbl.addacctgamt1;
                        tblcashbank.addacctgoid2 = tbl.addacctgoid2;
                        tblcashbank.addacctgamt2 = tbl.addacctgamt2;
                        tblcashbank.addacctgoid3 = tbl.addacctgoid3;
                        tblcashbank.addacctgamt3 = tbl.addacctgamt3;

                        tblcashbank.cashbanktakegiroreal = (tblcashbank.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbanktakegiroreal);
                        tblcashbank.cashbankgiroreal = (tblcashbank.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbankgiroreal);
                        tblcashbank.cashbankaptype = (tblcashbank.cashbankaptype == null ? "" : tblcashbank.cashbankaptype);
                        tblcashbank.cashbanktaxtype = "NON TAX";

                        if (action == "New Data")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                            {
                                tblcashbank.cashbankoid = cashbankoid;
                                tbl.cashbankoid = cashbankoid;
                            }
                                
                            tblcashbank.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpardate);
                            tblcashbank.upduser = tbl.createuser;
                            tblcashbank.updtime = tbl.createtime;

                            db.QL_trncashbankmst.Add(tblcashbank);
                            db.SaveChanges();

                            if (db.QL_trndpar.Find(tbl.cmpcode, tbl.dparoid) != null)
                                tbl.dparoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpardate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndpar.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.dparoid + " WHERE tablename='QL_trndpar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tblcashbank.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {           
                            tblcashbank.updtime = servertime;
                            tblcashbank.upduser = Session["UserID"].ToString();
                            db.Entry(tblcashbank).State = EntityState.Modified;
                            db.SaveChanges();

                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (tbl.dparstatus == "Post")
                        {
                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DP A/P|No=" + tbl.dparno + "", tbl.dparstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            //Cash/Bank/Giro
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, (tbl.dparpaytype == "BGM" ? iGiroAcctgOid : tbl.dparpayacctgoid), "D", cashbankamt, tbl.dparno, tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, cashbankamt * 1, cashbankamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                            //Additional Cost 1+/-
                            if (tbl.addacctgamt1 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt1), tbl.dparno, tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * 1, Math.Abs(tbl.addacctgamt1) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 1 - DP A/P|No=" + tbl.dparno + "", (tbl.addacctgamt1 > 0 ? "M" : "K"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 2+/-
                            if (tbl.addacctgamt2 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt2), tbl.dparno, tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * 1, Math.Abs(tbl.addacctgamt2) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 2 - DP A/P|No=" + tbl.dparno + "", (tbl.addacctgamt2 > 0 ? "M" : "K"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 3+/-
                            if (tbl.addacctgamt3 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt3), tbl.dparno, tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * 1, Math.Abs(tbl.addacctgamt3) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 3 - DP A/P|No=" + tbl.dparno + "", (tbl.addacctgamt3 > 0 ? "M" : "K"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Uang Muka
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.dparamt, tbl.dparno, tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, tbl.dparamt * 1, tbl.dparamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iSeq += 1;

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        //if (string.IsNullOrEmpty(closing))
                        //    return RedirectToAction("Form/" + tbl.dparoid + "/" + tbl.cmpcode);
                        //else
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dparMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl = db.QL_trndpar.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trncashbank = db.QL_trncashbankmst.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == CompnyCode);
                        db.QL_trncashbankmst.RemoveRange(trncashbank);
                        db.SaveChanges();

                        db.QL_trndpar.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDPAR.rpt"));
            report.SetParameterValue("sWhere", " WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparoid IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "DownPaymentARPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}