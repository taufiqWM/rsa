﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class DebetNoteController : Controller
    {
        private  QL_RSAEntities db = new  QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class trndebetnote
        {
            public string cmpcode { get; set; }
            public int dnoid { get; set; }
            public string dnno { get; set; }
            [DataType(DataType.Date)]
            public DateTime? dndate { get; set; }
            public string divgroup { get; set; }
            public string tipe { get; set; }
            public string suppcustname { get; set; }
            public string aparno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dnamt { get; set; }
            public string dnstatus { get; set; }
            public string dnnote { get; set; }
            public string divname { get; set; }
        }

        public class apardata
        {
            public string cmpcode { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgaccount { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public string transcheckdate { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public decimal amttrans { get; set; }
            public decimal amttransidr { get; set; }
            public decimal amttransusd { get; set; }
            public decimal amtbayar { get; set; }
            public decimal amtbayaridr { get; set; }
            public decimal amtbayarusd { get; set; }
            public decimal amtbalance { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtbalanceusd { get; set; }
            public decimal aparpaidamt { get; set; }
            public decimal aparpaidamtidr { get; set; }
            public decimal aparpaidamtusd { get; set; }
            public decimal aparbalance { get; set; }
            public decimal aparbalanceidr { get; set; }
            public decimal aparbalanceusd { get; set; }
            public decimal aparamt { get; set; }
            public decimal aparamtidr { get; set; }
            public decimal aparamtusd { get; set; }
            public decimal amtdncn { get; set; }
            public decimal amtdncnidr { get; set; }
            public decimal amtdncnusd { get; set; }
            public DateTime apardate { get; set; }
            public DateTime aparcheckdate { get; set; }
            public string dntype { get; set; }
            public string aparno { get; set; }
            public string suppcustname { get; set; }
            public string dbacctgdesc { get; set; }
        }

        public class supplier
        {
            public int oid { get; set; }
            public string code { get; set; }
            public string name { get; set; }
        }

        private void InitDDL(QL_trndebetnote tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var dbacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dbacctgoid);
            ViewBag.dbacctgoid = dbacctgoid;
            var cracctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.cracctgoid);
            ViewBag.cracctgoid = cracctgoid;
        }

        [HttpPost]
        public ActionResult BindSupplierData(string cmpcode, string reftype, int divgroupoid)
        {
            List<supplier> tbl = new List<supplier>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT DISTINCT a.suppoid AS oid,a.suppcode AS code,a.suppname AS name FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,(ap.amttrans+ap.amttransidr+ap.amttransusd) apamt," +
                "ap.trnapdate,(ISNULL((SELECT SUM(ap2.amtbayar+ap2.amtbayaridr+ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS appaidamt," +
                "(ISNULL((SELECT SUM(ap2.amtbayar+ap2.amtbayaridr+ap2.amtbayarusd) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode And ap.reftype=ap2.reftype And ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS apdncnamt " +
                "FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + cmpcode + "' AND s.divgroupoid='"+divgroupoid+"' AND ISNULL(ap.payrefoid,0)=0 " +
                ") AS a WHERE ROUND(a.apamt, 4)<>ROUND(a.appaidamt+a.apdncnamt, 4) " +
                "ORDER BY a.suppcode ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd) aramt," +
                "ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS arpaidamt," +
                "(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype And ar.refoid = ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS ardncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND c.divgroupoid='" + divgroupoid + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype NOT LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.ardncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            else
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd)*-1 aramt," +
                "ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS arpaidamt," +
                "(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype And ar.refoid = ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS ardncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND c.divgroupoid='" + divgroupoid + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.ardncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataAPAR(string cmpcode, string reftype, string suppcustoid)
        {
            List<apardata> tbl = new List<apardata>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,ap.amttransidr,ap.amttransusd,vap.transno,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transcheckdate,vap.currency," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncn," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncnusd " +
                "FROM QL_conap ap INNER JOIN View_AddInfoConAP vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + cmpcode + "' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" + suppcustoid + "" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans,ar.amttransidr,ar.amttransusd,var.transno,CONVERT(VARCHAR(10),ar.trnardate,101) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate,var.currency," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncn," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype NOT LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            else
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans*-1 amttrans,ar.amttransidr*-1 amttransidr,ar.amttransusd*-1 amttransusd,var.transno," +
                "(CASE ar.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT CONVERT(VARCHAR(10),argendate,101) FROM QL_trnarretgenmst argm INNER JOIN QL_trnargenmst arm ON arm.cmpcode=argm.cmpcode AND arm.argenmstoid=argm.argenmstoid WHERE argm.cmpcode=ar.cmpcode AND argm.arretgenmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretitemmst' THEN (SELECT CONVERT(VARCHAR(10),aritemdate,101) FROM QL_trnarretitemmst arit INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=arit.cmpcode AND arm.aritemmstoid=arit.aritemmstoid WHERE arit.cmpcode=ar.cmpcode AND arit.arretitemmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretrawmst' THEN (SELECT CONVERT(VARCHAR(10),arrawdate,101) FROM QL_trnarretrawmst arrm INNER JOIN QL_trnarrawmst arm ON arm.cmpcode=arrm.cmpcode AND arm.arrawmstoid=arrm.arrawmstoid WHERE arrm.cmpcode=ar.cmpcode AND arrm.arretrawmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretsawnmst' THEN (SELECT CONVERT(VARCHAR(10),arsawndate,101) FROM QL_trnarretsawnmst arst INNER JOIN QL_trnarsawnmst arm ON arm.cmpcode=arst.cmpcode AND arm.arsawnmstoid=arst.arsawnmstoid WHERE arst.cmpcode=ar.cmpcode AND arst.arretsawnmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretspmst' THEN (SELECT CONVERT(VARCHAR(10),arspdate,101) FROM QL_trnarretspmst arsp INNER JOIN QL_trnarspmst arm ON arm.cmpcode=arsp.cmpcode AND arm.arspmstoid=arsp.arspmstoid WHERE arsp.cmpcode=ar.cmpcode AND arsp.arretspmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretlogmst' THEN (SELECT CONVERT(VARCHAR(10),arlogdate,101) FROM QL_trnarretlogmst arwp INNER JOIN QL_trnarlogmst arm ON arm.cmpcode=arwp.cmpcode AND arm.arlogmstoid=arwp.arlogmstoid WHERE arwp.cmpcode=ar.cmpcode AND arwp.arretlogmstoid=ar.refoid) END) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate," +
                "var.currency," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncn," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            tbl = db.Database.SqlQuery<apardata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, cmp) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string dnno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(dnno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndebetnote tbl)
        {
            if (tbl.reftype != "")
            {
                ViewBag.suppcustname = Session["suppcustname"];
                ViewBag.aparno = Session["aparno"];
                ViewBag.aparamt = Session["aparamt"];
                ViewBag.aparamtidr = Session["aparamtidr"];
                ViewBag.aparamtusd = Session["aparamtusd"];
                ViewBag.aparpaidamt = Session["aparpaidamt"];
                ViewBag.aparbalance = Session["aparbalance"];
                ViewBag.aparbalanceidr = Session["aparbalanceidr"];
                ViewBag.aparbalanceusd = Session["aparbalanceusd"];
                ViewBag.aparbalanceafter = Session["aparbalanceafter"];
                ViewBag.aparcheckdate = Session["aparcheckdate"];
                ViewBag.apardate = Session["apardate"];
                ViewBag.dntype = Session["dntype"];
                ViewBag.dmaxamount = Session["dmaxamount"];
                ViewBag.dbacctgdesc = Session["dbacctgdesc"];
            }
        }

        private string GenerateNo(string cmp)
        {
            var dnno = "";
            if (cmp != "")
            {
                string sNo = "DN-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dnno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndebetnote WHERE cmpcode='" + cmp + "' AND dnno LIKE '%" + sNo + "%' ";
                dnno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
            }
            return dnno;
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: debetnoteMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dn", divgroupoid, "divgroupoid");
            sSql = "SELECT * FROM (" +
            "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno, ISNULL((select gendesc from ql_mstgen where genoid=ISNULL(dn.divgroupoid,0)),'') divgroup, dn.dnnote,dn.dndate,'A/P' AS tipe,s.suppname AS suppcustname," +
            "CASE dn.reftype WHEN 'QL_trnapassetmst' THEN (SELECT apas.apassetno FROM QL_trnapassetmst apas WHERE apas.cmpcode=dn.cmpcode AND apas.apassetmstoid=dn.refoid)" +
            "WHEN 'QL_trnapgenmst' THEN (SELECT apgm.apgenno FROM QL_trnapgenmst apgm WHERE apgm.cmpcode=dn.cmpcode AND apgm.apgenmstoid=dn.refoid)" +
            "WHEN 'QL_trnapitemmst' THEN (SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode=dn.cmpcode AND apit.apitemmstoid=dn.refoid)" +
            "WHEN 'QL_trnaprawmst' THEN (SELECT aprm.aprawno FROM QL_trnaprawmst aprm WHERE aprm.cmpcode=dn.cmpcode AND aprm.aprawmstoid=dn.refoid)" +
            "WHEN 'QL_trnapservicemst' THEN (SELECT apsv.apserviceno FROM QL_trnapservicemst apsv WHERE apsv.cmpcode=dn.cmpcode AND apsv.apservicemstoid=dn.refoid)" +
            "WHEN 'QL_trnapspmst' THEN (SELECT apsp.apspno FROM QL_trnapspmst apsp WHERE apsp.cmpcode=dn.cmpcode AND apsp.apspmstoid=dn.refoid)" +
            "WHEN 'QL_trnapsubconmst' THEN (SELECT apsc.apsubconno FROM QL_trnapsubconmst apsc WHERE apsc.cmpcode=dn.cmpcode AND apsc.apsubconmstoid=dn.refoid)" +
            "WHEN 'QL_trnapimportmst' THEN (SELECT apic.apimportno FROM QL_trnapimportmst apic WHERE apic.cmpcode=dn.cmpcode AND apic.apimportmstoid=dn.refoid)" +
            "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
            "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid " +
            "WHERE left(dn.reftype,8)='ql_trnap' " +
            "UNION ALL " +
            "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno, ISNULL((select gendesc from ql_mstgen where genoid=ISNULL(dn.divgroupoid,0)),'') divgroup, dn.dnnote,dn.dndate,'A/R' AS tipe,c.custname AS suppcustname," +
            "CASE dn.reftype WHEN 'QL_trnarassetmst' THEN (SELECT aras.arassetno FROM QL_trnarassetmst aras WHERE aras.cmpcode=dn.cmpcode AND aras.arassetmstoid=dn.refoid)" +
            "WHEN 'QL_trnargenmst' THEN (SELECT argm.argenno FROM QL_trnargenmst argm WHERE argm.cmpcode=dn.cmpcode AND argm.argenmstoid=dn.refoid)" +
            "WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.aritemmstoid=dn.refoid)" +
            "WHEN 'QL_trnarrawmst' THEN (SELECT arrm.arrawno FROM QL_trnarrawmst arrm WHERE arrm.cmpcode=dn.cmpcode AND arrm.arrawmstoid=dn.refoid)" +
            "WHEN 'QL_trnarspmst' THEN (SELECT arsp.arspno FROM QL_trnarspmst arsp WHERE arsp.cmpcode=dn.cmpcode AND arsp.arspmstoid=dn.refoid)" +
            "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
            "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid " +
            "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " + sqlplus +
            //"UNION ALL " +
            //"SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno, dn.dnnote,dn.dndate,'A/R Return' AS tipe,c.custname AS suppcustname," +
            //"CASE dn.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT argm.arretgenno FROM QL_trnarretgenmst argm WHERE argm.cmpcode=dn.cmpcode AND argm.arretgenmstoid=dn.refoid)" +
            //"WHEN 'QL_trnarretitemmst' THEN (SELECT arit.arretitemno FROM QL_trnarretitemmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.arretitemmstoid=dn.refoid)" +
            //"WHEN 'QL_trnarretrawmst' THEN (SELECT arrm.arretrawno FROM QL_trnarretrawmst arrm WHERE arrm.cmpcode=dn.cmpcode AND arrm.arretrawmstoid=dn.refoid)" +
            //"WHEN 'QL_trnarretspmst' THEN (SELECT arsp.arretspno FROM QL_trnarretspmst arsp WHERE arsp.cmpcode=dn.cmpcode AND arsp.arretspmstoid=dn.refoid)" +
            //"END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue " +
            //"FROM QL_trndebetnote dn " +
            //"INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
            //"INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid " +
            //"WHERE left(dn.reftype,11)='ql_trnarret'"+ 
            ") AS a WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "a.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "a.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND dnstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND dnstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND dnstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND CONVERT(DATETIME, a.dndate)>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND CONVERT(DATETIME, a.dndate)<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND dnstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY CONVERT(DATETIME, a.dndate) DESC, a.dnoid DESC";

            List<trndebetnote> dt = db.Database.SqlQuery<trndebetnote>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trndebetnote", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________

            return View(dt);
        }

        // GET: debetnoteMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trndebetnote tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trndebetnote();
                tbl.dnoid = ClassFunction.GenerateID("QL_trndebetnote");
                tbl.dndate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.dnstatus = "In Process";
                tbl.reftype = "";
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trndebetnote.Find(cmp, id);

                List<apardata> tbldtl = new List<apardata>();
                sSql = "SELECT * FROM (" +
                "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,s.suppname AS suppcustname,dn.reftype tipe,dn.refoid,vap.transno AS aparno," +
                "dn.curroid,dn.rateoid,dn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayarusd," +
                "ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ap2.cmpcode AND dn1.dnoid=ap2.payrefoid AND ap2.trnaptype='DNAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>dn.dnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncn," +
                "ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ap2.cmpcode AND dn1.dnoid=ap2.payrefoid AND ap2.trnaptype='DNAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>dn.dnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncnidr," +
                "ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ap2.cmpcode AND dn1.dnoid=ap2.payrefoid AND ap2.trnaptype='DNAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>dn.dnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncnusd," +
                "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ap.trnapdate aparcheckdate " +
                "FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
                "INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.reftype=dn.reftype AND ap.refoid=dn.refoid AND ap.payrefoid=0 " +
                "INNER JOIN View_AddInfoConAP vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
                "WHERE left(dn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,c.custname AS suppcustname,dn.reftype AS tipe,dn.refoid,var.transno AS aparno," +
                "dn.curroid,dn.rateoid,dn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayarusd," +
                "ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncn," +
                "ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncnidr," +
                "ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                "ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncnusd," +
                "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ar.trnardate aparcheckdate " +
                "FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
                "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
                "INNER JOIN QL_conar ar ON ar.cmpcode=dn.cmpcode AND ar.reftype=dn.reftype AND ar.refoid=dn.refoid AND ar.payrefoid=0 " +
                "INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " +
                //"UNION ALL " +
                //"SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,c.custname AS suppcustname,dn.reftype AS tipe,dn.refoid,var.transno AS aparno," +
                //"dn.curroid,dn.rateoid,dn.rate2oid," +
                //"(CASE dn.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT argendate FROM QL_trnarretgenmst argm INNER JOIN QL_trnargenmst arm ON arm.cmpcode=argm.cmpcode AND arm.argenmstoid=argm.argenmstoid WHERE argm.cmpcode=dn.cmpcode AND argm.arretgenmstoid=dn.refoid)" +
                //"WHEN 'QL_trnarretitemmst' THEN (SELECT aritemdate FROM QL_trnarretitemmst arit INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=arit.cmpcode AND arm.aritemmstoid=arit.aritemmstoid WHERE arit.cmpcode=dn.cmpcode AND arit.arretitemmstoid=dn.refoid)" +
                //"WHEN 'QL_trnarretrawmst' THEN (SELECT arrawdate FROM QL_trnarretrawmst arrm INNER JOIN QL_trnarrawmst arm ON arm.cmpcode=arrm.cmpcode AND arm.arrawmstoid=arrm.arrawmstoid WHERE arrm.cmpcode=dn.cmpcode AND arrm.arretrawmstoid=dn.refoid)" +
                //"WHEN 'QL_trnarretsawnmst' THEN (SELECT arsawndate FROM QL_trnarretsawnmst arst INNER JOIN QL_trnarsawnmst arm ON arm.cmpcode=arst.cmpcode AND arm.arsawnmstoid=arst.arsawnmstoid WHERE arst.cmpcode=dn.cmpcode AND arst.arretsawnmstoid=dn.refoid)" +
                //"WHEN 'QL_trnarretspmst' THEN (SELECT arspdate FROM QL_trnarretspmst arsp INNER JOIN QL_trnarspmst arm ON arm.cmpcode=arsp.cmpcode AND arm.arspmstoid=arsp.arspmstoid WHERE arsp.cmpcode=dn.cmpcode AND arsp.arretspmstoid=dn.refoid)" +
                //"WHEN 'QL_trnarretlogmst' THEN (SELECT arlogdate FROM QL_trnarretlogmst arwp INNER JOIN QL_trnarlogmst arm ON arm.cmpcode=arwp.cmpcode AND arm.arlogmstoid=arwp.arlogmstoid WHERE arwp.cmpcode=dn.cmpcode AND arwp.arretlogmstoid=dn.refoid) END) apardate," +
                //"ar.amttrans*-1 aparamt,ar.amttransidr*-1 aparamtidr,ar.amttransusd*-1 aparamtusd," +
                //"(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayar," +
                //"(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayaridr," +
                //"(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayarusd," +
                //"ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                //"ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncn," +
                //"ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                //"ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncnidr," +
                //"ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 " +
                //"ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncnusd," +
                //"dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ar.trnardate aparcheckdate " +
                //"FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode " +
                //"INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
                //"INNER JOIN QL_conar ar ON ar.cmpcode=dn.cmpcode AND ar.reftype=dn.reftype AND ar.refoid=dn.refoid AND ar.payrefoid=0 " +
                //"INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                //"WHERE left(dn.reftype,11)='ql_trnarret' 
                ") AS a WHERE a.dnoid=" + id;
                tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();

                Session["aparno"] = tbldtl[0].aparno;
                Session["apardate"] = tbldtl[0].apardate;
                Session["aparcheckdate"] = tbldtl[0].aparcheckdate;
                if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
                {
                    Session["dntype"] = "AP";
                } else if (ClassFunction.Left(tbl.reftype.ToUpper(), 11) == "QL_TRNARRET")
                {
                    Session["dntype"] = "ARRET";
                }
                else
                {
                    Session["dntype"] = "AR";
                }
                Session["dmaxamount"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["suppcustname"] = tbldtl[0].suppcustname;

                Session["aparbalance"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["aparbalanceidr"] = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
                Session["aparbalanceusd"] = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
                Session["aparamt"] = tbldtl[0].aparamt;
                Session["aparamtidr"] = tbldtl[0].aparamtidr;
                Session["aparamtusd"] = tbldtl[0].aparamtusd;
                Session["aparpaidamt"] = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
                Session["aparbalanceafter"] = Convert.ToDecimal(Session["aparbalance"]) + (tbl.dnamt * (Session["dntype"].ToString() != "AR" ? -1 : 1));
                Session["dbacctgdesc"] = tbldtl[0].dbacctgdesc;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: debetnoteMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndebetnote tbl, string action, string closing, string aparno, string apardate, string aparcheckdate, string dntype, decimal aparbalanceafter, decimal aparbalanceidr, decimal aparbalanceusd, decimal aparbalance, decimal dmaxamount, string suppcustname, decimal aparamt, decimal aparamtidr, decimal aparamtusd , decimal aparpaidamt, string dbacctgdesc)
        {
            Session["aparno"] = aparno;
            Session["apardate"] = apardate;
            Session["aparcheckdate"] = aparcheckdate;
            Session["dntype"] = dntype;
            Session["aparbalanceafter"] = aparbalanceafter;
            Session["aparbalanceidr"] = aparbalanceidr;
            Session["aparbalanceusd"] = aparbalanceusd;
            Session["aparbalance"] = aparbalance;
            Session["dmaxamount"] = dmaxamount;
            Session["suppcustname"] = suppcustname;
            Session["aparamt"] = aparamt;
            Session["aparamtidr"] = aparamtidr;
            Session["aparamtusd"] = aparamtusd;
            Session["aparpaidamt"] = aparpaidamt;
            Session["dbacctgdesc"] = dbacctgdesc;

            var reftype = dntype;
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.dnno == null)
                tbl.dnno = "";
            string sErrReply = "";
            if (Convert.ToDateTime(tbl.dndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(apardate) || Convert.ToDateTime(tbl.dndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(aparcheckdate))
            {
                ModelState.AddModelError("", "DN DATE must be more than AP/AR DATE");
            }
            if (tbl.refoid == 0)
            {
                ModelState.AddModelError("", "Please select " + reftype + " first.");
            }
            else
            {
                if (tbl.dnamt <= 0 && aparbalance > 0)
                {
                    ModelState.AddModelError("", "Debet Note amount must be greater than 0.");
                }
                else
                {
                    if (reftype == "AP" || reftype == "ARRET")
                    {
                        if (tbl.dnamt > dmaxamount)
                        {
                            ModelState.AddModelError("", "Maximum Debet Note amount is " + dmaxamount + "");
                        }
                        else
                        {
                            if (reftype == "AP")
                            {
                                sSql = "SELECT ISNULL(SUM(amttrans - amtbayar), 0) FROM QL_conap con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND ISNULL(conflag, '')<>'Lebih Bayar'";
                            }
                            else
                            {
                                sSql = "SELECT ISNULL(SUM(amttrans - amtbayar)*-1, 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            }
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.dnoid;
                            }
                            decimal dNewBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (reftype == "AP")
                            {
                                sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr), 0) FROM QL_conap con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND ISNULL(conflag, '')<>'Lebih Bayar'";
                            }
                            else
                            {
                                sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr)*-1, 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            }
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.dnoid;
                            }
                            decimal dNewBalanceIDR = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceidr = dNewBalanceIDR;
                            if (reftype == "AP")
                            {
                                sSql = "SELECT ISNULL(SUM(amttransusd - amtbayarusd), 0) FROM QL_conap con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND ISNULL(conflag, '')<>'Lebih Bayar'";
                            }
                            else
                            {
                                sSql = "SELECT ISNULL(SUM(amttransusd - amtbayarusd)*-1, 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            }
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.dnoid;
                            }
                            decimal dNewBalanceUSD = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceusd = dNewBalanceUSD;
                            if (dmaxamount != dNewBalance)
                            {
                                dmaxamount = dNewBalance;
                                aparbalance = dNewBalance;
                            }
                            if (tbl.dnamt > dmaxamount)
                            {
                                ModelState.AddModelError("", "Maximum Debet Note has been updated by another user. Maximum Debet Note amount is " + dmaxamount + "");
                            }
                        }
                    }
                }
            }
            if (tbl.suppcustoid == 0)
                ModelState.AddModelError("", "Please select SUPPLIER/CUSTOMER field!");
            if (tbl.dbacctgoid == 0)
                ModelState.AddModelError("", "Please select DEBET ACCOUNT field");
            if (tbl.cracctgoid == 0)
                ModelState.AddModelError("", "Please select CREDIT ACCOUNT field");
            if (tbl.dnamt <= 0)
            {
                ModelState.AddModelError("", "DN AMOUNT must be more than 0!");
            }else
            {
                if (!ClassFunction.isLengthAccepted("dnamt", "QL_trndebetnote", tbl.dnamt, ref sErrReply))
                    ModelState.AddModelError("", "DP AMOUNT must be less than MAX DP AMOUNT (" + sErrReply + ") allowed stored in database!");
            }

            var cRate = new ClassRate();
            if (tbl.dnstatus == "Post")
            {
                tbl.dnno = GenerateNo(tbl.cmpcode);
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, Convert.ToDateTime(tbl.dndate)))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(tbl.dndate.Month).ToString() + " " + tbl.dndate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.dnstatus = "In Process";
                }
                cRate.SetRateValue(tbl.curroid, Convert.ToDateTime(apardate).ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.dnstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.dnstatus = "In Process";
                }  
            }
            if (!ModelState.IsValid)
                tbl.dnstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trndebetnote");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trndebetnote WHERE dnoid=" + tbl.dnoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trndebetnote");
                    }
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.dntaxtype = "NON TAX";
                tbl.dntaxamt = 0;
                tbl.dnno = (tbl.dnno == null ? "" : tbl.dnno);
                tbl.dnnote = (tbl.dnnote == null ? "" : tbl.dnnote);

                tbl.dnamtidr = tbl.dnamt * cRate.GetRateMonthlyIDRValue;
                tbl.dnamtusd = tbl.dnamt * cRate.GetRateMonthlyUSDValue;
                tbl.dntaxamtidr = tbl.dntaxamt * cRate.GetRateMonthlyIDRValue;
                tbl.dntaxamtusd = tbl.dntaxamt * cRate.GetRateMonthlyUSDValue;
                var dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conar con WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnarstatus='Post' AND (CASE WHEN trnartype LIKE 'PAYAR%' THEN ISNULL((SELECT NULLIF(payarres1, 'Kurang Bayar') FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid), '') ELSE '' END)=''").FirstOrDefault();
                if (reftype.ToUpper() == "AP")
                {
                    dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conap WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnapstatus='Post' AND ISNULL(conflag, '')<>'Lebih Bayar'").FirstOrDefault();
                }
                if (dbalancepost == 0)
                {
                    tbl.dnamtidr = aparbalanceidr;
                    tbl.dnamtusd = aparbalanceusd;
                    tbl.dntaxamtidr = 0;
                    tbl.dntaxamtusd = 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trndebetnote.Find(tbl.cmpcode, tbl.dnoid) != null)
                                tbl.dnoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndebetnote.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.dnoid + " WHERE tablename='QL_trndebetnote'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //Select OLD Data
                            var sOldType = tbl.reftype;
                            var sOldOid = tbl.refoid;
                            if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                            {
                                var conap = db.QL_conap.Where(x => x.payrefoid == tbl.dnoid && x.trnaptype == "DNAP");
                                db.QL_conap.RemoveRange(conap);
                                db.SaveChanges();

                                sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } else if (ClassFunction.Left(sOldType, 11) == "QL_TRNARRET")
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNARRET");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();

                                sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNAR");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();
                            }

                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (reftype.ToUpper() == "AP")
                        {
                            QL_conap conap = new QL_conap();
                            conap.cmpcode = tbl.cmpcode;
                            conap.conapoid = conapoid++;
                            conap.reftype = tbl.reftype;
                            conap.refoid = tbl.refoid;
                            conap.payrefoid = tbl.dnoid;
                            conap.suppoid = tbl.suppcustoid;
                            conap.acctgoid = tbl.dbacctgoid;
                            conap.trnapstatus = tbl.dnstatus;
                            conap.trnaptype = "DN" + reftype;
                            conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                            conap.paymentacctgoid = tbl.cracctgoid;
                            conap.paymentdate = tbl.dndate;
                            conap.payrefno = tbl.dnno;
                            conap.paybankoid = 0;
                            conap.payduedate = tbl.dndate;
                            conap.amttrans = 0;
                            conap.amtbayar= tbl.dnamt + tbl.dntaxamt;
                            conap.trnapnote = "DN No. " + tbl.dnno + " for A / P No. " + aparno + "";
                            conap.createuser = tbl.createuser;
                            conap.createtime = tbl.createtime;
                            conap.upduser = tbl.upduser;
                            conap.updtime = tbl.updtime;
                            conap.amttransidr = 0;
                            conap.amtbayaridr = tbl.dnamtidr + tbl.dntaxamtidr;
                            conap.amttransusd = 0;
                            conap.amtbayarusd = tbl.dnamtusd + tbl.dntaxamtusd;
                            conap.amtbayarother = 0;
                            //conap.conflag = "";

                            db.QL_conap.Add(conap);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (aparbalanceafter <= 0)
                            {
                                var sRef = tbl.reftype.ToUpper();
                                sSql = "UPDATE " + sRef + " SET " + sRef.Replace("QL_TRN", "") + "STATUS='Closed', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sRef.Replace("QL_TRN", "") + "oid=" + tbl.refoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        } else if (reftype.ToUpper() == "AR")
                        {
                            QL_conar conar = new QL_conar();
                            conar.cmpcode = tbl.cmpcode;
                            conar.conaroid = conaroid++;
                            conar.reftype = tbl.reftype;
                            conar.refoid = tbl.refoid;
                            conar.payrefoid = tbl.dnoid;
                            conar.custoid = tbl.suppcustoid;
                            conar.acctgoid = tbl.dbacctgoid;
                            conar.trnarstatus = tbl.dnstatus;
                            conar.trnartype = "DN" + reftype;
                            conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                            conar.paymentacctgoid = tbl.cracctgoid;
                            conar.paymentdate = tbl.dndate;
                            conar.payrefno = tbl.dnno;
                            conar.paybankoid = 0;
                            conar.payduedate = tbl.dndate;
                            conar.amttrans = 0;
                            conar.amtbayar = (tbl.dnamt + tbl.dntaxamt) * -1;
                            conar.trnarnote = "DN No. " + tbl.dnno + " for A / P No. " + aparno + "";
                            conar.createuser = tbl.createuser;
                            conar.createtime = tbl.createtime;
                            conar.upduser = tbl.upduser;
                            conar.updtime = tbl.updtime;
                            conar.amttransidr = 0;
                            conar.amtbayaridr = (tbl.dnamtidr + tbl.dntaxamtidr) * -1;
                            conar.amttransusd = 0;
                            conar.amtbayarusd = (tbl.dnamtusd + tbl.dntaxamtusd) * -1;
                            conar.amtbayarother = 0;

                            db.QL_conar.Add(conar);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            QL_conar conar = new QL_conar();
                            conar.cmpcode = tbl.cmpcode;
                            conar.conaroid = conaroid++;
                            conar.reftype = tbl.reftype;
                            conar.refoid = tbl.refoid;
                            conar.payrefoid = tbl.dnoid;
                            conar.custoid = tbl.suppcustoid;
                            conar.acctgoid = tbl.dbacctgoid;
                            conar.trnarstatus = tbl.dnstatus;
                            conar.trnartype = "DN" + reftype;
                            conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                            conar.paymentacctgoid = tbl.cracctgoid;
                            conar.paymentdate = tbl.dndate;
                            conar.payrefno = tbl.dnno;
                            conar.paybankoid = 0;
                            conar.payduedate = tbl.dndate;
                            conar.amttrans = 0;
                            conar.amtbayar = (tbl.dnamt + tbl.dntaxamt) * -1;
                            conar.trnarnote = "DN No. " + tbl.dnno + " for A / P No. " + aparno + "";
                            conar.createuser = tbl.createuser;
                            conar.createtime = tbl.createtime;
                            conar.upduser = tbl.upduser;
                            conar.updtime = tbl.updtime;
                            conar.amttransidr = 0;
                            conar.amtbayaridr = (tbl.dnamtidr + tbl.dntaxamtidr) * -1;
                            conar.amttransusd = 0;
                            conar.amtbayarusd = (tbl.dnamtusd + tbl.dntaxamtusd) * -1;
                            conar.amtbayarother = 0;

                            db.QL_conar.Add(conar);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (aparbalanceafter <= 0)
                            {
                                var sRef = tbl.reftype.ToUpper();
                                sSql = "UPDATE " + sRef + " SET " + sRef.Replace("QL_TRN", "") + "STATUS='Closed', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sRef.Replace("QL_TRN", "") + "oid=" + tbl.refoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (tbl.dnstatus == "Post")
                        {
                            var sDate = tbl.dndate;
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);

                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote", tbl.dnoid.ToString(), "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote", tbl.dnoid.ToString(), "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.dnoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: debetnoteMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndebetnote tbl = db.QL_trndebetnote.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Select OLD Data
                        var sOldType = tbl.reftype;
                        var sOldOid = tbl.refoid;
                        if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                        {
                            var conap = db.QL_conap.Where(x => x.payrefoid == tbl.dnoid && x.trnaptype == "DNAP");
                            db.QL_conap.RemoveRange(conap);
                            db.SaveChanges();

                            sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', upduser='" + Session["UserID"] + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (ClassFunction.Left(sOldType, 11) == "QL_TRNARRET")
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNARRET");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();

                            sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', upduser='" + Session["UserID"] + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNAR");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();
                        }

                        db.QL_trndebetnote.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDebetNote.rpt"));
            report.SetParameterValue("sWhere", " WHERE [CmpCode]='" + cmp + "' AND [Oid] IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "DebetNotePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}