﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class CreditNoteController : Controller
    {
        private  QL_RSAEntities db = new  QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class trncreditnote
        {
            public string cmpcode { get; set; }
            public int cnoid { get; set; }
            public string divgroup { get; set; }
            public string cnno { get; set; }
            [DataType(DataType.Date)]
            public DateTime? cndate { get; set; }
            public string tipe { get; set; }
            public string suppcustname { get; set; }
            public string aparno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cnamt { get; set; }
            public string cnstatus { get; set; }
            public string cnnote { get; set; }
            public string divname { get; set; }
        }

        public class apardata
        {
            public string cmpcode { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgaccount { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public string transcheckdate { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public decimal amttrans { get; set; }
            public decimal amttransidr { get; set; }
            public decimal amttransusd { get; set; }
            public decimal amtbayar { get; set; }
            public decimal amtbayaridr { get; set; }
            public decimal amtbayarusd { get; set; }
            public decimal amtbalance { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtbalanceusd { get; set; }
            public decimal aparpaidamt { get; set; }
            public decimal aparpaidamtidr { get; set; }
            public decimal aparpaidamtusd { get; set; }
            public decimal aparbalance { get; set; }
            public decimal aparbalanceidr { get; set; }
            public decimal aparbalanceusd { get; set; }
            public decimal aparamt { get; set; }
            public decimal aparamtidr { get; set; }
            public decimal aparamtusd { get; set; }
            public decimal amtdncn { get; set; }
            public decimal amtdncnidr { get; set; }
            public decimal amtdncnusd { get; set; }
            public DateTime apardate { get; set; }
            public DateTime aparcheckdate { get; set; }
            public string cntype { get; set; }
            public string aparno { get; set; }
            public string suppcustname { get; set; }
            public string dbacctgdesc { get; set; }
        }

        public class supplier
        {
            public int oid { get; set; }
            public string code { get; set; }
            public string name { get; set; }
        }

        private void InitDDL(QL_trncreditnote tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var dbacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dbacctgoid);
            ViewBag.dbacctgoid = dbacctgoid;
            var cracctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.cracctgoid);
            ViewBag.cracctgoid = cracctgoid;
        }

        [HttpPost]
        public ActionResult BindSupplierData(string cmpcode, string reftype,int divgroupoid)
        {
            List<supplier> tbl = new List<supplier>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT DISTINCT a.suppoid AS oid,a.suppcode AS code,a.suppname AS name FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,(ap.amttrans+ap.amttransidr+ap.amttransusd) apamt," +
                "ap.trnapdate,(ISNULL((SELECT SUM(ap2.amtbayar+ap2.amtbayaridr+ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS appaidamt," +
                "(ISNULL((SELECT SUM(ap2.amtbayar+ap2.amtbayaridr+ap2.amtbayarusd) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode And ap.reftype=ap2.reftype And ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('cnAP','CNAP')),0.0)) AS apcncnamt " +
                "FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + cmpcode + "' AND s.divgroupoid='" + divgroupoid + "' AND ISNULL(ap.payrefoid,0)=0 " +
                ") AS a WHERE ROUND(a.apamt, 4)<>ROUND(a.appaidamt+a.apcncnamt, 4) " +
                "ORDER BY a.suppcode ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd) aramt," +
                "ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS arpaidamt," +
                "(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype And ar.refoid = ar2.refoid " +
                "AND ar2.trnartype IN ('cnAR','CNAR')),0.0)) AS arcncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND c.divgroupoid='" + divgroupoid + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype NOT LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.arcncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            else
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd)*-1 aramt," +
                "ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS arpaidamt," +
                "(ISNULL((SELECT SUM(ar2.amtbayar+ar2.amtbayaridr+ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype And ar.refoid = ar2.refoid " +
                "AND ar2.trnartype IN ('cnARRET','CNARRET')),0.0))*-1 AS arcncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND c.divgroupoid='" + divgroupoid + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.arcncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataAPAR(string cmpcode, string reftype, string suppcustoid)
        {
            List<apardata> tbl = new List<apardata>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,ap.amttransidr,ap.amttransusd,vap.transno,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transcheckdate,vap.currency," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncn," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ap2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncnusd " +
                "FROM QL_conap ap INNER JOIN View_AddInfoConAP vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + cmpcode + "' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" + suppcustoid + "" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans,ar.amttransidr,ar.amttransusd,var.transno,CONVERT(VARCHAR(10),ar.trnardate,101) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate,var.currency," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncn," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype NOT LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            else
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans*-1 amttrans,ar.amttransidr*-1 amttransidr,ar.amttransusd*-1 amttransusd,var.transno," +
                "(CASE ar.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT CONVERT(VARCHAR(10),argendate,101) FROM QL_trnarretgenmst argm INNER JOIN QL_trnargenmst arm ON arm.cmpcode=argm.cmpcode AND arm.argenmstoid=argm.argenmstoid WHERE argm.cmpcode=ar.cmpcode AND argm.arretgenmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretitemmst' THEN (SELECT CONVERT(VARCHAR(10),aritemdate,101) FROM QL_trnarretitemmst arit INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=arit.cmpcode AND arm.aritemmstoid=arit.aritemmstoid WHERE arit.cmpcode=ar.cmpcode AND arit.arretitemmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretrawmst' THEN (SELECT CONVERT(VARCHAR(10),arrawdate,101) FROM QL_trnarretrawmst arrm INNER JOIN QL_trnarrawmst arm ON arm.cmpcode=arrm.cmpcode AND arm.arrawmstoid=arrm.arrawmstoid WHERE arrm.cmpcode=ar.cmpcode AND arrm.arretrawmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretsawnmst' THEN (SELECT CONVERT(VARCHAR(10),arsawndate,101) FROM QL_trnarretsawnmst arst INNER JOIN QL_trnarsawnmst arm ON arm.cmpcode=arst.cmpcode AND arm.arsawnmstoid=arst.arsawnmstoid WHERE arst.cmpcode=ar.cmpcode AND arst.arretsawnmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretspmst' THEN (SELECT CONVERT(VARCHAR(10),arspdate,101) FROM QL_trnarretspmst arsp INNER JOIN QL_trnarspmst arm ON arm.cmpcode=arsp.cmpcode AND arm.arspmstoid=arsp.arspmstoid WHERE arsp.cmpcode=ar.cmpcode AND arsp.arretspmstoid=ar.refoid)" +
                "WHEN 'QL_trnarretlogmst' THEN (SELECT CONVERT(VARCHAR(10),arlogdate,101) FROM QL_trnarretlogmst arwp INNER JOIN QL_trnarlogmst arm ON arm.cmpcode=arwp.cmpcode AND arm.arlogmstoid=arwp.arlogmstoid WHERE arwp.cmpcode=ar.cmpcode AND arwp.arretlogmstoid=ar.refoid) END) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate," +
                "var.currency," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayarusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncn," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncnidr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ar2.trnartype IN ('DNARRET','CNARRET')),0.0))*-1 AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + cmpcode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            tbl = db.Database.SqlQuery<apardata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, cmp) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cnno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cnno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncreditnote tbl)
        {
            if (tbl.reftype != "")
            {
                ViewBag.suppcustname = Session["suppcustname"];
                ViewBag.aparno = Session["aparno"];
                ViewBag.aparamt = Session["aparamt"];
                ViewBag.aparamtidr = Session["aparamtidr"];
                ViewBag.aparamtusd = Session["aparamtusd"];
                ViewBag.aparpaidamt = Session["aparpaidamt"];
                ViewBag.aparbalance = Session["aparbalance"];
                ViewBag.aparbalanceidr = Session["aparbalanceidr"];
                ViewBag.aparbalanceusd = Session["aparbalanceusd"];
                ViewBag.aparbalanceafter = Session["aparbalanceafter"];
                ViewBag.aparcheckdate = Session["aparcheckdate"];
                ViewBag.apardate = Session["apardate"];
                ViewBag.cntype = Session["cntype"];
                ViewBag.dmaxamount = Session["dmaxamount"];
                ViewBag.dbacctgdesc = Session["dbacctgdesc"];
            }
        }

        private string GenerateNo(string cmp)
        {
            var cnno = "";
            if (cmp != "")
            {
                string sNo = "CN-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cnno, 6) AS INTEGER)) + 1, 1) AS IcnEW FROM QL_trncreditnote WHERE cmpcode='" + cmp + "' AND cnno LIKE '%" + sNo + "%' ";
                cnno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
            }
            return cnno;
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: creditnoteMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cn", divgroupoid, "divgroupoid");
            sSql = "SELECT * FROM (" +
            "SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno, ISNULL((select gendesc from ql_mstgen where genoid=ISNULL(cn.divgroupoid,0)),'') divgroup, cn.cnnote,cn.cndate,'A/P' AS tipe,s.suppname AS suppcustname," +
            "CASE cn.reftype WHEN 'QL_trnapassetmst' THEN (SELECT apas.apassetno FROM QL_trnapassetmst apas WHERE apas.cmpcode=cn.cmpcode AND apas.apassetmstoid=cn.refoid)" +
            "WHEN 'QL_trnapgenmst' THEN (SELECT apgm.apgenno FROM QL_trnapgenmst apgm WHERE apgm.cmpcode=cn.cmpcode AND apgm.apgenmstoid=cn.refoid)" +
            "WHEN 'QL_trnapitemmst' THEN (SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apitemmstoid=cn.refoid)" +
            "WHEN 'QL_trnaprawmst' THEN (SELECT aprm.aprawno FROM QL_trnaprawmst aprm WHERE aprm.cmpcode=cn.cmpcode AND aprm.aprawmstoid=cn.refoid)" +
            "WHEN 'QL_trnapservicemst' THEN (SELECT apsv.apserviceno FROM QL_trnapservicemst apsv WHERE apsv.cmpcode=cn.cmpcode AND apsv.apservicemstoid=cn.refoid)" +
            "WHEN 'QL_trnapspmst' THEN (SELECT apsp.apspno FROM QL_trnapspmst apsp WHERE apsp.cmpcode=cn.cmpcode AND apsp.apspmstoid=cn.refoid)" +
            "WHEN 'QL_trnapsubconmst' THEN (SELECT apsc.apsubconno FROM QL_trnapsubconmst apsc WHERE apsc.cmpcode=cn.cmpcode AND apsc.apsubconmstoid=cn.refoid)" +
            "WHEN 'QL_trnapimportmst' THEN (SELECT apic.apimportno FROM QL_trnapimportmst apic WHERE apic.cmpcode=cn.cmpcode AND apic.apimportmstoid=cn.refoid)" +
            "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue " +
            "FROM QL_trncreditnote cn " +
            "INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
            "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid " +
            "WHERE left(cn.reftype,8)='ql_trnap' " + sqlplus +
            "UNION ALL " +
            "SELECT cn.cmpcode,dv.divname,cn.cnoid, cn.cnno, ISNULL((select gendesc from ql_mstgen where genoid=ISNULL(cn.divgroupoid,0)),'') divgroup, cn.cnnote,cn.cndate,'A/R' AS tipe,c.custname AS suppcustname," +
            "CASE cn.reftype WHEN 'QL_trnarassetmst' THEN (SELECT aras.arassetno FROM QL_trnarassetmst aras WHERE aras.cmpcode=cn.cmpcode AND aras.arassetmstoid=cn.refoid)" +
            "WHEN 'QL_trnargenmst' THEN (SELECT argm.argenno FROM QL_trnargenmst argm WHERE argm.cmpcode=cn.cmpcode AND argm.argenmstoid=cn.refoid)" +
            "WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.aritemmstoid=cn.refoid)" +
            "WHEN 'QL_trnarrawmst' THEN (SELECT arrm.arrawno FROM QL_trnarrawmst arrm WHERE arrm.cmpcode=cn.cmpcode AND arrm.arrawmstoid=cn.refoid)" +
            "WHEN 'QL_trnarspmst' THEN (SELECT arsp.arspno FROM QL_trnarspmst arsp WHERE arsp.cmpcode=cn.cmpcode AND arsp.arspmstoid=cn.refoid)" +
            "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue " +
            "FROM QL_trncreditnote cn " +
            "INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
            "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid " +
            "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " + sqlplus+
            //"UNION ALL " +
            //"SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno, cn.cnnote,cn.cndate,'A/R Return' AS tipe,c.custname AS suppcustname," +
            //"CASE cn.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT argm.arretgenno FROM QL_trnarretgenmst argm WHERE argm.cmpcode=cn.cmpcode AND argm.arretgenmstoid=cn.refoid)" +
            //"WHEN 'QL_trnarretitemmst' THEN (SELECT arit.arretitemno FROM QL_trnarretitemmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.arretitemmstoid=cn.refoid)" +
            //"WHEN 'QL_trnarretrawmst' THEN (SELECT arrm.arretrawno FROM QL_trnarretrawmst arrm WHERE arrm.cmpcode=cn.cmpcode AND arrm.arretrawmstoid=cn.refoid)" +
            //"WHEN 'QL_trnarretspmst' THEN (SELECT arsp.arretspno FROM QL_trnarretspmst arsp WHERE arsp.cmpcode=cn.cmpcode AND arsp.arretspmstoid=cn.refoid)" +
            //"END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue " +
            //"FROM QL_trncreditnote cn " +
            //"INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
            //"INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid " +
            //"WHERE left(cn.reftype,11)='ql_trnarret'" + 
            ") AS a WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "a.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "a.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cnstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cnstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cnstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND CONVERT(DATETIME, a.cndate)>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND CONVERT(DATETIME, a.cndate)<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cnstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY CONVERT(DATETIME, a.cndate) DESC, a.cnoid DESC";

            List<trncreditnote> dt = db.Database.SqlQuery<trncreditnote>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trncreditnote", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: creditnoteMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trncreditnote tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trncreditnote();
                tbl.cnoid = ClassFunction.GenerateID("QL_trncreditnote");
                tbl.cndate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cnstatus = "In Process";
                tbl.reftype = "";
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncreditnote.Find(cmp, id);

                List<apardata> tbldtl = new List<apardata>();
                sSql = "SELECT * FROM (" +
                "SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,s.suppname AS suppcustname,cn.reftype tipe,cn.refoid,vap.transno AS aparno," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid " +
                "WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid " +
                "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayarusd," +
                "ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ap2.cmpcode AND cn1.cnoid=ap2.payrefoid AND ap2.trnaptype='cnAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>cn.cnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncn," +
                "ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ap2.cmpcode AND cn1.cnoid=ap2.payrefoid AND ap2.trnaptype='cnAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>cn.cnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncnidr," +
                "ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ap2.cmpcode AND cn1.cnoid=ap2.payrefoid AND ap2.trnaptype='cnAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>cn.cnoid " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ap2.cmpcode AND cn.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 " +
                "WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ap.trnapdate aparcheckdate " +
                "FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conap ap ON ap.cmpcode=cn.cmpcode AND ap.reftype=cn.reftype AND ap.refoid=cn.refoid AND ap.payrefoid=0 " +
                "INNER JOIN View_AddInfoConAP vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
                "WHERE left(cn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,c.custname AS suppcustname,cn.reftype AS tipe,cn.refoid,var.transno AS aparno," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                "WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                "AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayarusd," +
                "ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncn," +
                "ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncnidr," +
                "ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                "ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+" +
                "ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                "ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 " +
                "WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ar.trnardate aparcheckdate " +
                "FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
                "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conar ar ON ar.cmpcode=cn.cmpcode AND ar.reftype=cn.reftype AND ar.refoid=cn.refoid AND ar.payrefoid=0 " +
                "INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " +
                //"UNION ALL " +
                //"SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,c.custname AS suppcustname,cn.reftype AS tipe,cn.refoid,var.transno AS aparno," +
                //"cn.curroid,cn.rateoid,cn.rate2oid," +
                //"(CASE cn.reftype WHEN 'QL_trnarretgenmst' THEN (SELECT argendate FROM QL_trnarretgenmst argm INNER JOIN QL_trnargenmst arm ON arm.cmpcode=argm.cmpcode AND arm.argenmstoid=argm.argenmstoid WHERE argm.cmpcode=cn.cmpcode AND argm.arretgenmstoid=cn.refoid)" +
                //"WHEN 'QL_trnarretitemmst' THEN (SELECT aritemdate FROM QL_trnarretitemmst arit INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=arit.cmpcode AND arm.aritemmstoid=arit.aritemmstoid WHERE arit.cmpcode=cn.cmpcode AND arit.arretitemmstoid=cn.refoid)" +
                //"WHEN 'QL_trnarretrawmst' THEN (SELECT arrawdate FROM QL_trnarretrawmst arrm INNER JOIN QL_trnarrawmst arm ON arm.cmpcode=arrm.cmpcode AND arm.arrawmstoid=arrm.arrawmstoid WHERE arrm.cmpcode=cn.cmpcode AND arrm.arretrawmstoid=cn.refoid)" +
                //"WHEN 'QL_trnarretsawnmst' THEN (SELECT arsawndate FROM QL_trnarretsawnmst arst INNER JOIN QL_trnarsawnmst arm ON arm.cmpcode=arst.cmpcode AND arm.arsawnmstoid=arst.arsawnmstoid WHERE arst.cmpcode=cn.cmpcode AND arst.arretsawnmstoid=cn.refoid)" +
                //"WHEN 'QL_trnarretspmst' THEN (SELECT arspdate FROM QL_trnarretspmst arsp INNER JOIN QL_trnarspmst arm ON arm.cmpcode=arsp.cmpcode AND arm.arspmstoid=arsp.arspmstoid WHERE arsp.cmpcode=cn.cmpcode AND arsp.arretspmstoid=cn.refoid)" +
                //"WHEN 'QL_trnarretlogmst' THEN (SELECT arlogdate FROM QL_trnarretlogmst arwp INNER JOIN QL_trnarlogmst arm ON arm.cmpcode=arwp.cmpcode AND arm.arlogmstoid=arwp.arlogmstoid WHERE arwp.cmpcode=cn.cmpcode AND arwp.arretlogmstoid=cn.refoid) END) apardate," +
                //"ar.amttrans*-1 aparamt,ar.amttransidr*-1 aparamtidr,ar.amttransusd*-1 aparamtusd," +
                //"(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayar," +
                //"(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayaridr," +
                //"(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid " +
                //"WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid " +
                //"AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0))*-1 AS amtbayarusd," +
                //"ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                //"ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncn," +
                //"ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                //"ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncnidr," +
                //"ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 " +
                //"ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='cnARRET' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1+" +
                //"ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn " +
                //"ON cn.cmpcode=ar2.cmpcode AND cn.cnoid=ar2.payrefoid AND ar2.trnartype='CNARRET' AND ar2.payrefoid<>0 " +
                //"WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)*-1 AS amtdncnusd," +
                //"cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ar.trnardate aparcheckdate " +
                //"FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode " +
                //"INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                //"INNER JOIN QL_conar ar ON ar.cmpcode=cn.cmpcode AND ar.reftype=cn.reftype AND ar.refoid=cn.refoid AND ar.payrefoid=0 " +
                //"INNER JOIN View_AddInfoConAR var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                //"WHERE left(cn.reftype,11)='ql_trnarret' 
                ") AS a WHERE a.cnoid=" + id;
                tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();

                Session["aparno"] = tbldtl[0].aparno;
                Session["apardate"] = tbldtl[0].apardate;
                Session["aparcheckdate"] = tbldtl[0].aparcheckdate;
                if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
                {
                    Session["cntype"] = "AP";
                } else if (ClassFunction.Left(tbl.reftype.ToUpper(), 11) == "QL_TRNARRET")
                {
                    Session["cntype"] = "ARRET";
                }
                else
                {
                    Session["cntype"] = "AR";
                }
                Session["dmaxamount"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["suppcustname"] = tbldtl[0].suppcustname;

                Session["aparbalance"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["aparbalanceidr"] = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
                Session["aparbalanceusd"] = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
                Session["aparamt"] = tbldtl[0].aparamt;
                Session["aparamtidr"] = tbldtl[0].aparamtidr;
                Session["aparamtusd"] = tbldtl[0].aparamtusd;
                Session["aparpaidamt"] = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
                Session["aparbalanceafter"] = Convert.ToDecimal(Session["aparbalance"]) + (tbl.cnamt * (Session["cntype"].ToString() == "AR" ? -1 : 1));
                Session["dbacctgdesc"] = tbldtl[0].dbacctgdesc;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: creditnoteMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncreditnote tbl, string action, string closing, string aparno, string apardate, string aparcheckdate, string cntype, decimal aparbalanceafter, decimal aparbalanceidr, decimal aparbalanceusd, decimal aparbalance, decimal dmaxamount, string suppcustname, decimal aparamt, decimal aparamtidr, decimal aparamtusd , decimal aparpaidamt, string dbacctgdesc)
        {
            Session["aparno"] = aparno;
            Session["apardate"] = apardate;
            Session["aparcheckdate"] = aparcheckdate;
            Session["cntype"] = cntype;
            Session["aparbalanceafter"] = aparbalanceafter;
            Session["aparbalanceidr"] = aparbalanceidr;
            Session["aparbalanceusd"] = aparbalanceusd;
            Session["aparbalance"] = aparbalance;
            Session["dmaxamount"] = dmaxamount;
            Session["suppcustname"] = suppcustname;
            Session["aparamt"] = aparamt;
            Session["aparamtidr"] = aparamtidr;
            Session["aparamtusd"] = aparamtusd;
            Session["aparpaidamt"] = aparpaidamt;
            Session["dbacctgdesc"] = dbacctgdesc;

            var reftype = cntype;
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.cnno == null)
                tbl.cnno = "";
            string sErrReply = "";
            if (Convert.ToDateTime(tbl.cndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(apardate) || Convert.ToDateTime(tbl.cndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(aparcheckdate))
            {
                ModelState.AddModelError("", "CN DATE must be more than AP/AR DATE");
            }
            if (tbl.refoid == 0)
            {
                ModelState.AddModelError("", "Please select " + reftype + " first.");
            }
            else
            {
                if (tbl.cnamt <= 0 && aparbalance > 0)
                {
                    ModelState.AddModelError("", "Credit Note amount must be greater than 0.");
                }
                else
                {
                    if (reftype == "AR")
                    {
                        if (tbl.cnamt > dmaxamount)
                        {
                            ModelState.AddModelError("", "Maximum Credit Note amount is " + dmaxamount + "");
                        }
                        else
                        {
                            sSql = "SELECT ISNULL(SUM(amttrans - amtbayar), 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr), 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalanceIDR = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceidr = cnewBalanceIDR;
                            sSql = "SELECT ISNULL(SUM(amttransusd - amtbayarusd), 0) FROM QL_conar con LEFT JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid AND ISNULL(payarres1, '')<>'Lebih Bayar' WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "' AND payrefoid NOT IN (SELECT payaroid FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND ISNULL(payarres1, '')='Lebih Bayar' AND pay.reftype=con.reftype AND pay.refoid=con.refoid)";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalanceUSD = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceusd = cnewBalanceUSD;
                            if (dmaxamount != cnewBalance)
                            {
                                dmaxamount = cnewBalance;
                                aparbalance = cnewBalance;
                            }
                            if (tbl.cnamt > dmaxamount)
                            {
                                ModelState.AddModelError("", "Maximum Credit Note has been updated by another user. Maximum Credit Note amount is " + dmaxamount + "");
                            }
                        }
                    }
                }
            }
            if (tbl.suppcustoid == 0)
                ModelState.AddModelError("", "Please select SUPPLIER/CUSTOMER field!");
            if (tbl.dbacctgoid == 0)
                ModelState.AddModelError("", "Please select DEBET ACCOUNT field");
            if (tbl.cracctgoid == 0)
                ModelState.AddModelError("", "Please select CREDIT ACCOUNT field");
            if (tbl.cnamt <= 0)
            {
                ModelState.AddModelError("", "cn AMOUNT must be more than 0!");
            }else
            {
                if (!ClassFunction.isLengthAccepted("cnamt", "QL_trncreditnote", tbl.cnamt, ref sErrReply))
                    ModelState.AddModelError("", "DP AMOUNT must be less than MAX DP AMOUNT (" + sErrReply + ") allowed stored in database!");
            }

            var cRate = new ClassRate();
            if (tbl.cnstatus == "Post")
            {
                tbl.cnno = GenerateNo(tbl.cmpcode);
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, Convert.ToDateTime(tbl.cndate)))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(tbl.cndate.Month).ToString() + " " + tbl.cndate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.cnstatus = "In Process";
                }
                cRate.SetRateValue(tbl.curroid, Convert.ToDateTime(apardate).ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cnstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cnstatus = "In Process";
                }  
            }
            if (!ModelState.IsValid)
                tbl.cnstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncreditnote");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncreditnote WHERE cnoid=" + tbl.cnoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncreditnote");
                    }
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cntaxtype = "NON TAX";
                tbl.cntaxamt = 0;
                tbl.cnno = (tbl.cnno == null ? "" : tbl.cnno);
                tbl.cnnote = (tbl.cnnote == null ? "" : tbl.cnnote);

                tbl.cnamtidr = tbl.cnamt * cRate.GetRateMonthlyIDRValue;
                tbl.cnamtusd = tbl.cnamt * cRate.GetRateMonthlyUSDValue;
                tbl.cntaxamtidr = tbl.cntaxamt * cRate.GetRateMonthlyIDRValue;
                tbl.cntaxamtusd = tbl.cntaxamt * cRate.GetRateMonthlyUSDValue;
                var dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conar con WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnarstatus='Post' AND (CASE WHEN trnartype LIKE 'PAYAR%' THEN ISNULL((SELECT NULLIF(payarres1, 'Kurang Bayar') FROM QL_trnpayar pay WHERE pay.cmpcode=con.cmpcode AND payaroid=con.payrefoid), '') ELSE '' END)=''").FirstOrDefault();
                if (reftype.ToUpper() == "AP")
                {
                    dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conap WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnapstatus='Post' AND ISNULL(conflag, '')<>'Lebih Bayar'").FirstOrDefault();
                }
                if (dbalancepost == 0)
                {
                    tbl.cnamtidr = aparbalanceidr;
                    tbl.cnamtusd = aparbalanceusd;
                    tbl.cntaxamtidr = 0;
                    tbl.cntaxamtusd = 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trncreditnote.Find(tbl.cmpcode, tbl.cnoid) != null)
                                tbl.cnoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncreditnote.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.cnoid + " WHERE tablename='QL_trncreditnote'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //Select OLD Data
                            var sOldType = tbl.reftype;
                            var sOldOid = tbl.refoid;
                            if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                            {
                                var conap = db.QL_conap.Where(x => x.payrefoid == tbl.cnoid && x.trnaptype == "CNAP");
                                db.QL_conap.RemoveRange(conap);
                                db.SaveChanges();
                            } else if (ClassFunction.Left(sOldType, 11) == "QL_TRNARRET")
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNARRET");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();
                            }
                            else
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNAR");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();

                                sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (reftype.ToUpper() == "AP")
                        {
                            QL_conap conap = new QL_conap();
                            conap.cmpcode = tbl.cmpcode;
                            conap.conapoid = conapoid++;
                            conap.reftype = tbl.reftype;
                            conap.refoid = tbl.refoid;
                            conap.payrefoid = tbl.cnoid;
                            conap.suppoid = tbl.suppcustoid;
                            conap.acctgoid = tbl.dbacctgoid;
                            conap.trnapstatus = tbl.cnstatus;
                            conap.trnaptype = "cn" + reftype;
                            conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                            conap.paymentacctgoid = tbl.cracctgoid;
                            conap.paymentdate = tbl.cndate;
                            conap.payrefno = tbl.cnno;
                            conap.paybankoid = 0;
                            conap.payduedate = tbl.cndate;
                            conap.amttrans = 0;
                            conap.amtbayar = (tbl.cnamt + tbl.cntaxamt) * -1;
                            conap.trnapnote = "CN No. " + tbl.cnno + " for A / P No. " + aparno + "";
                            conap.createuser = tbl.createuser;
                            conap.createtime = tbl.createtime;
                            conap.upduser = tbl.upduser;
                            conap.updtime = tbl.updtime;
                            conap.amttransidr = 0;
                            conap.amtbayaridr = (tbl.cnamtidr + tbl.cntaxamtidr) * -1;
                            conap.amttransusd = 0;
                            conap.amtbayarusd = (tbl.cnamtusd + tbl.cntaxamtusd) * -1;
                            conap.amtbayarother = 0;
                          

                            db.QL_conap.Add(conap);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        } else if (reftype.ToUpper() == "AR")
                        {
                            QL_conar conar = new QL_conar();
                            conar.cmpcode = tbl.cmpcode;
                            conar.conaroid = conaroid++;
                            conar.reftype = tbl.reftype;
                            conar.refoid = tbl.refoid;
                            conar.payrefoid = tbl.cnoid;
                            conar.custoid = tbl.suppcustoid;
                            conar.acctgoid = tbl.dbacctgoid;
                            conar.trnarstatus = tbl.cnstatus;
                            conar.trnartype = "cn" + reftype;
                            conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                            conar.paymentacctgoid = tbl.cracctgoid;
                            conar.paymentdate = tbl.cndate;
                            conar.payrefno = tbl.cnno;
                            conar.paybankoid = 0;
                            conar.payduedate = tbl.cndate;
                            conar.amttrans = 0;
                            conar.amtbayar = tbl.cnamt + tbl.cntaxamt;
                            conar.trnarnote = "CN No. " + tbl.cnno + " for A / P No. " + aparno + "";
                            conar.createuser = tbl.createuser;
                            conar.createtime = tbl.createtime;
                            conar.upduser = tbl.upduser;
                            conar.updtime = tbl.updtime;
                            conar.amttransidr = 0;
                            conar.amtbayaridr = tbl.cnamtidr + tbl.cntaxamtidr;
                            conar.amttransusd = 0;
                            conar.amtbayarusd = tbl.cnamtusd + tbl.cntaxamtusd;
                            conar.amtbayarother = 0;

                            db.QL_conar.Add(conar);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (aparbalanceafter <= 0)
                            {
                                var sRef = tbl.reftype.ToUpper();
                                sSql = "UPDATE " + sRef + " SET " + sRef.Replace("QL_TRN", "") + "STATUS='Closed', closeuser='" + Session["UserID"] + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sRef.Replace("QL_TRN", "") + "oid=" + tbl.refoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            QL_conar conar = new QL_conar();
                            conar.cmpcode = tbl.cmpcode;
                            conar.conaroid = conaroid++;
                            conar.reftype = tbl.reftype;
                            conar.refoid = tbl.refoid;
                            conar.payrefoid = tbl.cnoid;
                            conar.custoid = tbl.suppcustoid;
                            conar.acctgoid = tbl.dbacctgoid;
                            conar.trnarstatus = tbl.cnstatus;
                            conar.trnartype = "cn" + reftype;
                            conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                            conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                            conar.paymentacctgoid = tbl.cracctgoid;
                            conar.paymentdate = tbl.cndate;
                            conar.payrefno = tbl.cnno;
                            conar.paybankoid = 0;
                            conar.payduedate = tbl.cndate;
                            conar.amttrans = 0;
                            conar.amtbayar = tbl.cnamt + tbl.cntaxamt;
                            conar.trnarnote = "CN No. " + tbl.cnno + " for A / P No. " + aparno + "";
                            conar.createuser = tbl.createuser;
                            conar.createtime = tbl.createtime;
                            conar.upduser = tbl.upduser;
                            conar.updtime = tbl.updtime;
                            conar.amttransidr = 0;
                            conar.amtbayaridr = tbl.cnamtidr + tbl.cntaxamtidr;
                            conar.amttransusd = 0;
                            conar.amtbayarusd = tbl.cnamtusd + tbl.cntaxamtusd;
                            conar.amtbayarother = 0;

                            db.QL_conar.Add(conar);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.cnstatus == "Post")
                        {
                            var sDate = tbl.cndate;
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);

                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid??0));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote", tbl.cnoid.ToString(), "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote", tbl.cnoid.ToString(), "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.cnoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: creditnoteMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncreditnote tbl = db.QL_trncreditnote.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Select OLD Data
                        var sOldType = tbl.reftype;
                        var sOldOid = tbl.refoid;
                        if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                        {
                            var conap = db.QL_conap.Where(x => x.payrefoid == tbl.cnoid && x.trnaptype == "CNAP");
                            db.QL_conap.RemoveRange(conap);
                            db.SaveChanges();
                        }
                        else if (ClassFunction.Left(sOldType, 11) == "QL_TRNARRET")
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNARRET");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();
                        }
                        else
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNAR");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();
                        }

                        if (ClassFunction.Left(sOldType.ToUpper(), 11) != "QL_TRNARRET" || ClassFunction.Left(sOldType.ToUpper(), 8) != "QL_TRNAP")
                        {
                            sSql = "UPDATE " + sOldType.ToUpper() + " SET " + sOldType.ToUpper().Replace("QL_TRN", "") + "STATUS='Approved', upduser='" + Session["UserID"] + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + tbl.cmpcode + "' AND " + sOldType.ToUpper().Replace("QL_TRN", "") + "OID=" + sOldOid;
                                db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.QL_trncreditnote.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCreditNote.rpt"));
            report.SetParameterValue("sWhere", " WHERE [CmpCode]='" + cmp + "' AND [Oid] IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CreditNotePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}