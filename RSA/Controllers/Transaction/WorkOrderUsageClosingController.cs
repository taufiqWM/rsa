﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class WorkOrderUsageClosingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();

        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public WorkOrderUsageClosingController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class wodtl1
        {
            public int wodtl1seq { get; set; }
            public string sorawno { get; set; }
            public string wodtl1reftypedesc { get; set; }
            public string matrawlongdesc { get; set; }
            public string matrawcode { get; set; }
            public decimal wodtl1qty { get; set; }
            public string wodtl1unit { get; set; }
            public string wodtl1note { get; set; }
        }

        public class wodtl2
        {
            public int wodtl2seq { get; set; }
            public string wodtl1seqdesc { get; set; }
            public string route_from { get; set; }
            public string route_to { get; set; }
            public string wodtl2reftypedesc { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal wodtl2qty { get; set; }
            public string wodtl2unit { get; set; }
            public string wodtl2note { get; set; }
        }

        public class wodtl3
        {
            public int wodtl2seq { get; set; }
            public int wodtl3seq { get; set; }
            public string wodtl1seqdesc { get; set; }
            public string wodtl2seqdesc { get; set; }
            public string wodtl3reftypedesc { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal wodtl3qty { get; set; }
            public string wodtl3unit { get; set; }
            public string wodtl3note { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetWOData(string cmp)
        {
            var result = "";
            JsonResult js = null;
            List<QL_trnwomst> tbl = new List<QL_trnwomst>();
            try
            {
                sSql = "SELECT * FROM QL_trnwomst wom WHERE wom.cmpcode='" + cmp + "' AND isnull(wom.womstres2,'')='' and womststatus in ('Post','Closed')";
                tbl = db.Database.SqlQuery<QL_trnwomst>(sSql).ToList();
                if (tbl.Count == 0)
                    result = "Data Not Found!";
            } catch(Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            InitDDL();
            return View();            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveData(string cmp, int womstoid)
        {
            var msg = ""; var result = "failed";
            if (string.IsNullOrEmpty(cmp))
                msg += "Please select BUSINESS UNIT!<br />";
            if (womstoid == 0)
                msg += "Please select WO NO. field!<br />";

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_trnwomst tbl = db.QL_trnwomst.Find(cmp, womstoid);

                        tbl.womstres2 = "Closed";
                        tbl.updtime = ClassFunction.GetServerTime();
                        tbl.upduser = Session["UserID"].ToString();
                        db.Entry(tbl).State = EntityState.Modified;

                        result = "success";
                        db.SaveChanges();
                        objTrans.Commit();

                        msg = "Usage For WO No. " + tbl.wono + " have been Closed successfully<br />";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}