﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class APAsetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class apassetmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int apassetmstoid { get; set; }
            public string apassetno { get; set; }
            public DateTime apassetdate { get; set; }
            public DateTime apassetdatetakegiro { get; set; }
            public DateTime duedate { get; set; }
            public string suppname { get; set; }
            public string apassetmststatus { get; set; }
            public string apassetmstnote { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apassetgrandtotal { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apassetgrandtotalsupp { get; set; }
            public string divname { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
        }

        public class apassetdtl
        {
            public int apassetdtlseq { get; set; }
            public int poassetmstoid { get; set; }
            public string poassetno { get; set; }
            public int poassetdtloid { get; set; }
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
            public int mrassetdtloid { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal apassetqty { get; set; }
            public int apassetunitoid { get; set; }
            public string apassetunit { get; set; }
            public decimal apassetprice { get; set; }
            public decimal apassetdtlamt { get; set; }
            public string apassetdtldisctype { get; set; }
            public decimal apassetdtldiscvalue { get; set; }
            public decimal apassetdtldiscamt { get; set; }
            public decimal apassetdtlnetto { get; set; }
            public decimal apassetdtltaxamt { get; set; }
            public string apassetdtlnote { get; set; }
            public string apassetdtlres1 { get; set; }
            public string apassetdtlres2 { get; set; }
        }

        private void InitDDL(QL_trnapassetmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var apassetpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.apassetpaytypeoid);
            ViewBag.apassetpaytypeoid = apassetpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp, int apassetmstoid, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            //sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid FROM QL_mstsupp s INNER JOIN QL_trnregistermst regm ON regm.suppoid=s.suppoid INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode=regm.cmpcode AND mrm.registermstoid=regm.registermstoid WHERE regm.cmpcode='" + cmp + "' AND registertype='gen' AND registermststatus='Closed' AND ISNULL(mrassetmstres1, '')='' AND mrm.mrassetmststatus='Post' AND regm.registermstoid NOT IN (SELECT DISTINCT registermstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode=apd.cmpcode AND apm.apassetmstoid=apd.apassetmstoid WHERE apd.cmpcode='" + cmp + "' AND apassetmststatus<>'Rejected' AND apm.apassetmstoid <> " + apassetmstoid + ") ORDER BY suppcode ";
            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid " +
                "FROM QL_mstsupp s " +
                "INNER JOIN QL_trnpoassetmst regm ON regm.suppoid = s.suppoid " +
                "INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.poassetmstoid = regm.poassetmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND poassetmststatus IN('Approved', 'Closed') and s.divgroupoid=" + divgroupoid + " and mrm.divgroupoid=" + divgroupoid + " AND ISNULL(mrassetmstres1, '')= '' AND mrm.mrassetmststatus = 'Post' AND regm.poassetmstoid NOT IN(SELECT DISTINCT poassetmstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = apd.cmpcode AND apm.apassetmstoid = apd.apassetmstoid WHERE apd.cmpcode = '" + cmp + "' AND apassetmststatus <> 'Rejected' AND apm.apassetmstoid <> " + apassetmstoid + ") ORDER BY suppcode";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int poassetmstoid { get; set; }
            public string poassetno { get; set; }
            public DateTime poassetdate { get; set; }
            public string poassetdocrefno { get; set; }
            public string poassetmstnote { get; set; }
            public string poflag { get; set; }
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int apassetmstoid, int curroid, int divgroupoid)
        {
            cmp = CompnyCode;
            List<pomst> tbl = new List<pomst>();
            sSql = "SELECT DISTINCT regm.poassetmstoid, regm.poassetno, regm.poassetdate, '' [poassetdocrefno], ISNULL(regm.poassetmstnote, '') [poassetmstnote], mrm.mrassetmstoid, mrm.mrassetno " +
                "FROM QL_trnpoassetmst regm " +
                "INNER JOIN QL_trnpoassetdtl regd ON regd.cmpcode = regm.cmpcode AND regd.poassetmstoid = regm.poassetmstoid " +
                "INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.poassetmstoid = regm.poassetmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND regm.suppoid = " + suppoid + " AND regm.poassetmststatus IN ('Approved', 'Closed') AND regm.poassetmstoid NOT IN(SELECT poassetmstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = apd.cmpcode AND apm.apassetmstoid = apd.apassetmstoid WHERE apd.cmpcode = '" + cmp + "' AND apassetmststatus <> 'Rejected' AND apm.apassetmstoid <> " + apassetmstoid + ") AND ISNULL(mrassetmstres1, '') = '' AND mrm.mrassetmststatus = 'Post' /*AND regm.poassetmstoid NOT IN(SELECT pretm.poassetmstoid FROM QL_trnpretassetmst pretm WHERE pretm.cmpcode = '" + cmp + "' AND pretassetmststatus IN ('In Process', 'Revised', 'In Approval'))*/ AND regm.curroid = " + curroid + " and regm.divgroupoid=" + divgroupoid + " ORDER BY regm.poassetmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int mrassetmstoid, string apassetdtlres1, string apassetdtlres2)
        {

            List<apassetdtl> tbl = new List<apassetdtl>();

            sSql = "SELECT apassetdtlseq, poassetmstoid, poassetno, poassetdtloid, mrassetmstoid, mrassetno, mrassetdtloid, matgenoid, matgencode, matgenlongdesc, apassetqty, apassetunitoid, apassetunit, apassetprice,(apassetqty * apassetprice) AS apassetdtlamt, apassetdtldisctype, apassetdtldiscvalue, " +
                "(CASE apassetdtldisctype WHEN 'P' THEN(apassetqty * apassetprice) * (apassetdtldiscvalue / 100) ELSE(apassetdtldiscvalue * apassetqty) END)apassetdtldiscamt, " +
                "((apassetqty * apassetprice) - (CASE apassetdtldisctype WHEN 'P' THEN(apassetqty * apassetprice) * (apassetdtldiscvalue / 100) ELSE(apassetdtldiscvalue * apassetqty) END)) AS apassetdtlnetto, " +
                "(CASE poassettaxtype WHEN 'TAX' THEN(((apassetqty * apassetprice) - (CASE apassetdtldisctype WHEN 'P' THEN(apassetqty * apassetprice) * (apassetdtldiscvalue / 100) ELSE(apassetdtldiscvalue * apassetqty) END)) *poassettaxamt) / 100 ELSE 0 END) apassetdtltaxamt, " +
                "apassetdtlnote ,apassetdtlres1 ,apassetdtlres2 " +
                "FROM (SELECT 0 AS apassetdtlseq, regm.poassetmstoid, regm.poassetno, regd.poassetdtloid, mrm.mrassetmstoid, mrassetno, mrassetdtloid, mrd.mrassetrefoid matgenoid, matgencode, matgenlongdesc, (mrassetqty - ISNULL((SELECT SUM(pretassetqty) FROM QL_trnpretassetdtl pretd INNER JOIN QL_trnpretassetmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretassetmstoid = pretd.pretassetmstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mrassetdtloid = mrd.mrassetdtloid AND pretassetmststatus <> 'Rejected'), 0.0)) AS apassetqty, mrassetunitoid AS apassetunitoid, gendesc AS apassetunit, poassetprice apassetprice, poassetdtldisctype apassetdtldisctype, (CASE poassetdtldisctype WHEN 'A' THEN(poassetdtldiscvalue / poassetqty) ELSE poassetdtldiscvalue END) AS apassetdtldiscvalue, poassettaxtype, poassettaxamt, '' apassetdtlnote, '" + apassetdtlres1 + "' AS apassetdtlres1, '" + apassetdtlres2 + "' AS apassetdtlres2 FROM QL_trnpoassetdtl regd INNER JOIN QL_trnpoassetmst regm ON regm.cmpcode = regd.cmpcode AND regm.poassetmstoid = regd.poassetmstoid INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = regd.cmpcode  AND mrm.poassetmstoid = regd.poassetmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode = regd.cmpcode AND mrm.mrassetmstoid = mrd.mrassetmstoid  AND mrd.poassetdtloid = regd.poassetdtloid INNER JOIN QL_mstmatgen m ON m.matgenoid = mrd.mrassetrefoid INNER JOIN QL_mstgen g ON genoid = matgenunitoid WHERE regd.cmpcode = '" + cmp + "' AND mrm.mrassetmstoid = " + mrassetmstoid + " AND ISNULL(mrassetdtlres1, '')= '' AND ISNULL(mrassetmstres1, '')= '' AND mrassetmststatus = 'Post' AND mrassetdtlstatus = '') AS tbl";

            tbl = db.Database.SqlQuery<apassetdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<apassetdtl> dtDtl)
        {
            Session["QL_trnapassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnapassetdtl"] == null)
            {
                Session["QL_trnapassetdtl"] = new List<apassetdtl>();
            }

            List<apassetdtl> dataDtl = (List<apassetdtl>)Session["QL_trnapassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnapassetmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            //ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnapassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: apassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "apm", divgroupoid, "divgroupoid");
            sSql = "SELECT apassetmstoid, (select gendesc from ql_mstgen where genoid=apm.divgroupoid) divgroup, apassetno, apassetdate, suppname, apassetmststatus, apassetmstnote, divname, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'gen' AS formtype, 'False' AS checkvalue, apassetgrandtotal , apassetdatetakegiro,  CONVERT(DATETIME,DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g1.gendesc AS INT) END),apassetdate), 101) duedate FROM QL_trnapassetmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=apm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=apassetpaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "apm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "apm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND apassetmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND apassetmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND apassetmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND apassetdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND apassetdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND apassetmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND apm.createuser='" + Session["UserID"].ToString() + "'";
            sSql += "AND apassetmstoid>0 ORDER BY CONVERT(DATETIME, apassetdate) DESC, apassetmstoid DESC ";

            List<apassetmst> dt = db.Database.SqlQuery<apassetmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnapassetmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        [HttpPost]
        public ActionResult generateNo(string cmp, int apmstoid)
        {
            sSql = "SELECT apassetno FROM QL_trnapassetmst WHERE cmpcode='" + cmp + "' AND apassetmstoid=" + apmstoid;
            string sNoAP = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (string.IsNullOrEmpty(sNoAP))
            {

                string sNo = "APRM-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapassetmst WHERE cmpcode='" + cmp + "' AND apassetno LIKE '" + sNo + "%'";

                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

                sNo = sNo + sCounter;
                return Json(sNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(sNoAP, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: apassetMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnapassetmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnapassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.apassetmstoid = ClassFunction.GenerateID("QL_trnapassetmst");
                tbl.apassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.apassetmststatus = "In Process";

                Session["QL_trnapassetdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnapassetmst.Find(cmp, id);

                sSql = "SELECT apassetdtlseq, apd.poassetmstoid, poassetno, poassetdtloid, apd.mrassetmstoid, mrassetno, mrassetdtloid, apd.apassetrefoid matgenoid, matgencode, matgenlongdesc, apassetqty, apassetunitoid, gendesc AS apassetunit, apassetprice, apassetdtlamt, apassetdtldisctype, apassetdtldiscvalue, apassetdtldiscamt, apassetdtlnetto, apassetdtltaxamt, apassetdtlnote, ISNULL(apassetdtlres1, '') AS apassetdtlres1, ISNULL(apassetdtlres2, '') AS apassetdtlres2 " +
                    "FROM QL_trnapassetdtl apd " +
                    "INNER JOIN QL_trnpoassetmst regm ON regm.cmpcode = apd.cmpcode AND regm.poassetmstoid = apd.poassetmstoid " +
                    "INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = apd.cmpcode AND mrm.mrassetmstoid = apd.mrassetmstoid AND mrm.poassetmstoid = regm.poassetmstoid " +
                    "INNER JOIN QL_mstmatgen m ON m.matgenoid = apd.apassetrefoid " +
                    "INNER JOIN QL_mstgen g ON genoid = apassetunitoid " +
                    "WHERE apassetmstoid = " + id + " AND apd.cmpcode = '" + cmp + "' ORDER BY apassetdtlseq  ";
                Session["QL_trnapassetdtl"] = db.Database.SqlQuery<apassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapassetmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.apassetno == null)
                tbl.apassetno = "";

            List<apassetdtl> dtDtl = (List<apassetdtl>)Session["QL_trnapassetdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnmrassetmst mrm WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + dtDtl[i].mrassetmstoid + " AND mrassetmststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND mrassetmstoid NOT IN (SELECT apd.mrassetmstoid FROM QL_trnapassetdtl apd WHERE apd.cmpcode=mrm.cmpcode AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "PO No. " + dtDtl[i].poassetno + " has been used by another data. Please cancel this transaction or use another PO No.!");
                    }
                }
            }

            var msg = "";

            cRate.SetRateValue(tbl.curroid, tbl.apassetdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.apassetmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                //AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapassetmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapassetmst' AND activeflag='ACTIVE'").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.apassetmststatus == "Revised")
                tbl.apassetmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnapassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapassetdtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.apassettotalamtidr = (tbl.apassettotalamt * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotalamtusd = (tbl.apassettotalamt * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassettotaldiscidr = (tbl.apassettotaldisc * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotaldiscusd = (tbl.apassettotaldisc * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassettotaltaxidr = (tbl.apassettotaltax * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotaltaxusd = (tbl.apassettotaltax * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassetgrandtotalidr = (tbl.apassetgrandtotal * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassetgrandtotalusd = (tbl.apassetgrandtotal * decimal.Parse(tbl.apassetrate2tousd));

                //tbl.cancelseq = 0;
                //tbl.apcloseuser = "";
                //tbl.apclosetime = DateTime.Parse("1/1/1900 00:00:00");
                if (tbl.apassetmstnote == null)
                    tbl.apassetmstnote = "";
                if (tbl.apassettotaldisc == null)
                    tbl.apassettotaldisc = 0;
                if (tbl.apassettotaldiscidr == null)
                    tbl.apassettotaldiscidr = 0;
                if (tbl.apassettotaldiscusd == null)
                    tbl.apassettotaldiscusd = 0;
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = DateTime.Parse("1/1/1900 00:00:00");
                tbl.rejecttime = DateTime.Parse("1/1/1900 00:00:00");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnapassetmst.Find(tbl.cmpcode, tbl.apassetmstoid) != null)
                                tbl.apassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.apassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnapassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.apassetmstoid + " WHERE tablename='QL_trnapassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE MRDTL SET mrassetdtlstatus='' FROM (SELECT mrassetdtlstatus FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl apd ON apd.cmpcode=mrd.cmpcode AND apd.mrassetmstoid=mrd.mrassetmstoid AND apd.mrassetdtloid=mrd.mrassetdtloid  WHERE apd.cmpcode='" + tbl.cmpcode + "' AND apd.apassetmstoid=" + tbl.apassetmstoid + ") AS MRDTL ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapassetdtl.Where(a => a.apassetmstoid == tbl.apassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnapassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].apassetdtlnote == null)
                                dtDtl[i].apassetdtlnote = "";

                            tbldtl = new QL_trnapassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.apassetdtloid = dtloid++;
                            tbldtl.apassetmstoid = tbl.apassetmstoid;
                            tbldtl.apassetdtlseq = i + 1;
                            tbldtl.poassetdtloid = dtDtl[i].poassetdtloid;
                            tbldtl.poassetmstoid = dtDtl[i].poassetmstoid;
                            tbldtl.mrassetdtloid = dtDtl[i].mrassetdtloid;
                            tbldtl.mrassetmstoid = dtDtl[i].mrassetmstoid;
                            tbldtl.apassetreftype = "QL_mstmatgen";
                            tbldtl.apassetrefoid = dtDtl[i].matgenoid;
                            tbldtl.apassetqty = dtDtl[i].apassetqty;
                            tbldtl.apassetunitoid = dtDtl[i].apassetunitoid;
                            tbldtl.apassetprice = dtDtl[i].apassetprice;
                            tbldtl.apassetdtlamt = dtDtl[i].apassetdtlamt;
                            tbldtl.apassetdtldisctype = dtDtl[i].apassetdtldisctype;
                            tbldtl.apassetdtldiscvalue = dtDtl[i].apassetdtldiscvalue;
                            tbldtl.apassetdtldiscamt = dtDtl[i].apassetdtldiscamt;
                            tbldtl.apassetdtlnetto = dtDtl[i].apassetdtlnetto;
                            tbldtl.apassetdtltaxamt = dtDtl[i].apassetdtltaxamt;
                            tbldtl.apassetdtlnote = dtDtl[i].apassetdtlnote;
                            tbldtl.apassetdtlstatus = "";
                            tbldtl.apassetdtlres1 = dtDtl[i].apassetdtlres1;
                            tbldtl.apassetdtlres2 = dtDtl[i].apassetdtlres2;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnapassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetdtloid=" + dtDtl[i].mrassetdtloid + " AND mrassetmstoid=" + dtDtl[i].mrassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + dtDtl[i].mrassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnapassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.apassetmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AP-gen" + tbl.apassetmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnapassetmst";
                                tblApp.oid = tbl.apassetmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.apassetmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            tbl.apassetmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapassetmst tbl = db.QL_trnapassetmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetdtloid IN (SELECT mrassetdtloid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();



                        var trndtl = db.QL_trnapassetdtl.Where(a => a.apassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnapassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPTrn.rpt"));

            //report.SetParameterValue("sRegister", "(SELECT registerno FROM QL_trnregistermst regm WHERE regm.cmpcode=apd.cmpcode AND regm.registermstoid=apd.registermstoid )");
            report.SetParameterValue("sRegister", "(SELECT poassetno FROM QL_trnpoassetmst regm WHERE regm.cmpcode=apd.cmpcode AND regm.poassetmstoid=apd.poassetmstoid )");
            report.SetParameterValue("sMatCode", "(SELECT matgencode FROM QL_mstmatgen m WHERE m.matgenoid=apd.apassetrefoid)");
            report.SetParameterValue("sMatDesc", "(SELECT matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=apd.apassetrefoid)");
            report.SetParameterValue("sType", "asset");
            report.SetParameterValue("sHeader", "A/P ASET PRINT OUT");
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + cmp + "' AND apm.apassetmstoid IN (" + id + ")");


            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "apassetMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}