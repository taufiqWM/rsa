﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class SOCancellationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class somst
        {
            public string cmpcode { get; set; }
            public int somstoid { get; set; }
            public int custoid { get; set; }
            public string sono { get; set; }
            public string sType { get; set; }
            public string sodate { get; set; }
            public string somststatus { get; set; }
            public string somstnote { get; set; }
            public string custname { get; set; }
            public decimal sograndtotalamt { get; set; }
        }

        public class sodtl
        {
            public string cmpcode { get; set; }
            public int sodtloid { get; set; }
            public int somstoid { get; set; }
            public string sotype { get; set; }
            public int sodtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal soqty { get; set; }
            public string sounit { get; set; }
            public string sodtlnote { get; set; }
            public string sodtlstatus { get; set; }
            public string somststatus { get; set; }
            public decimal soprice { get; set; }
            public int assetmstoid { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetSalesOrderData(string cmp, string sType)
        {
            List<somst> tbl = new List<somst>();
            sSql = "SELECT so" + sType + "mstoid AS somstoid, So" + sType + "no AS sono, CONVERT(VARCHAR(10), So" + sType + "date, 101) AS sodate, so" + sType + "mststatus AS somststatus, so" + sType + "mstnote AS somstnote, c.custname, so" + sType + "grandtotalamt AS sograndtotalamt FROM QL_trnso" + sType + "mst s INNER JOIN QL_mstcust C ON s.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE s.cmpcode='" + cmp + "' AND (so" + sType + "mststatus='Approved') AND so" + sType + "mstoid NOT IN (SELECT so" + sType + "mstoid FROM QL_trndo" + sType + "dtl dod INNER JOIN QL_trndo" + sType + "mst dom ON dod.do" + sType + "mstoid=dom.do" + sType + "mstoid WHERE dod.cmpcode='" + cmp + "' AND dom.do" + sType + "mststatus<>'Cancel')";
            if (sType.ToUpper() == "ITEM")
            {
                sSql += " AND so" + sType + "mstoid NOT IN (SELECT somstoid FROM QL_trnwomst wom  WHERE wom.cmpcode='" + cmp + "' and sotype='QL_trnsoitemmst' AND womststatus<>'Cancel')";
            }else if(sType.ToUpper() == "RAW")
            {
                sSql += " AND so" + sType + "mstoid NOT IN (SELECT somstoid FROM QL_trnwomst wom  WHERE wom.cmpcode='" + cmp + "' and sotype='QL_trnsorawmst' AND womststatus<>'Cancel')";
            }
            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND s.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " ORDER BY so" + sType + "date DESC, somstoid DESC";

            tbl = db.Database.SqlQuery<somst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<sodtl> tbl = new List<sodtl>();
            if (sType.ToUpper() == "RAW")
            {
                sSql = "SELECT sod.sorawdtloid AS sodtloid, sod.sorawdtlseq AS sodtlseq, sod.sorawdtloid AS matoid, '' AS matcode,sodtldesc AS matlongdesc, sod.sorawqty AS soqty, g.gendesc AS sounit, sod.sorawdtlnote AS sodtlnote, sod.sorawdtlstatus AS sodtlstatus, sorawprice as soprice, 0 assetmstoid FROM QL_trnsorawdtl sod INNER JOIN QL_mstgen g ON sod.sorawunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.sorawmstoid=" + iOid;
            } else if (sType.ToUpper() == "GEN")
            {
                sSql = "SELECT sod.sogendtloid AS sodtloid, sod.sogendtlseq AS sodtlseq, sod.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, sod.sogenqty AS soqty, g.gendesc AS sounit, sod.sogendtlnote AS sodtlnote, sod.sogendtlstatus AS sodtlstatus, sogenprice as soprice, 0 assetmstoid FROM QL_trnsogendtl sod INNER JOIN QL_mstmatgen m ON sod.matgenoid=m.matgenoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON sod.sogenunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.sogenmstoid=" + iOid;
            } else if (sType.ToUpper() == "SP")
            {
                sSql = "SELECT sod.sospdtloid AS sodtloid, sod.sospdtlseq AS sodtlseq, sod.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, sod.sospqty AS soqty, g.gendesc AS sounit, sod.sospdtlnote AS sodtlnote, sod.sospdtlstatus AS sodtlstatus, sospprice as soprice, 0 assetmstoid FROM QL_trnsospdtl sod INNER JOIN QL_mstsparepart m ON sod.sparepartoid=m.sparepartoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON sod.sospunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.sospmstoid=" + iOid;
            } else if (sType.ToUpper() == "ITEM")
            {
                sSql = "SELECT sod.soitemdtloid AS sodtloid, sod.soitemdtlseq AS sodtlseq, sod.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, sod.soitemqty AS soqty, g.gendesc AS sounit, sod.soitemdtlnote AS sodtlnote, sod.soitemdtlstatus AS sodtlstatus, soitemprice as soprice, 0 assetmstoid FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON sod.itemoid=m.itemoid AND m.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON sod.soitemunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.soitemmstoid=" + iOid;
            } else if (sType.ToUpper() == "ASSET")
            {
                sSql = "SELECT sod.soassetdtloid AS sodtloid, sod.soassetdtlseq AS sodtlseq, fam.refoid AS matoid, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matcode, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS matlongdesc, sod.soassetqty AS soqty, g.gendesc AS sounit, sod.soassetdtlnote AS sodtlnote, sod.soassetdtlstatus AS sodtlstatus, sod.soassetprice as soprice, fam.assetmstoid FROM QL_trnsoassetdtl sod INNER JOIN QL_assetmst fam ON fam.assetmstoid=sod.assetmstoid INNER JOIN QL_mstgen g ON sod.soassetunitoid=g.genoid WHERE sod.cmpcode='" + cmp + "' AND sod.soassetmstoid=" + iOid;
            } else if (sType.ToUpper() == "LOG")
            {
                sSql = "SELECT sod.sologdtloid AS sodtloid, sod.sologdtlseq AS sodtlseq, sod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, sod.sologqty AS soqty, g.gendesc AS sounit, sod.sologdtlnote AS sodtlnote, sod.sologdtlstatus AS sodtlstatus, sologprice as soprice FROM QL_trnsologdtl sod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=sod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON sod.sologunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.sologmstoid=" + iOid;
            } else if (sType.ToUpper() == "SAWN")
            {
                sSql = "SELECT sod.sosawndtloid AS sodtloid, sod.sosawndtlseq AS sodtlseq, sod.cat3oid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, sod.sosawnqty AS soqty, g.gendesc AS sounit, sod.sosawndtlnote AS sodtlnote, sod.sosawndtlstatus AS sodtlstatus, sosawnprice as soprice FROM QL_trnsosawndtl sod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=sod.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON sod.sosawnunitoid=g.genoid AND g.activeflag='ACTIVE' WHERE sod.cmpcode='" + cmp + "' AND sod.sosawnmstoid=" + iOid;
            }
            tbl = db.Database.SqlQuery<sodtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<sodtl> dtDtl)
        {
            Session["QL_trnsodtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<sodtl> dtDtl = (List<sodtl>)Session["QL_trnsodtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].somstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    sSql = "SELECT COUNT(*) FROM QL_trnso" + dtDtl[0].sotype + "mst WHERE so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND so" + dtDtl[0].sotype + "mststatus='Cancel'";
                    var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck > 0)
                    {
                        ModelState.AddModelError("", "This data has been canceled by another user. Please CANCEL this transaction and try again!");
                    }
                    else
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trndo" + dtDtl[0].sotype + "dtl dod INNER JOIN QL_trndo" + dtDtl[0].sotype + "mst dom ON dod.do" + dtDtl[0].sotype + "mstoid=dom.do" + dtDtl[0].sotype + "mstoid WHERE so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND dom.do" + dtDtl[0].sotype + "mststatus<>'Cancel' ";
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "This data has been created DO by another user. Please CANCEL this transaction and try again!");
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            sSql = "UPDATE QL_trnso" + dtDtl[0].sotype + "mst SET so" + dtDtl[0].sotype + "mststatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            if (dtDtl[0].somststatus == "In Approval")
                            {
                                sSql = "UPDATE QL_approval SET statusrequest='Cancel', event='Cancel' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND tablename='QL_trnso" + dtDtl[0].sotype + "mst' AND oid=" + dtDtl[0].somstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (dtDtl[0].sotype.ToUpper() == "ASSET")
                            {
                                if (dtDtl != null)
                                {
                                    if (dtDtl.Count() > 0)
                                    {
                                        for (var i = 0; i < dtDtl.Count(); i++)
                                        {
                                            sSql = "UPDATE QL_assetmst SET assetmstflag='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND assetmstoid=" + dtDtl[i].assetmstoid;
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                            objTrans.Commit();
                            Session["QL_trnsodtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}