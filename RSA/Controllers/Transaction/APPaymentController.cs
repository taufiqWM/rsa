﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class APPaymentController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string divgroup { get; set; }
            public string cashbankno { get; set; }
            public DateTime cashbankdate { get; set; }
            public string suppname { get; set; }
            public string cashbanktype { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string divname { get; set; }           
            public decimal cashbankamt { get; set; }
            public string createuser { get; set; }
        }

        public class payap
        {
            public int payapseq { get; set; }
            public int suppoid { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public string payaprefno { get; set; }
            public DateTime transdate { get; set; }
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
            public decimal amttrans { get; set; }
            public decimal amtpaid { get; set; }
            public decimal amtbalance { get; set; }
            public decimal payapamt { get; set; }
            public decimal payapamtidr { get; set; }
            public decimal payapamtusd { get; set; }
            public string payapnote { get; set; }
        }

        [HttpPost]
        public string GenerateCashBankNo(string cashbankdate, string cashbanktype)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(cashbankdate);
            string sNo = cashbanktype + "-" + sDate.ToString("yy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + 6 + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%'";
            cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6) ;

            return cashbankno;
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: APPayment
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cb", divgroupoid, "divgroupoid");

            sSql = "SELECT cb.cmpcode, (select gendesc from ql_mstgen where genoid=cb.divgroupoid) divgroup, cashbankoid, cashbankno, cashbankdate, suppname, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BGK' THEN 'GIRO/CHEQUE' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS cashbanktype, cashbankstatus, cashbanknote, '' divname, cashbankamt, cb.createuser FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=cb.refsuppoid WHERE cb.cashbankgroup='AP'";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND cb.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cashbankstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cashbankstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cashbankstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND cashbankdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cashbankstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cb.createuser='" + Session["UserID"].ToString() + "'";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "appayment", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: APPayment/PrintReport?id=&cmp=&isbbk=
        public ActionResult PrintReport(int id, bool isbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPaymentAPAll.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", "WHERE cashbankgroup='AP' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid=" + id + "");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", Session["UserName"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "APPaymentAllPrintOut.pdf");
        }

        // GET: APPayment/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "New Data";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.cashbankgroup = "AP";
                tbl.cashbankresamt = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trnpayap"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);

              
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY personname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;

            var sVar = "VAR_CASH";
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                sVar = "VAR_BANK";
            else if (tbl.cashbanktype == "BLK")
                sVar = "VAR_DP_AP";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            //var acctgoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLTripleField>(GetQueryBindListCOA(sVar)).ToList(), "ifield", "sfield");
            ViewBag.acctgoid = acctgoid;
            sSql = GetQueryBindListCOA("VAR_ADD_COST");
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
            ViewBag.addacctgoid1_temp = (tbl.addacctgoid1.ToString() == null ? '0' : tbl.addacctgoid1);
            ViewBag.addacctgoid2_temp = (tbl.addacctgoid2.ToString() == null ? '0' : tbl.addacctgoid2);
            ViewBag.addacctgoid3_temp = (tbl.addacctgoid3.ToString() == null ? '0' : tbl.addacctgoid3);
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupplierData(string action, int id, int curroid, int divgroupoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_mstsupp> tbl = new List<QL_mstsupp>();
                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 's.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstsupp') AND name NOT IN ('suppnote') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT DISTINCT "+ cols +", g.gendesc AS suppnote FROM QL_conap ap INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstgen g ON g.genoid=supppaymentoid WHERE ap.cmpcode='"+ CompnyCode +"' AND ap.trnapstatus='Post' AND ISNULL((SELECT pap.payapres1 FROM QL_trnpayap pap WHERE pap.cmpcode=ap.cmpcode AND s.divgroupoid='"+divgroupoid+"' AND pap.payapoid=ap.payrefoid),'')<>'Lebih Bayar'AND ap.amttrans>0 GROUP BY "+ cols +", g.gendesc HAVING SUM(ap.amttrans)>SUM(ap.amtbayar) ORDER BY suppcode";               
                tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch(Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }        

        [HttpPost]
        public ActionResult GetDPData(int curroid, int suppoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_trndpap> tbl = new List<QL_trndpap>();

                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'dp.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_trndpap') AND name NOT IN ('dpappayrefno', 'dpapduedate', 'dpapamt') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT " + cols + ", acctgdesc dpappayrefno, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) dpapamt, (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpapduedate FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ORDER BY dp.dpapoid";
                tbl = db.Database.SqlQuery<QL_trndpap>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch(Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<int>("SELECT supppaymentoid FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstsupp s INNER JOIN QL_mstgen g ON genoid=supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.email = db.Database.SqlQuery<string>("SELECT suppemail FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            if (tbl.cashbanktype == "BLK")
            {
                ViewBag.dpno = db.Database.SqlQuery<string>("SELECT dpapno FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dpapoid=" + tbl.giroacctgoid + "").FirstOrDefault();
            }
        }

        //[HttpPost]
        //public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        //{
        //    return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetDataDetails(int suppoid, int curroid, int cashbankoid)
        {            
            JsonResult js = null;
            try
            {
                List<payap> tbl = new List<payap>();

                sSql = "SELECT *, 0 payapseq, amtpaid,(amttrans - amtpaid) amtbalance, 0.0000 payapamt FROM (";
                sSql += " SELECT con.refoid refoid, ap.apitemno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.aprawno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnaprawmst ap ON ap.cmpcode=con.cmpcode AND ap.aprawmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaprawmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apgenno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnapgenmst ap ON ap.cmpcode=con.cmpcode AND ap.apgenmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapgenmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apserviceno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnapservicemst ap ON ap.cmpcode=con.cmpcode AND ap.apservicemstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapservicemst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apspno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnapspmst ap ON ap.cmpcode=con.cmpcode AND ap.apspmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapspmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apassetno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON ap.cmpcode=con.cmpcode AND ap.apassetmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                sSql += ") con WHERE amttrans>amtpaid";

                tbl = db.Database.SqlQuery<payap>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<payap> dtDtl)
        {
            Session["QL_trnpayap"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            List<payap> dtl = new List<payap>();

            try
            {
                sSql = "DECLARE @oid as INTEGER; DECLARE @cmpcode as VARCHAR(30) SET @oid = " + id + "; SET @cmpcode = '" + CompnyCode + "' ";
                sSql += " SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apitemno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apitemgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnapitemmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apitemmstoid AND pay.reftype = 'QL_trnapitemmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apassetno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apassetgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnapassetmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apassetmstoid AND pay.reftype = 'QL_trnapassetmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.aprawno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.aprawgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnaprawmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.aprawmstoid AND pay.reftype = 'QL_trnaprawmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apgenno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apgengrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnapgenmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apgenmstoid AND pay.reftype = 'QL_trnapgenmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apspno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apspgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnapspmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apspmstoid AND pay.reftype = 'QL_trnapspmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apserviceno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apservicegrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance FROM QL_trnpayap pay INNER JOIN QL_trnapservicemst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apservicemstoid AND pay.reftype = 'QL_trnapservicemst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
            
                dtl = db.Database.SqlQuery<payap>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
                else
                {
                    var no = 0;
                    for (int i = 0; i < dtl.Count; i++)
                    {
                        no = no + 1; ;
                        dtl[i].payapseq = no;
                        dtl[i].amtbalance = dtl[i].amttrans - dtl[i].amtpaid;
                    }
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult Form(QL_trncashbankmst tbl, List<payap> dtDtl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //is Input Valid
            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            if (tbl.cashbanknote == null)
                tbl.cashbanknote = "";
            if (tbl.cashbankrefno == null)
                tbl.cashbankrefno = "";
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            if (tbl.acctgoid == 0)
            {
                msg += "Silahkan Pilih Akun Pembayaran !<BR />";
            }
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktype == "BGK")
                {
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        msg += "Please fill REF. NO. field!<BR />";
                    if (tbl.cashbankdate > tbl.cashbankduedate)
                        msg += "DUE DATE must be more or equal than Payment DATE !<BR />";
                }    
            }
            else if (tbl.cashbanktype == "BLK")
            {
                if (tbl.giroacctgoid == 0)
                    msg += "Please select DP NO. field!";
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault())
                    msg += "PAYMENT DATE must be more than DP DATE!<BR />";
            }
            if (tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                    msg += "Please fill REF. NO. field!<BR />";
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    msg += "DATE TAKE GIRO must be less or equal than DUE DATE!<BR />";
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    msg += "DATE TAKE GIRO must be more or equal than Payment DATE!<BR />";
                if (tbl.refsuppoid == 0)
                    msg += "Please select SUPPLIER field!";
            }
            //if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankdpp, ref sErrReply))
            //    ModelState.AddModelError("", "TOTAL PAYMENT must be less than MAX TOTAL PAYMENT allowed stored in database!");
            if (tbl.cashbankdpp <= 0)
            {
                msg += "TOTAL PAYMENT must be more than 0!<BR />";
            }
            //if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
            //    ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT allowed stored in database!");
            if (tbl.cashbankamt <= 0)
            {
                msg += "GRAND TOTAL must be more than 0!<BR />";
            }
            else
            {
                if (tbl.cashbanktype == "BLK")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        msg += "GRAND TOTAL must be less than DP AMOUNT!<BR />";
                    }
                    else
                    {
                        sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            msg += "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!<BR />";
                    }
                }
            }

            if (tbl.addacctgoid1 != 0)
            {
                if (tbl.addacctgamt1 == 0)
                {
                    msg += "Amount ADD Cost 1 must be more than 0!<BR />";
                }
            }
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                {
                    msg += "Amount ADD Cost 2 must be more than 0!<BR />";
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                {
                    msg += "Amount ADD Cost 3 must be more than 0!<BR />";
                }
            }

            if (tbl.addacctgamt1 != 0)
            {
                if (tbl.addacctgoid1 == 0)
                {
                    msg += "Please Select Add Cost 1 !<BR />";
                }
            }
            if (tbl.addacctgamt2 != 0)
            {
                if (tbl.addacctgoid2 == 0)
                {
                    msg += "Please Select Add Cost 2 !<BR />";
                }
            }
            if (tbl.addacctgamt3 != 0)
            {
                if (tbl.addacctgoid3 == 0)
                {
                    msg += "Please Select Add Cost 3 !<BR />";
                }
            }


            //is Input Detail Valid
            if (dtDtl == null)
                msg += "Silahkan isi detail data !!<BR />";
            else if (dtDtl.Count <= 0)
                msg += "Silahkan isi detail data !!<BR />";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].payapnote))
                            dtDtl[i].payapnote = "";
                        if (dtDtl[i].payapamt <= 0)
                        {
                            msg += "Payment Amount " + dtDtl[i].payaprefno + " Belum Diisi !!<BR />";
                        }
                        if (dtDtl[i].payapamt > dtDtl[i].amtbalance)
                        {
                            msg += "Payment Amount " + dtDtl[i].payaprefno + " Melebihi AP Balance !!<BR />";
                        }
                        if (dtDtl[i].transdate > tbl.cashbankdate)
                        {
                            msg += "PAYMENT DATE tidak boleh lebih keci dari Invoice DATE " + dtDtl[i].payaprefno + " !!<BR />";
                        }
                        //if (db.Database.SqlQuery<string>("SELECT apitemmststatus FROM QL_trnapitemmst ap WHERE ap.cmpcode='" + tbl.cmpcode + "' AND ap.apitemmstoid=" + dtDtl[i].refoid).FirstOrDefault() == "Closed"){
                        //    ModelState.AddModelError("", " Invoice " + dtDtl[i].payaprefno + " Sudah Closed, Silahkan Pilih Invoice Lain !!<BR />");
                        //}
                    }
                }
            }

            if (tbl.cashbankstatus == "Post")
            {
                tbl.cashbankno = GenerateCashBankNo(tbl.cashbankdate.ToString(),tbl.cashbanktype);
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_AP_LOCAL", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_AP_LOCAL") + "<BR />";
                //if (!ClassFunction.IsInterfaceExists("VAR_GIRO", CompnyCode))
                //    msg += ClassFunction.GetInterfaceWarning("VAR_GIRO") + "<BR />";
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AP", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_DP_AP") + "<BR />";
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            if (msg == "")
            {
                DateTime sDate = tbl.cashbankdate;
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                DateTime sDateGL = new DateTime();
                if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK" || tbl.cashbanktype=="BBK")
                {
                    tbl.cashbankduedate = tbl.cashbankdate;
                    sDateGL = tbl.cashbankdate;
                }
                else
                {
                    sDateGL = tbl.cashbankduedate??tbl.cashbankdate;
                }
                string sPeriodGL = ClassFunction.GetDateToPeriodAcctg(sDateGL);
                
                tbl.cmpcode = CompnyCode;
                tbl.periodacctg = sPeriod;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbanktaxtype = "";
                tbl.cashbanktaxpct = 0;
                tbl.cashbanktaxamt = 0;
                tbl.cashbankothertaxamt = 0;
                tbl.cashbankaptype = "";
                tbl.cashbankapoid = 0;
                tbl.deptoid = 0;
                //tbl.curroid_to = 0;
                //tbl.cashbankresamt2 = 0;
                //tbl.cashbanksuppaccoid = 0;
                //tbl.groupoid = 0;
                tbl.cashbankgiroreal = DateTime.Parse("01/01/1900");
                tbl.cashbanktakegiroreal = DateTime.Parse("01/01/1900");
                tbl.updtime = servertime;
                tbl.upduser = Session["UserID"].ToString();

                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_LOCAL", CompnyCode));
                var iAcctgOidCB = tbl.acctgoid;
                //if (tbl.cashbanktype == "BLK")
                //{
                //    iAcctgOidCB = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                //}
                //if (tbl.cashbanktype == "BGK")
                //{
                //    tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                //}

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpayap");
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            //tbl.cashbankoid = mstoid;
                            tbl.cashbankres1 = "";
                            tbl.cashbankres2 = "";
                            tbl.cashbankres3 = "";
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();                            
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_MSTOID set lastoid = " + mstoid + " Where tablename = 'QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Invoice
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].reftype == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnaprawmst")
                                {
                                    sSql = "UPDATE QL_trnaprawmst SET aprawmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapgenmst")
                                {
                                    sSql = "UPDATE QL_trnapgenmst SET apgenmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapspmst")
                                {
                                    sSql = "UPDATE QL_trnapspmst SET apspmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apspmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapservicemst")
                                {
                                    sSql = "UPDATE QL_trnapservicemst SET apservicemststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apservicemstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapdirmst")
                                {
                                    sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Post', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            //Delete Conap
                            sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYAP' AND payrefoid IN (SELECT DISTINCT payapoid FROM QL_trnpayap WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpayap.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpayap.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpayap tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpayap();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.payapoid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.suppoid = tbl.refsuppoid;
                            tbldtl.refoid = dtDtl[i].refoid;
                            tbldtl.reftype = dtDtl[i].reftype;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.payaprefno = dtDtl[i].payaprefno;
                            tbldtl.payapduedate = tbl.cashbankduedate;
                            tbldtl.payapamt = dtDtl[i].payapamt;
                            tbldtl.payapamtidr = dtDtl[i].payapamt;
                            tbldtl.payapamtusd = 0;
                            tbldtl.payapnote = dtDtl[i].payapnote;
                            tbldtl.payapres1 = "";
                            tbldtl.payapres2 = "";
                            tbldtl.payapres3 = "";
                            tbldtl.payapstatus = "";
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.payapseq = i + 1;
                            tbldtl.giroacctgoid = 0;
                            tbldtl.payaptakegiro = DateTime.Parse("01/01/1900");
                            tbldtl.payaptakegiroreal = DateTime.Parse("01/01/1900");
                            tbldtl.payapgiroflag = "";
                            tbldtl.payapgironote = "";

                            db.QL_trnpayap.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].amtbalance == dtDtl[i].payapamt)
                            {
                                //Update Invoice
                                if (dtDtl[i].reftype == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnaprawmst")
                                {
                                    sSql = "UPDATE QL_trnaprawmst SET aprawmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapgenmst")
                                {
                                    sSql = "UPDATE QL_trnapgenmst SET apgenmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapspmst")
                                {
                                    sSql = "UPDATE QL_trnapspmst SET apspmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apspmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapservicemst")
                                {
                                    sSql = "UPDATE QL_trnapservicemst SET apservicemststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apservicemstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapdirmst")
                                {
                                    sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.cashbankstatus == "Post")
                            {
                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, dtDtl[i].reftype, dtDtl[i].refoid, tbldtl.payapoid, tbl.refsuppoid, iAcctgOidAP, "Post", "PAYAP", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, "", 0, tbl.cashbankduedate??tbl.cashbankdate, 0, dtDtl[i].payapamt, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, dtDtl[i].payapamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, tbl.divgroupoid ?? 0, ""));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_MSTOID SET lastoid=" + (dtloid - 1)  + " WHERE tablename='QL_trnpayap'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = (dpapaccumamt + " + tbl.cashbankresamt + ") WHERE cmpcode = '"+ CompnyCode +"' AND dpapoid = "+ tbl.giroacctgoid +"";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }                           

                            var glseq = 1; var dAmt = dtDtl.Sum(m => m.payapamt);
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(sDate.ToString("MM/dd/yyyy")), sPeriodGL, "Pelunasan Hutang|No. " + tbl.cashbankno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1,tbl.divgroupoid??0));
                            db.SaveChanges();
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "D", dAmt, tbl.cashbankno, "Pelunasan Hutang|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, dAmt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            //Add Cost +
                            if (tbl.addacctgamt1 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "D", tbl.addacctgamt1, tbl.cashbankno, "Add Cost 1 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt1 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "D", tbl.addacctgamt2, tbl.cashbankno, "Add Cost 2 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt2 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "D", tbl.addacctgamt3, tbl.cashbankno, "Add Cost 3 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt3 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCB, "C", dAmt, tbl.cashbankno, "Pelunasan Hutang|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            //Add Cost -
                            if (tbl.addacctgamt1 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "C", (tbl.addacctgamt1 *(-1)), tbl.cashbankno, "Add Cost 1 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt1 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "C", (tbl.addacctgamt2 * (-1)), tbl.cashbankno, "Add Cost 2 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt2 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "C", (tbl.addacctgamt3 * (-1)), tbl.cashbankno, "Add Cost 3 PH|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt3 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_MSTOID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_MSTOID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "draft " + tbl.cashbankoid.ToString();
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "ter Posting";
                            sReturnNo = tbl.cashbankno;
                        }
                        else
                        {
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            List<payap> dtDtl = (List<payap>)Session["QL_trnpayap"];
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (dtDtl == null)
                msg = "Silahkan isi detail data !!";
            else if (dtDtl.Count <= 0)
                msg = "Silahkan isi detail data !!";

            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Update Invoice
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].reftype == "QL_trnapitemmst")
                            {
                                sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnaprawmst")
                            {
                                sSql = "UPDATE QL_trnaprawmst SET aprawmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aprawmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapgenmst")
                            {
                                sSql = "UPDATE QL_trnapgenmst SET apgenmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apgenmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapspmst")
                            {
                                sSql = "UPDATE QL_trnapspmst SET apspmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apspmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapservicemst")
                            {
                                sSql = "UPDATE QL_trnapservicemst SET apservicemststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apservicemstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapdirmst")
                            {
                                sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Post', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapassetmst")
                            {
                                sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        //Delete Conap
                        sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYAP' AND payrefoid IN (SELECT payapoid FROM QL_trnpayap WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpayap.Where(a => a.cashbankoid == id);
                        db.QL_trnpayap.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
    }
}