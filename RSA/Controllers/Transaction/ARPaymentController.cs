﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Transaction
{
    public class ARPaymentController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string divgroup { get; set; }
            public string cashbankno { get; set; }
            public DateTime cashbankdate { get; set; }
            public string custname { get; set; }
            public string cashbanktype { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string divname { get; set; }
            public decimal cashbankamt { get; set; }
            public string createuser { get; set; }
        }

        public class payar
        {
            public int payarseq { get; set; }
            public int custoid { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public string payarrefno { get; set; }
            public DateTime transdate { get; set; }
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
            public decimal amttrans { get; set; }
            public decimal amtpaid { get; set; }
            public decimal amtbalance { get; set; }
            public decimal payaramt { get; set; }
            public decimal payaramtidr { get; set; }
            public decimal payaramtusd { get; set; }
            public string payarnote { get; set; }
        }

        [HttpPost]
        public string GenerateCashBankNo(string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(cashbankdate);
            string sNo = cashbanktype + "-" + sDate.ToString("yy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + 6 + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%'";
            cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);

            return cashbankno;
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: APPayment
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cb", divgroupoid, "divgroupoid");
            sSql = "SELECT cb.cmpcode, (select gendesc from ql_mstgen where genoid=cb.divgroupoid) divgroup, cashbankoid, cashbankno, cashbankdate, custname, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BGM' THEN 'GIRO/CHEQUE' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE '' END) AS cashbanktype, cashbankstatus, cashbanknote, '' divname, cashbankamt, cb.createuser FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON custoid=cb.refsuppoid WHERE cb.cashbankgroup='AR'";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND cb.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cashbankstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cashbankstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cashbankstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND cashbankdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cashbankstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cb.createuser='" + Session["UserID"].ToString() + "'";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "arpayment", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: APPayment/PrintReport?id=&cmp=&isbbk=
        public ActionResult PrintReport(int id, bool isbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPaymentARAll.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", "WHERE cashbankgroup='AR' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid=" + id + "");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", Session["UserName"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ARPaymentAllPrintOut.pdf");
        }

        // GET: APPayment/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.cashbankgroup = "AR";
                tbl.cashbankresamt = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trnpayar"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);

              
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY personname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;

            var sVar = "VAR_CASH";
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
                sVar = "VAR_BANK";
            else if (tbl.cashbanktype == "BLM")
                sVar = "VAR_DP_AR";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
            sSql = GetQueryBindListCOA("VAR_ADD_COST");
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
            ViewBag.addacctgoid1_temp = (tbl.addacctgoid1.ToString() == null ? '0' : tbl.addacctgoid1);
            ViewBag.addacctgoid2_temp = (tbl.addacctgoid2.ToString() == null ? '0' : tbl.addacctgoid2);
            ViewBag.addacctgoid3_temp = (tbl.addacctgoid3.ToString() == null ? '0' : tbl.addacctgoid3);
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCusttomerData(string action, int id, int curroid, int divgroupoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_mstcust> tbl = new List<QL_mstcust>();
                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 's.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstcust') AND name NOT IN ('custnote') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT DISTINCT " + cols + ", g.gendesc AS custnote FROM QL_conar ap INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstgen g ON g.genoid=custpaymentoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.trnarstatus='Post' AND s.divgroupoid='" + divgroupoid + "' AND ISNULL((SELECT pap.payarres1 FROM QL_trnpayar pap WHERE pap.cmpcode=ap.cmpcode AND pap.payaroid=ap.payrefoid),'')<>'Lebih Bayar'AND ap.amttrans>0 GROUP BY " + cols + ", g.gendesc HAVING SUM(ap.amttrans)>SUM(ap.amtbayar) ORDER BY custcode";
                tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDPData(int curroid, int custoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_trndpar> tbl = new List<QL_trndpar>();

                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'dp.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_trndpar') AND name NOT IN ('dparpayrefno', 'dparduedate', 'dparamt') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT " + cols + ", acctgdesc dparpayrefno, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) dparamt, (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dparduedate FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid";
                tbl = db.Database.SqlQuery<QL_trndpar>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<int>("SELECT custpaymentoid FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstcust s INNER JOIN QL_mstgen g ON genoid=custpaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.email = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            if (tbl.cashbanktype == "BLM")
            {
                ViewBag.dpno = db.Database.SqlQuery<string>("SELECT dparno FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dparoid=" + tbl.giroacctgoid + "").FirstOrDefault();
            }
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(int custoid)
        {
            JsonResult js = null;
            try
            {
                List<payar> tbl = new List<payar>();

                sSql = "SELECT *, 0 payarseq, (amttrans - amtpaid) amtbalance, (amttrans - amtpaid) payaramt FROM (";
                sSql += " SELECT con.refoid refoid, ap.aritemno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype FROM QL_conar con INNER JOIN QL_trnaritemmst ap ON ap.cmpcode=con.cmpcode AND ap.aritemmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " ";
                sSql += " Union ALL SELECT con.refoid refoid, ap.arrawno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype FROM QL_conar con INNER JOIN QL_trnarrawmst ap ON ap.cmpcode=con.cmpcode AND ap.arrawmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarrawmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " ";
                //sSql += " Union ALL SELECT con.refoid refoid, ap.argenno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype FROM QL_conar con INNER JOIN QL_trnargenmst ap ON ap.cmpcode=con.cmpcode AND ap.argenmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnargenmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " ";
                //sSql += " Union ALL SELECT con.refoid refoid, ap.arspno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype FROM QL_conar con INNER JOIN QL_trnarspmst ap ON ap.cmpcode=con.cmpcode AND ap.arspmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarspmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " ";
                //sSql += " Union ALL SELECT con.refoid refoid, ap.arserviceno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype FROM QL_conar con INNER JOIN QL_trnarservicemst ap ON ap.cmpcode=con.cmpcode AND ap.arservicemstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=1 INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarservicemst' AND con.payrefoid=0 AND con.custoid=" + custoid + " ";
                sSql += ") con WHERE amttrans>amtpaid";
                tbl = db.Database.SqlQuery<payar>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<payar> dtDtl)
        {
            Session["QL_trnpayar"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            List<payar> dtl = new List<payar>();

            try
            {
                sSql = "DECLARE @oid as INTEGER; DECLARE @cmpcode as VARCHAR(30) SET @oid = " + id + "; SET @cmpcode = '" + CompnyCode + "' ";
                sSql += " SELECT pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.aritemno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.aritemgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAR', 'CNAR')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance FROM QL_trnpayar pay INNER JOIN QL_trnaritemmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.aritemmstoid AND pay.reftype = 'QL_trnaritemmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                sSql += " Union ALL SELECT pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.arrawno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.arrawgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAR', 'CNAR')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance FROM QL_trnpayar pay INNER JOIN QL_trnarrawmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.arrawmstoid AND pay.reftype = 'QL_trnarrawmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                sSql += " Union ALL SELECT pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.argenno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.argengrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAR', 'CNAR')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance FROM QL_trnpayar pay INNER JOIN QL_trnargenmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.argenmstoid AND pay.reftype = 'QL_trnargenmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                sSql += " Union ALL SELECT pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.arspno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.arspgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAR', 'CNAR')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance FROM QL_trnpayar pay INNER JOIN QL_trnarspmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.arspmstoid AND pay.reftype = 'QL_trnarspmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                //sSql += " Union ALL SELECT pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.arserviceno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.arservicegrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAR', 'CNAR')),0.0)) AS amtpaid, pay.payaramt, 1 AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance FROM QL_trnpayar pay INNER JOIN QL_trnarservicemst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.arservicemstoid AND pay.reftype = 'QL_trnarservicemst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid =1 WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";

                dtl = db.Database.SqlQuery<payar>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
                else
                {
                    var no = 0;
                    for (int i = 0; i < dtl.Count; i++)
                    {
                        no = no + 1; ;
                        dtl[i].payarseq = no;
                        dtl[i].amtbalance = dtl[i].amttrans - dtl[i].amtpaid;
                    }
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, List<payar> dtDtl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //is Input Valid            
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            if (tbl.cashbanknote == null)
                tbl.cashbanknote = "";
            if (tbl.cashbankrefno == null)
                tbl.cashbankrefno = "";
            tbl.cashbanktakegiro = tbl.cashbankduedate??tbl.cashbankdate;
            string sErrReply = "";
            if (tbl.acctgoid == 0)
            {
                msg += "Silahkan Pilih Akun Pembayaran !<BR />";
            }
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktype == "BBM")
                {
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        msg += "Please fill REF. NO. field!<BR />";
                    if (tbl.cashbankdate > tbl.cashbankduedate)
                        msg += "DUE DATE must be more or equal than Payment DATE !<BR />";
                }  
            }
            else if (tbl.cashbanktype == "BLM")
            {
                if (tbl.giroacctgoid == 0)
                    msg += "Please select DP NO. field!<BR />";
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault())
                    msg += "PAYMENT DATE must be more than DP DATE!<BR />";
            }
            if (tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                    msg += "Please fill REF. NO. field!<BR />";
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    msg += "DATE TAKE GIRO must be less or equal than DUE DATE!<BR />";
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    msg += "DATE TAKE GIRO must be more or equal than Payment DATE!<BR />";
                if (tbl.refsuppoid == 0)
                    msg += "Please select SUPPLIER field!<BR />";
            }
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankdpp, ref sErrReply))
                msg += "TOTAL PAYMENT must be less than MAX TOTAL PAYMENT allowed stored in database!<BR />";
            if (tbl.cashbankdpp <= 0)
            {
                msg += "TOTAL PAYMENT must be more than 0!<BR />";
            }
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                msg += "AMOUNT must be less than MAX AMOUNT allowed stored in database!<BR />";
            if (tbl.cashbankamt <= 0)
            {
                msg += "GRAND TOTAL must be more than 0!<BR />";
            }
            else
            {
                if (tbl.cashbanktype == "BLM")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        msg += "GRAND TOTAL must be less than DP AMOUNT!<BR />";
                    }
                    else
                    {
                        sSql = "SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            msg += "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!<BR />";
                    }
                }
            }

            if (tbl.addacctgoid1 != 0)
            {
                if (tbl.addacctgamt1 == 0)
                {
                    msg += "Amount ADD Cost 1 must be more than 0!<BR />";
                }
            }
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                {
                    msg += "Amount ADD Cost 2 must be more than 0!<BR />";
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                {
                    msg += "Amount ADD Cost 3 must be more than 0!<BR />";
                }
            }
            if (tbl.addacctgamt1 != 0)
            {
                if (tbl.addacctgoid1 == 0)
                {
                    msg += "Please Select Add Cost 1 !<BR />";
                }
            }
            if (tbl.addacctgamt2 != 0)
            {
                if (tbl.addacctgoid2 == 0)
                {
                    msg += "Please Select Add Cost 2 !<BR />";
                }
            }
            if (tbl.addacctgamt3 != 0)
            {
                if (tbl.addacctgoid3 == 0)
                {
                    msg += "Please Select Add Cost 3 !<BR />";
                }
            }

            //is Input Detail Valid
            
            if (dtDtl == null)
                msg += "Silahkan isi detail data !!<BR />";
            else if (dtDtl.Count <= 0)
                msg += "Silahkan isi detail data !!<BR />";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].payarnote))
                            dtDtl[i].payarnote = "";
                        if (dtDtl[i].payaramt <= 0)
                        {
                            msg += "Payment Amount " + dtDtl[i].payarrefno + " Belum Diisi !!<BR />";
                        }
                        if (dtDtl[i].payaramt > dtDtl[i].amtbalance)
                        {
                            msg += "Payment Amount " + dtDtl[i].payarrefno + " Melebihi AP Balance !!<BR />";
                        }
                        if (dtDtl[i].transdate > tbl.cashbankdate)
                        {
                            msg += "PAYMENT DATE tidak boleh lebih keci dari Invoice DATE " + dtDtl[i].payarrefno + " !!<BR />";
                        }
                        if (db.Database.SqlQuery<string>("SELECT aritemmststatus FROM QL_trnaritemmst ap WHERE ap.cmpcode='" + tbl.cmpcode + "' AND ap.aritemmstoid=" + dtDtl[i].refoid).FirstOrDefault() == "Closed")
                        {
                            msg += " Invoice " + dtDtl[i].payarrefno + " Sudah Closed, Silahkan Pilih Invoice Lain !!<BR />";
                        }
                    }
                }
            }

            if (tbl.cashbankstatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_AR", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_AR")+"<BR />";
                if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_GIRO_IN") + "<BR />";
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AR", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_DP_AR") + "<BR />";
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            var area = "";
            if (tbl.cashbankstatus == "Post")
            {
                tbl.cashbankno = GenerateCashBankNo(tbl.cashbankdate.ToString("MM/dd/yyyy"), tbl.cashbanktype, 0);
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += cRate.GetRateMonthlyLastError;
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            if (msg == "")
            {
                DateTime sDate = tbl.cashbankdate;
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                DateTime sDateGL = new DateTime();
                if (tbl.cashbanktype == "BKM" || tbl.cashbanktype == "BLM"||tbl.cashbankaptype=="BBM")
                {
                    tbl.cashbankduedate = tbl.cashbankdate;
                    sDateGL = tbl.cashbankdate;
                }
                else
                {
                    sDateGL = tbl.cashbankduedate ?? tbl.cashbankdate;
                }
                string sPeriodGL = ClassFunction.GetDateToPeriodAcctg(sDateGL);


                tbl.cmpcode = CompnyCode;
                tbl.periodacctg = sPeriod;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbanktaxtype = "";
                tbl.cashbanktaxpct = 0;
                tbl.cashbanktaxamt = 0;
                tbl.cashbankothertaxamt = 0;
                tbl.cashbankaptype = "";
                tbl.cashbankapoid = 0;
                tbl.deptoid = 0;
                tbl.cashbankgiroreal = DateTime.Parse("01/01/1900");
                tbl.cashbanktakegiroreal = DateTime.Parse("01/01/1900");
                tbl.updtime = servertime;
                tbl.upduser = Session["UserID"].ToString();

                var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", CompnyCode));
                var iAcctgOidCB = tbl.acctgoid;
                if (tbl.cashbanktype == "BLM")
                {
                    iAcctgOidCB = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                }
                if (tbl.cashbanktype == "BGM")
                {
                    tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                }

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpayar");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            //tbl.cashbankoid = mstoid;
                            tbl.cashbankres1 = "";
                            tbl.cashbankres2 = area;
                            tbl.cashbankres3 = "";
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_mstoid set lastoid = " + mstoid + " Where tablename = 'QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.cashbankres2 = tbl.createuser;
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Invoice
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].reftype == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarrawmst")
                                {
                                    sSql = "UPDATE QL_trnarrawmst SET arrawmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arrawmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnargenmst")
                                {
                                    sSql = "UPDATE QL_trnargenmst SET argenmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND argenmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarspmst")
                                {
                                    sSql = "UPDATE QL_trnarspmst SET arspmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arspmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarservicemst")
                                {
                                    sSql = "UPDATE QL_trnarservicemst SET arservicemststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arservicemstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            //Delete Conar
                            sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnartype='PAYAR' AND payrefoid IN (SELECT DISTINCT payaroid FROM QL_trnpayar WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpayar.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpayar.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpayar tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpayar();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.payaroid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.custoid = tbl.refsuppoid;
                            tbldtl.refoid = dtDtl[i].refoid;
                            tbldtl.reftype = dtDtl[i].reftype;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.payarrefno = dtDtl[i].payarrefno;
                            tbldtl.payarduedate = tbl.cashbankduedate;
                            tbldtl.payaramt = dtDtl[i].payaramt;
                            tbldtl.payaramtidr = dtDtl[i].payaramt;
                            tbldtl.payaramtusd = 0;
                            tbldtl.payarnote = dtDtl[i].payarnote;
                            tbldtl.payarres1 = "";
                            tbldtl.payarres2 = "";
                            tbldtl.payarres3 = "";
                            tbldtl.payarstatus = "";
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.payarseq = i + 1;
                            tbldtl.giroacctgoid = 0;
                            tbldtl.payartakegiro = DateTime.Parse("01/01/1900");
                            tbldtl.payartakegiroreal = DateTime.Parse("01/01/1900");
                            tbldtl.payargiroflag = "";
                            tbldtl.payargironote = "";

                            db.QL_trnpayar.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].amtbalance == dtDtl[i].payaramt)
                            {
                                //Update Invoice
                                if (dtDtl[i].reftype == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarrawmst")
                                {
                                    sSql = "UPDATE QL_trnarrawmst SET arrawmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arrawmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnargenmst")
                                {
                                    sSql = "UPDATE QL_trnargenmst SET argenmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND argenmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarspmst")
                                {
                                    sSql = "UPDATE QL_trnarspmst SET arspmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arspmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarservicemst")
                                {
                                    sSql = "UPDATE QL_trnarservicemst SET arservicemststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arservicemstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Closed', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.cashbankstatus == "Post")
                            {
                                // Insert QL_conar                      
                                db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, "QL_trnaritemmst", dtDtl[i].refoid, tbldtl.payaroid, dtDtl[i].custoid, iAcctgOidAR, "Post", "PAYAR", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, "", 0, tbl.cashbankduedate??tbl.cashbankdate, 0, dtDtl[i].payaramt, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, dtDtl[i].payaramt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, "",tbl.divgroupoid??0));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpayar'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = (dparaccumamt + " + tbl.cashbankresamt + ") WHERE cmpcode = '" + CompnyCode + "' AND dparoid = " + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var glseq = 1; var dAmt = dtDtl.Sum(m => m.payaramt);
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(sDate.ToString("MM/dd/yyyy")), sPeriodGL, "Pelunasan Piutang|No. " + tbl.cashbankno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCB, "D", dAmt, tbl.cashbankno, "Pelunasan Piutang|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            //Add Cost -
                            if (tbl.addacctgamt1 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "D", (tbl.addacctgamt1 * (-1)), tbl.cashbankno, "Add Cost 1 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt1 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "D", (tbl.addacctgamt2 * (-1)), tbl.cashbankno, "Add Cost 2 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt2 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "D", (tbl.addacctgamt3 * (-1)), tbl.cashbankno, "Add Cost 3 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt3 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }                           
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAR, "C", dAmt, tbl.cashbankno, "Pelunasan Piutang|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, dAmt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            //Add Cost +
                            if (tbl.addacctgamt1 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "C", tbl.addacctgamt1, tbl.cashbankno, "Add Cost 1 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt1 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "C", tbl.addacctgamt2, tbl.cashbankno, "Add Cost 2 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt2 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "C", tbl.addacctgamt3, tbl.cashbankno, "Add Cost 3 PP|No. " + tbl.cashbankno + "", "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt3 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, area, null, "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }


                            sSql = "UPDATE  QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "draft " + tbl.cashbankoid.ToString();
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "ter Posting";
                            sReturnNo = tbl.cashbankno;
                        }
                        else
                        {
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            List<payar> dtDtl = (List<payar>)Session["QL_trnpayar"];
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Update Invoice
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].reftype == "QL_trnaritemmst")
                            {
                                sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnarrawmst")
                            {
                                sSql = "UPDATE QL_trnarrawmst SET arrawmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arrawmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnargenmst")
                            {
                                sSql = "UPDATE QL_trnargenmst SET argenmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND argenmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnarspmst")
                            {
                                sSql = "UPDATE QL_trnarspmst SET arspmststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arspmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnarservicemst")
                            {
                                sSql = "UPDATE QL_trnarservicemst SET arservicemststatus='Approved', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arservicemstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnarassetmst")
                            {
                                sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Post', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        //Delete Conar
                        sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYAR' AND payrefoid IN (SELECT payaroid FROM QL_trnpayar WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpayar.Where(a => a.cashbankoid == id);
                        db.QL_trnpayar.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
    }
}