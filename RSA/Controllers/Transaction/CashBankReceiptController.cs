﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class CashBankReceiptController : Controller
    {
        private  QL_RSAEntities db = new  QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string divgroup { get; set; }
            public string cashbankno { get; set; }
            [DataType(DataType.Date)]
            public DateTime? cashbankdate { get; set; }
            public string cashbanktype { get; set; }
            public string acctgdesc { get; set; }
            public string personname { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string custname { get; set; }
            public string CBType { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamt { get; set; }
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
            public decimal cashbankglamtidr { get; set; }
            public decimal cashbankglamtusd { get; set; }
            public int curroid { get; set; }
        }

        public class trndpar
        {
            public int dparoid { get; set; }
            public string dparno { get; set; }
            public DateTime dpardate { get; set; }
            public int dparacctgoid { get; set; }
            public string acctgdesc { get; set; }
            public string dparnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dparamt { get; set; }
            public DateTime dpdateforcheck { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custtaxable { get; set; }
            public decimal custtaxvalue { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + tbl.cmpcode + "' ORDER BY personname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
        }

        [HttpPost]
        public ActionResult InitDDLPerson(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstperson> tbl = new List<QL_mstperson>();
            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + cmp + "' ORDER BY personname";
            tbl = db.Database.SqlQuery<QL_mstperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDiv(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindCustomerData()
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT custoid, custcode, custname, custaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS custtaxable, ISNULL((SELECT TOP 1 CAST (gendesc AS DECIMAL) FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC), 0) custtaxvalue FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY custcode";
            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindDPData(string cmp, int custoid, int curroid)
        {
            List<trndpar> tbl = new List<trndpar>();
            sSql = "SELECT dp.dparoid, dp.dparno, dpardate, dp.acctgoid AS dparacctgoid, a.acctgdesc, dp.dparnote, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) AS dparamt, (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + cmp + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid";

            tbl = db.Database.SqlQuery<trndpar>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindTotalDP(string cmpcode, string action, int curroid, int custoid, int cashbankoid)
        {
            decimal totaldpamt = 0;
            if (action == "Update Data")
            {
                sSql = " SELECT ISNULL((SELECT SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + cmpcode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt), 0.0) + cashbankamt FROM QL_trncashbankmst cb WHERE cb.cashbankoid=" + cashbankoid;
            }else
            {
                sSql = " SELECT ISNULL(SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)), 0.0) AS dparamt FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + cmpcode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ";
            }
            totaldpamt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Json(totaldpamt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            //(db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault())
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, 1 curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl)
        {
            Session["QL_trncashbankgl_rc"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankgl_rc"] == null)
            {
                Session["QL_trncashbankgl_rc"] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankgl_rc"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.cashbankrefno = db.Database.SqlQuery<string>("SELECT cashbankrefno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            ViewBag.dparno = "";
            ViewBag.totaldpamt = 0;
            ViewBag.cashbankresamt = 0;
            ViewBag.dpdateforcheck = DateTime.Parse("1/1/1900 00:00:00");
            if (tbl.cashbanktype == "BLM")
            {
                ViewBag.dparno = db.Database.SqlQuery<string>("SELECT dparno FROM QL_trndpar WHERE dparoid=" + tbl.giroacctgoid).FirstOrDefault();
                ViewBag.totaldpamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)), 0.0) FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparstatus='Post' AND dp.custoid=" + tbl.refsuppoid + " AND dp.curroid=" + tbl.curroid + " AND dp.dparamt > dp.dparaccumamt").FirstOrDefault() + tbl.cashbankamt;
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(dparamt - ISNULL(dp.dparaccumamt, 0.0), 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault() + tbl.cashbankamt;
                ViewBag.dpdateforcheck = db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault();
            }
        }

        private string GenerateReceiptNo2(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateReceiptNo(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cb", divgroupoid, "divgroupoid");
            sSql = "SELECT cb.cmpcode,  (select gendesc from ql_mstgen where genoid=cb.divgroupoid) divgroup, cb.cashbankoid, cb.cashbankno, cb.cashbankdate, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, p.personname, cb.cashbankstatus, cb.cashbanknote, ISNULL(custname, '') AS custname, '' AS CBType,cashbankamt FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstperson p ON p.personoid=cb.personoid LEFT JOIN QL_mstcust s ON s.custoid=cb.refsuppoid ";
            //if (cbVoucher)
            //    sSql += " INNER JOIN QL_mstaccount ma ON cb.acctgoid=ma.acctgoid INNER JOIN QL_mstbank mb ON ma.bankoid=mb.bankoid INNEr JOIN QL_mstgen mg ON mg.genoid=mb.bankmstoid ";
            sSql += " WHERE cb.cashbankgroup='RECEIPT' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND cb.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cashbankstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cashbankstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cashbankstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND cashbankdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cashbankstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cb.createuser='" + Session["UserID"].ToString() + "'";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trncashbankmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trncashbankmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trncashbankgl_rc"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncashbankmst.Find(cmp, id);
                Session["lastcashbanktype"] = tbl.cashbanktype;
                Session["lastdparoid"] = tbl.giroacctgoid;
                Session["lastdparamt"] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, 0 groupoid, '' groupdesc FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + cmp + "' ORDER BY cashbankgloid";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankgl_rc"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            
            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankgl_rc"];
            string sErrReply = "";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktype == "BBM")
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than RECEIPT DATE");
            } else if (tbl.cashbanktype == "BLM")
            {
                if (tbl.giroacctgoid == 0)
                    ModelState.AddModelError("", "Please select DP NO. field!");
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault())
                    ModelState.AddModelError("", "PAYMENT DATE must be more than DP DATE!");
            }
            if (tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be less or equal than DUE DATE!");
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than RECEIPT DATE!");
                if (tbl.refsuppoid == 0)
                    ModelState.AddModelError("", "Please select CUSTOMER field!");
            }
            if (tbl.personoid == 0)
                ModelState.AddModelError("", "Please select PIC field!");
            if (!ClassFunction.isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", tbl.cashbanktaxamt, ref sErrReply))
                ModelState.AddModelError("", "OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbankamt < 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }else
            {
                if (tbl.cashbanktype == "BLM")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        ModelState.AddModelError("", "GRAND TOTAL must be less than DP AMOUNT!");
                    }
                    else
                    {
                        sSql = "SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (action == "Update Data")
                            dNewDPBalance += tbl.cashbankamt;
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            ModelState.AddModelError("", "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!");
                    }
                }
            }
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].cashbankglamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (!ClassFunction.isLengthAccepted("cashbankglamt", "QL_trncashbankgl", dtDtl[i].cashbankglamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }

                        dtDtl[i].groupoid = 0;
                    }
                }
            }

            var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKM" || tbl.cashbanktype == "BLM" || tbl.cashbanktype=="BBM")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = Convert.ToDateTime(tbl.cashbankduedate);
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBM")
                sDate = Convert.ToDateTime(tbl.cashbankduedate);
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iTaxAcctgOid = 0;
            int iOtherTaxAcctgOid = 0;
            int iGiroAcctgOid = tbl.giroacctgoid;

            if (tbl.cashbankstatus == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, sDate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(sDate.Month).ToString() + " " + sDate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.cashbankstatus = "In Process";
                }
                if (tbl.cashbanktype == "BLM")
                    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                else
                    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cashbankstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cashbankstatus = "In Process";
                }
                string sVarErr = "";
                if (tbl.cashbanktaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", tbl.cmpcode))
                        sVarErr = "VAR_PPN_OUT";
                    else
                        iTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", tbl.cmpcode));
                }
                if (tbl.cashbankothertaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_OTHER_TAX_RECEIPT", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_OTHER_TAX_RECEIPT";
                    else
                        iOtherTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_OTHER_TAX_RECEIPT", tbl.cmpcode));
                }
                if (tbl.cashbanktype == "BGM")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO_IN";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", tbl.cmpcode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.cashbankstatus = "In Process";
                }    
            }
            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        //mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateReceiptNo2(tbl.cmpcode, tbl.cashbankdate, tbl.cashbanktype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = tbl.cashbankamt * cRate.GetRateMonthlyUSDValue;
                tbl.cashbankgroup = "RECEIPT";
                tbl.cashbanktakegiro = (tbl.cashbanktype == "BGM" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.refsuppoid).FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {


                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " + Session["lastdparamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + Session["lastdparoid"];
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                                
                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKM" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.groupoid = dtDtl[i].groupoid;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Cash/Bank Receipt|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt < 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", -dtDtl[i].cashbankglamt, tbl.cashbankno, dtDtl[i].cashbankglnote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", dtDtl[i].groupoid, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktype == "BGM")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iGiroAcctgOid, "D", tbl.cashbankamt, tbl.cashbankno, tbl.cashbanknote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            else
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", tbl.cashbankamt, tbl.cashbankno, tbl.cashbanknote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            for (var i=0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "C", dtDtl[i].cashbankglamt, tbl.cashbankno, dtDtl[i].cashbankglnote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", dtDtl[i].groupoid, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iTaxAcctgOid, "C", tbl.cashbanktaxamt, tbl.cashbankno, "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbanktaxamt * cRate.GetRateMonthlyIDRValue, tbl.cashbanktaxamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            if (tbl.cashbankothertaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iOtherTaxAcctgOid, "C", tbl.cashbankothertaxamt, tbl.cashbankno, "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankothertaxamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankothertaxamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Other Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.cashbankoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLM")
                        {
                            sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " + Session["lastdparamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + Session["lastdparoid"];
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == cmp);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptReceipt.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", " WHERE cashbankgroup='RECEIPT' AND cb.cmpcode='" + cmp + "' AND cb.cashbankoid IN (" + id + ")");
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ReceiptPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}