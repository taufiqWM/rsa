﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class SOFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class soitemmst
        {
            public string cmpcode { get; set; }
            public int soitemmstoid { get; set; }
            public string divgroup { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string soitemmststatus { get; set; }
            public string soitemmstnote { get; set; }
            public string divname { get; set; }
            public string sotype { get; set; }
        }

        public class soitemdtl
        {
            public int soitemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string itemnote { get; set; }
            public decimal stockqty { get; set; }
            public decimal soitemqty { get; set; }
            public int soitemunitoid { get; set; }
            public string soitemunit { get; set; }
            public decimal soitemlastprice { get; set; }
            public decimal soitemprice { get; set; }
            public decimal soitemprice_intax { get; set; }
            public decimal soitemdtlamt { get; set; }
            public decimal soitemdtlamt_intax { get; set; }
            public string soitemdtldisctype { get; set; }
            public decimal soitemdtldiscvalue { get; set; }
            public decimal soitemdtldiscvalue_intax { get; set; }
            public decimal soitemdtldiscamt { get; set; }
            public decimal soitemdtldiscamt_intax { get; set; }
            public decimal soitemdtlnetto { get; set; }
            public decimal soitemdtlnetto_intax { get; set; }
            public string soitemdtlnote { get; set; }
            public DateTime soitemdtletd { get; set; }
            public string soitemdtletdstr { get; set; }
        }

        public class mstaccount
        {
            public int accountoid { get; set; }
            public string accountdesc { get; set; }
        }

        public class container
        {
            public int containerseq { get; set; }
            public int containeroid { get; set; }
            public string containertype { get; set; }
            public decimal soitemcontqty { get; set; }
            public decimal soitemcontpct { get; set; }

        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal soratetousd { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal sorate2tousd { get; set; }
        }

        private void InitDDL(QL_trnsoitemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var sopaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.soitempaytypeoid);
            ViewBag.sopaytypeoid = sopaytypeoid;

            sSql = "select * from QL_mstperson p inner join QL_mstlevel l on p.leveloid=l.leveloid and leveldesc='SALES' where p.cmpcode='" + CompnyCode + "' order by personname";
            var salesoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personname", "personname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            sSql = "SELECT * FROM QL_mstaccount a WHERE a.cmpcode='" + tbl.cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY a.accountno";
            var accountoid = new SelectList(db.Database.SqlQuery<QL_mstaccount>(sSql).ToList(), "accountoid", "accountno", tbl.accountoid);
            ViewBag.accountoid = accountoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtladdr FROM QL_mstcustdtl a WHERE a.custdtloid ='" + tbl.custdtloid + "'").FirstOrDefault();

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;
        }
        public class listalamat
        {
            public int custdtloid { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
            public int custdtlcityoid { get; set; }
            public string custdtlcity { get; set; }
        }
        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtloid, custdtlname, custdtladdr , custdtlcityoid , g.gendesc custdtlcity FROM QL_mstcustdtl c Left JOIN QL_mstgen g ON g.genoid=c.custdtlcityoid WHERE c.custoid = " + custoid + " ORDER BY custdtloid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public class listbom
        {
            public int bomoid { get; set; }
            public string bomcode { get; set; }
            public string bomwipdesc { get; set; }
        }
        [HttpPost]
        public ActionResult GetBomData()
        {
            List<listbom> tbl = new List<listbom>();

            sSql = "SELECT bomoid, bomcode, bomwipdesc FROM QL_mstbillofmat ORDER BY bomoid";
            tbl = db.Database.SqlQuery<listbom>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDivision(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<mstaccount> tbl = new List<mstaccount>();
            sSql = "SELECT a.accountoid, b.bankname+' - '+a.accountno accountdesc  FROM QL_mstaccount a INNER JOIN QL_mstbank b ON b.bankoid = a.bankoid WHERE a.cmpcode='" + cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY b.bankname+a.accountno";
            tbl = db.Database.SqlQuery<mstaccount>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetContainerData()
        {
            List<container> tbl = new List<container>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY gendesc) AS INT) AS containerseq, genoid AS containeroid, gendesc AS containertype, 0.0 AS soitemcontqty, 0.0 AS soitemcontpct FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CONTAINER TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<container>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust c WHERE c.cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND custoid>0 and c.divgroupoid=" + divgroupoid + " ORDER BY custname DESC";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }
                
                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.soratetousd = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.sorate2tousd = cRate.GetRateMonthlyUSDValue;
                }else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int curroid, int custoid, int divgroupoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<soitemdtl> tbl = new List<soitemdtl>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, itemoid, itemcode, itemlongdesc, 0.0 AS stockqty, 0.0 AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemlastprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE'AND m.divgroupoid=" + divgroupoid + " /* AND m.itemoid IN (SELECT DISTINCT sod.itemoid FROM QL_trnsoitemdtl sod WHERE sod.upduser='" + Session["UserID"].ToString() + "')*/ ORDER BY itemcode";

            tbl = db.Database.SqlQuery<soitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailData(int bomoid, int curroid, int custoid, decimal bomqty, int divgroupoid)
        {
            var cmp = CompnyCode;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<soitemdtl> tbl = new List<soitemdtl>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, 1.0 * " + bomqty+" AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.itemoid INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid="+bomoid+ " AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + "  ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(item2qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.itemoid2 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + " AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(item3qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.itemoid3 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(pt1qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.pt1 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(pt2qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.pt2 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(pt3qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.pt3 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(ly1qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.ly1 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";
            sSql += " Union ALL SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) soitemdtlseq, CONVERT(varchar(10), GETDATE(), 101) soitemdtletdstr, m.itemoid, itemcode, itemlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND refoid=m.itemoid), 0.0) AS stockqty, isnull(ly2qty,0) * " + bomqty + " AS soitemqty, itemunitoid AS soitemunitoid, gendesc AS soitemunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS soitemprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstitemprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.itemoid=m.itemoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS soitemdtlnote, 'P' AS soitemdtldisctype, 0.0 AS soitemdtldiscvalue, m.itemnote FROM QL_mstitem m inner join ql_mstbillofmat bm on m.itemoid=bm.ly2 INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunitoid WHERE m.cmpcode='" + CompnyCode + "' and bm.bomoid=" + bomoid + "  AND m.activeflag='ACTIVE' AND m.divgroupoid=" + divgroupoid + " ";

            tbl = db.Database.SqlQuery<soitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<soitemdtl> dtDtl)
        {
            Session["QL_trnsoitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsCont(List<container> dtDtlCont)
        {
            Session["QL_trnsoitemcont"] = dtDtlCont;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsoitemdtl"] == null)
            {
                Session["QL_trnsoitemdtl"] = new List<soitemdtl>();
            }

            List<soitemdtl> dataDtl = (List<soitemdtl>)Session["QL_trnsoitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailContainer()
        {
            if (Session["QL_trnsoitemcont"] == null)
            {
                Session["QL_trnsoitemcont"] = new List<container>();
            }

            List<container> dataDtlCont = (List<container>)Session["QL_trnsoitemcont"];
            return Json(dataDtlCont, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsoitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.custpaytype = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid=" + tbl.soitempaytypeoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: SOitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "som", divgroupoid, "divgroupoid");
            sSql = "SELECT som.cmpcode, (select gendesc from ql_mstgen where genoid=som.divgroupoid) divgroup, som.soitemmstoid, som.soitemno, som.soitemdate, c.custname, som.soitemmststatus, som.soitemmstnote, div.divname, som.soitemtype sotype FROM QL_TRNSOitemMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND soitemmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND soitemmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND soitemmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND soitemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND soitemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND soitemmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " AND isnull(som.sssflag,0)=0 ORDER BY som.soitemdate DESC, som.soitemmstoid DESC";

            List<soitemmst> dt = db.Database.SqlQuery<soitemmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnsoitemmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: SOitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var cmp = CompnyCode;
            QL_trnsoitemmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnsoitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.soitemmstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                tbl.soitemdate = ClassFunction.GetServerTime();
                tbl.socustrefdate = ClassFunction.GetServerTime();
                tbl.soitemetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.lastrevtime = new DateTime(1900, 1, 1);
                tbl.soitemmststatus = "In Process";
                tbl.ismadetoorder = true;
                tbl.isexcludetax = true;
                
                Session["QL_trnsoitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnsoitemmst.Find(cmp, id);
                string sAdd = "";
                //if (tbl.isexcludetax)
                //{
                //    sAdd = "";
                //}

                sSql = "SELECT sod.soitemdtlseq, sod.itemoid, CONVERT(CHAR(10),sod.soitemdtletd,101) soitemdtletdstr,sod.soitemdtletd, itemcode, m.itemlongdesc, sod.soitemqty, sod.soitemunitoid, g.gendesc AS soitemunit, sod.soitemprice" + sAdd + " soitemprice, sod.soitemprice_intax, sod.soitemdtlamt" + sAdd + " soitemdtlamt, sod.soitemdtldisctype, soitemdtldiscvalue" + sAdd + " soitemdtldiscvalue, soitemdtldiscamt" + sAdd + " soitemdtldiscamt, soitemdtlnetto" + sAdd + " soitemdtlnetto, sod.soitemdtlnote, ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstitemprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstitemprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), 0.0)) AS soitemlastprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=" + tbl.custoid + " AND crd.itemoid=sod.itemoid ORDER BY crd.updtime DESC), 0.0)) AS lastcurroid FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.soitemunitoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=sod.cmpcode AND som.soitemmstoid=sod.soitemmstoid WHERE sod.soitemmstoid=" + id + " ORDER BY sod.soitemdtlseq";
                Session["QL_trnsoitemdtl"] = db.Database.SqlQuery<soitemdtl>(sSql).ToList();

               
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: SOitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsoitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.soitemno == null)
                tbl.soitemno = "";

            if (tbl.soitemdate > tbl.soitemetd)
            {
                ModelState.AddModelError("", "ETD must be more than SO DATE!");
            }

            List<soitemdtl> dtDtl = (List<soitemdtl>)Session["QL_trnsoitemdtl"];
            //List<container> dtDtlCont = (List<container>)Session["QL_trnsoitemcont"];

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.soitemdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            msg = "";
            if (msg != "")
                ModelState.AddModelError("", msg);

            if (dtDtl == null)
            {
                ModelState.AddModelError("", "Please fill detail data!");
            }
            else
            {
                if (dtDtl.Count <= 0)
                {
                    ModelState.AddModelError("", "Please fill detail data!");
                }
                else
                {
                    if (dtDtl != null)
                    {
                        if (dtDtl.Count > 0)
                        {
                            for (var i = 0; i < dtDtl.Count; i++)
                            {
                                if (tbl.ismadetoorder)
                                {
                                    if (tbl.soitemmststatus == "In Approval")
                                    {
                                        //sSql = "SELECT COUNT(*) FROM QL_mstitem m WHERE m.cmpcode='" + tbl.cmpcode + "' AND m.activeflag='ACTIVE' AND m.itemoid IN (SELECT itemoid FROM QL_mstitemdtl2 WHERE cmpcode='" + tbl.cmpcode + "' AND itemoid=" + dtDtl[i].itemoid + " AND custoid=" + tbl.custoid + ")";
                                        //if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() < 0)
                                        //{
                                        //    ModelState.AddModelError("", "Kode " + dtDtl[i].itemcode + "  Belum Di buatkan External Name!");
                                        //}
                                    }
                                    //if (dtDtl[i].soitemprice <= 0)
                                    //{
                                    //    ModelState.AddModelError("", "SO PRICE for code " + dtDtl[i].itemcode + " must more than 0");
                                    //}
                                    if (dtDtl[i].soitemdtldisctype == "P")
                                    {
                                        if (dtDtl[i].soitemdtldiscvalue < 0 || dtDtl[i].soitemdtldiscvalue > 100)
                                            ModelState.AddModelError("", dtDtl[i].itemcode + " Percentage of DETAIL DISC must be between 0 and 100!");
                                    }
                                    else
                                    {
                                        if (dtDtl[i].soitemdtldiscvalue < 0 || dtDtl[i].soitemdtldiscvalue > dtDtl[i].soitemdtldiscamt)
                                            ModelState.AddModelError("", dtDtl[i].itemcode + " Amount of DETAIL DISC must be between 0 and " + string.Format("{0:#,0.00}", dtDtl[i].soitemdtldiscamt) + " (DETAIL AMOUNT)!");
                                    }
                                }
                                if (dtDtl[i].soitemqty <= 0)
                                {
                                    ModelState.AddModelError("", "SO QTY for code " + dtDtl[i].itemcode + " must more than 0");
                                }
                            }
                        }
                    }
                }
            }
            tbl.ismadetoorder = true;
            tbl.soitempaymethod = "TT";
            if (tbl.ismadetoorder)
            {
                //if (dtDtlCont == null)
                //{
                //    ModelState.AddModelError("", "CONTAINER DATA is Empty!");
                //}
                if (tbl.soitemportship == null)
                {
                    //ModelState.AddModelError("", "Please Fill PORT OF SHIP Field!");
                    tbl.soitemportship = "";
                }
                if (tbl.soitemportdischarge == null)
                {
                    //ModelState.AddModelError("", "Please Fill PORT OF DISCHARGE Field!");
                    tbl.soitemportdischarge = "";
                }
            }else
            {
                if (tbl.soitemportship == null)
                {
                    tbl.soitemportship = "";
                }
                if (tbl.soitemportdischarge == null)
                {
                    tbl.soitemportdischarge = "";
                }
            }
            if (tbl.soitemgrandtotalamt == 0)
            {
                ModelState.AddModelError("", "Grand total Amount cannot be 0!");
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.soitemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsoitemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvaluser + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsoitemdtl");
                var socontoid = ClassFunction.GenerateID("QL_trnsoitemcont");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                if (tbl.custoid == 0)
                    tbl.custoid = -1;
                if (tbl.curroid == 0)
                    tbl.curroid = 1;
                if (tbl.accountoid == 0)
                    tbl.accountoid = -1;
                tbl.soitemmstdisctype = "P";
                tbl.soitemmstdiscvalue = 0;
                tbl.soitemmstdiscamt = 0;
                tbl.soiteminvoiceval = "";
                tbl.accountoid = 0;
                tbl.groupoid = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;
                        tbl.revstatus = tbl.revstatus == null ? "" : tbl.revstatus;
                        tbl.lastrevuser = tbl.lastrevuser == null ? "" : tbl.lastrevuser;
                        tbl.lastrevtime = tbl.lastrevtime == null ? new DateTime(1900, 1, 1) : tbl.lastrevtime;
                        tbl.soitempino = tbl.soitempino == null ? "" : tbl.soitempino;
                        tbl.soitemreqshipdate = new DateTime(1900, 1, 1);
                        tbl.soitemrevetd = new DateTime(1900, 1, 1);

                        if (action == "New Data")
                        {
                            if (db.QL_trnsoitemmst.Find(tbl.cmpcode, tbl.soitemmstoid) != null)
                                tbl.soitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.soitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.soitembank = "18";
                            db.QL_trnsoitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.soitemmstoid + " WHERE tablename='QL_trnsoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            tbl.soitembank = "18";
                            db.SaveChanges();

                            var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == tbl.soitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                         
                        }

                        QL_trnsoitemdtl tbldtl;
                        //QL_trnsoitemcont tbldtlcont;
                        decimal dDiv = 1M;
                        if (!tbl.isexcludetax)
                        {
                            dDiv = 1.1M;
                        }
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsoitemdtl();
                            dtDtl[i].soitemprice_intax = dtDtl[i].soitemprice / dDiv;
                            dtDtl[i].soitemdtlamt_intax = dtDtl[i].soitemdtlamt / dDiv;
                            dtDtl[i].soitemdtldiscvalue_intax = dtDtl[i].soitemdtldiscvalue / dDiv;
                            dtDtl[i].soitemdtldiscamt_intax = dtDtl[i].soitemdtldiscamt / dDiv;
                            dtDtl[i].soitemdtlnetto_intax = dtDtl[i].soitemdtlnetto / dDiv;

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.soitemdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.soitemmstoid = tbl.soitemmstoid;
                            tbldtl.soitemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.soitemqty = dtDtl[i].soitemqty;
                            tbldtl.soitemunitoid = dtDtl[i].soitemunitoid;
                            tbldtl.soitemprice = dtDtl[i].soitemprice;
                            tbldtl.soitemprice_intax = dtDtl[i].soitemprice_intax;
                            tbldtl.soitemdtlamt = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtlamt_intax = dtDtl[i].soitemdtlamt_intax;
                            tbldtl.soitemdtldisctype = dtDtl[i].soitemdtldisctype;
                            tbldtl.soitemdtldiscvalue = dtDtl[i].soitemdtldiscvalue;
                            tbldtl.soitemdtldiscvalue_intax = dtDtl[i].soitemdtldiscvalue_intax;
                            tbldtl.soitemdtldiscamt = dtDtl[i].soitemdtldiscamt;
                            tbldtl.soitemdtldiscamt_intax = dtDtl[i].soitemdtldiscamt_intax;
                            tbldtl.soitemdtlnetto = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlnetto_intax = dtDtl[i].soitemdtlnetto_intax;
                            tbldtl.soitemdtlstatus = "";
                            tbldtl.soitemdtlnote = (dtDtl[i].soitemdtlnote == null ? "" : dtDtl[i].soitemdtlnote);
                            tbldtl.soitemdtletd = DateTime.Parse(dtDtl[i].soitemdtletdstr);
                            tbldtl.isfoc = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnsoitemdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnsoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //for (var i = 0; i < dtDtlCont.Count(); i++) 
                        //{
                        //    if (dtDtlCont[i].soitemcontqty > 0)
                        //    {
                        //        tbldtlcont = new QL_trnsoitemcont();
                        //        tbldtlcont.cmpcode = tbl.cmpcode;
                        //        tbldtlcont.soitemcontoid = socontoid++;
                        //        tbldtlcont.soitemmstoid = tbl.soitemmstoid;
                        //        tbldtlcont.containeroid = dtDtlCont[i].containeroid;
                        //        tbldtlcont.soitemcontqty = dtDtlCont[i].soitemcontqty;
                        //        tbldtlcont.soitemcontpct = dtDtlCont[i].soitemcontpct;
                        //        tbldtlcont.soitemcontstatus = "";
                        //        tbldtlcont.upduser = tbl.upduser;
                        //        tbldtlcont.updtime = tbl.updtime;

                        //        db.QL_trnsoitemcont.Add(tbldtlcont);
                        //        db.SaveChanges();
                        //    }
                        //}

                        //sSql = "UPDATE QL_mstoid SET lastoid=" + socontoid + " WHERE tablename='QL_trnsoitemcont'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.soitemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SO-FG" + tbl.soitemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnsoitemmst";
                                tblApp.oid = tbl.soitemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.soitemmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: SOitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tbl = db.QL_trnsoitemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnsoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                      
                        db.QL_trnsoitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string sotype, string printtype, Boolean cbprice)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
         
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOPrintOut.rpt"));
           

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE som.cmpcode='" + cmp + "' AND som.soitemmstoid IN (" + id + ")");
            report.SetParameterValue("sType", "item");
            report.SetParameterValue("sqlJoinMat", "(SELECT itemoid matoid, itemcode matcode, itemlongdesc matlongdesc, itemnote matnote FROM QL_mstitem) m ON matoid=sod.itemoid");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            report.Close(); report.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "SOFinishGoodReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}