﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;


namespace RSA.Controllers
{
    public class POAsetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpoassetmst
        {
            public string cmpcode { get; set; }
            public int poassetmstoid { get; set; }
            public string divgroup { get; set; }
            public string poassettype { get; set; }
            public int poassetpaytypeoid { get; set; }
            public string poassetno { get; set; }
            public DateTime poassetdate { get; set; }
            public string suppname { get; set; }
            public string poassetmststatus { get; set; }
            public string poassetmstnote { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
        }

        public class trnpoassetdtl
        {
            public int poassetdtlseq { get; set; }
            public int prassetmstoid { get; set; }
            public int prassetdtloid { get; set; }
            public string prassetno { get; set; }
            public string prassetdatestr { get; set; }
            public DateTime prassetdate { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal prassetqty { get; set; }
            public decimal poassetqty { get; set; }
            public int poassetunitoid { get; set; }
            public string poassetunit { get; set; }
            public decimal poassetprice { get; set; }
            public decimal poassetdtlamt { get; set; }
            public string poassetdtldisctype { get; set; }
            public decimal poassetdtldiscvalue { get; set; }
            public decimal poassetdtldiscamt { get; set; }
            public decimal poassetdtlnetto { get; set; }
            public string poassetdtlnote { get; set; }
            public string poassetdtlnote2 { get; set; }
            public string prassetarrdatereqstr { get; set; }
            public DateTime prassetarrdatereq { get; set; }
            public decimal lastpoprice { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnpoassetmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            ViewBag.approvalcode = approvalcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var poassetpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.poassetpaytypeoid);
            ViewBag.poassetpaytypeoid = poassetpaytypeoid;

            //sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'";
            //var porawunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.porawunitoid);
            //ViewBag.porawunitoid = porawunitoid;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prmstoid, int suppoid, int pomstoid)
        {
            List<trnpoassetdtl> tbl = new List<trnpoassetdtl>();

            sSql = "SELECT 0 AS poassetdtlseq, prd.prassetmstoid, prd.prassetdtloid, prm.prassetno, prd.prassetrefoid matgenoid, ISNULL((prd.prassetqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(pod.poassetqty - (CASE WHEN pod.poassetdtlres1 IS NULL THEN ISNULL(pod.closeqty, 0) WHEN pod.poassetdtlres1='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.poassetqty - CONVERT(decimal(18,2), pod.poassetdtlres1)) END)), 0) FROM QL_trnpoassetdtl pod INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=pod.cmpcode AND pom.poassetmstoid=pod.poassetmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prassetdtloid=prd.prassetdtloid AND pom.poassetmststatus NOT IN ('Cancel', 'Rejected') AND pod.poassetmstoid<>" + pomstoid + ")) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prassetdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0)), 0) AS prassetqty, prd.prassetunitoid AS poassetunitoid, m.matgencode, m.matgenlongdesc, m.matgenlimitqty, g2.gendesc AS poassetunit, 0.0 AS poassetqty, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.prassetrefoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.prassetrefoid ORDER BY crd.updtime DESC), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.prassetrefoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.prassetrefoid ORDER BY crd.updtime DESC), 1)) AS lastcurroid, isnull(prd.prassetdtlnote,'') AS poassetdtlnote, 0.0 AS poassetprice, CONVERT(VARCHAR(10), prm.prassetdate, 101) AS prassetdatestr, prm.prassetdate, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prassetarrdatereqstr, prd.prassetarrdatereq, 0.0 AS poassetdtlamt, 'A' AS poassetdtldisctype, 0.0 AS poassetdtldiscvalue, 0.0 AS poassetdtldiscamt, 0.0 AS poassetdtlnetto, '' AS poassetdtlnote2 FROM QL_prassetdtl prd INNER JOIN QL_prassetmst prm ON prm.cmpcode=prd.cmpcode AND prm.prassetmstoid=prd.prassetmstoid INNER JOIN QL_mstmatgen m ON prd.prassetrefoid=m.matgenoid INNER JOIN QL_mstgen g2 ON prd.prassetunitoid=g2.genoid WHERE prd.cmpcode='" + CompnyCode + "' AND prd.prassetmstoid=" + prmstoid + " AND prd.prassetdtlstatus='' AND prm.prassetmststatus='Approved'";

            tbl = db.Database.SqlQuery<trnpoassetdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpoassetdtl> dtDtl)
        {
            Session["QL_trnpoassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpoassetdtl"] == null)
            {
                Session["QL_trnpoassetdtl"] = new List<trnpoassetdtl>();
            }

            List<trnpoassetdtl> dataDtl = (List<trnpoassetdtl>)Session["QL_trnpoassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpoassetmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.potype = tbl.poassettype;
            ViewBag.potaxtype = tbl.poassettaxtype;
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            public string supptaxable { get; set; }
            public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, (CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 11 ELSE 0 END) supptaxamt, gp.gendesc supppayment  FROM QL_mstsupp s INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' and s.divgroupoid=" + divgroupoid + " AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class prassetmst
        {
            public int prassetmstoid { get; set; }
            public string prassetno { get; set; }
            public string prassetdate { get; set; }
            public string prassetexpdate { get; set; }
            public string deptname { get; set; }
            public string prassetmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp, int divgroupoid)
        {
            List<prassetmst> tbl = new List<prassetmst>();
            sSql = " SELECT DISTINCT prm.prassetmstoid, prassetno, CONVERT(VARCHAR(10), prassetdate, 101) AS prassetdate, CONVERT(VARCHAR(10), prassetexpdate, 101) AS prassetexpdate, deptname, prassetmstnote FROM QL_prassetmst prm INNER JOIN QL_prassetdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prassetmstoid=prd.prassetmstoid INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" + CompnyCode + "' AND prm.divgroupoid=" + divgroupoid + " AND prm.prassetmststatus='Approved' ORDER BY prassetdate DESC, prm.prassetmstoid DESC ";

            tbl = db.Database.SqlQuery<prassetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: PORawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil) //string filter
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "pom", divgroupoid, "divgroupoid");
            sSql = "SELECT pom.cmpcode, (select gendesc from ql_mstgen where genoid=pom.divgroupoid) divgroup, poassetmstoid, poassetno, poassetdate, suppname, poassetmststatus, poassetmstnote, divname FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.cmpcode='" + CompnyCode + "' AND s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND poassetmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND poassetmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND poassetmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND poassetdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poassetdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND poassetmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpoassetmst> dt = db.Database.SqlQuery<trnpoassetmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnpoassetmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: PORawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnpoassetmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnpoassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.poassetmstoid = ClassFunction.GenerateID("QL_trnpoassetmst");
                tbl.poassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.poassetmststatus = "In Process";

                Session["QL_trnpoassetdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpoassetmst.Find(cmp, id);

                sSql = "SELECT pod.poassetdtlseq, prd.prassetmstoid, prm.prassetno, CONVERT(VARCHAR(10), prm.prassetdate, 101) AS prassetdatestr, prm.prassetdate, pod.prassetdtloid, pod.poassetrefoid matgenoid, m.matgencode, m.matgenlongdesc, m.matgenlimitqty, pod.poassetqty, ISNULL((prd.prassetqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.poassetqty - (CASE WHEN podtl.poassetdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.poassetdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.poassetqty - CONVERT(decimal(18,2), podtl.poassetdtlres1)) END)), 0) FROM QL_trnpoassetdtl podtl INNER JOIN QL_trnpoassetmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.poassetmstoid=podtl.poassetmstoid WHERE podtl.prassetdtloid=prd.prassetdtloid AND podtl.poassetmstoid<>" + id + " AND pomst.poassetmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prassetdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0) AS prassetqty,  0.0 AS poqtyuse,  pod.poassetunitoid, g.gendesc AS poassetunit, pod.poassetprice, pod.poassetdtlamt, pod.poassetdtldisctype, pod.poassetdtldiscvalue, pod.poassetdtldiscamt, pod.poassetdtlnetto, pod.poassetdtlnote, /*ISNULL(pod.poassetdtlnote2,'') poassetdtlnote2,*/ ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.poassetrefoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.poassetrefoid ORDER BY crd.updtime), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.poassetrefoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.poassetrefoid ORDER BY crd.updtime), 1)) AS lastcurroid, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prassetarrdatereqstr, prd.prassetarrdatereq FROM QL_trnpoassetdtl pod INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=pod.cmpcode AND pom.poassetmstoid=pod.poassetmstoid INNER JOIN QL_mstmatgen m ON pod.poassetrefoid=m.matgenoid INNER JOIN QL_mstgen g ON pod.poassetunitoid=g.genoid INNER JOIN QL_prassetdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prassetdtloid=pod.prassetdtloid INNER JOIN QL_prassetmst prm ON prm.cmpcode=pod.cmpcode AND prm.prassetmstoid=pod.prassetmstoid WHERE pod.poassetmstoid=" + id + " AND pod.cmpcode='" + cmp + "' ORDER BY pod.poassetdtlseq";
                Session["QL_trnpoassetdtl"] = db.Database.SqlQuery<trnpoassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpoassetmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.poassetno == null)
                tbl.poassetno = "";

            List<trnpoassetdtl> dtDtl = (List<trnpoassetdtl>)Session["QL_trnpoassetdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].poassetqty <= 0)
                        {
                            ModelState.AddModelError("", "PO QTY for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }

                        if (dtDtl[i].poassetprice <= 0)
                        {
                            ModelState.AddModelError("", "PO Price for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }

                        sSql = "SELECT ISNULL((prd.prassetqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.poassetqty - (CASE WHEN podtl.poassetdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.poassetdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.poassetqty - CONVERT(decimal(18,2), podtl.poassetdtlres1)) END)), 0) FROM QL_trnpoassetdtl podtl INNER JOIN QL_trnpoassetmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.poassetmstoid=podtl.poassetmstoid WHERE podtl.cmpcode=prd.cmpcode AND podtl.prassetmstoid=prd.prassetmstoid AND  podtl.prassetdtloid=prd.prassetdtloid AND podtl.poassetmstoid<>" + tbl.poassetmstoid + " AND pomst.poassetmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prassetdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0.0) AS prassetqty FROM QL_prassetdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prassetdtloid=" + dtDtl[i].prassetdtloid + "";
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].prassetqty)
                            dtDtl[i].prassetqty = dQty;
                        if (dQty < dtDtl[i].poassetqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PR Qty!");

                        //sSql = "SELECT COUNT(-1) FROM QL_prrawdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prrawmstoid=" + dtDtl[i].prrawmstoid + " AND prd.prrawdtloid=" + dtDtl[i].prrawdtloid + " AND prd.prrawdtlstatus='COMPLETE'";
                        //var CheckDataExists = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (CheckDataExists > 0 )
                        //{
                        //    ModelState.AddModelError("", "There are some PR detail data has been closed by another user. You cannot use this PR detail data anymore!");
                        //}
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.poassetmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoassetmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpoassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpoassetdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.poassetratetoidrchar = "0";
                        tbl.poassetratetousdchar = "0";
                        tbl.poassetrate2toidrchar = "0";
                        tbl.poassetrate2tousdchar = "0";
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpoassetmst.Find(tbl.cmpcode, tbl.poassetmstoid) != null)
                                tbl.poassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.poassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                           
                            db.QL_trnpoassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.poassetmstoid + " WHERE tablename='QL_trnpoassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_prassetdtl SET prassetdtlstatus='', prassetdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND prassetdtloid IN (SELECT prassetdtloid FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_prassetmst SET prassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prassetmstoid IN (SELECT DISTINCT prassetmstoid FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpoassetdtl.Where(a => a.poassetmstoid == tbl.poassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpoassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpoassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpoassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.poassetdtloid = dtloid++;
                            tbldtl.poassetmstoid = tbl.poassetmstoid;
                            tbldtl.poassetdtlseq = i + 1;
                            tbldtl.prassetmstoid = dtDtl[i].prassetmstoid;
                            tbldtl.prassetdtloid = dtDtl[i].prassetdtloid;
                            tbldtl.poassetreftype = "QL_mstmatgen";
                            tbldtl.poassetrefoid = dtDtl[i].matgenoid;
                            tbldtl.poassetqty = dtDtl[i].poassetqty;
                            tbldtl.poassetunitoid = dtDtl[i].poassetunitoid;
                            tbldtl.poassetprice = dtDtl[i].poassetprice;
                            tbldtl.poassetdtlamt = dtDtl[i].poassetdtlamt;
                            tbldtl.poassetdtldisctype = dtDtl[i].poassetdtldisctype;
                            tbldtl.poassetdtldiscvalue = dtDtl[i].poassetdtldiscvalue;
                            tbldtl.poassetdtldiscamt = dtDtl[i].poassetdtldiscamt;
                            tbldtl.poassetdtlnetto = dtDtl[i].poassetdtlnetto;
                            tbldtl.poassetdtlnote = dtDtl[i].poassetdtlnote == null ? "" : dtDtl[i].poassetdtlnote;
                            tbldtl.poassetdtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnpoassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].poassetqty >= dtDtl[i].prassetqty)
                            {
                                sSql = "UPDATE QL_prassetdtl SET prassetdtlstatus='COMPLETE', prassetdtlres1='" + dtloid + "' WHERE cmpcode='" + tbl.cmpcode + "' AND prassetdtloid=" + dtDtl[i].prassetdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_prassetmst SET prassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND prassetmstoid=" + dtDtl[i].prassetmstoid + " AND (SELECT COUNT(prassetdtloid) FROM QL_prassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND prassetdtlstatus='' AND prassetmstoid=" + dtDtl[i].prassetmstoid + " AND prassetdtloid<>" + +dtDtl[i].prassetdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_TRNpoassetDTL'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.poassetmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-gen" + tbl.poassetmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpoassetmst";
                                tblApp.oid = tbl.poassetmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        //if (tbl.poassetmststatus.ToUpper() == "In Approval")
                        //{
                        //    var dt = null;
                        //    //Dim dt As DataTable = Session("AppPerson")
                        //    for (int i = 0; i < dtLastHdrData.Count(); i++)
                        //    {
                        //        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'PO-RAW" & poassetmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & poassetdate.Text & "', 'New', 'QL_trnpoassetmst', " & poassetmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();
                        //    }
                        //    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.poassetmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            tbl.poassetmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoassetmst tbl = db.QL_trnpoassetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_prassetdtl SET prassetdtlstatus='', prassetdtlres1='', prclosingqty=0.0 WHERE cmpcode='" + tbl.cmpcode + "' AND prassetdtloid IN (SELECT prassetdtloid FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prassetmst SET prassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prassetmstoid IN (SELECT DISTINCT prassetmstoid FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = GetQueryInsertStatus_Multi(Session("UserID"), DDLBusUnit.SelectedValue, "QL_trnpoassetmst", poassetmstoid.Text, "Open", "poassetmstoid", "QL_prrawmst", "prrawmstoid", "Open karena PO dihapus");
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnpoassetdtl.Where(a => a.poassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpoassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpoassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string supptype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpoassetmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));

            //if (supptype == "LOCAL")
            //    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));
            //else
            //    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_TrnEn.rpt"));

            //sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.poassetmstoid [Oid], CONVERT(VARCHAR(10), pom.poassetmstoid) AS [Draft No.], pom.poassetno [PO No.], pom.poassetdate [PO Date], pom.poassetmstnote [Header Note], pom.poassetmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.poassetmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.poassetmstdiscamt, 0) AS [Disc Amount], pom.poassettotalnetto [Total Netto], pom.poassettaxtype [Tax Type], CONVERT(VARCHAR(10), pom.poassettaxamt) AS [Tax Amount], ISNULL(pom.poassetvat, 0) AS [VAT], ISNULL(pom.poassetdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.poassetothercost, 0) AS [Other Cost], pom.poassetgrandtotalamt [Grand Total], pod.poassetdtloid [DtlOid], ISNULL((SELECT m2.matgendtl2code FROM QL_mstmatgendtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matgenoid=m2.matgenoid), m.matgencode) AS [Code], ISNULL((SELECT m2.matgendtl2name FROM QL_mstmatgendtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matgenoid=m2.matgenoid), m.matgenlongdesc) AS [Material], prm.prassetno AS [PR No.], prm.prassetdate AS [PR Date], pod.poassetqty AS [Qty], g2.gendesc AS [Unit], pod.poassetprice AS [Price], pod.poassetdtlamt AS [Amount], ISNULL(pod.poassetdtldiscamt, 0) AS [Detail Disc.], pod.poassetdtlnetto AS [Netto], pod.poassetdtlnote AS [Note], pom.poassettype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.poassetsuppref AS [SuppReff], pod.poassetdtldisctype AS [DtlDiscType], ISNULL(pod.poassetdtldiscvalue,0) AS [DtlDiscValue], pod.poassetdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prassetarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpoassetmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.poassetpaytypeoid INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode=pom.cmpcode AND pod.poassetmstoid=pom.poassetmstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pod.matgenoid LEFT JOIN QL_prassetmst prm ON prm.cmpcode=pod.cmpcode AND prm.prassetmstoid=pod.prassetmstoid LEFT JOIN QL_prassetdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prassetmstoid=pod.prassetmstoid AND prd.prassetdtloid=pod.prassetdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.poassetunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.poassetmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.poassetmstoid, pod.poassetdtlseq ";
            sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.poassetmstoid [Oid], CONVERT(VARCHAR(10), pom.poassetmstoid) AS [Draft No.], pom.poassetno [PO No.], pom.poassetdate [PO Date], pom.poassetmstnote [Header Note], pom.poassetmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.poassetmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.poassetmstdiscamt, 0) AS [Disc Amount], pom.poassettotalnetto [Total Netto], pom.poassettaxtype [Tax Type], CONVERT(VARCHAR(10), pom.poassettaxamt) AS [Tax Amount], ISNULL(pom.poassetvat, 0) AS [VAT], ISNULL(pom.poassetdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.poassetothercost, 0) AS [Other Cost], pom.poassetgrandtotalamt [Grand Total], pod.poassetdtloid [DtlOid], '' AS [Code], '' AS [Material], prm.prassetno AS [PR No.], prm.prassetdate AS [PR Date], pod.poassetqty AS [Qty], g2.gendesc AS [Unit], pod.poassetprice AS [Price], pod.poassetdtlamt AS [Amount], ISNULL(pod.poassetdtldiscamt, 0) AS [Detail Disc.], pod.poassetdtlnetto AS [Netto], pod.poassetdtlnote AS [Note], pom.poassettype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.poassetsuppref AS [SuppReff], pod.poassetdtldisctype AS [DtlDiscType], ISNULL(pod.poassetdtldiscvalue,0) AS [DtlDiscValue], pod.poassetdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prassetarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode=pom.cmpcode AND pod.poassetmstoid=pom.poassetmstoid left JOIN QL_mstmatgen m ON m.matgenoid=pod.poassetrefoid inner JOIN QL_prassetmst prm ON prm.cmpcode=pod.cmpcode AND prm.prassetmstoid=pod.prassetmstoid left JOIN QL_mstgen g1 ON g1.genoid=pom.poassetpaytypeoid  LEFT JOIN QL_prassetdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prassetmstoid=pod.prassetmstoid AND prd.prassetdtloid=pod.prassetdtloid left JOIN QL_mstgen g2 ON g2.genoid=pod.poassetunitoid left JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.poassetmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.poassetmstoid, pod.poassetdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPORaw");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "poassetMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}