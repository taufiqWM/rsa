﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class MaterialRequestNonKIKCancellationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnmatreqmst
        {
            public string cmpcode { get; set; }
            public int matreqmstoid { get; set; }
            public int deptoid { get; set; }
            public string matreqno { get; set; }
            public string matreqdate { get; set; }
            public string matreqmststatus { get; set; }
            public string matreqmstnote { get; set; }
            public string deptname { get; set; }
        }

        public class trnmatreqdtl
        {
            public string cmpcode { get; set; }
            public int matreqdtloid { get; set; }
            public int matreqmstoid { get; set; }
            public int matreqdtlseq { get; set; }
            public string matreqreftype { get; set; }
            public int matreqrefoid { get; set; }
            public string matreqrefcode { get; set; }
            public string matreqreflongdesc { get; set; }
            public decimal matreqqty { get; set; }
            public string matrequnit { get; set; }
            public string matreqdtlnote { get; set; }
            public string matreqdtlstatus { get; set; }
            public string matreqmststatus { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            //ViewBag.cmpcode = cmpcode;
            ViewBag.cmpcode = CompnyCode;
        }

        [HttpPost]
        public ActionResult GetMatReqData(string cmp)
        {
            List<trnmatreqmst> tbl = new List<trnmatreqmst>();

            sSql = "SELECT matreqmstoid, matreqno, CONVERT(VARCHAR(10), matreqdate, 101) AS matreqdate, deptname, gendesc AS matreqwh, matreqmstnote, matreqmststatus FROM QL_trnmatreqmst req INNER JOIN QL_mstdept de ON de.cmpcode=req.cmpcode AND de.deptoid=req.deptoid INNER JOIN QL_mstgen g ON genoid=matreqwhoid WHERE req.cmpcode='" + cmp + "' AND matreqmststatus='Post' AND matreqmstoid NOT IN (SELECT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" + cmp + "') ORDER BY matreqmstoid";
            
            tbl = db.Database.SqlQuery<trnmatreqmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int iOid)
        {
            List<trnmatreqdtl> tbl = new List<trnmatreqdtl>();

            sSql = "SELECT req.cmpcode, req.matreqdtloid, req.matreqmstoid, matreqdtlseq, matreqreftype, matreqrefoid, (CASE matreqreftype WHEN 'Raw' THEN (SELECT matrawcode FROM QL_mstmatraw WHERE matrawoid=matreqrefoid) WHEN 'General' THEN (SELECT matgencode FROM QL_mstmatgen WHERE matgenoid=matreqrefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart WHERE sparepartoid=matreqrefoid) WHEN 'Log' THEN (SELECT m.logno FROM QL_mstlog m WHERE m.logoid=req.matreqrefoid) WHEN 'Sawn Timber' THEN (SELECT m.palletno FROM QL_mstpallet m WHERE m.palletoid=req.matreqrefoid) ELSE '' END) AS matreqrefcode, (CASE matreqreftype WHEN 'Raw' THEN (SELECT matrawlongdesc FROM QL_mstmatraw WHERE matrawoid=matreqrefoid) WHEN 'General' THEN (SELECT matgenlongdesc FROM QL_mstmatgen WHERE matgenoid=matreqrefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart WHERE sparepartoid=matreqrefoid) WHEN 'Log' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m INNER JOIN QL_mstlog m1 ON m1.refoid = m.matrawoid WHERE m1.logoid=req.matreqrefoid ) WHEN 'Sawn Timber' THEN (SELECT m.palletlongdesc FROM QL_mstpallet m WHERE m.palletoid=req.matreqrefoid) ELSE '' END) AS matreqreflongdesc, matreqqty, matrequnitoid, gendesc AS matrequnit, matreqdtlnote, matreqdtlstatus, reqm.matreqmststatus FROM QL_trnmatreqdtl req INNER JOIN QL_trnmatreqmst reqm ON req.cmpcode=reqm.cmpcode AND req.matreqmstoid=reqm.matreqmstoid INNER JOIN QL_mstgen ON genoid=matrequnitoid WHERE req.cmpcode='" + cmp + "' AND reqm.matreqmstoid=" + iOid + " ORDER BY matreqdtlseq";
            tbl = db.Database.SqlQuery<trnmatreqdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnmatreqdtl> dtDtl)
        {
            Session["QL_trnmatreqdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<trnmatreqdtl> dtDtl = (List<trnmatreqdtl>)Session["QL_trnmatreqdtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].matreqmstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    sSql = "SELECT COUNT(*) FROM QL_trnmatreqmst WHERE matreqmstoid=" + dtDtl[0].matreqmstoid + " AND matreqmststatus='Cancel'";
                    var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck > 0)
                    {
                        ModelState.AddModelError("", "This data has been canceled by another user. Please CANCEL this transaction and try again!");
                    }

                    sSql = "SELECT COUNT(*) FROM QL_trnmatusagedtl usgd WHERE usgd.cmpcode='" + dtDtl[0].cmpcode + "' AND usgd.matreqmstoid='" + dtDtl[0].matreqmstoid + "'";
                    var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck2 > 0)
                    {
                        ModelState.AddModelError("", "This data has been Material Usage Non KIK by another user. Please CANCEL this transaction and try again!");
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                           sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND matreqmstoid=" + dtDtl[0].matreqmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND matreqmstoid=" + dtDtl[0].matreqmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                            objTrans.Commit();
                            Session["QL_trnmatreqdtl"] = null;
                            TempData["SuccessMessage"] = "Success";
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}