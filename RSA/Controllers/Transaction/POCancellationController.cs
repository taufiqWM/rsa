﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class POCancellationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpomst
        {
            public string cmpcode { get; set; }
            public int pomstoid { get; set; }
            public int suppoid { get; set; }
            public string pono { get; set; }
            public string sType { get; set; }
            public string podate { get; set; }
            public string pomststatus { get; set; }
            public string pomstnote { get; set; }
            public string suppname { get; set; }
        }

        public class trnpodtl
        {
            public string cmpcode { get; set; }
            public int podtloid { get; set; }
            public int pomstoid { get; set; }
            public string potype { get; set; }
            public int podtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal poqty { get; set; }
            public string pounit { get; set; }
            public string podtlnote { get; set; }
            public string podtlstatus { get; set; }
            public string pomststatus { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, string sType)
        {
            List<trnpomst> tbl = new List<trnpomst>();

            sSql = "SELECT po" + sType + "mstoid AS pomstoid, po" + sType + "no AS pono, CONVERT(VARCHAR(10), po" + sType + "date, 101) AS podate, po" + sType + "mststatus AS pomststatus, po" + sType + "mstnote AS pomstnote, s.suppname, s.suppemail, po" + sType + "date FROM QL_trnpo" + sType + "mst p INNER JOIN QL_mstsupp S ON p.suppoid=s.suppoid WHERE p.cmpcode='" + cmp + "' AND (po" + sType + "mststatus='Approved') AND po" + sType + "mstoid NOT IN (SELECT regm.po" + sType + "mstoid FROM QL_trnmr" + sType + "mst regm WHERE regm.cmpcode='" + cmp + "')";



            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND p.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " ORDER BY po" + sType + "date DESC, pomstoid DESC";

            tbl = db.Database.SqlQuery<trnpomst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<trnpodtl> tbl = new List<trnpodtl>();
            if (sType.ToUpper() == "RAW")
                sSql = "SELECT pod.porawdtloid AS podtloid, pod.porawdtlseq AS podtlseq, pod.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, pod.porawqty AS poqty, g.gendesc AS pounit, pod.porawdtlnote AS podtlnote, pod.porawdtlstatus AS podtlstatus FROM QL_trnporawdtl pod INNER JOIN QL_mstmatraw m ON pod.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON pod.porawunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.porawmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "GEN")
                sSql = "SELECT pod.pogendtloid AS podtloid, pod.pogendtlseq AS podtlseq, pod.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, pod.pogenqty AS poqty, g.gendesc AS pounit, pod.pogendtlnote AS podtlnote, pod.pogendtlstatus AS podtlstatus FROM QL_trnpogendtl pod INNER JOIN QL_mstmatgen m ON pod.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON pod.pogenunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.pogenmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "SP")
                sSql = "SELECT pod.pospdtloid AS podtloid, pod.pospdtlseq AS podtlseq, pod.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, pod.pospqty AS poqty, g.gendesc AS pounit, pod.pospdtlnote AS podtlnote, pod.pospdtlstatus AS podtlstatus FROM QL_trnpospdtl pod INNER JOIN QL_mstsparepart m ON pod.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON pod.pospunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.pospmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "ASSET")
                sSql = "SELECT pod.poassetdtloid AS podtloid, pod.poassetdtlseq AS podtlseq, pod.poassetrefoid AS matoid, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=pod.poassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=pod.poassetrefoid) ELSE '' END) AS matcode, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=pod.poassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=pod.poassetrefoid) ELSE '' END) AS matlongdesc, pod.poassetqty AS poqty, g.gendesc AS pounit, pod.poassetdtlnote AS podtlnote, pod.poassetdtlstatus AS podtlstatus FROM QL_trnpoassetdtl pod INNER JOIN QL_mstgen g ON pod.poassetunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.poassetmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "WIP")
                sSql = "SELECT pod.powipdtloid AS podtloid, pod.powipdtlseq AS podtlseq, pod.matwipoid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, (RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END))) AS matlongdesc, pod.powipqty AS poqty, g.gendesc AS pounit, pod.powipdtlnote AS podtlnote, pod.powipdtlstatus AS podtlstatus FROM QL_trnpowipdtl pod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=pod.matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid AND c1.cat1res1='Raw' AND c1.cat1res2='Log' INNER JOIN QL_mstgen g ON pod.powipunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.powipmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "SAWN")
                sSql = "SELECT pod.posawndtloid AS podtloid, pod.posawndtlseq AS podtlseq, pod.matsawnoid AS matoid, matrawcode AS matcode, matrawlongdesc AS matlongdesc, pod.posawnqty AS poqty, g.gendesc AS pounit, pod.posawndtlnote AS podtlnote, pod.posawndtlstatus AS podtlstatus FROM QL_trnposawndtl pod INNER JOIN QL_mstmatraw m ON matrawoid=matsawnoid INNER JOIN QL_mstgen g ON pod.posawnunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.posawnmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "SAWN2")
                sSql = "SELECT pod.posawn2dtloid AS podtloid, pod.posawn2dtlseq AS podtlseq, pod.matsawnoid AS matoid, (cat1code + '.' + cat2code) AS matcode, (RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END))) AS matlongdesc, pod.posawn2qty AS poqty, g.gendesc AS pounit, pod.posawn2dtlnote AS podtlnote, pod.posawn2dtlstatus AS podtlstatus FROM QL_trnposawn2dtl pod INNER JOIN QL_mstcat2 m ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1oid=m.cat1oid INNER JOIN QL_mstgen g ON pod.posawn2unitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.posawnmstoid=" + iOid + " ORDER BY podtlseq";
            else if (sType.ToUpper() == "ITEM")
                sSql = "SELECT pod.poitemdtloid AS podtloid, pod.poitemdtlseq AS podtlseq, pod.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, pod.poitemqty AS poqty, g.gendesc AS pounit, pod.poitemdtlnote AS podtlnote, pod.poitemdtlstatus AS podtlstatus FROM QL_trnpoitemdtl pod INNER JOIN QL_mstitem m ON pod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON pod.poitemunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.poitemmstoid=" + iOid + " ORDER BY podtlseq";

            tbl = db.Database.SqlQuery<trnpodtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpodtl> dtDtl)
        {
            Session["QL_trnpodtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            List<trnpodtl> dtDtl = (List<trnpodtl>)Session["QL_trnpodtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].pomstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    sSql = "SELECT COUNT(*) FROM QL_trnpo" + dtDtl[0].potype + "mst WHERE po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid + " AND po" + dtDtl[0].potype + "mststatus='Cancel'";
                    var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck > 0)
                    {
                        ModelState.AddModelError("", "This data has been canceled by another user. Please CANCEL this transaction and try again!");
                    }

                    sSql = "SELECT COUNT(*) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.registermstoid=regm.registermstoid  WHERE regd.cmpcode='" + dtDtl[0].cmpcode + "' AND regm.registertype='" + dtDtl[0].potype + "' AND regd.porefmstoid=" + dtDtl[0].pomstoid;
                    var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck2 > 0)
                    {
                        ModelState.AddModelError("", "This data has been Registered by another user. Please CANCEL this transaction and try again!");
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if ((dtDtl[0].potype.ToUpper() != "SERVICE") && (dtDtl[0].potype.ToUpper() != "SAWN2"))
                            {
                                sSql = "UPDATE QL_pr" + dtDtl[0].potype + "dtl SET pr" + dtDtl[0].potype + "dtlstatus='', pr" + dtDtl[0].potype + "dtlres1='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + dtDtl[0].potype + "dtloid IN (SELECT pr" + dtDtl[0].potype + "dtloid FROM QL_trnpo" + dtDtl[0].potype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_pr" + dtDtl[0].potype + "mst SET pr" + dtDtl[0].potype + "mststatus='Approved' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + dtDtl[0].potype + "mstoid IN (SELECT DISTINCT pr" + dtDtl[0].potype + "mstoid FROM QL_trnpo" + dtDtl[0].potype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (dtDtl[0].potype.ToUpper() == "SAWN2")
                            {
                                sSql = "UPDATE QL_prsawndtl SET prsawndtlstatus='', prsawndtlres1='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND prsawndtloid IN (SELECT prsawndtloid FROM QL_trnposawn2dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND posawn2mstoid=" + dtDtl[0].pomstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_prsawnmst SET prsawnmststatus='Approved' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND prsawnmstoid IN (SELECT DISTINCT prsawnmstoid FROM QL_trnposawn2dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND posawn2mstoid=" + dtDtl[0].pomstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            

                            sSql = "UPDATE QL_trnpo" + dtDtl[0].potype + "mst SET po" + dtDtl[0].potype + "mststatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            if (dtDtl[0].pomststatus == "In Approval")
                            {
                                sSql = "UPDATE QL_approval SET statusrequest='Cancel', event='Cancel' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND tablename='QL_trnpo" + dtDtl[0].potype + "mst' AND oid=" + dtDtl[0].pomstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            objTrans.Commit();
                            Session["QL_trnpodtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}