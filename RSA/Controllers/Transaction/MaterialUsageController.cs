﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class MaterialUsageController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();

        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string HRISServer = System.Configuration.ConfigurationManager.AppSettings["HRISServer"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public MaterialUsageController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class usagemst
        {
            public string cmpcode { get; set; }
            public int usagemstoid { get; set; }
            public string divgroup { get; set; }
            public string usageno { get; set; }
            public DateTime usagedate { get; set; }
            public string routename { get; set; }
            public string usagemststatus { get; set; }
            public string usagemstnote { get; set; }
            public string usagewh { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public string matrawlongdesc { get; set; }
            public string postappreason { get; set; }
        }
     
        public static string GetQueryInitDDLWH(string cmpcode, string action)
        {
            var cmp = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            var res = "SELECT * FROM QL_mstgen WHERE cmpcode='" + cmp + "' AND gengroup='MATERIAL LOCATION' AND genoid>0 AND gendesc='GUDANG PRODUKSI'";
            if (action == "New Data")
                res += " AND activeflag='ACTIVE'";
            res += " ORDER BY gendesc";
            return res;
        }

        private void InitDDL(QL_trnusagemst tbl, string action)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            var cmp = cmpcode.First().Value;
            if (!string.IsNullOrEmpty(tbl.cmpcode) && cmpcode.Count() > 0)
                cmp = tbl.cmpcode;

            var usagewhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(GetQueryInitDDLWH(cmp, action)).ToList(), "genoid", "gendesc", tbl.usagewhoid);
            ViewBag.usagewhoid = usagewhoid;

        }

        private string GenerateNo(string cmp)
        {
            var usageno = "";
            if (!string.IsNullOrEmpty(cmp))
            {
                string sNo = "USG-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(usageno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnusagemst WHERE cmpcode='" + cmp + "' AND usageno LIKE '%" + sNo + "%'";
                usageno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return usageno;
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode, string action)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = GetQueryInitDDLWH(cmpcode, action);
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public class usagedtl2
        {
            public int usagedtl2seq { get; set; }
            public int womstoid { get; set; }
            public string usagedtl2note { get; set; }
            public string mattype { get; set; }
            public string refno { get; set; }
            public string refno2 { get; set; }
            public string sn { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public int usagedtl2unitoid { get; set; }
            public string usagedtl2unit { get; set; }
            public decimal usagedtl2qty { get; set; }
            public decimal usagevalueidr { get; set; }
            public decimal usagevalueusd { get; set; }
            public decimal konnversiberat { get; set; }
            public decimal totalusagevalueidr { get; set; }
            public decimal totalusagevalueusd { get; set; }
            public decimal stock { get; set; }
        }

        [HttpPost]
        public ActionResult GetMatData(string cmp, int divgroupoid, int whoid)
        {
            var result = "";
            JsonResult js = null;
            List<usagedtl2> tbl = new List<usagedtl2>();
            try
            {
                sSql = "SELECT * FROM( ";
                sSql += "SELECT 0 usagedtl2seq, con.refname mattype, con.refoid matoid, matrawcode matcode,i.matrawlongdesc matlongdesc, sum(qtyin - qtyout) stock, isnull(con.refno,'') refno, isnull(con.serialno,'') sn, isnull((select conx.refno2 from QL_conmat conx where conx.conmtroid = (select max(x.conmtroid) from ql_conmat x where x.refoid = con.refoid and x.refname = con.refname)),'')  refno2, 0.0 usagedtl2qty, i.matrawunitoid usagedtl2unitoid, (select x.gendesc from ql_mstgen x where genoid=i.matrawunitoid) usagedtl2unit, (case i.matrawres3 when '' then 1 else isnull(i.beratpcs,0.0) end) konnversiberat,'' usagedtl2note from QL_conmat con inner join QL_mstmatraw i on i.matrawoid = con.refoid and con.refname = 'RAW MATERIAL' WHERE con.divgroupoid=" + divgroupoid+" AND con.cmpcode='" + cmp + "' and con.mtrwhoid="+whoid+ " group by  con.refoid, refno, con.serialno, matrawlongdesc, matrawcode, matrawunitoid, matrawres3, i.beratpcs, refname, con.mtrwhoid having  sum(qtyin-qtyout)>0 ";
                sSql += "  UNION ALL  SELECT DISTINCT 0 usagedtl2seq, con.refname mattype, con.refoid matoid, '' matcode,i.sodtldesc matlongdesc, sum(qtyin - qtyout) stock, isnull(con.refno,'') refno, isnull(con.serialno,'') sn, isnull((select conx.refno2 from QL_conmat conx where conx.conmtroid = (select max(x.conmtroid) from ql_conmat x where x.refoid = con.refoid and x.refname = con.refname)),'')  refno2, 0.0 usagedtl2qty, i.sorawunitoid usagedtl2unitoid, (select x.gendesc from ql_mstgen x where genoid=i.sorawunitoid) usagedtl2unit, 1 konnversiberat,'' usagedtl2note from QL_conmat con inner join QL_trnsorawdtl i on i.sorawdtloid = con.refoid and con.refname = 'SHEET' WHERE con.divgroupoid=" + divgroupoid + " AND con.cmpcode='" + cmp + "' and con.mtrwhoid=" + whoid + " group by  con.refoid, refno, con.serialno, sodtldesc, sorawunitoid, refname, con.mtrwhoid having  sum(qtyin-qtyout)>0 ";
                sSql += " )AS t ORDER BY matlongdesc";
                tbl = db.Database.SqlQuery<usagedtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].usagedtl2seq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch(Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData2(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<usagedtl2> dtl1 = new List<usagedtl2>();

            try
            {
                sSql = "SELECT DISTINCT 0 usagedtl2seq, mattype, matoid, matcode, matlongdesc, (select sum(qtyin - qtyout) from ql_conmat c where c.refoid=u.matoid and c.refname=u.mattype and c.refno=u.refno and c.serialno=u.sn) stock, refno, sn, refno1 refno2, usagedtl2qty, usagedtl2unitoid, usagedtl2unit, konnversiberat,  usagedtl2note from QL_trnusagedtl2 u WHERE u.cmpcode='" + cmp + "' and u.usagemstoid="+oid;
                dtl1 = db.Database.SqlQuery<usagedtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].usagedtl2seq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class usagedtl
        {
            public int usagedtlseq { get; set; }
            public int womstoid { get; set; }
            public string usagedtlnote { get; set; }
            public string wotype { get; set; }
            public string wono { get; set; }
            public string itemlongdesc { get; set; }
            public string wodate { get; set; }
            public int confirmmstoid { get; set; }
            public int confirmdtloid { get; set; }
            public int sodtloid { get; set; }
            public string confirmno { get; set; }
            public decimal confirmqty { get; set; }
            public decimal berat { get; set; }
            public decimal totalberat { get; set; }
            public decimal usagevalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int divgroupoid)
        {
            var result = "";
            JsonResult js = null;
            List<usagedtl> tbl = new List<usagedtl>();

            try
            {
                sSql = "SELECT 0 usagedtlseq, w.womstoid, w.sodtloid, i.itemlongdesc, wono, FORMAT(wodate, 'MM/dd/yyyy') wodate, c.confirmmstoid, confirmdtloid, confirmno, confirmqty, w.wsheet berat,0.0 AS usageqty, 'FINISH GOOD' wotype, '' AS usagedtlnote FROM QL_trnconfirmmst c inner join ql_trnconfirmdtl d on d.confirmmstoid=c.confirmmstoid inner join QL_trnwomst w on w.womstoid=d.womstoid inner join ql_mstitem i on w.itemoid=i.itemoid WHERE w.cmpcode='" + cmp + "'  AND ISNULL(womstres1,'')='' AND ISNULL(womstres2,'')='' and confirmmststatus IN('Post','Approved') and w.divgroupoid=" + divgroupoid+ " and confirmdtloid not in (select confirmdtloid from ql_trnusagedtl)";
                sSql += " Union ALL SELECT 0 usagedtlseq, w.womstoid, w.sodtloid, i.sodtldesc itemlongdesc, wono, FORMAT(wodate, 'MM/dd/yyyy') wodate, c.confirmmstoid, confirmdtloid, confirmno, confirmqty, w.wsheet berat, 0.0 AS usageqty, 'SHEET' wotype,'' AS usagedtlnote FROM  QL_trnconfirmmst c inner join ql_trnconfirmdtl d on d.confirmmstoid=c.confirmmstoid inner join QL_trnwomst w on w.womstoid=d.womstoid inner join ql_trnsorawdtl i on w.itemoid=i.sorawdtloid WHERE w.cmpcode='" + cmp + "' and w.divgroupoid=" + divgroupoid + "  AND ISNULL(womstres1,'')='Sheet' AND ISNULL(womstres2,'')='' and confirmmststatus IN('Post','Approved') and confirmdtloid not in (select confirmdtloid from ql_trnusagedtl)";
                tbl = db.Database.SqlQuery<usagedtl>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].usagedtlseq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<usagedtl> dtl1 = new List<usagedtl>();

            try
            {
                sSql = "SELECT 0 usagedtlseq, u.womstoid, u.sodtloid, (case wotype when 'FINISH GOOD' then (select itemlongdesc from ql_mstitem x where x.itemoid=w.itemoid) else (select sodtldesc from ql_trnsorawdtl where sorawdtloid=u.sodtloid) end) itemlongdesc, u.wono, u.wodate, confirmmstoid, confirmdtloid, confirmno, confirmqty, berat, totalberat, wotype, usagedtlnote FROM QL_trnusagedtl u inner join QL_trnwomst w on u.womstoid=w.womstoid WHERE u.cmpcode='" + cmp + "' and usagemstoid='"+oid+"'";
                dtl1 = db.Database.SqlQuery<usagedtl>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].usagedtlseq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: RawMaterialUsage
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "usagem", divgroupoid, "divgroupoid");
            sSql = "SELECT usagem.cmpcode, (select gendesc from ql_mstgen where genoid=usagem.divgroupoid) divgroup, div.divname, usagem.usagemstoid, usagem.usageno, usagedate,  g1.gendesc AS usagewh, usagem.usagemststatus, usagem.usagemstnote, usagem.createuser FROM QL_trnusagemst usagem INNER JOIN QL_mstgen g1 ON g1.genoid=usagem.usagewhoid INNER JOIN QL_mstdivision div ON div.cmpcode=usagem.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "usagem.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "usagem.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND usagemststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND usagemststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND usagemststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND usagedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND usagedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND usagemststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND usagem.createuser='" + Session["UserID"].ToString() + "'";

            List<usagemst> dt = db.Database.SqlQuery<usagemst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnusagemst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: RawMaterialUsage/PrintReport/id/cmp?type1=&type2=
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            ids = ClassFunction.Left(ids, ids.Length - 1);
            ids = ids.Replace("x", ",");

            var rptname = "rptUsageByWO";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT mum.usagemstoid AS [Oid], CONVERT(VARCHAR(10), mum.usagemstoid) AS [Draft No.], usageno AS [Usage No.], usagedate AS [Usage Date], d.routename AS [Department], '' AS [Request No.], gwh.gendesc AS [Warehouse], usagemstnote AS [Header Note], usagemststatus AS [Status], usagereftype AS [Type], matrawcode AS [Code], matrawlongdesc AS [Description], usageqty AS [Qty], gunit.gendesc AS [Unit], usagedtlnote AS [Detail Note], divname AS [BU Name], divaddress AS [BU Address], gcity.gendesc AS [BU City], gprov.gendesc AS [BU Province], gcont.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Telp. 1], ISNULL(divphone2, '') AS [BU Telp. 2], ISNULL(divfax1, '') AS [BU Fax 1], ISNULL(divfax2, '') AS [BU Fax 2], ISNULL(divemail, '') AS [BU Email], ISNULL(divpostcode, '') AS [BU Post Code], '' AS [Division], som.wono [SO No.], ''  [Person Name], ''  [NIP] FROM QL_trnusagemst mum INNER JOIN QL_trnusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagemstoid=mum.usagemstoid INNER JOIN QL_trnwomst som ON som.cmpcode=mum.cmpcode AND mum.womstoid=som.womstoid INNER JOIN QL_mstroute d ON d.cmpcode=mum.cmpcode AND d.routeoid=mum.routeoid INNER JOIN QL_mstmatraw m ON m.matrawoid=mud.usagerefoid INNER JOIN QL_mstgen gwh ON gwh.genoid=usagewhoid INNER JOIN QL_mstgen gunit ON gunit.genoid=usageunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode INNER JOIN QL_mstgen gcity ON gcity.genoid=divcityoid INNER JOIN QL_mstgen gprov ON gprov.genoid=CAST(gcity.genother1 AS INT) INNER JOIN QL_mstgen gcont ON gcont.genoid=CAST(gcity.genother2 AS INT) WHERE mum.usagemstoid IN (" + ids + ") ORDER BY mum.usagemstoid, usagedtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", Session["UserName"].ToString());
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "MaterialUsagePrintOut.pdf");
        }

        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnusagemst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnusagemst();
                tbl.usagemstoid = ClassFunction.GenerateID("QL_trnusagemst");
                tbl.usagedate = ClassFunction.GetServerTime();
                tbl.usagemststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnusagemst.Find(cmp, id);
              
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        private string generateBatchNo(string refno)
        {
            var tgl = ClassFunction.GetServerTime();
            var sNo = refno + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno2, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refno2 LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 3);
            sNo = sNo + sCounter;

            return sNo;
        }

        private string UpdateValueFG(ref QL_RSAEntities db, QL_trnusagemst tbl, List<usagedtl> dtl1)
        {
            var error =  "";
            try
            {
                //update surat jalan
                foreach (var item in dtl1)
                {
                    decimal value_upd = tbl.usagefgvalueidr.Value * item.berat;
                    if (item.womstoid > 0)
                    {
                        var list_wo = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == item.womstoid);
                        int item_id = list_wo.itemoid;
                        var list_stock = db.QL_conmat.FirstOrDefault(x => x.refoid == item_id && x.deptoid == item.womstoid && x.formaction == "QL_trnconfirmdtl");
                        if (list_stock != null)
                        {
                            var refno = list_stock.refno;
                            var list2 = db.QL_conmat.Where(x => x.refoid == item_id && x.refno == refno && x.formaction != "QL_trnconfirmdtl");
                            if (list2 != null && list2.Count() > 0)
                            {
                                //update shipment
                                foreach (var st in list2)
                                {
                                    if (st.formaction == "QL_trnshipmentitemdtl")
                                    {
                                        sSql = $"UPDATE QL_trnshipmentitemdtl SET shipmentitemvalueidr={value_upd} WHERE itemoid={st.refoid} AND sn='{st.refno}' AND shipmentitemmstoid={st.formoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    else if (st.formaction == "QL_trnshipmentrawdtl")
                                    {
                                        sSql = $"UPDATE QL_trnshipmentrawdtl SET shipmentrawvalueidr={value_upd} WHERE matrawoid={st.refoid} AND sn='{st.refno}' AND shipmentrawmstoid={st.formoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
                //update gl
                var iDelAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode, tbl.divgroupoid.Value));
                var iHPPAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_HPP_JUAL", CompnyCode, tbl.divgroupoid.Value));
                var wo_id = dtl1.Select(x => x.womstoid);
                var fg_id = db.QL_trnwomst.Where(x => wo_id.Contains(x.womstoid)).Select(s => s.itemoid);
                var list_refno = db.QL_conmat.Where(x => x.formaction == "QL_trnconfirmdtl" && wo_id.Contains(x.deptoid)).Select(x => x.refno);
                var list_con = db.QL_conmat.Where(x => x.formaction != "QL_trnconfirmdtl" && fg_id.Contains(x.refoid) && list_refno.Contains(x.refno));
                if (list_con != null && list_con.Count() > 0)
                {
                    var cek_sj = list_con.Where(x => x.formaction == "QL_trnshipmentitemdtl");
                    if (cek_sj != null && cek_sj.Count() > 0)
                    {
                        foreach (var sj in cek_sj.GroupBy(g => g.formoid))
                        {
                            var sj_id = sj.Key;
                            var tbl_sj = db.QL_trnshipmentitemdtl.Join(db.QL_trnshipmentitemmst, d => d.shipmentitemmstoid, h => h.shipmentitemmstoid, (d, h) => new { h.shipmentitemmstoid, h.shipmentitemmststatus, d.shipmentitemqty, d.shipmentitemvalueidr, d.shipmentitemvalueusd }).Where(w => w.shipmentitemmstoid == sj_id && new List<string> { "Post", "Approved", "Closed" }.Contains(w.shipmentitemmststatus));
                            if (tbl_sj != null && tbl_sj.Count() > 0)
                            {
                                decimal sum_idr = tbl_sj.Sum(s => s.shipmentitemvalueidr * s.shipmentitemqty);
                                decimal sum_usd = tbl_sj.Sum(s => s.shipmentitemvalueusd * s.shipmentitemqty);
                                sSql = $"UPDATE QL_trngldtl SET glamt={sum_idr}, glamtidr={sum_idr}, glamtusd={sum_usd} WHERE glother1='QL_trnshipmentitemmst {sj_id}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        var sj_refid = cek_sj.Select(s => s.formoid).Distinct();
                        var list_si = db.QL_trnaritemdtl.Where(x => sj_refid.Contains(x.shipmentitemmstoid));
                        if (list_si != null && list_si.Count() > 0)
                        {
                            foreach (var si in list_si.GroupBy(g => g.aritemmstoid))
                            {
                                var ar_id = si.Key;
                                var tbl_ar = db.QL_trnaritemdtl.Join(db.QL_trnaritemmst, d => d.aritemmstoid, h => h.aritemmstoid, (d, h) => new { h.aritemmstoid, h.aritemmststatus, d.aritemqty, d.shipmentitemdtloid }).Join(db.QL_trnshipmentitemdtl, d => d.shipmentitemdtloid, shd => shd.shipmentitemdtloid, (d, shd) => new { d.aritemmstoid, d.aritemmststatus, d.aritemqty, d.shipmentitemdtloid, shd.shipmentitemvalueidr, shd.shipmentitemvalueusd }).Where(w => w.aritemmstoid == ar_id && new List<string> { "Post", "Approved", "Closed" }.Contains(w.aritemmststatus));
                                if (tbl_ar != null && tbl_ar.Count() > 0)
                                {
                                    decimal sum_idr = tbl_ar.Sum(s => s.shipmentitemvalueidr * s.aritemqty);
                                    decimal sum_usd = tbl_ar.Sum(s => s.shipmentitemvalueusd * s.aritemqty);
                                    sSql = $"UPDATE QL_trngldtl SET glamt={sum_idr}, glamtidr={sum_idr}, glamtusd={sum_usd} WHERE glother1='QL_trnaritemmst {ar_id}' AND acctgoid IN({iDelAcctgOid},{iHPPAcctgOid})";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    cek_sj = list_con.Where(x => x.formaction == "QL_trnshipmentrawdtl");
                    if (cek_sj != null && cek_sj.Count() > 0)
                    {
                        foreach (var sj in cek_sj.GroupBy(g => g.formoid))
                        {
                            var sj_id = sj.Key;
                            var tbl_sj = db.QL_trnshipmentrawdtl.Join(db.QL_trnshipmentrawmst, d => d.shipmentrawmstoid, h => h.shipmentrawmstoid, (d, h) => new { h.shipmentrawmstoid, h.shipmentrawmststatus, d.shipmentrawqty, d.shipmentrawvalueidr, d.shipmentrawvalueusd }).Where(w => w.shipmentrawmstoid == sj_id && new List<string> { "Post", "Approved", "Closed" }.Contains(w.shipmentrawmststatus));
                            if (tbl_sj != null && tbl_sj.Count() > 0)
                            {
                                decimal sum_idr = tbl_sj.Sum(s => s.shipmentrawvalueidr * s.shipmentrawqty);
                                decimal sum_usd = tbl_sj.Sum(s => s.shipmentrawvalueusd * s.shipmentrawqty);
                                sSql = $"UPDATE QL_trngldtl SET glamt={sum_idr}, glamtidr={sum_idr}, glamtusd={sum_usd} WHERE glother1='QL_trnshipmentrawmst {sj_id}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        var sj_refid = cek_sj.Select(s => s.formoid).Distinct();
                        var list_si = db.QL_trnarrawdtl.Where(x => sj_refid.Contains(x.shipmentrawmstoid));
                        if (list_si != null && list_si.Count() > 0)
                        {
                            foreach (var si in list_si.GroupBy(g => g.arrawmstoid))
                            {
                                var ar_id = si.Key;
                                var tbl_ar = db.QL_trnarrawdtl.Join(db.QL_trnarrawmst, d => d.arrawmstoid, h => h.arrawmstoid, (d, h) => new { h.arrawmstoid, h.arrawmststatus, d.arrawqty, d.shipmentrawdtloid }).Join(db.QL_trnshipmentrawdtl, d => d.shipmentrawdtloid, shd => shd.shipmentrawdtloid, (d, shd) => new { d.arrawmstoid, d.arrawmststatus, d.arrawqty, d.shipmentrawdtloid, shd.shipmentrawvalueidr, shd.shipmentrawvalueusd }).Where(w => w.arrawmstoid == ar_id && new List<string> { "Post", "Approved", "Closed" }.Contains(w.arrawmststatus));
                                if (tbl_ar != null && tbl_ar.Count() > 0)
                                {
                                    decimal sum_idr = tbl_ar.Sum(s => s.shipmentrawvalueidr * s.arrawqty);
                                    decimal sum_usd = tbl_ar.Sum(s => s.shipmentrawvalueusd * s.arrawqty);
                                    sSql = $"UPDATE QL_trngldtl SET glamt={sum_idr}, glamtidr={sum_idr}, glamtusd={sum_usd} WHERE glother1='QL_trnarrawmst {ar_id}' AND acctgoid IN({iDelAcctgOid},{iHPPAcctgOid})";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
                //update conmat
                foreach (var con in dtl1)
                {
                    decimal value_upd = tbl.usagefgvalueidr.Value * con.berat;
                    if (con.womstoid > 0)
                    {
                        var list_wo = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == con.womstoid);
                        int item_id = list_wo.itemoid;
                        var list_stock = db.QL_conmat.FirstOrDefault(x => x.refoid == item_id && x.deptoid == con.womstoid && x.formaction == "QL_trnconfirmdtl");
                        if (list_stock != null)
                        {
                            sSql = $"UPDATE QL_conmat SET valueidr={value_upd} WHERE refoid={item_id} AND refno='{list_stock.refno}'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                    }
                }
                //end
            }
            catch (Exception e)
            {
                error = e.ToString();
            }
            return error;
        }

        // POST: POService/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnusagemst tbl, List<usagedtl> dtl1, List<usagedtl2> dtl2, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            var msg = ""; var result = "failed"; var hdrid = "0";
            decimal totalberatspk = 0;
            decimal totalvaluemat = 0;
            var servertime = ClassFunction.GetServerTime();
            if (string.IsNullOrEmpty(tbl.cmpcode))
                msg += "Please select BUSINESS UNIT!<br />";
            if (string.IsNullOrEmpty(tbl.usageno))
                tbl.usageno = "";
            if (string.IsNullOrEmpty(tbl.usagemstnote))
                tbl.usagemstnote = "";
            if (string.IsNullOrEmpty(tbl.usagemstres1))
                tbl.usagemstres1 = "";
            if (string.IsNullOrEmpty(tbl.usagemstres2))
                tbl.usagemstres2 = "";
            if (string.IsNullOrEmpty(tbl.usagemstres3))
                tbl.usagemstres3 = "";
       
            if (tbl.usagewhoid == 0)
                msg += "Please select WAREHOUSE field!<br />";
            
            if (dtl1 == null)
                msg += "Please fill detail SPK!<br />";
            else if (dtl1.Count <= 0)
                msg += "Please fill detail SPK!<br />";
            else
            {
                if (dtl1.Count > 0)
                {
                    for (int i = 0; i < dtl1.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl1[i].usagedtlnote))
                        {
                            dtl1[i].usagedtlnote = "";
                        }
                        dtl1[i].totalberat = dtl1[i].berat * dtl1[i].confirmqty;
                        totalberatspk += dtl1[i].totalberat;
                    }
                }
            }

            if (dtl2 == null)
                msg += "Please fill detail Material data!<br />";
            else if (dtl2.Count <= 0)
                msg += "Please fill detail Material data!<br />";
            else
            {
                if (dtl2.Count > 0)
                {
                    for (int i = 0; i < dtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl2[i].usagedtl2note))
                        {
                            dtl2[i].usagedtl2note = "";
                        }
                        if(dtl2[i].usagedtl2qty <= 0)
                        {
                            msg += "Usage Material Must be more than 0 !<BR>";
                        }
                        else
                        {
                            if (!ClassFunction.IsStockAvailableWithSN(tbl.cmpcode, servertime, dtl2[i].matoid, tbl.usagewhoid, dtl2[i].usagedtl2qty, dtl2[i].mattype, dtl2[i].refno ?? "", dtl2[i].sn ?? ""))
                            {
                                msg += "- Usage Material QTY field Code " + dtl2[i].matcode + " must be less than STOCK QTY!<BR>";
                            }
                        }
                        dtl2[i].usagevalueidr = ClassFunction.GetStockValueWithSN(CompnyCode, dtl2[i].mattype, dtl2[i].matoid, dtl2[i].refno ?? "", dtl2[i].sn ?? "");
                        dtl2[i].totalusagevalueidr = dtl2[i].usagevalueidr * dtl2[i].usagedtl2qty;
                        totalvaluemat += dtl2[i].totalusagevalueidr;
                        dtl2[i].refno2 = generateBatchNo(dtl2[i].refno);
                    }
                }
            }
            tbl.usagefgvalueidr = Math.Round((totalvaluemat / totalberatspk),2);
            tbl.usagefgvalueusd = 0;
            tbl.mattype = "";

            int iAcctgoidRM = 0; int iAcctgoidFG = 0;  //int iAcctgoidST = 0; int iAcctgoidSP = 0; 
            decimal dAmtRM = 0; decimal dAmtSTout = 0;
            decimal dAmtFG = 0; decimal dAmtSTin = 0;
            tbl.usageno = "";
            if (tbl.usagemststatus == "Post")
            {
                tbl.usageno = GenerateNo(CompnyCode);
                //batchno2 = "";

                var divisi = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupoid).FirstOrDefault();
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                //if (!ClassFunction.IsInterfaceExists("VAR_STOCK_SHEET_" + divisi, tbl.cmpcode))
                //{
                //    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_SHEET_" + divisi) + "<BR>";
                //}
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_FG") + "<BR>";
                }

                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value));
                //iAcctgoidST = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value));
                iAcctgoidFG = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid.Value));
                //iAcctgoidSP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_SP", tbl.cmpcode, tbl.divgroupoid.Value));

              
                var plus = dtl1.GroupBy(x => x.wotype).Select(x => new { Nama = x.Key, Amt = x.Sum(y => (y.totalberat * tbl.usagefgvalueidr.Value)), AmtUSD = 0 }).ToList();
                for (int i = 0; i < plus.Count(); i++)
                {
                    if (plus[i].Nama.ToUpper() == "FINISH GOOD")
                    {
                        dAmtFG = plus[i].Amt; 
                    }
                    else if (plus[i].Nama.ToUpper() == "SHEET")
                    {
                        dAmtSTin = plus[i].Amt; 
                    }
                }

                var min = dtl2.GroupBy(x => x.mattype).Select(x => new { Nama = x.Key, Amt = x.Sum(y => (y.usagedtl2qty * y.usagevalueidr)), AmtUSD = 0 }).ToList();
                for (int i = 0; i < min.Count(); i++)
                {
                    if (min[i].Nama.ToUpper() == "RAW MATERIAL")
                    {
                        dAmtRM = min[i].Amt;
                    }
                    else if (min[i].Nama.ToUpper() == "SHEET")
                    {
                        dAmtSTout = min[i].Amt;
                    }
                }
            }

            if (msg == "")
            {
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
                var iStockValOid = ClassFunction.GenerateID("QL_stockvalue");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                int mstoid = ClassFunction.GenerateID("QL_trnusagemst");
                int dtloid = ClassFunction.GenerateID("QL_trnusagedtl");
                int iConOid = ClassFunction.GenerateID("QL_CONMAT");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.usagedate);
                            tbl.usagemstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnusagemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.usagemstoid + " WHERE tablename='QL_trnusagemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            for (int i = 0; i < dtl1.Count(); i++)
                            {
                                if (dtl1[i].wotype == "SHEET")
                                {
                                    sSql = "UPDATE QL_trnsorawdtl SET sorawdtlres2='' WHERE sorawdtloid=" + dtl1[i].sodtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlres2='' WHERE soitemdtloid=" + dtl1[i].sodtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                            var trndtl = db.QL_trnusagedtl.Where(a => a.usagemstoid == tbl.usagemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnusagedtl.RemoveRange(trndtl);
                            var trndtl2 = db.QL_trnusagedtl2.Where(a => a.usagemstoid == tbl.usagemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnusagedtl2.RemoveRange(trndtl2);
                        }

                        QL_trnusagedtl tbldtl;

                        for (int i = 0; i < dtl1.Count(); i++)
                        {
                            tbldtl = new QL_trnusagedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.usagedtloid = dtloid++;
                            tbldtl.usagemstoid = tbl.usagemstoid;
                            tbldtl.usagedtlseq = i + 1;
                            tbldtl.wotype = dtl1[i].wotype;
                            tbldtl.womstoid = dtl1[i].womstoid;
                            tbldtl.wono = dtl1[i].wono;
                            tbldtl.wodate = dtl1[i].wodate;
                            tbldtl.confirmmstoid = dtl1[i].confirmmstoid;
                            tbldtl.confirmno = dtl1[i].confirmno;
                            tbldtl.confirmdtloid = dtl1[i].confirmdtloid;
                            tbldtl.usageqty = dtl1[i].confirmqty;
                            tbldtl.confirmqty = dtl1[i].confirmqty;
                            tbldtl.usagevalueidr = tbl.usagefgvalueidr.Value * dtl1[i].berat;
                            tbldtl.usagevalueusd = 0;
                            tbldtl.usagedtlnote = dtl1[i].usagedtlnote;
                            tbldtl.usagedtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.berat = dtl1[i].berat;
                            tbldtl.totalberat = dtl1[i].totalberat;
                            tbldtl.sodtloid = dtl1[i].sodtloid;
                            db.QL_trnusagedtl.Add(tbldtl);
                            db.SaveChanges();
                            if (tbldtl.wotype == "SHEET")
                            {
                                sSql = "UPDATE QL_trnsorawdtl SET sorawdtlres2='DONE' WHERE sorawdtloid="+tbldtl.sodtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlres2='DONE' WHERE soitemdtloid=" + tbldtl.sodtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                         
                            if (tbl.usagemststatus == "Post")
                            {
                                // Update/Insert QL_stockvalue
                                int itemoid = db.Database.SqlQuery<int>("select itemoid from ql_trnwomst where womstoid=" + tbldtl.womstoid).FirstOrDefault();
                                sSql = ClassFunction.GetQueryUpdateStockValue(dtl1[i].confirmqty, tbl.usagefgvalueidr.Value, tbl.usagefgvalueusd.Value, "QL_trnusagemst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, itemoid, dtl1[i].wotype);
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(dtl1[i].confirmqty, tbl.usagefgvalueidr.Value, tbl.usagefgvalueusd.Value, "QL_trnusagemst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, itemoid, dtl1[i].wotype, iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                        }

                        QL_trnusagedtl2 tbldtl2;
                        for (int i = 0; i < dtl2.Count(); i++)
                        {
                            tbldtl2 = new QL_trnusagedtl2();
                            tbldtl2.cmpcode = tbl.cmpcode;
                            tbldtl2.divgroupoid = tbl.divgroupoid??0;
                            tbldtl2.usagedtl2oid = dtloid++;
                            tbldtl2.usagemstoid = tbl.usagemstoid;
                            tbldtl2.mattype = dtl2[i].mattype;
                            tbldtl2.matoid = dtl2[i].matoid;
                            tbldtl2.matcode = dtl2[i].matcode ?? "";
                            tbldtl2.matlongdesc = dtl2[i].matlongdesc;
                            tbldtl2.refno = dtl2[i].refno;
                            tbldtl2.refno1 = dtl2[i].refno2;
                            tbldtl2.sn = dtl2[i].sn??"";
                            tbldtl2.usagedtl2qty = dtl2[i].usagedtl2qty;
                            tbldtl2.usagedtl2unitoid = dtl2[i].usagedtl2unitoid;
                            tbldtl2.usagedtl2unit = dtl2[i].usagedtl2unit;
                            tbldtl2.usagedtl2note = dtl2[i].usagedtl2note;
                            tbldtl2.usagedtl2status = "";
                            tbldtl2.upduser = tbl.upduser;
                            tbldtl2.updtime = tbl.updtime;
                            tbldtl2.usagevalueidr = dtl2[i].usagevalueidr;
                            tbldtl2.konnversiberat = dtl2[i].konnversiberat;
                            tbldtl2.totalusagevalueidr = dtl2[i].totalusagevalueidr;
                            tbldtl2.usagevalueusd = 0;
                            tbldtl2.totalusagevalueusd = 0;

                            db.QL_trnusagedtl2.Add(tbldtl2);
                            db.SaveChanges();

                            if (tbl.usagemststatus == "Post")
                            {
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "USRM", "QL_trnusagemst", tbl.usagemstoid, dtl2[i].matoid, dtl2[i].mattype, tbl.usagewhoid, tbldtl2.usagedtl2qty * -1, "Usage RAW MATERIAL", tbl.usageno, tbl.upduser, tbldtl2.refno, tbldtl2.refno1, tbldtl2.usagevalueidr, tbldtl2.usagevalueusd, 0, null, tbldtl2.usagedtl2oid, tbl.divgroupoid ?? 0,tbldtl2.sn));
                                db.SaveChanges();

                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(dtl2[i].usagedtl2qty * -1, dtl2[i].usagevalueidr, dtl2[i].usagevalueusd, "QL_trnusagemst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].matoid, dtl2[i].mattype);
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(dtl2[i].usagedtl2qty * -1, dtl2[i].usagevalueidr, dtl2[i].usagevalueusd, "QL_trnusagemst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].matoid, dtl2[i].mattype, iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }

                            }
                        }

                        if (tbl.usagemststatus == "Post")
                        {
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, servertime, sPeriod, "Mat. Usage Non PP|No. " + tbl.usageno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;

                            // Persediaan Ke;luar
                            if (dAmtRM != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "C", dAmtRM, tbl.usageno, "Usage PP|No. " + tbl.usageno+"|RAW", "Post", tbl.upduser, servertime, dAmtRM, 0, "QL_trnusagemst " + tbl.usagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (dAmtSTout != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "C", dAmtSTout, tbl.usageno, "Usage PP|No. " + tbl.usageno+"|SHEET", "Post", tbl.upduser, servertime, dAmtSTout, 0, "QL_trnusagemst " + tbl.usagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            // Persediaan masuk
                            if (dAmtFG != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidFG, "D", dAmtFG, tbl.usageno, "Usage PP|No. " + tbl.usageno + "|FG", "Post", tbl.upduser, servertime, dAmtRM, 0, "QL_trnusagemst " + tbl.usagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (dAmtSTin != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "D", dAmtSTin, tbl.usageno, "Usage PP SHEET|No. " + tbl.usageno+"|SHEET", "Post", tbl.upduser, servertime, dAmtSTin, 0, "QL_trnusagemst " + tbl.usagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnusagedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iConOid - 1) + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_stockvalue'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //update value next transaksi
                        if (tbl.usagemststatus == "Post") { msg = UpdateValueFG(ref db, tbl, dtl1); }
                        if (!string.IsNullOrEmpty(msg)) throw new Exception(msg);

                        objTrans.Commit();
                        hdrid = tbl.usagemstoid.ToString();
                        msg = "Data saved using Draft No. " + hdrid + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: POService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnusagemst tbl = db.QL_trnusagemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        var trndtl = db.QL_trnusagedtl.Where(a => a.usagemstoid == id && a.cmpcode == cmp);
                        db.QL_trnusagedtl.RemoveRange(trndtl);
                        var trndtl2 = db.QL_trnusagedtl2.Where(a => a.usagemstoid == id && a.cmpcode == cmp);
                        db.QL_trnusagedtl2.RemoveRange(trndtl2);

                        db.QL_trnusagemst.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}