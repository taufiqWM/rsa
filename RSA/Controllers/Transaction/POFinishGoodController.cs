﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class POFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpoitemmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int poitemmstoid { get; set; }
            public string poitemtype { get; set; }
            public int poitempaytypeoid { get; set; }
            public string poitemno { get; set; }
            public DateTime poitemdate { get; set; }
            public string suppname { get; set; }
            public string poitemmststatus { get; set; }
            public string poitemmstnote { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnpoitemdtl
        {
            public int poitemdtlseq { get; set; }
            public int pritemmstoid { get; set; }
            public int pritemdtloid { get; set; }
            public string pritemno { get; set; }
            public string pritemdatestr { get; set; }
            public DateTime pritemdate { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal itemlimitqty { get; set; }
            public decimal pritemqty { get; set; }
            public decimal poitemqty { get; set; }
            public int poitemunitoid { get; set; }
            public string poitemunit { get; set; }
            public decimal poitemprice { get; set; }
            public decimal poitemdtlamt { get; set; }
            public string poitemdtldisctype { get; set; }
            public decimal poitemdtldiscvalue { get; set; }
            public decimal poitemdtldiscamt { get; set; }
            public decimal poitemdtlnetto { get; set; }
            public string poitemdtlnote { get; set; }
            public string poitemdtlnote2 { get; set; }
            public string pritemarrdatereqstr { get; set; }
            public DateTime pritemarrdatereq { get; set; }
            public decimal lastpoprice { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnpoitemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            ViewBag.approvalcode = approvalcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var poitempaytype = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.poitempaytypeoid);
            ViewBag.poitempaytype = poitempaytype;

            //sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'";
            //var poitemunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.poitemunitoid);
            //ViewBag.poitemunitoid = poitemunitoid;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prmstoid, int suppoid,int pomstoid)
        {
            List<trnpoitemdtl> tbl = new List<trnpoitemdtl>();

            sSql = "SELECT 0 AS poitemdtlseq, prd.pritemmstoid, prd.pritemdtloid, prm.pritemno, prd.itemoid, ISNULL((prd.pritemqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(pod.poitemqty - (CASE WHEN pod.poitemdtlres1 IS NULL THEN ISNULL(pod.closeqty, 0) WHEN pod.poitemdtlres1='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.poitemqty - CONVERT(decimal(18,2), pod.poitemdtlres1)) END)), 0) FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=pod.cmpcode AND pom.poitemmstoid=pod.poitemmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.pritemdtloid=prd.pritemdtloid AND pom.poitemmststatus NOT IN ('Cancel', 'Rejected') AND pod.poitemmstoid<>" + pomstoid + ")) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='item' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pritemdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0)), 0) AS pritemqty, prd.pritemunitoid AS poitemunitoid, m.itemcode, m.itemlongdesc, m.itemlimitqty, g2.gendesc AS poitemunit, 0.0 AS poitemqty, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstitemprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.itemoid=prd.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstitemprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.itemoid=prd.itemoid ORDER BY crd.updtime DESC), 0.0)) AS poitemprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.itemoid=prd.itemoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.itemoid=prd.itemoid ORDER BY crd.updtime DESC), 1)) AS lastcurroid, prd.pritemdtlnote AS poitemdtlnote, 0.0 AS lastpoprice, CONVERT(VARCHAR(10), prm.pritemdate, 101) AS pritemdatestr, prm.pritemdate, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereqstr, prd.pritemarrdatereq, 0.0 AS poitemdtlamt, 'A' AS poitemdtldisctype, 0.0 AS poitemdtldiscvalue, 0.0 AS poitemdtldiscamt, 0.0 AS poitemdtlnetto, '' AS poitemdtlnote2 FROM QL_pritemdtl prd INNER JOIN QL_pritemmst prm ON prm.cmpcode=prd.cmpcode AND prm.pritemmstoid=prd.pritemmstoid INNER JOIN QL_mstitem m ON prd.itemoid=m.itemoid INNER JOIN QL_mstgen g2 ON prd.pritemunitoid=g2.genoid WHERE prd.cmpcode='" + CompnyCode + "' AND prd.pritemmstoid=" + prmstoid + " AND prd.pritemdtlstatus='' AND prm.pritemmststatus='Approved'";

            tbl = db.Database.SqlQuery<trnpoitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpoitemdtl> dtDtl)
        {
            Session["QL_trnpoitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpoitemdtl"] == null)
            {
                Session["QL_trnpoitemdtl"] = new List<trnpoitemdtl>();
            }

            List<trnpoitemdtl> dataDtl = (List<trnpoitemdtl>)Session["QL_trnpoitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpoitemmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.potype = tbl.poitemtype;
            ViewBag.potaxtype = tbl.poitemtaxtype;
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            //public string supptaxable { get; set; }
            //public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

            [HttpPost]
        public ActionResult GetSuppData(int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, /*(CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 10 ELSE 0 END) supptaxamt, c.currcode, suppcurroid,*/ gp.gendesc supppayment  FROM QL_mstsupp s /*INNER JOIN QL_mstcurr c ON suppcurroid=c.curroid*/ INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' and s.divgroupoid=" +divgroupoid+ " AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pritemmst
        {
            public int pritemmstoid { get; set; }
            public string pritemno { get; set; }
            public string pritemdate { get; set; }
            public string pritemexpdate { get; set; }
            public string deptname { get; set; }
            public string pritemmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp, int divgroupoid)
        {
            List<pritemmst> tbl = new List<pritemmst>();
            sSql = " SELECT DISTINCT prm.pritemmstoid, pritemno, CONVERT(VARCHAR(10), pritemdate, 101) AS pritemdate, CONVERT(VARCHAR(10), pritemexpdate, 101) AS pritemexpdate, deptname, pritemmstnote FROM QL_pritemmst prm INNER JOIN QL_pritemdtl prd ON prm.cmpcode=prd.cmpcode AND prm.pritemmstoid=prd.pritemmstoid INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" + CompnyCode + "' AND prm.divgroupoid='"+divgroupoid+"' AND prm.pritemmststatus='Approved' ORDER BY pritemdate DESC, prm.pritemmstoid DESC ";

            tbl = db.Database.SqlQuery<pritemmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: PORawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, mdFilter modfil) //string filter
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "pom", divgroupoid, "divgroupoid");
            sSql = "SELECT pom.cmpcode, (select gendesc from ql_mstgen where genoid=pom.divgroupoid) divgroup, poitemmstoid, poitemno, poitemdate, suppname, poitemmststatus, poitemmstnote, divname FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.cmpcode='" + CompnyCode + "' AND s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";

            sSql += sqlplus;

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND poitemdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poitemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND poitemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND poitemdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND poitemdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND poitemmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiod1 != null & modfil.filterperiod2 != null)
                {
                    sSql += " AND poitemdate>=CAST('" + modfil.filterperiod1 + " 00:00:00' AS DATETIME) AND poitemdate<=CAST('" + modfil.filterperiod2 + " 23:59:59' AS DATETIME)"; //ToString("MM/dd/yyyy")
                    if (modfil.ddlstatus != "ALL")
                        sSql += " AND poitemmststatus='" + modfil.ddlstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.ddlstatus == "ALL" | modfil.ddlstatus == "Post" | modfil.ddlstatus == "Approved" | modfil.ddlstatus == "Closed" | modfil.ddlstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND poitemmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpoitemmst> dt = db.Database.SqlQuery<trnpoitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: PORawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnpoitemmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnpoitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.poitemmstoid = ClassFunction.GenerateID("QL_trnpoitemmst");
                tbl.poitemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.poitemmststatus = "In Process";

                Session["QL_trnpoitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpoitemmst.Find(cmp, id);

                sSql = "SELECT pod.poitemdtlseq, prd.pritemmstoid, prm.pritemno, CONVERT(VARCHAR(10), prm.pritemdate, 101) AS pritemdatestr, prm.pritemdate, pod.pritemdtloid, pod.itemoid, m.itemcode, m.itemlongdesc, m.itemlimitqty, pod.poitemqty, ISNULL((prd.pritemqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.poitemqty - (CASE WHEN podtl.poitemdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.poitemdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.poitemqty - CONVERT(decimal(18,2), podtl.poitemdtlres1)) END)), 0) FROM QL_trnpoitemdtl podtl INNER JOIN QL_trnpoitemmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.poitemmstoid=podtl.poitemmstoid WHERE podtl.pritemdtloid=prd.pritemdtloid AND podtl.poitemmstoid<>" + id + " AND pomst.poitemmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='item' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pritemdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0) AS pritemqty,  0.0 AS poqtyuse,  pod.poitemunitoid, g.gendesc AS poitemunit, pod.poitemprice, pod.poitemdtlamt, pod.poitemdtldisctype, pod.poitemdtldiscvalue, pod.poitemdtldiscamt, pod.poitemdtlnetto, pod.poitemdtlnote, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstitemprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.itemoid=pod.itemoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstitemprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.itemoid=pod.itemoid ORDER BY crd.updtime), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.itemoid=pod.itemoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.itemoid=pod.itemoid ORDER BY crd.updtime), 1)) AS lastcurroid, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereqstr, prd.pritemarrdatereq FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=pod.cmpcode AND pom.poitemmstoid=pod.poitemmstoid INNER JOIN QL_mstitem m ON pod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON pod.poitemunitoid=g.genoid INNER JOIN QL_pritemdtl prd ON prd.cmpcode=pod.cmpcode AND prd.pritemdtloid=pod.pritemdtloid INNER JOIN QL_pritemmst prm ON prm.cmpcode=pod.cmpcode AND prm.pritemmstoid=pod.pritemmstoid WHERE pod.poitemmstoid=" + id + " AND pod.cmpcode='" + cmp + "' ORDER BY pod.poitemdtlseq";
                Session["QL_trnpoitemdtl"] = db.Database.SqlQuery<trnpoitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpoitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.poitemno == null)
                tbl.poitemno = "";

            List<trnpoitemdtl> dtDtl = (List<trnpoitemdtl>)Session["QL_trnpoitemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].poitemqty <= 0)
                        {
                            ModelState.AddModelError("", "PO QTY for Material " + dtDtl[i].itemlongdesc + " must be more than 0");
                        }

                        if (dtDtl[i].poitemprice <= 0)
                        {
                            ModelState.AddModelError("", "PO Price for Material " + dtDtl[i].itemlongdesc + " must be more than 0");
                        }

                        sSql = "SELECT ISNULL((prd.pritemqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.poitemqty - (CASE WHEN podtl.poitemdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.poitemdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.poitemqty - CONVERT(decimal(18,2), podtl.poitemdtlres1)) END)), 0) FROM QL_trnpoitemdtl podtl INNER JOIN QL_trnpoitemmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.poitemmstoid=podtl.poitemmstoid WHERE podtl.cmpcode=prd.cmpcode AND podtl.pritemmstoid=prd.pritemmstoid AND  podtl.pritemdtloid=prd.pritemdtloid AND podtl.poitemmstoid<>" + tbl.poitemmstoid + " AND pomst.poitemmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='item' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pritemdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0.0) AS pritemqty FROM QL_pritemdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.pritemdtloid=" + dtDtl[i].pritemdtloid + "";
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].pritemqty)
                            dtDtl[i].pritemqty = dQty;
                        if (dQty < dtDtl[i].poitemqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PR Qty!");

                        //sSql = "SELECT COUNT(-1) FROM QL_prrawdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.pritemmstoid=" + dtDtl[i].pritemmstoid + " AND prd.prrawdtloid=" + dtDtl[i].prrawdtloid + " AND prd.prrawdtlstatus='COMPLETE'";
                        //var CheckDataExists = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (CheckDataExists > 0 )
                        //{
                        //    ModelState.AddModelError("", "There are some PR detail data has been closed by another user. You cannot use this PR detail data anymore!");
                        //}
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.poitemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpoitemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpoitemdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpoitemmst.Find(tbl.cmpcode, tbl.poitemmstoid) != null)
                                tbl.poitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.poitemdate);
                            //tbl.divoid = divoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.poitemratetoidrchar = "0";
                            tbl.poitemratetousdchar = "0";
                            tbl.poitemrate2toidrchar = "0";
                            tbl.poitemrate2tousdchar = "0";
                            //tbl.poitemtaxtype_inex = "0";
                            //tbl.poitemmststatus_onpib = "";
                            db.QL_trnpoitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.poitemmstoid + " WHERE tablename='QL_trnpoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.poitemratetoidrchar = "0";
                            tbl.poitemratetousdchar = "0";
                            tbl.poitemrate2toidrchar = "0";
                            tbl.poitemrate2tousdchar = "0";
                            //tbl.poitemtaxtype_inex = "0";
                            //tbl.poitemmststatus_onpib = "";
                            //--
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_pritemdtl SET pritemdtlstatus='', pritemdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND pritemdtloid IN (SELECT pritemdtloid FROM QL_trnpoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_pritemmst SET pritemmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND pritemmstoid IN (SELECT DISTINCT pritemmstoid FROM QL_trnpoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpoitemdtl.Where(a => a.poitemmstoid == tbl.poitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpoitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpoitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.poitemdtloid = dtloid++;
                            tbldtl.poitemmstoid = tbl.poitemmstoid;
                            tbldtl.poitemdtlseq = i + 1;
                            tbldtl.pritemmstoid = dtDtl[i].pritemmstoid;
                            tbldtl.pritemdtloid = dtDtl[i].pritemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.poitemqty = dtDtl[i].poitemqty;
                            tbldtl.poitemunitoid = dtDtl[i].poitemunitoid;
                            tbldtl.poitemprice = dtDtl[i].poitemprice;
                            tbldtl.poitemdtlamt = dtDtl[i].poitemdtlamt;
                            tbldtl.poitemdtldisctype = dtDtl[i].poitemdtldisctype;
                            tbldtl.poitemdtldiscvalue = dtDtl[i].poitemdtldiscvalue;
                            tbldtl.poitemdtldiscamt = dtDtl[i].poitemdtldiscamt;
                            tbldtl.poitemdtlnetto = dtDtl[i].poitemdtlnetto;
                            tbldtl.poitemdtlnote = dtDtl[i].poitemdtlnote == null ? "" : dtDtl[i].poitemdtlnote;
                            //tbldtl.poitemdtlnote2 = dtDtl[i].poitemdtlnote2;
                            tbldtl.poitemdtlstatus = "";
                            //tbldtl.porawdtlstatus_onpib = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnpoitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].poitemqty >= dtDtl[i].pritemqty)
                            {
                                sSql = "UPDATE QL_pritemdtl SET pritemdtlstatus='COMPLETE', pritemdtlres1='" + dtloid + "' WHERE cmpcode='" + tbl.cmpcode + "' AND pritemdtloid=" + dtDtl[i].pritemdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_pritemmst SET pritemmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND pritemmstoid=" + dtDtl[i].pritemmstoid + " AND (SELECT COUNT(pritemdtloid) FROM QL_pritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pritemdtlstatus='' AND pritemmstoid=" + dtDtl[i].pritemmstoid + " AND pritemdtloid<>" + +dtDtl[i].pritemdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnpoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.poitemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-FG" + tbl.poitemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpoitemmst";
                                tblApp.oid = tbl.poitemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        //if (tbl.poitemmststatus.ToUpper() == "In Approval")
                        //{
                        //    var dt = null;
                        //    //Dim dt As DataTable = Session("AppPerson")
                        //    for (int i = 0; i < dtLastHdrData.Count(); i++)
                        //    {
                        //        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'PO-RAW" & poitemmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & poitemdate.Text & "', 'New', 'QL_trnpoitemmst', " & poitemmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();
                        //    }
                        //    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.poitemmstoid );
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.poitemmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoitemmst tbl = db.QL_trnpoitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_pritemdtl SET pritemdtlstatus='', pritemdtlres1='', prclosingqty=0.0 WHERE cmpcode='" + tbl.cmpcode + "' AND pritemdtloid IN (SELECT pritemdtloid FROM QL_trnpoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_pritemmst SET pritemmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND pritemmstoid IN (SELECT DISTINCT pritemmstoid FROM QL_trnpoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poitemmstoid=" + tbl.poitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = GetQueryInsertStatus_Multi(Session("UserID"), DDLBusUnit.SelectedValue, "QL_trnpoitemdtl", poitemmstoid.Text, "Open", "poitemmstoid", "QL_pritemmst", "pritemmstoid", "Open karena PO dihapus");
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnpoitemdtl.Where(a => a.poitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpoitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string supptype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpoitemmst.Find(cmp, id);
            if (tbl == null)
                return null;

            if (supptype == "LOCAL")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_TrnEn.rpt"));

            //sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.poitemmstoid [Oid], CONVERT(VARCHAR(10), pom.poitemmstoid) AS [Draft No.], pom.poitemno [PO No.], pom.poitemdate [PO Date], pom.poitemmstnote [Header Note], pom.poitemmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.porawmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.porawmstdiscamt, 0) AS [Disc Amount], pom.porawtotalnetto [Total Netto], pom.porawtaxtype [Tax Type], CONVERT(VARCHAR(10), pom.porawtaxamt) AS [Tax Amount], ISNULL(pom.porawvat, 0) AS [VAT], ISNULL(pom.porawdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.porawothercost, 0) AS [Other Cost], pom.porawgrandtotalamt [Grand Total], pod.porawdtloid [DtlOid], ISNULL((SELECT m2.matrawdtl2code FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawcode) AS [Code], ISNULL((SELECT m2.matrawdtl2name FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawlongdesc) AS [Material], prm.prrawno AS [PR No.], prm.prrawdate AS [PR Date], pod.porawqty AS [Qty], g2.gendesc AS [Unit], pod.porawprice AS [Price], pod.porawdtlamt AS [Amount], ISNULL(pod.porawdtldiscamt, 0) AS [Detail Disc.], pod.porawdtlnetto AS [Netto], pod.porawdtlnote AS [Note], pom.porawtype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.porawsuppref AS [SuppReff], pod.porawdtldisctype AS [DtlDiscType], ISNULL(pod.porawdtldiscvalue,0) AS [DtlDiscValue], pod.porawdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prrawarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpoitemmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.porawpaytypeoid INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode=pom.cmpcode AND pod.poitemmstoid=pom.poitemmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid LEFT JOIN QL_pritemmst prm ON prm.cmpcode=pod.cmpcode AND prm.pritemmstoid=pod.pritemmstoid LEFT JOIN QL_prrawdtl prd ON prd.cmpcode=pod.cmpcode AND prd.pritemmstoid=pod.pritemmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.porawunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.poitemmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.poitemmstoid, pod.porawdtlseq ";
            sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.poitemmstoid [Oid], CONVERT(VARCHAR(10), pom.poitemmstoid) AS [Draft No.], pom.poitemno [PO No.], pom.poitemdate [PO Date], pom.poitemmstnote [Header Note], pom.poitemmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.poitemmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.poitemmstdiscamt, 0) AS [Disc Amount], pom.poitemtotalnetto [Total Netto], pom.poitemtaxtype [Tax Type], CONVERT(VARCHAR(10), pom.poitemtaxamt) AS [Tax Amount], ISNULL(pom.poitemvat, 0) AS [VAT], ISNULL(pom.poitemdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.poitemothercost, 0) AS [Other Cost], pom.poitemgrandtotalamt [Grand Total], pod.poitemdtloid [DtlOid], '' AS [Code], '' AS [Material], prm.pritemno AS [PR No.], prm.pritemdate AS [PR Date], pod.poitemqty AS [Qty], g2.gendesc AS [Unit], pod.poitemprice AS [Price], pod.poitemdtlamt AS [Amount], ISNULL(pod.poitemdtldiscamt, 0) AS [Detail Disc.], pod.poitemdtlnetto AS [Netto], pod.poitemdtlnote AS [Note], pom.poitemtype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.poitemsuppref AS [SuppReff], pod.poitemdtldisctype AS [DtlDiscType], ISNULL(pod.poitemdtldiscvalue,0) AS [DtlDiscValue], pod.poitemdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.pritemarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpoitemmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.poitempaytypeoid INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode=pom.cmpcode AND pod.poitemmstoid=pom.poitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=pod.itemoid LEFT JOIN QL_pritemmst prm ON prm.cmpcode=pod.cmpcode AND prm.pritemmstoid=pod.pritemmstoid LEFT JOIN QL_pritemdtl prd ON prd.cmpcode=pod.cmpcode AND prd.pritemmstoid=pod.pritemmstoid AND prd.pritemdtloid=pod.pritemdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.poitemunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.poitemmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.poitemmstoid, pod.poitemdtlseq ";


            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPORaw");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "POItemMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
