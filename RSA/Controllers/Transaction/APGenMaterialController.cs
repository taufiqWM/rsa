﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class APGenMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class apgenmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int apgenmstoid { get; set; }
            public string apgenno { get; set; }
            public DateTime apgendate { get; set; }
            public DateTime apgendatetakegiro { get; set; }
            public DateTime duedate { get; set; }
            public string suppname { get; set; }
            public string apgenmststatus { get; set; }
            public string apgenmstnote { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apgengrandtotal { get; set; }

            [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
            public decimal apgengrandtotalsupp { get; set; }
            public string divname { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
        }

        public class apgendtl
        {
            public int apgendtlseq { get; set; }
            public int pogenmstoid { get; set; }
            public string pogenno { get; set; }
            public int pogendtloid { get; set; }
            public int mrgenmstoid { get; set; }
            public string mrgenno { get; set; }
            public int mrgendtloid { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal apgenqty { get; set; }
            public int apgenunitoid { get; set; }
            public string apgenunit { get; set; }
            public decimal apgenprice { get; set; }
            public decimal apgendtlamt { get; set; }
            public string apgendtldisctype { get; set; }
            public decimal apgendtldiscvalue { get; set; }
            public decimal apgendtldiscamt { get; set; }
            public decimal apgendtlnetto { get; set; }
            public decimal apgendtltaxamt { get; set; }
            public string apgendtlnote { get; set; }
            public string apgendtlres1 { get; set; }
            public string apgendtlres2 { get; set; }
        }

        private void InitDDL(QL_trnapgenmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var apgenpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.apgenpaytypeoid);
            ViewBag.apgenpaytypeoid = apgenpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapgenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapgenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp, int apgenmstoid, int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            //sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid FROM QL_mstsupp s INNER JOIN QL_trnregistermst regm ON regm.suppoid=s.suppoid INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode=regm.cmpcode AND mrm.registermstoid=regm.registermstoid WHERE regm.cmpcode='" + cmp + "' AND registertype='gen' AND registermststatus='Closed' AND ISNULL(mrgenmstres1, '')='' AND mrm.mrgenmststatus='Post' AND regm.registermstoid NOT IN (SELECT DISTINCT registermstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode=apd.cmpcode AND apm.apgenmstoid=apd.apgenmstoid WHERE apd.cmpcode='" + cmp + "' AND apgenmststatus<>'Rejected' AND apm.apgenmstoid <> " + apgenmstoid + ") ORDER BY suppcode ";
            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid " +
                "FROM QL_mstsupp s " +
                "INNER JOIN QL_trnpogenmst regm ON regm.suppoid = s.suppoid " +
                "INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.pogenmstoid = regm.pogenmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND pogenmststatus IN('Approved', 'Closed') and s.divgroupoid="+ divgroupoid + " and mrm.divgroupoid="+ divgroupoid + " AND ISNULL(mrgenmstres1, '')= '' AND mrm.mrgenmststatus = 'Post' AND regm.pogenmstoid NOT IN(SELECT DISTINCT pogenmstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode = apd.cmpcode AND apm.apgenmstoid = apd.apgenmstoid WHERE apd.cmpcode = '" + cmp + "' AND apgenmststatus <> 'Rejected' AND apm.apgenmstoid <> " + apgenmstoid + ") ORDER BY suppcode";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int pogenmstoid { get; set; }
            public string pogenno { get; set; }
            public DateTime pogendate { get; set; }
            public string pogendocrefno { get; set; }
            public string pogenmstnote { get; set; }
            public string poflag { get; set; }
            public int mrgenmstoid { get; set; }
            public string mrgenno { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid, int apgenmstoid, int curroid,int divgroupoid)
        {
            cmp = CompnyCode;
            List<pomst> tbl = new List<pomst>();
            sSql = "SELECT DISTINCT regm.pogenmstoid, regm.pogenno, regm.pogendate, '' [pogendocrefno], ISNULL(regm.pogenmstnote, '') [pogenmstnote], mrm.mrgenmstoid, mrm.mrgenno " +
                "FROM QL_trnpogenmst regm " +
                "INNER JOIN QL_trnpogendtl regd ON regd.cmpcode = regm.cmpcode AND regd.pogenmstoid = regm.pogenmstoid " +
                "INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode = regm.cmpcode AND mrm.pogenmstoid = regm.pogenmstoid " +
                "WHERE regm.cmpcode = '" + cmp + "' AND regm.suppoid = " + suppoid + " AND regm.pogenmststatus IN ('Approved', 'Closed') AND regm.pogenmstoid NOT IN(SELECT pogenmstoid FROM QL_trnapgendtl apd INNER JOIN QL_trnapgenmst apm ON apm.cmpcode = apd.cmpcode AND apm.apgenmstoid = apd.apgenmstoid WHERE apd.cmpcode = '" + cmp + "' AND apgenmststatus <> 'Rejected' AND apm.apgenmstoid <> " + apgenmstoid + ") AND ISNULL(mrgenmstres1, '') = '' AND mrm.mrgenmststatus = 'Post' AND regm.pogenmstoid NOT IN(SELECT pretm.pogenmstoid FROM QL_trnpretgenmst pretm WHERE pretm.cmpcode = '" + cmp + "' AND pretgenmststatus IN ('In Process', 'Revised', 'In Approval')) AND regm.curroid = " + curroid + " and regm.divgroupoid=" + divgroupoid+" ORDER BY regm.pogenmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int pogenmstoid, string apgendtlres1, string apgendtlres2)
        {

            List<apgendtl> tbl = new List<apgendtl>();
         
            sSql = "SELECT apgendtlseq, pogenmstoid, pogenno, pogendtloid, mrgenmstoid, mrgenno, mrgendtloid, matgenoid, matgencode, matgenlongdesc, apgenqty, apgenunitoid, apgenunit, apgenprice,(apgenqty * apgenprice) AS apgendtlamt, apgendtldisctype, apgendtldiscvalue, " +
                "(CASE apgendtldisctype WHEN 'P' THEN(apgenqty * apgenprice) * (apgendtldiscvalue / 100) ELSE(apgendtldiscvalue * apgenqty) END)apgendtldiscamt, " +
                "((apgenqty * apgenprice) - (CASE apgendtldisctype WHEN 'P' THEN(apgenqty * apgenprice) * (apgendtldiscvalue / 100) ELSE(apgendtldiscvalue * apgenqty) END)) AS apgendtlnetto, " +
                "(CASE pogentaxtype WHEN 'TAX' THEN(((apgenqty * apgenprice) - (CASE apgendtldisctype WHEN 'P' THEN(apgenqty * apgenprice) * (apgendtldiscvalue / 100) ELSE(apgendtldiscvalue * apgenqty) END)) *pogentaxamt) / 100 ELSE 0 END) apgendtltaxamt, " +
                "apgendtlnote ,apgendtlres1 ,apgendtlres2 " +
                "FROM (SELECT 0 AS apgendtlseq, regm.pogenmstoid, regm.pogenno, regd.pogendtloid, mrm.mrgenmstoid, mrgenno, mrgendtloid, mrd.matgenoid, matgencode, matgenlongdesc, (mrgenqty - ISNULL((SELECT SUM(pretgenqty) FROM QL_trnpretgendtl pretd INNER JOIN QL_trnpretgenmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretgenmstoid = pretd.pretgenmstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mrgendtloid = mrd.mrgendtloid AND pretgenmststatus <> 'Rejected'), 0.0)) AS apgenqty, mrgenunitoid AS apgenunitoid, gendesc AS apgenunit, pogenprice apgenprice, pogendtldisctype apgendtldisctype, (CASE pogendtldisctype WHEN 'A' THEN(pogendtldiscvalue / pogenqty) ELSE pogendtldiscvalue END) AS apgendtldiscvalue, pogentaxtype, pogentaxamt, '' apgendtlnote, '" + apgendtlres1 + "' AS apgendtlres1, '" + apgendtlres2 + "' AS apgendtlres2 FROM QL_trnpogendtl regd INNER JOIN QL_trnpogenmst regm ON regm.cmpcode = regd.cmpcode AND regm.pogenmstoid = regd.pogenmstoid INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode = regd.cmpcode  AND mrm.pogenmstoid = regd.pogenmstoid INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode = regd.cmpcode AND mrm.mrgenmstoid = mrd.mrgenmstoid  AND mrd.pogendtloid = regd.pogendtloid INNER JOIN QL_mstmatgen m ON m.matgenoid = mrd.matgenoid INNER JOIN QL_mstgen g ON genoid = matgenunitoid WHERE regd.cmpcode = '" + cmp + "' AND mrm.mrgenmstoid = " + pogenmstoid + " AND ISNULL(mrgendtlres1, '')= '' AND ISNULL(mrgenmstres1, '')= '' AND mrgenmststatus = 'Post' AND mrgendtlstatus = '') AS tbl";

            tbl = db.Database.SqlQuery<apgendtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<apgendtl> dtDtl)
        {
            Session["QL_trnapgendtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnapgendtl"] == null)
            {
                Session["QL_trnapgendtl"] = new List<apgendtl>();
            }

            List<apgendtl> dataDtl = (List<apgendtl>)Session["QL_trnapgendtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnapgenmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            //ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnapgenmst WHERE cmpcode='" + tbl.cmpcode + "' AND apgenmstoid=" + tbl.apgenmstoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: APGenMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "apm", divgroupoid, "divgroupoid");
            sSql = "SELECT apgenmstoid, (select gendesc from ql_mstgen where genoid=apm.divgroupoid) divgroup, apgenno, apgendate, suppname, apgenmststatus, apgenmstnote, divname, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'gen' AS formtype, 'False' AS checkvalue, apgengrandtotal , apgendatetakegiro,  CONVERT(DATETIME,DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 WHEN 'CBD' THEN 0 ELSE CAST(g1.gendesc AS INT) END),apgendate), 101) duedate FROM QL_trnapgenmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=apm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=apgenpaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "apm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "apm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND apgenmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND apgenmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND apgenmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND apgendate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND apgendate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND apgenmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND apm.createuser='" + Session["UserID"].ToString() + "'";
            sSql += "AND apgenmstoid>0 ORDER BY CONVERT(DATETIME, apgendate) DESC, apgenmstoid DESC ";

            List<apgenmst> dt = db.Database.SqlQuery<apgenmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnapgenmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        [HttpPost]
        public ActionResult generateNo(string cmp, int apmstoid)
        {
            sSql = "SELECT apgenno FROM QL_trnapgenmst WHERE cmpcode='" + cmp + "' AND apgenmstoid=" + apmstoid;
            string sNoAP = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (string.IsNullOrEmpty(sNoAP))
            {

                string sNo = "APRM-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apgenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapgenmst WHERE cmpcode='" + cmp + "' AND apgenno LIKE '" + sNo + "%'";

                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

                sNo = sNo + sCounter;
                return Json(sNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(sNoAP, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: APgenMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnapgenmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnapgenmst();
                tbl.cmpcode = CompnyCode;
                tbl.apgenmstoid = ClassFunction.GenerateID("QL_trnapgenmst");
                tbl.apgendate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.apgenmststatus = "In Process";

                Session["QL_trnapgendtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnapgenmst.Find(cmp, id);

                sSql = "SELECT apgendtlseq, apd.pogenmstoid, pogenno, pogendtloid, apd.mrgenmstoid, mrgenno, mrgendtloid, apd.matgenoid, matgencode, matgenlongdesc, apgenqty, apgenunitoid, gendesc AS apgenunit, apgenprice, apgendtlamt, apgendtldisctype, apgendtldiscvalue, apgendtldiscamt, apgendtlnetto, apgendtltaxamt, apgendtlnote, ISNULL(apgendtlres1, '') AS apgendtlres1, ISNULL(apgendtlres2, '') AS apgendtlres2 " +
                    "FROM QL_trnapgendtl apd " +
                    "INNER JOIN QL_trnpogenmst regm ON regm.cmpcode = apd.cmpcode AND regm.pogenmstoid = apd.pogenmstoid " +
                    "INNER JOIN QL_trnmrgenmst mrm ON mrm.cmpcode = apd.cmpcode AND mrm.mrgenmstoid = apd.mrgenmstoid AND mrm.pogenmstoid = regm.pogenmstoid " +
                    "INNER JOIN QL_mstmatgen m ON m.matgenoid = apd.matgenoid " +
                    "INNER JOIN QL_mstgen g ON genoid = apgenunitoid " +
                    "WHERE apgenmstoid = " + id + " AND apd.cmpcode = '" + cmp + "' ORDER BY apgendtlseq  ";
                Session["QL_trnapgendtl"] = db.Database.SqlQuery<apgendtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: APGenMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapgenmst tbl, string action, string closing)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.apgenno == null)
                tbl.apgenno = "";

            List<apgendtl> dtDtl = (List<apgendtl>)Session["QL_trnapgendtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnmrgenmst mrm WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + dtDtl[i].mrgenmstoid + " AND mrgenmststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND mrgenmstoid NOT IN (SELECT apd.mrgenmstoid FROM QL_trnapgendtl apd WHERE apd.cmpcode=mrm.cmpcode AND apgenmstoid=" + tbl.apgenmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "PO No. " + dtDtl[i].pogenno + " has been used by another data. Please cancel this transaction or use another PO No.!");
                    }
                }
            }

            var msg = "";

            cRate.SetRateValue(tbl.curroid, tbl.apgendate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.apgenmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                //AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapgenmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnapgenmst' AND activeflag='ACTIVE'").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.apgenmststatus == "Revised")
                tbl.apgenmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnapgenmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapgendtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.apgentotalamtidr = (tbl.apgentotalamt * decimal.Parse(tbl.apgenrate2toidr));
                tbl.apgentotalamtusd = (tbl.apgentotalamt * decimal.Parse(tbl.apgenrate2tousd));
                tbl.apgentotaldiscidr = (tbl.apgentotaldisc * decimal.Parse(tbl.apgenrate2toidr));
                tbl.apgentotaldiscusd = (tbl.apgentotaldisc * decimal.Parse(tbl.apgenrate2tousd));
                tbl.apgentotaltaxidr = (tbl.apgentotaltax * decimal.Parse(tbl.apgenrate2toidr));
                tbl.apgentotaltaxusd = (tbl.apgentotaltax * decimal.Parse(tbl.apgenrate2tousd));
                tbl.apgengrandtotalidr = (tbl.apgengrandtotal * decimal.Parse(tbl.apgenrate2toidr));
                tbl.apgengrandtotalusd = (tbl.apgengrandtotal * decimal.Parse(tbl.apgenrate2tousd));

                //tbl.cancelseq = 0;
                //tbl.apcloseuser = "";
                //tbl.apclosetime = DateTime.Parse("1/1/1900 00:00:00");
                if (tbl.apgenmstnote == null)
                    tbl.apgenmstnote = "";
                if (tbl.apgentotaldisc == null)
                    tbl.apgentotaldisc = 0;
                if (tbl.apgentotaldiscidr == null)
                    tbl.apgentotaldiscidr = 0;
                if (tbl.apgentotaldiscusd == null)
                    tbl.apgentotaldiscusd = 0;
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = DateTime.Parse("1/1/1900 00:00:00");
                tbl.rejecttime = DateTime.Parse("1/1/1900 00:00:00");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnapgenmst.Find(tbl.cmpcode, tbl.apgenmstoid) != null)
                                tbl.apgenmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.apgendate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnapgenmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.apgenmstoid + " WHERE tablename='QL_trnapgenmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE MRDTL SET mrgendtlstatus='' FROM (SELECT mrgendtlstatus FROM QL_trnmrgendtl mrd INNER JOIN QL_trnapgendtl apd ON apd.cmpcode=mrd.cmpcode AND apd.mrgenmstoid=mrd.mrgenmstoid AND apd.mrgendtloid=mrd.mrgendtloid  WHERE apd.cmpcode='" + tbl.cmpcode + "' AND apd.apgenmstoid=" + tbl.apgenmstoid + ") AS MRDTL ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrgenmst SET mrgenmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnapgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND apgenmstoid=" + tbl.apgenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapgendtl.Where(a => a.apgenmstoid == tbl.apgenmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapgendtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnapgendtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].apgendtlnote == null)
                                dtDtl[i].apgendtlnote = "";

                            tbldtl = new QL_trnapgendtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.apgendtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.apgenmstoid = tbl.apgenmstoid;
                            tbldtl.apgendtlseq = i + 1;
                            tbldtl.pogendtloid = dtDtl[i].pogendtloid;
                            tbldtl.pogenmstoid = dtDtl[i].pogenmstoid;
                            tbldtl.mrgendtloid = dtDtl[i].mrgendtloid;
                            tbldtl.mrgenmstoid = dtDtl[i].mrgenmstoid;
                            tbldtl.matgenoid = dtDtl[i].matgenoid;
                            tbldtl.apgenqty = dtDtl[i].apgenqty;
                            tbldtl.apgenunitoid = dtDtl[i].apgenunitoid;
                            tbldtl.apgenprice = dtDtl[i].apgenprice;
                            tbldtl.apgendtlamt = dtDtl[i].apgendtlamt;
                            tbldtl.apgendtldisctype = dtDtl[i].apgendtldisctype;
                            tbldtl.apgendtldiscvalue = dtDtl[i].apgendtldiscvalue;
                            tbldtl.apgendtldiscamt = dtDtl[i].apgendtldiscamt;
                            tbldtl.apgendtlnetto = dtDtl[i].apgendtlnetto;
                            tbldtl.apgendtltaxamt = dtDtl[i].apgendtltaxamt;
                            tbldtl.apgendtlnote = dtDtl[i].apgendtlnote;
                            tbldtl.apgendtlstatus = "";
                            tbldtl.apgendtlres1 = dtDtl[i].apgendtlres1;
                            tbldtl.apgendtlres2 = dtDtl[i].apgendtlres2;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnapgendtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrgendtl SET mrgendtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid=" + dtDtl[i].mrgendtloid + " AND mrgenmstoid=" + dtDtl[i].mrgenmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnmrgenmst SET mrgenmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid=" + dtDtl[i].mrgenmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnapgendtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.apgenmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AP-gen" + tbl.apgenmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnapgenmst";
                                tblApp.oid = tbl.apgenmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.apgenmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            tbl.apgenmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: APgenMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapgenmst tbl = db.QL_trnapgenmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmrgendtl SET mrgendtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgendtloid IN (SELECT mrgendtloid FROM QL_trnapgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND apgenmstoid=" + tbl.apgenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmrgenmst SET mrgenmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrgenmstoid IN (SELECT mrgenmstoid FROM QL_trnapgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND apgenmstoid=" + tbl.apgenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();



                        var trndtl = db.QL_trnapgendtl.Where(a => a.apgenmstoid == id && a.cmpcode == cmp);
                        db.QL_trnapgendtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapgenmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPTrn.rpt"));

            //report.SetParameterValue("sRegister", "(SELECT registerno FROM QL_trnregistermst regm WHERE regm.cmpcode=apd.cmpcode AND regm.registermstoid=apd.registermstoid )");
            report.SetParameterValue("sRegister", "(SELECT pogenno FROM QL_trnpogenmst regm WHERE regm.cmpcode=apd.cmpcode AND regm.pogenmstoid=apd.pogenmstoid )");
            report.SetParameterValue("sMatCode", "(SELECT matgencode FROM QL_mstmatgen m WHERE m.matgenoid=apd.matgenoid)");
            report.SetParameterValue("sMatDesc", "(SELECT matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=apd.matgenoid)");
            report.SetParameterValue("sType", "gen");
            report.SetParameterValue("sHeader", "A/P gen MATERIAL PRINT OUT");
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + cmp + "' AND apm.apgenmstoid IN (" + id + ")");


            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "APgenMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}