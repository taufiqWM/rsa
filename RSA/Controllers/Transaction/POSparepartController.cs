﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class POSparepartController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpospmst
        {
            public string cmpcode { get; set; }
            public int pospmstoid { get; set; }
            public string posptype { get; set; }
            public int posppaytypeoid { get; set; }
            public string pospno { get; set; }
            public DateTime pospdate { get; set; }
            public string suppname { get; set; }
            public string pospmststatus { get; set; }
            public string pospmstnote { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnpospdtl
        {
            public int pospdtlseq { get; set; }
            public int prspmstoid { get; set; }
            public int prspdtloid { get; set; }
            public string prspno { get; set; }
            public string prspdatestr { get; set; }
            public DateTime prspdate { get; set; }
            public int sparepartoid { get; set; }
            public string sparepartcode { get; set; }
            public string sparepartlongdesc { get; set; }
            public decimal sparepartlimitqty { get; set; }
            public decimal prspqty { get; set; }
            public decimal pospqty { get; set; }
            public int pospunitoid { get; set; }
            public string pospunit { get; set; }
            public decimal pospprice { get; set; }
            public decimal pospdtlamt { get; set; }
            public string pospdtldisctype { get; set; }
            public decimal pospdtldiscvalue { get; set; }
            public decimal pospdtldiscamt { get; set; }
            public decimal pospdtlnetto { get; set; }
            public string pospdtlnote { get; set; }
            public string pospdtlnote2 { get; set; }
            public string prsparrdatereqstr { get; set; }
            public DateTime prsparrdatereq { get; set; }
            public decimal lastpoprice { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnpospmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpospmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            ViewBag.approvalcode = approvalcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var posppaytype = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.posppaytypeoid);
            ViewBag.posppaytype = posppaytype;

            //sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'";
            //var pospunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.pospunitoid);
            //ViewBag.pospunitoid = pospunitoid;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpospmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prmstoid, int suppoid,int pomstoid)
        {
            List<trnpospdtl> tbl = new List<trnpospdtl>();

                sSql = "SELECT 0 AS pospdtlseq, prd.prspmstoid, prd.prspdtloid, prm.prspno, prd.sparepartoid, ISNULL((prd.prspqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(pod.pospqty - (CASE WHEN pod.pospdtlres1 IS NULL THEN ISNULL(pod.closeqty, 0) WHEN pod.pospdtlres1='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.pospqty - CONVERT(decimal(18,2), pod.pospdtlres1)) END)), 0) FROM QL_trnpospdtl pod INNER JOIN QL_trnpospmst pom ON pom.cmpcode=pod.cmpcode AND pom.pospmstoid=pod.pospmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prspdtloid=prd.prspdtloid AND pom.pospmststatus NOT IN ('Cancel', 'Rejected') AND pod.pospmstoid<>" + pomstoid + ")) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='sp' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prspdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0)), 0) AS prspqty, prd.prspunitoid AS pospunitoid, m.sparepartcode, m.sparepartlongdesc, m.sparepartlimitqty, g2.gendesc AS pospunit, 0.0 AS pospqty, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstsparepartprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.sparepartoid=prd.sparepartoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstsparepartprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.sparepartoid=prd.sparepartoid ORDER BY crd.updtime DESC), 0.0)) AS pospprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstsparepartprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.sparepartoid=prd.sparepartoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstsparepartprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.sparepartoid=prd.sparepartoid ORDER BY crd.updtime DESC), 1)) AS lastcurroid, prd.prspdtlnote AS pospdtlnote, 0.0 AS lastpoprice, CONVERT(VARCHAR(10), prm.prspdate, 101) AS prspdatestr, prm.prspdate, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prsparrdatereqstr, prd.prsparrdatereq, 0.0 AS pospdtlamt, 'A' AS pospdtldisctype, 0.0 AS pospdtldiscvalue, 0.0 AS pospdtldiscamt, 0.0 AS pospdtlnetto, '' AS pospdtlnote2 FROM QL_prspdtl prd INNER JOIN QL_prspmst prm ON prm.cmpcode=prd.cmpcode AND prm.prspmstoid=prd.prspmstoid INNER JOIN QL_mstsparepart m ON prd.sparepartoid=m.sparepartoid /*INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1code=SUBSTRING(m.sparepartcode, 1, 2) AND cat1res1='sp' AND cat1res2 LIKE '%WIP%'*/ INNER JOIN QL_mstgen g2 ON prd.prspunitoid=g2.genoid WHERE prd.cmpcode='" + CompnyCode + "' AND prd.prspmstoid=" + prmstoid + " AND prd.prspdtlstatus='' AND prm.prspmststatus='Approved'";

            tbl = db.Database.SqlQuery<trnpospdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpospdtl> dtDtl)
        {
            Session["QL_trnpospdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpospdtl"] == null)
            {
                Session["QL_trnpospdtl"] = new List<trnpospdtl>();
            }

            List<trnpospdtl> dataDtl = (List<trnpospdtl>)Session["QL_trnpospdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpospmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.potype = tbl.posptype;
            ViewBag.potaxtype = tbl.posptaxtype;
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            //public string supptaxable { get; set; }
            public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

            [HttpPost]
        public ActionResult GetSuppData()
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, /*(CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 10 ELSE 0 END) supptaxamt, c.currcode, suppcurroid,*/ gp.gendesc supppayment,0 supptaxamt FROM QL_mstsupp s /*INNER JOIN QL_mstcurr c ON suppcurroid=c.curroid*/ INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class prspmst
        {
            public int prspmstoid { get; set; }
            public string prspno { get; set; }
            public string prspdate { get; set; }
            public string prspexpdate { get; set; }
            public string deptname { get; set; }
            public string prspmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp)
        {
            List<prspmst> tbl = new List<prspmst>();
            sSql = " SELECT DISTINCT prm.prspmstoid, prspno, CONVERT(VARCHAR(10), prspdate, 101) AS prspdate, CONVERT(VARCHAR(10), prspexpdate, 101) AS prspexpdate, deptname, prspmstnote FROM QL_prspmst prm INNER JOIN QL_prspdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prspmstoid=prd.prspmstoid INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" + CompnyCode + "' AND prm.prspmststatus='Approved' ORDER BY prspdate DESC, prm.prspmstoid DESC ";
            
            tbl = db.Database.SqlQuery<prspmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: POspMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, mdFilter modfil) //string filter
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT pom.cmpcode, pospmstoid, pospno, pospdate, suppname, pospmststatus, pospmstnote, divname, posptype FROM QL_trnpospmst pom INNER JOIN QL_mstsupp s ON s.cmpcode='" + CompnyCode + "' AND s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND pospdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pospdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pospmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND pospdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pospdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pospmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiod1 != null & modfil.filterperiod2 != null)
                {
                    sSql += " AND pospdate>=CAST('" + modfil.filterperiod1 + " 00:00:00' AS DATETIME) AND pospdate<=CAST('" + modfil.filterperiod2 + " 23:59:59' AS DATETIME)"; //ToString("MM/dd/yyyy")
                    if (modfil.ddlstatus != "ALL")
                        sSql += " AND pospmststatus='" + modfil.ddlstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.ddlstatus == "ALL" | modfil.ddlstatus == "Post" | modfil.ddlstatus == "Approved" | modfil.ddlstatus == "Closed" | modfil.ddlstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND pospmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpospmst> dt = db.Database.SqlQuery<trnpospmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: POspMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpospmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnpospmst();
                tbl.cmpcode = CompnyCode;
                tbl.pospmstoid = ClassFunction.GenerateID("QL_trnpospmst");
                tbl.pospdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.pospmststatus = "In Process";

                Session["QL_trnpospdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpospmst.Find(cmp, id);

                sSql = "SELECT pod.pospdtlseq, prd.prspmstoid, prm.prspno, CONVERT(VARCHAR(10), prm.prspdate, 101) AS prspdatestr, prm.prspdate, pod.prspdtloid, pod.sparepartoid, m.sparepartcode, m.sparepartlongdesc, m.sparepartlimitqty, pod.pospqty, ISNULL((prd.prspqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.pospqty - (CASE WHEN podtl.pospdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.pospdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.pospqty - CONVERT(decimal(18,2), podtl.pospdtlres1)) END)), 0) FROM QL_trnpospdtl podtl INNER JOIN QL_trnpospmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.pospmstoid=podtl.pospmstoid WHERE podtl.prspdtloid=prd.prspdtloid AND podtl.pospmstoid<>" + id + " AND pomst.pospmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='sp' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prspdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0) AS prspqty,  0.0 AS poqtyuse,  pod.pospunitoid, g.gendesc AS pospunit, pod.pospprice, pod.pospdtlamt, pod.pospdtldisctype, pod.pospdtldiscvalue, pod.pospdtldiscamt, pod.pospdtlnetto, pod.pospdtlnote, /*ISNULL(pod.pospdtlnote2,'') pospdtlnote2,*/ ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstsparepartprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.sparepartoid=pod.sparepartoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstsparepartprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.sparepartoid=pod.sparepartoid ORDER BY crd.updtime), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstsparepartprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.sparepartoid=pod.sparepartoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstsparepartprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.sparepartoid=pod.sparepartoid ORDER BY crd.updtime), 1)) AS lastcurroid, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prsparrdatereqstr, prd.prsparrdatereq FROM QL_trnpospdtl pod INNER JOIN QL_trnpospmst pom ON pom.cmpcode=pod.cmpcode AND pom.pospmstoid=pod.pospmstoid INNER JOIN QL_mstsparepart m ON pod.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON pod.pospunitoid=g.genoid INNER JOIN QL_prspdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prspdtloid=pod.prspdtloid INNER JOIN QL_prspmst prm ON prm.cmpcode=pod.cmpcode AND prm.prspmstoid=pod.prspmstoid WHERE pod.pospmstoid=" + id + " AND pod.cmpcode='"+ cmp + "' ORDER BY pod.pospdtlseq";
                Session["QL_trnpospdtl"] = db.Database.SqlQuery<trnpospdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: POspMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpospmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.pospno == null)
                tbl.pospno = "";

            List<trnpospdtl> dtDtl = (List<trnpospdtl>)Session["QL_trnpospdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].pospqty <= 0)
                        {
                            ModelState.AddModelError("", "PO QTY for Material " + dtDtl[i].sparepartlongdesc + " must be more than 0");
                        }

                        if (dtDtl[i].pospprice <= 0)
                        {
                            ModelState.AddModelError("", "PO Price for Material " + dtDtl[i].sparepartlongdesc + " must be more than 0");
                        }

                        sSql = "SELECT ISNULL((prd.prspqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.pospqty - (CASE WHEN podtl.pospdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.pospdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.pospqty - CONVERT(decimal(18,2), podtl.pospdtlres1)) END)), 0) FROM QL_trnpospdtl podtl INNER JOIN QL_trnpospmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.pospmstoid=podtl.pospmstoid WHERE podtl.cmpcode=prd.cmpcode AND podtl.prspmstoid=prd.prspmstoid AND  podtl.prspdtloid=prd.prspdtloid AND podtl.pospmstoid<>" + tbl.pospmstoid + " AND pomst.pospmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='sp' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prspdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0.0) AS prspqty FROM QL_prspdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prspdtloid=" + dtDtl[i].prspdtloid + "";
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].prspqty)
                            dtDtl[i].prspqty = dQty;
                        if (dQty < dtDtl[i].pospqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PR Qty!");

                        //sSql = "SELECT COUNT(-1) FROM QL_prspdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prspmstoid=" + dtDtl[i].prspmstoid + " AND prd.prspdtloid=" + dtDtl[i].prspdtloid + " AND prd.prspdtlstatus='COMPLETE'";
                        //var CheckDataExists = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (CheckDataExists > 0 )
                        //{
                        //    ModelState.AddModelError("", "There are some PR detail data has been closed by another user. You cannot use this PR detail data anymore!");
                        //}
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.pospmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpospmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpospmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpospdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpospmst.Find(tbl.cmpcode, tbl.pospmstoid) != null)
                                tbl.pospmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pospdate);
                            //tbl.divoid = divoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.pospratetoidrchar = "0";
                            tbl.pospratetousdchar = "0";
                            tbl.posprate2toidrchar = "0";
                            tbl.posprate2tousdchar = "0";
                            tbl.posptaxtype = "0";
                            //tbl.pospmststatus_onpib = "";
                            db.QL_trnpospmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.pospmstoid + " WHERE tablename='QL_trnpospmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.pospratetoidrchar = "0";
                            tbl.pospratetousdchar = "0";
                            tbl.posprate2toidrchar = "0";
                            tbl.posprate2tousdchar = "0";
                            tbl.posptaxtype = "0";
                            //tbl.pospmststatus_onpib = "";
                            //--
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_prspdtl SET prspdtlstatus='', prspdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND prspdtloid IN (SELECT prspdtloid FROM QL_trnpospdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pospmstoid=" + tbl.pospmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_prspmst SET prspmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prspmstoid IN (SELECT DISTINCT prspmstoid FROM QL_trnpospdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pospmstoid=" + tbl.pospmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpospdtl.Where(a => a.pospmstoid == tbl.pospmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpospdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpospdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpospdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.pospdtloid = dtloid++;
                            tbldtl.pospmstoid = tbl.pospmstoid;
                            tbldtl.pospdtlseq = i + 1;
                            tbldtl.prspmstoid = dtDtl[i].prspmstoid;
                            tbldtl.prspdtloid = dtDtl[i].prspdtloid;
                            tbldtl.sparepartoid = dtDtl[i].sparepartoid;
                            tbldtl.pospqty = dtDtl[i].pospqty;
                            tbldtl.pospunitoid = dtDtl[i].pospunitoid;
                            tbldtl.pospprice = dtDtl[i].pospprice;
                            tbldtl.pospdtlamt = dtDtl[i].pospdtlamt;
                            tbldtl.pospdtldisctype = dtDtl[i].pospdtldisctype;
                            tbldtl.pospdtldiscvalue = dtDtl[i].pospdtldiscvalue;
                            tbldtl.pospdtldiscamt = dtDtl[i].pospdtldiscamt;
                            tbldtl.pospdtlnetto = dtDtl[i].pospdtlnetto;
                            tbldtl.pospdtlnote = dtDtl[i].pospdtlnote == null ? "" : dtDtl[i].pospdtlnote;
                            //tbldtl.pospdtlnote2 = dtDtl[i].pospdtlnote2;
                            tbldtl.pospdtlstatus = "";
                            //tbldtl.pospdtlstatus_onpib = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnpospdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].pospqty >= dtDtl[i].prspqty)
                            {
                                sSql = "UPDATE QL_prspdtl SET prspdtlstatus='COMPLETE', prspdtlres1='" + dtloid + "' WHERE cmpcode='" + tbl.cmpcode + "' AND prspdtloid=" + dtDtl[i].prspdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_prspmst SET prspmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND prspmstoid=" + dtDtl[i].prspmstoid + " AND (SELECT COUNT(prspdtloid) FROM QL_prspdtl WHERE cmpcode='" + tbl.cmpcode + "' AND prspdtlstatus='' AND prspmstoid=" + dtDtl[i].prspmstoid + " AND prspdtloid<>" + +dtDtl[i].prspdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnpospdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.pospmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-sp" + tbl.pospmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpospmst";
                                tblApp.oid = tbl.pospmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        //if (tbl.pospmststatus.ToUpper() == "In Approval")
                        //{
                        //    var dt = null;
                        //    //Dim dt As DataTable = Session("AppPerson")
                        //    for (int i = 0; i < dtLastHdrData.Count(); i++)
                        //    {
                        //        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'PO-sp" & pospmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & pospdate.Text & "', 'New', 'QL_trnpospmst', " & pospmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();
                        //    }
                        //    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.pospmstoid + "?cmp=" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.pospmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: POspMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpospmst tbl = db.QL_trnpospmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_prspdtl SET prspdtlstatus='', prspdtlres1='', prclosingqty=0.0 WHERE cmpcode='" + tbl.cmpcode + "' AND prspdtloid IN (SELECT prspdtloid FROM QL_trnpospdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pospmstoid=" + tbl.pospmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prspmst SET prspmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prspmstoid IN (SELECT DISTINCT prspmstoid FROM QL_trnpospdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pospmstoid=" + tbl.pospmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = GetQueryInsertStatus_Multi(Session("UserID"), DDLBusUnit.SelectedValue, "QL_trnpospdtl", pospmstoid.Text, "Open", "pospmstoid", "QL_prspmst", "prspmstoid", "Open karena PO dihapus");
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnpospdtl.Where(a => a.pospmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpospdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpospmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string supptype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpospmst.Find(cmp, id);
            if (tbl == null)
                return null;

            if (supptype == "LOCAL")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_TrnEn.rpt"));

            //sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.pospmstoid [Oid], CONVERT(VARCHAR(10), pom.pospmstoid) AS [Draft No.], pom.pospno [PO No.], pom.pospdate [PO Date], pom.pospmstnote [Header Note], pom.pospmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.pospmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.pospmstdiscamt, 0) AS [Disc Amount], pom.posptotalnetto [Total Netto], pom.posptaxtype [Tax Type], CONVERT(VARCHAR(10), pom.posptaxamt) AS [Tax Amount], ISNULL(pom.pospvat, 0) AS [VAT], ISNULL(pom.pospdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.pospothercost, 0) AS [Other Cost], pom.pospgrandtotalamt [Grand Total], pod.pospdtloid [DtlOid], ISNULL((SELECT m2.sparepartdtl2code FROM QL_mstsparepartdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.sparepartoid=m2.sparepartoid), m.sparepartcode) AS [Code], ISNULL((SELECT m2.sparepartdtl2name FROM QL_mstsparepartdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.sparepartoid=m2.sparepartoid), m.sparepartlongdesc) AS [Material], prm.prspno AS [PR No.], prm.prspdate AS [PR Date], pod.pospqty AS [Qty], g2.gendesc AS [Unit], pod.pospprice AS [Price], pod.pospdtlamt AS [Amount], ISNULL(pod.pospdtldiscamt, 0) AS [Detail Disc.], pod.pospdtlnetto AS [Netto], pod.pospdtlnote AS [Note], pom.posptype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.pospsuppref AS [SuppReff], pod.pospdtldisctype AS [DtlDiscType], ISNULL(pod.pospdtldiscvalue,0) AS [DtlDiscValue], pod.pospdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prsparrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpospmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.posppaytypeoid INNER JOIN QL_trnpospdtl pod ON pod.cmpcode=pom.cmpcode AND pod.pospmstoid=pom.pospmstoid INNER JOIN QL_mstsparepart m ON m.sparepartoid=pod.sparepartoid LEFT JOIN QL_prspmst prm ON prm.cmpcode=pod.cmpcode AND prm.prspmstoid=pod.prspmstoid LEFT JOIN QL_prspdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prspmstoid=pod.prspmstoid AND prd.prspdtloid=pod.prspdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.pospunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.pospmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.pospmstoid, pod.pospdtlseq ";
            sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.pospmstoid [Oid], CONVERT(VARCHAR(10), pom.pospmstoid) AS [Draft No.], pom.pospno [PO No.], pom.pospdate [PO Date], pom.pospmstnote [Header Note], pom.pospmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.pospmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.pospmstdiscamt, 0) AS [Disc Amount], pom.posptotalnetto [Total Netto], pom.posptaxtype [Tax Type], CONVERT(VARCHAR(10), pom.posptaxamt) AS [Tax Amount], ISNULL(pom.pospvat, 0) AS [VAT], ISNULL(pom.pospdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.pospothercost, 0) AS [Other Cost], pom.pospgrandtotalamt [Grand Total], pod.pospdtloid [DtlOid], '' AS [Code], '' AS [Material], prm.prspno AS [PR No.], prm.prspdate AS [PR Date], pod.pospqty AS [Qty], g2.gendesc AS [Unit], pod.pospprice AS [Price], pod.pospdtlamt AS [Amount], ISNULL(pod.pospdtldiscamt, 0) AS [Detail Disc.], pod.pospdtlnetto AS [Netto], pod.pospdtlnote AS [Note], pom.posptype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.pospsuppref AS [SuppReff], pod.pospdtldisctype AS [DtlDiscType], ISNULL(pod.pospdtldiscvalue,0) AS [DtlDiscValue], pod.pospdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prsparrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpospmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.posppaytypeoid INNER JOIN QL_trnpospdtl pod ON pod.cmpcode=pom.cmpcode AND pod.pospmstoid=pom.pospmstoid INNER JOIN QL_mstsparepart m ON m.sparepartoid=pod.sparepartoid LEFT JOIN QL_prspmst prm ON prm.cmpcode=pod.cmpcode AND prm.prspmstoid=pod.prspmstoid LEFT JOIN QL_prspdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prspmstoid=pod.prspmstoid AND prd.prspdtloid=pod.prspdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.pospunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.pospmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.pospmstoid, pod.pospdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPOsp");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "POspMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
