﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PurchaseReturnRawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"]);
        private string sSql = "";

        public class trnpretrawmst
        {
            public string cmpcode { get; set; }
            public int pretrawmstoid { get; set; }
            public string pretrawno { get; set; }
            public string porawno { get; set; }
            public DateTime pretrawdate { get; set; }
            public string suppname { get; set; }
            public string supptype { get; set; }
            public string pretrawmststatus { get; set; }
            public string pretrawmstnote { get; set; }
            public string divname { get; set; }
            public int curroid { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnpretrawdtl
        {
            public int pretrawdtlseq { get; set; }
            public int mrrawmstoid { get; set; }
            public int mrrawdtloid { get; set; }
            public string mrrawno { get; set; }
            public int pretrawwhoid { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal matrawlimitqty { get; set; }
            public decimal mrrawqty { get; set; }
            public decimal pretrawqty { get; set; }
            public int pretrawunitoid { get; set; }
            public string pretrawunit { get; set; }
            public string pretrawdtlnote { get; set; }
            public decimal mrqty { get; set; }
            public int reasonoid { get; set; }

        }

        private void InitDDL(QL_trnpretrawmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;

            List<QL_mstgen> tblgen = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PURCHASE RETURN REASON' AND activeflag='ACTIVE'";
            tblgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            var reason = "";
            for (int i = 0;i < tblgen.Count; i++)
            {
                reason += "<option value=\'" + tblgen[i].genoid + "\'>" + tblgen[i].gendesc + "</option>";
            }
            ViewBag.reason = reason;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int mrrawmstoid, int pretrawmstoid)
        {
            List<trnpretrawdtl> tbl = new List<trnpretrawdtl>();

            sSql = "SELECT DISTINCT 0 AS pretrawdtlseq, mrrawno, mrd.mrrawmstoid, mrrawdtloid, mrd.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, mrrawunitoid AS pretrawunitoid, gendesc AS pretrawunit, (mrrawqty - ISNULL((SELECT SUM(pretrawqty) FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrrawdtloid=mrd.mrrawdtloid AND pretrawmststatus<>'Rejected' AND pretd.pretrawmstoid<>" + pretrawmstoid + "), 0)) AS mrrawqty, 0.0 AS pretrawqty, '' AS pretrawdtlnote, mrrawdtlseq, 0 AS reasonoid, mrrawwhoid AS pretrawwhoid FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnmrrawmst mrm ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g ON genoid=mrrawunitoid WHERE mrd.cmpcode='" + cmp + "' AND mrd.mrrawmstoid=" + mrrawmstoid + " AND ISNULL(mrrawdtlres1, '')<>'Complete' ORDER BY mrrawdtlseq ";

            tbl = db.Database.SqlQuery<trnpretrawdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpretrawdtl> dtDtl)
        {
            Session["QL_trnpretrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpretrawdtl"] == null)
            {
                Session["QL_trnpretrawdtl"] = new List<trnpretrawdtl>();
            }

            List<trnpretrawdtl> dataDtl = (List<trnpretrawdtl>)Session["QL_trnpretrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpretrawmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.porawno = db.Database.SqlQuery<string>("SELECT porawno FROM QL_trnporawmst WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + "").FirstOrDefault();
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnporawmst WHERE cmpcode='" + cmp + "' AND porawmstoid IN (SELECT porawmstoid FROM QL_trnmrrawmst WHERE cmpcode='" + cmp + "' AND mrrawmststatus='Post' AND ISNULL(mrrawmstres1, '')<>'Closed') AND porawmstoid NOT IN (SELECT porawmstoid FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid WHERE apd.cmpcode='" + cmp + "' AND aprawmststatus<>'Rejected') AND (CASE porawtype WHEN 'Local' THEN 'Closed' ELSE porawmststatus END)='Closed')  ORDER BY suppcode, suppname";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class pomst
        {
            public int porawmstoid { get; set; }
            public string porawno { get; set; }
            public DateTime porawdate { get; set; }
            public string porawdocrefno { get; set; }
            public string porawmstnote { get; set; }
            public string porawflag { get; set; }
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid)
        {
            List<pomst> tbl = new List<pomst>();

            //sSql = "SELECT registermstoid, registerno, registerdate, registerdocrefno, registerdocrefdate, registermstnote, registerflag FROM QL_trnregistermst WHERE cmpcode='" + cmp + "' AND suppoid=" + suppoid + " AND registertype='RAW' AND registermstoid IN (SELECT registermstoid FROM QL_trnmrrawmst WHERE cmpcode='" + cmp + "' AND mrrawmststatus='Post' AND ISNULL(mrrawmstres1, '')<>'Closed') AND registermstoid NOT IN (SELECT registermstoid FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid WHERE apd.cmpcode='" + cmp + "' AND aprawmststatus<>'Rejected') AND (CASE registerflag WHEN 'Local' THEN 'Closed' ELSE registermststatus END)='Closed' ORDER BY registermstoid ";
            sSql = "SELECT porawmstoid, porawno, porawdate, '' [porawdocrefno], ISNULL(porawmstnote, '') [porawmstnote], porawtype " +
                "FROM QL_trnporawmst " +
                "WHERE cmpcode = '" + cmp + "' AND suppoid = " + suppoid + " AND porawmstoid IN(SELECT porawmstoid FROM QL_trnmrrawmst WHERE cmpcode = '" + cmp + "' AND mrrawmststatus = 'Post' AND ISNULL(mrrawmstres1, '') <> 'Closed') AND porawmstoid NOT IN (SELECT porawmstoid FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode = apd.cmpcode AND apm.aprawmstoid = apd.aprawmstoid WHERE apd.cmpcode = '" + cmp + "' AND aprawmststatus <> 'Rejected') AND(CASE porawtype WHEN 'Local' THEN 'Closed' ELSE porawmststatus END) = 'Closed' " +
                "ORDER BY porawmstoid";

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class mrmst
        {
            public int mrrawmstoid { get; set; }
            public string mrrawno { get; set; }
            public DateTime mrrawdate { get; set; }
            public string mrrawwh { get; set; }
            public string mrrawmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetMRData(string cmp, int porawmstoid)
        {
            List<mrmst> tbl = new List<mrmst>();
            sSql = "SELECT mrrawmstoid, mrrawno, mrrawdate, mrrawwhoid, gendesc AS mrrawwh, mrrawmstnote FROM QL_trnmrrawmst mrm INNER JOIN QL_mstgen ON genoid=mrrawwhoid WHERE mrm.cmpcode='" + cmp + "' AND porawmstoid=" + porawmstoid + " AND mrrawmststatus='Post' ORDER BY mrrawmstoid";

            tbl = db.Database.SqlQuery<mrmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = " SELECT pretm.cmpcode, pretrawmstoid, pretm.pretrawno, pretm.pretrawdate, suppname, porawno, pretrawmststatus, pretrawmstnote, divname FROM QL_trnpretrawmst pretm INNER JOIN QL_trnporawmst reg ON reg.cmpcode = pretm.cmpcode AND reg.porawmstoid = pretm.porawmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pretm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pretm.cmpcode WHERE ";
            
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pretm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND pretrawdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretrawdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pretrawmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND pretrawdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretrawdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND pretrawmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pretrawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pretrawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND pretrawmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND pretrawmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND pretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pretmm.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpretrawmst> dt = db.Database.SqlQuery<trnpretrawmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretrawmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnpretrawmst();
                tbl.cmpcode = CompnyCode;
                tbl.pretrawmstoid = ClassFunction.GenerateID("QL_trnpretrawmst");
                tbl.pretrawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.pretrawmststatus = "In Process";
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);

                Session["QL_trnpretrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpretrawmst.Find(cmp, id);

                sSql = "SELECT pretrawdtlseq, pretd.mrrawdtloid, pretd.mrrawmstoid, mrrawno, pretrawwhoid, pretd.mrrawdtloid, pretd.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, (mrrawqty - ISNULL((SELECT SUM(x.pretrawqty) FROM QL_trnpretrawdtl x INNER JOIN QL_trnpretrawmst xm ON xm.cmpcode=x.cmpcode AND xm.pretrawmstoid=x.pretrawmstoid WHERE x.cmpcode=pretd.cmpcode AND x.mrrawdtloid=pretd.mrrawdtloid AND x.pretrawmstoid<>pretd.pretrawmstoid AND xm.pretrawmststatus<>'Rejected'), 0)) AS mrrawqty, pretrawqty, pretrawunitoid, g.gendesc AS pretrawunit, ISNULL(pretrawdtlnote, '') [pretrawdtlnote], reasonoid, mrrawqty AS mrqty FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode=pretd.cmpcode AND mrm.mrrawmstoid=pretd.mrrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pretd.matrawoid INNER JOIN QL_mstgen g ON g.genoid=pretrawunitoid INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrrawdtloid=pretd.mrrawdtloid LEFT JOIN QL_mstgen gres ON gres.genoid=reasonoid WHERE pretd.cmpcode='" + cmp + "' AND pretd.pretrawmstoid=" + id + " ORDER BY pretrawdtlseq ";

                Session["QL_trnpretrawdtl"] = db.Database.SqlQuery<trnpretrawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }
        
        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpretrawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.pretrawno == null)
                tbl.pretrawno = "";

            List<trnpretrawdtl> dtDtl = (List<trnpretrawdtl>)Session["QL_trnpretrawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].pretrawqty <= 0)
                        {
                            ModelState.AddModelError("", "PRET QTY for Material " + dtDtl[i].matrawlongdesc + " must be more than 0");
                        }
                        //rounded qty

                        sSql = "SELECT (mrrawqty - ISNULL((SELECT SUM(pretrawqty) FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.mrrawdtloid=mrd.mrrawdtloid AND pretd.pretrawmstoid<>" + tbl.pretrawmstoid + " AND pretrawmststatus<>'Rejected'), 0)) AS mrrawqty FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode='" + tbl.cmpcode + "' AND mrd.mrrawmstoid=" + dtDtl[i].mrrawmstoid + " AND mrd.mrrawdtloid=" + dtDtl[i].mrrawdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].mrrawqty)
                            dtDtl[i].mrrawqty = dQty;
                        if (dQty < dtDtl[i].pretrawqty)
                            ModelState.AddModelError("", "MR QTY for some detail data has been updated by another user. Please check that every detail MR QTY must be more or equal than RETURN QTY!");

                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.pretrawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                //AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretrawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpretrawmst' AND activeflag='ACTIVE'").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpretrawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpretrawdtl");
                var servertime = ClassFunction.GetServerTime();
                //tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpretrawmst.Find(tbl.cmpcode, tbl.pretrawmstoid) != null)
                                tbl.pretrawmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pretrawdate);
                            tbl.registermstoid = 0;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnpretrawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.pretrawmstoid + " WHERE tablename='QL_trnpretrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();


                            sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnpretrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretrawmstoid=" + tbl.pretrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrrawmst SET mrrawmstres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnpretrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretrawmstoid=" + tbl.pretrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpretrawdtl.Where(a => a.pretrawmstoid == tbl.pretrawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpretrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpretrawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {

                            tbldtl = new QL_trnpretrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.pretrawdtloid = dtloid++;
                            tbldtl.pretrawmstoid = tbl.pretrawmstoid;
                            tbldtl.pretrawdtlseq = i + 1;
                            tbldtl.mrrawmstoid = dtDtl[i].mrrawmstoid;
                            tbldtl.mrrawdtloid = dtDtl[i].mrrawdtloid;
                            tbldtl.pretrawwhoid = dtDtl[i].pretrawwhoid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.pretrawqty = dtDtl[i].pretrawqty;
                            tbldtl.pretrawunitoid = dtDtl[i].pretrawunitoid;
                            tbldtl.pretrawdtlstatus = "";
                            tbldtl.pretrawdtlnote = dtDtl[i].pretrawdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.pretrawvalueidr = 0;
                            tbldtl.pretrawvalueusd = 0;
                            tbldtl.reasonoid = dtDtl[i].reasonoid;
                            //tbldtl.pretrawvalueidr_stock = 0;
                            //tbldtl.pretrawvalueusd_stock = 0;
                            db.QL_trnpretrawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].pretrawqty >= dtDtl[i].mrrawqty)
                            {
                                sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlres1='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawdtloid=" + dtDtl[i].mrrawdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmrrawmst SET mrrawmstres1='Closed' WHERE cmpcode='" + tbl.cmpcode +  "' AND mrrawmstoid=" + dtDtl[i].mrrawmstoid + " AND (SELECT COUNT(*) FROM QL_trnmrrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid=" + dtDtl[i].mrrawmstoid + " AND mrrawdtloid<>" + dtDtl[i].mrrawdtloid + " AND ISNULL(mrrawdtlres1, '')<>'Complete')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpretrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.pretrawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PRET-RAW" + tbl.pretrawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpretrawmst";
                                tblApp.oid = tbl.pretrawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.pretrawmstoid + "?cmp=" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.pretrawno = "";
            tbl.pretrawmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretrawmst tbl = db.QL_trnpretrawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnmrrawdtl SET mrrawdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnpretrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretrawmstoid=" + tbl.pretrawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnmrrawmst SET mrrawmstres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrrawmstoid IN (SELECT mrrawmstoid FROM QL_trnpretrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND pretrawmstoid=" + tbl.pretrawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpretrawdtl.Where(a => a.pretrawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpretrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpretrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpretrawmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPRet_PrintOut.rpt"));
            //sSql = "SELECT pretm.pretrawmstoid [Oid], CONVERT(VARCHAR(10), pretm.pretrawmstoid) AS [Draft No.], pretrawno [Return No.], pretrawdate [Return Date], registerno [Register No.], pretrawmstnote [Header Note], pretrawmststatus [Status], suppname [Supplier], matrawcode [Code], matrawlongdesc [Material], pretrawdtlnote [Detail Note], pretrawdtlseq [No.], pretrawqty [Qty], gendesc AS [Unit], divname [Business Unit] FROM QL_trnpretrawmst pretm INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretd.pretrawmstoid=pretm.pretrawmstoid INNER JOIN QL_trnregistermst reg ON reg.cmpcode=pretm.cmpcode AND reg.registermstoid=pretm.registermstoid INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pretd.matrawoid INNER JOIN QL_mstgen ON genoid=pretrawunitoid INNER JOIN QL_mstdivision div ON div.cmpcode=pretm.cmpcode WHERE pretm.cmpcode='" + cmp + "' AND pretm.pretrawmstoid IN (" + id + ") ORDER BY pretm.pretrawmstoid, pretrawdtlseq ";
            sSql = "SELECT pretm.pretrawmstoid [Oid], CONVERT(VARCHAR(10), pretm.pretrawmstoid) AS [Draft No.], pretrawno [Return No.], pretrawdate [Return Date], porawno [poraw No.], pretrawmstnote [Header Note], pretrawmststatus [Status], suppname [Supplier], matrawcode [Code], matrawlongdesc [Material], pretrawdtlnote [Detail Note], pretrawdtlseq [No.], pretrawqty [Qty], gendesc AS [Unit], divname [Business Unit] " +
                "FROM QL_trnpretrawmst pretm " +
                "INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretd.pretrawmstoid = pretm.pretrawmstoid " +
                "INNER JOIN QL_trnporawmst reg ON reg.cmpcode = pretm.cmpcode AND reg.porawmstoid = pretm.porawmstoid " +
                "INNER JOIN QL_mstsupp s ON s.suppoid = pretm.suppoid " +
                "INNER JOIN QL_mstmatraw m ON m.matrawoid = pretd.matrawoid " +
                "INNER JOIN QL_mstgen ON genoid = pretrawunitoid " +
                "INNER JOIN QL_mstdivision div ON div.cmpcode = pretm.cmpcode " +
                "WHERE pretm.cmpcode = '" + cmp + "' AND pretm.pretrawmstoid IN (" + id + ") ORDER BY pretm.pretrawmstoid, pretrawdtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPRetRaw");

            report.SetDataSource(dtRpt);
            //report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            //report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "PurchaseReturnRawMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
