﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class POGenMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpogenmst
        {
            public string cmpcode { get; set; }
            public int pogenmstoid { get; set; }
            public string divgroup { get; set; }
            public string pogentype { get; set; }
            public int pogenpaytypeoid { get; set; }
            public string pogenno { get; set; }
            public DateTime pogendate { get; set; }
            public string suppname { get; set; }
            public string pogenmststatus { get; set; }
            public string pogenmstnote { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
        }

        public class trnpogendtl
        {
            public int pogendtlseq { get; set; }
            public int prgenmstoid { get; set; }
            public int prgendtloid { get; set; }
            public string prgenno { get; set; }
            public string prgendatestr { get; set; }
            public DateTime prgendate { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal prgenqty { get; set; }
            public decimal pogenqty { get; set; }
            public int pogenunitoid { get; set; }
            public string pogenunit { get; set; }
            public decimal pogenprice { get; set; }
            public decimal pogendtlamt { get; set; }
            public string pogendtldisctype { get; set; }
            public decimal pogendtldiscvalue { get; set; }
            public decimal pogendtldiscamt { get; set; }
            public decimal pogendtlnetto { get; set; }
            public string pogendtlnote { get; set; }
            public string pogendtlnote2 { get; set; }
            public string prgenarrdatereqstr { get; set; }
            public DateTime prgenarrdatereq { get; set; }
            public decimal lastpoprice { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnpogenmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpogenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            ViewBag.approvalcode = approvalcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var pogenpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.pogenpaytypeoid);
            ViewBag.pogenpaytypeoid = pogenpaytypeoid;

            //sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'";
            //var porawunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.porawunitoid);
            //ViewBag.porawunitoid = porawunitoid;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpogenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prmstoid, int suppoid,int pomstoid)
        {
            List<trnpogendtl> tbl = new List<trnpogendtl>();

            sSql = "SELECT 0 AS pogendtlseq, prd.prgenmstoid, prd.prgendtloid, prm.prgenno, prd.matgenoid, ISNULL((prd.prgenqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(pod.pogenqty - (CASE WHEN pod.pogendtlres1 IS NULL THEN ISNULL(pod.closeqty, 0) WHEN pod.pogendtlres1='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.pogenqty - CONVERT(decimal(18,2), pod.pogendtlres1)) END)), 0) FROM QL_trnpogendtl pod INNER JOIN QL_trnpogenmst pom ON pom.cmpcode=pod.cmpcode AND pom.pogenmstoid=pod.pogenmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prgendtloid=prd.prgendtloid AND pom.pogenmststatus NOT IN ('Cancel', 'Rejected') AND pod.pogenmstoid<>" + pomstoid + ")) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prgendtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0)), 0) AS prgenqty, prd.prgenunitoid AS pogenunitoid, m.matgencode, m.matgenlongdesc, m.matgenlimitqty, g2.gendesc AS pogenunit, 0.0 AS pogenqty, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.matgenoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.matgenoid ORDER BY crd.updtime DESC), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.matgenoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matgenoid=prd.matgenoid ORDER BY crd.updtime DESC), 1)) AS lastcurroid, isnull(prd.prgendtlnote,'') AS pogendtlnote, 0.0 AS pogenprice, CONVERT(VARCHAR(10), prm.prgendate, 101) AS prgendatestr, prm.prgendate, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prgenarrdatereqstr, prd.prgenarrdatereq, 0.0 AS pogendtlamt, 'A' AS pogendtldisctype, 0.0 AS pogendtldiscvalue, 0.0 AS pogendtldiscamt, 0.0 AS pogendtlnetto, '' AS pogendtlnote2 FROM QL_prgendtl prd INNER JOIN QL_prgenmst prm ON prm.cmpcode=prd.cmpcode AND prm.prgenmstoid=prd.prgenmstoid INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid /*INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1code=SUBSTRING(m.matgencode, 1, 2) AND cat1res1='gen' AND cat1res2 LIKE '%WIP%'*/ INNER JOIN QL_mstgen g2 ON prd.prgenunitoid=g2.genoid WHERE prd.cmpcode='" + CompnyCode + "' AND prd.prgenmstoid=" + prmstoid + " AND prd.prgendtlstatus='' AND prm.prgenmststatus='Approved'";

            tbl = db.Database.SqlQuery<trnpogendtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpogendtl> dtDtl)
        {
            Session["QL_trnpogendtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpogendtl"] == null)
            {
                Session["QL_trnpogendtl"] = new List<trnpogendtl>();
            }

            List<trnpogendtl> dataDtl = (List<trnpogendtl>)Session["QL_trnpogendtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnpogenmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.potype = tbl.pogentype;
            ViewBag.potaxtype = tbl.pogentaxtype;
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            public string supptaxable { get; set; }
            public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

            [HttpPost]
        public ActionResult GetSuppData(int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, (CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 11 ELSE 0 END) supptaxamt, gp.gendesc supppayment  FROM QL_mstsupp s /*INNER JOIN QL_mstcurr c ON suppcurroid=c.curroid*/ INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' and s.divgroupoid=" + divgroupoid + " AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class prgenmst
        {
            public int prgenmstoid { get; set; }
            public string prgenno { get; set; }
            public string prgendate { get; set; }
            public string prgenexpdate { get; set; }
            public string deptname { get; set; }
            public string prgenmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp, int divgroupoid)
        {
            List<prgenmst> tbl = new List<prgenmst>();
            sSql = " SELECT DISTINCT prm.prgenmstoid, prgenno, CONVERT(VARCHAR(10), prgendate, 101) AS prgendate, CONVERT(VARCHAR(10), prgenexpdate, 101) AS prgenexpdate, deptname, prgenmstnote FROM QL_prgenmst prm INNER JOIN QL_prgendtl prd ON prm.cmpcode=prd.cmpcode AND prm.prgenmstoid=prd.prgenmstoid INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" + CompnyCode + "' AND prm.divgroupoid=" + divgroupoid + " AND prm.prgenmststatus='Approved' ORDER BY prgendate DESC, prm.prgenmstoid DESC ";

            tbl = db.Database.SqlQuery<prgenmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: PORawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil) //string filter
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "pom", divgroupoid, "divgroupoid");
            sSql = "SELECT pom.cmpcode, (select gendesc from ql_mstgen where genoid=pom.divgroupoid) divgroup, pogenmstoid, pogenno, pogendate, suppname, pogenmststatus, pogenmstnote, divname FROM QL_trnpogenmst pom INNER JOIN QL_mstsupp s ON s.cmpcode='" + CompnyCode + "' AND s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND pogenmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND pogenmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND pogenmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND pogendate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND pogendate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND pogenmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<trnpogenmst> dt = db.Database.SqlQuery<trnpogenmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnpogenmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: PORawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnpogenmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnpogenmst();
                tbl.cmpcode = CompnyCode;
                tbl.pogenmstoid = ClassFunction.GenerateID("QL_trnpogenmst");
                tbl.pogendate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.pogenmststatus = "In Process";

                Session["QL_trnpogendtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnpogenmst.Find(cmp, id);

                sSql = "SELECT pod.pogendtlseq, prd.prgenmstoid, prm.prgenno, CONVERT(VARCHAR(10), prm.prgendate, 101) AS prgendatestr, prm.prgendate, pod.prgendtloid, pod.matgenoid, m.matgencode, m.matgenlongdesc, m.matgenlimitqty, pod.pogenqty, ISNULL((prd.prgenqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.pogenqty - (CASE WHEN podtl.pogendtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.pogendtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.pogenqty - CONVERT(decimal(18,2), podtl.pogendtlres1)) END)), 0) FROM QL_trnpogendtl podtl INNER JOIN QL_trnpogenmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.pogenmstoid=podtl.pogenmstoid WHERE podtl.prgendtloid=prd.prgendtloid AND podtl.pogenmstoid<>" + id + " AND pomst.pogenmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prgendtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0) AS prgenqty,  0.0 AS poqtyuse,  pod.pogenunitoid, g.gendesc AS pogenunit, pod.pogenprice, pod.pogendtlamt, pod.pogendtldisctype, pod.pogendtldiscvalue, pod.pogendtldiscamt, pod.pogendtlnetto, pod.pogendtlnote, /*ISNULL(pod.pogendtlnote2,'') pogendtlnote2,*/ ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.matgenoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.matgenoid ORDER BY crd.updtime), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.matgenoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatgenprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matgenoid=pod.matgenoid ORDER BY crd.updtime), 1)) AS lastcurroid, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prgenarrdatereqstr, prd.prgenarrdatereq FROM QL_trnpogendtl pod INNER JOIN QL_trnpogenmst pom ON pom.cmpcode=pod.cmpcode AND pom.pogenmstoid=pod.pogenmstoid INNER JOIN QL_mstmatgen m ON pod.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON pod.pogenunitoid=g.genoid INNER JOIN QL_prgendtl prd ON prd.cmpcode=pod.cmpcode AND prd.prgendtloid=pod.prgendtloid INNER JOIN QL_prgenmst prm ON prm.cmpcode=pod.cmpcode AND prm.prgenmstoid=pod.prgenmstoid WHERE pod.pogenmstoid=" + id + " AND pod.cmpcode='" + cmp + "' ORDER BY pod.pogendtlseq";
                Session["QL_trnpogendtl"] = db.Database.SqlQuery<trnpogendtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpogenmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.pogenno == null)
                tbl.pogenno = "";

            List<trnpogendtl> dtDtl = (List<trnpogendtl>)Session["QL_trnpogendtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].pogenqty <= 0)
                        {
                            ModelState.AddModelError("", "PO QTY for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }

                        if (dtDtl[i].pogenprice <= 0)
                        {
                            ModelState.AddModelError("", "PO Price for Material " + dtDtl[i].matgenlongdesc + " must be more than 0");
                        }

                        sSql = "SELECT ISNULL((prd.prgenqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.pogenqty - (CASE WHEN podtl.pogendtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.pogendtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.pogenqty - CONVERT(decimal(18,2), podtl.pogendtlres1)) END)), 0) FROM QL_trnpogendtl podtl INNER JOIN QL_trnpogenmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.pogenmstoid=podtl.pogenmstoid WHERE podtl.cmpcode=prd.cmpcode AND podtl.prgenmstoid=prd.prgenmstoid AND  podtl.prgendtloid=prd.prgendtloid AND podtl.pogenmstoid<>" + tbl.pogenmstoid + " AND pomst.pogenmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='gen' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prgendtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0.0) AS prgenqty FROM QL_prgendtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prgendtloid=" + dtDtl[i].prgendtloid + "";
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].prgenqty)
                            dtDtl[i].prgenqty = dQty;
                        if (dQty < dtDtl[i].pogenqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PR Qty!");

                        //sSql = "SELECT COUNT(-1) FROM QL_prrawdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prrawmstoid=" + dtDtl[i].prrawmstoid + " AND prd.prrawdtloid=" + dtDtl[i].prrawdtloid + " AND prd.prrawdtlstatus='COMPLETE'";
                        //var CheckDataExists = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (CheckDataExists > 0 )
                        //{
                        //    ModelState.AddModelError("", "There are some PR detail data has been closed by another user. You cannot use this PR detail data anymore!");
                        //}
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.pogenmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnpogenmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpogenmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpogendtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnpogenmst.Find(tbl.cmpcode, tbl.pogenmstoid) != null)
                                tbl.pogenmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pogendate);
                            //tbl.divoid = divoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.pogenratetoidrchar = "0";
                            tbl.pogenratetousdchar = "0";
                            tbl.pogenrate2toidrchar = "0";
                            tbl.pogenrate2tousdchar = "0";
                            tbl.pogentaxtype_inex = "0";
                            //tbl.pogenmststatus_onpib = "";
                            db.QL_trnpogenmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.pogenmstoid + " WHERE tablename='QL_trnpogenmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.pogenratetoidrchar = "0";
                            tbl.pogenratetousdchar = "0";
                            tbl.pogenrate2toidrchar = "0";
                            tbl.pogenrate2tousdchar = "0";
                            tbl.pogentaxtype_inex = "0";
                            //tbl.pogenmststatus_onpib = "";
                            //--
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_prgendtl SET prgendtlstatus='', prgendtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND prgendtloid IN (SELECT prgendtloid FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_prgenmst SET prgenmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prgenmstoid IN (SELECT DISTINCT prgenmstoid FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpogendtl.Where(a => a.pogenmstoid == tbl.pogenmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpogendtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpogendtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpogendtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.pogendtloid = dtloid++;
                            tbldtl.pogenmstoid = tbl.pogenmstoid;
                            tbldtl.pogendtlseq = i + 1;
                            tbldtl.prgenmstoid = dtDtl[i].prgenmstoid;
                            tbldtl.prgendtloid = dtDtl[i].prgendtloid;
                            tbldtl.matgenoid = dtDtl[i].matgenoid;
                            tbldtl.pogenqty = dtDtl[i].pogenqty;
                            tbldtl.pogenunitoid = dtDtl[i].pogenunitoid;
                            tbldtl.pogenprice = dtDtl[i].pogenprice;
                            tbldtl.pogendtlamt = dtDtl[i].pogendtlamt;
                            tbldtl.pogendtldisctype = dtDtl[i].pogendtldisctype;
                            tbldtl.pogendtldiscvalue = dtDtl[i].pogendtldiscvalue;
                            tbldtl.pogendtldiscamt = dtDtl[i].pogendtldiscamt;
                            tbldtl.pogendtlnetto = dtDtl[i].pogendtlnetto;
                            tbldtl.pogendtlnote = dtDtl[i].pogendtlnote == null ? "" : dtDtl[i].pogendtlnote;
                            //tbldtl.pogendtlnote2 = dtDtl[i].pogendtlnote2;
                            tbldtl.pogendtlstatus = "";
                            //tbldtl.pogendtlstatus_onpib = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnpogendtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].pogenqty >= dtDtl[i].prgenqty)
                            {
                                sSql = "UPDATE QL_prgendtl SET prgendtlstatus='COMPLETE', prgendtlres1='" + dtloid + "' WHERE cmpcode='" + tbl.cmpcode + "' AND prgendtloid=" + dtDtl[i].prgendtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_prgenmst SET prgenmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND prgenmstoid=" + dtDtl[i].prgenmstoid + " AND (SELECT COUNT(prgendtloid) FROM QL_prgendtl WHERE cmpcode='" + tbl.cmpcode + "' AND prgendtlstatus='' AND prgenmstoid=" + dtDtl[i].prgenmstoid + " AND prgendtloid<>" + +dtDtl[i].prgendtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_TRNPOGENDTL'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.pogenmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-gen" + tbl.pogenmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnpogenmst";
                                tblApp.oid = tbl.pogenmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        //if (tbl.pogenmststatus.ToUpper() == "In Approval")
                        //{
                        //    var dt = null;
                        //    //Dim dt As DataTable = Session("AppPerson")
                        //    for (int i = 0; i < dtLastHdrData.Count(); i++)
                        //    {
                        //        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'PO-RAW" & pogenmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & pogendate.Text & "', 'New', 'QL_trnpogenmst', " & pogenmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();
                        //    }
                        //    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.pogenmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            tbl.pogenmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpogenmst tbl = db.QL_trnpogenmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_prgendtl SET prgendtlstatus='', prgendtlres1='', prclosingqty=0.0 WHERE cmpcode='" + tbl.cmpcode + "' AND prgendtloid IN (SELECT prgendtloid FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prgenmst SET prgenmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prgenmstoid IN (SELECT DISTINCT prgenmstoid FROM QL_trnpogenmst WHERE cmpcode='" + tbl.cmpcode + "' AND pogenmstoid=" + tbl.pogenmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = GetQueryInsertStatus_Multi(Session("UserID"), DDLBusUnit.SelectedValue, "QL_trnpogenmst", pogenmstoid.Text, "Open", "pogenmstoid", "QL_prrawmst", "prrawmstoid", "Open karena PO dihapus");
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnpogendtl.Where(a => a.pogenmstoid == id && a.cmpcode == cmp);
                        db.QL_trnpogendtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpogenmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string supptype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnpogenmst.Find(cmp, id);
            if (tbl == null)
                return null;

            if (supptype == "LOCAL")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_TrnEn.rpt"));

            //sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.pogenmstoid [Oid], CONVERT(VARCHAR(10), pom.pogenmstoid) AS [Draft No.], pom.pogenno [PO No.], pom.pogendate [PO Date], pom.pogenmstnote [Header Note], pom.pogenmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.pogenmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.pogenmstdiscamt, 0) AS [Disc Amount], pom.pogentotalnetto [Total Netto], pom.pogentaxtype [Tax Type], CONVERT(VARCHAR(10), pom.pogentaxamt) AS [Tax Amount], ISNULL(pom.pogenvat, 0) AS [VAT], ISNULL(pom.pogendeliverycost, 0) AS [Delivery Cost], ISNULL(pom.pogenothercost, 0) AS [Other Cost], pom.pogengrandtotalamt [Grand Total], pod.pogendtloid [DtlOid], ISNULL((SELECT m2.matgendtl2code FROM QL_mstmatgendtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matgenoid=m2.matgenoid), m.matgencode) AS [Code], ISNULL((SELECT m2.matgendtl2name FROM QL_mstmatgendtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matgenoid=m2.matgenoid), m.matgenlongdesc) AS [Material], prm.prgenno AS [PR No.], prm.prgendate AS [PR Date], pod.pogenqty AS [Qty], g2.gendesc AS [Unit], pod.pogenprice AS [Price], pod.pogendtlamt AS [Amount], ISNULL(pod.pogendtldiscamt, 0) AS [Detail Disc.], pod.pogendtlnetto AS [Netto], pod.pogendtlnote AS [Note], pom.pogentype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.pogensuppref AS [SuppReff], pod.pogendtldisctype AS [DtlDiscType], ISNULL(pod.pogendtldiscvalue,0) AS [DtlDiscValue], pod.pogendtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prgenarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpogenmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.pogenpaytypeoid INNER JOIN QL_trnpogendtl pod ON pod.cmpcode=pom.cmpcode AND pod.pogenmstoid=pom.pogenmstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pod.matgenoid LEFT JOIN QL_prgenmst prm ON prm.cmpcode=pod.cmpcode AND prm.prgenmstoid=pod.prgenmstoid LEFT JOIN QL_prgendtl prd ON prd.cmpcode=pod.cmpcode AND prd.prgenmstoid=pod.prgenmstoid AND prd.prgendtloid=pod.prgendtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.pogenunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.pogenmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.pogenmstoid, pod.pogendtlseq ";
            sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.pogenmstoid [Oid], CONVERT(VARCHAR(10), pom.pogenmstoid) AS [Draft No.], pom.pogenno [PO No.], pom.pogendate [PO Date], pom.pogenmstnote [Header Note], pom.pogenmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.pogenmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.pogenmstdiscamt, 0) AS [Disc Amount], pom.pogentotalnetto [Total Netto], pom.pogentaxtype [Tax Type], CONVERT(VARCHAR(10), pom.pogentaxamt) AS [Tax Amount], ISNULL(pom.pogenvat, 0) AS [VAT], ISNULL(pom.pogendeliverycost, 0) AS [Delivery Cost], ISNULL(pom.pogenothercost, 0) AS [Other Cost], pom.pogengrandtotalamt [Grand Total], pod.pogendtloid [DtlOid], '' AS [Code], '' AS [Material], prm.prgenno AS [PR No.], prm.prgendate AS [PR Date], pod.pogenqty AS [Qty], g2.gendesc AS [Unit], pod.pogenprice AS [Price], pod.pogendtlamt AS [Amount], ISNULL(pod.pogendtldiscamt, 0) AS [Detail Disc.], pod.pogendtlnetto AS [Netto], pod.pogendtlnote AS [Note], pom.pogentype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.pogensuppref AS [SuppReff], pod.pogendtldisctype AS [DtlDiscType], ISNULL(pod.pogendtldiscvalue,0) AS [DtlDiscValue], pod.pogendtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prgenarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnpogenmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.pogenpaytypeoid INNER JOIN QL_trnpogendtl pod ON pod.cmpcode=pom.cmpcode AND pod.pogenmstoid=pom.pogenmstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pod.matgenoid LEFT JOIN QL_prgenmst prm ON prm.cmpcode=pod.cmpcode AND prm.prgenmstoid=pod.prgenmstoid LEFT JOIN QL_prgendtl prd ON prd.cmpcode=pod.cmpcode AND prd.prgenmstoid=pod.prgenmstoid AND prd.prgendtloid=pod.prgendtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.pogenunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.pogenmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.pogenmstoid, pod.pogendtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPORaw");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "POGenMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
