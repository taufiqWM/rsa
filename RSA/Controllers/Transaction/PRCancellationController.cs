﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class PRCancellationController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prmst
        {
            public string cmpcode { get; set; }
            public int prmstoid { get; set; }
            public int deptoid { get; set; }
            public string prno { get; set; }
            public string sType { get; set; }
            public string prdate { get; set; }
            public string prexpdate { get; set; }
            public string prmststatus { get; set; }
            public string prmstnote { get; set; }
            public string deptname { get; set; }
        }

        public class prdtl
        {
            public string cmpcode { get; set; }
            public int prdtloid { get; set; }
            public int prmstoid { get; set; }
            public string prtype { get; set; }
            public int prdtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal prqty { get; set; }
            public string prunit { get; set; }
            public string prdtlnote { get; set; }
            public string prdtlstatus { get; set; }
            public string prmststatus { get; set; }
            public string prarrdatereq { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp, string sType)
        {
            List<prmst> tbl = new List<prmst>();

        sSql = "SELECT p.cmpcode, pr" + sType + "mstoid AS prmstoid, pr" + sType + "no AS prno,'" + sType + "' AS sType , CONVERT(VARCHAR(10), pr" + sType + "date, 101) AS prdate, p.deptoid, deptname, CONVERT(VARCHAR(10), pr" + sType + "expdate, 101) AS prexpdate, pr" + sType + "mststatus AS prmststatus, pr" + sType + "mstnote AS prmstnote FROM QL_pr" + sType + "mst p INNER JOIN QL_mstdept d ON p.deptoid=d.deptoid WHERE p.cmpcode='" + cmp + "' AND (pr" + sType + "mststatus='Approved'";

            if (sType.ToUpper() == "SUBCON")
            {
                sSql += " AND prsubconmstoid NOT IN (SELECT prsubconmstoid FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.cmpcode='" + cmp + "' AND posubconmststatus NOT IN ('Rejected', 'Cancel')))";
            }
            else
            {
                sSql += " AND pr" + sType + "mstoid NOT IN (SELECT pr" + sType + "mstoid FROM QL_trnpo" + sType + "dtl pod INNER JOIN QL_trnpo" + sType + "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" + sType + "mstoid=pod.po" + sType + "mstoid WHERE pod.cmpcode='" + cmp + "' AND po" + sType + "mststatus NOT IN ('Rejected', 'Cancel')))" ;
            }
            sSql += " ORDER BY pr" + sType + "date DESC, pr" + sType + "mstoid DESC";


          
            //if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
            //    sSql += " AND s.createuser='" + Session["UserID"].ToString() + "'";
            //sSql += " ORDER BY so" + sType + "date DESC, somstoid DESC";

            tbl = db.Database.SqlQuery<prmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<prdtl> tbl = new List<prdtl>();
            if (sType.ToUpper() == "RAW")
                sSql = "SELECT prd.prrawdtloid AS prdtloid, prd.prrawdtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prarrdatereq, prd.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, prd.prrawqty AS prqty, g.gendesc AS prunit, prd.prrawdtlnote AS prdtlnote, prd.prrawdtlstatus AS prdtlstatus FROM QL_prrawdtl prd INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON prd.prrawunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prrawmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "GEN")
                sSql = "SELECT prd.prgendtloid AS prdtloid, prd.prgendtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prarrdatereq, prd.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, prd.prgenqty AS prqty, g.gendesc AS prunit, prd.prgendtlnote AS prdtlnote, prd.prgendtlstatus AS prdtlstatus FROM QL_prgendtl prd INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON prd.prgenunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prgenmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "SP")
                sSql = "SELECT prd.prspdtloid AS prdtloid, prd.prspdtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prarrdatereq, prd.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, prd.prspqty AS prqty, g.gendesc AS prunit, prd.prspdtlnote AS prdtlnote, prd.prspdtlstatus AS prdtlstatus FROM QL_prspdtl prd INNER JOIN QL_mstsparepart m ON prd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON prd.prspunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prspmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "ASSET")
                sSql = "SELECT prd.prassetdtloid AS prdtloid, prd.prassetdtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prarrdatereq, prd.prassetrefoid AS matoid, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS matcode, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS matlongdesc, prd.prassetqty AS prqty, g.gendesc AS prunit, prd.prassetdtlnote AS prdtlnote, prd.prassetdtlstatus AS prdtlstatus FROM QL_prassetdtl prd INNER JOIN QL_mstgen g ON prd.prassetunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prassetmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "WIP")
                sSql = "SELECT prd.prwipdtloid AS prdtloid, prd.prwipdtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prwiparrdatereq, 101) AS prarrdatereq, prd.matwipoid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, prd.prwipqty AS prqty, g.gendesc AS prunit, prd.prwipdtlnote AS prdtlnote, prd.prwipdtlstatus AS prdtlstatus FROM QL_prwipdtl prd INNER JOIN QL_mstcat3 c3 ON cat3oid=matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON prd.prwipunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prwipmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "SAWN")
                sSql = "SELECT prd.prsawndtloid AS prdtloid, prd.prsawndtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prsawnarrdatereq, 101) AS prarrdatereq, prd.matsawnoid AS matoid, (cat1code + '.' + cat2code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) AS matlongdesc, prd.prsawnqty AS prqty, g.gendesc AS prunit, prd.prsawndtlnote AS prdtlnote, prd.prsawndtlstatus AS prdtlstatus FROM QL_prsawndtl prd INNER JOIN QL_mstcat2 c2 ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid INNER JOIN QL_mstgen g ON prd.prsawnunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prsawnmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "ITEM")
                sSql = "SELECT prd.pritemdtloid AS prdtloid, prd.pritemdtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS prarrdatereq, prd.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, prd.pritemqty AS prqty, g.gendesc AS prunit, prd.pritemdtlnote AS prdtlnote, prd.pritemdtlstatus AS prdtlstatus FROM QL_pritemdtl prd INNER JOIN QL_mstitem m ON prd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON prd.pritemunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.pritemmstoid=" + iOid + " ORDER BY prdtlseq";
            else
                sSql = "SELECT prd.prsubcondtloid AS prdtloid, prd.prsubcondtlseq AS prdtlseq, CONVERT(VARCHAR(10), prd.prsubconarrdatereq, 101) AS prarrdatereq, prd.matrefoid AS matoid, (CASE prm.prsubconref WHEN 'Raw' THEN (SELECT r.matrawcode FROM QL_mstmatraw r WHERE r.matrawoid=prd.matrefoid) WHEN 'General' THEN (SELECT g.matgencode FROM QL_mstmatgen g WHERE g.matgenoid=prd.matrefoid) WHEN 'Spare Part' THEN (SELECT s.sparepartcode FROM QL_mstsparepart s WHERE s.sparepartoid=prd.matrefoid) ELSE '' END) matcode, (CASE prm.prsubconref WHEN 'Raw' THEN (SELECT r.matrawlongdesc FROM QL_mstmatraw r WHERE r.matrawoid=prd.matrefoid) WHEN 'General' THEN (SELECT g.matgenlongdesc FROM QL_mstmatgen g WHERE g.matgenoid=prd.matrefoid) WHEN 'Spare Part' THEN (SELECT s.sparepartlongdesc FROM QL_mstsparepart s WHERE s.sparepartoid=prd.matrefoid) ELSE '' END) matlongdesc, prd.prsubconqty AS prqty, g.gendesc AS prunit, prd.prsubcondtlnote AS prdtlnote, prd.prsubcondtlstatus AS prdtlstatus FROM QL_prsubcondtl prd INNER JOIN QL_prsubconmst prm ON prm.prsubconmstoid=prd.prsubconmstoid INNER JOIN QL_mstgen g ON prd.prsubconunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prsubconmstoid=" + iOid + " ORDER BY prdtlseq";

            tbl = db.Database.SqlQuery<prdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<prdtl> dtDtl)
        {
            Session["QL_prdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            List<prdtl> dtDtl = (List<prdtl>)Session["QL_prdtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].prmstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    sSql = "SELECT COUNT(*) FROM QL_pr" + dtDtl[0].prtype + "mst WHERE pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND pr" + dtDtl[0].prtype + "mststatus='Cancel'";
                    var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    if (iCheck > 0)
                    {
                        ModelState.AddModelError("", "This data has been canceled by another user. Please CANCEL this transaction and try again!");
                    }
                    else
                    {
                        if (dtDtl[0].prtype.ToUpper() == "SUBCON")
                        {
                            sSql = "SELECT COUNT(*) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.prsubconmstoid=" + +dtDtl[0].prmstoid + " AND posubconmststatus NOT IN ('Rejected', 'Cancel')";
                        }
                        else
                        {
                            sSql = "SELECT COUNT(*) FROM QL_trnpo" + dtDtl[0].prtype + "dtl pod INNER JOIN QL_trnpo" + dtDtl[0].prtype + "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" + dtDtl[0].prtype + "mstoid=pod.po" + dtDtl[0].prtype + "mstoid WHERE pod.pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND po" + dtDtl[0].prtype + "mststatus NOT IN ('Rejected', 'Cancel')";
                        }
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "This data has been created PO by another user. Please CANCEL this transaction and try again!");
                        }

                        //        If ddlType.SelectedValue = "subcon" Then
                        //    sSql = "SELECT COUNT(*) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.prsubconmstoid=" & prmstoid.Text & " AND posubconmststatus NOT IN ('Rejected', 'Cancel')"
                        //Else
                        //    sSql = "SELECT COUNT(*) FROM QL_trnpo" & ddlType.SelectedValue & "dtl pod INNER JOIN QL_trnpo" & ddlType.SelectedValue & "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" & ddlType.SelectedValue & "mstoid=pod.po" & ddlType.SelectedValue & "mstoid WHERE pod.pr" & ddlType.SelectedValue & "mstoid=" & prmstoid.Text & " AND po" & ddlType.SelectedValue & "mststatus NOT IN ('Rejected', 'Cancel')"
                        //End If
                        //If CheckDataExists(sSql)Then
                        //    showMessage("This data has been created PO by another user. Please CANCEL this transaction and try again!", 2)
                        //    Exit Sub
                        //End If
                        //sSql = "SELECT COUNT(*) FROM QL_trndo" + dtDtl[0].sotype + "dtl dod INNER JOIN QL_trndo" + dtDtl[0].sotype + "mst dom ON dod.do" + dtDtl[0].sotype + "mstoid=dom.do" + dtDtl[0].sotype + "mstoid WHERE so" + dtDtl[0].sotype + "mstoid=" + dtDtl[0].somstoid + " AND dom.do" + dtDtl[0].sotype + "mststatus<>'Cancel' ";
                        //var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (iCheck2 > 0)
                        //{
                        //    ModelState.AddModelError("", "This data has been created DO by another user. Please CANCEL this transaction and try again!");
                        //}
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            sSql = "UPDATE QL_pr" + dtDtl[0].prtype + "mst SET pr" + dtDtl[0].prtype + "mststatus='Cancel', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            if (dtDtl[0].prmststatus == "In Approval")
                            {
                                sSql = "UPDATE QL_approval SET statusrequest='Cancel', event='Cancel' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND tablename='QL_pr" + dtDtl[0].prtype + "mst' AND oid=" + dtDtl[0].prmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                           
                            objTrans.Commit();
                            Session["QL_prdtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}