﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class TransformPulpController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();

        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string HRISServer = System.Configuration.ConfigurationManager.AppSettings["HRISServer"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public TransformPulpController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class usagemst
        {
            public string cmpcode { get; set; }
            public int transformpulpmstoid { get; set; }
            public string divgroup { get; set; }
            public string transformpulpno { get; set; }
            public DateTime transformpulpdate { get; set; }
            public string transformpulpmststatus { get; set; }
            public string transformpulpmstnote { get; set; }
         
        }

     

     
        public static string GetQueryInitDDLWH(string cmpcode, string action)
        {
            var cmp = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            var res = "SELECT * FROM QL_mstgen WHERE cmpcode='" + cmp + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            if (action == "New Data")
                res += " AND activeflag='ACTIVE'";
            res += " ORDER BY gendesc";
            return res;
        }

        public class COA
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }
        private void InitDDL(QL_trntransformpulpmst tbl, string action)
        {
            var divisi = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupoid).FirstOrDefault();

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            var cmp = cmpcode.First().Value;
            if (!string.IsNullOrEmpty(tbl.cmpcode) && cmpcode.Count() > 0)
                cmp = tbl.cmpcode;

            var transformpulpwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(GetQueryInitDDLWH(cmp, action)).ToList(), "genoid", "gendesc", tbl.transformpulpwhoid);
            ViewBag.transformpulpwhoid = transformpulpwhoid;

            var coalist = ClassFunction.GetDataAcctgOid("VAR_TRANS_BTKL", CompnyCode);
            List<COA> getCoa = new List<COA>();
            sSql = "select 0 acctgoid, '' acctgcode, 'NONE' acctgdesc UNION ALL SELECT acctgoid, acctgcode, acctgdesc from ql_mstacctg where cmpcode='" + CompnyCode + "' and acctgoid in (" + coalist + ")";
            ViewBag.btklacctgoid = new SelectList(db.Database.SqlQuery<COA>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.btklacctgoid);
        }

        private string GenerateNo(string cmp)
        {
            var usageno = "";
            if (!string.IsNullOrEmpty(cmp))
            {
                string sNo = "TFMPULP-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformpulpno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransformpulpmst WHERE cmpcode='" + cmp + "' AND transformpulpno LIKE '%" + sNo + "%'";
                usageno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return usageno;
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode, string action)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = GetQueryInitDDLWH(cmpcode, action);
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public class transformdtl1
        {
            public int transformdtl1seq { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public int matrawunitoid { get; set; }
            public string matrawunit { get; set; }
            public string transformpulpdtlnote { get; set; }
            public decimal transformpulpdtlvalue { get; set; }
            public decimal transformpulpqty { get; set; }
            public decimal stock { get; set; }
        }

        [HttpPost]
        public ActionResult GetMatData(string cmp, int divgroupoid, int whoid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl1> tbl = new List<transformdtl1>();
            try
            {
                sSql = "SELECT DISTINCT 0 transformdtl1seq, 0.0 transformpulpqty,con.refname mattype ,con.refoid matrawoid, matrawcode,i.matrawlongdesc, sum(qtyin - qtyout) stock, i.matrawunitoid, (select x.gendesc from ql_mstgen x where genoid=i.matrawunitoid) matrawunit,'' transformpulpdtlnote from QL_conmat con inner join QL_mstmatraw i on i.matrawoid = con.refoid and con.refname = 'RAW MATERIAL' WHERE  i.divgroupoid=" + divgroupoid+ " AND i.matrawres3='Waste' AND con.cmpcode='" + cmp + "' and con.mtrwhoid="+whoid+" group by  con.refoid, matrawlongdesc, matrawcode, matrawunitoid, refname, con.mtrwhoid having  sum(qtyin-qtyout)>0  ORDER BY matrawlongdesc";
                tbl = db.Database.SqlQuery<transformdtl1>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].transformdtl1seq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch(Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl1> dtl1 = new List<transformdtl1>();

            try
            {
                sSql = "SELECT 0 transformdtl1seq,* from QL_trntransformpulpdtl u WHERE u.cmpcode='" + cmp + "' and u.transformpulpmstoid=" + oid;
                dtl1 = db.Database.SqlQuery<transformdtl1>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].transformdtl1seq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class transformdtl2
        {
            public int transformdtl2seq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string transformpulpdtl2note { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public decimal transformpulpdtl2value { get; set; }
            public decimal transformpulpdtl2qty { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int divgroupoid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl2> tbl = new List<transformdtl2>();

            try
            {
                sSql = "SELECT 0 transformdtl2seq, itemoid, 0.0 transformpulpdtl2qty,itemcode, itemlongdesc, itemunitoid, (select x.gendesc from ql_mstgen x where x.genoid=i.itemunitoid) itemunit, '' transformpulpdtl2note FROM QL_mstitem i  WHERE i.cmpcode='" + cmp + "'  AND i.tipefg='R-PULP' and i.divgroupoid=" + divgroupoid + "";
                tbl = db.Database.SqlQuery<transformdtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < tbl.Count(); i++)
                {
                    no = no + 1; ;
                    tbl[i].transformdtl2seq = no;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        [HttpPost]
        public ActionResult FillDetailData2(string cmp, string oid)
        {
            var result = "";
            JsonResult js = null;
            List<transformdtl2> dtl1 = new List<transformdtl2>();

            try
            {
                sSql = "SELECT 0 transformdtl2seq, * FROM QL_trntransformpulpdtl2 u WHERE u.cmpcode='" + cmp + "' and transformpulpmstoid='" + oid+"'";
                dtl1 = db.Database.SqlQuery<transformdtl2>(sSql).ToList();
                var no = 0;
                for (var i = 0; i < dtl1.Count(); i++)
                {
                    no = no + 1; ;
                    dtl1[i].transformdtl2seq = no;
                }
                if (dtl1.Count == 0)
                    result = "Data Not Found.";

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl1 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: RawMaterialUsage
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "usagem", divgroupoid, "divgroupoid");
            sSql = "SELECT usagem.*,(select gendesc from ql_mstgen where genoid=usagem.divgroupoid) divgroup FROM QL_trntransformpulpmst usagem INNER JOIN QL_mstgen g1 ON g1.genoid=usagem.transformpulpwhoid INNER JOIN QL_mstdivision div ON div.cmpcode=usagem.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "usagem.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "usagem.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND transformpulpmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND transformpulpmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND transformpulpmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND transformpulpdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND transformpulpdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND transformpulpmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND usagem.createuser='" + Session["UserID"].ToString() + "'";

            List<usagemst> dt = db.Database.SqlQuery<usagemst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trntransformpulpmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }


        // GET: RawMaterialUsage/PrintReport/id/cmp?type1=&type2=
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            ids = ClassFunction.Left(ids, ids.Length - 1);
            ids = ids.Replace("x", ",");

            var rptname = "rptUsageByWO";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT mum.usagemstoid AS [Oid], CONVERT(VARCHAR(10), mum.usagemstoid) AS [Draft No.], usageno AS [Usage No.], usagedate AS [Usage Date], d.routename AS [Department], '' AS [Request No.], gwh.gendesc AS [Warehouse], usagemstnote AS [Header Note], usagemststatus AS [Status], usagereftype AS [Type], matrawcode AS [Code], matrawlongdesc AS [Description], usageqty AS [Qty], gunit.gendesc AS [Unit], usagedtlnote AS [Detail Note], divname AS [BU Name], divaddress AS [BU Address], gcity.gendesc AS [BU City], gprov.gendesc AS [BU Province], gcont.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Telp. 1], ISNULL(divphone2, '') AS [BU Telp. 2], ISNULL(divfax1, '') AS [BU Fax 1], ISNULL(divfax2, '') AS [BU Fax 2], ISNULL(divemail, '') AS [BU Email], ISNULL(divpostcode, '') AS [BU Post Code], '' AS [Division], som.wono [SO No.], ''  [Person Name], ''  [NIP] FROM QL_trnusagemst mum INNER JOIN QL_trnusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagemstoid=mum.usagemstoid INNER JOIN QL_trnwomst som ON som.cmpcode=mum.cmpcode AND mum.womstoid=som.womstoid INNER JOIN QL_mstroute d ON d.cmpcode=mum.cmpcode AND d.routeoid=mum.routeoid INNER JOIN QL_mstmatraw m ON m.matrawoid=mud.usagerefoid INNER JOIN QL_mstgen gwh ON gwh.genoid=usagewhoid INNER JOIN QL_mstgen gunit ON gunit.genoid=usageunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode INNER JOIN QL_mstgen gcity ON gcity.genoid=divcityoid INNER JOIN QL_mstgen gprov ON gprov.genoid=CAST(gcity.genother1 AS INT) INNER JOIN QL_mstgen gcont ON gcont.genoid=CAST(gcity.genother2 AS INT) WHERE mum.usagemstoid IN (" + ids + ") ORDER BY mum.usagemstoid, usagedtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", Session["UserName"].ToString());
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "MaterialUsagePrintOut.pdf");
        }

        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trntransformpulpmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trntransformpulpmst();
                tbl.transformpulpdate = ClassFunction.GetServerTime();
                tbl.transformpulpmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trntransformpulpmst.Find(cmp, id);
              
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

      
        // POST: POService/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trntransformpulpmst tbl, List<transformdtl1> dtl1, List<transformdtl2> dtl2, string action, string closing, string batch)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var iStockValOid = ClassFunction.GenerateID("QL_stockvalue");
            var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
            var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
            var msg = ""; var result = "failed"; var hdrid = "0";decimal stock = 0;  
            int iConOid = ClassFunction.GenerateID("QL_CONMAT");
            decimal totalvaluemat1 = 0;
            decimal valueperfg = 0;
            decimal qtyfg = 0;
            if (string.IsNullOrEmpty(tbl.cmpcode))
                msg += "Please select BUSINESS UNIT!<br />";

            if (string.IsNullOrEmpty(tbl.transformpulpmstnote))
                tbl.transformpulpmstnote = "";
            
       
            if (tbl.transformpulpwhoid == 0)
                msg += "Please select WAREHOUSE field!<br />";

            if (dtl1 == null)
                msg += "Please fill detail Material!<br />";
            else if (dtl1.Count <= 0)
                msg += "Please fill detail Material!<br />";
            else
            {
                if (dtl1.Count > 0)
                {
                    for (int i = 0; i < dtl1.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl1[i].transformpulpdtlnote))
                        {
                            dtl1[i].transformpulpdtlnote = "";
                        }
                        if (dtl1[i].transformpulpqty <= 0)
                        {
                            msg += "Transform Material Must be more than 0 !<BR>";
                        }
                        else
                        {
                            sSql = "select sum(qtyin-qtyout) from ql_conmat where refoid=" + dtl1[i].matrawoid + " and refname='RAW MATERIAL'";
                            stock = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dtl1[i].stock != stock)
                            {
                                dtl1[i].stock = stock;
                            }
                            if (dtl1[i].stock < dtl1[i].transformpulpqty)
                            {
                                msg += "Usage Transform Must be equal or less than Stock QTY !<BR>";
                            }
                        }
                        dtl1[i].transformpulpdtlvalue = ClassFunction.GetStockValueIDR(db, CompnyCode, "", "RAW MATERIAL", dtl1[i].matrawoid);
                        totalvaluemat1 += dtl1[i].transformpulpdtlvalue*dtl1[i].transformpulpqty;
                    }
                }
            }

            if (dtl2 == null)
                msg += "Please fill detail FG data!<br />";
            else if (dtl2.Count <= 0)
                msg += "Please fill detail FG data!<br />";
            else
            {
                if (dtl2.Count > 0)
                {
                    for (int i = 0; i < dtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtl2[i].transformpulpdtl2note))
                        {
                            dtl2[i].transformpulpdtl2note = "";
                        }
                        if(dtl2[i].transformpulpdtl2qty <= 0)
                        {
                            msg += "QTY FG Must be more than 0 !<BR>";
                        }
                        else
                        {
                            qtyfg += dtl2[i].transformpulpdtl2qty;
                        }
                        
                    }
                }
            }
            valueperfg = Math.Round(((totalvaluemat1+tbl.btklvalue) / qtyfg),2);
            tbl.transformpulptotalvalue = totalvaluemat1;
            if (tbl.btklvalue > 0)
            {
                if(string.IsNullOrEmpty(tbl.btklnote))
                {
                    msg += "Silahkan Isi Keterangan BTKL !<BR>";
                }
                if (tbl.btklacctgoid == 0)
                {
                    msg += "Silahkan Pilih Akun BTKL !<BR>";
                }
            }
            if (tbl.btklvalue < 0)
            {
                msg += "Nominal BTKL Tidak Boleh Kurang dari 0!<BR>";
            }
            tbl.transformpulpnetto = tbl.transformpulptotalvalue + tbl.btklvalue;
            int iAcctgoidRM = 0;int iAcctgoidFG = 0;

            tbl.transformpulpno = "";
            if (tbl.transformpulpmststatus == "Post")
            {
                tbl.transformpulpno = GenerateNo(CompnyCode);


                var divisi = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupoid).FirstOrDefault();
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_FG") + "<BR>";
                }

                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM" , tbl.cmpcode, tbl.divgroupoid));
              
                iAcctgoidFG = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid));
              
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.transformpulpdate);
               
                

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                      
                        if (action == "New Data")
                        {
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trntransformpulpmst.Add(tbl);
                            db.SaveChanges();

                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            var trndtl = db.QL_trntransformpulpdtl.Where(a => a.transformpulpmstoid == tbl.transformpulpmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trntransformpulpdtl.RemoveRange(trndtl);
                            var trndtl2 = db.QL_trntransformpulpdtl2.Where(a => a.transformpulpmstoid == tbl.transformpulpmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trntransformpulpdtl2.RemoveRange(trndtl2);
                        }

                        QL_trntransformpulpdtl tbldtl;

                        for (int i = 0; i < dtl1.Count(); i++)
                        {
                            tbldtl = new QL_trntransformpulpdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.transformpulpmstoid = tbl.transformpulpmstoid;
                            tbldtl.matrawoid = dtl1[i].matrawoid;
                            tbldtl.matrawcode = dtl1[i].matrawcode;
                            tbldtl.matrawlongdesc = dtl1[i].matrawlongdesc;
                            tbldtl.transformpulpqty = dtl1[i].transformpulpqty;
                            tbldtl.matrawunitoid = dtl1[i].matrawunitoid;
                            tbldtl.matrawunit = dtl1[i].matrawunit;
                            tbldtl.transformpulpdtlnote = dtl1[i].transformpulpdtlnote;
                            tbldtl.transformpulpdtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.transformpulpdtlvalue = dtl1[i].transformpulpdtlvalue;
                            tbldtl.transformpulpdtlvalueidr = dtl1[i].transformpulpdtlvalue;
                            tbldtl.transformpulpdtlvalueusd = 0;
                            db.QL_trntransformpulpdtl.Add(tbldtl);
                            db.SaveChanges();
                         
                         
                            if (tbl.transformpulpmststatus == "Post")
                            {
                                // STOCK OUT
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "TRFMP", "QL_trntransformpulpmst", tbl.transformpulpmstoid, tbldtl.matrawoid, "RAW MATERIAL", tbl.transformpulpwhoid, tbldtl.transformpulpqty * -1, "Transform RAW MATERIAL", tbl.transformpulpno, tbl.upduser, "", "", tbldtl.transformpulpqty*tbldtl.transformpulpdtlvalueidr, tbldtl.transformpulpdtlvalueusd*tbldtl.transformpulpqty, 0, null, tbldtl.transformpulpdtloid, tbl.divgroupoid, ""));
                                db.SaveChanges();
                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(tbldtl.transformpulpqty, tbldtl.transformpulpdtlvalueidr, tbldtl.transformpulpdtlvalueusd, "QL_trntransformpulpmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl1[i].matrawoid, "RAW MATERIAL");
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(tbldtl.transformpulpqty, tbldtl.transformpulpdtlvalueidr, tbldtl.transformpulpdtlvalueusd, "QL_trnusagemst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl1[i].matrawoid, "RAW MATERIAL", iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                         
                        }
                        QL_trntransformpulpdtl2 tbldtl2;

                        for (int i = 0; i < dtl2.Count(); i++)
                        {
                            tbldtl2 = new QL_trntransformpulpdtl2();
                            tbldtl2.cmpcode = tbl.cmpcode;
                            tbldtl2.divgroupoid = tbl.divgroupoid;
                            tbldtl2.transformpulpmstoid = tbl.transformpulpmstoid;
                            tbldtl2.itemoid = dtl2[i].itemoid;
                            tbldtl2.itemcode = dtl2[i].itemcode;
                            tbldtl2.itemlongdesc = dtl2[i].itemlongdesc;
                            tbldtl2.transformpulpdtl2qty = dtl2[i].transformpulpdtl2qty;
                            tbldtl2.itemunitoid = dtl2[i].itemunitoid;
                            tbldtl2.itemunit = dtl2[i].itemunit;
                            tbldtl2.transformpulpdtl2value = valueperfg;
                            tbldtl2.transformpulpdtl2valueidr = valueperfg;
                            tbldtl2.transformpulpdtl2valueusd = 0;
                            tbldtl2.transformpulpdtl2note = dtl2[i].transformpulpdtl2note;
                            tbldtl2.transformpulpdtl2status = "";
                            tbldtl2.upduser = tbl.upduser;
                            tbldtl2.updtime = tbl.updtime;
                 

                            db.QL_trntransformpulpdtl2.Add(tbldtl2);
                            db.SaveChanges();

                            if (tbl.transformpulpmststatus == "Post")
                            {
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConOid++, "TRFMP", "QL_trntransformpulpmst", tbl.transformpulpmstoid, dtl2[i].itemoid, "FINISH GOOD", tbl.transformpulpwhoid, tbldtl2.transformpulpdtl2qty, "RECEIVE TRANSFORM R-CYCLE", tbl.transformpulpno, tbl.upduser, "", "", tbldtl2.transformpulpdtl2valueidr*tbldtl2.transformpulpdtl2qty, tbldtl2.transformpulpdtl2qty*tbldtl2.transformpulpdtl2valueusd, 0, null, tbldtl2.transformpulpdtl2oid, tbl.divgroupoid,""));
                                db.SaveChanges();

                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(tbldtl2.transformpulpdtl2qty, tbldtl2.transformpulpdtl2valueidr, tbldtl2.transformpulpdtl2valueusd, "QL_trntransformpulpmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].itemoid, "FINISH GOOD");
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(tbldtl2.transformpulpdtl2qty, tbldtl2.transformpulpdtl2valueidr, tbldtl2.transformpulpdtl2valueusd, "QL_trntransformpulpmst", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtl2[i].itemoid, "FINISH GOOD", iStockValOid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }

                            }
                        }

                        if (tbl.transformpulpmststatus == "Post")
                        {
                         
               
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, servertime, sPeriod, "TRANSFORM RE-PULP|No. " + tbl.transformpulpno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid));
                            db.SaveChanges();

                            int iSeq = 1;

                            // Persediaan Ke;luar
                            
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "C", tbl.transformpulptotalvalue, tbl.transformpulpno, "TRANSFORM PULP|No. " + tbl.transformpulpno, "Post", tbl.upduser, servertime, tbl.transformpulptotalvalue, 0, "QL_trntransformpulpmst " + tbl.transformpulpmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                            
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidFG, "D", tbl.transformpulpnetto, tbl.transformpulpno, "TRANSFORM PULP|No. " + tbl.transformpulpno, "Post", tbl.upduser, servertime, tbl.transformpulpnetto, 0, "QL_trntransformpulpmst " + tbl.transformpulpmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                            
                            if (tbl.btklvalue > 0)
                            {
                                // Insert QL_trngldtl
                                //BTKL
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, tbl.btklacctgoid, "C", tbl.btklvalue, tbl.transformpulpno, "TRANSFORM PULP BTKL|No. " + tbl.transformpulpno+" | "+ tbl.btklnote, "Post", tbl.upduser, servertime, tbl.btklvalue, 0, "QL_trntransformpulpmst " + tbl.transformpulpmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                            }
                            
                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + iConOid + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_stockvalue'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        hdrid = tbl.transformpulpmstoid.ToString();
                        msg = "Data saved using Draft No. " + hdrid + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: POService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransformpulpmst tbl = db.QL_trntransformpulpmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        var trndtl = db.QL_trntransformpulpdtl.Where(a => a.transformpulpmstoid == id && a.cmpcode == cmp);
                        db.QL_trntransformpulpdtl.RemoveRange(trndtl);

                        var trndtl2 = db.QL_trntransformpulpdtl2.Where(a => a.transformpulpmstoid == id && a.cmpcode == cmp);
                        db.QL_trntransformpulpdtl2.RemoveRange(trndtl2);

                        db.QL_trntransformpulpmst.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}