﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class RawMaterialTransferController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class transrawmst
        {
            public string cmpcode { get; set; }
            public string divname { get; set; }
            public int transmstoid { get; set; }
            public string transno { get; set; }
            public DateTime transdate { get; set; }
            public string transdocrefno { get; set; }
            public string transmststatus { get; set; }
            public string transmstnote { get; set; }
            public string createuser { get; set; }
        }
         
        public class transrawdtl
        {
            public int transdtlseq { get; set; }
            public int transfromwhoid { get; set; }
            public string transfromwh { get; set; }
            public int transtowhoid { get; set; }
            public string transtowh { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal transqty { get; set; }
            public decimal stockqty { get; set; }
            public int transunitoid { get; set; }
            public string transunit { get; set; }
            public string transdtlnote { get; set; }
            public decimal transvalueidr { get; set; }
            public decimal transvalueusd { get; set; }
        }

        public class mstperson
        {
            public string cmpcode { get; set; }
            public int personoid { get; set; }
            public string nip { get; set; }
            public string personname { get; set; }
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        public class mstmixing
        {
            public int matoid_mix { get; set; }
            public string matcode_mix { get; set; }
            public string matlongdesc_mix { get; set; }
            public int unitoid_mix { get; set; }
            public string unit_mix { get; set; }
        }

        public class matreqmst
        {
            public int matreqmstoid { get; set; }
            public string matreqno { get; set; }
            public DateTime matreqdate { get; set; }
            public int matreqwhoid { get; set; }
            public string matreqwh { get; set; }
            public string matreqmstnote { get; set; }
        }

        private string generateNo(string cmp, DateTime tgl)
        {
            string sCode = "TW-" + tgl.ToString("yyyy") + "." + tgl.ToString("MM") + "-";
            int formatCounter = 6;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transrawno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransrawmst WHERE cmpcode='" + cmp + "' AND transrawno LIKE '" + sCode + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        private decimal GetStockValue(int sOid, string sRef, string sType, string cmp)
        {
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT ISNULL(a,0) AS b FROM( SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" + sType + ", 0)) / SUM(ISNULL(stockqty, 0)) AS a FROM QL_stockvalue WHERE cmpcode='" + cmp + "' AND periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND refoid=" + sOid + " AND refname='" + sRef + "' AND closeflag='') AS tbl";
            var Value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Value;
        }

        private void InitDDL(QL_trntransrawmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc";
            //var transrawmstcityOid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.transrawmstcityOid);
            //ViewBag.transrawmstcityOid = transrawmstcityOid;

            sSql = "SELECT * FROM QL_mstgen WHERE gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc";
            var fromwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", ViewBag.fromwhoid);
            ViewBag.fromwhoid = fromwhoid;

            sSql = "SELECT * FROM QL_mstgen WHERE gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc";
            var towhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", ViewBag.towhoid);
            ViewBag.towhoid = towhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWHTo(string cmpcode, int fromwhoid)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE genother1='" + cmpcode + "' AND activeflag='ACTIVE' AND gengroup='MATERIAL LOCATION' AND genoid<>"+ fromwhoid + " ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE genother1='" + cmpcode + "' AND activeflag='ACTIVE' AND gengroup='MATERIAL LOCATION' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmpcode, int whoid)
        {
            List<transrawdtl> tbl = new List<transrawdtl>();
            string sSql = "";
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT TOP 1000 CAST(ROW_NUMBER() OVER(ORDER BY (crd.refoid)) AS INT) transdtlseq, crd.refoid AS matoid, matrawcode AS matcode, matrawlongdesc AS matlongdesc, matrawunitoid AS transunitoid, gendesc AS transunit, SUM(crd.saldoakhir) AS stockqty, 0.0 AS transqty, '' AS transdtlnote FROM QL_crdmtr crd INNER JOIN QL_mstmatraw m ON matrawoid=crd.refoid INNER JOIN QL_mstgen g ON genoid=matrawunitoid WHERE crd.cmpcode='" + cmpcode + "' AND refname='RAW MATERIAL' AND mtrwhoid=" + whoid + " AND crd.periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND closingdate='01/01/1900' GROUP BY crd.refoid, matrawcode, matrawlongdesc, matrawunitoid, gendesc HAVING SUM(crd.saldoakhir)>0 ORDER BY matcode";
            tbl = db.Database.SqlQuery<transrawdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<transrawdtl> dtDtl)
        {
            Session["QL_trntransrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trntransrawdtl"] == null)
            {
                Session["QL_trntransrawdtl"] = new List<transrawdtl>();
            }

            List<transrawdtl> dataDtl = (List<transrawdtl>)Session["QL_trntransrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trntransrawmst tbl)
        {
            //ViewBag.requireno = db.Database.SqlQuery<string>("SELECT requireno FROM QL_trnrequiremst WHERE cmpcode='" + tbl.cmpcode + "' AND requiremstoid=" + tbl.requiremstoid + "").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetRequestByData(string cmp)
        {
            List<mstperson> tbl = new List<mstperson>();

            sSql = "SELECT  p.cmpcode, p.personoid, p.nip, p.personname, p.deptoid, dp.deptname deptname FROM QL_mstperson p  INNER JOIN QL_mstdept dp ON dp.deptoid=p.deptoid INNER JOIN QL_mstdivision d ON d.cmpcode=p.cmpcode WHERE p.activeflag='ACTIVE' AND p.cmpcode='" + cmp + "'";
            tbl = db.Database.SqlQuery<mstperson>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListMatMix(string cmp)
        {
            List<mstmixing> tbl = new List<mstmixing>();
            sSql = "SELECT * FROM (SELECT matrawoid AS matoid_mix, matrawcode AS matcode_mix, matrawlongdesc AS matlongdesc_mix, gendesc AS unit_mix, matrawunitoid AS unitoid_mix FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=matrawunitoid WHERE m.cmpcode='" + cmp + "' AND LEFT(matrawcode, 2) IN (SELECT cat1code FROM QL_mstcat1 c1 WHERE c1.cmpcode='" + cmp + "' AND cat1oid IN (SELECT c0d.cat1oid FROM QL_mstcat0dtl c0d INNER JOIN QL_mstoid oi ON oi.cmpcode=c0d.cmpcode AND lastoid=cat0oid WHERE c0d.cmpcode='" + cmp + "' AND tablename='SET MATERIAL MIXING'))) AS tbl_MatMix ORDER by matcode_mix";
            tbl = db.Database.SqlQuery<mstmixing>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListMatReq(string cmp, int deptoid)
        {
            List<matreqmst> tbl = new List<matreqmst>();
            sSql = "SELECT matreqmstoid, matreqno, matreqdate, matreqwhoid, gendesc AS matreqwh, matreqmstnote FROM QL_trnmatreqmst req INNER JOIN QL_mstgen g ON genoid=matreqwhoid WHERE req.cmpcode='" + cmp + "' AND matreqmststatus='Post' AND deptoid=" + deptoid + " ORDER BY matreqmstoid";
            tbl = db.Database.SqlQuery<matreqmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MatUsageNonKIK
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";
            
            sSql = "SELECT * FROM (SELECT tm.cmpcode, divname, transrawmstoid AS transmstoid, transrawno AS transno, transrawdate AS transdate, transrawdocrefno AS transdocrefno, transrawmststatus AS transmststatus, transrawmstnote AS transmstnote, tm.updtime, tm.createuser FROM QL_trntransrawmst tm INNER JOIN QL_mstdivision div ON div.cmpcode=tm.cmpcode) AS QL_trntransmst  WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "QL_trntransmst.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "QL_trntransmst.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND transdate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND transdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND transmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND transdate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND transdate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND transmststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND transdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND transdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND transmststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND transmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND QL_trntransmst.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND QL_trntransmst.createuser='" + Session["UserID"].ToString() + "'";

            List<transrawmst> dt = db.Database.SqlQuery<transrawmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MatUsageNonKIK/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransrawmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trntransrawmst();
                tbl.cmpcode = CompnyCode;
                tbl.transrawmstoid = ClassFunction.GenerateID("QL_trntransrawmst");
                tbl.transrawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.transrawmststatus = "In Process";

                Session["QL_trntransrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl = db.QL_trntransrawmst.Find(cmp, id);

                sSql = "SELECT transrawdtlseq AS transdtlseq, transrawfromwhoid AS transfromwhoid, g1.gendesc AS transfromwh, transrawtowhoid AS transtowhoid, g2.gendesc AS transtowh, td.matrawoid AS matoid, matrawcode AS matcode, matrawlongdesc AS matlongdesc, transrawqty AS transqty, ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdmtr crd WHERE crd.cmpcode=td.cmpcode AND refoid=td.matrawoid AND refname='RAW MATERIAL' AND mtrwhoid=transrawfromwhoid AND periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND closingdate='01/01/1900'), 0.0) AS stockqty, transrawunitoid AS transunitoid, g3.gendesc AS transunit, transrawdtlnote AS transdtlnote, transrawvalueidr AS transvalueidr, transrawvalueusd AS transvalueusd FROM QL_trntransrawdtl td INNER JOIN QL_mstmatraw m ON m.matrawoid=td.matrawoid INNER JOIN QL_mstgen g1 ON g1.genoid=transrawfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transrawtowhoid INNER JOIN QL_mstgen g3 ON g3.genoid=transrawunitoid WHERE transrawmstoid=" + id + " ORDER BY transdtlseq";
                Session["QL_trntransrawdtl"] = db.Database.SqlQuery<transrawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: RawMaterialTransfer/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trntransrawmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            int iAcctgoidRM = 0;
            decimal dAmtRM = 0; decimal dAmtRMUSD = 0;

            if (tbl.transrawmststatus == "Post")
            {
                tbl.transrawno = generateNo(tbl.cmpcode, ClassFunction.GetServerTime());
            }
            else
            {
                if (tbl.transrawno == null)
                    tbl.transrawno = "";
            }

            List<transrawdtl> dtDtl = (List<transrawdtl>)Session["QL_trntransrawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].transqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY Transfer Qty must be more than 0!");
                        }
                        dtDtl[i].transvalueidr = 0;
                        dtDtl[i].transvalueusd = 0;
                        if (tbl.transrawmststatus == "Post")
                        {
                            string sRef = "RAW MATERIAL";
                            if (!ClassFunction.IsStockAvailable(tbl.cmpcode, sPeriod, dtDtl[i].matoid, dtDtl[i].transfromwhoid, dtDtl[i].transqty, sRef))
                            {
                                ModelState.AddModelError("", "USAGE QTY field Code " + dtDtl[i].matcode + " must be less than STOCK QTY!");
                            }

                            //Get Stock Value
                            dtDtl[i].transvalueidr = GetStockValue(dtDtl[i].matoid, sRef, "idr", tbl.cmpcode);
                            dtDtl[i].transvalueusd = GetStockValue(dtDtl[i].matoid, sRef, "usd", tbl.cmpcode);
                            dAmtRM += dtDtl[i].transvalueidr;
                            dAmtRMUSD += dtDtl[i].transvalueusd;
                        }
                    }
                }
            }

            if (tbl.transrawmststatus == "Post")
            {
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode))
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_RM"));
                }
                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode));
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trntransrawmst");
                var dtloid = ClassFunction.GenerateID("QL_trntransrawdtl");
                var crdmatoid = ClassFunction.GenerateID("QL_crdmtr");
                var conmtroid = ClassFunction.GenerateID("QL_conmat");
                var stockvalueoid = ClassFunction.GenerateID("QL_stockvalue");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                //tbl.transrawmstto = "";

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trntransrawmst.Find(tbl.cmpcode, tbl.transrawmstoid) != null)
                                tbl.transrawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.transrawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trntransrawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.transrawmstoid + " WHERE tablename='QL_trntransrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trntransrawdtl.Where(a => a.transrawmstoid == tbl.transrawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trntransrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trntransrawdtl tbldtl;
                        //QL_conmat tblcm;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trntransrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.transrawdtloid = dtloid++;
                            tbldtl.transrawmstoid = tbl.transrawmstoid;
                            tbldtl.transrawdtlseq = i + 1;
                            tbldtl.transrawfromwhoid = dtDtl[i].transfromwhoid;
                            tbldtl.transrawtowhoid = dtDtl[i].transtowhoid;
                            tbldtl.matrawoid = dtDtl[i].matoid;
                            tbldtl.transrawqty = dtDtl[i].transqty;
                            tbldtl.transrawunitoid = dtDtl[i].transunitoid;
                            tbldtl.transrawdtlstatus = "";
                            tbldtl.transrawdtlnote = dtDtl[i].transdtlnote;
                            tbldtl.transrawvalueidr = dtDtl[i].transvalueidr;
                            tbldtl.transrawvalueusd = dtDtl[i].transvalueusd;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trntransrawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.transrawmststatus == "Post")
                            {
                                string sRef = "RAW MATERIAL";

                                //Insert QL_conmat OUT
                                QL_conmat tblcm;
                                tblcm = new QL_conmat();
                                tblcm.cmpcode = tbl.cmpcode;
                                tblcm.conmtroid = conmtroid++;
                                tblcm.type = "RMT";
                                tblcm.typemin = -1;
                                tblcm.trndate = ClassFunction.GetServerTime();
                                tblcm.periodacctg = sPeriod;
                                tblcm.formaction = "QL_trntransrawdtl";
                                tblcm.formoid = tbl.transrawmstoid;
                                tblcm.refoid = dtDtl[i].matoid;
                                tblcm.refname = sRef;
                                tblcm.mtrwhoid = dtDtl[i].transfromwhoid;
                                tblcm.qtyin = 0;
                                tblcm.qtyout = dtDtl[i].transqty;
                                tblcm.reason = sRef + " Transfer";
                                tblcm.note = tbl.transrawno;
                                tblcm.upduser = tbl.upduser;
                                tblcm.updtime = servertime;
                                tblcm.refno = "";
                                tblcm.valueidr = dtDtl[i].transvalueidr;
                                tblcm.valueusd = dtDtl[i].transvalueusd;
                                tblcm.deptoid = 0;
                                tblcm.conres = "";
                                tblcm.valueidr_backup = 0;
                                tblcm.valueusd_backup = 0;
                                tblcm.formdtloid = dtloid++;
                                db.QL_conmat.Add(tblcm);
                                db.SaveChanges();

                               

                                //Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(-dtDtl[i].transqty, dtDtl[i].transvalueidr, dtDtl[i].transvalueusd, "QL_trntransrawdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matoid, sRef);
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = sSql = ClassFunction.GetQueryInsertStockValue(-dtDtl[i].transqty, dtDtl[i].transvalueidr, dtDtl[i].transvalueusd, "QL_trntransrawdtl", servertime, tbl.upduser, tbl.cmpcode, sPeriod, dtDtl[i].matoid, sRef, stockvalueoid++);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                db.SaveChanges();

                                //Insert QL_conmat IN
                                tblcm = new QL_conmat();
                                tblcm.cmpcode = tbl.cmpcode;
                                tblcm.conmtroid = conmtroid++;
                                tblcm.type = "RMT";
                                tblcm.typemin = 1;
                                tblcm.trndate = ClassFunction.GetServerTime();
                                tblcm.periodacctg = sPeriod;
                                tblcm.formaction = "QL_trntransrawdtl";
                                tblcm.formoid = tbl.transrawmstoid;
                                tblcm.refoid = dtDtl[i].matoid;
                                tblcm.refname = sRef;
                                tblcm.mtrwhoid = dtDtl[i].transtowhoid;
                                tblcm.qtyin = dtDtl[i].transqty;
                                tblcm.qtyout = 0;
                                tblcm.reason = sRef + " Transfer";
                                tblcm.note = tbl.transrawno;
                                tblcm.upduser = tbl.upduser;
                                tblcm.updtime = servertime;
                                tblcm.refno = "";
                                tblcm.valueidr = dtDtl[i].transvalueidr;
                                tblcm.valueusd = dtDtl[i].transvalueusd;
                                tblcm.deptoid = 0;
                                tblcm.conres = "";
                                tblcm.valueidr_backup = 0;
                                tblcm.valueusd_backup = 0;
                                tblcm.formdtloid = dtloid++;
                                db.QL_conmat.Add(tblcm);
                                db.SaveChanges();

                               
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trntransrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.transrawmststatus == "Post")
                        {
                            sSql = "UPDATE QL_mstoid SET lastoid=" + crdmatoid + " WHERE tablename='QL_crdmtr'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + conmtroid + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + stockvalueoid + " WHERE tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //Insert Jurnal
                            QL_trnglmst tblglm;
                            tblglm = new QL_trnglmst();
                            tblglm.cmpcode = tbl.cmpcode;
                            tblglm.glmstoid = glmstoid++;
                            tblglm.gldate = servertime;
                            tblglm.periodacctg = sPeriod;
                            tblglm.glnote = "Transfer " + tbl.transrawno;
                            tblglm.glflag = "Post";
                            tblglm.postdate = servertime;
                            tblglm.createuser = tbl.upduser;
                            tblglm.createtime = servertime;
                            tblglm.upduser = tbl.upduser;
                            tblglm.updtime = servertime;
                            tblglm.type = "";
                            tblglm.glother1 = "";
                            tblglm.glother2 = "";
                            tblglm.glother3 = "";
                            tblglm.rateoid = 0;
                            tblglm.rate2oid = 0;
                            tblglm.glrateidr = 1;
                            tblglm.glrate2idr = 1;
                            tblglm.glrateusd = 0;
                            tblglm.glrate2usd = 0;
                            tblglm.glres = "";
                            tblglm.gl1side = "";
                            db.QL_trnglmst.Add(tblglm);
                            db.SaveChanges();

                            QL_trngldtl tblgld;
                            if (dAmtRM > 0)
                            {
                                tblgld = new QL_trngldtl();
                                tblgld.cmpcode = tbl.cmpcode;
                                tblgld.gldtloid = gldtloid++;
                                tblgld.glmstoid = tblglm.glmstoid;
                                tblgld.glseq = 1;
                                tblgld.acctgoid = iAcctgoidRM;
                                tblgld.gldbcr = "D";
                                tblgld.glamt = dAmtRM;
                                tblgld.noref = tbl.transrawno;
                                tblgld.glnote = "Transfer Raw|No " + tbl.transrawno;
                                tblgld.glother1 = "QL_trntransrawmst " + tbl.transrawmstoid;
                                tblgld.glother2 = "";
                                tblgld.glother3 = "";
                                tblgld.glother4 = "";
                                tblgld.glflag = "Post";
                                tblgld.upduser = tbl.upduser;
                                tblgld.updtime = servertime;
                                tblgld.glamtidr = dAmtRM;
                                tblgld.glamtusd = dAmtRMUSD;
                                tblgld.glamtidr_temp = 0;
                                tblgld.glamtusd_temp = 0;
                                tblgld.glother1_temp = "";
                                tblgld.gltype_temp = "";
                                tblgld.groupoid = 0;
                                db.QL_trngldtl.Add(tblgld);
                                db.SaveChanges();

                                tblgld = new QL_trngldtl();
                                tblgld.cmpcode = tbl.cmpcode;
                                tblgld.gldtloid = gldtloid++;
                                tblgld.glmstoid = tblglm.glmstoid;
                                tblgld.glseq = 1;
                                tblgld.acctgoid = iAcctgoidRM;
                                tblgld.gldbcr = "C";
                                tblgld.glamt = dAmtRM;
                                tblgld.noref = tbl.transrawno;
                                tblgld.glnote = "Transfer Raw|No " + tbl.transrawno;
                                tblgld.glother1 = "QL_trntransrawmst " + tbl.transrawmstoid;
                                tblgld.glother2 = "";
                                tblgld.glother3 = "";
                                tblgld.glother4 = "";
                                tblgld.glflag = "Post";
                                tblgld.upduser = tbl.upduser;
                                tblgld.updtime = servertime;
                                tblgld.glamtidr = dAmtRM;
                                tblgld.glamtusd = dAmtRMUSD;
                                tblgld.glamtidr_temp = 0;
                                tblgld.glamtusd_temp = 0;
                                tblgld.glother1_temp = "";
                                tblgld.gltype_temp = "";
                                tblgld.groupoid = 0;
                                db.QL_trngldtl.Add(tblgld);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        tbl.transrawmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.transrawmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: RawMaterialTransfer/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransrawmst tbl = db.QL_trntransrawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trntransrawdtl.Where(a => a.transrawmstoid == id && a.cmpcode == cmp);
                        db.QL_trntransrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trntransrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult PrintReport(int id, string cmp)
        //{
        //    if (Session["UserID"] == null)
        //        return RedirectToAction("Login", "Account");
        //    if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
        //        return RedirectToAction("NotAuthorize", "Account");

        //    sSql = "SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'";
        //    string profname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

        //    ReportDocument report = new ReportDocument();
        //    var tbl = db.QL_trntransrawmst.Find(cmp, id);
        //    if (tbl == null)
        //        return null;

        //    var rptname = "rptTransfer";
        //    report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

        //    sSql = "SELECT * FROM (SELECT tm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tm.cmpcode) [Business Unit], tm.transrawmstoid [ID Hdr], CAST(tm.transrawmstoid AS VARCHAR(10)) [Draft No.], transrawno [Transfer No.], transrawdate [Transfer Date], transrawdocrefno [Doc. Ref. No.], transrawmstnote [Header Note], transrawmststatus [Status], UPPER(tm.createuser) [Create User], tm.createtime [Create Time], (CASE transrawmststatus WHEN 'In Process' THEN '' ELSE UPPER(tm.upduser) END) [Post User], (CASE transrawmststatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE UPPER(tm.updtime) END) [Post Time], transrawdtloid [ID Dtl], transrawdtlseq [No.], g1.gendesc [From WH], g2.gendesc [To WH], matrawcode [Code], matrawlongdesc [Description], transrawqty [Qty], g3.gendesc [Unit], transrawdtlnote [Detail Note], transrawmstto [Transfer To], ISNULL(transrawmstaddr,'') [Address] , transrawmstcityOid, g4.gendesc [City] FROM QL_trntransrawmst tm INNER JOIN QL_trntransrawdtl td ON td.cmpcode=tm.cmpcode AND td.transrawmstoid=tm.transrawmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=transrawfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transrawtowhoid INNER JOIN QL_mstmatraw m ON m.matrawoid=td.matrawoid INNER JOIN QL_mstgen g3 ON g3.genoid=transrawunitoid INNER JOIN QL_mstgen g4 ON g4.gengroup='CITY' AND g4.genoid=transrawmstcityOid) AS tblTransfer WHERE cmpcode='" + cmp + "' AND [ID Hdr]=" + id + "";

        //    ClassConnection cConn = new ClassConnection();
        //    DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

        //    report.SetDataSource(dtRpt);
        //    report.SetParameterValue("MaterialType", "RAW MATERIAL");
        //    report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
        //    report.SetParameterValue("PrintUserName", profname);
        //    Response.Buffer = false;
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    return File(stream, "application/pdf", "MaterialTransferPrintOut.pdf");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}