﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;


namespace RSA.Controllers.Transaction
{
    public class BPSheetRealController : Controller
    {
        // GET: BPSheetReal
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class bpsheetmst
        {
            public string divgroup { get; set; }
            public int bpsheetmstoid { get; set; }
            public string bpsheetno { get; set; }
            public DateTime bpsheetdate { get; set; }
            public string bpsheetmstnote { get; set; }
            public string bpsheetmststatus { get; set; }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "h", divgroupoid, "divgroupoid");
            sSql = "SELECT bpsheetmstoid, (select gendesc from ql_mstgen where genoid=h.divgroupoid) divgroup, bpsheetno, bpsheetdate, bpsheetmstnote, bpsheetmststatus FROM QL_trnbpsheetmst h WHERE 1=1";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND bpsheetmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND bpsheetmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND bpsheetmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND bpsheetdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND bpsheetdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND bpsheetmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND h.createuser='" + Session["UserID"].ToString() + "'";

            List<bpsheetmst> dt = db.Database.SqlQuery<bpsheetmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnbpsheetmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        public class bpsheetdtl : QL_trnbpsheetdtl
        {
            public string matcode { get; set; }
            public string matdesc { get; set; }
            public string bpsheetunit { get; set; }
            public string bpsheetwh { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int divgroupoid, string refname, int whoid, string bpsheetdate)
        {
            JsonResult js = null;
            try
            {
                if (refname == "RAW MATERIAL")
                {
                    sSql = $"select 0 bpsheetdtlseq, 0 bpsheetdtloid, '{refname}' refname, m.matrawoid refoid, m.matrawcode matcode, m.matrawlongdesc matdesc, {whoid} bpsheetwhoid, isnull((select x.gendesc from QL_mstgen x where x.genoid = {whoid}),'') bpsheetwh, isnull(saldoqty,0.0) bpsheetqtybefore, 0.0 bpsheetqty, 0.0 bpsheetqtyafter, m.matrawunitoid bpsheetunitoid, g.gendesc bpsheetunit, isnull(con.refno,'') refno, isnull(con.serialno,'') serialno, isnull(con.valueidr,0.0) bpsheetvalueidr, 0.0 bpsheetvalueusd, '' bpsheetdtlnote from QL_mstmatraw m inner join QL_mstgen g on g.genoid=m.matrawunitoid outer apply( select c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr, sum(qtyin - qtyout) saldoqty from QL_conmat c where c.divgroupoid={divgroupoid} and c.refname = '{refname}' and c.refoid = m.matrawoid and c.mtrwhoid = {whoid} and c.updtime <= '{bpsheetdate} 23:59:59' group by c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr )AS con where m.divgroupoid={divgroupoid} and m.matrawoid in(select x.borax_matrawoid from QL_mstbpsheet x union all select x.soda_matrawoid from QL_mstbpsheet x union all select x.tapioka_rb_matrawoid from QL_mstbpsheet x union all select x.tapioka_rb2_matrawoid from QL_mstbpsheet x union all select x.tapioka_sg_matrawoid from QL_mstbpsheet x union all select x.soda_fl_matrawoid from QL_mstbpsheet x union all select x.cms_matrawoid from QL_mstbpsheet x)";
                }
                var tbl = db.Database.SqlQuery<bpsheetdtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<bpsheetdtl>();

            try
            {
                sSql = $"SELECT * FROM( select d.bpsheetdtlseq, d.bpsheetdtloid, d.refname, d.refoid, m.matrawcode matcode, m.matrawlongdesc matdesc, bpsheetwhoid, g.gendesc bpsheetwh, d.bpsheetqtybefore, d.bpsheetqty, d.bpsheetqtyafter, d.bpsheetunitoid, g2.gendesc bpsheetunit, d.refno, d.serialno, d.bpsheetvalueidr, d.bpsheetvalueusd, d.bpsheetdtlnote from QL_trnbpsheetdtl d inner join QL_trnbpsheetmst h on h.bpsheetmstoid=d.bpsheetmstoid inner join QL_mstmatraw m on m.matrawoid=d.refoid inner join QL_mstgen g on g.genoid=d.bpsheetwhoid  inner join QL_mstgen g2 on g2.genoid=d.bpsheetunitoid where h.cmpcode='{CompnyCode}' and d.bpsheetmstoid={id} and d.refname='RAW MATERIAL'  union all  select d.bpsheetdtlseq, d.bpsheetdtloid, d.refname, d.refoid, m.matgencode matcode, m.matgenlongdesc matdesc, bpsheetwhoid, g.gendesc bpsheetwh, d.bpsheetqtybefore, d.bpsheetqty, d.bpsheetqtyafter, d.bpsheetunitoid, g2.gendesc bpsheetunit, d.refno, d.serialno, d.bpsheetvalueidr, d.bpsheetvalueusd, d.bpsheetdtlnote from QL_trnbpsheetdtl d inner join QL_trnbpsheetmst h on h.bpsheetmstoid=d.bpsheetmstoid inner join QL_mstmatgen m on m.matgenoid=d.refoid inner join QL_mstgen g on g.genoid=d.bpsheetwhoid  inner join QL_mstgen g2 on g2.genoid=d.bpsheetunitoid where h.cmpcode='{CompnyCode}' and d.bpsheetmstoid={id} and d.refname='GENERAL MATERIAL'  union all  select d.bpsheetdtlseq, d.bpsheetdtloid, d.refname, d.refoid, m.itemcode matcode, m.itemlongdesc matdesc, bpsheetwhoid, g.gendesc bpsheetwh, d.bpsheetqtybefore, d.bpsheetqty, d.bpsheetqtyafter, d.bpsheetunitoid, g2.gendesc bpsheetunit, d.refno, d.serialno, d.bpsheetvalueidr, d.bpsheetvalueusd, d.bpsheetdtlnote from QL_trnbpsheetdtl d inner join QL_trnbpsheetmst h on h.bpsheetmstoid=d.bpsheetmstoid inner join QL_mstitem m on m.itemoid=d.refoid inner join QL_mstgen g on g.genoid=d.bpsheetwhoid  inner join QL_mstgen g2 on g2.genoid=d.bpsheetunitoid where h.cmpcode='{CompnyCode}' and d.bpsheetmstoid={id} and d.refname='FINISH GOOD'  union all  select d.bpsheetdtlseq, d.bpsheetdtloid, d.refname, d.refoid, '' matcode, m.sodtldesc matdesc, bpsheetwhoid, g.gendesc bpsheetwh, d.bpsheetqtybefore, d.bpsheetqty, d.bpsheetqtyafter, d.bpsheetunitoid, g2.gendesc bpsheetunit, d.refno, d.serialno, d.bpsheetvalueidr, d.bpsheetvalueusd, d.bpsheetdtlnote from QL_trnbpsheetdtl d inner join QL_trnbpsheetmst h on h.bpsheetmstoid=d.bpsheetmstoid inner join QL_trnsorawdtl m on m.sorawdtloid=d.refoid inner join QL_mstgen g on g.genoid=d.bpsheetwhoid  inner join QL_mstgen g2 on g2.genoid=d.bpsheetunitoid where h.cmpcode='{CompnyCode}' and d.bpsheetmstoid={id} and d.refname='SHEET' )AS dt ORDER BY dt.bpsheetdtlseq";
                dtl = db.Database.SqlQuery<bpsheetdtl>(sSql).ToList();

                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void InitDDL(QL_trnbpsheetmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA("VAR_STOCK_ADJUSTMENT")).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT ifield, sfield FROM ( SELECT genoid ifield, gendesc sfield FROM QL_mstgen WHERE gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' UNION ALL SELECT genoid ifield, gendesc sfield FROM QL_mstgen WHERE gengroup='ITEM LOCATION' AND activeflag='ACTIVE' ) AS t ORDER BY sfield";
            var whoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield");
            ViewBag.whoid = whoid;
        }

        private void FillAdditionalField(QL_trnbpsheetmst tbl)
        {

        }

        private string generateNo()
        {
            var sNo = $"PBP-{ClassFunction.GetServerTime().ToString("yyyy.MM")}-";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>($"SELECT ISNULL(MAX(CAST(RIGHT(bpsheetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbpsheetmst WHERE cmpcode='{CompnyCode}' AND bpsheetno LIKE '{sNo}%'").FirstOrDefault(), 6);
            sNo = sNo + sCounter;
            return sNo;
        }

        public ActionResult Form(string id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbpsheetmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnbpsheetmst();
                tbl.bpsheetmstoid = ClassFunction.GenerateID("QL_trnbpsheetmst");
                tbl.bpsheetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.bpsheetmststatus = "In Process";

            }
            else
            {
                action = "Update Data";
                int id2 = id.toInteger();
                tbl = db.QL_trnbpsheetmst.Find(id2);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnbpsheetmst tbl, List<bpsheetdtl> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode; tbl.acctgoid = 0;
            if (string.IsNullOrEmpty(tbl.bpsheetno)) tbl.bpsheetno = "";
            if (string.IsNullOrEmpty(tbl.bpsheetmstnote)) tbl.bpsheetmstnote = "";
            if (tbl.bpsheetmststatus.ToLower() == "revised") tbl.bpsheetmststatus = "In Process";

            if (dtDtl == null) msg += "- Please fill detail data!<br />";
            else if (dtDtl.Count <= 0) msg += "- Please fill detail data!<br />";
            else
            {
                foreach (var item in dtDtl)
                {
                    if (item.bpsheetqty <= 0) msg += "- Qty tidak boleh 0 atau minus!<br />";
                    else
                    {
                        if (tbl.bpsheetmststatus.ToLower() == "post")
                        {
                            if (!ClassFunction.IsStockAvailableWithSN(tbl.cmpcode, servertime, item.refoid, item.bpsheetwhoid, Math.Abs(item.bpsheetqty), item.refname, item.refno ?? "", item.serialno ?? ""))
                            {
                                msg += $"- QTY Material {item.matcode} {item.matdesc} must be less than STOCK QTY!<BR>";
                            }
                            if (item.bpsheetvalueidr <= 0) item.bpsheetvalueidr = ClassFunction.GetStockValueWithSN(CompnyCode, item.refname, item.refoid, item.refno ?? "", item.serialno ?? "");
                        }
                    }
                }
            }

            var iAcctgoidRM = 0;
            if (tbl.bpsheetmststatus.ToLower() == "post")
            {
                tbl.bpsheetno = generateNo();
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid));
            }

            if (msg == "")
            {
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.bpsheetdate);
                tbl.approvalcode = "";
                tbl.approvaluser = "";
                tbl.approvaldatetime = new DateTime(1900, 1, 1);
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);

                var sDateGL = servertime.ToString("MM/dd/yyyy").ToDateTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
                var mstoid = ClassFunction.GenerateID("QL_trnbpsheetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnbpsheetdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.bpsheetmstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnbpsheetmst.Add(tbl);
                            db.SaveChanges();

                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid = {tbl.bpsheetmstoid} WHERE tablename = 'QL_trnbpsheetmst'");
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnbpsheetdtl.Where(a => a.bpsheetmstoid == tbl.bpsheetmstoid);
                            db.QL_trnbpsheetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnbpsheetdtl tbldtl; var seq = 1;
                        foreach (var item in dtDtl)
                        {
                            tbldtl = (QL_trnbpsheetdtl)ClassFunction.MappingTable(new QL_trnbpsheetdtl(), item);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.bpsheetdtloid = dtloid++;
                            tbldtl.bpsheetmstoid = tbl.bpsheetmstoid;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.bpsheetdtlseq = seq++;
                            tbldtl.refno = (string.IsNullOrEmpty(item.refno) ? tbl.bpsheetno : item.refno);
                            tbldtl.bpsheetvalueusd = 0;
                            tbldtl.bpsheetdtlstatus = "";
                            tbldtl.bpsheetdtlnote = item.bpsheetdtlnote ?? "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnbpsheetdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.bpsheetmststatus.ToLower() == "post")
                            {
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "bpsheet", "QL_trnbpsheetmst", tbl.bpsheetmstoid, tbldtl.refoid, tbldtl.refname, tbldtl.bpsheetwhoid, tbldtl.bpsheetqty, $"Pemakaian BP Sheet {tbldtl.refname}", tbl.bpsheetno, tbl.upduser, tbldtl.refno, "", tbldtl.bpsheetvalueidr, tbldtl.bpsheetvalueusd, 0, null, tbldtl.bpsheetdtloid, tbl.divgroupoid, tbldtl.serialno));
                                db.SaveChanges();
                            }
                        }
                        db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid = {dtloid - 1} WHERE tablename='QL_trnbpsheetdtl'");
                        db.SaveChanges();

                        if (tbl.bpsheetmststatus.ToLower() == "post")
                        {
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={conmatoid - 1} WHERE tablename='QL_conmat'");
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDateGL, sPeriod, "Pemakaian BP Sheet|No. " + tbl.bpsheetno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid));
                            db.SaveChanges();

                            int iSeq = 1;

                            foreach (var d in dtDtl)
                            {
                                decimal amtIDR = d.bpsheetqty * d.bpsheetvalueidr;
                                decimal amtUSD = d.bpsheetqty * d.bpsheetvalueusd;
                                int acctgoid_debet = 0;
                                acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.borax_matrawoid == d.refoid)?.borax_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.soda_matrawoid == d.refoid)?.soda_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.tapioka_rb_matrawoid == d.refoid)?.tapioka_rb_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.tapioka_rb2_matrawoid == d.refoid)?.tapioka_rb2_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.tapioka_sg_matrawoid == d.refoid)?.tapioka_sg_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.soda_fl_matrawoid == d.refoid)?.soda_fl_acctgoid ?? 0;
                                if (acctgoid_debet == 0) acctgoid_debet = db.QL_mstbpsheet.FirstOrDefault(x => x.cms_matrawoid == d.refoid)?.cms_acctgoid ?? 0;
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, acctgoid_debet, "D", amtIDR, tbl.bpsheetno, "Pemakaian BP Sheet|No. " + tbl.bpsheetno + "", "Post", tbl.upduser, servertime, amtIDR, amtUSD, "QL_trnbpsheetmst " + tbl.bpsheetmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "C", amtIDR, tbl.bpsheetno, "Pemakaian BP Sheet|No. " + tbl.bpsheetno + "", "Post", tbl.upduser, servertime, amtIDR, amtUSD, "QL_trnbpsheetmst " + tbl.bpsheetmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                db.SaveChanges();
                            }
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={glmstoid} WHERE tablename='QL_trnglmst'");
                            db.SaveChanges();
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={gldtloid - 1} WHERE tablename='QL_trngldtl'");
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.bpsheetmstoid.ToString();
                        sReturnNo = "No. " + tbl.bpsheetno;
                        if (tbl.bpsheetmststatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbpsheetmst tbl = db.QL_trnbpsheetmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnbpsheetdtl.Where(a => a.bpsheetmstoid == id);
                        db.QL_trnbpsheetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnbpsheetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnbpsheetmst.Where(x=> x.bpsheetmstoid == id);
            if (tbl == null)
                return null;

            var rptname = "rptPBPSheet";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT mum.bpsheetmstoid [Oid], CONVERT(VARCHAR(20), mum.bpsheetmstoid) AS [Draft No.], mum.bpsheetno AS [Trans No], mum.bpsheetdate AS [Trans Date], mum.bpsheetmststatus AS [Status], mum.bpsheetmstnote AS [Header Note], mud.bpsheetdtloid, mud.bpsheetdtlseq AS [No.], g1.gendesc AS [Warehouse], mud.refname AS [Type], mud.refoid [Matoid], (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=mud.refoid) AS [Code], (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=mud.refoid) AS [Description], mud.bpsheetqty AS [Qty], g2.gendesc AS [Unit], mud.bpsheetdtlnote AS [Detail Note], di.divname AS [Business Unit], '' AS [Detail Location], '' [Person Name] , '' [NIP], mud.refno AS [No. Batch], mud.serialno AS [No. Serial], dv.gendesc [Divisi] FROM QL_trnbpsheetmst mum INNER JOIN QL_trnbpsheetdtl mud ON mud.cmpcode=mum.cmpcode AND mud.bpsheetmstoid=mum.bpsheetmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=mud.bpsheetwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.bpsheetunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode inner join QL_mstgen dv on dv.genoid=mum.divgroupoid WHERE mud.bpsheetmstoid=" + id + " ORDER BY mum.bpsheetmstoid, mud.bpsheetdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PBPSheetPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}