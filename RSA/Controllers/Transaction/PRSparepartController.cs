﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PRSparepartController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prspmst
        {
            public string cmpcode { get; set; }
            public int prspmstoid { get; set; }
            public string prspno { get; set; }
            public DateTime prspdate { get; set; }
            public string deptname { get; set; }
            public string prspmststatus { get; set; }
            public string prspmstnote { get; set; }
            public string divname { get; set; }
        }

        public class prspdtl
        {
            public int prspdtlseq { get; set; }
            public int matspoid { get; set; }
            public string matspcode { get; set; }
            public string matsplongdesc { get; set; }
            public decimal matsplimitqty { get; set; }
            public decimal prspqty { get; set; }
            public int prspunitoid { get; set; }
            public string prspunit { get; set; }
            public DateTime prsparrdatereq { get; set; }
            public string prspdtlnote { get; set; }
            public int requiredtloid { get; set; }
            public decimal requireqty { get; set; }
        }

        public class matsp
        {
            public string cmpcode { get; set; }
            public int matspoid { get; set; }
            public string matspcode { get; set; }
            public string matsplongdesc { get; set; }
            public decimal matsplimitqty { get; set; }
            public int prspunitoid { get; set; }
            public string prspunit { get; set; }
        }

        private void InitDDL(QL_prspmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_prspmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataDetails()
        {
            //List<prspdtl> tbl = new List<prspdtl>();

            //if (reqoid == 0)
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matspcode) AS INT) prspdtlseq, matspoid, matspcode, matsplongdesc, matsplimitqty, 0.0000 prspqty, matspunitoid prspunitoid, gendesc prspunit, '' prspdtlnote, DATEADD(D, 7, GETDATE()) prsparrdatereq, 0 requiredtloid, 0.0000 requireqty FROM QL_mstmatsp m INNER JOIN QL_mstgen g ON genoid=matspunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' AND ISNULL(matspres2, '')='Non Assets' AND ISNULL(matspres3, '') NOT IN ('Sawn Timber', 'Log') AND matspoid IN (SELECT DISTINCT prd.matspoid FROM QL_prspdtl prd WHERE prd.cmpcode='" + cmp + "' AND prd.upduser='" + Session["UserID"].ToString() + "') ORDER BY matspcode";
            //else
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matspcode) AS INT) prspdtlseq, m.matspoid, matspcode, matsplongdesc, matsplimitqty, requireqty prspqty, requireunitoid prspunitoid, gendesc prspunit, '' prspdtlnote, DATEADD(D, 7, GETDATE()) prsparrdatereq, requiredtloid, requireqty FROM QL_trnrequiredtl req INNER JOIN QL_mstmatsp m ON matspoid=requirerefoid INNER JOIN QL_mstgen g ON genoid=requireunitoid WHERE req.cmpcode='" + cmp + "' AND requiremstoid=" + reqoid + " AND requirereftype='sp' AND requireqty>0 ORDER BY matspcode";

            //tbl = db.Database.SqlQuery<prspdtl>(sSql).ToList();
            if(Session["QL_prspdtl"] == null)
            {
                Session["QL_prspdtl"] = new List<prspdtl>();
            }
            List<prspdtl> tbl = (List<prspdtl>)Session["QL_prspdtl"];

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<prspdtl> dtDtl)
        {
            Session["QL_prspdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_prspdtl"] == null)
            {
                Session["QL_prspdtl"] = new List<prspdtl>();
            }

            List<prspdtl> dataDtl = (List<prspdtl>)Session["QL_prspdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_prspmst tbl)
        {
            ViewBag.requireno = db.Database.SqlQuery<string>("SELECT requireno FROM QL_trnrequiremst WHERE cmpcode='" + tbl.cmpcode + "' AND requiremstoid=" + tbl.requiremstoid + "").FirstOrDefault();
        }

        //[HttpPost]
        //public ActionResult GetRequireData(string cmp)
        //{
        //    List<QL_trnrequiremst> tbl = new List<QL_trnrequiremst>();

        //    sSql = "SELECT * FROM QL_trnrequiremst req WHERE req.cmpcode='" + cmp + "' AND requiremststatus='Post' ORDER BY requiredate DESC";
        //    tbl = db.Database.SqlQuery<QL_trnrequiremst>(sSql).ToList();

        //    return Json(tbl, JsonRequestBehavior.AllowGet);
        //}

        // GET: PRspMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, mdFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRspMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT prm.cmpcode, prspmstoid, prspno, prspdate, deptname, prspmststatus, prspmstnote, divname FROM QL_prspmst prm INNER JOIN QL_mstdept de ON de.cmpcode=prm.cmpcode AND de.deptoid=prm.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=prm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "prm.cmpcode IN (" + Session["CompnyCode"].ToString() + ")";
            else
                sSql += "prm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                sSql += "  ";
            }
            else
            {
                sSql += " AND (prspdate BETWEEN '" + modfil.filterperiod1 + "' AND '" + modfil.filterperiod2 + "')";
                //sSql += " AND prspmststatus IN ('In Process', 'Revised')";
            }
            
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND prm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND prm.createuser='" + Session["UserID"].ToString() + "'";

            List<prspmst> dt = db.Database.SqlQuery<prspmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: PRspMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRspMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prspmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_prspmst();
                tbl.prspmstoid = ClassFunction.GenerateID("QL_prspmst");
                tbl.prspdate = ClassFunction.GetServerTime();
                tbl.prspexpdate = ClassFunction.GetServerTime().AddMonths(3);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.prspmststatus = "In Process";
                tbl.requiremstoid = 0;
                tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");

                Session["QL_prspdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_prspmst.Find(cmp, id);

                sSql = "SELECT prspdtlseq, prd.matspoid, matspcode, matsplongdesc, matsplimitqty, prspqty, prspunitoid, gendesc prspunit, ISNULL(prspdtlnote, '') prspdtlnote, prsparrdatereq/*, requiredtloid, requireqty*/ FROM QL_prspdtl prd INNER JOIN QL_mstmatsp m ON m.matspoid=prd.matspoid INNER JOIN QL_mstgen g ON genoid=prspunitoid WHERE prd.cmpcode='" + cmp + "' AND prd.prspmstoid=" + id + " ORDER BY prspdtlseq";
                Session["QL_prspdtl"] = db.Database.SqlQuery<prspdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRspMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_prspmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRspMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.prspno == null)
                tbl.prspno = "";

            List<prspdtl> dtDtl = (List<prspdtl>)Session["QL_prspdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.prspmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_prspmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_prspmst");
                var dtloid = ClassFunction.GenerateID("QL_prspdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + CompnyCode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.requiremstoid = 0;
                        tbl.prspno = tbl.prspno == null ? "" : tbl.prspno;
                        tbl.prspmstnote = tbl.prspmstnote == null ? "" : tbl.prspmstnote;
                        tbl.approvaluser = tbl.approvaluser == null ? "" : tbl.approvaluser;
                        tbl.approvalcode = tbl.approvalcode == null ? "" : tbl.approvalcode;
                        tbl.approvaldatetime = tbl.approvaldatetime == null ? DateTime.Parse("1900-01-01 00:00:00") : tbl.approvaldatetime;
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? servertime : tbl.revisetime; //DateTime.ParseExact("01/01/1900", "MM/dd/yyyy")
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? servertime : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_prspmst.Find(tbl.cmpcode, tbl.prspmstoid) != null)
                                tbl.prspmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.prspdate);
                            tbl.divoid = divoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                            tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");
                            db.QL_prspmst.Add(tbl);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.prspmstoid + " WHERE tablename='QL_prspmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            
                            var trndtl = db.QL_prspdtl.Where(a => a.prspmstoid == tbl.prspmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_prspdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_prspdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_prspdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.prspdtloid = dtloid++;
                            tbldtl.prspmstoid = tbl.prspmstoid;
                            tbldtl.prspdtlseq = i + 1;
                            tbldtl.prsparrdatereq = dtDtl[i].prsparrdatereq;
                            tbldtl.sparepartoid = dtDtl[i].matspoid;
                            tbldtl.prspqty = dtDtl[i].prspqty;
                            tbldtl.prspunitoid = dtDtl[i].prspunitoid;
                            tbldtl.prspdtlstatus = "";
                            tbldtl.prspdtlnote = dtDtl[i].prspdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            //tbldtl.requireqty = dtDtl[i].requireqty;
                            //tbldtl.requiredtloid = dtDtl[i].requiredtloid;
                            db.QL_prspdtl.Add(tbldtl);
                            db.SaveChanges();
                        }
                        
                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_prspdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.prspmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PR-sp" + tbl.prspmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_prspmst";
                                tblApp.oid = tbl.prspmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: PRspMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRspMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prspmst tbl = db.QL_prspmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_prspdtl.Where(a => a.prspmstoid == id && a.cmpcode == cmp);
                        db.QL_prspdtl.RemoveRange(trndtl);
                        db.SaveChanges();
                        
                        db.QL_prspmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMatsp(string cmp)
        {
            List<matsp> mstmatsp = new List<matsp>();
            sSql = "SELECT m.cmpcode, matspoid, matspcode, matsplongdesc, ISNULL(matsplimitqty, 1) [matsplimitqty], matspunitoid [prspunitoid], g.gendesc [prspunit] FROM QL_mstmatsp m INNER JOIN QL_mstgen g ON g.cmpcode = m.cmpcode AND g.genoid = m.matspunitoid WHERE m.cmpcode = '" + CompnyCode + "' AND m.activeflag='ACTIVE'";
            mstmatsp = db.Database.SqlQuery<matsp>(sSql).ToList();

            return Json(mstmatsp, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult PrintReport(int id, string cmp)
        //{
        //    if (Session["UserID"] == null)
        //        return RedirectToAction("Login", "Profile");
        //    if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
        //        return RedirectToAction("NotAuthorize", "Profile");

        //    ReportDocument report = new ReportDocument();
        //    var tbl = db.QL_prspmst.Find(cmp, id);
        //    if (tbl == null)
        //        return null;

        //    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPR_Trn.rpt"));
        //    sSql = "SELECT prm.prspmstoid AS prmstoid, prm.cmpcode, [BU Name] BUName, [BU Address] BUAddress, [BU City] BUCity, [BU Province] BUProvince, [BU Phone] BUPhone, [BU Email] BUEmail, [BU Phone2] BUPhone2, [BU PostCode] BUPostCode, [BU Fax1] BUFax1, [BU Fax2] BUFax2, [BU Country] BUCountry, prm.prspno AS prno, CONVERT(VARCHAR(10), prm.prspmstoid) AS draftno, prm.prspdate AS prdate, g1.deptname AS department, prm.prspmstnote AS prmstnote, prm.prspmststatus AS prmststatus, prd.prspdtlseq AS prdtlseq, prd.matspoid AS matoid, m.matspcode AS matcode, m.matsplongdesc AS matlongdesc, prd.prspqty AS prqty, prd.requireqty, ISNULL(requireno, '') RequireNo, ISNULL(requiredatefilter, CAST('1/1/1900' AS DATETIME)) RequireDate, ISNULL(requiremstnote, '') RequireHeaderNote, ISNULL(requiremststatus, '') RequireStatus, ISNULL(r.createtime, CAST('1/1/1900' AS DATETIME)) RequireCreateDate, ISNULL(r.createuser, '') RequireCreateUser, ISNULL(requiredatetype, '') RequireDateType, g4.gendesc as prunit, prd.prspdtlnote AS prdtlnote, prd.prsparrdatereq AS prarrdatereq, (SELECT ISNULL(pa.profname,'-') FROM QL_mstprof pa WHERE pa.profoid=prm.approvaluser) AS UserNameApproved, prm.approvaldatetime AS ApproveTime, (SELECT ISNULL(pc.profname,'-') FROM QL_mstprof pc WHERE pc.profoid=prm.createuser) AS CreateUserName, prm.createtime AS [CreateTime] FROM QL_prspmst prm INNER JOIN QL_prspdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prspmstoid=prd.prspmstoid LEFT JOIN QL_trnrequiremst r ON r.cmpcode=prm.cmpcode AND r.requiremstoid=prm.requiremstoid INNER JOIN QL_mstmatsp m ON prd.matspoid=m.matspoid INNER JOIN QL_mstdept g1 ON prm.cmpcode=g1.cmpcode AND prm.deptoid=g1.deptoid INNER JOIN QL_mstgen g4 ON prd.prspunitoid=g4.genoid INNER JOIN (SELECT d.cmpcode, d.divname AS [BU Name], d.divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], d.divphone AS [BU Phone], d.divemail AS [BU Email], d.divphone2 AS [BU Phone2], d.divpostcode AS [BU PostCode], d.divfax1	AS [BU Fax1], d.divfax2	AS [BU Fax2], g3.gendesc AS [BU Country] FROM QL_mstdivision d INNER JOIN QL_mstgen g1 ON g1.genoid=d.divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) AS BU ON BU.cmpcode=prm.cmpcode WHERE prm.cmpcode='" + cmp + "' AND prm.prspmstoid=" + id + " ORDER BY prm.prspmstoid";
        //    List<PrintOutModels.printout_pr> dtRpt = db.Database.SqlQuery<PrintOutModels.printout_pr>(sSql).ToList();

        //    report.SetDataSource(dtRpt);
        //    report.SetParameterValue("sUserID", Session["UserID"].ToString());
        //    Response.Buffer = false;
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    return File(stream, "application/pdf", "PRspMaterialReport.pdf");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
