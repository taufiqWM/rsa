﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PORawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnporawmst
        {
            public string cmpcode { get; set; }
            public int porawmstoid { get; set; }
            public string divgroup { get; set; }
            public string porawtype { get; set; }
            public int porawpaytypeoid { get; set; }
            public string porawno { get; set; }
            public DateTime porawdate { get; set; }
            public string suppname { get; set; }
            public string porawmststatus { get; set; }
            public string porawmstnote { get; set; }
            public string divname { get; set; }
            public string createuser { get; set; }
            public string reviseuser { get; set; }
            public string revisereason { get; set; }
            public DateTime createtime { get; set; }
        }

        public class trnporawdtl
        {
            public int porawdtlseq { get; set; }
            public int prrawmstoid { get; set; }
            public int prrawdtloid { get; set; }
            public string prrawno { get; set; }
            public string prrawdatestr { get; set; }
            public DateTime prrawdate { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal matrawlimitqty { get; set; }
            public decimal prrawqty { get; set; }
            public decimal porawqty { get; set; }
            public int porawunitoid { get; set; }
            public string porawunit { get; set; }
            public decimal porawprice { get; set; }
            public decimal porawdtlamt { get; set; }
            public string porawdtldisctype { get; set; }
            public decimal porawdtldiscvalue { get; set; }
            public decimal porawdtldiscamt { get; set; }
            public decimal porawdtlnetto { get; set; }
            public string porawdtlnote { get; set; }
            public string porawdtlnote2 { get; set; }
            public string prrawarrdatereqstr { get; set; }
            public DateTime prrawarrdatereq { get; set; }
            public decimal lastpoprice { get; set; }
            //public int lastcurroid { get; set; }

        }

        private void InitDDL(QL_trnporawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnporawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            ViewBag.approvalcode = approvalcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var porawpaytype = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.porawpaytypeoid);
            ViewBag.porawpaytype = porawpaytype;

            //sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'";
            //var porawunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.porawunitoid);
            //ViewBag.porawunitoid = porawunitoid;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnporawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int prmstoid, int suppoid,int pomstoid)
        {
            List<trnporawdtl> tbl = new List<trnporawdtl>();

                sSql = "SELECT 0 AS porawdtlseq, prd.prrawmstoid, prd.prrawdtloid, prm.prrawno, prd.matrawoid, ISNULL((prd.prrawqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(pod.porawqty - (CASE WHEN pod.porawdtlres1 IS NULL THEN ISNULL(pod.closeqty, 0) WHEN pod.porawdtlres1='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.porawqty - CONVERT(decimal(18,2), pod.porawdtlres1)) END)), 0) FROM QL_trnporawdtl pod INNER JOIN QL_trnporawmst pom ON pom.cmpcode=pod.cmpcode AND pom.porawmstoid=pod.porawmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prrawdtloid=prd.prrawdtloid AND pom.porawmststatus NOT IN ('Cancel', 'Rejected') AND pod.porawmstoid<>" + pomstoid + ")) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='Raw' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prrawdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0)), 0) AS prrawqty, prd.prrawunitoid AS porawunitoid, m.matrawcode, m.matrawlongdesc, m.matrawlimitqty, g2.gendesc AS porawunit, 0.0 AS porawqty, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatrawprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matrawoid=prd.matrawoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatrawprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matrawoid=prd.matrawoid ORDER BY crd.updtime DESC), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matrawoid=prd.matrawoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=" + suppoid + " AND crd.matrawoid=prd.matrawoid ORDER BY crd.updtime DESC), 1)) AS lastcurroid, prd.prrawdtlnote AS porawdtlnote, 0.0 AS porawprice, CONVERT(VARCHAR(10), prm.prrawdate, 101) AS prrawdatestr, prm.prrawdate, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prrawarrdatereqstr, prd.prrawarrdatereq, 0.0 AS porawdtlamt, 'A' AS porawdtldisctype, 0.0 AS porawdtldiscvalue, 0.0 AS porawdtldiscamt, 0.0 AS porawdtlnetto, '' AS porawdtlnote2 FROM QL_prrawdtl prd INNER JOIN QL_prrawmst prm ON prm.cmpcode=prd.cmpcode AND prm.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid /*INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1code=SUBSTRING(m.matrawcode, 1, 2) AND cat1res1='Raw' AND cat1res2 LIKE '%WIP%'*/ INNER JOIN QL_mstgen g2 ON prd.prrawunitoid=g2.genoid WHERE prd.cmpcode='" + CompnyCode + "' AND prd.prrawmstoid=" + prmstoid + " AND prd.prrawdtlstatus='' AND prm.prrawmststatus='Approved'";

            tbl = db.Database.SqlQuery<trnporawdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnporawdtl> dtDtl)
        {
            Session["QL_trnporawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnporawdtl"] == null)
            {
                Session["QL_trnporawdtl"] = new List<trnporawdtl>();
            }

            List<trnporawdtl> dataDtl = (List<trnporawdtl>)Session["QL_trnporawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnporawmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.potype = tbl.porawtype;
            ViewBag.potaxtype = tbl.porawtaxtype;
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppayment { get; set; }
            public string supptaxable { get; set; }
            public int supptaxamt { get; set; }
            //public int suppcurroid { get; set; }
            //public string currcode{ get; set; }
        }

            [HttpPost]
        public ActionResult GetSuppData(int divgroupoid)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT suppoid, suppcode, suppname,supppaymentoid, suppaddr, (CASE supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supptaxable WHEN 1 THEN 11 ELSE 0 END) supptaxamt, gp.gendesc supppayment  FROM QL_mstsupp s /*INNER JOIN QL_mstcurr c ON suppcurroid=c.curroid*/ INNER JOIN QL_mstgen gp ON gp.genoid=CAST(s.supppaymentoid AS varchar)  WHERE s.cmpcode='" + CompnyCode + "' and s.divgroupoid="+divgroupoid+" AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class prrawmst
        {
            public int prrawmstoid { get; set; }
            public string prrawno { get; set; }
            public string prrawdate { get; set; }
            public string prrawexpdate { get; set; }
            public string deptname { get; set; }
            public string prrawmstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp,int divgroupoid)
        {
            List<prrawmst> tbl = new List<prrawmst>();
            sSql = " SELECT DISTINCT prm.prrawmstoid, prrawno, CONVERT(VARCHAR(10), prrawdate, 101) AS prrawdate, CONVERT(VARCHAR(10), prrawexpdate, 101) AS prrawexpdate, deptname, ISNULL(prrawmstnote, '') [prrawmstnote] FROM QL_prrawmst prm INNER JOIN QL_prrawdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" + CompnyCode + "' AND prm.divgroupoid="+divgroupoid+" AND prm.prrawmststatus='Approved' ORDER BY prrawdate DESC, prm.prrawmstoid DESC ";
            
            tbl = db.Database.SqlQuery<prrawmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: PORawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil) //string filter
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "pom", divgroupoid, "divgroupoid");
            sSql = "SELECT pom.cmpcode, (select gendesc from ql_mstgen where genoid=pom.divgroupoid) divgroup, porawmstoid, porawno, porawdate, suppname, porawmststatus, ISNULL(porawmstnote, '') [porawmstnote], divname, porawtype FROM QL_trnporawmst pom INNER JOIN QL_mstsupp s ON s.cmpcode='" + CompnyCode + "' AND s.suppoid=pom.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "pom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "pom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND porawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND porawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND porawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND porawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND porawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND porawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND pom.createuser='" + Session["UserID"].ToString() + "'";

            List<trnporawmst> dt = db.Database.SqlQuery<trnporawmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnporawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: PORawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnporawmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnporawmst();
                tbl.cmpcode = CompnyCode;
                tbl.porawmstoid = ClassFunction.GenerateID("QL_trnporawmst");
                tbl.porawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.porawmststatus = "In Process";
                tbl.porawtolerance = 10;

                Session["QL_trnporawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnporawmst.Find(CompnyCode, id);

                sSql = "SELECT pod.porawdtlseq, prd.prrawmstoid, prm.prrawno, CONVERT(VARCHAR(10), prm.prrawdate, 101) AS prrawdatestr, prm.prrawdate, pod.prrawdtloid, pod.matrawoid, m.matrawcode, m.matrawlongdesc, m.matrawlimitqty, pod.porawqty, ISNULL((prd.prrawqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.porawqty - (CASE WHEN podtl.porawdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.porawdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.porawqty - CONVERT(decimal(18,2), podtl.porawdtlres1)) END)), 0) FROM QL_trnporawdtl podtl INNER JOIN QL_trnporawmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.porawmstoid=podtl.porawmstoid WHERE podtl.prrawdtloid=prd.prrawdtloid AND podtl.porawmstoid<>" + id + " AND pomst.porawmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='Raw' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prrawdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0) AS prrawqty,  0.0 AS poqtyuse,  pod.porawunitoid, g.gendesc AS porawunit, pod.porawprice, pod.porawdtlamt, pod.porawdtldisctype, pod.porawdtldiscvalue, pod.porawdtldiscamt, pod.porawdtlnetto, ISNULL(pod.porawdtlnote, '') [porawdtlnote], /*ISNULL(pod.porawdtlnote2,'') porawdtlnote2,*/ ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatrawprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matrawoid=pod.matrawoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstmatrawprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matrawoid=pod.matrawoid ORDER BY crd.updtime), 0.0)) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matrawoid=pod.matrawoid ORDER BY crd.updtime), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.refname='SUPPLIER' AND crd.refoid=pom.suppoid AND crd.matrawoid=pod.matrawoid ORDER BY crd.updtime), 1)) AS lastcurroid, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prrawarrdatereqstr, prd.prrawarrdatereq FROM QL_trnporawdtl pod INNER JOIN QL_trnporawmst pom ON pom.cmpcode=pod.cmpcode AND pom.porawmstoid=pod.porawmstoid INNER JOIN QL_mstmatraw m ON pod.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON pod.porawunitoid=g.genoid INNER JOIN QL_prrawdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid WHERE pod.porawmstoid=" + id + " AND pod.cmpcode='"+ cmp + "' ORDER BY pod.porawdtlseq";
                Session["QL_trnporawdtl"] = db.Database.SqlQuery<trnporawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnporawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.porawno == null)
                tbl.porawno = "";

            List<trnporawdtl> dtDtl = (List<trnporawdtl>)Session["QL_trnporawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].porawqty <= 0)
                        {
                            ModelState.AddModelError("", "PO QTY for Material " + dtDtl[i].matrawlongdesc + " must be more than 0");
                        }

                        if (dtDtl[i].porawprice <= 0)
                        {
                            ModelState.AddModelError("", "PO Price for Material " + dtDtl[i].matrawlongdesc + " must be more than 0");
                        }

                        sSql = "SELECT ISNULL((prd.prrawqty - ISNULL(prd.closeqty, 0) - ((SELECT ISNULL(SUM(podtl.porawqty - (CASE WHEN podtl.porawdtlres1 IS NULL THEN ISNULL(podtl.closeqty, 0) WHEN podtl.porawdtlres1='' THEN ISNULL(podtl.closeqty, 0) ELSE (podtl.porawqty - CONVERT(decimal(18,2), podtl.porawdtlres1)) END)), 0) FROM QL_trnporawdtl podtl INNER JOIN QL_trnporawmst pomst ON pomst.cmpcode=podtl.cmpcode AND pomst.porawmstoid=podtl.porawmstoid WHERE podtl.cmpcode=prd.cmpcode AND podtl.prrawmstoid=prd.prrawmstoid AND  podtl.prrawdtloid=prd.prrawdtloid AND podtl.porawmstoid<>" + tbl.porawmstoid + " AND pomst.porawmststatus NOT IN ('Cancel', 'Rejected')) + ISNULL((SELECT ISNULL(SUM(posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom2 ON pod2.posubconmstoid=pom2.posubconmstoid AND posubconref='Raw' WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prrawdtloid AND pom2.posubconmststatus NOT IN ('Cancel', 'Rejected')), 0.0))), 0.0) AS prrawqty FROM QL_prrawdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prrawdtloid=" + dtDtl[i].prrawdtloid + "";
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].prrawqty)
                            dtDtl[i].prrawqty = dQty;
                        if (dQty < dtDtl[i].porawqty)
                            ModelState.AddModelError("", "Some PO Qty has been updated by another user. Please check that every Qty must be less than PR Qty!");

                        //sSql = "SELECT COUNT(-1) FROM QL_prrawdtl prd WHERE prd.cmpcode='" + tbl.cmpcode + "' AND prd.prrawmstoid=" + dtDtl[i].prrawmstoid + " AND prd.prrawdtloid=" + dtDtl[i].prrawdtloid + " AND prd.prrawdtlstatus='COMPLETE'";
                        //var CheckDataExists = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (CheckDataExists > 0 )
                        //{
                        //    ModelState.AddModelError("", "There are some PR detail data has been closed by another user. You cannot use this PR detail data anymore!");
                        //}
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.porawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnporawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnporawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnporawdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnporawmst.Find(tbl.cmpcode, tbl.porawmstoid) != null)
                                tbl.porawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.porawdate);
                            //tbl.divoid = divoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.porawratetoidrchar = "0";
                            tbl.porawratetousdchar = "0";
                            tbl.porawrate2toidrchar = "0";
                            tbl.porawrate2tousdchar = "0";
                            tbl.porawtaxtype_inex = "0";
                            //tbl.porawmststatus_onpib = "";
                            db.QL_trnporawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.porawmstoid + " WHERE tablename='QL_trnporawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.porawratetoidrchar = "0";
                            tbl.porawratetousdchar = "0";
                            tbl.porawrate2toidrchar = "0";
                            tbl.porawrate2tousdchar = "0";
                            tbl.porawtaxtype_inex = "0";
                            //tbl.porawmststatus_onpib = "";
                            //--
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_prrawdtl SET prrawdtlstatus='', prrawdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND prrawdtloid IN (SELECT prrawdtloid FROM QL_trnporawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_prrawmst SET prrawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prrawmstoid IN (SELECT DISTINCT prrawmstoid FROM QL_trnporawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnporawdtl.Where(a => a.porawmstoid == tbl.porawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnporawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnporawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnporawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.porawdtloid = dtloid++;
                            tbldtl.porawmstoid = tbl.porawmstoid;
                            tbldtl.porawdtlseq = i + 1;
                            tbldtl.prrawmstoid = dtDtl[i].prrawmstoid;
                            tbldtl.prrawdtloid = dtDtl[i].prrawdtloid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.porawqty = dtDtl[i].porawqty;
                            tbldtl.porawunitoid = dtDtl[i].porawunitoid;
                            tbldtl.porawprice = dtDtl[i].porawprice;
                            tbldtl.porawdtlamt = dtDtl[i].porawdtlamt;
                            tbldtl.porawdtldisctype = dtDtl[i].porawdtldisctype;
                            tbldtl.porawdtldiscvalue = dtDtl[i].porawdtldiscvalue;
                            tbldtl.porawdtldiscamt = dtDtl[i].porawdtldiscamt;
                            tbldtl.porawdtlnetto = dtDtl[i].porawdtlnetto;
                            tbldtl.porawdtlnote = dtDtl[i].porawdtlnote == null ? "" : dtDtl[i].porawdtlnote;
                            //tbldtl.porawdtlnote2 = dtDtl[i].porawdtlnote2;
                            tbldtl.porawdtlstatus = "";
                            //tbldtl.porawdtlstatus_onpib = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnporawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].porawqty >= dtDtl[i].prrawqty)
                            {
                                sSql = "UPDATE QL_prrawdtl SET prrawdtlstatus='COMPLETE', prrawdtlres1='" + dtloid + "' WHERE cmpcode='" + tbl.cmpcode + "' AND prrawdtloid=" + dtDtl[i].prrawdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_prrawmst SET prrawmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND prrawmstoid=" + dtDtl[i].prrawmstoid + " AND (SELECT COUNT(prrawdtloid) FROM QL_prrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND prrawdtlstatus='' AND prrawmstoid=" + dtDtl[i].prrawmstoid + " AND prrawdtloid<>" + +dtDtl[i].prrawdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnporawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.porawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PO-RAW" + tbl.porawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnporawmst";
                                tblApp.oid = tbl.porawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        //if (tbl.porawmststatus.ToUpper() == "In Approval")
                        //{
                        //    var dt = null;
                        //    //Dim dt As DataTable = Session("AppPerson")
                        //    for (int i = 0; i < dtLastHdrData.Count(); i++)
                        //    {
                        //        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'PO-RAW" & porawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & porawdate.Text & "', 'New', 'QL_trnporawmst', " & porawmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();
                        //    }
                        //    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.porawmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.porawmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PORawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnporawmst tbl = db.QL_trnporawmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_prrawdtl SET prrawdtlstatus='', prrawdtlres1='', prclosingqty=0.0 WHERE cmpcode='" + tbl.cmpcode + "' AND prrawdtloid IN (SELECT prrawdtloid FROM QL_trnporawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_prrawmst SET prrawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND prrawmstoid IN (SELECT DISTINCT prrawmstoid FROM QL_trnporawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND porawmstoid=" + tbl.porawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = GetQueryInsertStatus_Multi(Session("UserID"), DDLBusUnit.SelectedValue, "QL_trnporawdtl", porawmstoid.Text, "Open", "porawmstoid", "QL_prrawmst", "prrawmstoid", "Open karena PO dihapus");
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnporawdtl.Where(a => a.porawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnporawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnporawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string supptype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnporawmst.Find(cmp, id);
            if (tbl == null)
                return null;

            if (supptype == "LOCAL")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPO_TrnEn.rpt"));

            //sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.porawmstoid [Oid], CONVERT(VARCHAR(10), pom.porawmstoid) AS [Draft No.], pom.porawno [PO No.], pom.porawdate [PO Date], pom.porawmstnote [Header Note], pom.porawmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.porawmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.porawmstdiscamt, 0) AS [Disc Amount], pom.porawtotalnetto [Total Netto], pom.porawtaxtype [Tax Type], CONVERT(VARCHAR(10), pom.porawtaxamt) AS [Tax Amount], ISNULL(pom.porawvat, 0) AS [VAT], ISNULL(pom.porawdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.porawothercost, 0) AS [Other Cost], pom.porawgrandtotalamt [Grand Total], pod.porawdtloid [DtlOid], ISNULL((SELECT m2.matrawdtl2code FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawcode) AS [Code], ISNULL((SELECT m2.matrawdtl2name FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawlongdesc) AS [Material], prm.prrawno AS [PR No.], prm.prrawdate AS [PR Date], pod.porawqty AS [Qty], g2.gendesc AS [Unit], pod.porawprice AS [Price], pod.porawdtlamt AS [Amount], ISNULL(pod.porawdtldiscamt, 0) AS [Detail Disc.], pod.porawdtlnetto AS [Netto], pod.porawdtlnote AS [Note], pom.porawtype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.porawsuppref AS [SuppReff], pod.porawdtldisctype AS [DtlDiscType], ISNULL(pod.porawdtldiscvalue,0) AS [DtlDiscValue], pod.porawdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prrawarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnporawmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.porawpaytypeoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid LEFT JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.porawunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.porawmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.porawmstoid, pod.porawdtlseq ";
            sSql = " SELECT pom.cmpcode [CMPCODE], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.porawmstoid [Oid], CONVERT(VARCHAR(10), pom.porawmstoid) AS [Draft No.], pom.porawno [PO No.], pom.porawdate [PO Date], pom.porawmstnote [Header Note], pom.porawmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.porawmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.porawmstdiscamt, 0) AS [Disc Amount], pom.porawtotalnetto [Total Netto], pom.porawtaxtype [Tax Type], CONVERT(VARCHAR(10), pom.porawtaxamt) AS [Tax Amount], ISNULL(pom.porawvat, 0) AS [VAT], ISNULL(pom.porawdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.porawothercost, 0) AS [Other Cost], pom.porawgrandtotalamt [Grand Total], pod.porawdtloid [DtlOid], matrawcode AS [Code], matrawlongdesc AS [Material], prm.prrawno AS [PR No.], prm.prrawdate AS [PR Date], pod.porawqty AS [Qty], g2.gendesc AS [Unit], pod.porawprice AS [Price], pod.porawdtlamt AS [Amount], ISNULL(pod.porawdtldiscamt, 0) AS [Detail Disc.], pod.porawdtlnetto AS [Netto], pod.porawdtlnote AS [Note], pom.porawtype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.porawsuppref AS [SuppReff], pod.porawdtldisctype AS [DtlDiscType], ISNULL(pod.porawdtldiscvalue,0) AS [DtlDiscValue], pod.porawdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prrawarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnporawmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.porawpaytypeoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid LEFT JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.porawunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser WHERE pom.cmpcode='" + cmp + "' AND pom.porawmstoid IN (" + id + ") ORDER BY pom.cmpcode, pom.porawmstoid, pod.porawdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintPORaw");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "PORawMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
