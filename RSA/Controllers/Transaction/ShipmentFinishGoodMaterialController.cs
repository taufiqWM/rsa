﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ShipmentFinishGoodMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class shipmentitemmst
        {
            public string cmpcode { get; set; }
            public int shipmentitemmstoid { get; set; }
            public string divgroup { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public string custname { get; set; }
            public string shipmentitemcontno { get; set; }
            public string shipmentitemmststatus { get; set; }
            public string shipmentitemmstnote { get; set; }
            public string divname { get; set; }
        }

        public class shipmentitemdtl
        {
            public int shipmentitemdtlseq { get; set; }
            public int soitemmstoid { get; set; }
            public int doitemmstoid { get; set; }
            public int doitemdtloid { get; set; }
            public string doitemno { get; set;}
            public string doitemdate { get; set; }
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
            public int soitemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public int doitemunitoid { get; set; }
            public string doitemunit { get; set; }
            public decimal stockqty { get; set; }
            public decimal doitemqty { get; set; }
            public decimal shipmentitemqty { get; set; }
            public int shipmentitemunitoid { get; set; }
            public string shipmentitemunit { get; set; }
            public string shipmentitemdtlnote { get; set; }
            public decimal shipmentitemvalueidr { get; set; }
            public decimal shipmentitemvalueusd { get; set; }
            public decimal itemlimitqty { get; set; }
            public string sn { get; set; }
            public DateTime expdate { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custtype { get; set; }
            public string custaddr { get; set; }
            public string soitemcustpono { get; set; }
        }
        public class listalamat
        {
            public int custdtloid { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
            public int custdtlcityoid { get; set; }
            public string custdtlcity { get; set; }
        }
        public class warehouse
        {
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
        }
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtloid, custdtlname, custdtladdr , custdtlcityoid , g.gendesc custdtlcity FROM QL_mstcustdtl c Left JOIN QL_mstgen g ON g.genoid=c.custdtlcityoid WHERE c.custoid = " + custoid + " ORDER BY custdtloid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private string generateNo(string cmp)
        {
            var sNo = "SJFG-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            var DefaultFormatCounter = 4;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentitemno, " + DefaultFormatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" + cmp + "' AND shipmentitemno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        private void InitDDL(QL_trnshipmentitemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;



            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0";
            var shipmentitemwhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", null);
            ViewBag.shipmentitemwhoid = shipmentitemwhoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND genoid>0"; //AND genother1='" + cmp +"'
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT DISTINCT cust.custoid, cust.custcode, cust.custname, cust.custtype, cust.custaddr, '' soitemcustpono FROM QL_mstcust cust INNER JOIN QL_trndoitemmst drm ON cust.cmpcode=drm.cmpcode AND cust.custoid=drm.custoid WHERE cust.cmpcode='" + CompnyCode + "' AND cust.divgroupoid=" + divgroupoid + " And cust.custoid IN (SELECT DISTINCT custoid FROM QL_trndoitemmst WHERE cmpcode='" + cmp + "' AND doitemmststatus='Post') ORDER BY custname";

            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDOData(string cmp, int custoid, string action, int shipmentitemmstoid, int divgroupoid)
        {
            List<QL_trndoitemmst> tbl = new List<QL_trndoitemmst>();
            sSql = "SELECT * FROM QL_trndoitemmst dom WHERE dom.cmpcode='" + cmp + "' AND dom.divgroupoid='"+divgroupoid+"' AND custoid=" + custoid;
            if (action == "Update Data")
            {
                sSql += " AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + shipmentitemmstoid + ")";
            } else
            {
                sSql += " AND doitemmststatus='Post'";
            }
            sSql += " ORDER BY doitemmstoid";

            tbl = db.Database.SqlQuery<QL_trndoitemmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int doitemmstoid, int shipmentitemwhoid, int shipmentitemmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<shipmentitemdtl> tbl = new List<shipmentitemdtl>();

            sSql = "SELECT som.doitemno, CONVERT(VARCHAR(10), som.doitemdate, 101) AS doitemdate, sod.doitemmstoid, sod.doitemdtloid, sod.doitemdtlseq, sod.itemoid, m.itemcode, m.itemlongdesc, " +
                "ISNULL(con.qty, 0.0) AS stockqty, " +
                "(sod.doitemqty - ISNULL((SELECT SUM(sd.shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sd.cmpcode = sod.cmpcode AND sm.shipmentitemmststatus <> 'Rejected' AND sd.doitemdtloid = sod.soitemdtloid AND sd.shipmentitemmstoid <> " + shipmentitemmstoid + "), 0.0)) AS doitemqty, " +
                "0.0 AS shipmentitemqty, sod.doitemunitoid [shipmentitemunitoid], g.gendesc[shipmentitemunit], ISNULL(sod.doitemdtlnote, '') [shipmentitemdtlnote], 1.0 itemlimitqty, con.sn " +
                "FROM QL_trndoitemdtl sod " +
                "INNER JOIN QL_trndoitemmst som ON som.cmpcode=sod.cmpcode AND som.doitemmstoid = sod.doitemmstoid " +
                "INNER JOIN QL_mstitem m ON m.cmpcode=sod.cmpcode AND m.itemoid = sod.itemoid " +
                "INNER JOIN QL_mstgen g ON g.cmpcode=sod.cmpcode AND g.genoid = sod.doitemunitoid " +
                "INNER JOIN (SELECT refoid, refname, refno sn, SUM(qtyin-qtyout) qty FROM QL_conmat WHERE cmpcode='" + cmp + "' AND refname='FINISH GOOD' AND mtrwhoid = "+  shipmentitemwhoid+" GROUP BY refoid, refname, refno  HAVING sum(qtyin-qtyout) > 0) con ON con.refoid=m.itemoid " +
                "WHERE sod.cmpcode = '" + cmp + "' AND sod.doitemmstoid = " + doitemmstoid;
            //if (action == "New Data") sSql += " AND sod.soitemdtlstatus=''";
            sSql += " ORDER BY doitemdtlseq ASC";

            decimal dSisa = 0;
            tbl = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();
            if (tbl.Count > 0)
            {
                var a = tbl.GroupBy(x => x.itemoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentitemqty), soitemqty = x.Select(y => y.doitemqty) }).ToList();

                for (var i = 0; i < tbl.Count(); i++)
                {
                    var tbldtl = tbl.Where(d => d.itemoid == tbl[i].itemoid).ToList();
                    for (var j = 0; j < tbldtl.Count(); j++)
                    {
                        if (dSisa == 0)
                            dSisa = tbl[i].doitemqty;

                        if (dSisa > tbl[i].stockqty)
                        {
                            tbl[i].shipmentitemqty = tbl[i].stockqty;
                            dSisa += tbl[i].doitemqty - tbl[i].stockqty;
                        }
                        else
                        {
                            tbl[i].shipmentitemqty = dSisa;
                        }
                    }                    
                }
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentitemdtl> dtDtl)
        {
            Session["QL_trnshipmentitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(int oid)
        {
            var result = "";
            JsonResult js = null;
            List<shipmentitemdtl> dtl = new List<shipmentitemdtl>();

            try
            {
                sSql = "SELECT sd.shipmentitemdtlseq, sd.doitemmstoid, dom.doitemno, CONVERT(VARCHAR(10), dom.doitemdate, 101) AS doitemdate, sd.shipmentitemwhoid, g1.gendesc AS shipmentitemwh, sd.doitemdtloid, sd.itemoid, m.itemcode, m.itemlongdesc, 0.0 itemlimitqty, " +
                    "ISNULL((SELECT SUM(qtyin-qtyout) qty FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refoid=sd.itemoid AND refname='FINISH GOOD' GROUP BY refoid), 0.0) AS stockqty, " +
                    "(dod.doitemqty - ISNULL((SELECT SUM(sdx.shipmentitemqty) FROM QL_trnshipmentitemdtl sdx INNER JOIN QL_trnshipmentitemmst smx ON smx.cmpcode = sdx.cmpcode AND smx.shipmentitemmstoid = sdx.shipmentitemmstoid WHERE sdx.cmpcode = sd.cmpcode AND smx.shipmentitemmststatus <> 'Rejected' AND sdx.doitemdtloid = sd.doitemdtloid AND sdx.shipmentitemmstoid <> sd.shipmentitemmstoid), 0.0)) AS doitemqty, " +
                    "sd.shipmentitemqty, sd.shipmentitemunitoid, g2.gendesc AS shipmentitemunit, sd.shipmentitemdtlnote, sd.sn " +
                    "FROM QL_trnshipmentitemdtl sd " +
                    "INNER JOIN QL_trndoitemmst dom ON dom.cmpcode = sd.cmpcode AND dom.doitemmstoid = sd.doitemmstoid " +
                    "INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode = sd.cmpcode AND dod.doitemdtloid = sd.doitemdtloid " +
                    "INNER JOIN QL_mstitem m ON m.itemoid = sd.itemoid " +
                    "INNER JOIN QL_mstgen g1 ON g1.genoid = sd.shipmentitemwhoid " +
                    "INNER JOIN QL_mstgen g2 ON g2.genoid = sd.shipmentitemunitoid " +
                    "WHERE sd.shipmentitemmstoid =" + oid + " ORDER BY sd.shipmentitemdtlseq";
                dtl = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();
                Session["QL_trnshipmentitemdtl"] = dtl;
            }
            catch(Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private void FillAdditionalField(QL_trnshipmentitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.curroid = db.Database.SqlQuery<int>("SELECT curroid FROM QL_trnshipmentitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + "").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtladdr FROM QL_mstcustdtl a WHERE a.custdtloid ='" + tbl.custdtloid + "'").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: shipmentitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "som", divgroupoid, "divgroupoid");

            sSql = "SELECT som.cmpcode, som.shipmentitemmstoid, som.shipmentitemno, som.shipmentitemdate, c.custname, som.shipmentitemmststatus, som.shipmentitemmstnote, div.divname FROM QL_TRNshipmentitemMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND shipmentitemmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND shipmentitemmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND shipmentitemmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND shipmentitemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND shipmentitemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND shipmentitemmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY som.shipmentitemdate DESC, som.shipmentitemmstoid DESC";

            List<shipmentitemmst> dt = db.Database.SqlQuery<shipmentitemmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnshipmentitemmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: shipmentitemMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnshipmentitemmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnshipmentitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.shipmentitemmstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                tbl.shipmentitemdate = ClassFunction.GetServerTime();
                tbl.shipmentitemetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.shipmentitemeta = ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.vessel = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.datetrans1 = new DateTime(1900, 1, 1);
                tbl.datetrans2 = new DateTime(1900, 1, 1);
                tbl.datetrans3 = new DateTime(1900, 1, 1);
                tbl.loadcont = new DateTime(1900, 1, 1);
                tbl.shipmentitemmststatus = "In Process";
                
                Session["QL_trnshipmentitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnshipmentitemmst.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.shipmentitemno == null)
                tbl.shipmentitemno = "";

            List<shipmentitemdtl> dtDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (Convert.ToDateTime(tbl.shipmentitemetd.ToString("MM/dd/yyyy")) > Convert.ToDateTime(tbl.shipmentitemeta.ToString("MM/dd/yyyy")))
            {
                ModelState.AddModelError("", "ETD must be less than ETA!");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].shipmentitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY must be more than 0!");
                        }
                        else
                        {
                            var totalqty = dtDtl.Where(x => x.itemoid == dtDtl[i].itemoid && x.shipmentitemwhoid == dtDtl[i].shipmentitemwhoid && x.doitemdtloid==dtDtl[i].doitemdtloid).Sum(x => x.shipmentitemqty);
                            if (totalqty > dtDtl[i].doitemqty)
                            {
                                ModelState.AddModelError("", "QTY FG"+dtDtl[i].itemlongdesc +" Batch. "+ dtDtl[i].sn +" must be less than DO QTY!");
                            }
                           
                        }

                        sSql = "SELECT (doitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sd.cmpcode = dod.cmpcode AND shipmentitemmststatus<> 'Rejected' AND sd.doitemdtloid = dod.soitemdtloid AND sd.shipmentitemmstoid <> " + tbl.shipmentitemmstoid + "), 0.0)) AS doitemqty FROM QL_trndoitemdtl dod WHERE dod.cmpcode = '" + tbl.cmpcode + "' AND doitemdtloid = " + dtDtl[i].doitemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].doitemqty)
                            dtDtl[i].doitemqty = dQty;
                        if (dQty < dtDtl[i].shipmentitemqty)
                            ModelState.AddModelError("", "Some DO Qty has been updated by another user. Please check that every Qty must be less than DO Qty!");

                        if (tbl.shipmentitemmststatus == "Post")
                        {
                            if (!ClassFunction.IsStockAvailableWithBatch(tbl.cmpcode, ClassFunction.GetServerTime(), dtDtl[i].itemoid, dtDtl[i].shipmentitemwhoid, dtDtl[i].shipmentitemqty, "FINISH GOOD", dtDtl[i].sn))  
                                ModelState.AddModelError("", "Every Qty must be less than Stock Qty before Post this data!");

                            dtDtl[i].shipmentitemvalueidr = ClassFunction.GetStockValueWithBatch(CompnyCode, "FINISH GOOD", dtDtl[i].itemoid, dtDtl[i].sn);
                        }
                        else
                        {
                            dtDtl[i].shipmentitemvalueidr = 0;
                            dtDtl[i].shipmentitemvalueusd = 0;
                        }
                    }
                }
            }

            // VAR UTK APPROVAL
            /*var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.shipmentitemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnshipmentitemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }*/
            // END VAR UTK APPROVAL

            if (tbl.shipmentitemmststatus == "Revised")
                tbl.shipmentitemmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.shipmentitemmststatus = "In Process";

            if (tbl.shipmentitemmststatus == "Post")
            {
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode, tbl.divgroupoid.Value))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", CompnyCode, tbl.divgroupoid.Value))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_FG"));                
            }                

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentitemdtl");
                var iConOid = ClassFunction.GenerateID("QL_conmat");
                var iStockValOid = ClassFunction.GenerateID("QL_stockvalue");
                var iGlMstOid = ClassFunction.GenerateID("QL_trnglmst");
                var iGlDtlOid = ClassFunction.GenerateID("QL_trngldtl");
                var iDelAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode, tbl.divgroupoid.Value));
                var iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", CompnyCode, tbl.divgroupoid.Value));
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;
                        tbl.datetrans1 = tbl.datetrans1 == null ? new DateTime(1900, 1, 1) : tbl.datetrans1;
                        tbl.datetrans2 = tbl.datetrans2 == null ? new DateTime(1900, 1, 1) : tbl.datetrans2;
                        tbl.datetrans3 = tbl.datetrans3 == null ? new DateTime(1900, 1, 1) : tbl.datetrans3;
                        tbl.countrans1 = tbl.countrans1 == null ? "" : tbl.countrans1;
                        tbl.countrans2 = tbl.countrans2 == null ? "" : tbl.countrans2;
                        tbl.countrans3 = tbl.countrans3 == null ? "" : tbl.countrans3;
                        tbl.vessel = tbl.vessel == null ? "" : tbl.vessel;
                        tbl.bol = tbl.bol == null ? "" : tbl.bol;
                        tbl.mvessel = tbl.mvessel == null ? "" : tbl.mvessel;
                        tbl.shipline = tbl.shipline == null ? "" : tbl.shipline;

                        if (tbl.shipmentitemmststatus == "Post")
                            tbl.shipmentitemno = generateNo(CompnyCode);


                        if (action == "New Data")
                        {
                            if (db.QL_trnshipmentitemmst.Find(tbl.cmpcode, tbl.shipmentitemmstoid) != null)
                                tbl.shipmentitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnshipmentitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.shipmentitemmstoid + " WHERE tablename='QL_trnshipmentitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == tbl.shipmentitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnshipmentitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.shipmentitemdtloid = dtloid++;
                            tbldtl.shipmentitemmstoid = tbl.shipmentitemmstoid;
                            tbldtl.shipmentitemdtlseq = i + 1;
                            tbldtl.doitemmstoid = dtDtl[i].doitemmstoid;
                            tbldtl.doitemdtloid = dtDtl[i].doitemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.shipmentitemqty = dtDtl[i].shipmentitemqty;
                            tbldtl.shipmentitemunitoid = dtDtl[i].shipmentitemunitoid;
                            tbldtl.shipmentitemwhoid = dtDtl[i].shipmentitemwhoid;
                            tbldtl.shipmentitemdtlstatus = "";
                            tbldtl.shipmentitemdtlnote = (dtDtl[i].shipmentitemdtlnote == null ? "" : dtDtl[i].shipmentitemdtlnote);
                            tbldtl.sn = dtDtl[i].sn;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentitemdtlres3 = "";
                            tbldtl.shipmentitemvalueidr = 0;
                            tbldtl.shipmentitemvalueusd = 0;

                            tbldtl.custpo = "";
                            tbldtl.containerno = "";
                            tbldtl.mattype = "";
                            tbldtl.shipmentextname = "";
                            tbldtl.dotype = "";

                            db.QL_trnshipmentitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.shipmentitemmststatus == "Post")
                            {

                                if (dtDtl[i].doitemqty > dtDtl[i].shipmentitemqty)
                                {
                                    sSql = "update ql_trnsoitemdtl set soitemdtlstatus='' where soitemdtloid= (select soitemdtloid from ql_trndoitemdtl where doitemdtloid=" + dtDtl[i].doitemdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "update ql_trnsoitemmst set soitemmststatus='Approved' where soitemmstoid= (select distinct soitemmstoid from ql_trndoitemdtl where doitemdtloid=" + dtDtl[i].doitemdtloid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                }
                            }


                            sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND doitemmstoid=" + dtDtl[i].doitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND doitemmstoid=" + dtDtl[i].doitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.shipmentitemmststatus == "Post")
                            {
                                sSql = "SELECT ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat WHERE cmpcode='" + CompnyCode + "' AND refoid=" + dtDtl[i].itemoid + " AND refname = 'FINISH GOOD' AND refno = '" + dtDtl[i].sn + "'),0.0) ";
                                var stockAkhir = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                                //Insert conmat
                                int dodtl_id = dtDtl[i].doitemdtloid;
                                int so_id = db.QL_trndoitemdtl.FirstOrDefault(x => x.doitemdtloid == dodtl_id)?.soitemmstoid ?? 0;
                                int wo_id = db.QL_trnwomst.FirstOrDefault(x => x.somstoid == so_id && x.sotype == "QL_trnsoitemmst")?.womstoid ?? 0;
                                db.QL_conmat.Add(ClassFunction.InsertConMatWithBatch(CompnyCode, iConOid++, "SJFG", "QL_trnshipmentitemdtl", tbl.shipmentitemmstoid, dtDtl[i].itemoid, "FINISH GOOD", dtDtl[i].shipmentitemwhoid, (dtDtl[i].shipmentitemqty * -1), "Shipment Finish Good", "SJFG | No. "+ tbl.shipmentitemno + " | Note. "+ dtDtl[i].shipmentitemdtlnote +"", Session["UserID"].ToString(), dtDtl[i].sn, dtDtl[i].shipmentitemvalueidr, dtDtl[i].shipmentitemvalueusd, wo_id, null, tbldtl.shipmentitemdtloid, tbl.divgroupoid??0,dtDtl[i].sn, dtDtl[i].expdate.ToString(), cust_id: tbl.custoid));

                                // Update/Insert QL_stockvalue
                                sSql = ClassFunction.GetQueryUpdateStockValue(dtDtl[i].shipmentitemqty * -1, dtDtl[i].shipmentitemvalueidr, dtDtl[i].shipmentitemvalueusd, "QL_trnshipmentitemdtl", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtDtl[i].itemoid, "FINISH GOOD", dtDtl[i].sn);
                                if (db.Database.ExecuteSqlCommand(sSql) <= 0)
                                {
                                    db.SaveChanges();
                                    sSql = ClassFunction.GetQueryInsertStockValue(dtDtl[i].shipmentitemqty * -1, dtDtl[i].shipmentitemvalueidr, dtDtl[i].shipmentitemvalueusd, "QL_trnshipmentitemdtl", servertime, Session["UserID"].ToString(), CompnyCode, sPeriod, dtDtl[i].itemoid, "FINISH GOOD", iStockValOid++, dtDtl[i].sn, dtDtl[i].expdate.ToString(), stockAkhir);
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                        }

                        if (tbl.shipmentitemmststatus == "Post")
                        {
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, iGlMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Shipment FG|No. " + tbl.shipmentitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 1, 1, 1, 1, 1, 1,tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            decimal glamt = dtDtl.Sum(x => x.shipmentitemqty * x.shipmentitemvalueidr);
                            decimal glamtidr = dtDtl.Sum(x => x.shipmentitemqty * x.shipmentitemvalueidr);
                            decimal glamtusd = dtDtl.Sum(x => x.shipmentitemqty * x.shipmentitemvalueusd);
                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid++, glseq++, iGlMstOid, iDelAcctgOid, "D", glamt, tbl.shipmentitemno, "Shipment FG|No. " + tbl.shipmentitemno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid++, glseq++, iGlMstOid, iStockAcctgOid, "C", glamt, tbl.shipmentitemno, "Shipment FG|No. " + tbl.shipmentitemno, "Post", Session["UserID"].ToString(), servertime, glamtidr, glamtusd, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            iGlMstOid++;

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iConOid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iStockValOid - 1) + " WHERE tablename='QL_stockvalue'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + iGlMstOid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iGlDtlOid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }                        

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnshipmentitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        

                        // INSERT APPROVAL
                        /*if (tbl.shipmentitemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SHP-RM" + tbl.shipmentitemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnshipmentitemmst";
                                tblApp.oid = tbl.shipmentitemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }*/
                        // END INSERT APPROVAL

                        objTrans.Commit();

                        using (var objTrans2 = db.Database.BeginTransaction())
                        {
                            try
                            {
                                if (tbl.shipmentitemmststatus == "Post")
                                {
                                    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trndoitemdtl WHERE cmpcode='" + CompnyCode + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ") AND doitemdtloid NOT IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trndoitemdtl WHERE cmpcode='" + CompnyCode + "' AND doitemmstoid IN (SELECT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ") AND doitemdtloid NOT IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + "))";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    objTrans2.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                objTrans2.Rollback();
                                tbl.shipmentitemmststatus = "In Process";
                                return View(ex.ToString());
                            }
                        }

                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.shipmentitemmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl = db.QL_trnshipmentitemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + cmp + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + cmp + "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + cmp + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnshipmentitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype,string printtype2)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnshipmentitemmst.Find(cmp, id);
            if (tbl == null)
                return null;
            var sReportName = "";
            if (printtype == "A4")
                sReportName = "rptShipment_Trn.rpt";
            else
                sReportName = "rptShipment_TrnA5.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            sSql = "SELECT sm.shipmentitemmstoid AS [MstOid], CONVERT(VARCHAR(10), sm.shipmentitemmstoid) AS [Draft No.], sm.shipmentitemno AS [Shipment No.], isnull(cd.custdtladdr,'') [Pengiriman], sm.shipmentitemdate AS [Shipment Date], sm.shipmentitemmstnote AS [Header Note], sm.shipmentitemmststatus AS [Status], [Customer] AS [Customer], sd.shipmentitemdtloid AS [DtlOid], sd.shipmentitemdtlseq AS [No.], m.itemoid AS [MatOid], m.itemcode AS [Code], m.itemlongdesc AS [Material], sd.shipmentitemqty AS [Qty], g2.gendesc AS [Unit], g3.gendesc AS [Warehouse], sd.shipmentitemdtlnote AS [Note], sm.cmpcode, [BU Name], [BU Address], [BU City], [BU Province], [BU Country], [BU Telp. 1], [BU Telp. 2], [BU Fax 1], [BU Fax 2], [BU Email], [BU Post Code], shipmentitemeta [ETA], shipmentitemetd [ETD], shipmentitemcontno [Container No.], shipmentitemportship [Port Ship], shipmentitemportdischarge [Port Discharge], shipmentitemnopol [Vehicle Police No.], [Customer Type], [Customer], [Customer Address], [Customer City],[Customer Province], [Customer Country], [Customer Phone1], [Customer Phone2], [Customer Phone3], [Customer Fax], [Customer Website], [Customer CP1], [Customer CP2], [Customer CPPhone1], [Customer CPPhone2], [Customer CPEmail1], [Customer CPEmail2], sd.sn AS [BATCH NO] FROM QL_trnshipmentitemmst sm  INNER JOIN (SELECT custoid, custcode [Customer Code],custtype [Customer Type],custname [Customer],custaddr [Customer Address],custcityoid,g.gendesc [Customer City],g2.gendesc [Customer Province] ,g3.gendesc [Customer Country],custphone1 [Customer Phone1],custphone2 [Customer Phone2],custphone3 [Customer Phone3],custfax1 [Customer Fax],custwebsite [Customer Website],custcp1name [Customer CP1],custcp2name [Customer CP2],custcp1phone [Customer CPPhone1],custcp2phone [Customer CPPhone2],custcp1email [Customer CPEmail1],custcp2email [Customer CPEmail2] FROM QL_mstcust c  " +
            "INNER JOIN QL_mstgen g ON g.genoid = custcityoid AND g.gengroup = 'City' " +
            "INNER JOIN QL_mstgen g2 ON g2.cmpcode = g.cmpcode AND g2.genoid = CONVERT(INT, g.genother1) " +
            "INNER JOIN QL_mstgen g3 ON g3.cmpcode = g.cmpcode AND g3.genoid = CONVERT(INT, g.genother2))AS TblCust ON TblCust.custoid = sm.custoid " +
            "INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode = sm.cmpcode AND sd.shipmentitemmstoid = sm.shipmentitemmstoid " +
            "INNER JOIN QL_mstitem m ON m.itemoid = sd.itemoid " +
            "INNER JOIN QL_mstgen g2 ON g2.genoid = sd.shipmentitemunitoid " +
            "INNER JOIN QL_mstgen g3 ON g3.genoid = sd.shipmentitemwhoid " +
            " Left Join QL_mstcustdtl cd on cd.custdtloid=sm.custdtloid " +
            " inner join QL_trndoitemdtl dod on dod.doitemdtloid=sd.doitemdtloid " +
            "INNER JOIN (SELECT di.cmpcode, divname AS[BU Name], divaddress AS[BU Address], g1.gendesc AS[BU City], g2.gendesc AS[BU Province], g3.gendesc AS[BU Country], ISNULL(divphone, '') AS[BU Telp. 1], ISNULL(divphone2, '') AS[BU Telp. 2], ISNULL(divfax1, '') AS[BU Fax 1], ISNULL(divfax2, '') AS[BU Fax 2], ISNULL(divemail, '') AS[BU Email], ISNULL(divpostcode, '') AS[BU Post Code]  FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid = divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode = g1.cmpcode AND g2.genoid = CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode = g1.cmpcode AND g3.genoid = CONVERT(INT, g1.genother2)  " +
            ") tblDiv ON tblDiv.cmpcode = sm.cmpcode " +
            "WHERE sm.cmpcode = '" + cmp + "' AND sm.shipmentitemmstoid = " + id + "";
           if (printtype2 != "ALL")
            {
                sSql += " AND dod.doitemprice>0 ";
            }
            sSql+=" ORDER BY sm.shipmentitemmstoid, sd.shipmentitemdtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptShipmentFG");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("Header", "PRINT OUT SURAT JALAN");
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ShipmentitemMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}