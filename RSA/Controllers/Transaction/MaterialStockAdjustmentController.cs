﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class MaterialStockAdjustmentController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class adjmst
        {
            public string divgroup { get; set; }
            public int adjmstoid { get; set; }
            public string adjno { get; set; }
            public DateTime adjdate { get; set; }
            public string adjmstnote { get; set; }
            public string adjmststatus { get; set; }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "h", divgroupoid, "divgroupoid");
            sSql = "SELECT adjmstoid, (select gendesc from ql_mstgen where genoid=h.divgroupoid) divgroup, adjno, adjdate, adjmstnote, adjmststatus FROM QL_trnadjmst h WHERE 1=1" ;
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND adjmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND adjmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND adjmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND adjdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND adjdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND adjmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND h.createuser='" + Session["UserID"].ToString() + "'";

            List<adjmst> dt = db.Database.SqlQuery<adjmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnadjmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        public class adjdtl : QL_trnadjdtl
        {
            public string matcode { get; set; }
            public string matdesc { get; set; }
            public string adjunit { get; set; }
            public string adjwh { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int divgroupoid, string refname, int whoid, string adjdate)
        {
            JsonResult js = null;
            try
            {
                if (refname == "RAW MATERIAL")
                {
                    sSql = $"select 0 adjdtlseq, 0 adjdtloid, '{refname}' refname, m.matrawoid refoid, m.matrawcode matcode, m.matrawlongdesc matdesc, {whoid} adjwhoid, isnull((select x.gendesc from QL_mstgen x where x.genoid = {whoid}),'') adjwh, isnull(saldoqty,0.0) adjqtybefore, 0.0 adjqty, 0.0 adjqtyafter, m.matrawunitoid adjunitoid, g.gendesc adjunit, isnull(con.refno,'') refno, isnull(con.serialno,'') serialno, isnull(con.valueidr,0.0) adjvalueidr, 0.0 adjvalueusd, '' adjdtlnote from QL_mstmatraw m inner join QL_mstgen g on g.genoid=m.matrawunitoid outer apply( select c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr, sum(qtyin - qtyout) saldoqty from QL_conmat c where c.divgroupoid={divgroupoid} and c.refname = '{refname}' and c.refoid = m.matrawoid and c.mtrwhoid = {whoid} and c.updtime <= '{adjdate} 23:59:59' group by c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr )AS con where m.divgroupoid={divgroupoid}";
                }
                else if (refname == "GENERAL MATERIAL")
                {
                    sSql = $"select 0 adjdtlseq, 0 adjdtloid, '{refname}' refname, m.matgenoid refoid, m.matgencode matcode, m.matgenlongdesc matdesc, {whoid} adjwhoid, isnull((select x.gendesc from QL_mstgen x where x.genoid = {whoid}),'') adjwh, isnull(saldoqty,0.0) adjqtybefore, 0.0 adjqty, 0.0 adjqtyafter, m.matgenunitoid adjunitoid, g.gendesc adjunit, isnull(con.refno,'') refno, isnull(con.serialno,'') serialno, isnull(con.valueidr,0.0) adjvalueidr, 0.0 adjvalueusd, '' adjdtlnote from QL_mstmatgen m inner join QL_mstgen g on g.genoid=m.matgenunitoid outer apply( select c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr, sum(qtyin - qtyout) saldoqty from QL_conmat c where c.divgroupoid={divgroupoid} and c.refname = '{refname}' and c.refoid = m.matgenoid and c.mtrwhoid = {whoid} and c.updtime <= '{adjdate} 23:59:59' group by c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr )AS con where m.divgroupoid={divgroupoid}";
                }
                else if (refname == "FINISH GOOD")
                {
                    sSql = $"select 0 adjdtlseq, 0 adjdtloid, '{refname}' refname, m.itemoid refoid, m.itemcode matcode, m.itemlongdesc matdesc, {whoid} adjwhoid, isnull((select x.gendesc from QL_mstgen x where x.genoid = {whoid}),'') adjwh, isnull(saldoqty,0.0) adjqtybefore, 0.0 adjqty, 0.0 adjqtyafter, m.itemunitoid adjunitoid, g.gendesc adjunit, isnull(con.refno,'') refno, isnull(con.serialno,'') serialno, isnull(con.valueidr,0.0) adjvalueidr, 0.0 adjvalueusd, '' adjdtlnote from QL_mstitem m inner join QL_mstgen g on g.genoid=m.itemunitoid outer apply( select c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr, sum(qtyin - qtyout) saldoqty from QL_conmat c where c.divgroupoid={divgroupoid} and c.refname = '{refname}' and c.refoid = m.itemoid and c.mtrwhoid = {whoid} and c.updtime <= '{adjdate} 23:59:59' group by c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr )AS con where m.divgroupoid={divgroupoid}";
                }
                else if (refname == "SHEET")
                {
                    sSql = $"select 0 adjdtlseq, 0 adjdtloid, '{refname}' refname, m.sorawdtloid refoid, '' matcode, m.sodtldesc matdesc, {whoid} adjwhoid, isnull((select x.gendesc from QL_mstgen x where x.genoid = {whoid}),'') adjwh, isnull(saldoqty,0.0) adjqtybefore, 0.0 adjqty, 0.0 adjqtyafter, m.sorawunitoid djunitoid, g.gendesc adjunit, isnull(con.refno,'') refno, isnull(con.serialno,'') serialno, isnull(con.valueidr,0.0) adjvalueidr, 0.0 adjvalueusd, '' adjdtlnote from QL_trnsorawdtl m inner join QL_mstgen g on g.genoid=m.sorawunitoid outer apply( select c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr, sum(qtyin - qtyout) saldoqty from QL_conmat c where c.divgroupoid={divgroupoid} and c.refname = '{refname}' and c.refoid = m.sorawdtloid and c.mtrwhoid = {whoid} and c.updtime <= '{adjdate} 23:59:59' group by c.refname, c.refoid, c.refno, c.serialno, c.mtrwhoid, c.valueidr )AS con where m.divgroupoid={divgroupoid}";
                }
                var tbl = db.Database.SqlQuery<adjdtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<adjdtl>();

            try
            {
                sSql = $"SELECT * FROM( select d.adjdtlseq, d.adjdtloid, d.refname, d.refoid, m.matrawcode matcode, m.matrawlongdesc matdesc, adjwhoid, g.gendesc adjwh, d.adjqtybefore, d.adjqty, d.adjqtyafter, d.adjunitoid, g2.gendesc adjunit, d.refno, d.serialno, d.adjvalueidr, d.adjvalueusd, d.adjdtlnote from QL_trnadjdtl d inner join QL_trnadjmst h on h.adjmstoid=d.adjmstoid inner join QL_mstmatraw m on m.matrawoid=d.refoid inner join QL_mstgen g on g.genoid=d.adjwhoid  inner join QL_mstgen g2 on g2.genoid=d.adjunitoid where h.cmpcode='{CompnyCode}' and d.adjmstoid={id} and d.refname='RAW MATERIAL'  union all  select d.adjdtlseq, d.adjdtloid, d.refname, d.refoid, m.matgencode matcode, m.matgenlongdesc matdesc, adjwhoid, g.gendesc adjwh, d.adjqtybefore, d.adjqty, d.adjqtyafter, d.adjunitoid, g2.gendesc adjunit, d.refno, d.serialno, d.adjvalueidr, d.adjvalueusd, d.adjdtlnote from QL_trnadjdtl d inner join QL_trnadjmst h on h.adjmstoid=d.adjmstoid inner join QL_mstmatgen m on m.matgenoid=d.refoid inner join QL_mstgen g on g.genoid=d.adjwhoid  inner join QL_mstgen g2 on g2.genoid=d.adjunitoid where h.cmpcode='{CompnyCode}' and d.adjmstoid={id} and d.refname='GENERAL MATERIAL'  union all  select d.adjdtlseq, d.adjdtloid, d.refname, d.refoid, m.itemcode matcode, m.itemlongdesc matdesc, adjwhoid, g.gendesc adjwh, d.adjqtybefore, d.adjqty, d.adjqtyafter, d.adjunitoid, g2.gendesc adjunit, d.refno, d.serialno, d.adjvalueidr, d.adjvalueusd, d.adjdtlnote from QL_trnadjdtl d inner join QL_trnadjmst h on h.adjmstoid=d.adjmstoid inner join QL_mstitem m on m.itemoid=d.refoid inner join QL_mstgen g on g.genoid=d.adjwhoid  inner join QL_mstgen g2 on g2.genoid=d.adjunitoid where h.cmpcode='{CompnyCode}' and d.adjmstoid={id} and d.refname='FINISH GOOD'  union all  select d.adjdtlseq, d.adjdtloid, d.refname, d.refoid, '' matcode, m.sodtldesc matdesc, adjwhoid, g.gendesc adjwh, d.adjqtybefore, d.adjqty, d.adjqtyafter, d.adjunitoid, g2.gendesc adjunit, d.refno, d.serialno, d.adjvalueidr, d.adjvalueusd, d.adjdtlnote from QL_trnadjdtl d inner join QL_trnadjmst h on h.adjmstoid=d.adjmstoid inner join QL_trnsorawdtl m on m.sorawdtloid=d.refoid inner join QL_mstgen g on g.genoid=d.adjwhoid  inner join QL_mstgen g2 on g2.genoid=d.adjunitoid where h.cmpcode='{CompnyCode}' and d.adjmstoid={id} and d.refname='SHEET' )AS dt ORDER BY dt.adjdtlseq";
                dtl = db.Database.SqlQuery<adjdtl>(sSql).ToList();

                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void InitDDL(QL_trnadjmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA("VAR_STOCK_ADJUSTMENT")).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT ifield, sfield FROM ( SELECT genoid ifield, gendesc sfield FROM QL_mstgen WHERE gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' UNION ALL SELECT genoid ifield, gendesc sfield FROM QL_mstgen WHERE gengroup='ITEM LOCATION' AND activeflag='ACTIVE' ) AS t ORDER BY sfield";
            var whoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield");
            ViewBag.whoid = whoid;
        }

        private void FillAdditionalField(QL_trnadjmst tbl)
        {
            
        }

        private string generateNo()
        {
            var sNo = $"ADJ-{ClassFunction.GetServerTime().ToString("yyyy.MM")}-";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>($"SELECT ISNULL(MAX(CAST(RIGHT(adjno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnadjmst WHERE cmpcode='{CompnyCode}' AND adjno LIKE '{sNo}%'").FirstOrDefault(), 6);
            sNo = sNo + sCounter;
            return sNo;
        }

        public ActionResult Form(string id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnadjmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnadjmst();
                tbl.adjmstoid = ClassFunction.GenerateID("QL_trnadjmst");
                tbl.adjdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.adjmststatus = "In Process";

            }
            else
            {
                action = "Update Data";
                int id2 = id.toInteger();
                tbl = db.QL_trnadjmst.Find(id2);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnadjmst tbl, List<adjdtl> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.adjno)) tbl.adjno = "";
            if (string.IsNullOrEmpty(tbl.adjmstnote)) tbl.adjmstnote = "";
            if (tbl.adjmststatus.ToLower() == "revised") tbl.adjmststatus = "In Process";

            if (dtDtl == null) msg += "- Please fill detail data!<br />";
            else if (dtDtl.Count <= 0) msg += "- Please fill detail data!<br />";
            else
            {
                foreach (var item in dtDtl)
                {
                    if (string.IsNullOrEmpty(item.serialno)) msg += "- Silahkan Isi Serial No!<br />";
                    if (item.adjvalueidr < 0) msg += "- Value tidak boleh minus!<br />";
                    if (item.adjqtyafter <= 0) msg += "- Real Stock tidak boleh 0 atau minus!<br />";
                    else
                    {
                        if (tbl.adjmststatus.ToLower() == "post")
                        {
                            if(item.adjqty < 0)
                            {
                                if (!ClassFunction.IsStockAvailableWithSN(tbl.cmpcode, servertime, item.refoid, item.adjwhoid, Math.Abs(item.adjqty), item.refname, item.refno ?? "", item.serialno?? ""))
                                {
                                    msg += $"- QTY Material {item.matcode} {item.matdesc} must be less than STOCK QTY!<BR>";
                                }
                                item.adjvalueidr = ClassFunction.GetStockValueWithSN(CompnyCode, item.refname, item.refoid, item.refno ?? "", item.serialno ?? "");
                            }
                        }
                    }
                }
            }

            var iAcctgoidRM = 0; var iAcctgoidGM = 0; var iAcctgoidFG = 0;
            if (tbl.adjmststatus.ToLower() == "post")
            {
                tbl.adjno = generateNo();
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_GM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_FG") + "<BR>";
                }
                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid));
                iAcctgoidGM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid));
                iAcctgoidFG = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tbl.cmpcode, tbl.divgroupoid));
            }

            if (msg == "")
            {
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.adjdate);
                tbl.approvalcode = "";
                tbl.approvaluser = "";
                tbl.approvaldatetime = new DateTime(1900,1,1);
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);

                var sDateGL = servertime.ToString("MM/dd/yyyy").ToDateTime();
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
                var mstoid = ClassFunction.GenerateID("QL_trnadjmst");
                var dtloid = ClassFunction.GenerateID("QL_trnadjdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.adjmstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnadjmst.Add(tbl);
                            db.SaveChanges();
                            
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid = {tbl.adjmstoid} WHERE tablename = 'QL_trnadjmst'");
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnadjdtl.Where(a => a.adjmstoid == tbl.adjmstoid);
                            db.QL_trnadjdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnadjdtl tbldtl; var seq = 1;
                        foreach (var item in dtDtl)
                        {
                            tbldtl = (QL_trnadjdtl)ClassFunction.MappingTable(new QL_trnadjdtl(), item);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.adjdtloid = dtloid++;
                            tbldtl.adjmstoid = tbl.adjmstoid;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.adjdtlseq = seq++;
                            tbldtl.refno = (string.IsNullOrEmpty(item.refno) ? tbl.adjno : item.refno);
                            tbldtl.adjvalueusd = 0;
                            tbldtl.adjdtlstatus = "";
                            tbldtl.adjdtlnote = item.adjdtlnote ?? "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnadjdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.adjmststatus.ToLower() == "post")
                            {
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "ADJ", "QL_trnadjmst", tbl.adjmstoid, tbldtl.refoid, tbldtl.refname, tbldtl.adjwhoid, tbldtl.adjqty, $"Adjustment {tbldtl.refname}", tbl.adjno, tbl.upduser, tbldtl.refno, "", tbldtl.adjvalueidr, tbldtl.adjvalueusd, 0, null, tbldtl.adjdtloid, tbl.divgroupoid, tbldtl.serialno));
                                db.SaveChanges();
                            }
                        }
                        db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid = {dtloid - 1} WHERE tablename='QL_trnadjdtl'");
                        db.SaveChanges();

                        if (tbl.adjmststatus.ToLower() == "post")
                        {
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={conmatoid - 1} WHERE tablename='QL_conmat'");
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDateGL, sPeriod, "Adjustment|No. " + tbl.adjno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid));
                            db.SaveChanges();

                            int iSeq = 1;

                            var glplus = dtDtl.Where(w => w.adjqty >= 0).GroupBy(x => x.refname).Select(x => new { Nama = x.Key, Amt = x.Sum(y => (y.adjvalueidr * y.adjqty)), AmtUSD = 0 });
                            if (glplus.Any())
                            {
                                foreach (var gl in glplus.ToList())
                                {
                                    int iAcctgOid = (gl.Nama == "RAW MATERIAL" ? iAcctgoidRM : gl.Nama == "GENERAL MATERIAL" ? iAcctgoidGM : iAcctgoidFG);
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgOid, "D", gl.Amt, tbl.adjno, "Adjustment|No. " + tbl.adjno + "", "Post", tbl.upduser, servertime, gl.Amt, gl.AmtUSD, "QL_trnadjmst " + tbl.adjmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                    db.SaveChanges();
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, tbl.acctgoid, "C", gl.Amt, tbl.adjno, "Adjustment|No. " + tbl.adjno + "", "Post", tbl.upduser, servertime, gl.Amt, gl.AmtUSD, "QL_trnadjmst " + tbl.adjmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                    db.SaveChanges();
                                }
                            }

                            var glmin = dtDtl.Where(w => w.adjqty < 0).GroupBy(x => x.refname).Select(x => new { Nama = x.Key, Amt = x.Sum(y => (y.adjvalueidr * (y.adjqty * (-1)))), AmtUSD = 0 });
                            if (glmin.Any())
                            {
                                foreach (var gl in glmin.ToList())
                                {
                                    int iAcctgOid = (gl.Nama == "RAW MATERIAL" ? iAcctgoidRM : gl.Nama == "GENERAL MATERIAL" ? iAcctgoidGM : iAcctgoidFG);
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, tbl.acctgoid, "D", gl.Amt, tbl.adjno, "Adjustment|No. " + tbl.adjno + "", "Post", tbl.upduser, servertime, gl.Amt, gl.AmtUSD, "QL_trnadjmst " + tbl.adjmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                    db.SaveChanges();
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgOid, "C", gl.Amt, tbl.adjno, "Adjustment|No. " + tbl.adjno + "", "Post", tbl.upduser, servertime, gl.Amt, gl.AmtUSD, "QL_trnadjmst " + tbl.adjmstoid.ToString(), null, null, null, 0, tbl.divgroupoid));
                                    db.SaveChanges();
                                }
                            }

                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={glmstoid} WHERE tablename='QL_trnglmst'");
                            db.SaveChanges();
                            db.Database.ExecuteSqlCommand($"UPDATE QL_mstoid SET lastoid={gldtloid - 1} WHERE tablename='QL_trngldtl'");
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.adjmstoid.ToString();
                        sReturnNo = "No. " + tbl.adjno;
                        if (tbl.adjmststatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnadjmst tbl = db.QL_trnadjmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnadjdtl.Where(a => a.adjmstoid == id);
                        db.QL_trnadjdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnadjmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnadjmst.Where(x => x.adjmstoid == id);
            if (tbl == null)
                return null;

            var rptname = "rptAdjustment";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT mum.adjmstoid [Oid], CONVERT(VARCHAR(20), mum.adjmstoid) AS [Draft No.], mum.adjno AS [Trans No], mum.adjdate AS [Trans Date], mum.adjmststatus AS [Status], mum.adjmstnote AS [Header Note], mud.adjdtloid, mud.adjdtlseq AS [No.], g1.gendesc AS [Warehouse], mud.refname AS [Type], mud.refoid [Matoid], (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=mud.refoid) AS [Code], (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=mud.refoid) AS [Description], mud.adjqty AS [Qty], g2.gendesc AS [Unit], mud.adjdtlnote AS [Detail Note], di.divname AS [Business Unit], '' AS [Detail Location], '' [Person Name] , '' [NIP], mud.refno AS [No. Batch], mud.serialno AS [No. Serial], dv.gendesc [Divisi] FROM QL_trnadjmst mum INNER JOIN QL_trnadjdtl mud ON mud.cmpcode=mum.cmpcode AND mud.adjmstoid=mum.adjmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=mud.adjwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.adjunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode inner join QL_mstgen dv on dv.genoid=mum.divgroupoid WHERE mud.adjmstoid=" + id + " ORDER BY mum.adjmstoid, mud.adjdtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "AdjustmentPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}