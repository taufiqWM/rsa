﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class CashBankExpenseController : Controller
    {
        private  QL_RSAEntities db = new  QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string divgroup { get; set; }
            public string cashbankno { get; set; }
            [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime? cashbankdate { get; set; }
            public string cashbanktype { get; set; }
            public string acctgdesc { get; set; }
            public string personname { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string suppname { get; set; }
            public string CBType { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamt { get; set; }
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
            public decimal cashbankglamtidr { get; set; }
            public decimal cashbankglamtusd { get; set; }
            public int curroid { get; set; }
        }

        public class suppbankaccount
        {
            public int accountoid { get; set; }
            public string accountname { get; set; }
        }

        public class trndpap
        {
            public int dpapoid { get; set; }
            public string dpapno { get; set; }
            public DateTime dpapdate { get; set; }
            public int dpapacctgoid { get; set; }
            public string acctgdesc { get; set; }
            public string dpapnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dpapamt { get; set; }
            public DateTime dpdateforcheck { get; set; }
        }

        public class supplier
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string supptaxable { get; set; }
            public decimal supptaxvalue { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + tbl.cmpcode + "' ORDER BY personname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            //if (tbl.refsuppoid != 0)
            //{
            //    sSql = "SELECT COUNT(*) FROM QL_mstsuppbankaccount WHERE activeflag='ACTIVE' AND suppoid=" + tbl.refsuppoid;
            //    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 0)
            //    {
            //        sSql = "SELECT 0 accountoid, '' accountname";
            //    }
            //    else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 1)
            //    {
            //        sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + tbl.refsuppoid + " ORDER BY accounttitle";
            //    }
            //    else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 1)
            //    {
            //        sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + tbl.refsuppoid + " ORDER BY accounttitle";
            //    }
            //}
            //else
            //{
            //    sSql = "SELECT 0 accountoid, '' accountname";
            //}
            //var cashbanksuppaccoid = new SelectList(db.Database.SqlQuery<suppbankaccount>(sSql).ToList(), "accountoid", "accountname", tbl.cashbanksuppaccoid);
            //ViewBag.cashbanksuppaccoid = cashbanksuppaccoid;
        }

        [HttpPost]
        public ActionResult InitDDLPerson(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstperson> tbl = new List<QL_mstperson>();
            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + cmp + "' ORDER BY personname";
            tbl = db.Database.SqlQuery<QL_mstperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDiv(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLSuppAccount(int suppoid)
        {
            var result = "sukses";
            var msg = "";
            List<suppbankaccount> tbl = new List<suppbankaccount>();
            if (suppoid != 0)
            {
                sSql = "SELECT COUNT(*) FROM QL_mstsuppbankaccount WHERE activeflag='ACTIVE' AND suppoid=" + suppoid;
                if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 0)
                {
                    sSql = "SELECT 0 accountoid, '' accountname";
                } else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 1)
                {
                    sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + suppoid + " ORDER BY accounttitle";
                }
                else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 1)
                {
                    sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + suppoid + " ORDER BY accounttitle";
                }
            }
            else
            {
                sSql = "SELECT 0 accountoid, '' accountname";
            }
            tbl = db.Database.SqlQuery<suppbankaccount>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindSupplierData()
        {
            List<supplier> tbl = new List<supplier>();
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr, (CASE supptaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable, ISNULL((SELECT TOP 1 CAST (gendesc AS DECIMAL) FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC), 0) supptaxvalue FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY suppcode";
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindDPData(string cmp, int suppoid, int curroid)
        {
            List<trndpap> tbl = new List<trndpap>();
            sSql = "SELECT dp.dpapoid, dp.dpapno, dpapdate, dp.acctgoid AS dpapacctgoid, a.acctgdesc, dp.dpapnote, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) AS dpapamt, (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + cmp + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ORDER BY dp.dpapoid";

            tbl = db.Database.SqlQuery<trndpap>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindTotalDP(string cmpcode, string action, int curroid, int suppoid, int cashbankoid)
        {
            decimal totaldpamt = 0;
            if (action == "Update Data")
            {
                sSql = " SELECT ISNULL((SELECT SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + cmpcode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt), 0.0) + cashbankamt FROM QL_trncashbankmst cb WHERE cb.cashbankoid=" + cashbankoid;
            }else
            {
                sSql = " SELECT ISNULL(SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)), 0.0) AS dpapamt FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + cmpcode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ";
            }
            totaldpamt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Json(totaldpamt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAccountName(int cashbanksuppaccoid)
        {
            return Json((db.Database.SqlQuery<string>("SELECT accounttitle FROM QL_mstsuppbankaccount WHERE accountoid=" + cashbanksuppaccoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            //(db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault())
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, 1 curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl)
        {
            Session["QL_trncashbankgl_ex"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankgl_ex"] == null)
            {
                Session["QL_trncashbankgl_ex"] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankgl_ex"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.cashbankrefno = db.Database.SqlQuery<string>("SELECT cashbankrefno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            //ViewBag.accounttitle = db.Database.SqlQuery<string>("SELECT accounttitle FROM QL_mstsuppbankaccount WHERE accountoid=" + tbl.cashbanksuppaccoid).FirstOrDefault();
            ViewBag.dpapno = "";
            ViewBag.totaldpamt = 0;
            ViewBag.cashbankresamt = 0;
            ViewBag.dpdateforcheck = DateTime.Parse("1/1/1900 00:00:00");
            if (tbl.cashbanktype == "BLK")
            {
                ViewBag.dpapno = db.Database.SqlQuery<string>("SELECT dpapno FROM QL_trndpap WHERE dpapoid=" + tbl.giroacctgoid).FirstOrDefault();
                ViewBag.totaldpamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)), 0.0) FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + tbl.refsuppoid + " AND dp.curroid=" + tbl.curroid + " AND dp.dpapamt > dp.dpapaccumamt").FirstOrDefault() + tbl.cashbankamt;
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(dpapamt - ISNULL(dp.dpapaccumamt, 0.0), 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault() + tbl.cashbankamt;
                ViewBag.dpdateforcheck = db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault();
            }
        }

        private string GenerateExpenseNo2(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cb", divgroupoid, "divgroupoid");
            sSql = "SELECT cb.cmpcode,  (select gendesc from ql_mstgen where genoid=cb.divgroupoid) divgroup, cb.cashbankoid, cb.cashbankno, cb.cashbankdate, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, p.personname, cb.cashbankstatus, cb.cashbanknote, ISNULL(suppname, '') AS suppname, '' AS CBType,cashbankamt FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstperson p ON p.personoid=cb.personoid LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid ";
            //if (cbVoucher)
            //    sSql += " INNER JOIN QL_mstaccount ma ON cb.acctgoid=ma.acctgoid INNER JOIN QL_mstbank mb ON ma.bankoid=mb.bankoid INNEr JOIN QL_mstgen mg ON mg.genoid=mb.bankmstoid ";
            sSql += " WHERE cb.cashbankgroup='EXPENSE' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND cb.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cashbankstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cashbankstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cashbankstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND cashbankdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cashbankstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cb.createuser='" + Session["UserID"].ToString() + "'";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trncashbankmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trncashbankmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trncashbankgl_ex"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncashbankmst.Find(cmp, id);
                Session["lastcashbanktype"] = tbl.cashbanktype;
                Session["lastdpapoid"] = tbl.giroacctgoid;
                Session["lastdpapamt"] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, 0 groupoid, '' groupdesc FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + cmp + "' ORDER BY cashbankgloid";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankgl_ex"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            
            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankgl_ex"];
            string sErrReply = "";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktype == "BBK")
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                //if (tbl.refsuppoid != 0)
                //    if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                //        if (tbl.cashbanksuppaccoid == 0)
                //            ModelState.AddModelError("", "Please fill SUPPLIER ACCOUNT field!");
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            } else if (tbl.cashbanktype == "BLK")
            {
                if (tbl.giroacctgoid == 0)
                    ModelState.AddModelError("", "Please select DP NO. field!");
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='"+ tbl.cmpcode +"' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault())
                    ModelState.AddModelError("", "PAYMENT DATE must be more than DP DATE!");
            }
            if (tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be less or equal than DUE DATE!");
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than EXPENSE DATE!");
                if (tbl.refsuppoid == 0)
                    ModelState.AddModelError("", "Please select SUPPLIER field!");
            }
            if (tbl.personoid == 0)
                ModelState.AddModelError("", "Please select PIC field!");
            if (!ClassFunction.isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", tbl.cashbanktaxamt, ref sErrReply))
                ModelState.AddModelError("", "OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbankamt < 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }else
            {
                if (tbl.cashbanktype == "BLK")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        ModelState.AddModelError("", "GRAND TOTAL must be less than DP AMOUNT!");
                    }
                    else
                    {
                        sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (action == "Update Data")
                            dNewDPBalance += tbl.cashbankamt;
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            ModelState.AddModelError("", "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!");
                    }
                }
            }
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].cashbankglamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (!ClassFunction.isLengthAccepted("cashbankglamt", "QL_trncashbankgl", dtDtl[i].cashbankglamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }
                        dtDtl[i].groupoid = 0;
                    }
                }
            }

            var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = Convert.ToDateTime(tbl.cashbankduedate);
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = Convert.ToDateTime(tbl.cashbankduedate);
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iTaxAcctgOid = 0;
            int iOtherTaxAcctgOid = 0;
            int iGiroAcctgOid = tbl.giroacctgoid;

            if (tbl.cashbankstatus == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, sDate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(sDate.Month).ToString() + " " + sDate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.cashbankstatus = "In Process";
                }
                if (tbl.cashbanktype == "BLK")
                    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                else
                    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cashbankstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cashbankstatus = "In Process";
                }
                string sVarErr = "";
                if (tbl.cashbanktaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", tbl.cmpcode))
                        sVarErr = "VAR_PPN_IN";
                    else
                        iTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", tbl.cmpcode));
                }
                if (tbl.cashbankothertaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_OTHER_TAX_EXPENSE", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_OTHER_TAX_EXPENSE";
                    else
                        iOtherTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_OTHER_TAX_EXPENSE", tbl.cmpcode));
                }
                if (tbl.cashbanktype == "BGK")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", tbl.cmpcode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.cashbankstatus = "In Process";
                }    
            }
            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                //var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        //mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateExpenseNo2(tbl.cmpcode, tbl.cashbankdate, tbl.cashbanktype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = tbl.cashbankamt * cRate.GetRateMonthlyUSDValue;
                tbl.cashbankgroup = "EXPENSE";
                tbl.cashbanktakegiro = (tbl.cashbanktype == "BGK" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid).FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                      

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " + Session["lastdpapamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + Session["lastdpapoid"];
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                                
                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.groupoid = dtDtl[i].groupoid;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Cash/Bank Expense|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;
                            for (var i=0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].cashbankglamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note Dtl. " + dtDtl[i].cashbankglnote + " | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", dtDtl[i].groupoid, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iTaxAcctgOid, "D", tbl.cashbanktaxamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note. PPN IN | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbanktaxamt * cRate.GetRateMonthlyIDRValue, tbl.cashbanktaxamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Tax C/B Expense No. " + tbl.cashbanknote + "", "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            if (tbl.cashbankothertaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iOtherTaxAcctgOid, "D", tbl.cashbankothertaxamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note. PPN IN | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankothertaxamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankothertaxamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Other Tax C/B Expense No. " + tbl.cashbanknote + "", "K", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt < 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "C", -dtDtl[i].cashbankglamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note Dtl. " + dtDtl[i].cashbankglnote + " | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", dtDtl[i].groupoid, tbl.divgroupoid ?? 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktype == "BGK")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iGiroAcctgOid, "C", tbl.cashbankamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note. PPN IN | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                            }
                            else
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, ClassFunction.Left("Form. EXPENSE | Note. PPN IN | Note Hdr. " + tbl.cashbanknote + " | Penerima. " + ViewBag.suppname + " | Refno. " + tbl.cashbankrefno + "", 200), tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, tbl.cashbankamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.cashbankoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLK")
                        {
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " + Session["lastdpapamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + Session["lastdpapoid"];
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == cmp);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptExpense.rpt"));
            sSql = "SELECT cb.cashbankoid, currcode AS [Currency], currsymbol AS [Curr Symbol], currdesc AS [Curr Desc], (CASE cashbanktype WHEN 'BKK' THEN 'Kas' WHEN 'BBK' THEN 'Transfer' WHEN 'BGK' THEN 'Giro/Cheque' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS [Payment Type], ('(' + a1.acctgcode + ') ' + a1.acctgdesc) AS [Payment Account], cashbankrefno AS [Ref. No.], cashbankduedate AS [Due Date], cashbanktakegiro AS [Date Take Giro], ISNULL(suppname, '') AS [Supplier], ISNULL(suppcode, '') AS [Supp. Code], ISNULL(suppaddr, '') AS [Supp. Address], ISNULL(g1.gendesc, '') AS [Supp. City], ISNULL(g2.gendesc, '') AS [Supp. Province], ISNULL(g3.gendesc, '') AS [Supp. Country], ISNULL(suppemail, '') AS [Supp. Email], ISNULL(suppphone1, '') AS [Supp. Phone 1], ISNULL(suppphone2, '') AS [Supp. Phone 2], '' AS [Supp. Phone 3], ISNULL(suppfax1, '') AS [Supp. Fax 1], '' AS [Supp. Fax 2], cashbankdpp AS [Total Amount], cashbanktaxtype AS [Tax Type], CONVERT(VARCHAR(10), cashbanktaxpct) AS [Tax Pct], cashbanktaxamt AS [Tax Amount], cashbankothertaxamt AS [Other Tax Amount], cashbankamt AS [Grand Total], personname AS [PIC], cashbankstatus AS [Status], cashbanknote AS [Header Note], a2.acctgcode AS [COA No.], a2.acctgdesc AS [COA Desc.], cashbankglamt AS [Amount], cashbankglnote AS [Note], divname AS [Business Unit], divaddress AS [BU Address], g4.gendesc AS [BU City], g5.gendesc AS [BU Province], g6.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Phone 1], ISNULL(divphone2, '') AS [BU Phone 2], divemail AS [BU Email], cb.upduser AS [Last Upd. User ID], ISNULL(profname,UPPER(cb.upduser)) AS [Last Upd. User Name], cb.updtime AS [Last Upd. Datetime], dg.groupdesc [Division], dg.groupcode [Code Division]";
            if (cbprintbbk)
            {
                sSql += ", ISNULL((SELECT cbx.cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), '') AS [Cash/Bank No.], ISNULL((SELECT cbx.cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), CAST('1/1/1900' AS DATETIME)) [Expense Date]";
            }else
            {
                sSql += ", cashbankno AS[Cash / Bank No.], cashbankdate AS[Expense Date]";
            }

            sSql += " FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstacctg a1 ON a1.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid INNER JOIN QL_mstperson p ON p.personoid=cb.personoid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gl.acctgoid INNER JOIN QL_mstdivision di ON di.cmpcode=cb.cmpcode LEFT JOIN QL_mstgen g1 ON g1.cmpcode=s.cmpcode AND g1.genoid=suppcityOid LEFT JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) LEFT JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2) INNER JOIN QL_mstgen g4 ON g4.genoid=divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g4.cmpcode AND g5.genoid=CONVERT(INT, g4.genother1) INNER JOIN QL_mstgen g6 ON g6.cmpcode=g4.cmpcode AND g6.genoid=CONVERT(INT, g4.genother2) LEFT JOIN QL_mstprof pr ON pr.profoid=cb.upduser Left JOIN QL_mstdeptgroup dg ON dg.cmpcode=gl.cmpcode AND dg.groupoid=gl.groupoid WHERE cashbankgroup='EXPENSE'";
            if (cbprintbbk)
            {
                sSql += " AND cashbanktype='BGK'";
            }
                sSql += " AND cb.cmpcode='" + cmp + "' AND cb.cashbankoid IN (" + id + ") ORDER BY cb.cashbankoid, gl.cashbankgloid";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintCashBank");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("UserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ExpensePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}