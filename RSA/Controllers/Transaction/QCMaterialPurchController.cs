﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class QCMaterialPurchController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class qcmatmst
        {
            public string cmpcode { get; set; }
            public int qcmstoid { get; set; }
            public DateTime qcdate { get; set; }
            public string qcmstno { get; set; }
            public string batchno { get; set; }
            public string mattype { get; set; }
            public int mrmstoid { get; set; }
            public string mrno { get; set; }
            public int matoid { get; set; }
            public string matdesc { get; set; }
            public int deptoid { get; set; }
            public string pic { get; set; }
            public string picname { get; set; }
            public DateTime qcdatestart { get; set; }
            public DateTime qcdateend { get; set; }
            public decimal qtyreceived { get; set; }
            public decimal qtysampling { get; set; }
            public decimal qtylot { get; set; }
            public int kesimpulanflag { get; set; }
            public string kesimpulan { get; set; }
            public string qcmstnote { get; set; }
            public string qcmststatus { get; set; }
            public string createuser { get; set; }
            public DateTime createtime { get; set; }
        }

        public class qcdtl
        {
            public int qcdtloid { get; set; }
            public int qcmstoid { get; set; }
            public int qcdtlseq { get; set; }
            public string pemeriksaan { get; set; }
            public string spesifikasi { get; set; }
            public string hasil { get; set; }
            public string qcdtlnote { get; set; }
        }

        public class mstperson
        {
            public string cmpcode { get; set; }
            public string personoid { get; set; }
            public string nip { get; set; }
            public string personname { get; set; }
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        public class mrmaterial
        {
            public int dtloid { get; set; }
            public string tipe { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matdesc { get; set; }
            public decimal mrqty { get; set; }
            public decimal qcqty { get; set; }
            public int unitoid { get; set; }
            public string unit { get; set; }
            public string matnote { get; set; }
        }

        public class mrmst
        {
            public int mrrawmstoid { get; set; }
            public string mrrawno { get; set; }
            public DateTime mrrawdate { get; set; }
            public string mrrawwh { get; set; }
            public string mrrawmstnote { get; set; }
        }

        // GET: QCMaterialPurch
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT mst.qcmstoid, mst.qcmstno, qcdate, mattype, mr.mrrawmstoid [mrmstoid], mr.mrrawno [mrno], r.matrawshortdesc [matdesc], qtyreceived, qtysampling, qtylot, CASE WHEN kesimpulanflag=1 THEN 'Memenuhi Syarat' ELSE 'Tidak Memenuhi Syarat' END [kesimpulan], qcmstnote, qcmststatus " +
                "FROM QL_trnqcreceivemst mst " +
                "INNER JOIN QL_mstmatraw r ON r.cmpcode = mst.cmpcode AND r.matrawoid = mst.matoid " +
                "INNER JOIN QL_mstprof p ON p.cmpcode = mst.cmpcode AND p.profoid = mst.pic " +
                "INNER JOIN QL_mstdept d ON d.cmpcode = mst.cmpcode AND d.deptoid = mst.deptoid " +
                "INNER JOIN QL_trnmrrawmst mr ON mr.cmpcode = mst.cmpcode AND mr.mrrawmstoid = mst.mrmstoid " +
                "WHERE mst.cmpcode = '" + CompnyCode + "'";
            List<qcmatmst> dt = db.Database.SqlQuery<qcmatmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: QCMaterialPurch/Form
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnqcreceivemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnqcreceivemst();
                tbl.cmpcode = CompnyCode;
                tbl.qcmstoid = ClassFunction.GenerateID("QL_trnqcreceivemst");
                tbl.qcdate = ClassFunction.GetServerTime();
                tbl.qcdatestart = ClassFunction.GetServerTime();
                tbl.qcdateend = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.qcmststatus = "In Process";

                Session["QL_trnqcreceivedtl"] = null;
            }
            else
            {
                action = "Update Data";
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl = db.QL_trnqcreceivemst.Find(cmp, id);
                
                sSql = "SELECT dtl.qcdtloid, dtl.qcmstoid, dtl.qcdtlseq, pemeriksaan, spesifikasi, hasil, ISNULL(qcdtlnote, '') [qcdtlnote] " +
                    "FROM QL_trnqcreceivedtl dtl " +
                    "INNER JOIN QL_trnqcreceivemst mst ON mst.cmpcode = dtl.cmpcode AND mst.qcmstoid = dtl.qcmstoid " +
                    "WHERE dtl.cmpcode = '" + CompnyCode + "' AND dtl.qcmstoid = " + id;
                Session["QL_trnqcreceivedtl"] = db.Database.SqlQuery<qcdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            if(action == "Update Data")
            {
                FillAdditionalField(tbl);
            }
            return View(tbl);
        }

        // POST: QCMaterialPurch/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnqcreceivemst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<qcdtl> dtDtl = (List<qcdtl>)Session["QL_trnqcreceivedtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnqcreceivemst");
                var dtloid = ClassFunction.GenerateID("QL_trnqcreceivedtl");
                var servertime = ClassFunction.GetServerTime();

                using(var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //tbl.qtysampling = tbl.qtysampling == null ? 0 : tbl.qtysampling;
                        //tbl.qtylot = tbl.qtylot == null ? 0 : tbl.qtylot;
                        tbl.qcmstnote = tbl.qcmstnote == null ? "" : tbl.qcmstnote;

                        if (action == "New Data")
                        {
                            if (db.QL_trnqcreceivemst.Find(tbl.cmpcode, tbl.qcmstoid) != null)
                                tbl.qcmstoid = mstoid;

                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnqcreceivemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.qcmstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnqcreceivemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnqcreceivemst.Where(a => a.qcmstoid == tbl.qcmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnqcreceivemst.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnqcreceivedtl tbldtl;
                        for(int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnqcreceivedtl();
                            tbldtl.cmpcode = CompnyCode;
                            tbldtl.qcdtloid = dtloid + i;
                            tbldtl.qcmstoid = tbl.qcmstoid;
                            tbldtl.qcdtlseq = i + 1;
                            tbldtl.pemeriksaan = dtDtl[i].pemeriksaan;
                            tbldtl.spesifikasi = dtDtl[i].spesifikasi;
                            tbldtl.hasil = dtDtl[i].hasil;
                            tbldtl.qcdtlnote = dtDtl[i].qcdtlnote;
                            tbldtl.upduser = Session["UserID"].ToString();
                            tbldtl.updtime = servertime;

                            db.QL_trnqcreceivedtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_trnqcreceivedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch(Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnqcreceivedtl"] == null)
            {
                Session["QL_trnqcreceivedtl"] = new List<qcdtl>();
            }

            List<qcdtl> dataDtl = (List<qcdtl>)Session["QL_trnqcreceivedtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnqcreceivemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;
        }

        private void FillAdditionalField(QL_trnqcreceivemst tbl)
        {
            ViewBag.mrno = db.Database.SqlQuery<string>("SELECT mr" + tbl.mattype + "no FROM QL_trnmr" + tbl.mattype + "mst WHERE cmpcode='" + CompnyCode + "' AND mr" + tbl.mattype + "mstoid=" + tbl.mrmstoid).FirstOrDefault();
            ViewBag.matdesc = db.Database.SqlQuery<string>("SELECT mat" + tbl.mattype + "shortdesc FROM QL_mstmat" + tbl.mattype + " WHERE cmpcode='" + CompnyCode + "' AND mat" + tbl.mattype + "oid=" + tbl.matoid).FirstOrDefault();
            ViewBag.picname = db.Database.SqlQuery<string>("SELECT ISNULL(profname, '') [profname] FROM QL_mstprof WHERE cmpcode='" + CompnyCode + "' AND profoid='" + tbl.pic + "' ORDER BY profname").FirstOrDefault();
            ViewBag.qtyreceived_temp = tbl.qtyreceived;
        }

        // POST: QCMaterialPurch/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnqcreceivemst tbl = db.QL_trnqcreceivemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnqcreceivedtl.Where(a => a.qcmstoid == id && a.cmpcode == cmp);
                        db.QL_trnqcreceivedtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnqcreceivemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMRData(string cmp, string mattype)
        {
            List<mrmst> tbl = new List<mrmst>();
            if(mattype == "Raw")
            {
                sSql = "SELECT mrrawmstoid, mrrawno, mrrawdate, mrrawwhoid, gendesc AS mrrawwh, mrrawmstnote FROM QL_trnmrrawmst mrm INNER JOIN QL_mstgen ON genoid=mrrawwhoid WHERE mrm.cmpcode='" + CompnyCode + "' AND mrrawmststatus='Post' ORDER BY mrrawmstoid";
            }else if(mattype == "Gen"){
                sSql = "SELECT mrm.mrgenmstoid [mrrawmstoid], mrgenno [mrrawno], mrgendate [mrrawdate], mrgenwhoid [mrrawwhoid], gendesc AS mrrawwh, mrgenmstnote [mrrawmstnote] FROM QL_trnmrgenmst mrm INNER JOIN QL_mstgen ON genoid = mrgenwhoid WHERE mrm.cmpcode = '" + CompnyCode + "' AND mrgenmststatus = 'Post' ORDER BY mrrawmstoid";
            }else if(mattype == "FinishGood"){
                sSql = "SELECT mrm.mritemmstoid [mrrawmstoid], mritemno [mrrawno], mritemdate [mrrawdate], mritemwhoid [mrrawwhoid], gendesc AS mrrawwh, mritemmstnote [mrrawmstnote] FROM QL_trnmritemmst mrm INNER JOIN QL_mstgen ON genoid = mritemwhoid WHERE mrm.cmpcode = '" + CompnyCode + "' AND mritemmststatus = 'Post' ORDER BY mrrawmstoid";
            }

            tbl = db.Database.SqlQuery<mrmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterial(string cmp, int mroid, string mattype)
        {
            List<mrmaterial> tbl = new List<mrmaterial>();

            sSql = "SELECT dtl.mrrawdtloid [dtloid], '" + mattype + "' [tipe], dtl.matrawoid [matoid], m.matrawcode [matcode], m.matrawshortdesc [matdesc], (dtl.mrrawqty - ISNULL((SELECT SUM(a.qtyreceived+a.qtysampling) FROM QL_trnqcreceivemst a WHERE a.cmpcode=dtl.cmpcode AND a.mrmstoid=dtl.mrrawmstoid AND a.matoid=m.matrawoid), 0.0)) [mrqty], 0.0 [qcqty], u.genoid [unitoid], u.gendesc [unit], ISNULL(dtl.mrrawdtlnote, '') [matnote] " +
                "FROM QL_trnmrrawdtl dtl " +
                "INNER JOIN QL_trnmrrawmst mst ON mst.cmpcode = dtl.cmpcode AND mst.mrrawmstoid = dtl.mrrawmstoid " +
                "INNER JOIN QL_mstmatraw m ON m.cmpcode = dtl.cmpcode AND m.matrawoid = dtl.matrawoid " +
                "INNER JOIN QL_mstgen u ON u.cmpcode = dtl.cmpcode AND u.genoid = mrrawunitoid " +
                "WHERE dtl.cmpcode = '" + CompnyCode + "' AND dtl.mrrawmstoid = " + mroid;
            tbl = db.Database.SqlQuery<mrmaterial>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPIC()
        {
            List<mstperson> tbl = new List<mstperson>();
            sSql = "SELECT cmpcode, '' [nip], profoid [personoid], profname [personname], '' [deptname]  FROM QL_mstprof WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY profname";
            tbl = db.Database.SqlQuery<mstperson>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<qcdtl> dtDtl)
        {
            Session["QL_trnqcreceivedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
