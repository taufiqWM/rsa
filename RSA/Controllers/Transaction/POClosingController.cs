﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class POClosingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class trnpomst
        {
            public string cmpcode { get; set; }
            public int pomstoid { get; set; }
            public int suppoid { get; set; }
            public string pono { get; set; }
            public string sType { get; set; }
            public string podate { get; set; }
            public string pomststatus { get; set; }
            public string pomstnote { get; set; }
            public string suppname { get; set; }
        }

        public class trnpodtl
        {
            public string cmpcode { get; set; }
            public int podtloid { get; set; }
            public int pomstoid { get; set; }
            public string potype { get; set; }
            public int podtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal poqty { get; set; }
            public string pounit { get; set; }
            public string podtlnote { get; set; }
            public string podtlstatus { get; set; }
            public string pomststatus { get; set; }
        }

        public class trnregistermst
        {
            public string cmpcode { get; set; }
            public int registermstoid { get; set; }
            public int registerno { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, string sType)
        {
            List<trnpomst> tbl = new List<trnpomst>();

            sSql = "SELECT * FROM (SELECT po" + sType + "mstoid AS pomstoid, po" + sType + "no AS pono, CONVERT(VARCHAR(10), po" + sType + "date, 101) AS podate, pom.suppoid, ISNULL((SELECT suppname FROM QL_mstsupp s WHERE s.suppoid=pom.suppoid), '') AS suppname, po" + sType + "mstnote AS pomstnote, po" + sType + "mststatus AS pomststatus, ISNULL(pom.closereason,'') closereason, ISNULL(UPPER(pom.createuser),'') createuser FROM QL_trnpo" + sType + "mst pom WHERE pom.cmpcode='" + cmp + "' AND po" + sType + "mststatus='Approved' AND po" + sType + "mstoid IN (SELECT regm.po" + sType + "mstoid FROM QL_trnmr" + sType + "mst regm WHERE regm.cmpcode='" + cmp + "' AND mr"+ sType +"mststatus IN('Approved','Post'))) AS tbl_PO ";

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND p.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY CONVERT(DATETIME, podate) DESC, pomstoid DESC";

            tbl = db.Database.SqlQuery<trnpomst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<trnpodtl> tbl = new List<trnpodtl>();
            if (sType.ToUpper() == "RAW")
                sSql = "SELECT pod.porawdtloid AS podtloid, pod.porawdtlseq AS podtlseq, pod.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, (porawqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.porawdtloid AND porefmstoid=pod.porawmstoid AND registertype='raw'), 0) + ISNULL((SELECT SUM(retrawqty) FROM QL_trnretrawdtl retd INNER JOIN QL_trnretrawmst retm ON retm.cmpcode=retd.cmpcode AND retm.retrawmstoid=retd.retrawmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.porawdtloid AND porefmstoid=pod.porawmstoid AND retrawmststatus='Post' AND ISNULL(retrawmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretrawqty) FROM QL_trnpretrawdtl pretd INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrrawdtloid=pretd.mrrawdtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.porawdtloid AND porefmstoid=pod.porawmstoid AND pretrawmststatus='Approved' AND ISNULL(pretrawmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.porawdtlnote AS podtlnote, pod.porawdtlstatus AS podtlstatus FROM QL_trnporawdtl pod INNER JOIN QL_mstmatraw m ON pod.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON pod.porawunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.porawmstoid=" + iOid + " AND porawdtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "GEN")
                sSql = "SELECT pod.pogendtloid AS podtloid, pod.pogendtlseq AS podtlseq, pod.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, (pogenqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.pogendtloid AND porefmstoid=pod.pogenmstoid AND registertype='gen'), 0) + ISNULL((SELECT SUM(retgenqty) FROM QL_trnretgendtl retd INNER JOIN QL_trnretgenmst retm ON retm.cmpcode=retd.cmpcode AND retm.retgenmstoid=retd.retgenmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.pogendtloid AND porefmstoid=pod.pogenmstoid AND retgenmststatus='Post' AND ISNULL(retgenmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretgenqty) FROM QL_trnpretgendtl pretd INNER JOIN QL_trnpretgenmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretgenmstoid=pretd.pretgenmstoid INNER JOIN QL_trnmrgendtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrgendtloid=pretd.mrgendtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.pogendtloid AND porefmstoid=pod.pogenmstoid AND pretgenmststatus='Approved' AND ISNULL(pretgenmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.pogendtlnote AS podtlnote, pod.pogendtlstatus AS podtlstatus FROM QL_trnpogendtl pod INNER JOIN QL_mstmatgen m ON pod.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON pod.pogenunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.pogenmstoid=" + iOid + " AND pogendtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "SP")
                sSql = "SELECT pod.pospdtloid AS podtloid, pod.pospdtlseq AS podtlseq, pod.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, (pospqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.pospdtloid AND porefmstoid=pod.pospmstoid AND registertype='sp'), 0) + ISNULL((SELECT SUM(retspqty) FROM QL_trnretspdtl retd INNER JOIN QL_trnretspmst retm ON retm.cmpcode=retd.cmpcode AND retm.retspmstoid=retd.retspmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.pospdtloid AND porefmstoid=pod.pospmstoid AND retspmststatus='Post' AND ISNULL(retspmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretspqty) FROM QL_trnpretspdtl pretd INNER JOIN QL_trnpretspmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretspmstoid=pretd.pretspmstoid INNER JOIN QL_trnmrspdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrspdtloid=pretd.mrspdtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.pospdtloid AND porefmstoid=pod.pospmstoid AND pretspmststatus='Approved' AND ISNULL(pretspmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.pospdtlnote AS podtlnote, pod.pospdtlstatus AS podtlstatus FROM QL_trnpospdtl pod INNER JOIN QL_mstsparepart m ON pod.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON pod.pospunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.pospmstoid=" + iOid + " AND pospdtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "ASSET")
                sSql = "SELECT pod.poassetdtloid AS podtloid, pod.poassetdtlseq AS podtlseq, pod.poassetrefoid AS matoid, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=pod.poassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=pod.poassetrefoid) ELSE '' END) AS matcode, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=pod.poassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=pod.poassetrefoid) ELSE '' END) AS matlongdesc, (poassetqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.poassetdtloid AND porefmstoid=pod.poassetmstoid AND registertype='asset'), 0) + ISNULL((SELECT SUM(retassetqty) FROM QL_trnretassetdtl retd INNER JOIN QL_trnretassetmst retm ON retm.cmpcode=retd.cmpcode AND retm.retassetmstoid=retd.retassetmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.poassetdtloid AND porefmstoid=pod.poassetmstoid AND retassetmststatus='Post' AND ISNULL(retassetmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretassetqty) FROM QL_trnpretassetdtl pretd INNER JOIN QL_trnpretassetmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretassetmstoid=pretd.pretassetmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrassetdtloid=pretd.mrassetdtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.poassetdtloid AND porefmstoid=pod.poassetmstoid AND pretassetmststatus='Approved' AND ISNULL(pretassetmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.poassetdtlnote AS podtlnote, pod.poassetdtlstatus AS podtlstatus FROM QL_trnpoassetdtl pod INNER JOIN QL_mstgen g ON pod.poassetunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.poassetmstoid=" + iOid + " AND poasssetdtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "WIP")
                sSql = "SELECT pod.powipdtloid AS podtloid, pod.powipdtlseq AS podtlseq, pod.matwipoid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, (RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END))) AS matlongdesc, (powipqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.powipdtloid AND porefmstoid=pod.powipmstoid AND registertype='wip'), 0) + ISNULL((SELECT SUM(retwipqty) FROM QL_trnretwipdtl retd INNER JOIN QL_trnretwipmst retm ON retm.cmpcode=retd.cmpcode AND retm.retwipmstoid=retd.retwipmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.powipdtloid AND porefmstoid=pod.powipmstoid AND retwipmststatus='Post' AND ISNULL(retwipmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretwipqty) FROM QL_trnpretwipdtl pretd INNER JOIN QL_trnpretwipmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretwipmstoid=pretd.pretwipmstoid INNER JOIN QL_trnmrwipdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrwipdtloid=pretd.mrwipdtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.powipdtloid AND porefmstoid=pod.powipmstoid AND pretwipmststatus='Approved' AND ISNULL(pretwipmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.powipdtlnote AS podtlnote, pod.powipdtlstatus AS podtlstatus FROM QL_trnpowipdtl pod INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=pod.matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid AND c1.cat1res1='Raw' AND c1.cat1res2='Log' INNER JOIN QL_mstgen g ON pod.powipunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.powipmstoid=" + iOid + " AND powipdtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "SAWN")
                sSql = "SELECT pod.posawndtloid AS podtloid, pod.posawndtlseq AS podtlseq, pod.matsawnoid AS matoid, matrawcode AS matcode, matrawlongdesc AS matlongdesc, (posawnqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawndtloid AND porefmstoid=pod.posawnmstoid AND registertype='sawn'), 0) + ISNULL((SELECT SUM(retsawnqty) FROM QL_trnretsawndtl retd INNER JOIN QL_trnretsawnmst retm ON retm.cmpcode=retd.cmpcode AND retm.retsawnmstoid=retd.retsawnmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawndtloid AND porefmstoid=pod.posawnmstoid AND retsawnmststatus='Post' AND ISNULL(retsawnmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretsawnqty) FROM QL_trnpretsawndtl pretd INNER JOIN QL_trnpretsawnmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretsawnmstoid=pretd.pretsawnmstoid INNER JOIN QL_trnmrsawndtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrsawndtloid=pretd.mrsawndtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawndtloid AND porefmstoid=pod.posawnmstoid AND pretsawnmststatus='Approved' AND ISNULL(pretsawnmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.posawndtlnote AS podtlnote, pod.posawndtlstatus AS podtlstatus FROM QL_trnposawndtl pod INNER JOIN QL_mstmatraw m ON matrawoid=matsawnoid INNER JOIN QL_mstgen g ON pod.posawnunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.posawnmstoid=" + iOid + " AND posawndtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "SAWN2")
                sSql = "SELECT pod.posawn2dtloid AS podtloid, pod.posawn2dtlseq AS podtlseq, pod.matsawnoid AS matoid, (cat1code + '.' + cat2code) AS matcode, (RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END))) AS matlongdesc, (posawn2qty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawn2dtloid AND porefmstoid=pod.posawn2mstoid AND registertype='sawn2'), 0) + ISNULL((SELECT SUM(retsawn2qty) FROM QL_trnretsawn2dtl retd INNER JOIN QL_trnretsawn2mst retm ON retm.cmpcode=retd.cmpcode AND retm.retsawn2mstoid=retd.retsawn2mstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawn2dtloid AND porefmstoid=pod.posawn2mstoid AND retsawn2mststatus='Post' AND ISNULL(retsawn2mstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretsawn2qty) FROM QL_trnpretsawn2dtl pretd INNER JOIN QL_trnpretsawn2mst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretsawn2mstoid=pretd.pretsawn2mstoid INNER JOIN QL_trnmrsawn2dtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrsawn2dtloid=pretd.mrsawn2dtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.posawn2dtloid AND porefmstoid=pod.posawn2mstoid AND pretsawn2mststatus='Approved' AND ISNULL(pretsawn2mstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.posawn2dtlnote AS podtlnote, pod.posawn2dtlstatus AS podtlstatus FROM QL_trnposawn2dtl pod INNER JOIN QL_mstcat2 m ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1oid=m.cat1oid INNER JOIN QL_mstgen g ON pod.posawn2unitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.posawnmstoid=" + iOid + " AND posawn2dtlstatus='' ORDER BY podtlseq";
            else if (sType.ToUpper() == "ITEM")
                sSql = "SELECT pod.poitemdtloid AS podtloid, pod.poitemdtlseq AS podtlseq, pod.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, (poitemqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(registerqty - ISNULL(regd.closeqty, 0)) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regm.cmpcode=regd.cmpcode AND regm.registermstoid=regd.registermstoid WHERE regd.cmpcode=pod.cmpcode AND porefdtloid=pod.poitemdtloid AND porefmstoid=pod.poitemmstoid AND registertype='item'), 0) + ISNULL((SELECT SUM(retitemqty) FROM QL_trnretitemdtl retd INNER JOIN QL_trnretitemmst retm ON retm.cmpcode=retd.cmpcode AND retm.retitemmstoid=retd.retitemmstoid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=retd.cmpcode AND regd.registerdtloid=retd.registerdtloid WHERE retd.cmpcode=pod.cmpcode AND porefdtloid=pod.poitemdtloid AND porefmstoid=pod.poitemmstoid AND retitemmststatus='Post' AND ISNULL(retitemmstres1, '')='Ganti Barang'), 0) + ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mritemdtloid=pretd.mritemdtloid INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=mrd.cmpcode AND regd.registerdtloid=mrd.registerdtloid WHERE pretd.cmpcode=pod.cmpcode AND porefdtloid=pod.poitemdtloid AND porefmstoid=pod.poitemmstoid AND pretitemmststatus='Approved' AND ISNULL(pretitemmstres2, '')='Retur Ganti Barang'), 0)) AS poqty, g.gendesc AS pounit, pod.poitemdtlnote AS podtlnote, pod.poitemdtlstatus AS podtlstatus FROM QL_trnpoitemdtl pod INNER JOIN QL_mstitem m ON pod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON pod.poitemunitoid=g.genoid WHERE pod.cmpcode='" + cmp + "' AND pod.poitemmstoid=" + iOid + " AND poitemdtlstatus='' ORDER BY podtlseq";

            tbl = db.Database.SqlQuery<trnpodtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpodtl> dtDtl)
        {
            Session["QL_trnpodtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string closereason)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            List<trnpodtl> dtDtl = (List<trnpodtl>)Session["QL_trnpodtl"];
            List<trnregistermst> dttbl = new List<trnregistermst>();

           

            if (dtDtl != null)
            {
                if (dtDtl[0].pomstoid == 0)
                {
                    //    ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    
                    if (closereason == "")
                        ModelState.AddModelError("", "Please Fill Closing Reason!");
                    

                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");


                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_trnpo" + dtDtl[0].potype + "dtl WHERE po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid + " AND po" + dtDtl[0].potype + "dtloid=" + dtDtl[i].podtloid + " AND po" + dtDtl[0].potype + "dtlstatus='Complete'";
                        var iCheck = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck > 0)
                        {
                            ModelState.AddModelError("", "This data " + dtDtl[i].matcode + " has been Closed by another user. Please CLOSING this transaction and try again!");
                        }
                    }

                    //Check outstanding Register
                    sSql = "SELECT DISTINCT regm.registermstoid, regm.registerno, CONVERT(VARCHAR(10), regm.registerdate, 101) AS registerdate, regm.registermstnote, regm.createuser, regm.registermststatus FROM QL_trnregistermst regm INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=regm.cmpcode AND regd.registermstoid=regm.registermstoid WHERE regm.cmpcode='" + dtDtl[0].cmpcode + "' AND regm.registermststatus IN ('In Process') AND regm.registertype='" + dtDtl[0].potype + "' AND regd.porefmstoid=" + dtDtl[0].pomstoid + "";
                    dttbl = db.Database.SqlQuery<trnregistermst>(sSql).ToList();

                    for (var i = 0; i < dttbl.Count(); i++)
                    {
                        ModelState.AddModelError("", "Register In Process: " + dttbl[i].registerno + "! ");
                    }

                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    var potype = dtDtl[0].potype.ToUpper();
                    if (potype == "SAWN2")
                        potype = "SAWN";

                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                sSql = "UPDATE QL_pr" + potype + "dtl SET pr" + potype + "dtlstatus='' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + potype + "dtloid IN (SELECT pr" + potype + "dtloid FROM QL_trnpo" + dtDtl[0].potype +  "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "dtloid=" +  dtDtl[i].podtloid +  ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_pr" + potype + "mst SET pr" + potype + "mststatus='Approved' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + potype + "mstoid IN (SELECT pr" + potype + "mstoid FROM QL_trnpo" + dtDtl[0].potype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "dtloid=" + dtDtl[i].podtloid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpo" + dtDtl[0].potype + "dtl SET po" + dtDtl[0].potype + "dtlstatus='Complete', closeqty = ISNULL(closeqty,0.0) + " + dtDtl[i].poqty + " WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "dtloid=" + dtDtl[i].podtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_trnpo" + dtDtl[0].potype + "mst SET po" + dtDtl[0].potype + "mststatus='Closed', closereason='" + closereason + "', closeuser='" + Session["UserID"].ToString() + "', closetime='" + servertime + "' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND po" + dtDtl[0].potype + "mstoid=" + dtDtl[0].pomstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            
                            objTrans.Commit();
                            Session["QL_trnpodtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}