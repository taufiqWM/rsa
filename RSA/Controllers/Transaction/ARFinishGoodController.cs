﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class ARFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class aritemmst
        {
            public string cmpcode { get; set; }
            public int aritemmstoid { get; set; }
            public string divgroup { get; set; }
            public string aritemno { get; set; }
            public DateTime aritemdate { get; set; }
            public string invoiceno { get; set; }
            public string custname { get; set; }
            public string aritemmststatus { get; set; }
            public string aritemmstnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal aritemgrandtotal { get; set; }
            public string divname { get; set; }
        }

        public class aritemdtl
        {
            public int aritemdtlseq { get; set; }
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemetd { get; set; }
            public int shipmentitemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string itemnote { get; set; }
            public decimal aritemqty { get; set; }
            public int aritemunitoid { get; set; }
            public string aritemunit { get; set; }
            public decimal aritemprice { get; set; }
            public decimal aritemdtlamt { get; set; }
            public string aritemdtldisctype { get; set; }
            public decimal aritemdtldiscvalue { get; set; }
            public decimal aritemdtldiscamt { get; set; }
            public decimal aritemdtlnetto { get; set; }
            public string aritemdtlnote { get; set; }
        }

        public class shipmentitem
        {
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public DateTime shipmentitemetd { get; set; }
            public string shipmentitemmstnote { get; set; }
            public string soitemno { get; set; }
            public string soitemcustpono { get; set; }
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        private void InitDDL(QL_trnaritemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var aritempaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.aritempaytypeoid);
            ViewBag.aritempaytypeoid = aritempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaritemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaritemmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();
            sSql = "SELECT * FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' and s.divgroupoid="+divgroupoid+" AND s.activeflag='ACTIVE' AND s.custoid IN (SELECT custoid FROM QL_trnshipmentitemmst WHERE cmpcode='" + cmp + "' AND shipmentitemmststatus='Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed') ORDER BY s.custname";

            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipmentData(string cmp, int custoid, int curroid, int divgroupoid)
        {
            List<shipmentitem> tbl = new List<shipmentitem>();
            sSql = "SELECT DISTINCT sm.shipmentitemmstoid, shipmentitemno, shipmentitemdate, shipmentitemmstnote, doitemno soitemno, shipmentitemetd FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentitemmstoid=sm.shipmentitemmstoid INNER JOIN QL_trndoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.doitemmstoid=sd.doitemmstoid WHERE sm.cmpcode='" + cmp + "' AND sm.custoid=" + custoid + " AND shipmentitemmststatus='Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed' And sm.divgroupoid="+divgroupoid+" AND sm.curroid=" + curroid + " ORDER BY shipmentitemdate DESC, sm.shipmentitemmstoid DESC";

            tbl = db.Database.SqlQuery<shipmentitem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int shipmentitemmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<aritemdtl> tbl = new List<aritemdtl>();
            sSql = "SELECT sd.shipmentitemmstoid, sd.shipmentitemdtloid, shipmentitemno, shipmentitemetd, shipmentitemdtlseq, sd.itemoid, itemcode, itemlongdesc, (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentitemdtloid=sd.shipmentitemdtloid AND sretitemmststatus<>'Rejected'), 0.0)) AS aritemqty, shipmentitemunitoid aritemunitoid, gendesc AS aritemunit, soitemprice AS aritemprice, soitemdtldisctype AS aritemdtldisctype, soitemdtldiscvalue AS aritemdtldiscvalue, itemnote FROM QL_trnshipmentitemdtl sd INNER JOIN QL_mstitem m ON m.itemoid=sd.itemoid INNER JOIN QL_mstgen g ON genoid=shipmentitemunitoid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid INNER JOIN QL_trnsoitemdtl sod on sod.cmpcode=dod.cmpcode and dod.soitemdtloid=sod.soitemdtloid INNER JOIN QL_trnshipmentitemmst sm ON sd.cmpcode=sm.cmpcode AND sd.shipmentitemmstoid=sm.shipmentitemmstoid WHERE sd.cmpcode='" + cmp + "' AND sd.shipmentitemmstoid=" + shipmentitemmstoid + " AND shipmentitemdtlstatus='' AND (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentitemdtloid=sd.shipmentitemdtloid AND sretitemmststatus<>'Rejected'), 0.0)) > 0 ORDER BY shipmentitemdtlseq";

            tbl = db.Database.SqlQuery<aritemdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<aritemdtl> dtDtl)
        {
            Session["QL_trnaritemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnaritemdtl"] == null)
            {
                Session["QL_trnaritemdtl"] = new List<aritemdtl>();
            }

            List<aritemdtl> dataDtl = (List<aritemdtl>)Session["QL_trnaritemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnaritemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: aritemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "arm", divgroupoid, "divgroupoid");
            sSql = "SELECT arm.cmpcode, (select gendesc from ql_mstgen where genoid=arm.divgroupoid) divgroup, div.divname, arm.aritemmstoid, arm.aritemno, aritemdate, s.custname, ISNULL(arm.aritemmstres3,'') AS invoiceno, arm.aritemmststatus, arm.aritemmstnote,aritemgrandtotal, aritemdatetakegiro, DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 ELSE CAST(g1.gendesc AS INT) END), aritemdate) duedate FROM QL_trnaritemmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=arm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=aritempaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "arm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "arm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND aritemmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND aritemmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND aritemmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND aritemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND aritemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND aritemmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND arm.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY CONVERT(DATETIME, arm.aritemdate) DESC, arm.aritemmstoid DESC";

            List<aritemmst> dt = db.Database.SqlQuery<aritemmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnaritemmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: aritemMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnaritemmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnaritemmst();
                tbl.cmpcode = CompnyCode;
                tbl.aritemmstoid = ClassFunction.GenerateID("QL_trnaritemmst");
                tbl.aritemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.aritemmststatus = "In Process";
                tbl.aritemtaxvalue = 0;
                tbl.aritemtaxamt = 0;
                
                Session["QL_trnaritemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnaritemmst.Find(cmp, id);

                sSql = "SELECT ard.aritemdtlseq, ard.shipmentitemmstoid, shm.shipmentitemno, shipmentitemetd,  ard.shipmentitemdtloid, ard.itemoid,m.itemcode, m.itemlongdesc, m.itemnote, ard.aritemqty, ard.aritemunitoid, g.gendesc AS aritemunit, ard.aritemprice,  ard.aritemdtlamt, ard.aritemdtldisctype, ard.aritemdtldiscvalue, ard.aritemdtldiscamt, ard.aritemdtlnetto, ard.aritemdtlnote FROM QL_trnaritemdtl ard INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_mstgen g ON g.genoid=ard.aritemunitoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=shd.cmpcode AND shm.shipmentitemmstoid=shd.shipmentitemmstoid WHERE ard.aritemmstoid=" + id + " ORDER BY ard.aritemdtlseq";
                Session["QL_trnaritemdtl"] = db.Database.SqlQuery<aritemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: aritemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnaritemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.aritemno == null)
                tbl.aritemno = "";
            
            List<aritemdtl> dtDtl = (List<aritemdtl>)Session["QL_trnaritemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.aritemdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            msg = "";
            if (msg != "")
                ModelState.AddModelError("", msg);

            
         
            
            if (tbl.aritemtaxtype == "TAX")
            {
                if (tbl.aritemtaxvalue > 0)
                {
                    if (tbl.aritemtaxvalue < 0 || tbl.aritemtaxvalue > 100)
                    {
                        ModelState.AddModelError("", "Tax value must be between 0 and 100.");
                    }
                }
            }
            if (Convert.ToDateTime(tbl.aritemdate.ToString("MM/dd/yyyy")) > Convert.ToDateTime(tbl.aritemdatetakegiro.ToString("MM/dd/yyyy")))
            {
                ModelState.AddModelError("", "Date Take Giro must be more or equal than PEB Date.");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        //if (dtDtl[i].aritemprice <= 0)
                        //{
                        //    ModelState.AddModelError("", "Price "+ dtDtl[i].itemcode +" must be greater than 0.");
                        //}
                        
                        sSql = "SELECT COUNT(*) FROM QL_trnshipmentitemmst sd WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid + " AND shipmentitemmststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND shipmentitemmstoid NOT IN (SELECT ard.shipmentitemmstoid FROM QL_trnaritemdtl ard WHERE ard.cmpcode=sd.cmpcode AND aritemmstoid=" + tbl.aritemmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "Shipment No. " + dtDtl[i].shipmentitemno + " has been used by another data. Please cancel this transaction or use another Shipment No.!");
                    }
                   
                }
            }
            
            if (tbl.aritemgrandtotal == 0)
            {
                ModelState.AddModelError("", "Grand Total must be greater than 0.");
            }
            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.aritemmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnaritemmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.aritemmststatus == "Revised")
                tbl.aritemmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.aritemmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnaritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnaritemdtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.aritemmstdisctype = "A";
                tbl.aritemmstdiscvalue = 0;
                tbl.aritemmstdiscamt = 0;
                tbl.aritemdeliverycost = 0;
                tbl.aritemothercost = 0;
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = DateTime.Parse("1/1/1900 00:00:00");
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = DateTime.Parse("1/1/1900 00:00:00");
                //tbl.sssflag = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.aritemrate2tousd = tbl.aritemrate2tousd == null ? "" : tbl.aritemrate2tousd;
                        tbl.aritemrate2toidr = tbl.aritemrate2toidr == null ? "" : tbl.aritemrate2toidr;
                        tbl.aritemratetousd = tbl.aritemratetousd == null ? "" : tbl.aritemratetousd;
                        tbl.aritemratetoidr = tbl.aritemratetoidr == null ? "" : tbl.aritemratetoidr;
                       

                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;
                        if (action == "New Data")
                        {
                            if (db.QL_trnaritemmst.Find(tbl.cmpcode, tbl.aritemmstoid) != null)
                                tbl.aritemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.aritemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnaritemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.aritemmstoid + " WHERE tablename='QL_trnaritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aritemmstoid=" + tbl.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aritemmstoid=" + tbl.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == tbl.aritemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnaritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnaritemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnaritemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.aritemdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.aritemmstoid = tbl.aritemmstoid;
                            tbldtl.aritemdtlseq = i + 1;
                            tbldtl.shipmentitemdtloid = dtDtl[i].shipmentitemdtloid;
                            tbldtl.shipmentitemmstoid = dtDtl[i].shipmentitemmstoid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.aritemqty = dtDtl[i].aritemqty;
                            tbldtl.aritemunitoid = dtDtl[i].aritemunitoid;
                            tbldtl.aritemprice = dtDtl[i].aritemprice;
                            tbldtl.aritemdtlamt = dtDtl[i].aritemdtlamt;
                            tbldtl.aritemdtldisctype = dtDtl[i].aritemdtldisctype;
                            tbldtl.aritemdtldiscvalue = dtDtl[i].aritemdtldiscvalue;
                            tbldtl.aritemdtldiscamt = dtDtl[i].aritemdtldiscamt;
                            tbldtl.aritemdtlnetto = dtDtl[i].aritemdtlnetto;
                            tbldtl.aritemdtlstatus = "";
                            tbldtl.aritemdtlnote = (dtDtl[i].aritemdtlnote == null ? "" : dtDtl[i].aritemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                           
                            tbldtl.mattype = "FINISH GOOD";

                            db.QL_trnaritemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemdtloid=" + dtDtl[i].shipmentitemdtloid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnaritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.aritemmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AR-FG" + tbl.aritemmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnaritemmst";
                                tblApp.oid = tbl.aritemmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.aritemmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: aritemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaritemmst tbl = db.QL_trnaritemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aritemmstoid=" + tbl.aritemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND aritemmstoid=" + tbl.aritemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == id && a.cmpcode == cmp);
                        db.QL_trnaritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnaritemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printouttype, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var swhere = " WHERE arm.cmpcode='" + cmp + "' AND arm.aritemmstoid=" + id;
            if (printtype!="ALL")
            {
                swhere += " And ard.aritemprice>0 ";
            }
            ReportDocument report = new ReportDocument();
            var sReportName = "";
            if (printouttype == "Doc")
                sReportName = "rptARTrn.rpt";
            else if (printouttype == "Faktur Pajak")
                sReportName = "rptARTrnInvoice.rpt";
            else
                sReportName = "rptARTrnFaktur.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", swhere);
            report.SetParameterValue("sType", "item");
            report.SetParameterValue("sMatDesc", "(SELECT itemlongdesc FROM QL_mstitem m WHERE m.itemoid=ard.itemoid)");
            report.SetParameterValue("sMatCode", "(SELECT itemcode FROM QL_mstitem m WHERE m.itemoid=ard.itemoid)");
            ClassProcedure.SetDBLogonForReport(report);
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ARFinishGoodReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}