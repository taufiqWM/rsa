﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PRRawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prrawmst
        {
            public string cmpcode { get; set; }
            public int prrawmstoid { get; set; }
            public string prrawno { get; set; }
            public string divgroup { get; set; }
            public DateTime prrawdate { get; set; }
            public string deptname { get; set; }
            public string prrawmststatus { get; set; }
            public string prrawmstnote { get; set; }
            public string divname { get; set; }
            public string revisereason { get; set; }
        }

        public class prrawdtl
        {
            public int prrawdtlseq { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal matrawlimitqty { get; set; }
            public decimal prrawqty { get; set; }
            public int prrawunitoid { get; set; }
            public string prrawunit { get; set; }
            public DateTime prrawarrdatereq { get; set; }
            public string prrawdtlnote { get; set; }
            public int requiredtloid { get; set; }
            public decimal requireqty { get; set; }
        }

        public class matraw
        {
            public string cmpcode { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal matrawlimitqty { get; set; }
            public int prrawunitoid { get; set; }
            public string prrawunit { get; set; }
        }

        private void InitDDL(QL_prrawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_prrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataDetails()
        {
            //List<prrawdtl> tbl = new List<prrawdtl>();

            //if (reqoid == 0)
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matrawcode) AS INT) prrawdtlseq, matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, 0.0000 prrawqty, matrawunitoid prrawunitoid, gendesc prrawunit, '' prrawdtlnote, DATEADD(D, 7, GETDATE()) prrawarrdatereq, 0 requiredtloid, 0.0000 requireqty FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON genoid=matrawunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' AND ISNULL(matrawres2, '')='Non Assets' AND ISNULL(matrawres3, '') NOT IN ('Sawn Timber', 'Log') AND matrawoid IN (SELECT DISTINCT prd.matrawoid FROM QL_prrawdtl prd WHERE prd.cmpcode='" + cmp + "' AND prd.upduser='" + Session["UserID"].ToString() + "') ORDER BY matrawcode";
            //else
            //    sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matrawcode) AS INT) prrawdtlseq, m.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, requireqty prrawqty, requireunitoid prrawunitoid, gendesc prrawunit, '' prrawdtlnote, DATEADD(D, 7, GETDATE()) prrawarrdatereq, requiredtloid, requireqty FROM QL_trnrequiredtl req INNER JOIN QL_mstmatraw m ON matrawoid=requirerefoid INNER JOIN QL_mstgen g ON genoid=requireunitoid WHERE req.cmpcode='" + cmp + "' AND requiremstoid=" + reqoid + " AND requirereftype='Raw' AND requireqty>0 ORDER BY matrawcode";

            //tbl = db.Database.SqlQuery<prrawdtl>(sSql).ToList();
            if(Session["QL_prrawdtl"] == null)
            {
                Session["QL_prrawdtl"] = new List<prrawdtl>();
            }
            List<prrawdtl> tbl = (List<prrawdtl>)Session["QL_prrawdtl"];

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<prrawdtl> dtDtl)
        {
            Session["QL_prrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_prrawdtl"] == null)
            {
                Session["QL_prrawdtl"] = new List<prrawdtl>();
            }

            List<prrawdtl> dataDtl = (List<prrawdtl>)Session["QL_prrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_prrawmst tbl)
        {
            //ViewBag.requireno = db.Database.SqlQuery<string>("SELECT requireno FROM QL_trnrequiremst WHERE cmpcode='" + tbl.cmpcode + "' AND requiremstoid=" + tbl.requiremstoid + "").FirstOrDefault();
        }

        //[HttpPost]
        //public ActionResult GetRequireData(string cmp)
        //{
        //    List<QL_trnrequiremst> tbl = new List<QL_trnrequiremst>();

        //    sSql = "SELECT * FROM QL_trnrequiremst req WHERE req.cmpcode='" + cmp + "' AND requiremststatus='Post' ORDER BY requiredate DESC";
        //    tbl = db.Database.SqlQuery<QL_trnrequiremst>(sSql).ToList();

        //    return Json(tbl, JsonRequestBehavior.AllowGet);
        //}

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRRawMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "prm", divgroupoid, "divgroupoid");
            sSql = "SELECT prm.cmpcode, (select gendesc from ql_mstgen where genoid=prm.divgroupoid) divgroup, prrawmstoid, prrawno, prrawdate, deptname, prrawmststatus, prrawmstnote, divname, isnull(prm.revisereason,'') revisereason FROM QL_prrawmst prm INNER JOIN QL_mstdept de ON de.cmpcode=prm.cmpcode AND de.deptoid=prm.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=prm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "prm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "prm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND prrawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND prrawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND prrawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND prrawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND prrawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND prrawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND prm.createuser='" + Session["UserID"].ToString() + "'";

            List<prrawmst> dt = db.Database.SqlQuery<prrawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnprrawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: PRRawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRRawMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prrawmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_prrawmst();
                tbl.prrawmstoid = ClassFunction.GenerateID("QL_prrawmst");
                tbl.prrawdate = ClassFunction.GetServerTime();
                tbl.prrawexpdate = ClassFunction.GetServerTime().AddMonths(3);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.prrawmststatus = "In Process";
                tbl.requiremstoid = 0;
                tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");
                tbl.specialflag = false;

                Session["QL_prrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_prrawmst.Find(cmp, id);

                sSql = "SELECT prrawdtlseq, prd.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, prrawqty, prrawunitoid, gendesc prrawunit, ISNULL(prrawdtlnote, '') prrawdtlnote, prrawarrdatereq/*, requiredtloid, requireqty*/ FROM QL_prrawdtl prd INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid INNER JOIN QL_mstgen g ON genoid=prrawunitoid WHERE prd.cmpcode='" + cmp + "' AND prd.prrawmstoid=" + id + " ORDER BY prrawdtlseq";
                Session["QL_prrawdtl"] = db.Database.SqlQuery<prrawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRRawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_prrawmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRRawMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.prrawno == null)
                tbl.prrawno = "";

            List<prrawdtl> dtDtl = (List<prrawdtl>)Session["QL_prrawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.prrawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_prrawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_prrawmst");
                var dtloid = ClassFunction.GenerateID("QL_prrawdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + CompnyCode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.requiremstoid = 0;
                        tbl.prrawno = tbl.prrawno == null ? "" : tbl.prrawno;
                        tbl.prrawmstnote = tbl.prrawmstnote == null ? "" : tbl.prrawmstnote;
                        tbl.approvaluser = tbl.approvaluser == null ? "" : tbl.approvaluser;
                        tbl.approvalcode = tbl.approvalcode == null ? "" : tbl.approvalcode;
                        tbl.approvaldatetime = tbl.approvaldatetime == null ? DateTime.Parse("1900-01-01 00:00:00") : tbl.approvaldatetime;
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? servertime : tbl.revisetime; //DateTime.ParseExact("01/01/1900", "MM/dd/yyyy")
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? servertime : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_prrawmst.Find(tbl.cmpcode, tbl.prrawmstoid) != null)
                                tbl.prrawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.prrawdate);
                            tbl.divoid = divoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                            tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");
                            db.QL_prrawmst.Add(tbl);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.prrawmstoid + " WHERE tablename='QL_prrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            
                            var trndtl = db.QL_prrawdtl.Where(a => a.prrawmstoid == tbl.prrawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_prrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_prrawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_prrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.prrawdtloid = dtloid ++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.prrawmstoid = tbl.prrawmstoid;
                            tbldtl.prrawdtlseq = i + 1;
                            tbldtl.prrawarrdatereq = dtDtl[i].prrawarrdatereq;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.prrawqty = dtDtl[i].prrawqty;
                            tbldtl.prrawunitoid = dtDtl[i].prrawunitoid;
                            tbldtl.prrawdtlstatus = "";
                            tbldtl.prrawdtlnote = dtDtl[i].prrawdtlnote == null ? "" : dtDtl[i].prrawdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            //tbldtl.requireqty = dtDtl[i].requireqty;
                            //tbldtl.requiredtloid = dtDtl[i].requiredtloid;
                            db.QL_prrawdtl.Add(tbldtl);
                            db.SaveChanges();
                        }
                        
                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_prrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.prrawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PR-RAW" + tbl.prrawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_prrawmst";
                                tblApp.oid = tbl.prrawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: PRRawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRRawMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prrawmst tbl = db.QL_prrawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_prrawdtl.Where(a => a.prrawmstoid == id && a.cmpcode == cmp);
                        db.QL_prrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();
                        
                        db.QL_prrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMatRaw(string cmp, int divgroupoid)
        {
            List<matraw> mstmatraw = new List<matraw>();
            sSql = "SELECT m.cmpcode, matrawoid, matrawcode, matrawlongdesc, ISNULL(matrawlimitqty, 1) [matrawlimitqty], matrawunitoid [prrawunitoid], g.gendesc [prrawunit] FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode = m.cmpcode AND g.genoid = m.matrawunitoid WHERE m.cmpcode = '" + CompnyCode + "' and m.divgroupoid='"+divgroupoid+"' AND m.activeflag='ACTIVE'";
            mstmatraw = db.Database.SqlQuery<matraw>(sSql).ToList();

            return Json(mstmatraw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var cmp = CompnyCode;
            ReportDocument report = new ReportDocument();
            var tbl = db.QL_prrawmst.Find(cmp, id);
            if (tbl == null)
                return null;

            string ReportURL = Path.Combine(Server.MapPath("~/Report"), "rptPR_Trn.rpt");
            report.Load(ReportURL);

            sSql = "SELECT prm.prrawmstoid AS prmstoid, prm.cmpcode, [BU Name] BUName, [BU Address] BUAddress, [BU City] BUCity, [BU Province] BUProvince, [BU Phone] BUPhone, [BU Email] BUEmail, [BU Phone2] BUPhone2, [BU PostCode] BUPostCode, [BU Fax1] BUFax1, [BU Fax2] BUFax2, [BU Country] BUCountry, prm.prrawno AS prno, CONVERT(VARCHAR(10), prm.prrawmstoid) AS draftno, prm.prrawdate AS prdate, g1.deptname AS department, prm.prrawmstnote AS prmstnote, prm.prrawmststatus AS prmststatus, prd.prrawdtlseq AS prdtlseq, prd.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, prd.prrawqty AS prqty, g4.gendesc as prunit, prd.prrawdtlnote AS prdtlnote, prd.prrawarrdatereq AS prarrdatereq, (SELECT ISNULL(pa.profname,'-') FROM QL_mstprof pa WHERE pa.profoid=prm.approvaluser) AS UserNameApproved, prm.approvaldatetime AS ApproveTime, (SELECT ISNULL(pc.profname,'-') FROM QL_mstprof pc WHERE pc.profoid=prm.createuser) AS CreateUserName, prm.createtime AS [CreateTime], '' [Manufacture] FROM QL_prrawmst prm INNER JOIN QL_prrawdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid  INNER JOIN QL_mstdept g1 ON prm.cmpcode=g1.cmpcode AND prm.deptoid=g1.deptoid INNER JOIN QL_mstgen g4 ON prd.prrawunitoid=g4.genoid INNER JOIN (SELECT d.cmpcode, d.divname AS [BU Name], d.divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], d.divphone AS [BU Phone], d.divemail AS [BU Email], d.divphone2 AS [BU Phone2], d.divpostcode AS [BU PostCode], d.divfax1	AS [BU Fax1], d.divfax2	AS [BU Fax2], g3.gendesc AS [BU Country] FROM QL_mstdivision d INNER JOIN QL_mstgen g1 ON g1.genoid=d.divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) AS BU ON BU.cmpcode=prm.cmpcode WHERE prm.cmpcode='" + cmp + "' AND prm.prrawmstoid=" + id + " ORDER BY prm.prrawmstoid";
            //List<PrintOutModels.printout_pr> dtRpt = db.Database.SqlQuery<PrintOutModels.printout_pr>(sSql).ToList();

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPR_Trn");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sUserID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            report.Close();report.Dispose();
            stream.Seek(0, SeekOrigin.Begin);

            //Response.AddHeader("Content-disposition", "attachment; filename=" + "rptPR_Trn");
            //byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            //return File(FileBytes, "application/pdf", "PRRawMaterialReport.pdf");
            return File(stream, "application/pdf", "PRRawMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
