﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class PRGenMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prgenmst
        {
            public string cmpcode { get; set; }
            public int prgenmstoid { get; set; }
            public string divgroup { get; set; }
            public string prgenno { get; set; }
            public DateTime prgendate { get; set; }
            public string deptname { get; set; }
            public string prgenmststatus { get; set; }
            public string prgenmstnote { get; set; }
            public string divname { get; set; }
            public string revisereason { get; set; }
        }

        public class prgendtl
        {
            public int prgendtlseq { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public decimal prgenqty { get; set; }
            public int prgenunitoid { get; set; }
            public string prgenunit { get; set; }
            public DateTime prgenarrdatereq { get; set; }
            public string prgendtlnote { get; set; }
            public int requiredtloid { get; set; }
            public decimal requireqty { get; set; }
        }

        public class matgen
        {
            public string cmpcode { get; set; }
            public int matgenoid { get; set; }
            public string matgencode { get; set; }
            public string matgenlongdesc { get; set; }
            public decimal matgenlimitqty { get; set; }
            public int prgenunitoid { get; set; }
            public string prgenunit { get; set; }
        }

        private void InitDDL(QL_prgenmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_prgenmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "', '" + CompnyCode + "') ORDER BY approvaluser";
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataDetails()
        {
            if (Session["QL_prgendtl"] == null)
            {
                Session["QL_prgendtl"] = new List<prgendtl>();
            }
            List<prgendtl> tbl = (List<prgendtl>)Session["QL_prgendtl"];

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<prgendtl> dtDtl)
        {
            Session["QL_prgendtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_prgendtl"] == null)
            {
                Session["QL_prgendtl"] = new List<prgendtl>();
            }

            List<prgendtl> dataDtl = (List<prgendtl>)Session["QL_prgendtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________


        // GET: PRGenMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRGenMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "prm", divgroupoid, "divgroupoid");
            sSql = "SELECT prm.cmpcode, (select gendesc from ql_mstgen where genoid=prm.divgroupoid) divgroup,prgenmstoid, prgenno, prgendate, deptname, prgenmststatus, prgenmstnote, divname, isnull(prm.revisereason,'') revisereason FROM QL_prgenmst prm INNER JOIN QL_mstdept de ON de.cmpcode=prm.cmpcode AND de.deptoid=prm.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=prm.cmpcode WHERE ";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "prm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "prm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND prgenmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND prgenmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND prgenmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND prgendate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND prgendate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND prgenmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND prm.createuser='" + Session["UserID"].ToString() + "'";

            List<prgenmst> dt = db.Database.SqlQuery<prgenmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnprgenmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: PRGenMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRGenMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prgenmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_prgenmst();
                tbl.prgenmstoid = ClassFunction.GenerateID("QL_prgenmst");
                tbl.prgendate = ClassFunction.GetServerTime();
                tbl.prgenexpdate = ClassFunction.GetServerTime().AddMonths(3);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.prgenmststatus = "In Process";
                tbl.requiremstoid = 0;
                tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");

                Session["QL_prgendtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_prgenmst.Find(cmp, id);

                sSql = "SELECT prgendtlseq, prd.matgenoid, matgencode, matgenlongdesc, matgenlimitqty, prgenqty, prgenunitoid, gendesc prgenunit, ISNULL(prgendtlnote, '') prgendtlnote, prgenarrdatereq FROM QL_prgendtl prd INNER JOIN QL_mstmatgen m ON m.matgenoid=prd.matgenoid INNER JOIN QL_mstgen g ON genoid=prgenunitoid WHERE prd.cmpcode='" + cmp + "' AND prd.prgenmstoid=" + id + " ORDER BY prgendtlseq";
                Session["QL_prgendtl"] = db.Database.SqlQuery<prgendtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            //FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: PRGenMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_prgenmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRGenMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.prgenno == null)
                tbl.prgenno = "";

            List<prgendtl> dtDtl = (List<prgendtl>)Session["QL_prgendtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.prgenmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_prgenmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_prgenmst");
                var dtloid = ClassFunction.GenerateID("QL_prgendtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + CompnyCode + "'").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.requiremstoid = 0;
                        tbl.prgenno = tbl.prgenno == null ? "" : tbl.prgenno;
                        tbl.prgenmstnote = tbl.prgenmstnote == null ? "" : tbl.prgenmstnote;
                        tbl.approvaluser = tbl.approvaluser == null ? "" : tbl.approvaluser;
                        tbl.approvalcode = tbl.approvalcode == null ? "" : tbl.approvalcode;
                        tbl.approvaldatetime = tbl.approvaldatetime == null ? DateTime.Parse("1900-01-01 00:00:00") : tbl.approvaldatetime;
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? servertime : tbl.revisetime; //DateTime.ParseExact("01/01/1900", "MM/dd/yyyy")
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? servertime : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_prgenmst.Find(tbl.cmpcode, tbl.prgenmstoid) != null)
                                tbl.prgenmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.prgendate);
                            tbl.divoid = divoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.revisetime = DateTime.Parse("1900-01-01 00:00:00");
                            tbl.rejecttime = DateTime.Parse("1900-01-01 00:00:00");
                            db.QL_prgenmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.prgenmstoid + " WHERE tablename='QL_prgenmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_prgendtl.Where(a => a.prgenmstoid == tbl.prgenmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_prgendtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_prgendtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_prgendtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.prgendtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.prgenmstoid = tbl.prgenmstoid;
                            tbldtl.prgendtlseq = i + 1;
                            tbldtl.prgenarrdatereq = dtDtl[i].prgenarrdatereq;
                            tbldtl.matgenoid = dtDtl[i].matgenoid;
                            tbldtl.prgenqty = dtDtl[i].prgenqty;
                            tbldtl.prgenunitoid = dtDtl[i].prgenunitoid;
                            tbldtl.prgendtlstatus = "";
                            tbldtl.prgendtlnote = dtDtl[i].prgendtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            //tbldtl.requireqty = dtDtl[i].requireqty;
                            //tbldtl.requiredtloid = dtDtl[i].requiredtloid;
                            db.QL_prgendtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_prgendtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.prgenmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "PR-GEN" + tbl.prgenmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_prgenmst";
                                tblApp.oid = tbl.prgenmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: PRGenMaterial/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("PRGenMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_prgenmst tbl = db.QL_prgenmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_prgendtl.Where(a => a.prgenmstoid == id && a.cmpcode == cmp);
                        db.QL_prgendtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_prgenmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMatGen(string cmp, int divgroupoid)
        {
            List<matgen> mstmatraw = new List<matgen>();
            sSql = "SELECT m.cmpcode, matgenoid, matgencode, matgenlongdesc, matgenlimitqty, matgenunitoid [prgenunitoid], g.gendesc [prgenunit] FROM QL_mstmatgen m INNER JOIN QL_mstgen g ON g.cmpcode = m.cmpcode AND g.genoid = m.matgenunitoid WHERE m.cmpcode = '" + cmp + "' AND m.activeflag='ACTIVE' AND isnull(matgenres1,'')='' and m.divgroupoid='" + divgroupoid + "'";
            mstmatraw = db.Database.SqlQuery<matgen>(sSql).ToList();

            return Json(mstmatraw, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var cmp = CompnyCode;
            ReportDocument report = new ReportDocument();
            var tbl = db.QL_prgenmst.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPR_Trn.rpt"));
            sSql = "SELECT prm.prgenmstoid AS prmstoid, prm.cmpcode, [BU Name] BUName, [BU Address] BUAddress, [BU City] BUCity, [BU Province] BUProvince, [BU Phone] BUPhone, [BU Email] BUEmail, [BU Phone2] BUPhone2, [BU PostCode] BUPostCode, [BU Fax1] BUFax1, [BU Fax2] BUFax2, [BU Country] BUCountry, prm.prgenno AS prno, CONVERT(VARCHAR(10), prm.prgenmstoid) AS draftno, prm.prgendate AS prdate, g1.deptname AS department, prm.prgenmstnote AS prmstnote, prm.prgenmststatus AS prmststatus, prd.prgendtlseq AS prdtlseq, prd.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, prd.prgenqty AS prqty, g4.gendesc as prunit, prd.prgendtlnote AS prdtlnote, prd.prgenarrdatereq AS prarrdatereq, (SELECT ISNULL(pa.profname,'-') FROM QL_mstprof pa WHERE pa.profoid=prm.approvaluser) AS UserNameApproved, prm.approvaldatetime AS ApproveTime, (SELECT ISNULL(pc.profname,'-') FROM QL_mstprof pc WHERE pc.profoid=prm.createuser) AS CreateUserName, prm.createtime AS [CreateTime], '' [Manufacture] FROM QL_prgenmst prm INNER JOIN QL_prgendtl prd ON prm.cmpcode=prd.cmpcode AND prm.prgenmstoid=prd.prgenmstoid INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid INNER JOIN QL_mstdept g1 ON prm.cmpcode=g1.cmpcode AND prm.deptoid=g1.deptoid INNER JOIN QL_mstgen g4 ON prd.prgenunitoid=g4.genoid INNER JOIN (SELECT d.cmpcode, d.divname AS [BU Name], d.divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], d.divphone AS [BU Phone], d.divemail AS [BU Email], d.divphone2 AS [BU Phone2], d.divpostcode AS [BU PostCode], d.divfax1	AS [BU Fax1], d.divfax2	AS [BU Fax2], g3.gendesc AS [BU Country] FROM QL_mstdivision d INNER JOIN QL_mstgen g1 ON g1.genoid=d.divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) AS BU ON BU.cmpcode=prm.cmpcode WHERE prm.cmpcode='" + cmp + "' AND prm.prgenmstoid=" + id + " ORDER BY prm.prgenmstoid";
            //List<PrintOutModels.printout_pr> dtRpt = db.Database.SqlQuery<PrintOutModels.printout_pr>(sSql).ToList();

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPR_Trn");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sUserID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PRgenMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
