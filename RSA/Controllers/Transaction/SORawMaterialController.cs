﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class SORawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string NumberCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private OleDbConnection connExcel = new OleDbConnection();
        private OleDbDataAdapter da = new OleDbDataAdapter();
        private string sSql = "";

        public class sorawmst
        {
            public string cmpcode { get; set; }
            public int sorawmstoid { get; set; }
            public string divgroup { get; set; }
            public string sorawno { get; set; }
            public DateTime sorawdate { get; set; }
            public string custname { get; set; }
            public string sorawmststatus { get; set; }
            public string sorawmstnote { get; set; }
            public string divname { get; set; }
            public string sotype { get; set; }
        }

        public class sorawdtl
        {
            public int sorawdtlseq { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public string matrawnote { get; set; }
            public decimal stockqty { get; set; }
            public decimal sorawqty { get; set; }
            public int sorawunitoid { get; set; }
            public string sorawunit { get; set; }
            public decimal sorawlastprice { get; set; }
            public decimal sorawprice { get; set; }
            public decimal sorawprice_intax { get; set; }
            public decimal sorawdtlamt { get; set; }
            public decimal sorawdtlamt_intax { get; set; }
            public string sorawdtldisctype { get; set; }
            public decimal sorawdtldiscvalue { get; set; }
            public decimal sorawdtldiscvalue_intax { get; set; }
            public decimal sorawdtldiscamt { get; set; }
            public decimal sorawdtldiscamt_intax { get; set; }
            public decimal sorawdtlnetto { get; set; }
            public decimal sorawdtlnetto_intax { get; set; }
            public string sorawdtlnote { get; set; }
            public decimal Lsht { get; set; }
            public decimal Psht { get; set; }
            public string gramatur { get; set; }
            public string tw { get; set; }
            public string flute { get; set; }
            public decimal c1 { get; set; }
            public decimal c2 { get; set; }
            public decimal c3 { get; set; }
            public decimal c4 { get; set; }
            public decimal c5 { get; set; }
            public DateTime etd { get; set; }
            public decimal lbhn { get; set; }
            public decimal outs { get; set; }
            public decimal pbhn { get; set; }
            public decimal rm { get; set; }
            public decimal luk { get; set; }
            public decimal puk { get; set; }
            public decimal luas { get; set; }
            public int hjual { get; set; }
            public int custdtloid { get; set; }
            public string discmargin { get; set; }
            public decimal hindex { get; set; }
            public string disc { get; set; }
            public decimal hinc { get; set; }

        }

        public class mstaccount
        {
            public int accountoid { get; set; }
            public string accountdesc { get; set; }
        }

        public class container
        {
            public int containerseq { get; set; }
            public int containeroid { get; set; }
            public string containertype { get; set; }
            public decimal sorawcontqty { get; set; }
            public decimal sorawcontpct { get; set; }

        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal soratetousd { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal sorate2tousd { get; set; }
        }

        private void InitDDL(QL_trnsorawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var sopaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.sorawpaytypeoid);
            ViewBag.sopaytypeoid = sopaytypeoid;

            sSql = "SELECT * FROM QL_mstaccount a WHERE a.cmpcode='" + tbl.cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY a.accountno";
            var accountoid = new SelectList(db.Database.SqlQuery<QL_mstaccount>(sSql).ToList(), "accountoid", "accountno", tbl.accountoid);
            ViewBag.accountoid = accountoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode='IDR'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtladdr FROM QL_mstcustdtl a WHERE a.custdtloid ='" + tbl.custdtloid + "'").FirstOrDefault();

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsorawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsorawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDivision(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<mstaccount> tbl = new List<mstaccount>();
            sSql = "SELECT a.accountoid, b.bankname+' - '+a.accountno accountdesc  FROM QL_mstaccount a INNER JOIN QL_mstbank b ON b.bankoid = a.bankoid WHERE a.cmpcode='" + cmpcode + "' AND a.activeflag='ACTIVE' ORDER BY b.bankname+a.accountno";
            tbl = db.Database.SqlQuery<mstaccount>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetContainerData()
        {
            List<container> tbl = new List<container>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY gendesc) AS INT) AS containerseq, genoid AS containeroid, gendesc AS containertype, 0.0 AS sorawcontqty, 0.0 AS sorawcontpct FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CONTAINER TYPE' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<container>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();
            var divgroupoid = int.Parse(Session["DivGroup"].ToString());
            sSql = "SELECT * FROM QL_mstcust c WHERE c.cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND custoid>0 and c.divgroupoid="+divgroupoid+" ORDER BY custname DESC";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.soratetousd = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.sorate2tousd = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int curroid, int custoid)
        {
            var divgroupoid = int.Parse(Session["DivGroup"].ToString());
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<sorawdtl> tbl = new List<sorawdtl>();
            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY matrawcode) AS INT) sorawdtlseq, matrawoid, matrawcode, matrawlongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode='" + cmp + "' AND refname='SHEET' AND refoid=matrawoid), 0.0) AS stockqty, 0.0 AS sorawqty, matrawunitoid AS sorawunitoid, gendesc AS sorawunit, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.matrawoid=m.matrawoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS sorawprice, ISNULL((SELECT TOP 1 lastsalesprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.matrawoid=m.matrawoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastsoprice, ISNULL((SELECT TOP 1 curroid FROM QL_mstmatrawprice mp WHERE mp.cmpcode='" + cmp + "' AND mp.matrawoid=m.matrawoid AND refoid=" + custoid + " AND refname='CUSTOMER' AND curroid=" + curroid + " ORDER BY mp.updtime DESC), 0.0) AS lastcurroid, '' AS sorawdtlnote, 'P' AS sorawdtldisctype, 0.0 AS sorawdtldiscvalue, m.matrawnote FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=matrawunitoid WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' AND m.divgroupoid="+divgroupoid+" AND ISNULL(matrawres2, '') IN ('Non Assets', '','0') AND ISNULL(m.matrawres3, '') NOT IN ('Log', 'Sawn Timber') ORDER BY matrawcode";

            tbl = db.Database.SqlQuery<sorawdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<sorawdtl> dtDtl)
        {
            Session["QL_trnsorawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsCont(List<container> dtDtlCont)
        {
            Session["QL_trnsorawcont"] = dtDtlCont;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsorawdtl"] == null)
            {
                Session["QL_trnsorawdtl"] = new List<sorawdtl>();
            }

            List<sorawdtl> dataDtl = (List<sorawdtl>)Session["QL_trnsorawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }
        public class listalamat
        {
            public int custdtloid { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
            public int custdtlcityoid { get; set; }
            public string custdtlcity { get; set; }
        }
        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtloid, custdtlname, custdtladdr , custdtlcityoid , g.gendesc custdtlcity FROM QL_mstcustdtl c Left JOIN QL_mstgen g ON g.genoid=c.custdtlcityoid WHERE c.custoid = " + custoid + " ORDER BY custdtloid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailContainer()
        {
            if (Session["QL_trnsorawcont"] == null)
            {
                Session["QL_trnsorawcont"] = new List<container>();
            }

            List<container> dataDtlCont = (List<container>)Session["QL_trnsorawcont"];
            return Json(dataDtlCont, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsorawmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.custpaytype = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid=" + tbl.sorawpaytypeoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: sorawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "som", divgroupoid, "divgroupoid");
            sSql = "SELECT som.cmpcode, (select gendesc from ql_mstgen where genoid=som.divgroupoid) divgroup, som.sorawmstoid, som.sorawno, som.sorawdate, c.custname, som.sorawmststatus, som.sorawmstnote, div.divname, som.sorawtype sotype FROM QL_TRNsorawMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND sorawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND sorawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND sorawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND sorawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND sorawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND sorawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND som.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY som.sorawdate DESC, som.sorawmstoid DESC";

            List<sorawmst> dt = db.Database.SqlQuery<sorawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnsorawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: sorawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsorawmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnsorawmst();
                tbl.cmpcode = CompnyCode;
                tbl.sorawmstoid = ClassFunction.GenerateID("QL_trnsorawmst");
                tbl.sorawdate = ClassFunction.GetServerTime();
                tbl.socustrefdate = ClassFunction.GetServerTime();
                tbl.sorawetd= ClassFunction.GetServerTime().AddDays(7);
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.sorawmststatus = "In Process";
                tbl.isexcludetax = true;
                
                Session["QL_trnsorawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnsorawmst.Find(cmp, id);
                //string sAdd = "";
                //if (tbl.isexcludetax)
                //{
                //    sAdd = "";
                //}

                sSql = "SELECT sod.sorawdtlseq, sod.matrawoid, 'SHEET' matrawcode, sod.sodtldesc matrawlongdesc, sod.sorawqty, sod.sorawunitoid, g.gendesc AS sorawunit, sod.sorawprice sorawprice, sod.sorawprice_intax, sod.sorawdtlamt sorawdtlamt, sod.sorawdtldisctype, sorawdtldiscvalue sorawdtldiscvalue, sorawdtldiscamt sorawdtldiscamt, sorawdtlnetto sorawdtlnetto, sod.sorawdtlnote, ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstmatrawprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=1 AND crd.matrawoid=sod.matrawoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.lastsalesprice FROM QL_mstmatrawprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=1 AND crd.matrawoid=sod.matrawoid ORDER BY crd.updtime DESC), 0.0)) AS sorawlastprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.cmpcode=sod.cmpcode AND crd.refname='CUSTOMER' AND crd.refoid=1 AND crd.matrawoid=sod.matrawoid ORDER BY crd.updtime DESC), ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstmatrawprice crd WHERE crd.refname='CUSTOMER' AND crd.refoid=1 AND crd.matrawoid=sod.matrawoid ORDER BY crd.updtime DESC), 0.0)) AS lastcurroid, Lsht, Psht, g1+g2+g3+isnull(g4,'')+isnull(g5,'') gramatur, tw, flute, Lsht,isnull(c1,0.0) c1,isnull(c2,0.0) c2,isnull(c3,0.0) c3,isnull(c4,0.0) c4,isnull(c5,0.0) c5,isnull(tol,0.0)tol,lbhn,outs,pbhn,isnull(rm,0.0)rm,luk,puk,luas,hjual,discmargin,isnull(hindex,0.0)hindex,disc,isnull(hinc,0.0)hinc FROM QL_trnsorawdtl sod INNER JOIN QL_mstgen g ON g.genoid=sod.sorawunitoid INNER JOIN QL_trnsorawmst som ON som.cmpcode=sod.cmpcode AND som.sorawmstoid=sod.sorawmstoid WHERE sod.sorawmstoid=" + id + " ORDER BY sod.sorawdtlseq";
                Session["QL_trnsorawdtl"] = db.Database.SqlQuery<sorawdtl>(sSql).ToList();

               
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        public ActionResult Import()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            ViewBag.Title ="SO Sheet";
            

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> UploadFiles()
        {
            if (Session["UserID"] == null)
                return null;

            var result = "sukses";
            var folderloc = "~/Files/FileTemps";
            var fileloc = "";
            List<string> colsdtl = new List<string>();
            List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {

                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var filename = (Path.GetFileName(fileContent.FileName)).Replace(" ","_");
                        //var randomdir = DateTime.Now.ToString("yyMMdd-hhmmss-ff");
                        var sext = Path.GetExtension(fileContent.FileName);
                        if (sext.ToLower() != ".xls" & sext.ToLower() != ".xlsx")
                        {
                            result = "Failed format file harus .xls/.xlsx";
                        }
                        var filename = DateTime.Now.ToString("yyMMdd-hhmmss-ff") + sext;
                        var sdir = Server.MapPath(folderloc);
                        var filepath = Path.Combine(sdir, filename);
                        fileloc = folderloc + "/" + filename;

                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(filepath, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
            }

            string msg = "";
            if (result == "sukses")
            {
                if (Path.GetExtension(fileloc).ToLower() == ".xlsx")
                {
                    connExcel.ConnectionString = "provider='Microsoft.ACE.OLEDB.12.0';data source='" + Server.MapPath(fileloc) + "';Extended Properties='Excel 12.0';";
                }
                else if (Path.GetExtension(fileloc).ToLower() == ".xls")
                {
                    connExcel.ConnectionString = "provider='Microsoft.Jet.OLEDB.4.0';data source='" + Server.MapPath(fileloc) + "';Extended Properties='Excel 8.0';";
                }
                connExcel.Open();
                var dtSheets = connExcel.GetSchema("Tables");
                var tbldtl = new DataTable();

                if (dtSheets.Rows.Count > 0)
                {
                    DataSet dsData = new DataSet();
                    //da = new OleDbDataAdapter("SELECT * FROM [" + dtSheets.Rows[1]["TABLE_NAME"].ToString() + "]", connExcel);
                    da = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connExcel);
                    da.Fill(dsData, "tabelImport");
                    tbldtl = dsData.Tables["tabelImport"];
                    DataView dvdtl = tbldtl.DefaultView;
                    dvdtl.RowFilter = "IDCUSTOMER > 0";
                    tbldtl= dvdtl.ToTable();
                }

                connExcel.Close();

                if (tbldtl.Rows.Count > 0)
                {
                    if (tbldtl.Columns.Count >= 31) //31 Colunmn
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl.Rows)
                        {
                            row = new Dictionary<string, object>();
                            int custdtloid = int.Parse(dr[2].ToString());
                            int custoid = int.Parse(dr[1].ToString());
                            int custdtl = db.Database.SqlQuery<int>("select isnull(custdtloid,0) from ql_mstcustdtl where custoid='" + custoid + "' and custdtloid='"+custdtloid+"'").FirstOrDefault();
                            var custname = db.Database.SqlQuery<string>("select custname from ql_mstcust where custoid='" + custoid + "'").FirstOrDefault();
                            var custdtlname = db.Database.SqlQuery<string>("select custdtlname from ql_mstcustdtl where custdtloid='" + custdtl + "'").FirstOrDefault();
                            var lgrama = dr[7].ToString().Length;
                            var tw = "SW";
                            if (lgrama == 5)
                            {
                                tw = "DW";
                            }
                            if (custdtl == 0)
                                result += "nomor Shipto \"" + ("Sheet " + dr[4].ToString() + " X " + dr[5].ToString() + "(" + dr[8].ToString() + ", " + tw + ", " + dr[7].ToString() + ")") + "\" tidak di temukan di data Master Shipto customer! <br>";
                            else
                            {
                                
                                row.Add("no", dr[0].ToString());
                                row.Add("custoid", dr[1].ToString());
                                row.Add("pono", dr[3].ToString());
                                row.Add("Lsht", dr[4].ToString());
                                row.Add("Psht", dr[5].ToString());
                                row.Add("sodtldesc", "Sheet " + dr[4].ToString() + " X " + dr[5].ToString() + "(" + dr[8].ToString() + ", " + tw + ", " + dr[7].ToString() + ")");
                                row.Add("tw", tw);
                                row.Add("gramatur", dr[7].ToString());
                                row.Add("flute", dr[8].ToString());
                                row.Add("tsheet", dr[9].ToString());
                                row.Add("c1", dr[10].ToString());
                                row.Add("c2", dr[11].ToString());
                                row.Add("c3", dr[12].ToString());
                                row.Add("c4", dr[13].ToString());
                                row.Add("c5", dr[14].ToString());
                                row.Add("sorawqty", dr[6].ToString());
                                row.Add("etd", dr[16].ToString());
                                row.Add("lbhn", dr[17].ToString());
                                row.Add("outs", dr[18].ToString());
                                row.Add("pbhn", dr[19].ToString());
                                row.Add("rm", dr[20].ToString());
                                row.Add("luk", dr[21].ToString());
                                row.Add("puk", dr[22].ToString());
                                row.Add("luas", dr[23].ToString());
                                row.Add("hjual", dr[24].ToString());
                                row.Add("discmargin", dr[25].ToString());
                                row.Add("hindex", dr[26].ToString());
                                row.Add("disc", dr[27].ToString());
                                row.Add("hinc", dr[28].ToString());
                                row.Add("sorawdtlnote", dr[29].ToString());
                                row.Add("sorawdtlamt", dr[30].ToString());
                                row.Add("custdtloid", dr[2].ToString());
                                row.Add("custname", custname);
                                row.Add("custdtlname", custdtlname);

                                rowsdtl.Add(row);
                            }
                        }
                    }
                    else
                    {
                        result = "template XLS salah! <br>";
                    }
                }
                else
                {
                    result = "Data tidak ada";
                }
            }

            return Json(new { result, fileloc, rowsdtl, msg }, JsonRequestBehavior.AllowGet);
        }

        private string GenerateTransNo()
        {
            var sNo = "SORM-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sorawno, " + int.Parse(NumberCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsorawmst WHERE cmpcode='" + CompnyCode + "' AND sorawno LIKE '" + sNo + "%'";
            sNo = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(NumberCounter));
            return sNo;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult saveSPC(List<QL_trnsorawdtl> dt_So)
        {
            if (Session["UserID"] == null)
                return null;//RedirectToAction("LogIn", "Account");

            var divgroupoid = Session["DivGroup"].ToString();
            string sMsg = "";
            //Is Input Valid?
            if (!dt_So.Any())
            {
                sMsg = "Data so harap di isi!!<br />";
            }
            else
            {
                foreach (var t in dt_So)
                {
                    var  cust = t.custoid;
                    var custoid = db.QL_mstcust.Where(w => w.custoid == cust ).Select(g => g.custoid).DefaultIfEmpty(0).First();
                    if (custoid == 0)
                    {
                        sMsg = "Data Customer tidak ditemukan";
                    }
               
                }
            }
           


            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_trnsorawmst");
            var dtloid = ClassFunction.GenerateID("QL_trnsorawdtl");
            

            if (sMsg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        foreach (var t in dt_So)
                        {
                            var sorawpaytypeoid = db.Database.SqlQuery<int>("select custpaymentoid from ql_mstcust where custoid='"+t.custoid+"'").FirstOrDefault();
                            var sales = db.Database.SqlQuery<string>("select salesoid from ql_mstcust where custoid='" + t.custoid + "'").FirstOrDefault();
                            var sorawtaxtype = db.Database.SqlQuery<int>("select custtaxable from ql_mstcust where custoid='" + t.custoid + "'").FirstOrDefault();
                            QL_trnsorawmst tbl = new QL_trnsorawmst();
                            tbl.divgroupoid = int.Parse(divgroupoid);
                            tbl.accountoid = 0;
                            tbl.sorawmstdisctype = "P";
                            tbl.sorawmstdiscvalue = 0;
                            tbl.sorawmstdiscamt = 0;
                            tbl.sorawtotaldiscdtl = 0;
                            tbl.sorawtotaldisc = 0;

                            tbl.cmpcode = Session["CompnyCode"].ToString();
                            tbl.sorawmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tbl.sorawtype = "LOCAL";
                            tbl.sorawdate = servertime;
                            tbl.sorawno = GenerateTransNo();
                            tbl.custoid = t.custoid;
                            tbl.sorawpaytypeoid = sorawpaytypeoid;
                            tbl.curroid = 1;
                            tbl.rateoid = 2;
                            tbl.sorawtotalamt = t.sorawdtlamt;
                            tbl.sorawtotalnetto = t.sorawdtlamt;
                            tbl.sorawtaxtype = "NON TAX";
                            tbl.sorawtaxamt = 0;
                            tbl.sorawvat = 0;
                            tbl.salesid = sales;
                            if (sorawtaxtype == 1)
                            {
                                tbl.sorawtaxtype = "TAX";
                                tbl.sorawtaxamt = 11;
                                tbl.sorawvat = t.sorawdtlamt/11;
                                tbl.sorawtotalamt = t.sorawdtlamt / 1.1M;
                                tbl.sorawtotalnetto = t.sorawdtlamt/ 1.1M;
                            }
                            tbl.sorawgrandtotalamt = t.sorawdtlamt;
                            tbl.sorawmststatus = "Approved";
                            tbl.sorawmstnote = t.sorawdtlnote;
                            tbl.createuser= Session["UserID"].ToString();
                            tbl.createtime = servertime;
                            tbl.upduser= Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.rate2oid = 0;
                            tbl.sorawratetoidr = 1;
                            tbl.sorawrate2toidr = 1;
                            tbl.sorawtotalamtidr = t.sorawdtlamt;
                            tbl.sorawtotalamtusd = 1;
                            tbl.sorawtotalnettoidr = t.sorawdtlamt;
                            tbl.sorawtotalnettousd = 1;
                            tbl.sorawgrandtotalamtidr = t.sorawdtlamt;
                            tbl.sorawgrandtotalamtusd = 1;
                            tbl.sorawratetousd = 1;
                            tbl.sorawrate2tousd = 1;
                            tbl.sorawinvoiceval = "FOB";
                            tbl.sorawportship = "";
                            tbl.sorawportdischarge = "";
                            tbl.sorawetd= t.etd;
                            tbl.sorawbank = "0";
                            tbl.sorawpaymethod = "TT";
                            tbl.accountoid = 0;
                            tbl.revisetime = new DateTime(1900, 1, 1);
                            tbl.rejecttime= new DateTime(1900, 1, 1);
                            tbl.socustrefdate = servertime;
                            tbl.isexcludetax = true;
                            tbl.revisereason = "";
                            tbl.rejectuser = "";
                            tbl.reviseuser = "";
                            tbl.rejectreason = "";
                            tbl.sorawcustref = t.pono;
                            tbl.custdtloid = t.custdtloid;
                            db.QL_trnsorawmst.Add(tbl);
                            db.SaveChanges();
                            decimal dDiv = 1M;
                            if (!tbl.isexcludetax)
                            {
                                dDiv = 1.1M;
                            }

                            QL_trnsorawdtl tbldtl = new QL_trnsorawdtl();
                            tbldtl = new QL_trnsorawdtl();
                            t.sorawprice_intax = t.sorawprice / dDiv;
                            t.sorawdtlamt_intax = t.sorawdtlamt / dDiv;
                            t.sorawdtldiscvalue_intax = 0 / dDiv;
                            t.sorawdtldiscamt_intax = 0 / dDiv;
                            t.sorawdtlnetto_intax = t.sorawdtlamt / dDiv;
       
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.sorawdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.sorawmstoid = tbl.sorawmstoid;
                            tbldtl.sorawdtlseq = 1;
                            tbldtl.matrawoid = 0;
                            tbldtl.sorawqty = t.sorawqty;
                            tbldtl.sorawunitoid = 337;//PCS 352 KGM
                            tbldtl.sorawprice = t.hjual;
                            tbldtl.sorawprice_intax = t.sorawprice_intax;
                            tbldtl.sorawdtlamt = t.sorawdtlamt;
                            tbldtl.sorawdtlamt_intax = t.sorawdtlamt_intax;
                            tbldtl.sorawdtldisctype = "P";
                            tbldtl.sorawdtldiscvalue = 0;
                            tbldtl.sorawdtldiscvalue_intax = 0;
                            tbldtl.sorawdtldiscamt = 0;
                            tbldtl.sorawdtldiscamtidr = 0;
                            tbldtl.sorawdtldiscamtusd = 0;
                            tbldtl.sorawdtldiscamt_intax = 0;
                            tbldtl.sorawdtlnetto = t.sorawdtlamt;
                            tbldtl.sorawdtlnetto_intax = t.sorawdtlnetto_intax;
                            tbldtl.sorawdtlstatus = "";
                            tbldtl.sorawdtlnote = t.sorawdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.Psht = t.Psht;
                            tbldtl.Lsht = t.Lsht;
                            tbldtl.sodtldesc = t.sodtldesc;
                            tbldtl.g1 = t.gramatur.Substring(0,1).ToUpper().Trim();
                            tbldtl.g2 = t.gramatur.Substring(1, 1).ToUpper().Trim();
                            tbldtl.g3 = t.gramatur.Substring(2, 1).ToUpper().Trim();
                            if (t.tw == "DW")
                            {
                                tbldtl.g4 = t.gramatur.Substring(3, 1).ToUpper().Trim();
                                tbldtl.g5 = t.gramatur.Substring(4, 1).ToUpper().Trim();
                            }
                            else
                            {
                                tbldtl.g4 = "";
                                tbldtl.g5 = "";
                            }
                            tbldtl.custdtloid = t.custdtloid;
                            tbldtl.tw = t.tw;
                            tbldtl.flute = t.flute;
                            tbldtl.tsheet = t.tsheet;
                            tbldtl.c1 = t.c1;
                            tbldtl.c2 = t.c2;
                            tbldtl.c3 = t.c3;
                            tbldtl.c4 = t.c4;
                            tbldtl.c5 = t.c5;
                            tbldtl.etd = t.etd;
                            tbldtl.pbhn = t.pbhn;
                            tbldtl.outs = t.outs;
                            tbldtl.lbhn = t.lbhn;
                            tbldtl.rm = t.rm;
                            tbldtl.puk = t.luk;
                            tbldtl.luk = t.luk;
                            tbldtl.luas = t.luas;
                            tbldtl.hjual = t.hjual;
                            tbldtl.discmargin = t.discmargin;
                            tbldtl.hindex = t.hindex;
                            tbldtl.disc = t.disc;
                            tbldtl.hinc = t.hinc;
                            tbldtl.custoid = t.custoid;
                            tbldtl.gramatur = t.gramatur;
                            tbldtl.pono = t.pono;
                            db.QL_trnsorawdtl.Add(tbldtl);
                            db.SaveChanges();
                            mstoid++;
                           
                        }
                        //Update lastoid
                        sSql = "UPDATE QL_MSTOID SET lastoid=" + (mstoid - 1) + " WHERE tablename='QL_trnsorawmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_MSTOID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnsorawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                sMsg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                  
                        }
                      
                    }
                }
            }
            return Json(sMsg, JsonRequestBehavior.AllowGet);
        }

        // POST: sorawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsorawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.sorawno == null)
                tbl.sorawno = "";

            if (tbl.sorawdate > tbl.sorawetd)
            {
                ModelState.AddModelError("", "ETD must be more than SO DATE!");
            }

            List<sorawdtl> dtDtl = (List<sorawdtl>)Session["QL_trnsorawdtl"];
            //List<container> dtDtlCont = (List<container>)Session["QL_trnsorawcont"];

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.sorawdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            msg = "";
            if (msg != "")
                ModelState.AddModelError("", msg);

            if (dtDtl == null)
            {
                ModelState.AddModelError("", "Please fill detail data!");
            }
            else
            {
                if (dtDtl.Count <= 0)
                {
                    ModelState.AddModelError("", "Please fill detail data!");
                }
                else
                {
                    if (dtDtl != null)
                    {
                        if (dtDtl.Count > 0)
                        {
                            for (var i = 0; i < dtDtl.Count; i++)
                            {
                                if (dtDtl[i].sorawprice <= 0)
                                {
                                    ModelState.AddModelError("", "SO PRICE for code " + dtDtl[i].matrawlongdesc + " must more than 0");
                                }
                                if (dtDtl[i].sorawqty <= 0)
                                {
                                    ModelState.AddModelError("", "SO QTY for code " + dtDtl[i].matrawlongdesc + " must more than 0");
                                }
                                if (dtDtl[i].Lsht != dtDtl[i].c1+ dtDtl[i].c2+ dtDtl[i].c3+ dtDtl[i].c4+ dtDtl[i].c5 & (dtDtl[i].c1 + dtDtl[i].c2 + dtDtl[i].c3 + dtDtl[i].c4 + dtDtl[i].c5) != 0)
                                {
                                    ModelState.AddModelError("", "SO creasing for code " + dtDtl[i].matrawlongdesc + " must be same with Lsht");
                                }
                            }
                        }
                    }
                }
            }
            //if (dtDtlCont == null)
            //{
            //    ModelState.AddModelError("", "CONTAINER DATA is Empty!");
            //}
            if (tbl.sorawportship == null)
            {
                //ModelState.AddModelError("", "Please Fill PORT OF SHIP Field!");
            }
            if (tbl.sorawportdischarge == null)
            {
                //ModelState.AddModelError("", "Please Fill PORT OF DISCHARGE Field!");
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.sorawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnsorawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL
            if (tbl.sorawmststatus == "Revised")
                tbl.sorawmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.sorawmststatus = "In Process";

            tbl.sorawportship = tbl.sorawportship == null ? "" : tbl.sorawportship;
            tbl.sorawportdischarge = tbl.sorawportdischarge == null ? "" : tbl.sorawportdischarge;

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnsorawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsorawdtl");
                var socontoid = ClassFunction.GenerateID("QL_trnsorawcont");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                if (tbl.custoid == 0)
                    tbl.custoid = -1;
                if (tbl.curroid == 0)
                    tbl.curroid = 1;
                if (tbl.accountoid == 0)
                    tbl.accountoid = -1;
                tbl.sorawmstdisctype = "P";
                tbl.sorawmstdiscvalue = 0;
                tbl.sorawmstdiscamt = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnsorawmst.Find(tbl.cmpcode, tbl.sorawmstoid) != null)
                                tbl.sorawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.sorawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.sorawbank = "18";
                            db.QL_trnsorawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.sorawmstoid + " WHERE tablename='QL_trnsorawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            tbl.sorawbank = "18";
                            db.SaveChanges();

                           
                        }

                       
                       
                        // INSERT APPROVAL
                        if (tbl.sorawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "SO-RM" + tbl.sorawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnsorawmst";
                                tblApp.oid = tbl.sorawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.sorawmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: sorawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsorawmst tbl = db.QL_trnsorawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnsorawdtl.Where(a => a.sorawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnsorawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                     

                        db.QL_trnsorawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, Boolean cbprice)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOSheetPrintOut.rpt"));
  

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE som.cmpcode='"+ cmp +"' AND som.sorawmstoid IN (" + id + ")");
            report.SetParameterValue("sType", "raw");
            report.SetParameterValue("sqlJoinMat", "(SELECT x.sorawdtloid matoid, 'Sheet' matcode, x.sodtldesc matlongdesc, '' matnote FROM QL_trnsorawdtl x) m ON matoid=sod.sorawdtloid");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            report.Close();report.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "SOSheet.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}