﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class ProdresController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prodresmst
        {
            public string cmpcode { get; set; }
            public int prodresmstoid { get; set; }
            public string divgroup { get; set; }
            public string prodresno { get; set; }
            public string wono { get; set; }
            public string itemlongdesc { get; set; }
            public string process { get; set; }
            public string prodresmstnote { get; set; }
            public decimal resultqty { get; set; }
            public decimal rejectqty { get; set; }
            public string result { get; set; }
            public string prodresmststatus { get; set; }
        }
   
        public class printout_bom
        {
            public string BusinessUnit { get; set; }
            public int ID { get; set; }
            public string WIPCode { get; set; }
            public string WIPDesc { get; set; }
            public string FGCode { get; set; }
            public string FinishGood { get; set; }
            public string FromDept { get; set; }
            public string ToDept { get; set; }
            public decimal WIPQty { get; set; }
            public string WIPUnit { get; set; }
            public string HeaderNote { get; set; }
            public string CreateUser { get; set; }
            public DateTime CreateDateTime { get; set; }
            public string LastUpdUser { get; set; }
            public DateTime LastUpdDateTime { get; set; }
            public decimal Thick { get; set; }
            public decimal Width { get; set; }
            public decimal Length { get; set; }
            public int IDDetail { get; set; }
            public int Nmr { get; set; }
            public string Type { get; set; }
            public string Code { get; set; }
            public string Description { get; set; }
            public decimal Qty { get; set; }
            public string Unit { get; set; }
            public string DetailNote { get; set; }
        }

        private string generateNo()
        {
            
            var sNo =  "RSLT-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prodresno, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnprodresmst WHERE cmpcode='" + CompnyCode + "' AND prodresno LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }

        public class mesin
        {
            public string gendesc { get; set; }
            public int genoid { get; set; }
        }

        private void InitDDL(QL_trnprodresmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnprodresmst tbl)
        {
            ViewBag.itemlongdesc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid + "'").FirstOrDefault();
            ViewBag.spkno = db.Database.SqlQuery<string>("SELECT wono FROM QL_trnwomst WHERE cmpcode='" + CompnyCode + "' AND womstoid='" + tbl.womstoid + "'").FirstOrDefault();
            ViewBag.process = db.Database.SqlQuery<string>("SELECT gendesc FROM ql_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid='" + tbl.processoid + "'").FirstOrDefault();
            ViewBag.spkqty = db.Database.SqlQuery<decimal>("SELECT spkqty FROM ql_trnwomst WHERE cmpcode='" + CompnyCode + "' AND womstoid='" + tbl.womstoid + "'").FirstOrDefault();
            ViewBag.prosesbfr = db.Database.SqlQuery<string>("SELECT isnull(gendesc,'') FROM QL_trnprodresmst p inner join ql_mstgen g on g.genoid=p.processoid WHERE p.cmpcode='" + CompnyCode + "' and p.womstoid='"+tbl.womstoid+"' AND p.wodtlseq='"+ (tbl.wodtlseq-1) + "'").FirstOrDefault();
            ViewBag.resultqtybfr = db.Database.SqlQuery<decimal>("SELECT isnull(resultqty,0.0) FROM QL_trnprodresmst p  WHERE p.cmpcode='" + CompnyCode + "' and p.womstoid='" + tbl.womstoid + "' AND p.wodtlseq='" + (tbl.wodtlseq - 1) + "'").FirstOrDefault();
            ViewBag.rejectqtybfr = db.Database.SqlQuery<decimal>("SELECT isnull(rejectqty,0.0) FROM QL_trnprodresmst p WHERE p.cmpcode='" + CompnyCode + "' and p.womstoid='" + tbl.womstoid + "' AND p.wodtlseq='" + (tbl.wodtlseq - 1) + "'").FirstOrDefault();
        }

        public class womst
        {
            public int womstoid { get; set; }
            public string wono { get; set; }
            public string womstnote { get; set; }
            public int itemoid { get; set; }
            public decimal spkqty { get; set; }
            public string itemlongdesc { get; set; }
            public DateTime wodate { get; set; }
        }

        [HttpPost]
        public ActionResult GetwoData(string cmp, int divgroupoid)
        {
            List<womst> tbl = new List<womst>();

            sSql = "SELECT wodate, womstoid,wono, isnull(womstnote,'') womstnote, w.itemoid,i.itemlongdesc,spkqty FROM QL_trnwomst w inner join QL_mstitem i on w.itemoid=i.itemoid WHERE isnull(womstres1,'')='' and w.cmpcode='" + CompnyCode + "' AND w.womststatus='Post' and (SELECT COUNT(*) FROM QL_trnwodtl x WHERE x.womstoid=w.womstoid AND ISNULL(x.wodtlstatus,'') = '') > 0 and w.divgroupoid=" + divgroupoid + "  ORDER BY wono";
            tbl = db.Database.SqlQuery<womst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class proces
        {
            public int processoid { get; set; }
            public string process { get; set; }
            public int wodtlseq { get; set; }
            public string prosesbfr { get; set; }
            public decimal resultqtybfr { get; set; }
            public decimal rejectqtybfr { get; set; }

        }
        [HttpPost]
        public ActionResult GetspkdtlData(string cmp, int womstoid)
        {
            List<proces> tbl = new List<proces>();

            sSql = "SELECT TOP 1 w.wodtlseq ,w.procesoid processoid, g.gendesc process, isnull((select isnull(gx.gendesc,'') from ql_trnprodresmst p inner join ql_trnwodtl x on p.wodtlseq=x.wodtlseq inner join ql_mstgen gx on gx.genoid=x.procesoid where x.womstoid=w.womstoid and p.womstoid=w.womstoid and p.wodtlseq=w.wodtlseq-1),'') prosesbfr, isnull((select isnull(p.resultqty,0.0) from ql_trnprodresmst p inner join ql_trnwodtl x on p.wodtlseq=x.wodtlseq where x.womstoid=w.womstoid and p.womstoid=w.womstoid and p.wodtlseq=w.wodtlseq-1),0.0) resultqtybfr, isnull((select isnull(p.rejectqty,0.0) from ql_trnprodresmst p inner join ql_trnwodtl x on p.wodtlseq=x.wodtlseq inner join ql_mstgen gx on gx.genoid=x.procesoid where x.womstoid=w.womstoid and p.womstoid=w.womstoid and p.wodtlseq=w.wodtlseq-1),0.0) rejectqtybfr FROM QL_trnwodtl w INNER JOIN QL_mstgen g ON g.genoid=w.procesoid WHERE g.cmpcode='" + CompnyCode + "' AND g.activeflag='ACTIVE' and w.womstoid='" + womstoid + "' and wodtlstatus='' ORDER BY wodtlseq";
            tbl = db.Database.SqlQuery<proces>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: Prodres
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
           
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "p", divgroupoid, "divgroupoid");
            sSql = "SELECT prodresmstoid, (select gendesc from ql_mstgen where genoid=w.divgroupoid) divgroup, prodresno, prodresdate, prodresmstnote, wono, itemlongdesc, gendesc process, result, resultqty, rejectqty, prodresmststatus FROM QL_trnprodresmst p INNER JOIN QL_trnwomst w ON w.womstoid=p.womstoid inner join ql_mstitem i on i.itemoid=p.itemoid inner join ql_mstgen g on p.processoid=g.genoid WHERE isnull(prodresmstres1,'')='' and ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "p.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "p.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND prodresmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND prodresmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND prodresmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND prodresdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND prodresdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND prodresmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND p.createuser='" + Session["UserID"].ToString() + "'";

            List<prodresmst> dt = db.Database.SqlQuery<prodresmst>(sSql).ToList();
            InitAdvFilterIndex(modfil, "QL_trnprodresmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: BillOfMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
          if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnprodresmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnprodresmst();
                tbl.prodresmstoid = ClassFunction.GenerateID("QL_mstbillofmat");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.prodresmststatus = "In Process";
                tbl.suppoid = 0;
                tbl.deptoid = 0;
                tbl.deptreplaceoid = 0;
                tbl.rejectqty = 0;
                tbl.resultqty = 0;

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnprodresmst.Find(CompnyCode, id);
               
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: BillOfMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnprodresmst tbl, string action, string process)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            // TAMBAHAN VALIDASI UTK BAGIAN HEADER
            if (tbl.womstoid == 0)
                ModelState.AddModelError("", "This field SPK is required.");
            if (tbl.itemoid == 0)
                ModelState.AddModelError("", "This field ITEM is required.");

            if (tbl.prodresmstnote != null)
            {
                if (tbl.prodresmstnote != "")
                {
                    tbl.prodresmstnote = tbl.prodresmstnote.ToUpper();
                }
            }
            if (tbl.rejectqty == null)
            {
                tbl.rejectqty = 0;
            }
            if (tbl.resultqty == null)
            {
                tbl.resultqty = 0;
            }
            if (tbl.resultqty == 0)
                ModelState.AddModelError("", "Result QTY must be more than 0.");
           
            var servertime = ClassFunction.GetServerTime();
            tbl.prodresno = "";
            if (tbl.prodresmststatus == "Post")
            {
                tbl.prodresno = generateNo();
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnprodresmst");
                var cek_wo = db.QL_trnwodtl.Where(x => x.womstoid == tbl.womstoid).Count();
                var next_proses = new List<string> { "CORRUGATOR", "FINISHING" };
                if (tbl.prodresmststatus == "Post" && !next_proses.Contains(process.ToUpper()))
                {
                    tbl.prodresmststatus = "Completed";
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.prodresmstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.prodresdate = servertime;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            db.QL_trnprodresmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.prodresmstoid + " WHERE tablename='QL_trnprodresmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "update ql_trnwodtl set wodtlstatus='' where womstoid=" + tbl.womstoid + " and procesoid=" + tbl.processoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //sSql = "update ql_trnprodresmst set prodresmststatus='Completed' where womstoid=" + tbl.womstoid + " and wodtlseq=" + (tbl.wodtlseq - 1) + "";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        sSql = "update ql_trnwodtl set wodtlstatus='Completed' where womstoid=" + tbl.womstoid + " and procesoid=" + tbl.processoid + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.prodresmststatus == "Post" || tbl.prodresmststatus == "Completed")
                        {
                            if (process.ToUpper() == "FINISHING" || cek_wo == 1)
                            {
                                sSql = "update ql_trnwomst set womststatus='Closed' where womstoid=" + tbl.womstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        tbl.prodresmststatus = "In Process";
                        ModelState.AddModelError("", err);
                    }
                }
            }
            
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: BillOfMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnprodresmst tbl = db.QL_trnprodresmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_trnprodresmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: BillOfMaterial/PrintReport/5/11
        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_mstbillofmat.Find(cmp, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptBillOfMaterial.rpt"));
            sSql = "SELECT divname [BusinessUnit], bom.bomoid [ID], bomcode [WIPCode], bomwipdesc [WIPDesc.], itemcode [FGCode], itemlongdesc [FinishGood], df.deptname [FromDept], dt.deptname [ToDept], bomqty [WIPQty], g1.gendesc [WIPUnit], bomnote [HeaderNote], (bom.createuser + ISNULL((SELECT ' - ' + profname FROM QL_mstprof pr WHERE pr.cmpcode=bom.cmpcode AND pr.profoid=bom.createuser), '')) [CreateUser], bom.createtime [CreateDateTime], (bom.upduser + ISNULL((SELECT ' - ' + profname FROM QL_mstprof pr WHERE pr.cmpcode=bom.cmpcode AND pr.profoid=bom.upduser), '')) [LastUpdUser], bom.updtime [LastUpdDateTime], ISNULL(bomthick, 0.0) [Thick], ISNULL(bomwidth, 0.0) [Width], ISNULL(bomlength, 0.0) [Length], bomdtloid [IDDetail], bomdtlseq [Nmr], (CASE bomreftype WHEN 'Material' THEN 'MAT' ELSE UPPER(bomreftype) END) [Type], (CASE bomreftype WHEN 'Material' THEN (SELECT matrawcode FROM QL_mstmatraw m WHERE matrawoid=bomrefoid) WHEN 'Kayu' THEN (SELECT cat1code + '.' + cat2code FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=bomrefoid) ELSE (SELECT bom.bomcode FROM QL_mstbillofmat bom WHERE bom.cmpcode=bod.cmpcode AND bom.bomoid=bod.bomrefoid) END) [Code], (CASE bomreftype WHEN 'Material' THEN (SELECT matrawlongdesc FROM QL_mstmatraw m WHERE matrawoid=bomrefoid) WHEN 'Kayu' THEN (SELECT RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE cat2oid=bomrefoid) ELSE (SELECT bomwipdesc + ' ' + itemlongdesc FROM QL_mstbillofmat bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bom.cmpcode=bod.cmpcode AND bom.bomoid=bod.bomrefoid) END) [Description], bomdtlqty [Qty], g2.gendesc [Unit], bomdtlnote [DetailNote] FROM QL_mstbillofmat bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid INNER JOIN QL_mstdept df ON df.cmpcode=bom.cmpcode AND df.deptoid=bomfromdeptoid INNER JOIN QL_mstdept dt ON dt.cmpcode=bom.cmpcode AND dt.deptoid=bomtodeptoid INNER JOIN QL_mstdivision div ON div.cmpcode=bom.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=bomunitoid INNER JOIN QL_mstbillofmatdtl bod ON bod.cmpcode=bom.cmpcode AND bod.bomoid=bom.bomoid INNER JOIN QL_mstgen g2 ON g2.genoid=bomdtlunitoid WHERE bom.cmpcode='" + cmp + "' AND bom.bomoid=" + id + " ORDER BY bom.bomoid, bomdtlseq";
            List<printout_bom> dtRpt = db.Database.SqlQuery<printout_bom>(sSql).ToList();

            report.SetDataSource(dtRpt);
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "BillOfMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
