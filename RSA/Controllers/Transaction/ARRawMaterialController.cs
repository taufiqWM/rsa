﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class ARRawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class arrawmst
        {
            public string cmpcode { get; set; }
            public int arrawmstoid { get; set; }
            public string divgroup { get; set; }
            public string arrawno { get; set; }
            public DateTime arrawdate { get; set; }
            public string invoiceno { get; set; }
            public string custname { get; set; }
            public string arrawmststatus { get; set; }
            public string arrawmstnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal arrawgrandtotal { get; set; }
            public string divname { get; set; }
        }

        public class arrawdtl
        {
            public int arrawdtlseq { get; set; }
            public int shipmentrawmstoid { get; set; }
            public string shipmentrawno { get; set; }
            public DateTime shipmentrawetd { get; set; }
            public int shipmentrawdtloid { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public string matrawnote { get; set; }
            public decimal arrawqty { get; set; }
            public int arrawunitoid { get; set; }
            public string arrawunit { get; set; }
            public decimal arrawprice { get; set; }
            public decimal arrawdtlamt { get; set; }
            public string arrawdtldisctype { get; set; }
            public decimal arrawdtldiscvalue { get; set; }
            public decimal arrawdtldiscamt { get; set; }
            public decimal arrawdtlnetto { get; set; }
            public string arrawdtlnote { get; set; }
        }

        public class shipmentraw
        {
            public int shipmentrawmstoid { get; set; }
            public string shipmentrawno { get; set; }
            public DateTime shipmentrawdate { get; set; }
            public DateTime shipmentrawetd { get; set; }
            public string shipmentrawmstnote { get; set; }
            public string sorawno { get; set; }
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        private void InitDDL(QL_trnarrawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var arrawpaytypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.arrawpaytypeoid);
            ViewBag.arrawpaytypeoid = arrawpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnarrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + tbl.cmpcode + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvalcode = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvalcode);
            //ViewBag.approvalcode = approvalcode;
        }

        [HttpPost]
        public ActionResult InitDDLAppUser(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_approvalperson> tbl = new List<QL_approvalperson>();
            sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_trnarrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
            tbl = db.Database.SqlQuery<QL_approvalperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();
            sSql = "SELECT * FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' And s.divgroupoid="+divgroupoid+" AND s.activeflag='ACTIVE' AND s.custoid IN (SELECT custoid FROM QL_trnshipmentrawmst WHERE cmpcode='" + cmp + "' AND shipmentrawmststatus='Post' AND ISNULL(shipmentrawmstres1,'')<>'Closed') ORDER BY s.custname";

            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipmentData(string cmp, int custoid, int curroid, int divgroupoid)
        {
            List<shipmentraw> tbl = new List<shipmentraw>();
            sSql = "SELECT DISTINCT sm.shipmentrawmstoid, shipmentrawno, shipmentrawdate, shipmentrawmstnote,  dorawno sorawno, shipmentrawetd FROM QL_trnshipmentrawmst sm INNER JOIN QL_trnshipmentrawdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentrawmstoid=sm.shipmentrawmstoid INNER JOIN QL_trndorawmst dom ON dom.cmpcode=sd.cmpcode AND dom.dorawmstoid=sd.dorawmstoid  WHERE sm.cmpcode='" + cmp + "' AND sm.custoid=" + custoid + " AND shipmentrawmststatus='Post' AND ISNULL(shipmentrawmstres1,'')<>'Closed' AND sm.curroid=" + curroid + " and sm.divgroupoid="+divgroupoid+" ORDER BY shipmentrawdate DESC, sm.shipmentrawmstoid DESC";

            tbl = db.Database.SqlQuery<shipmentraw>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int shipmentrawmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<arrawdtl> tbl = new List<arrawdtl>();
            sSql = "SELECT sd.shipmentrawmstoid, sd.shipmentrawdtloid, shipmentrawno, shipmentrawetd, shipmentrawdtlseq, sd.matrawoid, 'Sheet' matrawcode, sodtldesc matrawlongdesc, (shipmentrawqty - ISNULL((SELECT SUM(sretrawqty) FROM QL_trnsretrawdtl sretd INNER JOIN QL_trnsretrawmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretrawmstoid=sretd.sretrawmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentrawdtloid=sd.shipmentrawdtloid AND sretrawmststatus<>'Rejected'), 0.0)) AS arrawqty, shipmentrawunitoid arrawunitoid, gendesc AS arrawunit, sorawprice AS arrawprice, sorawdtldisctype AS arrawdtldisctype, sorawdtldiscvalue AS arrawdtldiscvalue, shipmentrawdtlnote matrawnote FROM QL_trnshipmentrawdtl sd INNER JOIN QL_mstgen g ON genoid=shipmentrawunitoid INNER JOIN QL_trndorawdtl sod ON sod.cmpcode=sd.cmpcode AND sod.dorawdtloid=sd.dorawdtloid inner join ql_trnsorawdtl dod on sod.sorawdtloid=dod.sorawdtloid INNER JOIN QL_trnshipmentrawmst sm ON sd.cmpcode=sm.cmpcode AND sd.shipmentrawmstoid=sm.shipmentrawmstoid WHERE sd.cmpcode='" + cmp + "' AND sd.shipmentrawmstoid=" + shipmentrawmstoid + " AND shipmentrawdtlstatus='' AND (shipmentrawqty - ISNULL((SELECT SUM(sretrawqty) FROM QL_trnsretrawdtl sretd INNER JOIN QL_trnsretrawmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretrawmstoid=sretd.sretrawmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentrawdtloid=sd.shipmentrawdtloid AND sretrawmststatus<>'Rejected'), 0.0)) > 0 ORDER BY shipmentrawdtlseq";

            tbl = db.Database.SqlQuery<arrawdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<arrawdtl> dtDtl)
        {
            Session["QL_trnarrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnarrawdtl"] == null)
            {
                Session["QL_trnarrawdtl"] = new List<arrawdtl>();
            }

            List<arrawdtl> dataDtl = (List<arrawdtl>)Session["QL_trnarrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnarrawmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: arrawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "arm", divgroupoid, "divgroupoid");
            sSql = "SELECT arm.cmpcode, (select gendesc from ql_mstgen where genoid=arm.divgroupoid) divgroup, div.divname, arm.arrawmstoid, arm.arrawno, arrawdate, s.custname, ISNULL(arm.arrawmstres3,'') AS invoiceno, arm.arrawmststatus, arm.arrawmstnote,arrawgrandtotal, arrawdatetakegiro, DATEADD(day,(CASE g1.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 ELSE CAST(g1.gendesc AS INT) END), arrawdate) duedate FROM QL_trnarrawmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=arm.cmpcode INNER JOIN QL_mstgen g1 ON g1.genoid=arrawpaytypeoid AND g1.gengroup='PAYMENT TYPE' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "arm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "arm.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND arrawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND arrawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND arrawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND arrawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND arrawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND arrawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND arm.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY CONVERT(DATETIME, arm.arrawdate) DESC, arm.arrawmstoid DESC";

            List<arrawmst> dt = db.Database.SqlQuery<arrawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnarrawmst", true);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: arrawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnarrawmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trnarrawmst();
                tbl.cmpcode = CompnyCode;
                tbl.arrawmstoid = ClassFunction.GenerateID("QL_trnarrawmst");
                tbl.arrawdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revisetime = new DateTime(1900, 1, 1);
                tbl.rejecttime = new DateTime(1900, 1, 1);
                tbl.arrawmststatus = "In Process";
                tbl.arrawtaxvalue = 0;
                tbl.arrawtaxamt = 0;
                
                Session["QL_trnarrawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnarrawmst.Find(cmp, id);

                sSql = "SELECT ard.arrawdtlseq, ard.shipmentrawmstoid, shm.shipmentrawno, shipmentrawetd,  ard.shipmentrawdtloid, ard.matrawoid,m.matrawcode, m.matrawlongdesc, m.matrawnote, ard.arrawqty, ard.arrawunitoid, g.gendesc AS arrawunit, ard.arrawprice,  ard.arrawdtlamt, ard.arrawdtldisctype, ard.arrawdtldiscvalue, ard.arrawdtldiscamt, ard.arrawdtlnetto, ard.arrawdtlnote FROM QL_trnarrawdtl ard INNER JOIN QL_mstmatraw m ON m.matrawoid=ard.matrawoid INNER JOIN QL_mstgen g ON g.genoid=ard.arrawunitoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid INNER JOIN QL_trnshipmentrawmst shm ON shm.cmpcode=shd.cmpcode AND shm.shipmentrawmstoid=shd.shipmentrawmstoid WHERE ard.arrawmstoid=" + id + " ORDER BY ard.arrawdtlseq";
                Session["QL_trnarrawdtl"] = db.Database.SqlQuery<arrawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: arrawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnarrawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.arrawno == null)
                tbl.arrawno = "";
            
            List<arrawdtl> dtDtl = (List<arrawdtl>)Session["QL_trnarrawdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.arrawdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            msg = "";
            if (msg != "")
                ModelState.AddModelError("", msg);

            if (tbl.arrawtaxtype == "TAX")
            {
                if (tbl.arrawtaxvalue > 0)
                {
                    if (tbl.arrawtaxvalue < 0 || tbl.arrawtaxvalue > 100)
                    {
                        ModelState.AddModelError("", "Tax value must be between 0 and 100.");
                    }
                }
            }
            if (Convert.ToDateTime(tbl.arrawdate.ToString("MM/dd/yyyy")) > Convert.ToDateTime(tbl.arrawdatetakegiro.ToString("MM/dd/yyyy")))
            {
                ModelState.AddModelError("", "Date Take Giro must be more or equal than PEB Date.");
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].arrawprice <= 0)
                        {
                            ModelState.AddModelError("", "Price "+ dtDtl[i].matrawcode +" must be greater than 0.");
                        }
                        if (dtDtl[i].arrawdtlamt <= 0)
                        {
                            ModelState.AddModelError("", "Detail Netto " + dtDtl[i].matrawcode + " must be greater than 0.");
                        }
                        sSql = "SELECT COUNT(*) FROM QL_trnshipmentrawmst sd WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid=" + dtDtl[i].shipmentrawmstoid + " AND shipmentrawmststatus='Closed'";
                        if (action == "Update Data")
                        {
                            sSql += " AND shipmentrawmstoid NOT IN (SELECT ard.shipmentrawmstoid FROM QL_trnarrawdtl ard WHERE ard.cmpcode=sd.cmpcode AND arrawmstoid=" + tbl.arrawmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "Shipment No. " + dtDtl[i].shipmentrawno + " has been used by another data. Please cancel this transaction or use another Shipment No.!");
                    }
                }
            }

            // VAR UTK APPROVAL
            var approvaloid = 0;
            List<QL_approvalperson> AppPerson = null;
            if (ModelState.IsValid && tbl.arrawmststatus == "In Approval")
            {
                approvaloid = ClassFunction.GenerateID("QL_approval");
                AppPerson = db.Database.SqlQuery<QL_approvalperson>("SELECT * FROM QL_approvalperson WHERE tablename='QL_trnarrawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" + CompnyCode + "', '" + tbl.cmpcode + "') AND (issuperuser='True' OR approvaluser='" + tbl.approvalcode + "')").ToList();
                if (AppPerson.Count <= 0)
                {
                    ModelState.AddModelError("", "There is no user can't approve this transaction. Please define approval user in APPROVAL USER form.");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.arrawmststatus == "Revised")
                tbl.arrawmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.arrawmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnarrawmst");
                var dtloid = ClassFunction.GenerateID("QL_trnarrawdtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.arrawmstdisctype = "A";
                tbl.arrawmstdiscvalue = 0;
                tbl.arrawmstdiscamt = 0;
                tbl.arrawdeliverycost = 0;
                tbl.arrawothercost = 0;
                tbl.rejectreason = "";
                tbl.rejectuser = "";
                tbl.rejecttime = DateTime.Parse("1/1/1900 00:00:00");
                tbl.revisereason = "";
                tbl.reviseuser = "";
                tbl.revisetime = DateTime.Parse("1/1/1900 00:00:00");
               

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.arrawrate2tousd = tbl.arrawrate2tousd == null ? "" : tbl.arrawrate2tousd;
                        tbl.arrawrate2toidr = tbl.arrawrate2toidr == null ? "" : tbl.arrawrate2toidr;
                        tbl.arrawratetousd = tbl.arrawratetousd == null ? "" : tbl.arrawratetousd;
                        tbl.arrawratetoidr = tbl.arrawratetoidr == null ? "" : tbl.arrawratetoidr;
                        

                        tbl.revisereason = tbl.revisereason == null ? "" : tbl.revisereason;
                        tbl.reviseuser = tbl.reviseuser == null ? "" : tbl.reviseuser;
                        tbl.revisetime = tbl.revisetime == null ? new DateTime(1900, 1, 1) : tbl.revisetime;
                        tbl.rejectreason = tbl.rejectreason == null ? "" : tbl.rejectreason;
                        tbl.rejectuser = tbl.rejectuser == null ? "" : tbl.rejectuser;
                        tbl.rejecttime = tbl.rejecttime == null ? new DateTime(1900, 1, 1) : tbl.rejecttime;

                        if (action == "New Data")
                        {
                            if (db.QL_trnarrawmst.Find(tbl.cmpcode, tbl.arrawmstoid) != null)
                                tbl.arrawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.arrawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnarrawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.arrawmstoid + " WHERE tablename='QL_trnarrawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawdtloid IN (SELECT shipmentrawdtloid FROM QL_trnarrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arrawmstoid=" + tbl.arrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid IN (SELECT shipmentrawmstoid FROM QL_trnarrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arrawmstoid=" + tbl.arrawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnarrawdtl.Where(a => a.arrawmstoid == tbl.arrawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnarrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnarrawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnarrawdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.arrawdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.arrawmstoid = tbl.arrawmstoid;
                            tbldtl.arrawdtlseq = i + 1;
                            tbldtl.shipmentrawdtloid = dtDtl[i].shipmentrawdtloid;
                            tbldtl.shipmentrawmstoid = dtDtl[i].shipmentrawmstoid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.arrawqty = dtDtl[i].arrawqty;
                            tbldtl.arrawunitoid = dtDtl[i].arrawunitoid;
                            tbldtl.arrawprice = dtDtl[i].arrawprice;
                            tbldtl.arrawdtlamt = dtDtl[i].arrawdtlamt;
                            tbldtl.arrawdtldisctype = dtDtl[i].arrawdtldisctype;
                            tbldtl.arrawdtldiscvalue = dtDtl[i].arrawdtldiscvalue;
                            tbldtl.arrawdtldiscamt = dtDtl[i].arrawdtldiscamt;
                            tbldtl.arrawdtlnetto = dtDtl[i].arrawdtlnetto;
                            tbldtl.arrawdtlstatus = "";
                            tbldtl.arrawdtlnote = (dtDtl[i].arrawdtlnote == null ? "" : dtDtl[i].arrawdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                           

                            db.QL_trnarrawdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawdtlstatus='COMPLETE' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawdtloid=" + dtDtl[i].shipmentrawdtloid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid=" + dtDtl[i].shipmentrawmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnarrawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.arrawmststatus == "In Approval")
                        {
                            for (int i = 0; i < AppPerson.Count(); i++)
                            {
                                QL_approval tblApp = new QL_approval();
                                tblApp.cmpcode = tbl.cmpcode;
                                tblApp.requestcode = "AR-RM" + tbl.arrawmstoid.ToString() + "_" + approvaloid.ToString();
                                tblApp.approvaloid = approvaloid++;
                                tblApp.requestuser = tbl.upduser;
                                tblApp.requestdate = tbl.updtime;
                                tblApp.statusrequest = "New";
                                tblApp.tablename = "QL_trnarrawmst";
                                tblApp.oid = tbl.arrawmstoid;
                                tblApp.@event = "In Approval";
                                tblApp.approvalcode = "0";
                                tblApp.approvaluser = AppPerson[i].approvaluser;
                                tblApp.approvaldate = new DateTime(1900, 1, 1);
                                tblApp.approvalnote = "";
                                db.QL_approval.Add(tblApp);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (approvaloid - 1).ToString() + " WHERE tablename='QL_approval'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.arrawmstoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: arrawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarrawmst tbl = db.QL_trnarrawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawdtloid IN (SELECT shipmentrawdtloid FROM QL_trnarrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arrawmstoid=" + tbl.arrawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentrawmstoid IN (SELECT shipmentrawmstoid FROM QL_trnarrawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arrawmstoid=" + tbl.arrawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnarrawdtl.Where(a => a.arrawmstoid == id && a.cmpcode == cmp);
                        db.QL_trnarrawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnarrawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printouttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var sReportName = "";
            if (printouttype == "Doc")
                sReportName = "rptARTrn.rpt";
            else if (printouttype == "Faktur Pajak")
                sReportName = "rptARTrnInvoice.rpt";
            else
                sReportName = "rptARTrnFaktur.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + cmp + "' AND arm.arrawmstoid=" + id);
            report.SetParameterValue("sType", "raw");
            report.SetParameterValue("sMatDesc", "(SELECT sodtldesc FROM QL_trnsorawdtl m WHERE m.sorawdtloid=ard.matrawoid)");
            report.SetParameterValue("sMatCode", "('Sheet')");
            ClassProcedure.SetDBLogonForReport(report);
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
           
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ARSheetReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}