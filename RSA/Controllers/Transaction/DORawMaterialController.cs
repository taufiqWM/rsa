﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class DORawMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class dorawmst
        {
            public string cmpcode { get; set; }
            public int dorawmstoid { get; set; }
            public string divgroup { get; set; }
            public string dorawno { get; set; }
            public DateTime dorawdate { get; set; }
            public string dorawmststatus { get; set; }
            public string custname { get; set; }
            public string dorawmstnote { get; set; }
            public string divname { get; set; }
        }

        public class dorawdtl
        {
            public int dorawdtlseq { get; set; }
            public string sorawno { get; set; }
            public int sorawmstoid { get; set; }
            public int sorawdtloid { get; set; }
            public int matrawoid { get; set; }
            public string matrawcode { get; set; }
            public string matrawlongdesc { get; set; }
            public decimal sorawqty { get; set; }
            public decimal dorawqty { get; set; }
            public int dorawunitoid { get; set; }
            public string dorawunit { get; set; }
            public decimal dorawprice { get; set; }
            public decimal dorawdtlamt { get; set; }
            public string dorawdtldisctype { get; set; }
            public decimal dorawdtldiscvalue { get; set; }
            public decimal dorawdtldiscamt { get; set; }
            public decimal dorawdtlnetto { get; set; }
            public string dorawdtlnote { get; set; }
            public string matrawnote { get; set; }
        }

        public class sorawcust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public string sorawtype { get; set; }
            public string sorawtaxtype { get; set; }
 
        }

        public class custshipto
        {
            public int custdtloid { get; set; }
            public string custdtlcode { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
        }

        private void InitDDL(QL_trndorawmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            sSql = "SELECT * FROM QL_mstgen WHERE gengroup = 'CONTAINER TYPE' AND activeflag = 'ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY gendesc";
            var dorawdelivtypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.dorawdelivtypeoid);
            ViewBag.dorawdelivtypeoid = dorawdelivtypeoid;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int oid, string intax, int doid)
        {
            List<dorawdtl> tbl = new List<dorawdtl>();
            double dDiv = 1;
            if (intax == "False") { dDiv = 1.1; }
            sSql = "SELECT sorawdtloid, som.sorawmstoid, som.sorawno, CAST(ROW_NUMBER() OVER(ORDER BY sorawdtloid) AS INT) sorawdtlseq, sod.sorawdtloid, sorawdtloid matrawoid,'' matrawcode, isnull(sodtldesc,'') matrawlongdesc, (sorawqty - ISNULL((SELECT SUM(dorawqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawmstoid=dod.dorawmstoid), 0)=0 THEN dorawqty ELSE (ISNULL((SELECT SUM(shipmentrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawdtloid=dod.dorawdtloid), 0) -ISNULL((SELECT SUM(sretrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnsretrawdtl sretd ON sd.shipmentrawdtloid=sretd.shipmentrawdtloid INNER JOIN QL_trnsretrawmst sretm ON sretd.sretrawmstoid=sretm.sretrawmstoid  WHERE sd.cmpcode=dod.cmpcode AND sretrawmststatus<>'Rejected' AND sd.dorawdtloid=dod.dorawdtloid), 0)) END) AS dorawqty FROM QL_trndorawdtl dod INNER JOIN QL_trndorawmst dom ON dom.dorawmstoid=dod.dorawmstoid and dod.cmpcode=dom.cmpcode WHERE dod.cmpcode=sod.cmpcode AND dod.sorawdtloid=sod.sorawdtloid AND dorawmststatus <>'Cancel' AND dod.dorawmstoid<>" + doid +") AS tbltmp), 0.0)) AS sorawqty, 0.0 AS dorawqty, sorawunitoid dorawunitoid, gendesc AS dorawunit, sorawdtlnote AS dorawdtlnote, isnull(sorawdtlnote,'') matrawnote, (sorawprice * " + dDiv + ") AS dorawprice, sorawdtldisctype dorawdtldisctype, ((CASE sorawdtldisctype WHEN 'P' THEN sorawdtldiscvalue ELSE (sorawdtldiscvalue * " + dDiv + ") END)) dorawdtldiscvalue, sorawmstdisctype AS dorawmstdisctype, sorawmstdiscvalue AS dorawmstdiscvalue, (sorawtotalamt - sorawtotaldiscdtl) AS dorawtotalamt FROM QL_trnsorawdtl sod INNER JOIN QL_trnsorawmst som ON som.cmpcode=sod.cmpcode AND som.sorawmstoid=sod.sorawmstoid INNER JOIN QL_mstgen g ON genoid=sorawunitoid WHERE sod.cmpcode='" + cmp + "' AND sod.sorawmstoid=" + oid + " AND sorawdtlstatus='' ORDER BY sorawdtlseq";

            tbl = db.Database.SqlQuery<dorawdtl>(sSql).ToList();
            if (tbl != null)
                if (tbl.Count > 0)
                    for(var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].dorawqty = tbl[i].sorawqty;
                    }
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<dorawdtl> dtDtl)
        {
            Session["QL_trndorawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trndorawdtl"] == null)
            {
                Session["QL_trndorawdtl"] = new List<dorawdtl>();
            }

            List<dorawdtl> dataDtl = (List<dorawdtl>)Session["QL_trndorawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndorawmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.currcode = db.Database.SqlQuery<string>("SELECT currcode FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND curroid=" + tbl.curroid + "").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<sorawcust> tbl = new List<sorawcust>();

            sSql = "SELECT DISTINCT c.custoid, custcode, custname, som.curroid, currcode, sorawtype, sorawtaxtype FROM QL_mstcust c INNER JOIN QL_trnsorawmst som ON som.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=som.curroid WHERE som.cmpcode='" + cmp + "' AND c.divgroupoid=" + divgroupoid + " AND sorawmststatus='Approved' ORDER BY custname";
            tbl = db.Database.SqlQuery<sorawcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipTo(int custoid)
        {
            List<custshipto> tbl = new List<custshipto>();

            sSql = "SELECT * FROM (SELECT custdtloid, custdtlcode, custdtlname, (custdtladdr + ' ' + gendesc) custdtladdr FROM QL_mstcustdtl c INNER JOIN QL_mstgen g ON genoid=custdtlcityoid WHERE c.cmpcode='" + CompnyCode + "' AND custoid=" + custoid + " AND c.activeflag='ACTIVE') AS tblShip ORDER BY custdtlcode";
            tbl = db.Database.SqlQuery<custshipto>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSOData(string cmp, int custoid, int curroid, string dorawtype, string dorawtaxtype, int divgroupoid)
        {
            List<QL_trnsorawmst> tbl = new List<QL_trnsorawmst>();

            sSql = "SELECT * FROM QL_trnsorawmst som WHERE som.cmpcode='" + cmp + "' AND sorawmststatus='Approved' AND sorawtype='" + dorawtype + "' AND sorawtaxtype='" + dorawtaxtype + "' AND custoid=" + custoid + "  AND som.divgroupoid=" + divgroupoid + " AND curroid=" + curroid + " ORDER BY sorawmstoid";
            tbl = db.Database.SqlQuery<QL_trnsorawmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: dorawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dom", divgroupoid, "divgroupoid");
            sSql = "SELECT dom.cmpcode, (select gendesc from ql_mstgen where genoid=dom.divgroupoid) divgroup, dorawmstoid, dorawno, dorawdate, custname, dorawmststatus, dorawmstnote, divname, 'False' AS checkvalue FROM QL_trndorawmst dom INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=dom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "dom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "dom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND dorawmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND dorawmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND dorawmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND dorawdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND dorawdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND dorawmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND dom.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY dorawdate DESC, dom.dorawmstoid DESC";

            List<dorawmst> dt = db.Database.SqlQuery<dorawmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trndorawmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: dorawMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trndorawmst tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_trndorawmst();
                tbl.dorawmstoid = ClassFunction.GenerateID("QL_trndorawmst");
                tbl.dorawdate = ClassFunction.GetServerTime();
                tbl.dorawreqdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.dorawmststatus = "In Process";
                tbl.isexcludetax = true;

                Session["QL_trndorawdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trndorawmst.Find(cmp, id);
                double dDiv = 1.1;
                if (tbl.isexcludetax)
                {
                    dDiv = 1;
                }
                            
                sSql = "SELECT dorawdtlseq, dod.sorawmstoid, sorawno, dod.sorawdtloid, dod.matrawoid,'' matrawcode, sodtldesc matrawlongdesc, '' matrawnote, (sorawqty - ISNULL((SELECT SUM(dorawqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawmstoid=dodxx.dorawmstoid), 0)=0 THEN dorawqty ELSE (ISNULL((SELECT SUM(shipmentrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawdtloid=dodxx.dorawdtloid), 0)-ISNULL((SELECT SUM(sretrawqty) FROM QL_trnshipmentrawdtl sd2 INNER JOIN QL_trnsretrawdtl sretd ON sd2.shipmentrawdtloid=sretd.shipmentrawdtloid INNER JOIN QL_trnsretrawmst sretm ON sretd.sretrawmstoid=sretm.sretrawmstoid  WHERE sd2.cmpcode=dodxx.cmpcode AND sretrawmststatus<>'Rejected' AND sd2.dorawdtloid=dodxx.dorawdtloid), 0)) END) AS dorawqty FROM QL_trndorawdtl dodxx INNER JOIN QL_trndorawmst domxx ON domxx.cmpcode=dodxx.cmpcode AND domxx.dorawmstoid=dodxx.dorawmstoid WHERE dodxx.cmpcode=sod.cmpcode AND dodxx.sorawdtloid=sod.sorawdtloid AND dodxx.dorawmstoid<>" + tbl.dorawmstoid + " AND dorawmststatus <>'Cancel') AS tbltmp), 0.0)) AS sorawqty, dod.dorawqty, dod.dorawunitoid, g2.gendesc AS dorawunit, dod.dorawdtlnote, (dorawprice * " + dDiv + ") dorawprice, (dorawdtlamt * " + dDiv + ") dorawdtlamt, dorawdtldisctype, (CASE dorawdtldisctype WHEN 'P' THEN dorawdtldiscvalue ELSE dorawdtldiscvalue * " + dDiv + " END) dorawdtldiscvalue, (dorawdtldiscamt * " + dDiv + ") dorawdtldiscamt, (dorawdtlnetto * " + dDiv + ") dorawdtlnetto FROM QL_trndorawdtl dod INNER JOIN QL_trnsorawmst som ON som.cmpcode=dod.cmpcode AND som.sorawmstoid=dod.sorawmstoid INNER JOIN QL_trnsorawdtl sod ON sod.cmpcode=dod.cmpcode AND sod.sorawdtloid=dod.sorawdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=dorawunitoid WHERE dorawmstoid=" + id + " AND dod.cmpcode='" + cmp + "' ORDER BY dorawdtlseq";
               
                Session["QL_trndorawdtl"] = db.Database.SqlQuery<dorawdtl>(sSql).ToList();
                List<dorawdtl> tt = new List<dorawdtl>();
                tt = db.Database.SqlQuery<dorawdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dorawMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndorawmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.dorawno == null)
                tbl.dorawno = "";

            List<dorawdtl> dtDtl = (List<dorawdtl>)Session["QL_trndorawdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            if (dtDtl != null)
            {
                if (dtDtl.Count() > 0)
                {
                    DataTable TblDtl = ClassFunction.ToDataTable(dtDtl);
                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].dorawqty <= 0)
                        {
                            ModelState.AddModelError("", dtDtl[i].matrawcode + " QUANTITY must be more than 0!");
                        }else
                        {
                            if (dtDtl[i].dorawqty > dtDtl[i].sorawqty)
                                ModelState.AddModelError("", dtDtl[i].matrawcode + " QUANTITY must be less than SO QTY!");
                        }
                      
                            
                        sSql = "SELECT (sorawqty - ISNULL((SELECT SUM(dorawqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawmstoid=dodxx.dorawmstoid), 0)=0 THEN dorawqty ELSE (ISNULL((SELECT SUM(shipmentrawqty) FROM QL_trnshipmentrawdtl sd INNER JOIN QL_trnshipmentrawmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentrawmstoid=sd.shipmentrawmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentrawmststatus<>'Rejected' AND sd.dorawdtloid=dodxx.dorawdtloid), 0)-ISNULL((SELECT SUM(sretrawqty) FROM QL_trnshipmentrawdtl sd2 INNER JOIN QL_trnsretrawdtl sretd ON sd2.shipmentrawdtloid=sretd.shipmentrawdtloid INNER JOIN QL_trnsretrawmst sretm ON sretd.sretrawmstoid=sretm.sretrawmstoid  WHERE sd2.cmpcode=dodxx.cmpcode AND sretrawmststatus<>'Rejected' AND sd2.dorawdtloid=dodxx.dorawdtloid), 0)) END) AS dorawqty FROM QL_trndorawdtl dodxx INNER JOIN QL_trndorawmst domxx ON domxx.dorawmstoid=dodxx.dorawmstoid WHERE dodxx.cmpcode=sod.cmpcode AND dodxx.sorawdtloid=sod.sorawdtloid AND dodxx.dorawmstoid<>" + tbl.dorawmstoid + " AND dorawmststatus <>'Cancel') AS tbltmp), 0.0)) AS sorawqty FROM QL_trnsorawdtl sod WHERE sod.cmpcode='" + tbl.cmpcode + "' AND sorawdtloid=" + dtDtl[i].sorawdtloid;
                        decimal dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].sorawqty)
                        {
                            dtDtl[i].sorawqty = dQty;
                            if (dQty < dtDtl[i].dorawqty)
                                ModelState.AddModelError("", "SO Qty for detail no. " + dtDtl[i].dorawdtlseq + "  has been updated by another user. Please check that every DO Qty must be less than SO Qty!");
                        }

                        if (tbl.dorawmststatus == "Post")
                        {
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                            sSql = "SELECT ISNULL(saldoakhir,0) FROM (SELECT SUM(qtyin-qtyout) saldoakhir FROM QL_conmat WHERE cmpcode='" + tbl.cmpcode + "' AND refname='SHEET' AND refoid=" + dtDtl[i].matrawoid + ") dt";
                            decimal dStockQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            sSql = "SELECT ISNULL(bookingqty, 0) FROM (SELECT SUM(bookingqty) bookingqty FROM ( ";
                            sSql += "SELECT SUM(dorawqty) bookingqty FROM QL_trndorawmst dom INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=dom.cmpcode AND dod.dorawmstoid=dom.dorawmstoid WHERE dom.cmpcode='" + tbl.cmpcode + "' AND dorawmststatus='Post' AND matrawoid=" + dtDtl[i].matrawoid + " AND dom.updtime<CAST('" + ClassFunction.GetServerTime() + "' AS DATETIME) AND dom.dorawmstoid<>" + tbl.dorawmstoid;
                            sSql += "UNION ALL ";
                            sSql += "SELECT SUM(dorawqty) bookingqty FROM QL_trndorawmst dom INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=dom.cmpcode AND dod.dorawmstoid=dom.dorawmstoid WHERE dom.cmpcode='" + tbl.cmpcode + "' AND dorawmststatus='Closed' AND matrawoid=" + dtDtl[i].matrawoid + " AND dom.updtime<CAST('" + ClassFunction.GetServerTime() + "' AS DATETIME) AND dom.dorawmstoid<>" + tbl.dorawmstoid + " AND dorawdtloid IN (SELECT dorawdtloid FROM QL_trnshipmentrawdtl shd INNER JOIN QL_trnshipmentrawmst shm ON shd.cmpcode=shm.cmpcode AND shd.shipmentrawmstoid=shm.shipmentrawmstoid WHERE shipmentrawmststatus NOT IN ('Approved','Closed','Rejected'))";
                            sSql += ") As Tbl) tbl2";
                            decimal dBookingQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            decimal dQtyDO = Convert.ToDecimal(TblDtl.Compute("SUM(dorawqty)", "matrawoid=" + dtDtl[i].matrawoid));
                            if ((dStockQty - dBookingQty) < dQtyDO)
                                ModelState.AddModelError("", "Total DO Qty for code " + dtDtl[i].matrawcode + " must be less than " + string.Format("{0:#,0.00}", (dStockQty - dBookingQty)) + " {Stock Qty (" + string.Format("{0:#,0.00}", dStockQty) + ") - Booking Qty (" + string.Format("{0:#,0.00}", dBookingQty) + ")}!");
                        }
                    }
                }
            }

            if (!ModelState.IsValid)
                tbl.dorawmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trndorawmst");
                var dtloid = ClassFunction.GenerateID("QL_trndorawdtl");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                if (tbl.dorawmststatus == "Post")
                {
                    string sNo = "DORM" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dorawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndorawmst WHERE cmpcode='" + tbl.cmpcode + "' AND dorawno LIKE '" + sNo + "%'";
                    tbl.dorawno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), Convert.ToInt32(DefaultFormatCounter));
                }
                
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trndorawmst.Find(tbl.cmpcode, tbl.dorawmstoid) != null)
                                tbl.dorawmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dorawdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndorawmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.dorawmstoid + " WHERE tablename='QL_trndorawmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trndorawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND dorawmstoid=" + tbl.dorawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawmstoid IN (SELECT sorawmstoid FROM QL_trndorawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND dorawmstoid=" + tbl.dorawmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trndorawdtl.Where(a => a.dorawmstoid == tbl.dorawmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trndorawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }
                        decimal dDiv = 1M;
                        if (!tbl.isexcludetax)
                        {
                            dDiv = 1.1M;
                        }
                        QL_trndorawdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trndorawdtl();
                            decimal dDiscValue = dtDtl[i].dorawdtldiscvalue;
                            if (dtDtl[i].dorawdtldisctype == "A")
                            {
                                dDiscValue = dtDtl[i].dorawdtldiscvalue / dDiv;
                            }
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.dorawdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.dorawmstoid = tbl.dorawmstoid;
                            tbldtl.dorawdtlseq = i + 1;
                            tbldtl.sorawmstoid = dtDtl[i].sorawmstoid;
                            tbldtl.sorawdtloid = dtDtl[i].sorawdtloid;
                            tbldtl.matrawoid = dtDtl[i].matrawoid;
                            tbldtl.dorawqty = dtDtl[i].dorawqty;
                            tbldtl.dorawunitoid = dtDtl[i].dorawunitoid;
                            tbldtl.dorawprice = dtDtl[i].dorawprice / dDiv;
                            tbldtl.dorawdtlamt = dtDtl[i].dorawdtlamt / dDiv;
                            tbldtl.dorawdtldisctype = dtDtl[i].dorawdtldisctype;
                            tbldtl.dorawdtldiscvalue = dDiscValue;
                            tbldtl.dorawdtldiscamt = dtDtl[i].dorawdtldiscamt / dDiv;
                            tbldtl.dorawdtlnetto = dtDtl[i].dorawdtlnetto / dDiv;
                            tbldtl.dorawdtlstatus = "";
                            tbldtl.dorawdtlnote = (dtDtl[i].dorawdtlnote == null ? "" : dtDtl[i].dorawdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            
                            db.QL_trndorawdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].dorawqty >= dtDtl[i].sorawqty)
                            {
                                sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='COMPLETE', upduser='" + tbl.upduser + "', updtime='" + tbl.updtime + "' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawdtloid=" + dtDtl[i].sorawdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Closed', upduser='" + tbl.upduser + "', updtime='" + tbl.updtime + "' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawmstoid=" + dtDtl[i].sorawmstoid + " AND (SELECT COUNT(*) FROM QL_trnsorawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND sorawdtlstatus='' AND sorawmstoid=" + dtDtl[i].sorawmstoid + " AND sorawdtloid<>" + dtDtl[i].sorawdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }  
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trndorawdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.dorawmstoid );
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dorawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndorawmst tbl = db.QL_trndorawmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsorawdtl SET sorawdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawdtloid IN (SELECT sorawdtloid FROM QL_trndorawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND dorawmstoid=" + tbl.dorawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsorawmst SET sorawmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND sorawmstoid IN (SELECT sorawmstoid FROM QL_trndorawdtl WHERE cmpcode='" + tbl.cmpcode + "' AND dorawmstoid=" + tbl.dorawmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trndorawdtl.Where(a => a.dorawmstoid == id && a.cmpcode == cmp);
                        db.QL_trndorawdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trndorawmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, Boolean cbprice)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trndorawmst.Find(cmp, id);
            if (tbl == null)
                return null;
            if (!cbprice)
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDO_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDO_TrnPrice.rpt"));

            string Slct = "";
            string Join = "";
           
                Slct = ", currcode AS [Curr. Code], currsymbol AS [Curr. Symbol], sorawprice AS [SO Price], dod.dorawprice AS [DO Price], dod.dorawdtlamt AS [DO Amount] ";
                Join = "INNER JOIN QL_trnsorawdtl sod ON sod.cmpcode=dod.cmpcode AND sod.sorawdtloid=dod.sorawdtloid INNER JOIN QL_mstcurr cu ON cu.curroid=dom.curroid ";
            

            sSql = "SELECT dom.cmpcode, [BU Name], [BU Address], [BU City], [BU Province], [BU Country], [BU Telp. 1], [BU Telp. 2], [BU Fax 1], [BU Fax 2], [BU Email], [BU Post Code], dom.dorawmstoid AS [MstOid], CONVERT(VARCHAR(20), dom.dorawmstoid) AS [Draft No.], dom.dorawno AS [DO No.], dom.dorawdate AS [DO Date], dom.dorawmstnote AS [Header Note], dom.dorawmststatus AS [Status], dom.custoid, [cust Name], [cust Code], [cust Address], [cust City], [cust Province], [cust Email], [cust Phone1], [cust Phone2], [cust Phone3], [cust Fax1], [cust Fax2], [cust Country], g1.gendesc AS [Delivery Type], dorawreqdate AS [Req. Shipment Date], dorawcustpono AS [Direct Cust. PO No.], dorawdest AS [Destination], dom.createuser AS [Created By], dom.createtime AS [Created Time], dom.upduser AS [Posted By], dom.updtime AS [Posted Time], dod.dorawdtloid AS [DtlOid], dod.dorawdtlseq AS [No.], som.sorawno AS [SO No.], etd AS [SO ETD], sod.sorawdtloid AS [MatOid], 'Sheet' AS [Code], sodtldesc AS [Material], dod.dorawqty AS [Qty], g2.gendesc AS [Unit], dod.dorawdtlnote AS [Note] , dom.dorawtotalamt AS [DO Total Amount]" + Slct + " FROM QL_trndorawmst dom INNER JOIN QL_mstgen g1 ON g1.genoid=dom.dorawdelivtypeoid INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=dom.cmpcode AND dod.dorawmstoid=dom.dorawmstoid INNER JOIN QL_trnsorawmst som ON som.cmpcode=dod.cmpcode AND som.sorawmstoid=dod.sorawmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=dod.dorawunitoid " + Join + " ";
            //Tbl Division
            sSql += " INNER JOIN (SELECT di.cmpcode, divname AS [BU Name], divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], g3.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Telp. 1], ISNULL(divphone2, '') AS [BU Telp. 2], ISNULL(divfax1, '') AS [BU Fax 1], ISNULL(divfax2, '') AS [BU Fax 2], ISNULL(divemail, '') AS [BU Email], ISNULL(divpostcode, '') AS [BU Post Code] FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid=divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) TblDiv ON TblDiv.cmpcode=dom.cmpcode";
            //Tbl Customer
            sSql += " INNER JOIN (SELECT c.custoid, c.custname AS [cust Name], c.custcode AS [cust Code], c.custaddr AS [cust Address], g1.gendesc AS [cust City], g2.gendesc AS [cust Province], c.custemail AS [cust Email], c.custphone1 AS [cust Phone1], c.custphone2 AS [cust Phone2], c.custphone3 AS [cust Phone3], c.custfax1 AS [cust Fax1], c.custfax2 AS [cust Fax2], g3.gendesc AS [cust Country] FROM QL_mstcust c INNER JOIN QL_mstgen g1 ON g1.cmpcode=c.cmpcode AND g1.gengroup='CITY' AND g1.genoid=c.custcityOid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) tblCust ON tblCust.custoid=dom.custoid WHERE dom.cmpcode='" + cmp + "' AND dom.dorawmstoid=" + id + " ORDER BY dom.dorawmstoid, dod.dorawdtlseq";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptDORM");
            report.SetDataSource(dtRpt);

            report.SetParameterValue("Header", "DO SHEET PRINT OUT");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "DOSHPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
