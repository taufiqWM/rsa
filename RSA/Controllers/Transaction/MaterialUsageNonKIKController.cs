﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class MaterialUsageNonKIKController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string HRISServer = System.Configuration.ConfigurationManager.AppSettings["HRISServer"];
        private string sSql = "";

        public class matusagemst
        {
            public string cmpcode { get; set; }
            public string divname { get; set; }
            public int matusagemstoid { get; set; }
            public string matusageno { get; set; }
            public DateTime matusagedate { get; set; }
            public string deptname { get; set; }
            public string matusagewh { get; set; }
            public string matusagemststatus { get; set; }
            public string matusagemstnote { get; set; }
            public string createuser { get; set; }
        }
        
        public class matusagedtl
        {
            public int matusagedtlseq { get; set; }
            public int matreqmstoid { get; set; }
            public DateTime matreqdate { get; set; }
            public string matreqno { get; set; }
            public int matusagewhoid { get; set; }
            public string matusagewh { get; set; }
            public int matreqdtloid { get; set; }
            public string matusagereftype { get; set; }
            public int matusagerefoid { get; set; }
            public string matusagerefcode { get; set; }
            public string matusagereflongdesc { get; set; }
            public decimal matusageqty { get; set; }
            public decimal matreqqty_awal { get; set; }
            public decimal matreqqty { get; set; }         
            public decimal stockqty { get; set; }
            public int matusageunitoid { get; set; }
            public string matusageunit { get; set; }
            public string matusagedtlnote { get; set; }
            public decimal matusagevalueidr { get; set; }
            public decimal matusagevalueusd { get; set; }
            public string sn { get; set; }
            public string refno { get; set; }
            public string refno2 { get; set; }
        }

        public class mstperson
        {
            public string cmpcode { get; set; }
            public string personoid { get; set; }
            public string nip { get; set; }
            public string personname { get; set; }
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        public class mstmixing
        {
            public int matoid_mix { get; set; }
            public string matcode_mix { get; set; }
            public string matlongdesc_mix { get; set; }
            public int unitoid_mix { get; set; }
            public string unit_mix { get; set; }
        }

        public class matreqmst
        {
            public int matreqmstoid { get; set; }
            public string matreqno { get; set; }
            public DateTime matreqdate { get; set; }
            public int matreqwhoid { get; set; }
            public string matreqwh { get; set; }
            public string mattype { get; set; }
            public string personoid { get; set; }
            public string matreqmstnote { get; set; }
            public string itemdesc { get; set; }
        }

        private string generateNo(string cmp, DateTime tgl)
        {
            string sCode = "USG-" + tgl.ToString("yyyy") + "." + tgl.ToString("MM") + "-";
            int formatCounter = 6;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matusageno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmatusagemst WHERE cmpcode='" + cmp + "' AND matusageno LIKE '" + sCode + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        private decimal GetStockValue(int sOid, string sRef,  string sType, string cmp)
        {
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            sSql = "SELECT ISNULL(a,0) AS b FROM( SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" + sType + ", 0)) / SUM(ISNULL(stockqty, 0)) AS a FROM QL_stockvalue WHERE cmpcode='" + cmp + "' AND periodacctg IN ('" + sPeriod + "', '" + ClassFunction.GetLastPeriod(sPeriod) + "') AND refoid=" + sOid + " AND refname='" + sRef + "' AND closeflag='') AS tbl";
            var Value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Value;
        }
        public static string GetQueryInitDDLWH(string cmpcode, string action)
        {
            var cmp = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            var res = "SELECT * FROM QL_mstgen WHERE cmpcode='" + cmp + "' AND gengroup='MATERIAL LOCATION' AND genoid>0";
            if (action == "New Data")
                res += " AND activeflag='ACTIVE'";
            res += " ORDER BY gendesc";
            return res;
        }
        private void InitDDL(QL_trnmatusagemst tbl, string action)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            var matusagewhoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(GetQueryInitDDLWH(CompnyCode,action)).ToList(), "genoid", "gendesc", tbl.matusagewhoid);
            ViewBag.matusagewhoid = matusagewhoid;
        }

      

        [HttpPost]
        public ActionResult GetDataDetails(string cmpcode, string mattype, int whoid, int divgroupoid)
        {
            List<matusagedtl> tbl = new List<matusagedtl>();
            string sType = ""; string sRef = ""; string sSql = "";
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (mattype == "Raw")
            {
                sType = "matraw";
                sRef = "RAW MATERIAL";
            }
            else if (mattype == "General")
            {
                sType = "matgen";
                sRef = "GENERAL MATERIAL";
            }
            else if (mattype == "Spare Part")
            {
                sType = "sparepart";
                sRef = "SPARE PART";
            }            
                    

            sSql = "SELECT 0 matusagedtlseq, '"+ sRef + "' matusagereftype," + sType + "oid matusagerefoid, " + sType + "code AS matusagerefcode, " + sType + "longdesc AS matusagereflongdesc, "+sType+"unitoid matusageunitoid, gendesc AS matusageunit, con.qty AS stockqty, con.refno, 0.0 AS matusageqty, '' AS matusagedtlnote,con.serialno sn FROM QL_mst" + sType + " m INNER JOIN QL_mstgen g ON genoid = "+sType+ "unitoid INNER JOIN (SELECT refoid, refname, refno, isnull((select conx.refno2 from QL_conmat conx where conx.conmtroid = (select max(x.conmtroid) from ql_conmat x where x.refoid = con.refoid and x.refname = con.refname and isnull(x.refno2,'')<>'')),'')  refno2, serialno, SUM(qtyin - qtyout) qty FROM QL_conmat con WHERE cmpcode = '" + cmpcode + "' AND refname = '"+ sRef +"' AND mtrwhoid = "+ whoid + " GROUP BY refoid, refname, refno, serialno HAVING sum(qtyin - qtyout) > 0) con ON con.refoid = m." + sType + "oid WHERE m.cmpcode = '" + cmpcode + "' and m.divgroupoid='"+ divgroupoid + "'";
             
            tbl = db.Database.SqlQuery<matusagedtl>(sSql).ToList();
            var no = 0;
            for (var i = 0; i < tbl.Count(); i++)
            {
                no = no + 1; ;
                tbl[i].matusagedtlseq = no;
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<matusagedtl> dtDtl)
        {
            Session["QL_trnmatusagedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            List<matusagedtl> dtl = new List<matusagedtl>();
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            try
            {
                sSql = "SELECT DISTINCT matusagedtlseq, matusagewhoid, matusagereftype, matusagerefoid, (CASE matusagereftype WHEN 'RAW MATERIAL' THEN (SELECT matrawcode FROM QL_mstmatraw WHERE matrawoid=matusagerefoid) WHEN 'GENERAL MATERIAL' THEN (SELECT matgencode FROM QL_mstmatgen WHERE matgenoid=matusagerefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart WHERE sparepartoid=matusagerefoid) ELSE '' END) AS matusagerefcode, matusagedtlres3 refno2, (CASE matusagereftype WHEN 'RAW MATERIAL' THEN (SELECT matrawlongdesc FROM QL_mstmatraw WHERE matrawoid=matusagerefoid) WHEN 'GENERAL MATERIAL' THEN (SELECT matgenlongdesc FROM QL_mstmatgen WHERE matgenoid=matusagerefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart WHERE sparepartoid=matusagerefoid) ELSE '' END) AS matusagereflongdesc, matusageqty, matusageunitoid, g2.gendesc AS matusageunit, matusagedtlnote, 0.0000 AS matusagevalueidr, (select sum(qtyin-qtyout) from ql_conmat con where con.refoid=matusagerefoid and con.refname=mud.matusagereftype and con.refno=mud.refno) stockqty,0.0000 AS matusagevalueusd, mud.refno, mud.sn FROM QL_trnmatusagedtl mud INNER JOIN QL_mstgen g2 ON g2.genoid=mud.matusageunitoid WHERE mud.cmpcode='" + CompnyCode + "' AND matusagemstoid=" + id + "";
                dtl = db.Database.SqlQuery<matusagedtl>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
                else
                {
                    var no = 0;
                    for (int i = 0; i < dtl.Count; i++)
                    {
                        no = no + 1; ;
                        dtl[i].matusagedtlseq = no;
                    }
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private void FillAdditionalField(QL_trnmatusagemst tbl)
        {

        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("dd/MM/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom2 = modfil.filterperiodfrom.ToString("dd/MM/yyyy");
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("dd/MM/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto2 = modfil.filterperiodto.ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }
        // GET: MatUsageNonKIK
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";

            sSql = "SELECT reqm.cmpcode,div.divname, reqm.matusagemstoid, reqm.matusageno, reqm.matusagedate, reqm.matusagemststatus, reqm.matusagemstnote, reqm.createuser FROM QL_trnmatusagemst reqm  INNER JOIN QL_mstdivision div ON div.cmpcode=reqm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "reqm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "reqm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND matusagemststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND matusagemststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND matusagemststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND matusagedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND matusagedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND matusagemststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }
            }
            else
            {
                sSql += " AND 1=0";
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND reqm.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " ORDER BY reqm.matusagedate DESC, reqm.matusagemstoid DESC";

            List<matusagemst> dt = db.Database.SqlQuery<matusagemst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnmatusagemst", true);
            ViewBag.isCollapse = isCollapse;
            return View(dt);
        }

        // GET: MatUsageNonKIK/Form/5/11
        public ActionResult Form(int? id, string cmp, int? idmatreq)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trnmatusagemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnmatusagemst();
                tbl.cmpcode = CompnyCode;
                tbl.matusagemstoid = ClassFunction.GenerateID("QL_trnmatusagemst");
                tbl.matusagedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.matusagemststatus = "In Process";
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
                Session["QL_trnmatusagedtl"] = null;
            }
            else if (id == 0 && cmp != null && idmatreq != null)
            {
                tbl = new QL_trnmatusagemst();
                tbl.cmpcode = CompnyCode;
                tbl.matusagemstoid = ClassFunction.GenerateID("QL_trnmatusagemst");
                tbl.matusagedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.matusagemststatus = "In Process";
              
                tbl.divgroupoid = int.Parse(Session["DivGroup"].ToString());
                ViewBag.matreqno = db.Database.SqlQuery<string>("SELECT matreqno FROM QL_trnmatreqmst WHERE matreqmstoid=" + idmatreq.Value + "").FirstOrDefault();
                ViewBag.matreqdate = db.Database.SqlQuery<string>("SELECT CONVERT(varchar(12), matreqdate , 101) FROM QL_trnmatreqmst WHERE matreqmstoid=" + idmatreq.Value + "").FirstOrDefault();
                ViewBag.matreqmstoid = idmatreq.Value;
                ViewBag.matreqwhoid = db.Database.SqlQuery<int>("SELECT matreqwhoid FROM QL_trnmatreqmst WHERE matreqmstoid=" + idmatreq.Value + "").FirstOrDefault();
            }
            else
            {
                action = "Update Data";
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl = db.QL_trnmatusagemst.Find(cmp, id);
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl,action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MatUsageNonKIK/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmatusagemst tbl, List<matusagedtl> dtDtl,string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            var servertime = ClassFunction.GetServerTime();
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            int iAcctgoidRM = 0; int iAcctgoidGM = 0; int iAcctgoidSP = 0; int iUsageacctgoid = 0;
            decimal dAmtRM = 0; decimal dAmtRMUSD = 0; decimal dAmtGM = 0; decimal dAmtGMUSD = 0;
            decimal dAmtSP = 0; decimal dAmtSPUSD = 0;
            decimal dTotalAmt = 0; decimal dTotalAmtUSD = 0;
            var spr = "";

            if (tbl.matusagemststatus == "Post")
            {
                tbl.matusageno = generateNo(tbl.cmpcode, ClassFunction.GetServerTime());
            }
            else
            {
                if (tbl.matusageno == null)
                    tbl.matusageno = "";
            }


            if (dtDtl == null)
                msg+= "Please fill detail data!<BR>";
            else if (dtDtl.Count <= 0)
                msg += "Please fill detail data!<BR>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                       
                        if (dtDtl[i].matusageqty <= 0)
                        {
                            msg += "QTY Pemakaian harus lebih dari 0!<BR>";
                        }
                        else
                        {
                           
                            
                        }
                        
                        dtDtl[i].matusagevalueidr = 0;
                        dtDtl[i].matusagevalueusd = 0;                        

                        if (tbl.matusagemststatus == "Post")
                        {
                            string sRef = dtDtl[i].matusagereftype;
                          
                            if (!ClassFunction.IsStockAvailableWithBatch(tbl.cmpcode, ClassFunction.GetServerTime(), dtDtl[i].matusagerefoid, tbl.matusagewhoid, dtDtl[i].matusageqty, sRef, dtDtl[i].refno))
                            {
                                if (dtDtl[i].refno != "")
                                {
                                    spr = " - ";
                                }

                                msg += "Qty Item " + dtDtl[i].matusagereflongdesc + spr + dtDtl[i].refno + " QTy Pemakaian tidak boleh lebih dari Stock Qty!";
                            }

                            decimal dValue = ClassFunction.GetStockValue(CompnyCode, sRef, dtDtl[i].matusagerefoid, dtDtl[i].refno);

                            dtDtl[i].matusagevalueidr = dValue;
                            dTotalAmt += dtDtl[i].matusagevalueidr;
                        }
                    }
                }
            }
          
            if (tbl.matusagemststatus == "Post")
            {
                var divisi = db.Database.SqlQuery<string>("select gendesc from ql_mstgen where genoid=" + tbl.divgroupoid).FirstOrDefault();
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_RM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_GM") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_USAGE_NON_KIK", tbl.cmpcode))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_USAGE_NON_KIK") + "<BR>";
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_SP", tbl.cmpcode, tbl.divgroupoid.Value))
                {
                    msg += ClassFunction.GetInterfaceWarning("VAR_STOCK_SP") + "<BR>";
                }
                iAcctgoidRM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RM", tbl.cmpcode, tbl.divgroupoid.Value));
                iAcctgoidGM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_GM", tbl.cmpcode, tbl.divgroupoid.Value));
                iAcctgoidSP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_SP", tbl.cmpcode, tbl.divgroupoid.Value));
     
                iUsageacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_USAGE_NON_KIK", tbl.cmpcode));
                var a = dtDtl.GroupBy(x => x.matusagereftype).Select(x => new { Nama = x.Key, Amt = x.Sum(y => (y.matusagevalueidr)), AmtUSD = x.Sum(y => (y.matusagevalueusd)) }).ToList();
                for (int i = 0; i < a.Count(); i++)
                {
                    if (a[i].Nama == "RAW MATERIAL")
                    {
                        dAmtRM = a[i].Amt; dAmtRMUSD = a[i].AmtUSD;
                    }
                    else if (a[i].Nama == "GENERAL MATERIAL")
                    {
                        dAmtGM = a[i].Amt; dAmtGMUSD = a[i].AmtUSD;
                    }
                    else if (a[i].Nama == "SPARE PART")
                    {
                        dAmtSP = a[i].Amt; dAmtSPUSD = a[i].AmtUSD;
                    }
                }
            }
            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmatusagemst");
                var dtloid = ClassFunction.GenerateID("QL_trnmatusagedtl");
                var conmtroid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
               
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnmatusagemst.Find(tbl.cmpcode, tbl.matusagemstoid) != null)
                                tbl.matusagemstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.matusagedate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnmatusagemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.matusagemstoid + " WHERE tablename='QL_trnmatusagemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND matreqdtloid IN (SELECT matreqdtloid FROM QL_trnmatusagedtl WHERE cmpcode='" + tbl.cmpcode + "' AND matusagemstoid=" + tbl.matusagemstoid + ") AND matreqmstoid IN (SELECT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" + tbl.cmpcode + "' AND matusagemstoid=" + tbl.matusagemstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND matreqmstoid IN (SELECT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" + tbl.cmpcode + "' AND matusagemstoid=" + tbl.matusagemstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmatusagedtl.Where(a => a.matusagemstoid == tbl.matusagemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmatusagedtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmatusagedtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnmatusagedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.matusagedtloid = dtloid++;
                            tbldtl.matusagemstoid = tbl.matusagemstoid;
                            tbldtl.matusagedtlseq = dtDtl[i].matusagedtlseq;
                            tbldtl.matusagewhoid = dtDtl[i].matusagewhoid;
                            tbldtl.matusagereftype = dtDtl[i].matusagereftype;
                            tbldtl.matusagerefoid = dtDtl[i].matusagerefoid;
                            tbldtl.matusageqty = dtDtl[i].matusageqty;
                            tbldtl.matusageunitoid = dtDtl[i].matusageunitoid;
                            tbldtl.matusagewhoid = tbl.matusagewhoid;
                            tbldtl.matusagedtlstatus = "";
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.matusagedtlnote = dtDtl[i].matusagedtlnote;
                            tbldtl.matusagevalueidr = dtDtl[i].matusagevalueidr;
                            tbldtl.matusagevalueusd = dtDtl[i].matusagevalueusd;
                            tbldtl.refno = dtDtl[i].refno ?? "";
                            tbldtl.sn = dtDtl[i].sn;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.matusagedtlres3 = dtDtl[i].refno2;
                            db.QL_trnmatusagedtl.Add(tbldtl);
                            db.SaveChanges();

                            var a = dtDtl.Where(x => x.matusagerefoid == dtDtl[i].matusagerefoid).GroupBy(x => x.matusagerefoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.matusageqty) }).ToList();
                            for (int j = 0; j < a.Count(); j++)
                            {
                                if (a[j].Qty >= dtDtl[i].matreqqty)
                                {
                                    sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND matreqmstoid=" + dtDtl[i].matreqmstoid + " AND matreqdtloid=" + dtDtl[i].matreqdtloid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND matreqmstoid=" + dtDtl[i].matreqmstoid + " AND (SELECT COUNT(*) FROM QL_trnmatreqdtl WHERE cmpcode='" + tbl.cmpcode + "' AND matreqmstoid=" + dtDtl[i].matreqmstoid + " AND matreqdtloid<>" + dtDtl[i].matreqdtloid + " AND matreqdtlstatus='')=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.matusagemststatus == "Post")
                            {
                                string sRef = dtDtl[i].matusagereftype;
                                if (dtDtl[i].matusagereftype == "Raw")
                                {
                                    sRef = "RAW MATERIAL";
                                }
                                else if (dtDtl[i].matusagereftype == "General")
                                {
                                    sRef = "GENERAL MATERIAL";
                                }
                                else if (dtDtl[i].matusagereftype == "Spare Part")
                                {
                                    sRef = "SPARE PART";
                                }

                                // Insert QL_conmat
                                db.QL_conmat.Add(ClassFunction.InsertConMatWithBatch(tbl.cmpcode, conmtroid++, "MU", "QL_trnmatusagedtl", tbl.matusagemstoid, dtDtl[i].matusagerefoid, sRef, tbl.matusagewhoid, dtDtl[i].matusageqty * -1, sRef + " Usage Non PP", tbl.matusageno, Session["UserID"].ToString(), dtDtl[i].refno, dtDtl[i].matusagevalueidr, 0, 0, null, tbldtl.matusagedtloid, tbl.divgroupoid ?? 0, dtDtl[i].sn));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmatusagedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.matusagemststatus == "Post")
                        {
                            sSql = "UPDATE QL_mstoid SET lastoid=" + (conmtroid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, servertime, sPeriod, "Mat. Usage Non PP|No. " + tbl.matusageno, "Post", servertime, tbl.upduser, servertime, tbl.upduser, servertime, 1, 1, 1, 1, 0, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iUsageacctgoid, "D", dTotalAmt, tbl.matusageno, "Mat. Usage Non PP|No. " + tbl.matusageno, "Post", tbl.upduser, servertime, dTotalAmt, dTotalAmtUSD, "QL_trnmatusagemst " + tbl.matusagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            if (dAmtRM != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidRM, "C", dAmtRM, tbl.matusageno, "Mat. Usage Non PP|No. " + tbl.matusageno, "Post", tbl.upduser, servertime, dAmtRM, dAmtRMUSD, "QL_trnmatusagemst " + tbl.matusagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (dAmtGM != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidGM, "C", dAmtGM, tbl.matusageno, "Mat. Usage Non PP|No. " + tbl.matusageno, "Post", tbl.upduser, servertime, dAmtGM, dAmtGMUSD, "QL_trnmatusagemst " + tbl.matusagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }
                            if (dAmtSP != 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid++, iSeq++, glmstoid, iAcctgoidSP, "C", dAmtSP, tbl.matusageno, "Mat. Usage Non PP|No. " + tbl.matusageno, "Post", tbl.upduser, servertime, dAmtSP, dAmtSPUSD, "QL_trnmatusagemst " + tbl.matusagemstoid.ToString(), null, null, null, 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();

                        hdrid = tbl.matusagemststatus.ToString();
                        sReturnNo = "draft " + tbl.matusagemstoid.ToString();
                        if (tbl.matusagemststatus == "In Approval")
                        {
                            sReturnState = "terkirim";
                        }
                        else
                        {
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: MatUsageNonKIK/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatusagemst tbl = db.QL_trnmatusagemst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                     

                        var trndtl = db.QL_trnmatusagedtl.Where(a => a.matusagemstoid == id && a.cmpcode == cmp);
                        db.QL_trnmatusagedtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmatusagemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmatusagemst.Find(cmp, id);
            if (tbl == null)
                return null;

            var rptname = "rptMatUsage";
            if (printtype == "A5")
            {
                rptname += "A5";
            }
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT DISTINCT mum.matusagemstoid, CONVERT(VARCHAR(20), mum.matusagemstoid) AS [Draft No.], mum.matusageno AS [Usage No.], mum.matusagedate AS [Usage Date], mum.matusagemststatus AS [Status], mum.matusagemstnote AS [Header Note], mud.matusagedtloid, mud.matusagedtlseq AS [No.], g1.gendesc AS [Warehouse], mud.matusagereftype AS [Type], mud.matusagerefoid, (CASE mud.matusagereftype WHEN 'Raw MATERIAL' THEN (SELECT m.matrawcode FROM QL_mstmatraw m WHERE m.matrawoid=mud.matusagerefoid) WHEN 'General MATERIAL' THEN (SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid=mud.matusagerefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid=mud.matusagerefoid) ELSE '' END) AS [Code], (CASE mud.matusagereftype WHEN 'Raw MATERIAL' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m WHERE m.matrawoid=mud.matusagerefoid) WHEN 'General MATERIAL' THEN (SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=mud.matusagerefoid) WHEN 'Spare Part' THEN (SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid=mud.matusagerefoid) ELSE '' END) AS [Description], mud.matusageqty AS [Qty], g2.gendesc AS [Unit], mud.matusagedtlnote AS [Detail Note], di.divname AS [Business Unit], '' AS [Detail Location], '' [Person Name] , '' [NIP], mud.refno AS [No. Batch] FROM QL_trnmatusagemst mum INNER JOIN QL_trnmatusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.matusagemstoid=mum.matusagemstoid INNER JOIN QL_mstgen g1 ON g1.genoid=mud.matusagewhoid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.matusageunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode WHERE mud.cmpcode='" + cmp + "' AND mud.matusagemstoid=" + id + " ORDER BY mum.matusagemstoid, mud.matusagedtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            //report.SetParameterValue("diPrint", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "MatUsagePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}