﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class DOFinishGoodController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class doitemmst
        {
            public string cmpcode { get; set; }
            public int doitemmstoid { get; set; }
            public string doitemno { get; set; }
            public string divgroup { get; set; }
            public DateTime doitemdate { get; set; }
            public string doitemmststatus { get; set; }
            public string custname { get; set; }
            public string doitemcustdo { get; set; }
            public string doitemmstnote { get; set; }
            public string divname { get; set; }
        }

        public class doitemdtl
        {
            public int doitemdtlseq { get; set; }
            public string soitemno { get; set; }
            public int soitemmstoid { get; set; }
            public int soitemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal soitemqty { get; set; }
            public decimal doitemqty { get; set; }
            public int doitemunitoid { get; set; }
            public string doitemunit { get; set; }
            public decimal doitemprice { get; set; }
            public decimal doitemdtlamt { get; set; }
            public string doitemdtldisctype { get; set; }
            public decimal doitemdtldiscvalue { get; set; }
            public decimal doitemdtldiscamt { get; set; }
            public decimal doitemdtlnetto { get; set; }
            public int precostcurr { get; set; }
            public decimal precostprice { get; set; }
            public string precostupduser { get; set; }
            public string precostupdtime { get; set; }
            public string doitemdtlnote { get; set; } 
            public string itemnote { get; set; }
            public string doitemextcode { get; set; } 
            public string doitemextname { get; set; }
            public decimal doitemwoodvol { get; set; }
            public int itemdtloid { get; set; } = 0;
            public string doitemdtl2code { get; set; }
            public string doitemdtl2name { get; set; }
            public decimal doitemdtl2gweight { get; set; }
            public decimal doitemdtl2nweight { get; set; }
            public decimal doitemdtl2qty { get; set; }
            public int doitemdtl2unitoid { get; set; }
            public decimal doitemdtl2width { get; set; }
            public decimal doitemdtl2depth { get; set; }
            public decimal doitemdtl2height { get; set; }
        }

        public class soitemcust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public string soitemtype { get; set; }
            public string soitemtaxtype { get; set; }
 
        }

        public class custshipto
        {
            public int custdtloid { get; set; }
            public string custdtlcode { get; set; }
            public string custdtlname { get; set; }
            public string custdtladdr { get; set; }
        }

        public class mstitemdtl
        {
            public int itemdtloid { get; set; }
            public string itemdtlcode { get; set; }
            public string itemdtlname { get; set; }
            public decimal itemdtlgweight { get; set; }
            public decimal itemdtlnweight { get; set; }
            public decimal itemdtlqty { get; set; }
            public decimal itemdtlwidth { get; set; }
            public decimal itemdtldepth { get; set; }
            public decimal itemdtlheight { get; set; }
            public int itemdtlunitoid { get; set; }
        }

        private void InitDDL(QL_trndoitemmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            sSql = "SELECT * FROM QL_mstgen WHERE gengroup = 'CONTAINER TYPE' AND activeflag = 'ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY gendesc";
            var doitemdelivtypeoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.doitemdelivtypeoid);
            ViewBag.doitemdelivtypeoid = doitemdelivtypeoid;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int oid, string intax, int doid)
        {
            List<doitemdtl> tbl = new List<doitemdtl>();
            double dDiv = 1;
            if (intax == "False") { dDiv = 1.1; }
            sSql = "SELECT soitemdtloid, som.soitemmstoid, som.soitemno, CAST(ROW_NUMBER() OVER(ORDER BY soitemdtloid) AS INT) soitemdtlseq, sod.itemoid, itemcode, itemlongdesc, (soitemqty - ISNULL((SELECT SUM(doitemqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemmstoid=dod.doitemmstoid), 0)=0 THEN doitemqty ELSE (ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dod.doitemdtloid), 0) -ISNULL((SELECT SUM(sretitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnsretitemdtl sretd ON sd.shipmentitemdtloid=sretd.shipmentitemdtloid INNER JOIN QL_trnsretitemmst sretm ON sretd.sretitemmstoid=sretm.sretitemmstoid  WHERE sd.cmpcode=dod.cmpcode AND sretitemmststatus<>'Rejected' AND sd.doitemdtloid=dod.doitemdtloid), 0)) END) AS doitemqty FROM QL_trndoitemdtl dod INNER JOIN QL_trndoitemmst dom ON dom.doitemmstoid=dod.doitemmstoid and dod.cmpcode=dom.cmpcode WHERE dod.cmpcode=sod.cmpcode AND dod.soitemdtloid=sod.soitemdtloid AND doitemmststatus <>'Cancel' AND dod.doitemmstoid<>" + doid +") AS tbltmp), 0.0)) AS soitemqty, 0.0 AS doitemqty, soitemunitoid doitemunitoid, gendesc AS doitemunit, soitemdtlnote AS doitemdtlnote, itemnote, (soitemprice * " + dDiv + ") AS doitemprice, soitemdtldisctype doitemdtldisctype, ((CASE soitemdtldisctype WHEN 'P' THEN soitemdtldiscvalue ELSE (soitemdtldiscvalue * " + dDiv + ") END)) doitemdtldiscvalue, soitemmstdisctype AS doitemmstdisctype, soitemmstdiscvalue AS doitemmstdiscvalue, (soitemtotalamt - soitemtotaldiscdtl) AS doitemtotalamt, 1.0 AS precostprice, ISNULL((SELECT pre.upduser FROM QL_mstprecost pre WHERE pre.cmpcode=sod.cmpcode AND pre.itemoid=sod.itemoid),'') AS precostupduser, '01/01/1900' AS precostupdtime, ISNULL(itemnote,'') doitemextcode, '' doitemextname, 0.0 doitemwoodvol FROM QL_trnsoitemdtl sod INNER JOIN QL_trnsoitemmst som ON som.cmpcode=sod.cmpcode AND som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON genoid=soitemunitoid WHERE sod.cmpcode='" + cmp + "' AND sod.soitemmstoid=" + oid + " and soitemdtlstatus='' ORDER BY soitemdtlseq";

            tbl = db.Database.SqlQuery<doitemdtl>(sSql).ToList();
            if (tbl != null)
                if (tbl.Count > 0)
                    for(var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].doitemqty = tbl[i].soitemqty;
                    }
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<doitemdtl> dtDtl)
        {
            Session["QL_trndoitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trndoitemdtl"] == null)
            {
                Session["QL_trndoitemdtl"] = new List<doitemdtl>();
            }

            List<doitemdtl> dataDtl = (List<doitemdtl>)Session["QL_trndoitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndoitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();
            ViewBag.currcode = db.Database.SqlQuery<string>("SELECT currcode FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND curroid='" + tbl.curroid + "'").FirstOrDefault();
            ViewBag.customershipto = db.Database.SqlQuery<string>("SELECT custdtlname FROM QL_mstcustdtl WHERE cmpcode='" + CompnyCode + "' AND custdtloid='" + tbl.custdtloid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp, int divgroupoid)
        {
            List<soitemcust> tbl = new List<soitemcust>();

            sSql = "SELECT DISTINCT c.custoid, custcode, custname, som.curroid, currcode, soitemtype, soitemtaxtype FROM QL_mstcust c INNER JOIN QL_trnsoitemmst som ON som.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=som.curroid WHERE som.cmpcode='" + cmp + "' AND soitemmststatus='Approved' AND c.divgroupoid="+divgroupoid+" AND isnull(som.sssflag,0)=0 ORDER BY custname";
            tbl = db.Database.SqlQuery<soitemcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipTo(int custoid)
        {
            List<custshipto> tbl = new List<custshipto>();

            sSql = "SELECT * FROM (SELECT custdtloid, custdtlcode, custdtlname, (custdtladdr + ' ' + gendesc) custdtladdr FROM QL_mstcustdtl c INNER JOIN QL_mstgen g ON genoid=custdtlcityoid WHERE c.cmpcode='" + CompnyCode + "' AND custoid=" + custoid + " AND c.activeflag='ACTIVE') AS tblShip ORDER BY custdtlcode";
            tbl = db.Database.SqlQuery<custshipto>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSOData(string cmp, int custoid, int curroid, string doitemtype, string doitemtaxtype, int divgroupoid)
        {
            List<QL_trnsoitemmst> tbl = new List<QL_trnsoitemmst>();

            sSql = "SELECT * FROM QL_trnsoitemmst som WHERE som.cmpcode='" + cmp + "' AND soitemmststatus='Approved' AND soitemtype='" + doitemtype + "' AND soitemtaxtype='" + doitemtaxtype + "' AND custoid=" + custoid + " AND som.divgroupoid=" + divgroupoid + " AND curroid=" + curroid + " ORDER BY soitemmstoid";
            tbl = db.Database.SqlQuery<QL_trnsoitemmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: doitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "dom", divgroupoid, "divgroupoid");
            sSql = "SELECT dom.cmpcode, (select gendesc from ql_mstgen where genoid=dom.divgroupoid) divgroup, doitemmstoid, doitemno, doitemdate, custname, doitemmststatus, doitemmstnote, doitemcustdo, divname, 'False' AS checkvalue FROM QL_trndoitemmst dom INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=dom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "dom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "dom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND doitemmststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND doitemmststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND doitemmststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND doitemdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND doitemdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND doitemmststatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND dom.createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY doitemdate DESC, dom.doitemmstoid DESC";

            List<doitemmst> dt = db.Database.SqlQuery<doitemmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trndoitemmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: doitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            var cmp = CompnyCode;
            QL_trndoitemmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trndoitemmst();
                tbl.doitemmstoid = ClassFunction.GenerateID("QL_trndoitemmst");
                tbl.doitemdate = ClassFunction.GetServerTime();
                tbl.doitemreqdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.doitemmststatus = "In Process";

                Session["QL_trndoitemdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trndoitemmst.Find(cmp, id);
                double dDiv = 1.1;
                if (tbl.isexcludetax)
                {
                    dDiv = 1;
                }
                            
                sSql = "SELECT doitemdtlseq, dod.soitemmstoid, soitemno, dod.soitemdtloid, dod.itemoid, itemcode, itemlongdesc, itemnote, (soitemqty - ISNULL((SELECT SUM(doitemqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemmstoid=dodxx.doitemmstoid), 0)=0 THEN doitemqty ELSE (ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dodxx.doitemdtloid), 0)-ISNULL((SELECT SUM(sretitemqty) FROM QL_trnshipmentitemdtl sd2 INNER JOIN QL_trnsretitemdtl sretd ON sd2.shipmentitemdtloid=sretd.shipmentitemdtloid INNER JOIN QL_trnsretitemmst sretm ON sretd.sretitemmstoid=sretm.sretitemmstoid  WHERE sd2.cmpcode=dodxx.cmpcode AND sretitemmststatus<>'Rejected' AND sd2.doitemdtloid=dodxx.doitemdtloid), 0)) END) AS doitemqty FROM QL_trndoitemdtl dodxx INNER JOIN QL_trndoitemmst domxx ON domxx.cmpcode=dodxx.cmpcode AND domxx.doitemmstoid=dodxx.doitemmstoid WHERE dodxx.cmpcode=sod.cmpcode AND dodxx.soitemdtloid=sod.soitemdtloid AND dodxx.doitemmstoid<>" + tbl.doitemmstoid + " AND doitemmststatus <>'Cancel') AS tbltmp), 0.0)) AS soitemqty, dod.doitemqty, dod.doitemunitoid, g2.gendesc AS doitemunit, dod.doitemdtlnote, (doitemprice * " + dDiv + ") doitemprice, (doitemdtlamt * " + dDiv + ") doitemdtlamt, '' doitemextcode,'' doitemextname, 0.0 doitemwoodvol, doitemdtldisctype, (CASE doitemdtldisctype WHEN 'P' THEN doitemdtldiscvalue ELSE doitemdtldiscvalue * " + dDiv + " END) doitemdtldiscvalue, (doitemdtldiscamt * " + dDiv + ") doitemdtldiscamt, (doitemdtlnetto * " + dDiv + ") doitemdtlnetto, 0 precostcurr,0.0 precostprice,'' precostupduser, dod.upduser AS precostupdtime FROM QL_trndoitemdtl dod INNER JOIN QL_trnsoitemmst som ON som.cmpcode=dod.cmpcode AND som.soitemmstoid=dod.soitemmstoid INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=dod.cmpcode AND sod.soitemdtloid=dod.soitemdtloid INNER JOIN QL_mstitem m ON m.itemoid=dod.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=doitemunitoid WHERE doitemmstoid=" + id + " AND dod.cmpcode='" + cmp + "' ORDER BY doitemdtlseq";
               
                Session["QL_trndoitemdtl"] = db.Database.SqlQuery<doitemdtl>(sSql).ToList();
                List<doitemdtl> tt = new List<doitemdtl>();
                tt = db.Database.SqlQuery<doitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: doitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndoitemmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            if (tbl.doitemno == null)
                tbl.doitemno = "";

            List<doitemdtl> dtDtl = (List<doitemdtl>)Session["QL_trndoitemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            if (dtDtl != null)
            {
                if (dtDtl.Count() > 0)
                {
                    DataTable TblDtl = ClassFunction.ToDataTable(dtDtl);
                   
                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].doitemqty <= 0)
                        {
                            ModelState.AddModelError("", dtDtl[i].itemcode + " QUANTITY must be more than 0!");
                        }else
                        {
                            if (dtDtl[i].doitemqty > dtDtl[i].soitemqty)
                                ModelState.AddModelError("", dtDtl[i].itemcode + " QUANTITY must be less than SO QTY!");
                        }
                        //if (dtDtl[i].doitemprice <= 0)
                        //    ModelState.AddModelError("", dtDtl[i].itemcode + " Please fill PRICE field!");
                        if (dtDtl[i].doitemdtldisctype == "P")
                        {
                            if (dtDtl[i].doitemdtldiscvalue < 0 || dtDtl[i].doitemdtldiscvalue > 100)
                                ModelState.AddModelError("", dtDtl[i].itemcode + " Percentage of DETAIL DISC must be between 0 and 100!");
                        }else
                        {
                            if (dtDtl[i].doitemdtldiscvalue < 0 || dtDtl[i].doitemdtldiscvalue > dtDtl[i].doitemdtldiscamt)
                                ModelState.AddModelError("", dtDtl[i].itemcode + " Amount of DETAIL DISC must be between 0 and " + string.Format("{0:#,0.00}", dtDtl[i].doitemdtldiscamt) + " (DETAIL AMOUNT)!");
                        }
                            
                        sSql = "SELECT (soitemqty - ISNULL((SELECT SUM(doitemqty) FROM (SELECT (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemmstoid=dodxx.doitemmstoid), 0)=0 THEN doitemqty ELSE (ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dodxx.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.doitemdtloid=dodxx.doitemdtloid), 0)-ISNULL((SELECT SUM(sretitemqty) FROM QL_trnshipmentitemdtl sd2 INNER JOIN QL_trnsretitemdtl sretd ON sd2.shipmentitemdtloid=sretd.shipmentitemdtloid INNER JOIN QL_trnsretitemmst sretm ON sretd.sretitemmstoid=sretm.sretitemmstoid  WHERE sd2.cmpcode=dodxx.cmpcode AND sretitemmststatus<>'Rejected' AND sd2.doitemdtloid=dodxx.doitemdtloid), 0)) END) AS doitemqty FROM QL_trndoitemdtl dodxx INNER JOIN QL_trndoitemmst domxx ON domxx.doitemmstoid=dodxx.doitemmstoid WHERE dodxx.cmpcode=sod.cmpcode AND dodxx.soitemdtloid=sod.soitemdtloid AND dodxx.doitemmstoid<>" + tbl.doitemmstoid + " AND doitemmststatus <>'Cancel') AS tbltmp), 0.0)) AS soitemqty FROM QL_trnsoitemdtl sod WHERE sod.cmpcode='" + tbl.cmpcode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                        decimal dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].soitemqty)
                        {
                            dtDtl[i].soitemqty = dQty;
                            if (dQty < dtDtl[i].doitemqty)
                                ModelState.AddModelError("", "SO Qty for detail no. " + dtDtl[i].doitemdtlseq + "  has been updated by another user. Please check that every DO Qty must be less than SO Qty!");
                        }

                        if (tbl.doitemmststatus == "Post")
                        {
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                            sSql = "SELECT ISNULL(saldoakhir,0) FROM (SELECT SUM(qtyin-qtyout) saldoakhir FROM QL_conmat WHERE cmpcode='" + tbl.cmpcode + "' AND refname='FINISH GOOD' AND refoid=" + dtDtl[i].itemoid + ") dt";
                            decimal dStockQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            sSql = "SELECT ISNULL(bookingqty, 0) FROM (SELECT SUM(bookingqty) bookingqty FROM ( ";
                            sSql += "SELECT SUM(doitemqty) bookingqty FROM QL_trndoitemmst dom INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=dom.cmpcode AND dod.doitemmstoid=dom.doitemmstoid WHERE dom.cmpcode='" + tbl.cmpcode + "' AND doitemmststatus='Post' AND itemoid=" + dtDtl[i].itemoid + " AND dom.updtime<CAST('" + ClassFunction.GetServerTime() + "' AS DATETIME) AND dom.doitemmstoid<>" + tbl.doitemmstoid;
                            sSql += "UNION ALL ";
                            sSql += "SELECT SUM(doitemqty) bookingqty FROM QL_trndoitemmst dom INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=dom.cmpcode AND dod.doitemmstoid=dom.doitemmstoid WHERE dom.cmpcode='" + tbl.cmpcode + "' AND doitemmststatus='Closed' AND itemoid=" + dtDtl[i].itemoid + " AND dom.updtime<CAST('" + ClassFunction.GetServerTime() + "' AS DATETIME) AND dom.doitemmstoid<>" + tbl.doitemmstoid + " AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shd.cmpcode=shm.cmpcode AND shd.shipmentitemmstoid=shm.shipmentitemmstoid WHERE shipmentitemmststatus NOT IN ('Approved','Closed','Rejected'))";
                            sSql += ") As Tbl) tbl2";
                            decimal dBookingQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            decimal dQtyDO = Convert.ToDecimal(TblDtl.Compute("SUM(doitemqty)", "itemoid=" + dtDtl[i].itemoid));
                            if ((dStockQty - dBookingQty) < dQtyDO)
                                ModelState.AddModelError("", "Total DO Qty for code " + dtDtl[i].itemcode + " must be less than " + string.Format("{0:#,0.00}", (dStockQty - dBookingQty)) + " {Stock Qty (" + string.Format("{0:#,0.00}", dStockQty) + ") - Booking Qty (" + string.Format("{0:#,0.00}", dBookingQty) + ")}!");
                        }
                    }
                }
            }

            if (!ModelState.IsValid)
                tbl.doitemmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trndoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trndoitemdtl");
                //var dtloid2 = ClassFunction.GenerateID("QL_trndoitemdtl2");
                var servertime = ClassFunction.GetServerTime();
                var divoid = db.Database.SqlQuery<int>("SELECT divoid FROM QL_mstdivision WHERE cmpcode='" + tbl.cmpcode + "'").FirstOrDefault();
                if (tbl.doitemmststatus == "Post")
                {
                    string sNo = "DOFG-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(doitemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndoitemmst WHERE cmpcode='" + tbl.cmpcode + "' AND doitemno LIKE '" + sNo + "%'";
                    tbl.doitemno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), Convert.ToInt32(DefaultFormatCounter));
                }
                
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trndoitemmst.Find(tbl.cmpcode, tbl.doitemmstoid) != null)
                                tbl.doitemmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.doitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndoitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.doitemmstoid + " WHERE tablename='QL_trndoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trndoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid=" + tbl.doitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trndoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid=" + tbl.doitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trndoitemdtl.Where(a => a.doitemmstoid == tbl.doitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trndoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }
                        decimal dDiv = 1M;
                        if (!tbl.isexcludetax)
                        {
                            dDiv = 1.1M;
                        }
                        QL_trndoitemdtl tbldtl;
                       
                        DataView dtView = ClassFunction.ToDataTable(dtDtl).DefaultView;

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trndoitemdtl();
                            decimal dDiscValue = dtDtl[i].doitemdtldiscvalue;
                            if (dtDtl[i].doitemdtldisctype == "A")
                            {
                                dDiscValue = dtDtl[i].doitemdtldiscvalue / dDiv;
                            }
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.doitemdtloid = dtloid++;
                            tbldtl.divgroupoid = tbl.divgroupoid;
                            tbldtl.doitemmstoid = tbl.doitemmstoid;
                            tbldtl.doitemdtlseq = i + 1;
                            tbldtl.soitemmstoid = dtDtl[i].soitemmstoid;
                            tbldtl.soitemdtloid = dtDtl[i].soitemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.doitemqty = dtDtl[i].doitemqty;
                            tbldtl.doitemunitoid = dtDtl[i].doitemunitoid;
                            tbldtl.doitemprice = dtDtl[i].doitemprice / dDiv;
                            tbldtl.doitemdtlamt = dtDtl[i].doitemdtlamt / dDiv;
                            tbldtl.doitemdtldisctype = dtDtl[i].doitemdtldisctype;
                            tbldtl.doitemdtldiscvalue = dDiscValue;
                            tbldtl.doitemdtldiscamt = dtDtl[i].doitemdtldiscamt / dDiv;
                            tbldtl.doitemdtlnetto = dtDtl[i].doitemdtlnetto / dDiv;
                            tbldtl.doitemdtlstatus = "";
                            tbldtl.doitemdtlnote = (dtDtl[i].doitemdtlnote == null ? "" : dtDtl[i].doitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.containerNo = "";
                            tbldtl.foc = "";
                            tbldtl.refname = "";
    
                            db.QL_trndoitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].doitemqty >= dtDtl[i].soitemqty)
                            {
                                sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='COMPLETE', upduser='" + tbl.upduser + "', updtime='" + tbl.updtime + "' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Closed', upduser='" + tbl.upduser + "', updtime='" + tbl.updtime + "' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemmstoid=" + dtDtl[i].soitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnsoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND soitemdtlstatus='' AND soitemmstoid=" + dtDtl[i].soitemmstoid + " AND soitemdtloid<>" + dtDtl[i].soitemdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                           
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trndoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                       

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.doitemmstoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: doitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            QL_trndoitemmst tbl = db.QL_trndoitemmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trndoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid=" + tbl.doitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trndoitemdtl WHERE cmpcode='" + tbl.cmpcode + "' AND doitemmstoid=" + tbl.doitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trndoitemdtl.Where(a => a.doitemmstoid == id && a.cmpcode == cmp);
                        db.QL_trndoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trndoitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, Boolean cbprice)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
               return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trndoitemmst.Find(cmp, id);
            if (tbl == null)
                return null;
            if (!cbprice)
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDOItem_Trn.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDOItem_TrnPrice.rpt"));

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE dom.cmpcode='" + cmp + "' AND dom.doitemmstoid=" + id);
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "DOFinishGoodReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
