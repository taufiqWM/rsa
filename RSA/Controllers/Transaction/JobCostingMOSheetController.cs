﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class JobCostingMOSheetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class womst
        {
            public string cmpcode { get; set; }
            public int womstoid { get; set; }
            public string divgroup { get; set; }
            public DateTime wodate { get; set; }
            public string wono { get; set; }
            public string soitemno { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal spkqty { get; set; }
            public string womststatus { get; set; }
            public string postuser { get; set; }
            public int countprint { get; set; }
        }

        public class sodata
        {
            public int soitemmstoid { get; set; }
            public int custoid { get; set; }
            public int custdtloid { get; set; }
            public string custdtlname { get; set; }
            public string salesoid { get; set; }
            public string soitemno { get; set; }
            public string soitemdate { get; set; }
            public string soitemtype { get; set; }
            public string custname { get; set; }
            public decimal toleransi { get; set; }
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
        }

        private void InitDDL(QL_trnwomst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            //substance
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code != '-' AND cat3oid IN(SELECT cat3oid FROM QL_mstcat3) ORDER BY cat4code";
            var sub1 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub1);
            ViewBag.sub1 = sub1;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code != '-' AND cat3oid IN(SELECT cat3oid FROM QL_mstcat3) ORDER BY cat4code";
            var sub2 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub2);
            ViewBag.sub2 = sub2;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code != '-' AND cat3oid IN(SELECT cat3oid FROM QL_mstcat3) ORDER BY cat4code";
            var sub3 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub3);
            ViewBag.sub3 = sub3;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code != '-' AND cat3oid IN(SELECT cat3oid FROM QL_mstcat3) ORDER BY cat4code";
            var sub4 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub4);
            ViewBag.sub4 = sub4;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' AND cat4code != '-' AND cat3oid IN(SELECT cat3oid FROM QL_mstcat3) ORDER BY cat4code";
            var sub5 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub5);
            ViewBag.sub5 = sub5;
            //subd
            ViewBag.sub1d = db.Database.SqlQuery<string>("select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + tbl.sub1 + "')").FirstOrDefault();
            ViewBag.sub2d = db.Database.SqlQuery<string>("select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + tbl.sub2 + "')").FirstOrDefault();
            ViewBag.sub3d = db.Database.SqlQuery<string>("select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + tbl.sub3 + "')").FirstOrDefault();
            ViewBag.sub4d = db.Database.SqlQuery<string>("select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + tbl.sub4 + "')").FirstOrDefault();
            ViewBag.sub5d = db.Database.SqlQuery<string>("select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + tbl.sub5 + "')").FirstOrDefault();

            //mesin
            //machine
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var machine1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.m1);
            ViewBag.m1 = machine1;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE'  ORDER BY gendesc";
            var machine2 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.m2);
            ViewBag.m2 = machine2;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var machine3 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.m3);
            ViewBag.m3 = machine3;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' ORDER BY gendesc";
            var machine4 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.m4);
            ViewBag.m4 = machine4;
        }
        private void FillAdditionalField(QL_trnwomst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("select custname from ql_mstcust where custoid = '" + tbl.custoid + "'").FirstOrDefault();
            ViewBag.itemlongdesc = db.Database.SqlQuery<string>("select sodtldesc from ql_trnsorawdtl where sorawdtloid = '" + tbl.itemoid + "'").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("select sorawno from ql_trnsorawmst where sorawmstoid = '" + tbl.somstoid + "'").FirstOrDefault();
            ViewBag.custdtlname = db.Database.SqlQuery<string>("select custdtlname from ql_mstcustdtl where custdtloid = '" + tbl.custdtloid + "'").FirstOrDefault();
            ViewBag.war1 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w1 + "'").FirstOrDefault();
            ViewBag.war2 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w2 + "'").FirstOrDefault();
            ViewBag.war3 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w3 + "'").FirstOrDefault();
            ViewBag.war4 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w4 + "'").FirstOrDefault();
            ViewBag.war5 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w5 + "'").FirstOrDefault();
            ViewBag.war6 = db.Database.SqlQuery<string>("select isnull(gendesc,'-') from ql_mstgen where genoid = '" + tbl.w6 + "'").FirstOrDefault();

          
        }

        [HttpPost]
        public ActionResult Getsub1(string sub1)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub1.Trim() + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub2(string sub2)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub2.Trim() + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub3(string sub3)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub3.Trim() + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub4(string sub4)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub4.Trim() + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub5(string sub5)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub5.Trim() + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }

        [HttpPost]
        public ActionResult getw1(int itemoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5, decimal outcore, decimal lbahan, decimal spkqty, string fluteoid)
        {
            if (outcore == 0)
            {
                outcore = 1;
            }
            var litem = db.QL_trnsorawdtl.FirstOrDefault(x => x.sorawdtloid == itemoid);

            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid=(select genoid from ql_mstgen where gendesc='" + fluteoid + "')";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid=(select genoid from ql_mstgen where gendesc='" + fluteoid + "')";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal luas = (litem.Psht.Value * litem.Lsht.Value) / 1000000;
            decimal berat = (luas * (g1 / 1000)) + ((luas * (g2 / 1000)) * tuf) + (luas * (g3 / 1000)) + ((luas * (g4 / 1000)) * tuf2) + (luas * (g5 / 1000));
            decimal trim = lbahan - (outcore * litem.Lsht.Value);
            decimal ct = Math.Round(spkqty / outcore);
            decimal RM = ct / 1000 * litem.Psht.Value;
            decimal wsheet = (berat > 0 ? (Math.Ceiling((decimal)berat * 1000) / 1000) : 0);
            decimal wtotal = wsheet * spkqty;

            return Json(new { trim = trim, ct = ct, RM = RM, wsheet = wsheet, wtotal = wtotal });
        }

        [HttpPost]
        public ActionResult GetSOData(string cmp, int divgroupoid)
        {
            List<sodata> tbl = new List<sodata>();

            sSql = "SELECT som.sorawmstoid soitemmstoid, som.sorawno soitemno, isnull(c.toleransikirim,0.0) toleransi, isnull(som.custdtloid,0) custdtloid, isnull(cd.custdtlname,'') custdtlname,  CONVERT(CHAR(10),som.sorawdate,101) soitemdate, som.sorawtype soitemtype, som.custoid,c.custname, som.salesid salesoid FROM QL_trnsorawmst som INNER JOIN QL_trnsorawdtl sod on som.sorawmstoid=sod.sorawmstoid Inner Join QL_mstcust c ON c.custoid=som.custoid left join ql_mstcustdtl cd on cd.custdtloid=som.custdtloid WHERE som.cmpcode='" + CompnyCode + "' AND som.sorawmststatus IN('Post','Approved') and isnull(som.statusspk,'')='' and som.divgroupoid=" + divgroupoid + " ORDER BY som.sorawmstoid DESC";
            tbl = db.Database.SqlQuery<sodata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public class item
        {
            public int itemoid { get; set; }
            public int sodtloid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal soitemqty { get; set; }

        }
        [HttpPost]
        public ActionResult GetItemData(string cmp, int soid)
        {
            List<item> tbl = new List<item>();

            sSql = "SELECT sd.sorawdtloid sodtloid,sorawdtloid itemoid, '' itemcode, sodtldesc itemlongdesc,sd.sorawqty soitemqty from ql_trnsorawdtl sd where sd.cmpcode='" + CompnyCode+"' and sd.sorawmstoid="+soid+ " and isnull(sorawdtlres1,'')=''  ORDER BY sodtldesc";
            tbl = db.Database.SqlQuery<item>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        public class detdata
        {
            public int sodtloid { get; set; }
            public decimal tolamt { get; set; }
            public string ordertype { get; set; }
            public decimal spkqty { get; set; }
            public string boxtype { get; set; }
            public decimal lidah { get; set; }
            public decimal up { get; set; }
            public decimal ul { get; set; }
            public decimal ut { get; set; }
            public string tw { get; set; }
            public string flute { get; set; }
            public decimal outflex { get; set; }
            public decimal trim { get; set; }
            public decimal psheet { get; set; }
            public decimal lsheet { get; set; }
            public decimal ct { get; set; }
            public decimal rm { get; set; }
            public decimal luassheet { get; set; }
            public decimal luastotal { get; set; }
            public decimal wsheet { get; set; }
            public decimal wtotal { get; set; }
            public string joint { get; set; }
            public decimal jmlstictch { get; set; }
            public string noplate { get; set; }
            public string nodc { get; set; }
            public int w1 { get; set; }
            public string war1 { get; set; }
            public int w2 { get; set; }
            public string war2 { get; set; }
            public int w3 { get; set; }
            public string war3 { get; set; }
            public int w4 { get; set; }
            public string war4 { get; set; }
            public int w5 { get; set; }
            public string war5 { get; set; }
            public int w6 { get; set; }
            public string war6 { get; set; }
            public string m1 { get; set; }
            public string mes1 { get; set; }
            public string m2 { get; set; }
            public string mes2 { get; set; }
            public string m3 { get; set; }
            public string mes3 { get; set; }
            public string m4 { get; set; }
            public string mes4 { get; set; }
            public string remark { get; set; }
            public decimal outcore { get; set; }
            public decimal lbahan { get; set; }
            public decimal jmllidah { get; set; }
            public string sub1 { get; set; }
            public string sub2 { get; set; }
            public string sub3 { get; set; }
            public string sub4 { get; set; }
            public string sub5 { get; set; }

        }
        [HttpPost]
        public ActionResult GetDetailData(string cmp, int soid, int sodtloid, int itemoid,decimal toleransi,decimal soqty, decimal lbahan, decimal outcore, decimal jmllidah)
        {
            sSql = "select * from ql_trnsorawdtl where sorawdtloid=" + itemoid;
            List<QL_trnsorawdtl> tblmat = db.Database.SqlQuery<QL_trnsorawdtl>(sSql).ToList();
           
            List<detdata> tbl = new List<detdata>();
            decimal spkqty = toleransi + soqty;
            var ordertype = "BARU";
            var boxtype = "Sheet";

         
            if (outcore == 0)
            {
                outcore = 1;
            }
            decimal lidah = 0;
            decimal up = tblmat[0].Psht ?? 0M;
            decimal ul = tblmat[0].Lsht ?? 0M;
            decimal ut = 0;
            decimal outcore1 = outcore;
            decimal lbahan1 = lbahan;

            var flute = tblmat[0].flute;
            decimal outflex = 0;
            decimal trim = lbahan - (outcore * tblmat[0].Lsht ?? 0M);
            decimal psheet = tblmat[0].Psht ?? 0M;
            decimal lsheet = tblmat[0].Lsht ?? 0M;
            decimal ct = Math.Round(spkqty / outcore);
            decimal RM = ct / 1000 * psheet;
            decimal luassheet = (psheet * lsheet) / 1000000;
            decimal luastotal = luassheet * spkqty;
            decimal wsheet = 0;
            decimal wtotal = wsheet * spkqty;
            var joint = "";
            decimal jmlstitch = 0;
            var noplate = "";
            var nodiecut = "";
            var tw = tblmat[0].tw;
            int w1 =0;
            int w2 =  0;
            int w3 =  0;
            int w4 = 0;
            int w5 =  0;
            int w6 = 0;
            var m1 = "0" ;
            var m2 =  "0";
            var m3 =  "0";
            var m4 =  "0";
            var war1= db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w1 + "").FirstOrDefault();
            var war2 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w2 + "").FirstOrDefault();
            var war3 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w3 + "").FirstOrDefault();
            var war4 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w4 + "").FirstOrDefault();
            var war5 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w5 + "").FirstOrDefault();
            var war6 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + w6 + "").FirstOrDefault();
            var mes1 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + m1 + "").FirstOrDefault();
            var mes2 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + m2 + "").FirstOrDefault();
            var mes3 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + m3 + "").FirstOrDefault();
            var mes4 = db.Database.SqlQuery<string>("select isnull(g.gendesc,'-') from ql_mstgen g where g.genoid=" + m4 + "").FirstOrDefault();
            var remark = (tblmat[0].tsheet == "GOTOR" ? "KOTOR" : tblmat[0].tsheet) + (string.IsNullOrWhiteSpace(tblmat[0].sorawdtlnote) ? "" : ", " + tblmat[0].sorawdtlnote);
            var sub1 = tblmat[0].g1;
            var sub2 = tblmat[0].g2;
            var sub3 = tblmat[0].g3;
            var sub4 = tblmat[0].g4;
            var sub5 = tblmat[0].g5;
            tbl.Add(new detdata { spkqty = spkqty,ordertype=ordertype,boxtype=boxtype,lidah=lidah,up=up,ul=ul,ut=ut, tw=tw, flute=flute, outflex=outflex,trim=trim, ct=ct, rm=RM, luassheet=luassheet, luastotal=luastotal, wsheet=wsheet, wtotal=wtotal, joint=joint, jmlstictch=jmlstitch, psheet=psheet,lsheet=lsheet, noplate=noplate, nodc=nodiecut, w1=w1,w2=w2,w3=w3,w4=w4,w5=w5,w6=w6,m1=m1,m2=m2,m3=m3,m4=m4, war1=war1, war2=war2,war3=war3,war4=war4,war5=war5,war6=war6,mes1=mes1,mes2=mes2,mes3=mes3,mes4=mes4, outcore=outcore1,lbahan=lbahan1, jmllidah=jmllidah,remark=remark, sub1 = sub1.ToUpper().Trim(), sub2 = sub2.ToUpper().Trim(), sub3 = sub3.ToUpper().Trim(), sub4 = sub4.ToUpper().Trim(), sub5 = sub5.ToUpper().Trim() });

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }


        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var myItems = new List<ReportModels.DDLDoubleFieldString>();
            myItems.Add(new ReportModels.DDLDoubleFieldString() { textfield = "Sudah DiPrint", valuefield = "cp1" });
            myItems.Add(new ReportModels.DDLDoubleFieldString() { textfield = "Belum DiPrint", valuefield = "cp0" });
            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").Union(myItems), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________

        // GET: JobCostingMO
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "wom", divgroupoid, "divgroupoid");
            sSql = "SELECT wom.cmpcode, (select gendesc from ql_mstgen where genoid=wom.divgroupoid) divgroup, wom.womstoid, wodate, wono, sorawno soitemno, '' itemcode, sodtldesc itemlongdesc, spkqty, womststatus, (CASE WHEN womststatus='In Process' THEN '' ELSE wom.upduser END) postuser, countprint FROM QL_trnwomst wom INNER JOIN QL_trnsorawmst som ON som.cmpcode=wom.cmpcode AND som.sorawmstoid=wom.somstoid INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=wom.itemoid WHERE isnull(womstres1,'')='Sheet' AND ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "wom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "wom.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND womststatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND womststatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND womststatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND wodate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND wodate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                    {
                        if (modfil.filterstatus == "cp1") sSql += " AND countprint >= 1";
                        else if (modfil.filterstatus == "cp0") sSql += " AND countprint <= 0";
                        else sSql += " AND womststatus='" + modfil.filterstatus + "'";
                    }
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND wom.createuser='" + Session["UserID"].ToString() + "'";

            List<womst> dt = db.Database.SqlQuery<womst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trnwomst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: JobCostingMO/PrintReport/id/cmp
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var tbl = db.QL_trnwomst.FirstOrDefault(x => x.womstoid == id);
            tbl.countprint = tbl.countprint + 1;
            db.Entry(tbl).State = EntityState.Modified;
            db.SaveChanges();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptJobCostingSheet.rpt"));
            report.SetParameterValue("sWhere", "WHERE wo.cmpcode='" + CompnyCode + "' AND wo.womstoid=" + id + "");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            report.Close();
            report.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "SPK.pdf");
        }

        // GET: JobCostingMO/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnwomst tbl;
            var cmp = CompnyCode;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnwomst();
                tbl.womstoid = ClassFunction.GenerateID("QL_trnwomst");
                tbl.wodate = ClassFunction.GetServerTime();
                tbl.woetd = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.womststatus = "In Process";
                tbl.jmllidah = 0;
                tbl.countprint = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnwomst.Find(cmp, id);

                var note = db.QL_trnsorawdtl.FirstOrDefault(x => x.sorawdtloid == tbl.sodtloid).tsheet;
                tbl.remark = (string.IsNullOrEmpty(tbl.remark) ? (note == "GOTOR" ? "KOTOR" : note) : tbl.remark);
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }
        private string generateNo(string boxtype, string sono)
        {

            var sNo = sono+"-00-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(wono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnwomst WHERE cmpcode='" + CompnyCode + "' AND wono LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 4);
            sNo = sNo + sCounter;

            return sNo;
        }


        public class mesin
        {
            public int moid { get; set; }
            public int spkdtlseq { get; set; }
        }

        // POST: dpapMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnwomst tbl, string action, string closing, string soitemno)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<mesin> tblmsn = new List<mesin>();
            if(tbl.m1 != 1680)
            {
              tblmsn.Add(new mesin {spkdtlseq=1,moid=tbl.m1 });
            }
            if (tbl.m2 != 1680)
            {
                tblmsn.Add(new mesin { spkdtlseq = 2, moid = tbl.m2 });
            }
            if (tbl.m3 != 1680)
            {
                tblmsn.Add(new mesin { spkdtlseq = 3, moid = tbl.m3 });
            }
            if (tbl.m4 != 1680)
            {
                tblmsn.Add(new mesin { spkdtlseq = 4, moid = tbl.m4 });
            }
            if (tblmsn != null && tblmsn.Count() > 0)
            {
                var cek_finishing = tblmsn.Where(x => new List<int>{ 1676, 1679 }.Contains(x.moid)).Count();
                if (cek_finishing <= 0)
                {
                    ModelState.AddModelError("", "Mesin/Proses Harus Ada CORRUGATOR/FINISHING!");
                }
            }
            else ModelState.AddModelError("", "Silahkan Pilih Mesin/Proses!");

            if (tbl.wsheet == 0)
            {
                ModelState.AddModelError("", "Berat Tidak boleh 0");
            }
            if (tbl.lbahan == 0)
            {
                ModelState.AddModelError("", "Lb. Bahan Tidak boleh 0");
            }
            if (tbl.trim < 0)
            {
                ModelState.AddModelError("", "Trim Tidak boleh lebih kecil 0");
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnwomst");
                var dtloid = ClassFunction.GenerateID("QL_trnwodtl");
                var servertime = ClassFunction.GetServerTime();
                tbl.wono = "";
                if (tbl.womststatus == "Post")
                {
                    tbl.wono = generateNo(tbl.boxtype, soitemno);
                }
                tbl.sotype = "QL_trnsorawmst";
                tbl.joint = "0";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //if (SaveAs != "")
                        //    action = "New Data";

                        if (action == "New Data")
                        {
                            if (db.QL_trnwomst.Find(CompnyCode,tbl.womstoid) != null)
                                tbl.womstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.wodate);
                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.womstres1 = "Sheet";
                            db.QL_trnwomst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.womstoid + " WHERE tablename='QL_trnwomst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                        }

                        sSql = "UPDATE QL_trnsorawdtl SET sorawdtlres1='Completed' WHERE sorawdtloid='"+tbl.sodtloid+"'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnsorawmst SET statusspk='Closed' WHERE sorawmstoid='" + tbl.somstoid + "' and (SELECT COUNT(sorawdtloid) FROM QL_trnsorawdtl WHERE cmpcode = '" + tbl.cmpcode + "' AND isnull(sorawdtlres1,'') = '' AND sorawmstoid = " + tbl.somstoid + " AND sorawdtloid<> " + tbl.sodtloid + ")= 0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        QL_trnwodtl tbldtl;
                        if (tbl.womststatus == "Post")
                        {
                            for (int i = 0; i < tblmsn.Count(); i++)
                            {
                                tbldtl = new QL_trnwodtl();
                                tbldtl.cmpcode = CompnyCode;
                                tbldtl.wodtloid = dtloid++;
                                tbldtl.divgroupoid = tbl.divgroupoid;
                                tbldtl.womstoid = tbl.womstoid;
                                tbldtl.wodtlseq = tblmsn[i].spkdtlseq;
                                tbldtl.procesoid = tblmsn[i].moid;
                                tbldtl.wodtlstatus = "";
                                tbldtl.upduser = tbl.upduser;
                                tbldtl.updtime = servertime;
                                db.QL_trnwodtl.Add(tbldtl);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid  + " WHERE tablename='QL_trnwodtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }

                        objTrans.Commit();


                        return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnwomst tbl = db.QL_trnwomst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsorawdtl SET sorawdtlres1='' WHERE sorawdtloid='" + tbl.sodtloid + "'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnsorawmst SET statusspk='' WHERE sorawmstoid='" + tbl.somstoid + "' and (SELECT COUNT(sorawdtloid) FROM QL_trnsorawdtl WHERE cmpcode = '" + tbl.cmpcode + "' AND isnull(sorawdtlres1,'') = '' AND sorawmstoid = " + tbl.somstoid + " AND sorawdtloid<> " + tbl.sodtloid + ")= 0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        db.QL_trnwomst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}