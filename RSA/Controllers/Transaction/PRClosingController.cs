﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class PRClosingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class prmst
        {
            public string cmpcode { get; set; }
            public int prmstoid { get; set; }
            public int deptoid { get; set; }
            public string prno { get; set; }
            public string sType { get; set; }
            public string prdate { get; set; }
            public string prexpdate { get; set; }
            public string prmststatus { get; set; }
            public string prmstnote { get; set; }
            public string deptname { get; set; }
        }

        public class prdtl
        {
            public string cmpcode { get; set; }
            public int prdtloid { get; set; }
            public int prmstoid { get; set; }
            public string prtype { get; set; }
            public int prdtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal prqty { get; set; }
            public string prunit { get; set; }
            public string prdtlnote { get; set; }
            public string prdtlstatus { get; set; }
            public string prmststatus { get; set; }
            public string prarrdatereq { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", null);
            ViewBag.cmpcode = cmpcode;
        }

        [HttpPost]
        public ActionResult GetPRData(string cmp, string sType)
        {
            List<prmst> tbl = new List<prmst>();


            var Sawn2 = "";

            if (sType == "SAWN")
            {
                Sawn2 = "  UNION ALL SELECT prsawnmstoid FROM QL_trnposawn2dtl pod INNER JOIN QL_trnposawn2mst pom ON pom.cmpcode=pod.cmpcode AND pom.posawn2mstoid=pod.posawn2mstoid WHERE pod.cmpcode='" + cmp + "' AND posawn2mststatus IN ('Approved', 'Closed')  ";
            }

            sSql = "SELECT cmpcode,prmstoid,deptoid, deptname , prno,sType,prdate, prexpdate,prmststatus, prmstnote FROM (SELECT prm.cmpcode, pr" + sType + "mstoid AS prmstoid, pr" + sType + "no AS prno, '" + sType + "' AS sType, CONVERT(VARCHAR(10), pr" + sType + "date, 101) AS prdate, CONVERT(VARCHAR(10), pr" + sType + "expdate, 101) AS prexpdate, prm.deptoid, ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.deptoid=prm.deptoid), '') AS deptname, pr" + sType + "mstnote AS prmstnote, prm.pr" + sType + "mststatus prmststatus FROM QL_pr" + sType + "mst prm WHERE prm.cmpcode='" + cmp + "' AND pr" + sType + "mststatus='Approved' AND pr" + sType + "mstoid IN (SELECT pr" + sType + "mstoid FROM QL_trnpo" + sType + "dtl pod INNER JOIN QL_trnpo" + sType + "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" + sType + "mstoid=pod.po" + sType + "mstoid WHERE pod.cmpcode='" + cmp + "' AND po" + sType + "mststatus IN ('Approved', 'Closed')" + Sawn2 + ")) AS tbl_PR  ORDER BY CONVERT(DATETIME, prdate) DESC, prmstoid DESC";



            tbl = db.Database.SqlQuery<prmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, string sType, int iOid)
        {
            List<prdtl> tbl = new List<prdtl>();
            if (sType.ToUpper() == "RAW")
                sSql = "SELECT prd.prrawdtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prrawdtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prarrdatereq, prd.matrawoid AS matoid, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, (prrawqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(porawqty - (CASE WHEN ISNULL(pod.porawdtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.porawqty - CONVERT(DECIMAL(18, 4), pod.porawdtlres1)) END)) FROM QL_trnporawdtl pod INNER JOIN QL_trnporawmst pom ON pom.cmpcode=pod.cmpcode AND pom.porawmstoid=pod.porawmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prrawdtloid=prd.prrawdtloid AND pod.prrawmstoid=prd.prrawmstoid AND porawmststatus NOT IN ('Cancel', 'Rejected')), 0) + ISNULL((SELECT SUM(pod2.posubcondtl2qty) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prrawdtloid AND pom.posubconmststatus IN ('Approved', 'Closed') AND pom.posubconref='Raw'), 0.0))) AS prqty, g.gendesc AS prunit, prd.prrawdtlnote AS prdtlnote, prd.prrawdtlstatus AS prdtlstatus FROM QL_prrawdtl prd INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON prd.prrawunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prrawmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "GEN")
                sSql = "SELECT prd.prgendtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prgendtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prarrdatereq, prd.matgenoid AS matoid, m.matgencode AS matcode, m.matgenlongdesc AS matlongdesc, (prgenqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(pogenqty - (CASE WHEN ISNULL(pod.pogendtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.pogenqty - CONVERT(DECIMAL(18, 4), pod.pogendtlres1)) END)) FROM QL_trnpogendtl pod INNER JOIN QL_trnpogenmst pom ON pom.cmpcode=pod.cmpcode AND pom.pogenmstoid=pod.pogenmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prgendtloid=prd.prgendtloid AND pod.prgenmstoid=prd.prgenmstoid AND pogenmststatus NOT IN ('Cancel', 'Rejected')), 0) + ISNULL((SELECT SUM(pod2.posubcondtl2qty) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prgendtloid AND pom.posubconmststatus IN ('Approved', 'Closed') AND pom.posubconref='General'), 0.0))) AS prqty, g.gendesc AS prunit, prd.prgendtlnote AS prdtlnote, prd.prgendtlstatus AS prdtlstatus FROM QL_prgendtl prd INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON prd.prgenunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prgenmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "SP")
                sSql = "SELECT prd.prspdtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prspdtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prarrdatereq, prd.sparepartoid AS matoid, m.sparepartcode AS matcode, m.sparepartlongdesc AS matlongdesc, (prspqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(pospqty - (CASE WHEN ISNULL(pod.pospdtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.pospqty - CONVERT(DECIMAL(18, 4), pod.pospdtlres1)) END)) FROM QL_trnpospdtl pod INNER JOIN QL_trnpospmst pom ON pom.cmpcode=pod.cmpcode AND pom.pospmstoid=pod.pospmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prspdtloid=prd.prspdtloid AND pod.prspmstoid=prd.prspmstoid AND pospmststatus NOT IN ('Cancel', 'Rejected')), 0) + ISNULL((SELECT SUM(pod2.posubcondtl2qty) FROM QL_trnposubcondtl2 pod2 INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod2.cmpcode AND pom.posubconmstoid=pod2.posubconmstoid WHERE pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.prspdtloid AND pom.posubconmststatus IN ('Approved', 'Closed') AND pom.posubconref='Spare Part'), 0.0))) AS prqty, g.gendesc AS prunit, prd.prspdtlnote AS prdtlnote, prd.prspdtlstatus AS prdtlstatus FROM QL_prspdtl prd INNER JOIN QL_mstsparepart m ON prd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON prd.prspunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prspmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "ASSET")
                sSql = "SELECT prd.prassetdtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prassetdtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prarrdatereq, prd.prassetrefoid AS matoid, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS matcode, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS matlongdesc, (prassetqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(poassetqty - (CASE WHEN ISNULL(pod.poassetdtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.poassetqty - CONVERT(DECIMAL(18, 4), pod.poassetdtlres1)) END)) FROM QL_trnpoassetdtl pod INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=pod.cmpcode AND pom.poassetmstoid=pod.poassetmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prassetdtloid=prd.prassetdtloid AND pod.prassetmstoid=prd.prassetmstoid AND poassetmststatus NOT IN ('Cancel', 'Rejected')), 0))) AS prqty, g.gendesc AS prunit, prd.prassetdtlnote AS prdtlnote, prd.prassetdtlstatus AS prdtlstatus FROM QL_prassetdtl prd INNER JOIN QL_mstgen g ON prd.prassetunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prassetmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "WIP")
                sSql = "SELECT prd.prwipdtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prwipdtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prwiparrdatereq, 101) AS prarrdatereq, prd.matwipoid AS matoid, (cat1code + '.' + cat2code + '.' + cat3code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END)) AS matlongdesc, (prwipqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(powipqty - (CASE WHEN ISNULL(pod.powipdtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.powipqty - CONVERT(DECIMAL(18, 4), pod.powipdtlres1)) END)) FROM QL_trnpowipdtl pod INNER JOIN QL_trnpowipmst pom ON pom.cmpcode=pod.cmpcode AND pom.powipmstoid=pod.powipmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prwipdtloid=prd.prwipdtloid AND pod.prwipmstoid=prd.prwipmstoid AND powipmststatus NOT IN ('Cancel', 'Rejected')), 0))) AS prqty, g.gendesc AS prunit, prd.prwipdtlnote AS prdtlnote, prd.prwipdtlstatus AS prdtlstatus FROM QL_prwipdtl prd INNER JOIN QL_mstcat3 c3 ON cat3oid=matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON prd.prwipunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prwipmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "SAWN")
                sSql = "SELECT prd.prsawndtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.prsawndtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.prsawnarrdatereq, 101) AS prarrdatereq, prd.matsawnoid AS matoid, (cat1code + '.' + cat2code) AS matcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) AS matlongdesc, (prsawnqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(posawnqty - (CASE WHEN ISNULL(pod.posawndtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.posawnqty - CONVERT(DECIMAL(18, 4), pod.posawndtlres1)) END)) FROM QL_trnposawndtl pod INNER JOIN QL_trnposawnmst pom ON pom.cmpcode=pod.cmpcode AND pom.posawnmstoid=pod.posawnmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prsawndtloid=prd.prsawndtloid AND pod.prsawnmstoid=prd.prsawnmstoid AND posawnmststatus NOT IN ('Cancel', 'Rejected')), 0) + ISNULL((SELECT SUM( (CASE WHEN ISNULL(pods2.closeqty,0)=0 THEN pods2.posawn2qty ELSE (ISNULL(pods2.posawn2qty,0)-ISNULL(pods2.closeqty,0)) END)) FROM QL_trnposawn2dtl pods2 INNER JOIN QL_trnposawn2mst pom ON pom.cmpcode=pods2.cmpcode AND pom.posawn2mstoid=pods2.posawn2mstoid WHERE pods2.cmpcode=prd.cmpcode AND pods2.prsawndtloid=prd.prsawndtloid AND pods2.prsawnmstoid=prd.prsawnmstoid AND posawn2mststatus IN ('Approved','Closed')), 0))) AS prqty, g.gendesc AS prunit, prd.prsawndtlnote AS prdtlnote, prd.prsawndtlstatus AS prdtlstatus FROM QL_prsawndtl prd INNER JOIN QL_mstcat2 c2 ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid INNER JOIN QL_mstgen g ON prd.prsawnunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.prsawnmstoid=" + iOid + " ORDER BY prdtlseq";
            else if (sType.ToUpper() == "ITEM")
                sSql = "SELECT prd.pritemdtloid AS prdtloid, CAST(ROW_NUMBER() OVER(ORDER BY prd.pritemdtlseq) AS INT) AS prdtlseq, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS prarrdatereq, prd.itemoid AS matoid, m.itemcode AS matcode, m.itemlongdesc AS matlongdesc, (pritemqty - ISNULL(prd.closeqty, 0) - (ISNULL((SELECT SUM(poitemqty - (CASE WHEN ISNULL(pod.poitemdtlres1, '')='' THEN ISNULL(pod.closeqty, 0) ELSE (pod.poitemqty - CONVERT(DECIMAL(18, 4), pod.poitemdtlres1)) END)) FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=pod.cmpcode AND pom.poitemmstoid=pod.poitemmstoid WHERE pod.cmpcode=prd.cmpcode AND pod.pritemdtloid=prd.pritemdtloid AND pod.pritemmstoid=prd.pritemmstoid AND poitemmststatus NOT IN ('Cancel', 'Rejected')), 0))) AS prqty, g.gendesc AS prunit, prd.pritemdtlnote AS prdtlnote, prd.pritemdtlstatus AS prdtlstatus FROM QL_pritemdtl prd INNER JOIN QL_mstitem m ON prd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON prd.pritemunitoid=g.genoid WHERE prd.cmpcode='" + cmp + "' AND prd.pritemmstoid=" + iOid + " ORDER BY prdtlseq";

            tbl = db.Database.SqlQuery<prdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<prdtl> dtDtl)
        {
            Session["QL_prdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string closereason)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            List<prdtl> dtDtl = (List<prdtl>)Session["QL_prdtl"];

            if (dtDtl != null)
            {
                if (dtDtl[0].prmstoid == 0)
                {
                    //ModelState.AddModelError("", "Please select SO Data first!");
                }
                else
                {
                    if (closereason == "")
                        ModelState.AddModelError("", "Please Fill Closing Reason!");

                    if (dtDtl == null)
                        ModelState.AddModelError("", "Please Fill Detail Data!");
                    else if (dtDtl.Count() <= 0)
                        ModelState.AddModelError("", "Please Fill Detail Data!");

                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        sSql = "SELECT COUNT(*) FROM QL_pr" + dtDtl[0].prtype + "dtl WHERE pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND pr" + dtDtl[0].prtype + "dtloid=" + dtDtl[i].prdtloid + " AND pr" + dtDtl[0].prtype + "dtlstatus='Complete'";
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "This data " + dtDtl[i].matcode + " has been Closed by another user. Please CLOSING this transaction and try again!");
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var servertime = ClassFunction.GetServerTime();
                    using (var objTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                sSql = "UPDATE QL_pr" + dtDtl[0].prtype + "dtl SET pr" + dtDtl[0].prtype + "dtlstatus='Complete', prclosingqty='" + dtDtl[i].prqty + "', closeqty='" + dtDtl[i].prqty + "' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND pr" + dtDtl[0].prtype + "dtloid=" + dtDtl[i].prdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_pr" + dtDtl[0].prtype + "mst SET pr" + dtDtl[0].prtype + "mststatus='Closed', closereason='" + closereason + "', closeuser='" + Session["UserID"].ToString() + "', closetime='" + servertime + "' WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND (SELECT COUNT(-1) FROM QL_pr" + dtDtl[0].prtype + "dtl WHERE cmpcode='" + dtDtl[0].cmpcode + "' AND pr" + dtDtl[0].prtype + "dtlstatus='' AND pr" + dtDtl[0].prtype + "mstoid=" + dtDtl[0].prmstoid + " AND pr" + dtDtl[0].prtype + "dtloid<>" + dtDtl[i].prdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            objTrans.Commit();
                            Session["QL_prdtl"] = null;
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("Error", ex.ToString());
                        }
                    }
                }
            }
            InitDDL();
            return View(dtDtl);
        }
    }
}