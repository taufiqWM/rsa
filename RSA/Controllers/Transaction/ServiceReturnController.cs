﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SELFIE.Models.DB;
using SELFIE.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace SELFIE.Controllers
{
    public class ServiceReturnController : Controller
    {
        private QL_SELFIEEntities db = new QL_SELFIEEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class retservicemst
        {
            public int retservicemstoid { get; set; }
            public string retserviceno { get; set; }
            public DateTime retservicedate { get; set; }
            public string suppname { get; set; }
            public string poserviceno { get; set; }
            public string retservicemststatus { get; set; }
            public string retservicemstnote { get; set; }
            public string divname { get; set; }
            public string cmpcode { get; set; }
        }

        public class retservicedtl
        {
            public int retservicedtlseq { get; set; }
            public int poservicedtloid { get; set; }
            public int serviceoid { get; set; }
            public string servicecode { get; set; }
            public string servicelongdesc { get; set; }
            public decimal retserviceqty { get; set; }
            public decimal poserviceqty { get; set; }
            public int retserviceunitoid { get; set; }
            public string retserviceunit { get; set; }
            public int reasonoid { get; set; }
            public string reasondesc { get; set; }
            public string retservicedtlnote { get; set; }
        }

        // GET: ServiceReturn
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "none";

            sSql = "SELECT retservicemstoid, retserviceno, retservicedate, suppname, poserviceno, retservicemststatus, retservicemstnote, divname, retm.cmpcode FROM QL_trnretservicemst retm INNER JOIN QL_trnposervicemst reg ON reg.cmpcode=retm.cmpcode AND reg.poservicemstoid=retm.poservicemstoid INNER JOIN QL_mstsupp s ON s.suppoid=retm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=retm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "retm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "retm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND retservicedate>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND retservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND retservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND retservicedate>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND retservicedate<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND retservicemststatus IN ('In Process', 'Revised')";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil != null)
                {
                    sSql += " AND retservicedate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND retservicedate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND retservicemststatus='" + modfil.filterstatus + "'";
                    DDTitle = "Custom Filter";
                    if (modfil.filterstatus == "ALL" | modfil.filterstatus == "Post" | modfil.filterstatus == "Approved" | modfil.filterstatus == "Closed" | modfil.filterstatus == "Cancel")
                        DisplayCol = "normal";
                }
            }
            else
            {
                sSql += " AND retservicemststatus IN ('In Process', 'Revised')";
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND retm.createuser='" + Session["UserID"].ToString() + "'";

            List<retservicemst> dt = db.Database.SqlQuery<retservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: ServiceReturn/PrintReport/id/cmp
        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnretservicemst.Find(cmp, id);
            if (tbl == null)
                return null;

            var rptname = "rptReturnService";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = " WHERE retm.cmpcode='" + cmp + "' AND retm.retservicemstoid=" + id;

            report.SetParameterValue("sWhere", sSql);
            ClassProcedure.SetDBLogonForReport(report);
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize;
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ServiceReturnPrintOut.pdf");
        }

        // GET: ServiceReturn/Form/id/cmp
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnretservicemst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnretservicemst();
                tbl.cmpcode = CompnyCode;
                tbl.retservicemstoid = ClassFunction.GenerateID("QL_trnretservicemst");
                tbl.retservicedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.retservicemststatus = "In Process";

                Session["QL_trnretservicedtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnretservicemst.Find(cmp, id);
                FillAdditionalField(tbl);

                sSql = "SELECT retservicedtlseq, retd.poservicedtloid, retd.serviceoid, servicecode, servicelongdesc, retserviceqty, (poserviceqty - ISNULL((SELECT SUM(x.retserviceqty) FROM QL_trnretservicedtl x WHERE x.cmpcode=retd.cmpcode AND x.poservicedtloid=retd.poservicedtloid AND x.retservicemstoid<>retd.retservicemstoid), 0.0)) poserviceqty, retserviceunitoid, g1.gendesc retserviceunit, reasonoid, g2.gendesc reasondesc, retservicedtlnote FROM QL_trnretservicedtl retd INNER JOIN QL_mstservice m ON m.serviceoid=retd.serviceoid INNER JOIN QL_trnposervicedtl pod ON pod.cmpcode=retd.cmpcode AND pod.poservicedtloid=retd.poservicedtloid INNER JOIN QL_mstgen g1 ON g1.genoid=retserviceunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=reasonoid WHERE retd.cmpcode='" + tbl.cmpcode + "' AND retservicemstoid=" + tbl.retservicemstoid + " ORDER BY retservicedtlseq";
                var tbldtl = db.Database.SqlQuery<retservicedtl>(sSql).ToList();

                Session["QL_trnretservicedtl"] = tbldtl;
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_trnretservicemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;
        }

        private void FillAdditionalField(QL_trnretservicemst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poserviceno = db.Database.SqlQuery<string>("SELECT poserviceno FROM QL_trnposervicemst WHERE cmpcode='" + tbl.cmpcode + "' AND poservicemstoid=" + tbl.poservicemstoid + "").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetSupplierData(string cmp)
        {
            List<QL_mstsupp> tbl = new List<QL_mstsupp>();
            
            sSql = "SELECT * FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnposervicemst WHERE cmpcode='" + cmp + "' AND poservicemststatus='Approved' AND ISNULL(poservicemstres1, '')='' AND ISNULL(poservicemstres2, '')<>'Closed' AND poservicemstoid NOT IN (SELECT poservicemstoid FROM QL_trnmrservicemst WHERE cmpcode='" + cmp + "')) ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string cmp, int suppoid)
        {
            List<QL_trnposervicemst> tbl = new List<QL_trnposervicemst>();

            sSql = "SELECT * FROM QL_trnposervicemst WHERE cmpcode='" + cmp + "' AND suppoid=" + suppoid + " AND poservicemststatus='Approved' AND ISNULL(poservicemstres1, '')='' AND ISNULL(poservicemstres2, '')<>'Closed' AND poservicemstoid NOT IN (SELECT poservicemstoid FROM QL_trnmrservicemst WHERE cmpcode='" + cmp + "') ORDER BY poservicemstoid";
            tbl = db.Database.SqlQuery<QL_trnposervicemst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
    }
}