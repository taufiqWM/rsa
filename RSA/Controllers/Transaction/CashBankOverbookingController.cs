﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class CashBankOverbookingController : Controller
    {
        private  QL_RSAEntities db = new  QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int cashbankoid { get; set; }
            public string cashbankno { get; set; }
            [DataType(DataType.Date)]
            public DateTime? cashbankdate { get; set; }
            public string cashbanktype { get; set; }
            public string acctgdesc { get; set; }
            public string personname { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamt { get; set; }
            public string acctgdesc_in { get; set; }
            public string cashbankno_in { get; set; }
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public string cashbankno { get; set; }
            public string cashbanktype { get; set; }
            public int addacctgoiddtl1 { get; set; }
            public decimal addacctgamtdtl1 { get; set; }
            public string addacctgdescdtl1 { get; set; }
            public int addacctgoiddtl2 { get; set; }
            public decimal addacctgamtdtl2 { get; set; }
            public string addacctgdescdtl2 { get; set; }
            public int addacctgoiddtl3 { get; set; }
            public decimal addacctgamtdtl3 { get; set; }
            public string addacctgdescdtl3 { get; set; }
            public string cashbanknocost { get; set; }
            public string cashbanktypecost { get; set; }
            public string cashbankno_in { get; set; }
            public int curroid { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
            //var curroid_to = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid_to);
            //ViewBag.curroid_to = curroid_to;

            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + tbl.cmpcode + "' ORDER BY personname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE activeflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY acctgcode";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;

            var addacctgoiddtl1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl1 = addacctgoiddtl1;
            var addacctgoiddtl2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl2 = addacctgoiddtl2;
            var addacctgoiddtl3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl3 = addacctgoiddtl3;
        }

        [HttpPost]
        public ActionResult InitDDLPerson(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstperson> tbl = new List<QL_mstperson>();
            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" + cmp + "' ORDER BY personname";
            tbl = db.Database.SqlQuery<QL_mstperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            //(db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault())
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, 1 curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOADtl(string cmp, string[] sVar, string sFilter)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp, sFilter);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, curroid, '' cashbankno_in, 0 addacctgoiddtl1, 0.0 addacctgamtdtl1, 0 addacctgoiddtl2, 0.0 addacctgamtdtl2, 0 addacctgoiddtl3, 0.0 addacctgamtdtl3, '' addacctgdescdtl1, '' addacctgdescdtl2, '' addacctgdescdtl3 FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl)
        {
            Session["QL_trncashbankgl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankgl"] == null)
            {
                Session["QL_trncashbankgl"] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankgl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.addcost = true;
            decimal addacctgamtdtl1 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt1), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='"+ tbl.cmpcode +"' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            decimal addacctgamtdtl2 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt2), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='" + tbl.cmpcode + "' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            decimal addacctgamtdtl3 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt3), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='" + tbl.cmpcode + "' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            if ((addacctgamtdtl1 + addacctgamtdtl2 + addacctgamtdtl3) > 0)
            {
                ViewBag.addcost = false;
            }
        }

        private string GenerateMutationNo2(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(),6);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateMutationNo(string cmp, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (cmp != "")
            {
                string sNo = cashbanktype + "-" + cashbankdate.ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + cmp + "' AND cashbankno LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)//tambahan_____________________
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM QL_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM QL_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }//endtambahan_____________
        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var isCollapse = "True";//tambahan_____________________
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "cb", divgroupoid, "divgroupoid");
            sSql = "SELECT cb.cmpcode, (select gendesc from ql_mstgen where genoid=cb.divgroupoid) divgroup, cb.cashbankoid, cb.cashbankno, cashbankdate, (CASE cb.cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS cashbanktype, (a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, p.personname, cb.cashbankstatus, cb.cashbanknote, 'False' AS checkvalue, cgl.cashbankglamt cashbankamt, (ax.acctgcode + ' - ' + ax.acctgdesc) AS acctgdesc_in , ISNULL(cbx.cashbankno, '') cashbankno_in  FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstperson p ON p.cmpcode=cb.cmpcode AND p.personoid=cb.personoid INNER JOIN QL_trncashbankgl cgl ON cgl.cmpcode=cb.cmpcode AND cgl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg ax ON ax.acctgoid=cgl.acctgoid LEFT JOIN (SELECT cglx.cmpcode,ISNULL(CAST(cglx.cashbankglres1 AS INT), 0) glres1, ISNULL(cbx.cashbankno, '') cashbankno, cbx.cashbankamtidr,cbx.cashbanknote FROM QL_trncashbankgl cglx INNER JOIN QL_trncashbankmst cbx ON cbx.cmpcode=cglx.cmpcode AND cbx.cashbankoid=cglx.cashbankoid AND cbx.cashbankgroup='MUTATIONTO') as cbx  ON cbx.cmpcode=cb.cmpcode AND cbx.glres1=cb.cashbankoid AND cbx.cashbankamtidr=cgl.cashbankglamtidr  AND cbx.cashbanknote=cgl.cashbankglnote WHERE cb.cashbankgroup='MUTATION' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND cb.cmpcode LIKE '%'";
            sSql += sqlplus;
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "1")//tambahan_____________________
                    sSql += " AND cashbankstatus IN ('In Process', 'Revised')";
                else if (filter == "2")
                    sSql += " AND cashbankstatus IN ('In Approval')";
                else if (filter == "3")
                    sSql += " AND cashbankstatus IN ('Approved', 'Post')";
                else if (filter == "POST")
                {
                    if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                    sSql += " AND " + modfil.filterddl + " LIKE '%" + ClassFunction.Tchar(modfil.filtertext) + "%'";
                    if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null)
                        sSql += " AND cashbankdate>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    if (modfil.filterstatus != "ALL")
                        sSql += " AND cashbankstatus='" + modfil.filterstatus + "'";
                    isCollapse = "False";
                }//endtambahan_____________________
            }
            else
            {
                sSql += " AND 1=0";//tambahan____________
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND cb.createuser='" + Session["UserID"].ToString() + "'";
            sSql += " ORDER BY CONVERT(DATETIME, cb.cashbankdate) DESC, cb.cashbankoid DESC";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            InitAdvFilterIndex(modfil, "QL_trncashbankmst", false);//tambahan____________
            ViewBag.isCollapse = isCollapse;//tambahan____________
            return View(dt);
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            cmp = CompnyCode;
            QL_trncashbankmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();
                tbl.cashbankresamt = 1;
                tbl.cashbankdpp = 0;
                ViewBag.addcost = true;

                Session["QL_trncashbankgl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncashbankmst.Find(cmp, id);
                Session["lastcashbanktype"] = tbl.cashbanktype;
                Session["lastdpapoid"] = tbl.giroacctgoid;
                Session["lastdpapamt"] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, '' AS cashbankno, '' AS cashbanktype, cbgl.addacctgoid1 addacctgoiddtl1, cbgl.addacctgamt1 addacctgamtdtl1, cbgl.addacctgoid2 addacctgoiddtl2, cbgl.addacctgamt2 addacctgamtdtl2, cbgl.addacctgoid3 addacctgoiddtl3, cbgl.addacctgamt3 addacctgamtdtl3, '' AS cashbanknocost, '' AS cashbanktypecost  , ISNULL((SELECT cashbankno FROM QL_trncashbankgl cbglx INNER JOIN QL_trncashbankmst cbmx ON cbmx.cmpcode=cbglx.cmpcode AND cbmx.cashbankoid=cbglx.cashbankoid WHERE cbglx.cmpcode=cbgl.cmpcode AND ISNULL(CAST(cbglx.cashbankglres1 AS INT), 0)=cbgl.cashbankoid AND cbmx.cashbankamtidr=cbgl.cashbankglamtidr AND cbmx.cashbanknote=cbgl.cashbankglnote and cbglx.acctgoid=cbgl.acctgoid), '') AS cashbankno_in, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid1), '') addacctgdescdtl1, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid2), '') addacctgdescdtl2, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid3), '') addacctgdescdtl3 FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + cmp + "'";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankgl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            
            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankgl"];
            if (tbl.cashbankresamt <= 0 )
            {
                ModelState.AddModelError("", "Please fill RATE field!");
            }
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select MUTATION TO ACCOUNT field!");
            if (tbl.cashbanktype != "BKK")
            {
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            }
            if (tbl.personoid == 0)
                ModelState.AddModelError("", "Please select PIC field!");
            if (tbl.addacctgoid1 != 0)
                if (tbl.addacctgamt1 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid2)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                }
                if (tbl.addacctgoid2 != 0)
                {
                    if (tbl.addacctgoid2 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                }
            }
            if (tbl.curroid != 1 )
            {
                ModelState.AddModelError("", "Please select One of Currency must be in IDR");
            }

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].acctgoid == tbl.acctgoid)
                            ModelState.AddModelError("", "Every Overbooking To COA must be different with Overbooking From COA!");
                        if (dtDtl[i].cashbankglamt <= 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be more than 0!");
                        }
                        if (dtDtl[i].addacctgoiddtl1 != 0)
                            if (dtDtl[i].addacctgamtdtl1 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
                        if (dtDtl[i].addacctgoiddtl2 != 0)
                        {
                            if (dtDtl[i].addacctgamtdtl2 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                            if (dtDtl[i].addacctgoiddtl1 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl1 == dtDtl[i].addacctgoiddtl2)
                                    ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                            }
                        }
                        if (dtDtl[i].addacctgoiddtl3 != 0)
                        {
                            if (dtDtl[i].addacctgamtdtl3 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                            if (dtDtl[i].addacctgoiddtl1 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl1 == dtDtl[i].addacctgoiddtl3)
                                    ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                            }
                            if (dtDtl[i].addacctgoiddtl2 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl2 == dtDtl[i].addacctgoiddtl3)
                                    ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                            }
                        }
                    }
                }
            }

            var cRate = new ClassRate(); var cRateTo = new ClassRate(); var cRateCost = new ClassRate(); var cRateCostTo = new ClassRate();
            int iSelisihAcctgOid = 0, iAyatSilangAcctgOid = 0;
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK"||tbl.cashbanktype=="BBM")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = Convert.ToDateTime(tbl.cashbankduedate);
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = Convert.ToDateTime(tbl.cashbankduedate);
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            if (tbl.cashbankstatus == "Post")
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                if (ClassFunction.isPeriodAcctgClosed(tbl.cmpcode, sDate))
                {
                    ModelState.AddModelError("", "Cannot posting accounting data to period " + mfi.GetMonthName(sDate.Month).ToString() + " " + sDate.Year.ToString() + " anymore because the period has been closed. Please select another period!"); tbl.cashbankstatus = "In Process";
                }
                cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "") ModelState.AddModelError("", cRate.GetRateDailyLastError);
                if (cRate.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRate.GetRateMonthlyLastError);


                cRateCost.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRateCost.GetRateDailyLastError != "") ModelState.AddModelError("", cRateCost.GetRateDailyLastError);
                if (cRateCost.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRateCost.GetRateMonthlyLastError);

                //cRateCostTo.SetRateValue(tbl.curroid_to, sDate.ToString("MM/dd/yyyy"));
                //if (cRateCostTo.GetRateDailyLastError != "") ModelState.AddModelError("", cRateCostTo.GetRateDailyLastError);
                //if (cRateCostTo.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRateCostTo.GetRateMonthlyLastError);

                string sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_AYAT_SILANG", tbl.cmpcode))
                    sVarErr = "VAR_AYAT_SILANG";
                else
                    iAyatSilangAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AYAT_SILANG", tbl.cmpcode));
                if (!ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", tbl.cmpcode))
                    sVarErr = "VAR_DIFF_CURR_IDR";
                else
                    iSelisihAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", tbl.cmpcode));

                if (sVarErr != "") ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));

               
            }
            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                //var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                int iCashBankOid = 0;
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        //mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateMutationNo2(tbl.cmpcode, tbl.cashbankdate, tbl.cashbanktype, tbl.acctgoid);
                    }
                    //iCashBankOid = mstoid + 1;
                }
                else
                {
                    //iCashBankOid = ClassFunction.GenerateID("QL_trncashbankmst");
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = tbl.cashbankamt * cRate.GetRateMonthlyUSDValue;
                tbl.cashbankgroup = "MUTATION";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = (tbl.cashbanktype == "BGK" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid).FirstOrDefault();
                decimal cashbankamt = tbl.cashbankamt - (tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3);

                if (tbl.cashbankstatus == "Post")
                {
                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        string sType = "BKM", sTypeCost = "BKK";
                        string sVar_Bank = ClassFunction.GetDataAcctgOid("VAR_BANK", tbl.cmpcode, sFilter: " AND acctgoid=" + dtDtl[i].acctgoid);
                        if (sVar_Bank != "0")
                        {
                            sType = "BBM";
                            sTypeCost = "BBK";
                        }

                        string sNo = sType + "-" + tbl.cashbankdate.ToString("yyyy.MM") + "-";
                        string cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>("SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankno LIKE '%" + sNo + "%'").FirstOrDefault(), int.Parse(DefaultFormatCounter));
                        string sNoCost = sTypeCost + "-" + tbl.cashbankdate.ToString("yyyy.MM") + "-";
                        string cashbanknocost = sNoCost + ClassFunction.GenNumberString(db.Database.SqlQuery<int>("SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankno LIKE '%" + sNoCost + "%'").FirstOrDefault(), int.Parse(DefaultFormatCounter));

                        dtDtl[i].cashbanktype = sType;
                        dtDtl[i].cashbankno = cashbankno;
                        dtDtl[i].cashbanktypecost = sTypeCost;
                        dtDtl[i].cashbanknocost = cashbanknocost;
                    }
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                        

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtDtl[i].cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);

                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = dtDtl[i].cashbankglnote;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.addacctgoid1 = dtDtl[i].addacctgoiddtl1;
                            tbldtl.addacctgoid2 = dtDtl[i].addacctgoiddtl2;
                            tbldtl.addacctgoid3 = dtDtl[i].addacctgoiddtl3;
                            tbldtl.addacctgamt1 = dtDtl[i].addacctgamtdtl1;
                            tbldtl.addacctgamt2 = dtDtl[i].addacctgamtdtl2;
                            tbldtl.addacctgamt3 = dtDtl[i].addacctgamtdtl3;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            decimal dTotalOutInIDR = 0;
                            decimal dTotalOutInUSD = 0
        ;
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Cash/Bank Overbooking|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Ayat Silang Debet
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iAyatSilangAcctgOid, "D", cashbankamt, tbl.cashbankno, "Ayat Silang - Cash/Bank Overbooking|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, cashbankamt * cRate.GetRateMonthlyIDRValue, cashbankamt * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            gldtloid += 1;
                            iSeq += 1;
                            dTotalOutInIDR += cashbankamt * cRate.GetRateMonthlyIDRValue;
                            dTotalOutInUSD += cashbankamt * cRate.GetRateMonthlyUSDValue;

                            //Additional Cost 1+/-
                            if (tbl.addacctgamt1 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt1), tbl.cashbankno, tbl.cashbanknote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * cRate.GetRateMonthlyIDRValue, Math.Abs(tbl.addacctgamt1) * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 1 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt1 * cRateCost.GetRateMonthlyIDRValue;
                                dTotalOutInUSD += tbl.addacctgamt1 * cRateCost.GetRateMonthlyUSDValue;
                            }
                            //Additional Cost 2+/-
                            if (tbl.addacctgamt2 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt2), tbl.cashbankno, tbl.cashbanknote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * cRate.GetRateMonthlyIDRValue, Math.Abs(tbl.addacctgamt2) * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 2 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt2 * cRateCost.GetRateMonthlyIDRValue;
                                dTotalOutInUSD += tbl.addacctgamt2 * cRateCost.GetRateMonthlyUSDValue;
                            }
                            //Additional Cost 3+/-
                            if (tbl.addacctgamt3 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt3), tbl.cashbankno, tbl.cashbanknote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * cRate.GetRateMonthlyIDRValue, Math.Abs(tbl.addacctgamt3) * cRate.GetRateMonthlyUSDValue, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 3 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt3 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt3 * cRateCost.GetRateMonthlyIDRValue;
                                dTotalOutInUSD += tbl.addacctgamt3 * cRateCost.GetRateMonthlyUSDValue;
                            }

                            //CREDIT
                            //Kas/Bank Keluar
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, "Ayat Silang - Cash/Bank Overbooking|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dTotalOutInIDR, dTotalOutInUSD, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                            db.SaveChanges();
                            gldtloid += 1;
                            iSeq += 1;
                            glmstoid += 1;

                            decimal dRate = 1;
                            for (var i=0; i < dtDtl.Count(); i++)
                            {
                                dtDtl[i].cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);

                                QL_trncashbankmst tblcashbank = new QL_trncashbankmst();
                                tblcashbank.cmpcode = tbl.cmpcode;
                                tblcashbank.cashbankoid = iCashBankOid;
                                tblcashbank.cashbankno = GenerateMutationNo2(tbl.cmpcode, tbl.cashbankdate, tbl.cashbanktype, tbl.acctgoid);
                                tblcashbank.cashbankdate = sDate;
                                tblcashbank.cashbanktype = dtDtl[i].cashbanktype;
                                tblcashbank.cashbankgroup = "MUTATIONTO";
                                tblcashbank.acctgoid = dtDtl[i].acctgoid;
                                tblcashbank.curroid = tbl.curroid;
                                tblcashbank.cashbankamt = dtDtl[i].cashbankglamt * dRate;
                                tblcashbank.cashbankamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                                tblcashbank.cashbankamtusd = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue;
                                tblcashbank.personoid = tbl.personoid;
                                tblcashbank.cashbankduedate = (tbl.cashbanktype == "BKK" ? tbl.cashbankdate : tbl.cashbankduedate);
                                tblcashbank.cashbankrefno = tbl.cashbankrefno;
                                tblcashbank.cashbanknote = dtDtl[i].cashbankglnote;
                                tblcashbank.cashbankstatus = tbl.cashbankstatus;
                                tblcashbank.createuser = tbl.createuser;
                                tblcashbank.createtime = tbl.createtime;
                                tblcashbank.cashbanktakegiro = DateTime.Parse("1/1/1900 00:00:00");
                                tblcashbank.addacctgoid1 = dtDtl[i].addacctgoiddtl1;
                                tblcashbank.addacctgamt1 = dtDtl[i].addacctgamtdtl1;
                                tblcashbank.addacctgoid2 = dtDtl[i].addacctgoiddtl2;
                                tblcashbank.addacctgamt2 = dtDtl[i].addacctgamtdtl2;
                                tblcashbank.addacctgoid3 = dtDtl[i].addacctgoiddtl3;
                                tblcashbank.addacctgamt3 = dtDtl[i].addacctgamtdtl3;
                                //tblcashbank.curroid_to = tbl.curroid;
                                //tblcashbank.cashbankresamt = tbl.cashbankresamt2;
                                //tblcashbank.cashbankresamt2 = tbl.cashbankresamt;

                                tblcashbank.cashbanktakegiroreal = (tblcashbank.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbanktakegiroreal);
                                tblcashbank.cashbankgiroreal = (tblcashbank.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbankgiroreal);
                                tblcashbank.cashbankaptype = (tblcashbank.cashbankaptype == null ? "" : tblcashbank.cashbankaptype);
                                tblcashbank.cashbanktaxtype = "NON TAX";
                                tblcashbank.periodacctg = sPeriod;
                                tblcashbank.upduser = tbl.createuser;
                                tblcashbank.updtime = tbl.createtime;

                                db.QL_trncashbankmst.Add(tblcashbank);
                                db.SaveChanges();

                                QL_trncashbankgl tbldtl2 = new QL_trncashbankgl();
                                tbldtl2.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                                tbldtl2.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                                tbldtl2.cashbankglgiroflag = "";

                                tbldtl2.cmpcode = tbl.cmpcode;
                                tbldtl2.cashbankgloid = dtloid++;
                                tbldtl2.cashbankoid = tblcashbank.cashbankoid;
                                tbldtl2.cashbankglseq = i + 1;
                                tbldtl2.acctgoid = tbl.acctgoid;
                                tbldtl2.cashbankglamt = dtDtl[i].cashbankglamt * dRate;
                                tbldtl2.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                                tbldtl2.cashbankglamtusd = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyUSDValue;
                                tbldtl2.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankdate : tbl.cashbankduedate);
                                tbldtl2.cashbankglrefno = tbl.cashbankrefno;
                                tbldtl2.cashbankglstatus = tbl.cashbankstatus;
                                tbldtl2.cashbankglnote = tbl.cashbanknote;
                                tbldtl2.createuser = tbl.createuser;
                                tbldtl2.createtime = tbl.createtime;
                                tbldtl2.upduser = tbl.upduser;
                                tbldtl2.updtime = tbl.updtime;
                                tbldtl2.cashbankglres1 = tbl.cashbankoid.ToString();

                                db.QL_trncashbankgl.Add(tbldtl2);
                                db.SaveChanges();

                                //JURNAL BANK MASUK
                                iSeq = 1;
                                //Insert Into GLMst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Cash/Bank Overbooking|No=" + dtDtl[i].cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRateTo.GetRateDailyOid, cRateTo.GetRateMonthlyOid, cRateTo.GetRateDailyIDRValue, cRateTo.GetRateMonthlyIDRValue, cRateTo.GetRateDailyUSDValue, cRateTo.GetRateMonthlyUSDValue, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                //Insert Into GLDtl
                                //Kas/Bank Masuk
                                decimal dTotCostPerLine = dtDtl[i].addacctgamtdtl1 + dtDtl[i].addacctgamtdtl2 + dtDtl[i].addacctgamtdtl3;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", (dtDtl[i].cashbankglamt * dRate) - dTotCostPerLine, dtDtl[i].cashbankno, dtDtl[i].cashbankglnote, tbl.cashbankstatus, tbl.upduser, tbl.updtime, (dtDtl[i].cashbankglamt * cRateTo.GetRateMonthlyIDRValue) - (dTotCostPerLine * cRateCostTo.GetRateMonthlyIDRValue), (dtDtl[i].cashbankglamt * cRateTo.GetRateMonthlyUSDValue) - (dTotCostPerLine * cRateCostTo.GetRateMonthlyUSDValue), "QL_trncashbankmst " + iCashBankOid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                gldtloid += 1;
                                iSeq += 1;
                                if (dTotCostPerLine > 0)
                                {
                                    //Additional Cost 1+/-
                                    if (dtDtl[i].addacctgamtdtl1 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl1, (dtDtl[i].addacctgamtdtl1 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl1), dtDtl[i].cashbankno, "Cash/Bank Overbooking Additional Cost 1 on Detail|No=" + dtDtl[i].cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl1) * cRateCostTo.GetRateMonthlyIDRValue, Math.Abs(dtDtl[i].addacctgamtdtl1) * cRateCostTo.GetRateMonthlyUSDValue, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 1 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl1 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                    //Additional Cost 2+/-
                                    if (dtDtl[i].addacctgamtdtl2 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl2, (dtDtl[i].addacctgamtdtl2 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl2), dtDtl[i].cashbankno, "Cash/Bank Overbooking Additional Cost 2 on Detail|No=" + dtDtl[i].cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl2) * cRateCostTo.GetRateMonthlyIDRValue, Math.Abs(dtDtl[i].addacctgamtdtl2) * cRateCostTo.GetRateMonthlyUSDValue, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 2 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl2 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                    //Additional Cost 3+/-
                                    if (dtDtl[i].addacctgamtdtl3 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl3, (dtDtl[i].addacctgamtdtl3 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl3), dtDtl[i].cashbankno, "Cash/Bank Overbooking Additional Cost 3 on Detail|No=" + dtDtl[i].cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl3) * cRateCostTo.GetRateMonthlyIDRValue, Math.Abs(dtDtl[i].addacctgamtdtl3) * cRateCostTo.GetRateMonthlyUSDValue, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 3 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl3 > 0 ? "K" : "M"), 0, tbl.divgroupoid ?? 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                }
                                //Ayat Silang
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iAyatSilangAcctgOid, "C", dtDtl[i].cashbankglamt * dRate, dtDtl[i].cashbankno, "Ayat Silang - Cash/Bank Overbooking|No=" + dtDtl[i].cashbankno + " dg " + tbl.cashbankno + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRateTo.GetRateMonthlyIDRValue, dtDtl[i].cashbankglamt * cRateTo.GetRateMonthlyUSDValue, "QL_trncashbankmst " + iCashBankOid + "", "", "", "", 0, tbl.divgroupoid ?? 0));
                                db.SaveChanges();
                                gldtloid += 1;
                                glmstoid += 1;
                                iCashBankOid += 1;
                                dtloid = dtloid + 1;
                            }

                            sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_TRNCASHBANKGL'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (iCashBankOid - 1) + " WHERE tablename='QL_TRNCASHBANKMST'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (glmstoid - 1) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.cashbankoid);
                        else
                            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == cmp);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
           if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            string sWhere = " WHERE cb.cashbankgroup='MUTATION' AND cb.cashbankoid IN (" + id + ")";
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMutation.rpt"));
            if (printtype == "In")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMutation2.rpt"));
                sWhere = " WHERE cb.cashbankgroup='MUTATIONTO' AND cb.cashbankoid IN (SELECT glx.cashbankoid FROM QL_trncashbankgl glx WHERE glx.cashbankglres1 IN (" + id + "))";
            }

            report.SetParameterValue("sWhere", sWhere);
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "OverbookingPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}