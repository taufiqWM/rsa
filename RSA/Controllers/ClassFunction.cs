﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.Models.ViewModel;
using RSA.Models.DB;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Hosting;

namespace RSA.Controllers
{
    static class ClassFunction
    {
        public class mstacctg
        {
            public int acctgoid { get; set; }
        }

        public class mstacctg2
        {
            public string acctgcode { get; set; }
        }

        public class trngldtl
        {
            public int glseq { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal acctgdebet { get; set; }
            public decimal acctgcredit { get; set; }
        }

        public static string FilterDataByUserLevel(string UserID, string alias,string divgroupoid, string tname)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            var uslevel = db.Database.SqlQuery<string>("SELECT isnull(proflevel,'USER') from QL_mstprof where profoid='" + UserID + "'").FirstOrDefault();
            var sqlQuery = "";
            if (uslevel == "USER")
            {
                sqlQuery += " AND " + alias + "."+tname+"=" + divgroupoid;
            }
        
            return sqlQuery;
        }
        public static bool checkPagePermission(string PagePath, List<QL_m04MN> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return false;
            }
            List<QL_m04MN> newrole = dtAccess.FindAll(o => o.mnfileloc == PagePath);
            return newrole.Count() <= 0;
        }

        public static bool checkPagePermissionForReport(string PagePath, List<QL_m04MN> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return false;
            }
            List<QL_m04MN> newrole = dtAccess.FindAll(o => o.mnfileloc.ToUpper() + "/" + o.mnmenuview.ToUpper() == PagePath.ToUpper());
            return newrole.Count() <= 0;
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        public static bool isSpecialAccess(string PagePath, List<RoleSpecial> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return true;
            }
            List<RoleSpecial> newrole = dtAccess.FindAll(o => o.formaddress.ToUpper() == PagePath.ToUpper());
            return newrole.Count() > 0;
        }

        public static Boolean CheckDataExists(string sSql)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            Boolean cDataEx = false;
            int sTmp = 0;

            sTmp = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (sTmp > 0)
            {
                cDataEx = true;
            }
            return cDataEx;
        }
        public static Boolean CheckDataExists(Int64 iKey, string[] sColumnKeyName, string[] sTable)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            Boolean cDataEx = false;
            string sSql = "";
            int sTmp = 0;

            for (int i = 0; i < sTable.Length; i++)
            {
                sSql = "SELECT COUNT(-1) FROM " + sTable[i] + " WHERE " + sColumnKeyName[i] + "=" + iKey;
                sTmp = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                if (sTmp > 0)
                {
                    cDataEx = true;
                    break;
                }
            }
            return cDataEx;
        }

        public static Boolean IsQtyRounded(decimal dQty, decimal dRound)
        {
            long iRes;
            string sResult = Convert.ToString(dQty / dRound);
            if (long.TryParse(sResult, out iRes))
                return true;
            else
                return false;
        }

        public static string toRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            // starting with the highest place (for 3046, it would be the thousands
            // place, or 3), get the roman numeral representation for that place
            // and add it to the final roman numeral string
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

        public static DateTime GetServerTime()
        {
            QL_RSAEntities db = new QL_RSAEntities();
            DateTime sDate = db.Database.SqlQuery<DateTime>("SELECT getdate() as tanggal").FirstOrDefault();
            //DateTime dTime = DateTime.Parse(sDate);
            return sDate;
        }

     

        public static string GetMultiUserStatus(string sTable, string sOidField, string sOidValue, string sStatusField, DateTime sUpdTime, string sType)
        {
            string MultiUserStatus = "";
            string sSql = "SELECT COUNT(*) FROM " + sTable + " WHERE " + sOidField + "=" + sOidValue + " AND " + sStatusField + "='In Process' AND (CONVERT(VARCHAR(10), updtime, 101) + ' ' + CONVERT(VARCHAR(10), updtime, 108))<>'" + sUpdTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";
            if (CheckDataExists(sSql))
            {
                MultiUserStatus = "This data has been updated by another user. Please CANCEL this transaction and try again!";
            }
            else
            {
                sSql = "SELECT COUNT(*) FROM " + sTable + " WHERE " + sOidField + "=" + sOidValue + " AND " + sStatusField + "='" + ((sType == "Post") ? sType : "In Approval") +"'";

                if (CheckDataExists(sSql))
                {
                    MultiUserStatus = "This data has been " + ((sType == "Post") ? "posted" : "sent for approval") + " by another user. Please CANCEL this transaction!";
                }
            }
            return MultiUserStatus;
        }

        public static string GenNumberString(int iNumber, int iDefaultCounter)
        {
            int iAdd = iNumber.ToString().Length - iDefaultCounter;
            if (iAdd > 0)
            {
                iDefaultCounter += iAdd;
            }
            string sFormat = "";
            for (int i = 1; i <= iDefaultCounter; i++)
            {
                sFormat += "0";
            }
            return iNumber.ToString(sFormat);
        }

        public static int GenerateID(string sTableName)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            var CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            int oid = 1;
            var mstoid = db.QL_mstoid.Where(w => w.tablename.ToUpper() == sTableName.ToUpper()).FirstOrDefault();

            if (mstoid != null)
            {
                oid = mstoid.lastoid + 1;
            }
            else
            {
                QL_mstoid ql_id = new QL_mstoid();
                ql_id.cmpcode = CompnyCode;
                ql_id.tablename = sTableName.ToUpper();
                ql_id.lastoid = 1;
                ql_id.tablegroup = "TRANSACTION";
                db.QL_mstoid.Add(ql_id);
                db.SaveChanges();
            }
            return oid;
        }

        public static string GetDateToPeriodAcctg(DateTime date)
        {
            return date.ToString("yyyyMM");
        }

        public static string GetDataAcctgOid(string sVar, string cmp, string sFilter = "", int divgroupoid = 0)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            List<mstacctg> tbl = new List<mstacctg>();
            string sSql = "";
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar + "' AND interfaceres1='" + cmp + "'";
            string sCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (sCode == "")
            {
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar + "' AND interfaceres1='All'";
                sCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            }
            if (sCode != "")
            {
                sSql = "SELECT DISTINCT a.acctgoid FROM QL_mstacctg a WHERE a.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' " + sFilter + " AND (";
                string[] sSplitCode = sCode.Split(',');
                for (var i = 0; i < sSplitCode.Length; i++)
                {
                    sSql += "a.acctgcode LIKE '" + sSplitCode[i].TrimStart() + "%'";
                    if (i < sSplitCode.Length - 1)
                    {
                        sSql += " OR ";
                    }
                }
                sSql += ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode)";
                if (divgroupoid > 0)
                {
                    sSql += " AND a.divgroupoid=" + divgroupoid + " ";
                }
            }
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();
            string sOid = "0";
            string sOidTemp = "";
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        sOidTemp += tbl[i].acctgoid.ToString() + ", ";
                    }
                    if (sOidTemp != "")
                        sOid = Left(sOidTemp, sOidTemp.Length - 2);
                }
            }
            return sOid;
        }

        public static string GetDataAcctgOid(string[] sVar, string cmp, string sFilter = "", int divgroupoid = 0)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            List<mstacctg> tbl = new List<mstacctg>();
            List<mstacctg2> tbl2 = new List<mstacctg2>();
            string sSql = "";
            string sCode = "";
            for (var i = 0; i < sVar.Length; i++)
            {
                sSql = "SELECT imd.acctgcode FROM QL_mstinterface im INNER JOIN QL_mstinterfacedtl imd ON imd.interfaceoid=im.interfaceoid INNER JOIN QL_mstacctg a ON a.acctgoid=imd.acctgoid WHERE im.cmpcode='" + CompnyCode + "' AND im.interfacevar='" + sVar[i] + "' ";
                if (divgroupoid > 0)
                {
                    sSql += " AND a.divgroupoid=" + divgroupoid + " ";
                }
                tbl2 = db.Database.SqlQuery<mstacctg2>(sSql).ToList();
                for (var ii = 0; ii < tbl2.Count; ii++)
                {
                    if (sCode == "")
                        sCode = tbl2[ii].acctgcode;
                    else
                        sCode += ", " + tbl2[ii].acctgcode;
                }
                    
            }
            if (sCode != "")
            {
                sSql = "SELECT DISTINCT a.acctgoid FROM QL_mstacctg a WHERE a.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' " + sFilter + " AND (";
                string[] sSplitCode = sCode.Split(',');
                for (var i = 0; i < sSplitCode.Length; i++)
                {
                    sSql += "a.acctgcode LIKE '" + sSplitCode[i].TrimStart() + "%'";
                    if (i < sSplitCode.Length - 1)
                    {
                        sSql += " OR ";
                    }
                }
                sSql += ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ";
            }
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();
            string sOid = "0";
            string sOidTemp = "";
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        sOidTemp += tbl[i].acctgoid.ToString() + ", ";
                    }
                    if (sOidTemp != "")
                        sOid = Left(sOidTemp, sOidTemp.Length - 2);
                }
            }
            return sOid;
        }

        public static string GetLastPeriod(string speriod)
        {
            string periodbefore = "";
            if (speriod != "")
            {
                if (speriod.Length == 6)
                {
                    var iVal_1 = Convert.ToInt32(Left(speriod, 2));
                    if (iVal_1 == 1)
                    {
                        var sVal_2 = (Convert.ToInt32(Left(speriod, 4)) - 1).ToString("0000");
                        periodbefore = sVal_2 + "12";
                    }
                    else
                    {
                        var sVal_3 = Left(speriod, 4);
                        var sVal_4 = (Convert.ToInt32(Right(speriod, 2)) - 1).ToString("00");
                        periodbefore = sVal_3 + sVal_4;
                    }
                }
            }
            return periodbefore;
        }

        public static List<dtFiles> getFileFromDir(UrlHelper Url, string fileloc, string fileflag, List<dtFiles> dtFiles)
        {
            //fileflag : "set to empty if delete able"
            var sdir = HttpContext.Current.Server.MapPath(fileloc);
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var dtfile = new dtFiles();
                    dtfile.seq = dtFiles.Count() + 1;
                    var sname = file.Split('\\');
                    dtfile.filename = sname[sname.Length - 1];
                    dtfile.fileurl = Url.Content(fileloc + "/" + dtfile.filename);
                    dtfile.fileflag = fileflag;
                    dtFiles.Add(dtfile);
                }
            }
            return dtFiles;
        }

        public static Byte[] getTandaTangan(string sPath)
        {
            Byte[] arrImage = null;
            if (File.Exists(sPath))
            {
                Image img = Image.FromFile(sPath);
                MemoryStream ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
            return arrImage;
        }

        public static string Tchar(string svar)
        {
            return svar.Trim().Replace("'", "''");
        }

        public static String Right(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length))
            {
                length = input.Length;
            }
            result = input.Substring((input.Length - length), length);
            return result;
        }

        public static String Left(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length))
            {
                length = input.Length;
            }
            result = input.Substring(0, length);
            return result;
        }

        public static string toDate(string sTgl)
        {
            string[] arTgl = sTgl.Split('/');
            try
            {
                return arTgl[1] + "/" + arTgl[0] + "/" + arTgl[2];
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public static Boolean IsInterfaceExists(string sVar, string cmp, int divgroupoid = 0)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            var sSql = ""; var flag = false;
            sSql = "SELECT COUNT(-1) FROM QL_mstinterface im INNER JOIN QL_mstinterfacedtl imd ON imd.interfaceoid=im.interfaceoid INNER JOIN QL_mstacctg a ON a.acctgoid=imd.acctgoid WHERE im.cmpcode='" + CompnyCode + "' AND im.activeflag='ACTIVE' AND im.interfacevar='" + sVar + "'";
            if (divgroupoid > 0)
            {
                sSql += " AND a.divgroupoid=" + divgroupoid + " ";
            }
            var iCountVar = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (iCountVar > 0)
            {
                flag = true;
            }
            return flag;
        }

        public static string GetInterfaceWarning(string sVar)
        {
            return "Please Define some COA for Variable " + sVar + " in Accounting -> Master -> Interface before continue this form!";
        }

        public static int GetAcctgOID(string sVar)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            var sSql = "";
            sSql = "SELECT TOP 1 acctgoid FROM QL_mstacctg WHERE acctgcode='" + sVar + "' and cmpcode='" + CompnyCode + "'";
            var iAcctgOid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            return iAcctgOid;
        }

        public static Boolean isLengthAccepted(string sField, string sTable, decimal dValue, ref string sErrReply)
        {
            int iPrec = 0, iScale = 0;
            using (var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnString"]))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" + sField + "' AND ta.name='" + sTable + "'";
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            iPrec = reader.GetOrdinal("precision");
                            iScale = reader.GetOrdinal("scale");
                        }
                        reader.Close();
                    }
                }
                catch
                {
                    iPrec = 0; iScale = 0;
                    sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!";
                    return false;
                }
                conn.Close();
            }
            if (iPrec > 0 && iScale > 0)
            {
                string sTextValue = "";
                for (var i = 0; i < (iPrec - iScale - 1); i++)
                {
                    sTextValue += "9";
                }
                if (iScale > 0)
                {
                    sTextValue += ".";
                    for (var i = 0; i < iScale - 1; i++)
                    {
                        sTextValue += "9";
                    }
                }
                if (dValue > Convert.ToDecimal(sTextValue))
                {
                    sErrReply = string.Format("{0:#,0.00}", Convert.ToDecimal(sTextValue));
                    return false;
                }
            }
            return true;
        }

        public static string GetVarInterface(string sVar, string cmp, int divgroupoid = 0)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            var sSql = "";
            sSql = "SELECT TOP 1 imd.acctgcode FROM QL_mstinterface im INNER JOIN QL_mstinterfacedtl imd ON imd.interfaceoid=im.interfaceoid INNER JOIN QL_mstacctg a ON a.acctgoid=imd.acctgoid WHERE im.interfacevar='" + sVar + "' AND im.cmpcode='" + CompnyCode + "'";
            if (divgroupoid > 0)
            {
                sSql += " AND a.divgroupoid=" + divgroupoid + " ";
            }
            var sAcctgCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            return sAcctgCode;
        }

        public static Boolean IsStockAvailable(string cmp, string sPeriod, int iMatOid, int iWhoid, decimal dQty, string sType)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            var sSql = "";
            sSql = "SELECT COUNT(refoid) FROM QL_conmat WHERE cmpcode='" + cmp + "' AND refoid=" + iMatOid + " AND refname LIKE '" + sType + "%' AND mtrwhoid=" + iWhoid + " GROUP BY refoid HAVING SUM(qtyin-qtyout)>=" + dQty;
            var iStockQty = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (iStockQty > 0) { return true; } else { return false; }
        }
        public static bool IsStockAvailableWithBatch(string sCmpcode, DateTime sDate, int iMatOid, int iWHOid, Decimal dQty, string sType, string sn = "")
        {
            bool isExist = false;
            string sSql = "SELECT COUNT(-1) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + " AND mtrwhoid=" + iWHOid + " AND refno='" + sn + "' AND updtime<='" + sDate.ToString("MM/dd/yyyy") + " 23:59:59' AND refname LIKE '" + sType + "%' HAVING SUM(qtyin-qtyout) >= " + dQty + "";
            QL_RSAEntities db = new QL_RSAEntities();
            isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
            return isExist;
        }
        public static decimal GetStockValueWithBatch(string cmpcode, string refname, int refoid, string sn = "")
        {
            decimal value = 0;
            //string sSql = "SELECT ISNULL((SELECT TOP 1 (valueidr * (qtyin-qtyout)) / (qtyin-qtyout) FROM QL_conmat cx where cx.refoid = " + refoid + " AND cx.refno = '" + sn + "' AND cx.refname = '" + refname + "' ORDER BY updtime DESC),0.0)";
            string sSql = "SELECT ISNULL((SELECT TOP 1 valueidr FROM QL_conmat cx where cx.refoid = " + refoid + " AND cx.refno = '" + sn + "' AND cx.refname = '" + refname + "' AND qtyin > 0 ORDER BY updtime),0.0)";
            QL_RSAEntities db = new QL_RSAEntities();
            if (db.Database.SqlQuery<decimal>(sSql).FirstOrDefault() > 0)
            {
                value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            }
            return value;
        }
        public static bool IsStockAvailableWithSN(string sCmpcode, DateTime sDate, int iMatOid, int iWHOid, Decimal dQty, string sType, string refno = "", string serialno = "")
        {
            bool isExist = false;
            string sSql = "SELECT COUNT(-1) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + " AND mtrwhoid=" + iWHOid + " AND refno='" + refno + "' AND serialno='" + serialno + "' AND updtime<='" + sDate.ToString("MM/dd/yyyy") + " 23:59:59' AND refname LIKE '" + sType + "%' HAVING SUM(qtyin-qtyout) >= " + dQty + "";
            QL_RSAEntities db = new QL_RSAEntities();
            isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
            return isExist;
        }
        public static decimal GetStockValueWithSN(string cmpcode, string refname, int refoid, string refno = "", string serialno = "")
        {
            decimal value = 0;
            string sSql = "SELECT ISNULL((SELECT TOP 1 valueidr FROM QL_conmat cx where cx.refoid = " + refoid + " AND cx.refno = '" + refno + "' AND cx.serialno = '" + serialno + "' AND cx.refname = '" + refname + "' AND qtyin > 0 ORDER BY updtime),0.0)";
            QL_RSAEntities db = new QL_RSAEntities();
            if (db.Database.SqlQuery<decimal>(sSql).FirstOrDefault() > 0)
            {
                value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            }
            return value;
        }
        public static string GetQueryUpdateStockValue(decimal dQty, decimal dValIDR, decimal dValUSD, string sFormType, DateTime dLastDate, string sLastUpdUser, string sCmpCode, string sPeriod, int iRefOid, string sRefName, string sn = "", string expdate = "1/1/1900")
        {
            return "UPDATE QL_stockvalue SET stockqty=stockqty + " + dQty + ", stockvalueidr=ROUND(ISNULL((CAST(((stockvalueidr * stockqty) + " + dQty * dValIDR + ") AS FLOAT) / NULLIF((stockqty + " + dQty + "), 0)), 0), 4), stockvalueusd=ROUND(ISNULL((CAST(((stockvalueusd * stockqty) + " + dQty * dValUSD + ") AS FLOAT) / NULLIF((stockqty + " + dQty + "), 0)), 0), 6), lasttranstype='" + sFormType + "', lasttransdate='" + dLastDate + "', upduser='" + sLastUpdUser + "', updtime=CURRENT_TIMESTAMP, backupqty=stockqty, backupvalueidr=stockvalueidr, backupvalueusd=stockvalueusd WHERE cmpcode='" + sCmpCode + "' AND refoid=" + iRefOid + " AND refname='" + sRefName + "'";
        }

        public static string GetQueryInsertStockValue(decimal dQty, decimal dValIDR, decimal dValUSD, string sFormType, DateTime dLastDate, string sLastUpdUser, string sCmpCode, string sPeriod, int iRefOid, string sRefName, int iOid, string sn = "", string expdate = "1/1/1900", decimal stockAkhir = 0)
        {
            return "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime, backupqty, backupvalueidr, backupvalueusd, closeflag, flag_temp, refno) VALUES ('" + sCmpCode + "', " + iOid + ", '" + sPeriod + "', " + iRefOid + ", '" + sRefName + "', " + (stockAkhir + dQty) + ", " + dValIDR + ", " + dValUSD + ", '" + sFormType + "', '" + dLastDate + "', '', '" + sLastUpdUser + "', CURRENT_TIMESTAMP, 0, 0, 0, '', '', '" + sn + "')";
        }

        public static decimal GetStockValueIDR(QL_RSAEntities db,string cmpcode, string periodacctg, string refname, int refoid)
        {
            return db.Database.SqlQuery<decimal>("SELECT ISNULL((SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / NULLIF(SUM(ISNULL(stockqty, 0)), 0)), 0) FROM QL_stockvalue WHERE cmpcode='" + cmpcode + "' AND periodacctg IN ('" + periodacctg + "', '" + GetLastPeriod(periodacctg) + "') AND refoid=" + refoid + " AND refname='" + refname + "' AND closeflag=''").FirstOrDefault();
        }

        public static decimal GetStockValueUSD(string cmpcode, string periodacctg, string refname, int refoid)
        {
            return new QL_RSAEntities().Database.SqlQuery<decimal>("SELECT ISNULL((SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / NULLIF(SUM(ISNULL(stockqty, 0)), 0)), 0) FROM QL_stockvalue WHERE cmpcode='" + cmpcode + "' AND periodacctg IN ('" + periodacctg + "', '" + GetLastPeriod(periodacctg) + "') AND refoid=" + refoid + " AND refname='" + refname + "' AND closeflag=''").FirstOrDefault();
        }
        public static decimal GetStockValue(string cmpcode, string refname, int refoid, string sn = "", string expdate = "1/1/1900")
        {
            //QL_SELFIEEntities db = new QL_SELFIEEntities();
            //db.Database.CommandTimeout = 0;
            return new QL_RSAEntities().Database.SqlQuery<decimal>("SELECT ISNULL((SUM(ISNULL(qtyin - qtyout, 0) * ISNULL(valueidr, 0)) / NULLIF(SUM(ISNULL(qtyin - qtyout, 0)), 0)), 0) FROM QL_conmat WHERE cmpcode='" + cmpcode + "' AND refname = '" + refname + "' AND refoid=" + refoid + "" ).FirstOrDefault();
        }

        public static string GetQueryUpdateStockValue(decimal dQty, decimal dValIDR, decimal dValUSD, string sFormType, DateTime dLastDate, string sLastUpdUser, string sCmpCode, string sPeriod, int iRefOid, string sRefName)
        {
            return "UPDATE QL_stockvalue SET stockqty=stockqty + " + dQty + ", stockvalueidr=ROUND(ISNULL((CAST(((stockvalueidr * stockqty) + " + dQty * dValIDR + ") AS FLOAT) / NULLIF((stockqty + " + dQty + "), 0)), 0), 4), stockvalueusd=ROUND(ISNULL((CAST(((stockvalueusd * stockqty) + " + dQty * dValUSD + ") AS FLOAT) / NULLIF((stockqty + " + dQty + "), 0)), 0), 6), lasttranstype='" + sFormType + "', lasttransdate='" + dLastDate + "', upduser='" + sLastUpdUser + "', updtime=CURRENT_TIMESTAMP, backupqty=stockqty, backupvalueidr=stockvalueidr, backupvalueusd=stockvalueusd WHERE cmpcode='" + sCmpCode + "' AND refoid=" + iRefOid + " AND refname='" + sRefName + "'";
        }

        public static string GetQueryInsertStockValue(decimal dQty, decimal dValIDR, decimal dValUSD, string sFormType, DateTime dLastDate, string sLastUpdUser, string sCmpCode, string sPeriod, int iRefOid, string sRefName, int iOid)
        {
            return "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime, backupqty, backupvalueidr, backupvalueusd, closeflag, flag_temp) VALUES ('" + sCmpCode + "', " + iOid + ", '', " + iRefOid + ", '" + sRefName + "', " + dQty + ", " + dValIDR + ", " + dValUSD + ", '" + sFormType + "', '" + dLastDate + "', '', '" + sLastUpdUser + "', CURRENT_TIMESTAMP, 0, 0, 0, '', '')";
        }

        public static QL_conmat InsertConMat(string cmpcode, int conmtroid, string type, string formaction, int formoid, int refoid, string refname, int mtrwhoid, decimal qty, string reason, string note, string upduser, string refno, string refno2, decimal valueidr, decimal valueusd, int deptoid, string conres, int formdtloid,int divgroupoid, string sn, int cust_id = 0)
        {
            QL_conmat tbl = new QL_conmat();
            var typemin = qty > 0 ? 1 : -1;
            var updtime = GetServerTime();

            tbl.cmpcode = cmpcode;
            tbl.conmtroid = conmtroid;
            tbl.type = type;
            tbl.typemin = typemin;
            tbl.trndate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
            tbl.periodacctg = GetDateToPeriodAcctg(updtime);
            tbl.formaction = formaction;
            tbl.formoid = formoid;
            tbl.refoid = refoid;
            tbl.refname = refname;
            tbl.mtrwhoid = mtrwhoid;
            tbl.qtyin = qty >= 0 ? qty : 0;
            tbl.qtyout = qty < 0 ? qty * -1 : 0;
            tbl.reason = reason;
            tbl.note = note;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.refno = refno;
            tbl.refno2 = refno2;
            tbl.valueidr = valueidr;
            tbl.valueusd = valueusd;
            tbl.deptoid = deptoid;
            tbl.divgroupoid = divgroupoid;
            tbl.conres = conres;
            tbl.serialno=sn;
            tbl.formdtloid = formdtloid;
            tbl.cust_id = cust_id;
            //tbl.valueusd_backup = 0;
            return tbl;
        }
        public static QL_conmat InsertConMatWithBatch(string cmpcode, int conmtroid, string type, string formaction, int formoid, int refoid, string refname, int mtrwhoid, decimal qty, string reason, string note, string upduser, string refno, decimal valueidr, decimal valueusd, int deptoid, string conres, int formdtloid, int divgroupoid, string sn = "", string expdate = "1/1/1900",string refno2 = "", int cust_id = 0)
        {
            QL_conmat tbl = new QL_conmat();
            var typemin = qty > 0 ? 1 : -1;
            var updtime = GetServerTime();

            tbl.cmpcode = cmpcode;
            tbl.conmtroid = conmtroid;
            tbl.type = type;
            tbl.typemin = typemin;
            tbl.trndate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
            tbl.periodacctg = GetDateToPeriodAcctg(updtime);
            tbl.formaction = formaction;
            tbl.formoid = formoid;
            tbl.refoid = refoid;
            tbl.refname = refname;
            tbl.mtrwhoid = mtrwhoid;
            tbl.qtyin = qty >= 0 ? qty : 0;
            tbl.qtyout = qty < 0 ? qty * -1 : 0;
            tbl.reason = reason;
            tbl.note = note;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.refno = refno;
            tbl.refno2 = refno2;
            tbl.valueidr = valueidr;
            tbl.valueusd = valueusd;
            tbl.deptoid = deptoid;
            //tbl.conres = conres;
            //tbl.valueidr_backup = 0;
            //tbl.valueusd_backup = 0;
            tbl.formdtloid = formdtloid;
            tbl.divgroupoid = divgroupoid;
            tbl.serialno = sn;
            tbl.cust_id = cust_id;
            return tbl;
        }


        public static QL_stockvalue UpdateOrInsertStockValue(string cmpcode, int stockvalueoid, int refoid, string refname, decimal stockqty, decimal stockvalueidr, decimal stockvalueusd, string lasttranstype, string note, string upduser, out string status)
        {
            var updtime = GetServerTime();
            var periodacctg = GetDateToPeriodAcctg(updtime);
            QL_stockvalue tbl = new QL_RSAEntities().QL_stockvalue.Where(s => s.cmpcode == cmpcode && s.periodacctg == periodacctg && s.refoid == refoid && s.refname == refname).FirstOrDefault();

            if (tbl == null)
            {
                status = "Insert";
                tbl = new QL_stockvalue();
                tbl.cmpcode = cmpcode;
                tbl.stockvalueoid = stockvalueoid;
                tbl.periodacctg = periodacctg;
                tbl.refoid = refoid;
                tbl.refname = refname;
                tbl.stockqty = stockqty;
                tbl.stockvalueidr = stockvalueidr;
                tbl.stockvalueusd = stockvalueusd;
                tbl.lasttranstype = lasttranstype;
                tbl.lasttransdate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
                tbl.note = null;
                tbl.upduser = upduser;
                tbl.updtime = updtime;
                tbl.backupqty = 0;
                tbl.backupvalueidr = 0;
                tbl.backupvalueusd = 0;
                tbl.closeflag = "";
                tbl.flag_temp = "";
            }
            else
            {
                status = "Update";
                tbl.backupqty = tbl.stockqty;
                tbl.backupvalueidr = tbl.stockvalueidr;
                tbl.backupvalueusd = tbl.stockvalueusd;
                if (tbl.stockqty + stockqty == 0)
                {
                    tbl.stockvalueidr = 0;
                    tbl.stockvalueusd = 0;
                }
                else
                {
                    tbl.stockvalueidr = (tbl.stockvalueidr * tbl.stockqty + stockvalueidr * stockqty) / (tbl.stockqty + stockqty);
                    tbl.stockvalueusd = (tbl.stockvalueusd * tbl.stockqty + stockvalueusd * stockqty) / (tbl.stockqty + stockqty);
                }
                tbl.stockqty += stockqty;
                tbl.lasttranstype = lasttranstype;
                tbl.lasttransdate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
                tbl.upduser = upduser;
                tbl.updtime = updtime;
            }

            return tbl;
        }

        public static Boolean isPeriodAcctgClosed(string cmpcode, DateTime sDate)
        {
            QL_RSAEntities db = new QL_RSAEntities();
            string sPeriod = GetDateToPeriodAcctg(sDate);
            string sSql = "SELECT COUNT(*) FROM QL_crdgl WHERE cmpcode='" + cmpcode + "' AND crdglflag='CLOSED' AND periodacctg='" + sPeriod + "'";
            if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                return true;
            else
                return false;
        }

        public static QL_trnglmst InsertGLMst(string cmpcode, int glmstoid, DateTime gldate, string periodacctg, string glnote, string glflag, DateTime postdate, string createuser, DateTime createtime, string upduser, DateTime updtime, int rateoid, int rate2oid, decimal glrateidr, decimal glrate2idr, decimal glrateusd, decimal glrate2usd, int divgroupoid, string gl1side = "")
        {
            QL_trnglmst tbl = new QL_trnglmst();

            tbl.cmpcode = cmpcode;
            tbl.glmstoid = glmstoid;
            tbl.gldate = gldate;
            tbl.periodacctg = periodacctg;
            tbl.glnote = Tchar((glnote == null ? "" : glnote));
            tbl.glflag = glflag;
            tbl.postdate = postdate;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.type = "";
            tbl.rateoid = rateoid;
            tbl.rate2oid = rate2oid;
            tbl.glrateidr = glrateidr;
            tbl.glrateusd = glrateusd;
            tbl.glrate2idr = glrate2idr;
            tbl.glrate2usd = glrate2usd;
            tbl.glres = "";
            tbl.gl1side = "";
            tbl.divgroupoid = divgroupoid;
            return tbl;
        }

        public static QL_trngldtl InsertGLDtl(string cmpcode, int gldtloid, int glseq, int glmstoid, int acctgoid, string gldbcr, decimal glamt, string noref, string glnote, string glflag, string upduser, DateTime updtime, decimal glamtidr, decimal glamtusd, string glother1, string glother2, string glother3, string glother4, int groupoid,int divgroupoid)
        {
            QL_trngldtl tbldtl = new QL_trngldtl();

            tbldtl.cmpcode = cmpcode;
            tbldtl.gldtloid = gldtloid;
            tbldtl.glmstoid = glmstoid;
            tbldtl.glseq = glseq;
            tbldtl.acctgoid = acctgoid;
            tbldtl.gldbcr = gldbcr;
            tbldtl.glamt = glamt;
            tbldtl.noref = Tchar((noref == null ? "" : noref));
            tbldtl.glnote = Tchar((glnote == null ? "" : glnote));
            tbldtl.glother1 = glother1;
            tbldtl.glother2 = glother2;
            tbldtl.glother3 = glother3;
            tbldtl.glother4 = glother4;
            tbldtl.glflag = glflag;
            tbldtl.upduser = upduser;
            tbldtl.updtime = updtime;
            tbldtl.glamtidr = glamtidr;
            tbldtl.glamtusd = glamtusd;
            tbldtl.groupoid = Convert.ToInt32(groupoid);
            tbldtl.divgroupoid = divgroupoid;
            return tbldtl;
        }
        public static List<trngldtl> ShowCOAPosting(string sNoRef, string sCmpCode, string sRateType = "Default", string sOther = "")
        {
            QL_RSAEntities db = new QL_RSAEntities();
            List<trngldtl> tbl = new List<trngldtl>();
            var sSql = "";
            var sFieldAmt = "glamt";
            if (sRateType == "IDR")
                sFieldAmt = "glamtidr";
            else if (sRateType == "USD")
                sFieldAmt = "glamtusd";

            sSql = "SELECT * FROM (SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref='" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "' UNION ALL SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgcredit FROM QL_trngldtl_hist d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref='" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "') tbl_posting ORDER BY glseq";
            tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();

            if (tbl == null)
            {
                sSql = "SELECT * FROM (SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS accgtcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref LIKE '%" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "' UNION ALL SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS accgtcredit FROM QL_trngldtl_hist d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref LIKE '%" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "') tbl_posting ORDER BY glseq";
                tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();
            }
            return tbl;
        }

      

        public static QL_conar InsertConAR(string cmpcode, int conaroid, string reftype, int refoid, int payrefoid, int custoid, int acctgoid, string trnarstatus, string trnartype, DateTime trnardate, string periodacctg, int paymentacctgoid, DateTime paymentdate, string payrefno, int paybankoid, DateTime payduedate, decimal amttrans, decimal amtbayar, string trnarnote, string trnarres1, string trnarres2, string trnarres3, string createuser, DateTime createtime, string upduser, DateTime updtime, decimal amttransidr, decimal amtbayaridr, decimal amttransusd, decimal amtbayarusd, decimal amtbayarother, string conflag,int divgroupoid)

        {
            QL_conar tbl = new QL_conar();


            tbl.cmpcode = cmpcode;
            tbl.conaroid = conaroid;
            tbl.reftype = reftype;
            tbl.refoid = refoid;
            tbl.payrefoid = payrefoid;
            tbl.custoid = custoid;
            tbl.acctgoid = acctgoid;
            tbl.trnarstatus = trnarstatus;
            tbl.trnartype = trnartype;
            tbl.trnardate = DateTime.Parse(trnardate.ToString("MM/dd/yyyy")); ;
            tbl.periodacctg = periodacctg;
            tbl.paymentacctgoid = paymentacctgoid;
            tbl.paymentdate = DateTime.Parse(paymentdate.ToString("MM/dd/yyyy")); ;
            tbl.payrefno = payrefno;
            tbl.paybankoid = paybankoid;
            tbl.payduedate = DateTime.Parse(payduedate.ToString("MM/dd/yyyy")); ;
            tbl.amttrans = amttrans;
            tbl.amtbayar = amtbayar;
            tbl.trnarnote = trnarnote;
            tbl.trnarres1 = trnarres1;
            tbl.trnarres2 = trnarres2;
            tbl.trnarres3 = trnarres3;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.amttransidr = amttransidr;
            tbl.amtbayaridr = amtbayaridr;
            tbl.amttransusd = amttransusd;
            tbl.amtbayarusd = amtbayarusd;
            tbl.amtbayarother = amtbayarother;
            tbl.divgroupoid = divgroupoid;
            return tbl;
        }

       

        public static decimal GetAvgStockValue(string sCmpcode, int iMatOid, Decimal dQty, Decimal dVal, string flag)
        {
            decimal value = 0; decimal stockAkhir = 0; decimal valueAkhir = 0;
            if (flag == "IN")
            {
                string sSql = "SELECT ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + "),0.0) ";
                QL_RSAEntities db = new QL_RSAEntities();
                if (db.Database.SqlQuery<decimal>(sSql).FirstOrDefault() > 0)
                {
                    stockAkhir = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                string sSql2 = "SELECT ISNULL((SELECT TOP 1 valueidr / (qtyin+qtyout) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + " ORDER BY updtime DESC),0.0) ";
                QL_RSAEntities db2 = new QL_RSAEntities();
                if (db2.Database.SqlQuery<decimal>(sSql2).FirstOrDefault() > 0)
                {
                    valueAkhir = db2.Database.SqlQuery<decimal>(sSql2).FirstOrDefault();
                }
                if (stockAkhir > 0 & valueAkhir > 0 & dQty > 0 & dVal > 0)
                {
                    value = ((stockAkhir * valueAkhir) + (dQty * dVal)) / (stockAkhir + dQty);
                }
                else if (stockAkhir == 0)
                {
                    value = dVal;
                }
            }
            else
            {
                value = dVal;
            }
            return (value * dQty);
        }

        public static Dictionary<string, string> ConvertListofStringToDictionaryofString(List<string> param)
        {
            var result = new Dictionary<string, string>();
            foreach (var item in param)
                result.Add(item, item);
            return result;
        }

        public static string SplitTransNo(string input, string prefix, out string draft)
        {
            var result = ""; draft = "";
            var ar_input = input.Split(';');
            for (int i = 0; i < ar_input.Length; i++)
            {
                if (ar_input[i].Contains(prefix))
                    result += "'" + ar_input[i] + "', ";
                else
                    draft += ar_input[i] + ", ";
            }
            if (!string.IsNullOrEmpty(result)) result = ClassFunction.Left(result, result.Length - 2);
            if (!string.IsNullOrEmpty(draft)) draft = ClassFunction.Left(draft, draft.Length - 2);
            return result;
        }
        public static QL_conap InsertConAP(string cmpcode, int conapoid, string reftype, int refoid, int payrefoid, int suppoid, int acctgoid, string trnapstatus, string trnaptype, DateTime trnapdate, string periodacctg, int paymentacctgoid, DateTime paymentdate, string payrefno, int paybankoid, DateTime payduedate, decimal amttrans, decimal amtbayar, string trnapnote, string trnapres1, string trnapres2, string trnapres3, string createuser, DateTime createtime, string upduser, DateTime updtime, decimal amttransidr, decimal amtbayaridr, decimal amttransusd, decimal amtbayarusd, decimal amtbayarother,int divgroupoid, string conflag = "")

        {
            QL_conap tbl = new QL_conap();


            tbl.cmpcode = cmpcode;
            tbl.conapoid = conapoid;
            tbl.reftype = reftype;
            tbl.refoid = refoid;
            tbl.payrefoid = payrefoid;
            tbl.suppoid = suppoid;
            tbl.acctgoid = acctgoid;
            tbl.trnapstatus = trnapstatus;
            tbl.trnaptype = trnaptype;
            tbl.trnapdate = DateTime.Parse(trnapdate.ToString("MM/dd/yyyy")); ;
            tbl.periodacctg = periodacctg;
            tbl.paymentacctgoid = paymentacctgoid;
            tbl.paymentdate = DateTime.Parse(paymentdate.ToString("MM/dd/yyyy")); ;
            tbl.payrefno = payrefno;
            tbl.paybankoid = paybankoid;
            tbl.payduedate = DateTime.Parse(payduedate.ToString("MM/dd/yyyy")); ;
            tbl.amttrans = amttrans;
            tbl.amtbayar = amtbayar;
            tbl.trnapnote = trnapnote;
            tbl.trnapres1 = trnapres1;
            tbl.trnapres2 = trnapres2;
            tbl.trnapres3 = trnapres3;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.amttransidr = amttransidr;
            tbl.amtbayaridr = amtbayaridr;
            tbl.amttransusd = amttransusd;
            tbl.amtbayarusd = amtbayarusd;
            tbl.amtbayarother = amtbayarother;
            tbl.divgroupoid = divgroupoid;
            tbl.conflag = conflag;

            return tbl;
        }

        public const string FileExt_Report = ".exported-rpt";

        public static string AsTempPath(this string filenameNoExt, string ext = "")
        {
            return System.IO.Path.GetTempPath() + filenameNoExt + ext;
        }

        public static string getObjVal(object obj)
        {
            string result = "";
            if (obj.GetType() == typeof(string[]))
            {
                var arr = (string[])obj;
                if (arr != null && arr.Length > 0) result = arr[0];
            }
            else result = obj?.ToString() ?? "";
            return result;
        }

        public static int toInteger(this string val)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(val))
            {
                try { int.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static string LString(this string val, int length)
        {
            var r = "";
            if ((length <= 0)) return r;
            if ((length > val.Length)) length = val.Length;
            r = val.Substring(0, length);
            return r;
        }

        public static string RString(this string val, int length)
        {
            var r = "";
            if ((length <= 0)) return r;
            if ((length > val.Length)) length = val.Length;
            r = val.Substring((val.Length - length), length);
            return r;
        }

        public static decimal ToDecimal(this string val)
        {
            decimal result = 0;
            if (!string.IsNullOrEmpty(val))
            {
                try { Decimal.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static bool ToBoolean(this string val)
        {
            bool result = false;
            if (val.ToLower() == "true") result = true;
            return result;
        }

        public static DateTime ToDateTime(this string val)
        {
            DateTime result = new DateTime(1900, 1, 1);
            if (!string.IsNullOrEmpty(val))
            {
                try { DateTime.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static List<SelectListItem> GetBulan()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem { Text = "Januari", Value = "1" },
                new SelectListItem { Text = "Februari", Value = "2" },
                new SelectListItem { Text = "Maret", Value = "3" },
                new SelectListItem { Text = "April", Value = "4" },
                new SelectListItem { Text = "Mei", Value = "5" },
                new SelectListItem { Text = "Juni", Value = "6" },
                new SelectListItem { Text = "Juli", Value = "7" },
                new SelectListItem { Text = "Agustus", Value = "8" },
                new SelectListItem { Text = "September", Value = "9" },
                new SelectListItem { Text = "Oktober", Value = "10" },
                new SelectListItem { Text = "November", Value = "11" },
                new SelectListItem { Text = "Desember", Value = "12" },
            };
            return result;
        }

        public static List<SelectListItem> GetTahun(int start = 2023)
        {
            var result = new List<SelectListItem>();
            for (int i = start; i <= DateTime.Now.Year; i++)
            {
                result.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            return result;
        }

        public static Byte[] getImages(string sPath)
        {
            Byte[] arrImage = null;
            if (File.Exists(sPath))
            {
                Image img = Image.FromFile(sPath);
                MemoryStream ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
            return arrImage;
        }

        public static ReportDocument getReportDoc(ReportModels.PrintOutRequest request, ref string error, ref string err_query)
        {
            string query = string.Empty;
            var path_logo = $"~/Images/Integra-logo.png";
            ReportDocument report = new ReportDocument();
            try
            {
                report.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Report"), request.rpt_name + ".rpt"));
                if (request.view_param != null) query = $"select * from {request.view_param.view_name} where {request.view_param.view_filter}";
                else if (!string.IsNullOrEmpty(request.query)) query = request.query;
                else if (request.rpt_src != null)
                {
                    if (request.rpt_logo) foreach (DataRow item in request.rpt_src.Rows) item["company_img"] = getImages(HostingEnvironment.MapPath(path_logo));
                    report.SetDataSource(request.rpt_src);
                }
                if (request.sub_view_param != null && request.sub_view_param.Count > 0)
                {
                    request.query_sub = new List<string>();
                    foreach (var item in request.sub_view_param) request.query_sub.Add($"select * from {item.view_name} where {item.view_filter}");
                }
                if (!string.IsNullOrEmpty(query))
                {
                    err_query = query;
                    var dt = new SharedConnection().getDataTable(query, "data");
                    if (request.rpt_logo) foreach (DataRow item in dt.Rows) item["company_img"] = getImages(HostingEnvironment.MapPath(path_logo));
                    report.SetDataSource(dt);
                    query = string.Empty;
                }
                if (request.query_sub != null && request.query_sub.Count > 0)
                {
                    for (int i = 0; i < request.query_sub.Count; i++)
                    {
                        err_query = request.query_sub[i];
                        var dt = new SharedConnection().getDataTable(request.query_sub[i], "data");
                        report.Subreports[i].SetDataSource(dt);
                    }
                }
                else if (request.rpt_sub_src != null && request.rpt_sub_src.Count > 0) for (int i = 0; i < request.rpt_sub_src.Count; i++) report.Subreports[i].SetDataSource(request.rpt_sub_src[i]);
                if (request.rpt_param != null && request.rpt_param.Count > 0) foreach (var item in request.rpt_param) report.SetParameterValue(item.Key, item.Value);
                report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }
            return report;
        }

        public static ActionResult setPrintOut(ReportModels.PrintOutRequest request)
        {
            string error = string.Empty; string rpt_id = string.Empty; string err_query = string.Empty;
            var report = getReportDoc(request, ref error, ref err_query);
            var reportId = Guid.NewGuid();
            var saveAs = reportId.ToString().AsTempPath(ClassFunction.FileExt_Report);
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CrystalReport, saveAs);
            report.Dispose(); report.Close();
            var sdir = Path.GetTempPath();
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime < DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy 00:00:00")) && file.EndsWith(".exported-rpt")) File.Delete(file);
                }
            }
            rpt_id = reportId.ToString();
            return new JsonResult()
            {
                Data = new { rpt_id, error, rptname = request.file_name, err_query },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
            };
        }

        public static object MappingTable(object tbl_dest, object tbl_source)
        {
            foreach (var prop in tbl_dest.GetType().GetProperties())
            {
                var prop_source = tbl_source.GetType().GetProperties().Where(g => g.Name == prop.Name).FirstOrDefault();
                if (prop_source != null)
                {
                    var fieldvalue = prop_source.GetValue(tbl_source, null);
                    prop.SetValue(tbl_dest, fieldvalue);
                }
            }
            return tbl_dest;
        }
    }
}