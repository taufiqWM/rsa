﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class ARReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL()
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<string> MCBStatus = new List<string> { "In Process", "In Approval", "Approved", "Closed", "Cancel", "Rejected", "Revised" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult GetCustData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND custoid IN (SELECT custoid from QL_trnarrawmst arm LEFT JOIN QL_trnarrawdtl ard ON arm.arrawmstoid=ard.arrawmstoid WHERE arm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND custoid IN (SELECT custoid from QL_trnarrawmst arm LEFT JOIN QL_trnarrawdtl ard ON arm.arrawmstoid=ard.arrawmstoid WHERE arm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND arm.arrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += ") ORDER BY [Code]";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer, string DDLNomor, string TextNomor)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, som.sorawmstoid [Draft No.], som.sorawno [SO No.], CONVERT(varchar(10),som.sorawdate,101) [SO Date], s.custname [Customer], som.sorawmstnote [Note], som.sorawmststatus [Status] FROM QL_trnsorawmst som INNER JOIN QL_trnsorawdtl sod ON som.sorawmstoid=sod.sorawmstoid INNER JOIN QL_mstcust s ON som.custoid=s.custoid AND s.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE som.sorawmstoid IN (SELECT DISTINCT sorawmstoid FROM QL_trnarrawmst arm INNER JOIN QL_trnarrawdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid INNER JOIN QL_trndorawdtl dod ON shd.cmpcode=dod.cmpcode AND dod.dorawdtloid=shd.dorawdtloid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE som.sorawmstoid IN (SELECT DISTINCT sorawmstoid FROM QL_trnarrawmst arm INNER JOIN QL_trnarrawdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid INNER JOIN QL_trndorawdtl dod ON shd.cmpcode=dod.cmpcode AND dod.dorawdtloid=shd.dorawdtloid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    sSql += " AND c.custcode IN ( '" + TextCustomer.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND arm.arrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY [SO Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetShipData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer, string DDLType, string TextSO, string TextShipment, string DDLNomor, string TextNomor)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, shm.shipmentrawmstoid [Draft No.], shm.shipmentrawno [Shipment No.], CONVERT(varchar(10),shm.shipmentrawdate,101) [Shipment Date], c.custname [Customer], shm.shipmentrawmstnote [Note], shm.shipmentrawmststatus [Status] FROM QL_trnshipmentrawmst shm INNER JOIN QL_trnshipmentrawdtl shd ON shm.shipmentrawmstoid=shd.shipmentrawmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE shm.shipmentrawmstoid IN (SELECT DISTINCT ard.shipmentrawmstoid FROM QL_trnarrawmst arm INNER JOIN QL_trnarrawdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid  INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE shm.shipmentrawmstoid IN (SELECT DISTINCT ard.shipmentrawmstoid FROM QL_trnarrawmst arm INNER JOIN QL_trnarrawdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid  INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode LIKE '%' ";

                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    sSql += " AND c.custcode IN ( '" + TextCustomer.Replace(";", "','") + "')";
                }

                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextSO))
                    {
                        sSql += " AND som.sorawno IN ( '" + TextSO.Replace(";", "','") + "')";
                    }
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND arm.arrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY [Shipment Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblShip");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetARData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string TextCustomer, string DDLType, string TextSO, string TextShipment)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, arm.arrawmstoid [Draft No.], arm.arrawno [AR No.], CONVERT(varchar(10),arm.arrawdate,101) [AR Date], c.custname [Customer], arm.arrawmstnote [Note], arm.arrawmststatus [Status] FROM QL_trnarrawmst arm LEFT JOIN QL_trnarrawdtl ard ON arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE arm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE arm.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    sSql += " AND c.custcode IN ( '" + TextCustomer.Replace(";", "','") + "')";
                }

                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextSO))
                    {
                        sSql += " AND ard.shipmentrawdtloid IN (SELECT shd.shipmentrawdtloid FROM QL_trnshipmentrawdtl shd INNER JOIN QL_trndorawdtl dod ON dod.dorawdtloid=shd.dorawdtloid INNER JOIN QL_trnsorawmst som ON som.sorawmstoid=dod.sorawmstoid ";
                        sSql += " WHERE ";
                        sSql += " som.sorawno IN ( '" + TextSO.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                    if (!string.IsNullOrEmpty(TextShipment))
                    {
                        sSql += " AND ard.shipmentrawmstoid IN (SELECT shm.shipmentrawmstoid FROM QL_trnshipmentrawmst shm ";
                        sSql += " WHERE ";
                        sSql += " shm.shipmentrawno IN ( '" + TextShipment.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND arm.arrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += " ORDER BY [AR Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMatData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string TextCustomer, string TextSO, string DDLType, string TextShipment)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.matrawcode [Code], m.matrawlongdesc [Description], g2.gendesc [Unit] FROM QL_trnarrawdtl ard INNER JOIN QL_trnarrawmst arm ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=ard.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=ard.arrawunitoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE arm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE arm.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    sSql += " AND c.custcode IN ( '" + TextCustomer.Replace(";", "','") + "')";
                }

               if(DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextSO))
                    {
                        sSql += " AND ard.shipmentrawdtloid IN (SELECT shd.shipmentrawdtloid FROM QL_trnshipmentrawdtl shd INNER JOIN QL_trndorawdtl dod ON dod.dorawdtloid=shd.dorawdtloid INNER JOIN QL_trnsorawmst som ON som.sorawmstoid=dod.sorawmstoid ";
                        sSql += " WHERE ";
                        sSql += " som.sorawno IN ( '" + TextSO.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                    if (!string.IsNullOrEmpty(TextShipment))
                    {
                        sSql += " AND ard.shipmentrawmstoid IN (SELECT shm.shipmentrawmstoid FROM QL_trnshipmentrawmst shm ";
                        sSql += " WHERE ";
                        sSql += " shm.shipmentrawno IN ( '" + TextShipment.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Draft No.")
                        sSql += " AND arm.arrawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                    else
                        sSql += " AND arm.arrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND arm.arrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }




        // GET: ARReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Finish Good";
                    break;
                case "Service":
                    ViewBag.FormType = "Makloon";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderby, string ReportType, string DDLType, string TextCustomer, string TextSO, string TextShipment, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";

            if(DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptAR_ReportARNoXls.rpt";
                else
                    rptfile = "rptAR_ReportARNo.rpt";
                rptname = "ARRAWMATERIALSUMMARY";
            }
            else
            {
                if(ReportType == "XLS")
                    rptfile = "rptAR_ReportDtlARNoXls.rpt";
                else
                {
                    if (DDLGrouping == "arno")
                        rptfile = "rptAR_ReportDtlARNo.rpt";
                    else if (DDLGrouping == "shipmentno")
                        rptfile = "rptAR_ReportDtlShipmentNo.rpt";
                    else
                        rptfile = "rptAR_ReportDtlCust.rpt";
                }
                rptname = "ARRAWMATERIALDETAIL";
            }


            var Slct = ""; var Join = "";

            if (DDLType == "Detail"){
                Slct += ",ard.arrawdtloid [Dtl Oid], ard.arrawdtlseq [Seq], ard.matrawoid [Mat Oid], m.matrawcode [Mat Code] , m.matrawlongdesc [Mat Longdesc], arrawqty [Qty] , g2.gendesc [Unit], arrawprice [Price], arrawdtlamt [Detail Amount], arrawdtldisctype [Disc Type], arrawdtldiscvalue [Disc Value], arrawdtldiscamt [Disc Amount], arrawdtlnetto [Dtl Netto], arrawdtlnote [AR Dtl Note] ";
                Join += " INNER JOIN QL_trnarrawdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arrawmstoid=ard.arrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=ard.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=ard.arrawunitoid ";

                Slct += " , shm.shipmentrawmstoid [Shipment Oid],shipmentrawno [Shipment No.], shipmentrawdate [Shipment Date], shipmentrawvalueidr [PriceSJIDR], shipmentrawvalueusd [PriceSJUSD], shipmentrawcontno [Shipment Container No.], shm.shipmentrawportship [Shipment Port Ship], shm.shipmentrawportdischarge [Shipment Port Discharge], shm.shipmentrawetd [Shipment ETD], shm.shipmentraweta [Shipment ETA], ISNULL((SELECT drivname FROM QL_mstdriver dr WHERE dr.drivoid=shm.driv),'') [Shipment Driver], ISNULL((SELECT vhcdesc FROM QL_mstvehicle v WHERE v.vhcoid=shm.vhc),'') [Shipment Vehicle], shm.shipmentrawnopol [Shipment Police No.], shm.shipmentrawmststatus [Shipment Status], '' [Shipment SJ Pajak], shipmentrawmstnote [Shipment Header Note], shm.createuser [Shipment Create User], shm.createtime [Shipment Create Datetime], (shm.approvaluser) AS [Shipment User Approval] , (shm.approvaldatetime) AS [Shipment ApprovalDate] ";
                Join += " INNER JOIN QL_trnshipmentrawmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentrawmstoid=ard.shipmentrawmstoid INNER JOIN QL_trnshipmentrawdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentrawdtloid=ard.shipmentrawdtloid  ";              

                Slct += " ,som.sorawmstoid [SO ID], sorawno [SO No.], sorawdate [SO Date], ((SELECT gendesc FROM QL_mstgen WHERE gengroup='PAYMENT TYPE' AND genoid=sorawpaytypeoid)) [SO Payment Type] , (CASE som.isexcludetax WHEN 1 THEN 'Exclude Tax' ELSE 'Include Tax' END) [SO Price Type], sorawtype [SO Local/Exp], ISNULL(sorawcustref, '') AS [SO Direct Cust PO No.], sorawpaymethod AS [Payment Method], sorawetd [SO ETD], sorawinvoiceval [Invoice Value], ISNULL((SELECT b.bankname+' - '+a.accountno FROM QL_mstaccount a INNER JOIN QL_mstbank b ON b.bankoid = a.bankoid WHERE a.accountoid=som.accountoid),'') [SO Account No.], sorawportship [SO Port Of Ship], sorawportdischarge [SO Port Of Discharge], sorawmstnote [SO Header Note], sorawmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], som.approvaluser AS [App User SO], som.approvaldatetime AS [App Date SO], som.socustrefdate [SO Cust PO Date] ";
                Join += " INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=shm.cmpcode AND dod.dorawdtloid=shd.dorawmstoid INNER JOIN QL_trnsorawmst som ON som.cmpcode=shm.cmpcode AND som.sorawmstoid=dod.sorawmstoid ";
            }
                sSql = "SELECT arm.arrawmstoid AS [armstoid], (CASE arm.arrawno WHEN '' THEN CONVERT(VARCHAR(10), arm.arrawmstoid) ELSE arm.arrawno END) AS [Draft No.], arm.arrawno AS [AR No.], arm.arrawdate AS [AR Date], arm.arrawdatetakegiro [AR Date Take Giro], (select x.gendesc from ql_mstgen x where x.genoid=arm.divgroupoid) [Divisi], arm.arrawmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gendesc [Payment Type], curr.currcode [Currency], arm.arrawratetoidr [Rate IDR], cast(arm.arrawratetousd as Float) [Rate USD], arm.arrawtotalamt [AR Total Amt], arm.arrawtotaldisc [AR Total Disc], arm.arrawtotalnetto [Total Netto], arm.arrawtaxtype [Tax Type], arm.arrawtaxvalue [AR Tax Value], arm.arrawtaxamt [AR Tax Amount], arm.arrawgrandtotal [AR Grand Total], (SELECT div.divname FROM QL_mstdivision div WHERE arm.cmpcode = div.cmpcode) AS [BU Name], arm.arrawmstnote AS [Header Note], ISNULL(arm.arrawmstres3,'') AS [Invoice No], arm.createuser [Create User], arm.createtime [Create Time], (arm.approvaluser) AS [AR User Approval] , (arm.approvaldatetime) AS [AR ApprovalDate] " + Slct + " FROM QL_trnarrawmst arm  " + Join + "  INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_mstgen g ON g.genoid=arm.arrawpaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE arm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE arm.cmpcode LIKE '%%'";

            if (!string.IsNullOrEmpty(TextCustomer))
            {
                sSql += " AND c.custcode IN ( '" + TextCustomer.Replace(";", "','") + "')";
            }

            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextSO))
                    sSql += " AND som.sorawno IN ( '" + TextSO.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextShipment))
                    sSql += " AND shm.shipmentrawno IN ( '" + TextShipment.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND matrawcode IN ( '" + TextMaterial.Replace(";", "','") + "')";
            }

            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND arm.arrawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND arm.arrawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND arm.arrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND arm.divgroupoid=" + DDLdivgroupoid;
            }


            if (DDLType == "Summary")
            {
                if (DDLSorting == "arm.arrawno")
                    sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby;
                else if (DDLSorting == "arm.approvaldatetime")
                    sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.approvaldatetime " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                else
                    sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.arrawdatetakegiro " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
            }
            else
            {
                if (DDLGrouping == "arno")
                {
                    if (DDLSorting == "shm.shipmentrawno")
                        sSql += " ORDER BY arm.cmpcode ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby + ", shm.shipmentrawno ASC ";
                    else if (DDLSorting == "arm.approvaldatetime")
                        sSql += " ORDER BY arm.cmpcode ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby + ", arm.approvaldatetime " + DDLOrderby;
                    else
                        sSql += " ORDER BY arm.cmpcode ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby + ", arm.arrawdatetakegiro " + DDLOrderby;
                }
                else if(DDLGrouping == "shipmentno")
                {
                    if (DDLSorting == "arm.arrawno")
                        sSql += " ORDER BY arm.cmpcode ASC, shm.shipmentrawno ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby;
                    else if (DDLSorting == "arm.approvaldatetime")
                        sSql += " ORDER BY arm.cmpcode ASC, shm.shipmentrawno ASC, arm.approvaldatetime " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                    else
                        sSql += " ORDER BY arm.cmpcode ASC, shm.shipmentrawno ASC, arm.arrawdatetakegiro " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                }
                else
                {
                    if (DDLSorting == "arm.arrawno")
                        sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.arrawno " + DDLOrderby + " , arm.arrawmstoid " + DDLOrderby;
                    else if (DDLSorting == "shm.shipmentrawno")
                        sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, shm.shipmentrawno " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                    else if (DDLSorting == "arm.approvaldatetimee")
                        sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.approvaldatetime " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                    else
                        sSql += " ORDER BY arm.cmpcode ASC, c.custname ASC, arm.arrawdatetakegiro " + DDLOrderby + ", arm.arrawno ASC , arm.arrawmstoid ASC ";
                }
            }


            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintTerminalID", "".ToString());
            if(ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
				this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
				if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if(ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }

}