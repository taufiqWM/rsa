﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class StockRMReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = "RSA";
        private string sSql = "";

        private void InitDDL(QL_conmat tbl)
        {
            sSql = "SELECT * FROM QL_mstgen g WHERE gengroup='MATERIAL LOCATION' ORDER BY g.gendesc";
            var DDLWH = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.mtrwhoid);
            ViewBag.DDLWH = DDLWH;
        }

        private void InitDDL1(QL_mstmatraw tbl1)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1code", "cat1shortdesc", tbl1.cat1oid);
            ViewBag.cat1code = cat1code;

            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1oid='" + tbl1.cat1oid + "' AND activeflag='ACTIVE'";
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2code", "cat2shortdesc", tbl1.cat2oid);
            ViewBag.cat2code = cat2code;

            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "' AND cat1oid= '" + tbl1.cat1oid + "'  AND cat2oid= '" + tbl1.cat2oid + "' AND  activeflag='ACTIVE'";
            var cat3code = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3code", "cat3shortdesc", tbl1.cat3oid);
            ViewBag.cat3code = cat3code;

            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "' AND cat1oid= '" + tbl1.cat1oid + "'  AND cat2oid= '" + tbl1.cat2oid + "'   AND cat3oid= '" + tbl1.cat3oid + "' AND activeflag ='ACTIVE'";
            var cat4code = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4shortdesc", tbl1.cat4oid);
            ViewBag.cat4code = cat4code;
        }

        [HttpPost]
        public ActionResult Getcat2(string[] cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE'";

            if (cat1 !=null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string[] cat1, string[] cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE'";

            if(cat1 !=null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 !=null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }
            
            sSql += " ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat4(string[] cat1, string[] cat2, string[] cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            if (cat3 != null)
            {
                if (cat3.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3.Count(); i++)
                    {
                        stsval3 += "'" + cat3[i] + "',";
                    }
                    sSql += " AND cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstcat4>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] cat1, string[] cat2, string[] cat3, string[] cat4)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gendesc [Unit] FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=m.itemunitoid WHERE m.cmpcode='" + CompnyCode + "' ";

                if (cat1 !=null)
                {
                    if (cat1.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < cat1.Count(); i++)
                        {
                            stsval += "'" + cat1[i] + "',";
                        }
                        sSql += " AND cat1code IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (cat2 != null)
                {
                    if (cat2.Count() > 0)
                    {
                        string stsval1 = "";
                        for (int i = 0; i < cat2.Count(); i++)
                        {
                            stsval1 += "'" + cat2[i] + "',";
                        }
                        sSql += " AND cat2code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                    }
                } 
                
                if(cat3 != null)
                {
                    if (cat3.Count() > 0)
                    {
                        string stsval2 = "";
                        for (int i = 0; i < cat3.Count(); i++)
                        {
                            stsval2 += "'" + cat3[i] + "',";
                        }
                        sSql += " AND cat3code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                    }
                }

                if (cat4 != null)
                {
                    if (cat4.Count() > 0)
                    {
                        string stsval3 = "";
                        for (int i = 0; i < cat4.Count(); i++)
                        {
                            stsval3 += "'" + cat4[i] + "',";
                        }
                        sSql += " AND cat4code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                    }
                }
                
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_conmat tbl;
            tbl = new QL_conmat();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            
            QL_mstmatraw tbl1;
            tbl1 = new QL_mstmatraw();
            if (tbl1 == null)
                return HttpNotFound();
            InitDDL1(tbl1);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLWH, string StartPeriod, string EndPeriod, string DDLPeriod, string TextMaterial, string reporttype, string[] cat1code, string[] cat2code, string[] cat3code, string[] cat4code)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptStockSumPdf.rpt"; 
                rptname = "Stock_SUMMARY";
            }
            else
            {
                rptfile = "rptStockDtlPdf.rpt";
                rptname = "Stock_DETAIL";
            }

            sSql = " DECLARE @startdate AS DATETIME; DECLARE @enddate AS DATETIME; SET @startdate='" + StartPeriod + " 00:00:00'; SET @enddate='" + EndPeriod + " 23:23:23'";
            if (DDLType == "Summary")
            {
                sSql += " SELECT BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo) saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, SUM(totalvalue) totalvalue, unit FROM (";
            }

            sSql += " SELECT '" + CompnyName + "' BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo)saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, avgvalue, totalvalue, unit, refno, note, updtime, seq, t.cat1code, t.cat2code, t.cat3code, t.cat4code FROM ( SELECT con.cmpcode, con.refoid itemoid, i.itemcode, i.itemdesc, con.mtrwhoid, g.gendesc whdesc, 0.0 saldo, con.qtyin, con.qtyout, (CASE WHEN con.valueidr > 0 THEN(con.valueidr / (con.qtyin + con.qtyout)) ELSE 0.0 END) avgvalue, con.valueidr totalvalue, g2.gendesc unit, con.refno, con.note, con.updtime, 3 seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_conmat con INNER JOIN QL_mstgen g ON g.genoid = con.mtrwhoid INNER JOIN QL_mstitem i ON i.itemoid = con.refoid INNER JOIN QL_mstgen g2 ON g2.genoid = i.itemunitoid WHERE con.trndate >= @startdate AND con.trndate<=@enddate ";

            sSql += " UNION ALL  SELECT con.cmpcode, con.refoid itemoid, i.itemcode, i.itemdesc, con.mtrwhoid, g.gendesc whdesc, ISNULL(SUM(con.qtyin - qtyout), 0.0) saldo, 0.0 qtyin, 0.0 qtyout, 0.0 avgvalue, 0.0 totalvalue, g2.gendesc unit, '' refno, 'SALDO AWAL' note, '" + StartPeriod + "' updtime, 2 seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_conmat con INNER JOIN QL_mstgen g ON g.genoid = con.mtrwhoid INNER JOIN QL_mstitem i ON i.itemoid = con.refoid INNER JOIN QL_mstgen g2 ON g2.genoid = i.itemunitoid WHERE con.trndate < @startdate GROUP BY con.cmpcode, con.refoid, i.itemcode, i.itemdesc, con.mtrwhoid, g.gendesc, g2.gendesc, i.cat1code, i.cat2code, i.cat3code, i.cat4code ";
            sSql += " ) AS t WHERE t.cmpcode='"+ CompnyCode +"'";

            if (DDLWH != null)
            {
                if (DDLWH.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLWH.Count(); i++)
                    {
                        stsval += "'" + DDLWH[i] + "',";
                    }
                    sSql += " AND t.mtrwhoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextMaterial != "")
            {
                string[] arr = TextMaterial.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND t.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (cat1code !=null)
            {
                if (cat1code.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1code.Count(); i++)
                    {
                        stsval1 += "'" + cat1code[i] + "',";
                    }
                    sSql += " AND t.cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }
            
            if(cat2code != null)
            {
                if (cat2code.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2code.Count(); i++)
                    {
                        stsval2 += "'" + cat2code[i] + "',";
                    }
                    sSql += " AND t.cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }
            
            if(cat3code != null)
            {
                if (cat3code.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3code.Count(); i++)
                    {
                        stsval3 += "'" + cat3code[i] + "',";
                    }
                    sSql += " AND t.cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }
            
            if(cat4code != null)
            {
                if (cat4code.Count() > 0)
                {
                    string stsval4 = "";
                    for (int i = 0; i < cat4code.Count(); i++)
                    {
                        stsval4 += "'" + cat4code[i] + "',";
                    }
                    sSql += " AND t.cat4code IN (" + ClassFunction.Left(stsval4, stsval4.Length - 1) + ")";
                }
            }
            
            sSql += " GROUP BY t.cmpcode, t.itemoid, t.itemcode, t.itemdesc, t.mtrwhoid, t.whdesc, t.avgvalue, t.totalvalue, t.unit, t.refno, t.note, t.updtime, t.seq, t.cat1code, t.cat2code, t.cat3code, t.cat4code";
            if (DDLType == "Summary")
            {
                sSql += " ) AS t GROUP BY t.BU, t.cmpcode, t.itemoid, t.itemcode, t.itemdesc, t.mtrwhoid, t.whdesc, t.unit, t.cat1code, t.cat2code, t.cat3code, t.cat4code";
            }       
            if (DDLType == "Summary")
            {
                sSql += " ORDER BY t.itemdesc";
            }
            else
            {
                sSql += " ORDER BY t.itemdesc, t.updtime, t.seq";
            }           

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}