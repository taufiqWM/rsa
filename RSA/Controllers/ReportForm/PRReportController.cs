﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class PRReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        private string GetQueryInitDDLDepartment(string cmp)
        {
            var result = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmp + "' AND activeflag='ACTIVE' ORDER BY deptname";
            if (string.IsNullOrEmpty(cmp))
            {
                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'de.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstdept') AND name<>'deptname' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                result = "SELECT " + cols + ", (deptname + ' - ' + divname) deptname FROM QL_mstdept de INNER JOIN QL_mstdivision div ON div.cmpcode=de.cmpcode WHERE de.activeflag='ACTIVE' AND div.activeflag='ACTIVE' ORDER BY deptname";
            }
            return result;
        }

        private string GetQueryInitDDLUserPO(string cmp)
        {
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'p.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstprof') AND name<>'profname' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", profoid profname FROM QL_mstprof p WHERE profoid IN (SELECT DISTINCT createuser FROM QL_trnporawmst WHERE cmpcode IN ('" + CompnyCode + "', '" + cmp + "')) ORDER BY profoid";
            if (string.IsNullOrEmpty(cmp))
            {
                result = "SELECT " + cols + ", (p.profoid + ' - ' + divname) profname FROM QL_mstprof p INNER JOIN QL_mstdivision div ON div.cmpcode=p.cmpcode WHERE p.profoid IN (SELECT DISTINCT createuser FROM QL_trnporawmst) ORDER BY profname";
            }
            return result;
        }
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL(string sType = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            if (DDLBusinessUnit.Count() > 0)
            {
                var DDLDepartment = new SelectList(db.Database.SqlQuery<QL_mstdept>(GetQueryInitDDLDepartment("")).ToList(), "deptoid", "deptname");
                ViewBag.DDLDepartment = DDLDepartment;

                if (sType != "")
                {
                    var DDLUserPO = new SelectList(db.Database.SqlQuery<QL_mstprof>(GetQueryInitDDLUserPO("")).ToList(), "profoid", "profname");
                    ViewBag.DDLUserPO = DDLUserPO;
                }
            }
            ViewBag.DDLBusinessUnit = CompnyCode;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            tbl = db.Database.SqlQuery<QL_mstdept>(GetQueryInitDDLDepartment(cmp)).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLUserPO(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstprof> tbl = new List<QL_mstprof>();
            tbl = db.Database.SqlQuery<QL_mstprof>(GetQueryInitDDLUserPO(cmp)).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPRData(string FormType, string DDLBusinessUnit, string DDLGroup, string DDLMatFlag, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {
        

            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, prm.prrawmstoid [Draft No.], prm.prrawno [PR No.], CONVERT(VARCHAR(10), prm.prrawdate, 101) [PR Date], d.deptname [Division], prm.prrawmstnote [Note] FROM QL_prrawmst prm  INNER JOIN QL_mstdept d ON d.cmpcode=prm.cmpcode AND d.deptoid=prm.deptoid ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE prm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE prm.cmpcode LIKE '%'";
                //if (!string.IsNullOrEmpty(DDLGroup))
                //    sSql += " AND prm.groupoid=" + DDLGroup + "";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND prrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextQty))
                {
                    if (DDLQty == "PR Qty")
                        sSql += " AND prd.prrawqty " + DDLCursorQty + " " + TextQty;
                }
               

                var objtype = new ReportModels.FullFormType(FormType);
                if (DDLMatFlag == "service")
                    sSql = sSql.Replace("raw", "service");

                sSql += " ORDER BY [Draft No.]";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit, string DDLGroup, string DDLMatFlag, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {


            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, matrawcode [Code], matrawlongdesc [Description], gendesc [Unit] FROM QL_prrawdtl prd INNER JOIN QL_prrawmst prm ON prm.cmpcode=prd.cmpcode AND prm.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid INNER JOIN QL_mstgen g ON genoid=prrawunitoid";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE prm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE prm.cmpcode LIKE '%'";
                if (!string.IsNullOrEmpty(DDLGroup))
                    sSql += " AND prm.groupoid=" + DDLGroup + "";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND prrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Draft No.")
                        sSql += " AND prm.prrawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                    else
                        sSql += " AND prrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(TextQty))
                {
                    if (DDLQty == "PR Qty")
                        sSql += " AND prd.prrawqty " + DDLCursorQty + " " + TextQty;
                }

                
                if (DDLMatFlag == "service")
                    sSql = sSql.Replace("matraw", "service").Replace("raw", "service").Replace("prd.serviceoid","prd.matserviceoid");


                sSql += " ORDER BY [Code]";

			var objtype = new ReportModels.FullFormType(FormType);
            DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");            	
            if (tbl.Rows.Count > 0)
            {
                int i = 1;
                Dictionary<string, object> row;
                foreach (DataRow dr in tbl.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns)
                    {
                        var item = dr[col].ToString();
                        if (col.ColumnName == "seq")
                            item = (i++).ToString();
                        row.Add(col.ColumnName, item);
                        if (!tblcols.Contains(col.ColumnName))
                            tblcols.Add(col.ColumnName);
                    }
                    tblrows.Add(row);
                }
            }
			else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    ViewBag.MatType = "raw";
                    ViewBag.MatRefType = "matraw";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    ViewBag.MatType = "gen";
                    ViewBag.MatRefType = "matgen";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    ViewBag.MatType = "sp";
                    ViewBag.MatRefType = "sparepart";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Obat Jadi";
                    ViewBag.MatType = "item";
                    ViewBag.MatRefType = "item";
                    break;
                default:
                    ViewBag.FormType = "";
                    ViewBag.MatType = "";
                    ViewBag.MatRefType = "";
                    break;
            }

            InitDDL();
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLDepartment, string DDLType, string DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLSortDir, string reporttype, string MatType, string MatRefType, string FormType, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            if (DDLBusinessUnit == "")
                DDLBusinessUnit = "All";

            if (DDLDepartment == "")
                DDLDepartment = "All";

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptPR_SumXls.rpt";
                else
                    rptfile = "rptPR_SumPdf.rpt";
                rptname = "PR" + FormType.ToUpper().Replace(" ", "") + "SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptPR_DtlXls.rpt";
                else
                {
                    if (DDLGrouping == "PR No.")
                        rptfile = "rptPR_DtlPRPdf.rpt";
                    else if (DDLGrouping == "Material Code")
                        rptfile = "rptPR_DtlMatCodePdf.rpt";
                    else
                        rptfile = "rptPR_DtlETAPdf.rpt";
                }
                rptname = "PR" + FormType.ToUpper().Replace(" ", "") + "DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , prd.pr" + MatType + "dtloid AS [Dtl ID], prd.pr" + MatType + "arrdatereq AS [ETA], m." + MatRefType + "code AS [Mat Code], m." + MatRefType + "longdesc AS [Mat Desc.], prd.pr" + MatType + "qty AS [PR Qty], 0 [Require Qty], g.gendesc AS [PR unit], prd.pr" + MatType + "dtlnote AS [Detail Note] ";
                Join = " INNER JOIN QL_pr" + MatType + "dtl prd ON prd.cmpcode=prm.cmpcode AND prd.pr" + MatType + "mstoid=prm.pr" + MatType + "mstoid INNER JOIN QL_mst" + MatRefType + " m ON  m." + MatRefType + "oid=prd." + MatRefType + "oid INNER JOIN QL_mstgen g ON g.genoid=prd.pr" + MatType + "unitoid ";
            }
            sSql = "SELECT prm.pr" + MatType + "mstoid AS [ID], (CASE prm.pr" + MatType + "no WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), prm.pr" + MatType + "mstoid) ELSE prm.pr" + MatType + "no END) AS [Draft/PR No], prm.pr" + MatType + "no AS [PR No.], prm.pr" + MatType + "date AS [PR Date], (select x.gendesc from ql_mstgen x where x.genoid=prm.divgroupoid) [Divisi],(SELECT div.divname FROM QL_mstdivision div WHERE prm.cmpcode = div.cmpcode) AS [BU], prm.deptoid AS [DeptID], (SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=prm.deptoid) AS [Dept Name], prm.pr" + MatType + "expdate AS [Expired Date], prm.pr" + MatType + "mststatus AS [PR Status], prm.cmpcode [Cmp Code], prm.pr" + MatType + "mstnote AS [Header Note], prm.approvaluser AS [App User], prm.approvaldatetime AS [App Date], prm.createuser AS [Create User], prm.createtime AS [Create Date], prm.closereason, prm.closeuser, prm.closetime " + Dtl + " FROM QL_pr" + MatType + "mst prm  " + Join + "";
            if (DDLBusinessUnit != "All")
                sSql += " WHERE prm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE prm.cmpcode LIKE '%'";
            if (DDLDepartment != "All")
                sSql += " AND prm.deptoid=" + DDLDepartment + "";
            if (DDLStatus != "All")
            {
                if (DDLStatus == "In Process")
                    sSql += " AND pr" + MatType + "mststatus IN ('In Process', 'In Approval', 'Revised')";
                else if (DDLStatus == "Approved")
                    sSql += " AND pr" + MatType + "mststatus IN ('Approved', 'Closed')";
                else
                    sSql += " AND pr" + MatType + "mststatus IN ('Rejected', 'Cancel')";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND prm.divgroupoid=" + DDLdivgroupoid;
            }
            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (TextNomor != "")
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND prm.pr" + MatType + "mstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND pr" + MatType + "no IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    sSql += " AND " + MatRefType + "code IN ('" + TextMaterial.Replace(";", "','") + "')";
                }
            }
            if (DDLType == "Summary")
            {
                if (DDLSorting == "prm.pr" + MatType + "no")
                    sSql += " ORDER BY prm.cmpcode ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                else
                    sSql += " ORDER BY prm.cmpcode ASC, " + DDLSorting + " " + DDLSortDir;
            }
            else
            {
                if (DDLGrouping == "prm.pr" + MatType + "no")
                    sSql += " ORDER BY prm.cmpcode ASC, prm.pr" + MatType + "no ASC, prm.pr" + MatType + "mstoid ASC, " + DDLSorting + " " + DDLSortDir;
                else if (DDLGrouping == "m." + MatRefType + "code")
                {
                    if (DDLSorting == "prm.pr" + MatType + "no")
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                    else
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir;
                }
                else
                {
                    if (DDLSorting == "prm.pr" + MatType + "no")
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                    else
                        sSql += " ORDER BY prm.cmpcode ASC, prd.pr" + MatType + "arrdatereq ASC, " + DDLSorting + " " + DDLSortDir;
                }
            }
            
            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        // GET: PRReport/ReportInPrice/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    ViewBag.MatType = "raw";
                    ViewBag.MatRefType = "matraw";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    ViewBag.MatType = "gen";
                    ViewBag.MatRefType = "matgen";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    ViewBag.MatType = "sp";
                    ViewBag.MatRefType = "sparepart";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Finish Good";
                    ViewBag.MatType = "item";
                    ViewBag.MatRefType = "item";
                    break;
                default:
                    ViewBag.FormType = "";
                    ViewBag.MatType = "";
                    ViewBag.MatRefType = "";
                    break;
            }

            InitDDL();
            return View();
        }

      
        // GET: PRReport/ReportStatus/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult ReportStatus(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    ViewBag.MatType = "raw";
                    ViewBag.MatRefType = "matraw";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    ViewBag.MatType = "gen";
                    ViewBag.MatRefType = "matgen";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    ViewBag.MatType = "sp";
                    ViewBag.MatRefType = "sparepart";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Finish Good";
                    ViewBag.MatType = "item";
                    ViewBag.MatRefType = "item";
                    break;
                default:
                    ViewBag.FormType = "";
                    ViewBag.MatType = "";
                    ViewBag.MatRefType = "";
                    break;
            }

            InitDDL(sType: "Status");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReportStatus(string DDLBusinessUnit, string DDLDepartment, string DDLType, string DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLSortDir, string reporttype, string MatType, string MatRefType, string FormType, string DDLTypeQty, string DDLCursorQty, string TextQty, string DDLUserPO, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            if (DDLBusinessUnit == "")
                DDLBusinessUnit = "All";

            if (DDLDepartment == "")
                DDLDepartment = "All";

            if (TextQty == "")
                TextQty = "0";

            if (DDLUserPO == "")
                DDLUserPO = "All";

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptPRStatusXls.rpt";
                else
                {
                    if (DDLGrouping == "prm.pr" + MatType + "no")
                        rptfile = "rptPRStatusPRNoPdf.rpt";
                    else if (DDLGrouping == "m." + MatRefType + "code")
                        rptfile = "rptPRStatusMatCodePdf.rpt";
                    else
                        rptfile = "rptPRStatusDeptPdf.rpt";
                }
                rptname = "PR" + FormType.ToUpper().Replace(" ", "") + "STATUSSUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptPRStatusDtlXls.rpt";
                else
                {
                    if (DDLGrouping == "prm.pr" + MatType + "no")
                        rptfile = "rptPRStatusDtlPRNoPdf.rpt";
                    else if (DDLGrouping == "m." + MatRefType + "code")
                        rptfile = "rptPRStatusDtlMatCodePdf.rpt";
                    else if (DDLGrouping == "pom.createuser")
                        rptfile = "rptPRStatusDtlPOCreatePdf.rpt";
                    else
                        rptfile = "rptPRStatusDtlDeptPdf.rpt";
                }
                rptname = "PR" + FormType.ToUpper().Replace(" ", "") + "STATUSDETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , ISNULL(POQtyAwal,0) POQtyAwal , ISNULL(poqty,0) poqty, ISNULL(pono,'') pono,  ISNULL (podate,CAST('01/01/1900' AS datetime)) AS podate, ISNULL(TotalPOQty,0) TotalPOQty,  ISNULL(POcreateuser,'') AS POcreateuser  , ISNULL (POCreateDate,CAST('01/01/1900' AS datetime)) AS POCreateDate, ISNULL(poQtyClosing,0) poQtyClosing, ISNULL(poQtyCancel,0) poQtyCancel ";
                Join = " LEFT JOIN (SELECT cmpcode, podtloid,matoid, mstoid,dtloid,POQtyAwal,poqty, closeqty AS poQtyClosing, pono, podate, POcreateuser, POCreateDate  FROM (SELECT pod.cmpcode, pod.po" + MatType + "dtloid podtloid,pod." + MatRefType + "oid matoid,pod.pr" + MatType + "mstoid mstoid,pod.pr" + MatType + "dtloid dtloid, pod.po" + MatType + "qty AS POQtyAwal, pod.po" + MatType + "qty AS poqty, pod.closeqty, pom.po" + MatType + "no AS pono, pom.po" + MatType + "date AS podate, pom.createuser AS POcreateuser, pom.createtime POCreateDate  FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" + MatType + "mstoid=pod.po" + MatType + "mstoid WHERE pom.po" + MatType + "mststatus IN ('Approved','Closed')) AS tbltemp) AS tblpod ON tblpod.cmpcode=prd.cmpcode AND tblpod.mstoid=prd.pr" + MatType + "mstoid AND tblpod.matoid=prd." + MatRefType + "oid AND tblpod.dtloid=prd.pr" + MatType + "dtloid ";
                Join += " LEFT JOIN (SELECT cmpcode,matoid, mstoid,dtloid, SUM(poqty) as poQtyCancel  FROM (SELECT pod.cmpcode, pod.po" + MatType + "mstoid AS pomstoid, pod.po" + MatType + "dtloid podtloid,pod." + MatRefType + "oid matoid,pod.pr" + MatType + "mstoid mstoid,pod.pr" + MatType + "dtloid dtloid, pod.po" + MatType + "qty AS POQtyAwal, (CASE WHEN ISNULL(pod.closeqty,0)=0 THEN pod.po" + MatType + "qty ELSE (ISNULL(pod.po" + MatType + "qty,0)-ISNULL(pod.closeqty,0)) END) AS poqty, pom.po" + MatType + "no AS pono, pom.po" + MatType + "date AS podate, pom.createuser AS POcreateuser, pom.createtime POCreateDate  FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pom.cmpcode=pod.cmpcode AND pom.po" + MatType + "mstoid=pod.po" + MatType + "mstoid WHERE pom.po" + MatType + "mststatus IN ('Cancel')) AS tbltemp GROUP BY cmpcode,matoid,mstoid,dtloid) AS tblpoc ON tblpoc.cmpcode=prd.cmpcode  AND tblpoc.matoid=prd." + MatRefType + "oid AND tblpoc.mstoid=prd.pr" + MatType + "mstoid AND tblpoc.dtloid=prd.pr" + MatType + "dtloid ";
                Join += "  LEFT JOIN (SELECT cmpcode, pr" + MatType + "mstoid, pr" + MatType + "dtloid, 0.0 TotalPOQty FROM QL_trnpo" + MatType + "dtl pod WHERE pod.po" + MatType + "mstoid IN (SELECT x.po" + MatType + "mstoid FROM QL_trnpo" + MatType + "mst x WHERE x.po" + MatType + "mststatus IN ('Approved','Closed')) GROUP BY cmpcode, pr" + MatType + "mstoid ,pr" + MatType + "dtloid) AS tblqty ON tblqty.cmpcode=prd.cmpcode AND tblqty.pr" + MatType + "mstoid=prd.pr" + MatType + "mstoid AND tblqty.pr" + MatType + "dtloid=prd.pr" + MatType + "dtloid ";
            }
            else
            {
                Dtl = " , (SELECT ISNULL(SUM(poqty),0) FROM (SELECT pod.po" + MatType + "qty AS poqty FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pod.po" + MatType + "mstoid=pom.po" + MatType + "mstoid WHERE pod.cmpcode=prd.cmpcode AND pod." + MatRefType + "oid=prd." + MatRefType + "oid AND pod.pr" + MatType + "dtloid=prd.pr" + MatType + "dtloid AND pom.po" + MatType + "mststatus IN ('Approved','Closed')) AS tbltemp) + (SELECT ISNULL(SUM(pod2.posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 WHERE pod2.posubconmstoid IN (SELECT x.posubconmstoid FROM QL_trnposubconmst x WHERE x.posubconmststatus IN ('Approved','Closed') AND x.posubconref='" + MatType + "') AND pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pr" + MatType + "dtloid) AS TotalPOQty ";
                // PO Qty Awal
                Dtl += " , (SELECT ISNULL(SUM(poqty),0) FROM (SELECT pod.po" + MatType + "qty AS poqty FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pod.po" + MatType + "mstoid=pom.po" + MatType + "mstoid WHERE pod.cmpcode=prd.cmpcode AND pod." + MatRefType + "oid=prd." + MatRefType + "oid AND pod.pr" + MatType + "dtloid=prd.pr" + MatType + "dtloid AND pom.po" + MatType + "mststatus IN ('Approved','Closed')) AS tbltemp) + (SELECT ISNULL(SUM(pod2.posubcondtl2qty),0) FROM QL_trnposubcondtl2 pod2 WHERE pod2.posubconmstoid IN (SELECT x.posubconmstoid FROM QL_trnposubconmst x WHERE x.posubconmststatus IN ('Approved','Closed') AND x.posubconref='" + MatType + "') AND pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pr" + MatType + "dtloid) AS POQtyAwal ";
                Dtl += " , (SELECT ISNULL(SUM(pod.closeqty),0) FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pod.po" + MatType + "mstoid=pom.po" + MatType + "mstoid WHERE pod.cmpcode=prd.cmpcode AND pod." + MatRefType + "oid=prd." + MatRefType + "oid AND pod.pr" + MatType + "dtloid=prd.pr" + MatType + "dtloid AND pom.po" + MatType + "mststatus IN ('Approved','Closed')) /*+ (SELECT ISNULL(SUM(pod2.closeqty),0) FROM QL_trnposubcondtl2 pod2 WHERE pod2.posubconmstoid IN (SELECT x.posubconmstoid FROM QL_trnposubconmst x WHERE x.posubconmststatus IN ('Approved','Closed') AND x.posubconref='" + MatType + "') AND pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pr" + MatType + "dtloid)*/ AS poQtyClosing";
                Dtl += " , (SELECT ISNULL(SUM(pod.closeqty),0) FROM QL_trnpo" + MatType + "dtl pod INNER JOIN QL_trnpo" + MatType + "mst pom ON pod.po" + MatType + "mstoid=pom.po" + MatType + "mstoid WHERE pod.cmpcode=prd.cmpcode AND pod." + MatRefType + "oid=prd." + MatRefType + "oid AND pod.pr" + MatType + "dtloid=prd.pr" + MatType + "dtloid AND pom.po" + MatType + "mststatus IN ('Cancel')) /*+ (SELECT ISNULL(SUM(pod2.closeqty),0) FROM QL_trnposubcondtl2 pod2 WHERE pod2.posubconmstoid IN (SELECT x.posubconmstoid FROM QL_trnposubconmst x WHERE x.posubconmststatus IN ('Cancel') AND x.posubconref='" + MatType + "') AND pod2.cmpcode=prd.cmpcode AND pod2.prsubcondtloid=prd.pr" + MatType + "dtloid)*/ AS poQtyCancel";
            }

            sSql = "SELECT prm.pr" + MatType + "mstoid AS prmstoid, prd.pr" + MatType + "dtloid AS prdtloid, ('Draft No. ' + CONVERT(VARCHAR(10), prm.pr" + MatType + "mstoid)) AS draftno, (select x.gendesc from ql_mstgen x where x.genoid=prm.divgroupoid) [Divisi], prm.pr" + MatType + "no AS prno, prd.pr" + MatType + "arrdatereq AS prarrdatereq, prm.pr" + MatType + "date AS prdate, prm.pr" + MatType + "mststatus AS prmststatus, m." + MatRefType + "code AS matcode, m." + MatRefType + "longdesc AS matlongdesc, prd.pr" + MatType + "qty AS prqty, prm.cmpcode, prm.deptoid, d.deptname " + Dtl + ", 0.0 AS balanceqty, ISNULL(prd.closeqty,0) AS PRClosingQty, g.gendesc AS prunit, (SELECT div.divname FROM QL_mstdivision div WHERE prm.cmpcode = div.cmpcode) AS divname, prd.pr" + MatType + "dtlnote AS prdtlnote, prm.pr" + MatType + "mstnote AS prmstnote, prm.approvaluser, prm.approvaldatetime, prm.createuser, prm.createtime, prm.closereason, prm.closeuser, prm.closetime FROM QL_pr" + MatType + "mst prm INNER JOIN QL_pr" + MatType + "dtl prd ON prd.cmpcode=prm.cmpcode AND prd.pr" + MatType + "mstoid=prm.pr" + MatType + "mstoid " + Join + " INNER JOIN QL_mst" + MatRefType + " m ON  m." + MatRefType + "oid=prd." + MatRefType + "oid INNER JOIN QL_mstgen g ON g.genoid=prd.pr" + MatType + "unitoid INNER JOIN QL_mstdept d ON d.deptoid=prm.deptoid ";

            if (DDLBusinessUnit != "All")
                sSql += " WHERE prm.cmpcode='" + CompnyCode + "'";
            else
                sSql += " WHERE prm.cmpcode LIKE '%'";

            if (DDLDepartment != "All")
                sSql += " AND prm.deptoid=" + DDLDepartment + "";
            if (DDLStatus != "All")
            {
                if (DDLStatus == "In Process")
                    sSql += " AND pr" + MatType + "mststatus IN ('In Process', 'In Approval', 'Revised')";
                else if (DDLStatus == "Approved")
                    sSql += " AND pr" + MatType + "mststatus IN ('Approved', 'Closed')";
                else
                    sSql += " AND pr" + MatType + "mststatus IN ('Rejected', 'Cancel')";
            }
            if (StartPeriod != "" && EndPeriod != "")
            {
                DDLPeriod = DDLPeriod.Replace(" ", MatType);
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND prm.divgroupoid=" + DDLdivgroupoid;
            }
            if (TextNomor != "")
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND prm.pr" + MatType + "mstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND pr" + MatType + "no IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (DDLType == "Detail")
            {
                if (DDLUserPO != "All")
                {
                    sSql += " AND POcreateuser='" + DDLUserPO + "' ";
                }
            }
            if (TextMaterial != "")
            {
                sSql += " AND " + MatRefType + "code IN ('" + TextMaterial.Replace(";", "','") + "')";
            }
            if (DDLType == "Summary")
            {
                if (DDLSorting == "prm.pr" + MatType + "no")
                    sSql += " ORDER BY prm.cmpcode ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                else
                    sSql += " ORDER BY prm.cmpcode ASC, " + DDLSorting + " " + DDLSortDir;
            }
            else
            {
                if (DDLGrouping == "prm.pr" + MatType + "no")
                    sSql += " ORDER BY prm.cmpcode ASC, " + DDLSorting + " " + DDLSortDir + ", prm.pr" + MatType + "mstoid ASC";
                else if (DDLGrouping == "m." + MatRefType + "code")
                {
                    if (DDLSorting == "prm.pr" + MatType + "no")
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                    else
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir;
                }
                else
                {
                    if (DDLSorting == "prm.pr" + MatType + "no")
                        sSql += " ORDER BY prm.cmpcode ASC, m." + MatRefType + "code ASC, " + DDLSorting + " " + DDLSortDir + " , prm.pr" + MatType + "mstoid " + DDLSortDir;
                    else
                        sSql += " ORDER BY prm.cmpcode ASC, prd.pr" + MatType + "arrdatereq ASC, " + DDLSorting + " " + DDLSortDir;
                }
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}