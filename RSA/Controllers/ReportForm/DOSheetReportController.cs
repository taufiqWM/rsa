﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class DOSheetReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL(string rpttype = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");

            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<string> MCBStatus = new List<string> { "In Process", "Post", "Closed", "Cancel" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult GetCustomerData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = " SELECT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
                //SELECT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND custoid IN (SELECT custoid from QL_trnsorawmst som INNER JOIN QL_trnsorawdtl sod ON som.sorawmstoid=sod.sorawmstoid 
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND custoid IN (SELECT custoid from QL_trnDOrawmst dom WHERE dom.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND custoid IN (SELECT custoid from QL_trnDOrawmst dom WHERE dom.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND dom.dorawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY custcode";
                
                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDOData(string FormType, string DDLBusinessUnit, string DDLNo, string DDLType, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust,string TextSO,string TextDO, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = " SELECT DISTINCT 0 seq, dom.DOrawmstoid [Draft No.], dom.DOrawno [DO No.], dom.DOrawdate [DO Date], c.custname [Customer], dom.DOrawmstnote [Note], dom.DOrawmststatus [Status] FROM QL_trnDOrawmst dom INNER JOIN QL_trnDOrawdtl dod ON dom.DOrawmstoid=dod.DOrawmstoid LEFT JOIN QL_trnSOrawmst som ON som.SOrawmstoid=dod.SOrawmstoid AND som.cmpcode=dom.cmpcode INNER JOIN QL_mstcust c ON dom.custoid=c.custoid AND c.activeflag='ACTIVE'";
                //if (DDLType == "Detail")
                //{
                //    if (!string.IsNullOrEmpty(TextSO))
                //    {
                //        sSql += " AND (";
                //        sSql += " som.SOrawno LIKE '%'(" + TextSO.Replace(";", ",") + ")";
                //        sSql += " OR";
                //        sSql += " )";
                //    }
                //}
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE dom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE dom.cmpcode LIKE '%'";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND dom.dorawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextSO))
                    sSql += "AND som.SOrawno IN ('" + TextSO.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextDO))
                {
                    if (DDLNo == "DO No.")
                        sSql += " AND dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "')";
                    else
                        sSql += " AND dom.DOrawmstoid IN (" + TextDO.Replace(";", ",") + ")";
                }

                sSql += " ORDER BY dom.DOrawdate DESC, dom.DOrawmstoid DESC";
                
                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblDO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust,string TextSO,string TextDO, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = " SELECT DISTINCT 0 seq, som.SOrawno [No], som.SOrawdate [Date], som.SOrawmststatus [Status], som.SOrawmstnote [Note] FROM QL_trnSOrawmst som";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE som.SOrawmstoid IN (SELECT dod.SOrawmstoid from QL_trnDOrawmst dom INNER JOIN QL_trnDOrawdtl dod ON dom.DOrawmstoid=dod.DOrawmstoid INNER JOIN QL_mstcust c ON dom.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE dom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE som.SOrawmstoid IN (SELECT dod.SOrawmstoid from QL_trnDOrawmst dom INNER JOIN QL_trnDOrawdtl dod ON dom.DOrawmstoid=dod.DOrawmstoid INNER JOIN QL_mstcust c ON dom.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE dom.cmpcode LIKE '%'";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND dom.dorawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += " ) ORDER BY som.SOrawdate DESC, som.SOrawno DESC";
                
                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit,string DDLType, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust, string DDLNo, string TextDO,string TextSO, string DDLQty = "", string DDLCursorQty = "", string TextQty = "")
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = " SELECT DISTINCT 0 seq, 'Sheet' [Code], sodtldesc [Desc], g2.gendesc [Unit] FROM QL_trnDOrawdtl dod INNER JOIN QL_trnDOrawmst dom ON dom.cmpcode=dod.cmpcode AND dom.DOrawmstoid=dod.DOrawmstoid LEFT JOIN QL_trnSOrawmst som ON som.SOrawmstoid=dod.SOrawmstoid AND dom.cmpcode=som.cmpcode INNER JOIN ql_trnsorawdtl sod on sod.sorawdtloid=dod.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=dod.DOrawunitoid INNER JOIN QL_mstcust c ON dom.custoid=c.custoid AND c.activeflag='ACTIVE'";
                //if (DDLType == "Detail")
                //{
                //    if (!string.IsNullOrEmpty(TextSO))
                //    {
                //        sSql += " AND (";
                //        sSql += " som.SOrawno LIKE '%'(" + TextSO.Replace(";", ",") + ")";
                //        sSql += " OR";
                //        sSql += " )";
                //    }
                //}
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE dom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE dom.cmpcode LIKE '%'";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND dom.dorawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextDO))
                {
                    if (DDLNo == "DO No.")
                        sSql += " AND dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "')";
                    else
                        sSql += " AND dom.DOrawmstoid IN (" + TextDO.Replace(";", ",") + ")";
                }
                if (!string.IsNullOrEmpty(TextSO))
                    sSql += "AND som.SOrawno IN ('" + TextSO.Replace(";", "','") + "')";
                
                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            //if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<RoleDetail>)Session["Role"]))
            //    return RedirectToAction("NotAuthorize", "Profile");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            //if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<RoleDetail>)Session["Role"]))
            //    return RedirectToAction("NotAuthorize", "Profile");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextCust, string DDLNo, string TextDO,string TextSO, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptDO_SumXls.rpt";
                else
                    rptfile = "rptDO_SumPdf.rpt";
                rptname = "DORAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptDO_DtlXls.rpt";
                else
                {
                    if (DDLGrouping == "dom.DOrawno")
                        rptfile = "rptDO_DtlSOPdf.rpt";
                    else if (DDLGrouping == "sodtldesc")
                        rptfile = "rptDO_DtlMatCodePdf.rpt";
                    else if (DDLGrouping == "som.SOrawno")
                        rptfile = "rptDO_DtlSOPdf.rpt";
                    else
                        rptfile = "rptDO_DtlCustPdf.rpt";
                }
                rptname = "DORAWMATERIALDETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , DOrawdtlseq [No.], 'Sheet' [Code], sodtldesc [Material], ISNULL((som.SOrawno),'') AS [SO No.], sod.etd AS [SO ETD],  ISNULL((sod.pono ),'')[Customer PO], ''  AS [Port Of Discharge], DOrawqty [Qty], g3.gendesc [Unit], DOrawdtlnote [Detail Note], 0.0 [Pack Volume FG] ";
                Join = " INNER JOIN QL_trnDOrawdtl dod ON dod.cmpcode=dom.cmpcode AND dod.DOrawmstoid=dom.DOrawmstoid INNER JOIN QL_trnSOrawmst som ON som.SOrawmstoid=dod.SOrawmstoid inner join ql_trnsorawdtl sod on sod.sorawdtloid=dod.sorawdtloid INNER JOIN QL_mstgen g3 ON g3.genoid=DOrawunitoid  ";
            }
            sSql = " SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dom.cmpcode = div.cmpcode) AS [Business Unit], (select x.gendesc from ql_mstgen x where x.genoid=dom.divgroupoid) [Divisi], dom.cmpcode [CMPCODE], CONVERT(VARCHAR(20), dom.DOrawmstoid) [Draft No.], dom.DOrawmstoid [ID], DOrawdate [Date], DOrawno [DO No.], custname [Customer], custcode [Customer Code], currcode [Currency], DOrawmstnote [Header Note], DOrawmststatus [Status], dom.createuser AS [Create User], dom.createtime AS [Create Date], g1.gendesc AS [Delivery Type], DOrawreqdate AS [DO Request Date], DOrawcustpono AS [DO Cust PO NO], DOrawdest AS [DO Destination], (CASE DOrawmststatus WHEN 'In Process' THEN '' ELSE UPPER(dom.upduser) END) [Post User], (CASE DOrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE dom.updtime END) [Post Date], 'RM' AS [DO Type],0.0 AS [Total CBF] " + " , 0 AS [ID CI],'' AS [Draft No. CI],'' AS [CI No.],'01/01/1900' AS [CI Date],'' AS [Delivery Type],'' AS [Port Of Discharge],'' AS [Port Of Ship], '01/01/1900' AS [ETD], '01/01/1900' AS [ETA], '' AS [Vessel Name], '' AS [Invoice No.], '' AS [Faktur Pajak No.], '' AS [Note CI], '' AS [Status CI], '' AS [Create UserID CI], '' AS [Create Username CI], '01/01/1900' AS [Create Datetime CI], '' AS [Post UserID CI],'' AS [Post Username CI], '01/01/1900' AS [Post Datetime CI] " + Dtl + " FROM QL_trnDOrawmst dom INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=dom.curroid INNER JOIN QL_mstgen g1 ON g1.genoid=DOrawdelivtypeoid " + Join + " ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE dom.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE dom.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(TextCust))
                sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND dom.dorawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND dom.divgroupoid=" + DDLdivgroupoid;
            }
            if (!string.IsNullOrEmpty(TextDO))
            {
                if (DDLNo == "DO No.")
                    sSql += " AND dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "')";
                else
                    sSql += " AND dom.DOrawmstoid IN (" + TextDO.Replace(";", ",") + ")";
            }
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND sodtldesc IN ('" + TextMaterial.Replace(";", "','") + "')";
                else if (!string.IsNullOrEmpty(TextSO))
                    sSql += "AND som.SOrawno IN ('" + TextSO.Replace(";", "','") + "')";
            }

            if (DDLType == "Summary")
            {
                if (DDLSorting == "dom.DOrawno")
                    sSql += " ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                else
                    sSql += " ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "dom.DOrawno")
                {
                    if (DDLSorting == "som.SOrawno")
                        sSql += " ORDER BY dom.cmpcode ASC , dom.DOrawmstoid " + DDLOrderBy + " , " + DDLSorting + " " + DDLOrderBy;
                    else
                        sSql += " ORDER BY dom.cmpcode ASC , dom.DOrawmstoid " + DDLOrderBy + " , " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "sodtldesc")
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += "ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid ";
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "som.SOrawno")
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += " ORDER BY dom.cmpcode ASC, som.SOrawno , " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, som.SOrawno , " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += "ORDER BY dom.cmpcode ASC, c.custname , " + DDLSorting + " " + DDLOrderBy ;
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, c.custname , " + DDLSorting + " " + DDLOrderBy;
                }

            }

            sSql = sSql.ToLower();
            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportInPrice(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextCust, string DDLNo, string TextDO, string TextSO, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptDO_SumXls2.rpt";
                else
                    rptfile = "rptDO_SumPdf2.rpt";
                rptname = "DORAWMATERIALSUMMARYINPRICE";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptDO_DtlXls2.rpt";
                else
                {
                    if (DDLGrouping == "dom.DOrawno")
                        rptfile = "rptDO_DtlSOPdf2.rpt";
                    else if (DDLGrouping == "sodtldesc")
                        rptfile = "rptDO_DtlMatCodePdf2.rpt";
                    else if (DDLGrouping == "som.SOrawno")
                        rptfile = "rptDO_DtlSOPdf2.rpt";
                    else
                        rptfile = "rptDO_DtlCustPdf2.rpt";
                }
                rptname = "DORAWMATERIALDETAILINPRICE";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , DOrawdtlseq [No.], 'Sheet' [Code], sodtldesc [Material], ISNULL((som.SOrawno),'') AS [SO No.], sod.SOrawdtletd AS [SO ETD],  ISNULL((som.SOrawrefno ),'')[Customer PO], ''  AS [Port Of Discharge], DOrawqty [Qty], g3.gendesc [Unit], DOrawdtlnote [Detail Note], 0.0 [Pack Volume FG] ";
                Dtl += ", dom.dorawtotalamt [Total Value], dod.dorawprice [Price], dod.dorawdtlamt [Value], (SELECT ISNULL((SOrawdtlnetto/SOrawqty),0.0) FROM QL_trnSOrawdtl sod WHERE sod.SOrawdtloid=dod.SOrawdtloid) AS [Value SO], (SELECT ISNULL((SOrawdtlnetto/SOrawqty)*DOrawqty,0.0) FROM QL_trnSOrawdtl sod WHERE sod.SOrawdtloid=dod.SOrawdtloid) AS [Value X Qty SO]";
                Join = " INNER JOIN QL_trnDOrawdtl dod ON dod.cmpcode=dom.cmpcode AND dod.DOrawmstoid=dom.DOrawmstoid INNER JOIN QL_trnSOrawmst som ON som.SOrawmstoid=dod.SOrawmstoid inner join ql_trnsorawdtl sod on sod.sorawdtloid=dod.sorawdtloid INNER JOIN QL_mstgen g3 ON g3.genoid=DOrawunitoid  ";
            }
            else
            {
                Dtl += ", dom.dorawtotalamt [Total Value], (SELECT SUM(ISNULL((SOrawdtlnetto/SOrawqty),0.0)) FROM QL_trnDOrawdtl dod INNER JOIN QL_trnSOrawdtl sod ON sod.SOrawdtloid=dod.SOrawdtloid WHERE dod.DOrawmstoid=dom.DOrawmstoid) AS [Total Value SO], (SELECT SUM(ISNULL((SOrawdtlnetto/SOrawqty)*DOrawqty,0.0)) FROM QL_trnDOrawdtl dod INNER JOIN QL_trnSOrawdtl sod ON sod.SOrawdtloid=dod.SOrawdtloid WHERE dod.DOrawmstoid=dom.DOrawmstoid) AS [Total Value X Qty SO]";
            }
            sSql = " SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dom.cmpcode = div.cmpcode) AS [Business Unit], dom.cmpcode [CMPCODE], CONVERT(VARCHAR(20), dom.DOrawmstoid) [Draft No.], dom.DOrawmstoid [ID], DOrawdate [Date], DOrawno [DO No.], custname [Customer], custcode [Customer Code], currcode [Currency], DOrawmstnote [Header Note], DOrawmststatus [Status], dom.createuser AS [Create User], dom.createtime AS [Create Date], g1.gendesc AS [Delivery Type], DOrawreqdate AS [DO Request Date], DOrawcustpono AS [DO Cust PO NO], DOrawdest AS [DO Destination], (CASE DOrawmststatus WHEN 'In Process' THEN '' ELSE UPPER(dom.upduser) END) [Post User], (CASE DOrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE dom.updtime END) [Post Date], 'RM' AS [DO Type],0.0 AS [Total CBF] " + " , 0 AS [ID CI],'' AS [Draft No. CI],'' AS [CI No.],'01/01/1900' AS [CI Date],'' AS [Delivery Type],'' AS [Port Of Discharge],'' AS [Port Of Ship], '01/01/1900' AS [ETD], '01/01/1900' AS [ETA], '' AS [Vessel Name], '' AS [Invoice No.], '' AS [Faktur Pajak No.], '' AS [Note CI], '' AS [Status CI], '' AS [Create UserID CI], '' AS [Create Username CI], '01/01/1900' AS [Create Datetime CI], '' AS [Post UserID CI],'' AS [Post Username CI], '01/01/1900' AS [Post Datetime CI] " + Dtl + " FROM QL_trnDOrawmst dom INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=dom.curroid INNER JOIN QL_mstgen g1 ON g1.genoid=DOrawdelivtypeoid " + Join + " ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE dom.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE dom.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(TextCust))
                sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND dom.dorawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

            if (!string.IsNullOrEmpty(TextDO))
            {
                if (DDLNo == "DO No.")
                    sSql += " AND dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "')";
                else
                    sSql += " AND dom.DOrawmstoid IN (" + TextDO.Replace(";", ",") + ")";
            }
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND sodtldesc IN ('" + TextMaterial.Replace(";", "','") + "')";
                else if (!string.IsNullOrEmpty(TextSO))
                    sSql += "AND som.SOrawno IN ('" + TextSO.Replace(";", "','") + "')";
            }

            if (DDLType == "Summary")
            {
                if (DDLSorting == "dom.DOrawno")
                    sSql += " ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                else
                    sSql += " ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "dom.DOrawno")
                {
                    if (DDLSorting == "som.SOrawno")
                        sSql += " ORDER BY dom.cmpcode ASC, dom.DOrawno  " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy + " , " + DDLSorting + " " + DDLOrderBy;
                    else
                        sSql += " ORDER BY dom.cmpcode ASC, dom.DOrawno  " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy + " , " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "sodtldesc")
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += "ORDER BY dom.cmpcode ASC , " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "som.SOrawno")
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += " ORDER BY dom.cmpcode ASC, som.SOrawno , " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, som.SOrawno , " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLSorting == "dom.DOrawno")
                        sSql += "ORDER BY dom.cmpcode ASC, c.custname , " + DDLSorting + " " + DDLOrderBy + " , dom.DOrawmstoid " + DDLOrderBy;
                    else
                        sSql += "ORDER BY dom.cmpcode ASC, c.custname , " + DDLSorting + " " + DDLOrderBy;
                }

            }

            sSql = sSql.ToLower();
            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}