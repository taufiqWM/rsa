﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class FixAssetReportController : Controller
    {
        // GET: FixAssetReport
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public FixAssetReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(string rpttype = "")
        {
            sSql = " SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_mstacctg a";
            var DDLAccount = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLAccount = DDLAccount;

            List<SelectListItem> DDLMonthStart = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonthStart.Add(item);
            }

            ViewBag.DDLMonthStart = DDLMonthStart;
            List<SelectListItem> DDLYearStart = new List<SelectListItem>();
            int start = 2023;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYearStart.Add(item);
            }
            ViewBag.DDLYearStart = DDLYearStart;

            List<SelectListItem> DDLMonthEnd = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonthEnd.Add(item);
            }
            ViewBag.DDLMonthEnd = DDLMonthEnd;

            List<SelectListItem> DDLYearEnd = new List<SelectListItem>();
            int start2 = 2023;
            int end2 = DateTime.Today.Year;
            for (int i = start2; i <= end2; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYearEnd.Add(item);
            }
            ViewBag.DDLYearEnd = DDLYearEnd;
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult InitDDLAccount()
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLDepartmentModel> tbl = new List<ReportModels.DDLDepartmentModel>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_ASSET");
                tbl = db.Database.SqlQuery<ReportModels.DDLDepartmentModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData()
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, matgenoid [ID], matgencode [Code], matgenlongdesc [Description] FROM QL_mstmatgen WHERE matgenres1='ASET' ORDER BY matgencode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAssetsData(string TextMaterial)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, fam.assetmstoid [ID], fam.assetno [Assets No.], (SELECT matgencode FROM QL_mstmatgen i WHERE i.matgenoid=fam.refoid AND i.matgenres1='ASET') [Assets Code], (SELECT matgenlongdesc FROM QL_mstmatgen i WHERE i.matgenoid=fam.refoid AND i.matgenres1='ASET') [Description] FROM QL_assetmst fam WHERE fam.cmpcode='" + CompnyCode + "'";

                if (!string.IsNullOrEmpty(TextMaterial))
                {
                    sSql += " AND refoid IN ( SELECT matgenoid FROM QL_mstmatgen i WHERE i.matgenoid=fam.refoid AND i.matgenres1='ASET' AND matgencode IN ('" + TextMaterial.Replace(";", "','") + "'))";
                }
                sSql += "ORDER BY fam.assetno ";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAssets");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        } 

        [HttpPost]
        public ActionResult PrintReport(string DDLMonthStart, string DDLMonthEnd, string DDLYearStart, string DDLYearEnd, string TextMaterial, string TextAssets, string DDLCurrency, string DDLAccount, string ReportType)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sKurs = DDLCurrency; var rptfile = ""; var rptname = "";

            //if (ReportType == "XLS")
            //    rptfile = "rptFixedAssetsXls.rpt";
            //else
            //    rptfile = "rptFixedAssetPdf.rpt";
            rptfile = "rptFAReport.rpt";
            rptname = "FixedAssetsReport";

            //Declare & Set Variable On Query
            sSql = "DECLARE @cmpcode VARCHAR(10); ";
            sSql += "DECLARE @startperiod VARCHAR(10); ";
            sSql += "DECLARE @endperiod VARCHAR(10); ";
            sSql += "DECLARE @startdate DATETIME; ";
            sSql += "DECLARE @enddate DATETIME; ";
            sSql += "SET @cmpcode='" + CompnyCode + "'; ";
            sSql += "SET @startperiod='" + DDLYearStart + DDLMonthStart + "'; ";
            sSql += "SET @endperiod='" + DDLYearEnd + DDLMonthEnd + "'; ";
            sSql += "SET @startdate=CAST('" + DDLMonthStart + "/01/" + DDLYearStart + " 00:00' AS DATETIME); ";
            sSql += "SET @enddate=CAST('" + DDLMonthEnd + "/" + DateTime.DaysInMonth(int.Parse(DDLYearEnd), int.Parse(DDLMonthEnd)) + "/" + DDLYearEnd + " 23:59' AS DATETIME);"; //(DDLMonth, DDLYear)

            //Select Query
            sSql += "SELECT * FROM (SELECT am.cmpcode, 'ANUGRAH PRATAMA' [Business Unit], (a1.acctgcode + ' - ' + a1.acctgdesc) [COA Asset], am.assetmstoid [ID Asset], assetno [No. Asset], (SELECT matgencode FROM QL_mstmatgen i WHERE i.matgenoid=refoid) [Kode Asset], (SELECT matgenlongdesc FROM QL_mstmatgen i WHERE i.matgenoid=refoid) [Nama Asset], assetmstnote [Note],  '' [No. MRReg], '' [No. FAC], deptname [Department], ISNULL((SELECT (groupcode + ' - ' + groupdesc) groupdesc FROM QL_mstdeptgroup dg INNER JOIN QL_mstdeptgroupdtl dgd ON dgd.cmpcode=dg.cmpcode AND dgd.groupoid=dg.groupoid WHERE dgd.cmpcode=am.cmpcode AND dgd.deptoid=de.deptoid), '') [Division], '' [Supplier], (CASE WHEN CONVERT(VARCHAR(10), assetdepdate, 101)='01/01/1900' THEN (CASE WHEN ISNULL(assetmstres1,'')='Assets In Progress Closed' THEN closetime ELSE assetdate END) ELSE assetdate END) [Tgl Perolehan], 1.0 [Qty],(SELECT gendesc FROM QL_mstmatgen i INNER JOIN QL_mstgen g ON g.genoid=matgenunitoid WHERE i.matgenoid=refoid ) [Satuan], assetpurchase [Harga Per Unit], assetpurchaseidr [Harga IDR Per Unit], 'IDR' [Kurs Beli], (CASE WHEN assetdate> @enddate THEN 0.0 ELSE (CASE WHEN(CONVERT(VARCHAR(10), shipmenttime, 101) <> '01/01/1900' AND shipmenttime < @startdate) THEN 0.0 WHEN(CONVERT(VARCHAR(10), disposetime, 101) <> '01/01/1900' AND disposetime < @startdate) THEN 0.0 ELSE(CASE WHEN ISNULL(assetmstres1, '') = 'Init Balance' THEN assetpurchase ELSE(CASE WHEN assetdate < @startdate THEN assetvalue ELSE 0.0 END) END) END) END) [S.Awal Perolehan], (CASE WHEN assetdate>@enddate THEN 0.0 ELSE(CASE WHEN ISNULL(assetmstres1, '')='Init Balance' THEN 0.0 ELSE(CASE WHEN assetdate<@startdate THEN 0.0 ELSE assetvalue END) END) END) [Penambahan Perolehan], (CASE WHEN assetdate>@enddate THEN 0.0 ELSE(CASE WHEN (shipmenttime>=@startdate AND shipmenttime<=@enddate) THEN(CASE WHEN ISNULL(assetmstres1, '')='Init Balance' THEN assetpurchase ELSE(CASE WHEN assetdate<@startdate THEN assetvalue ELSE 0.0 END) END) ELSE(CASE WHEN (disposetime>=@startdate AND disposetime<=@enddate) THEN(CASE WHEN ISNULL(assetmstres1, '')='Init Balance' THEN assetpurchase ELSE(CASE WHEN assetdate<@startdate THEN assetvalue ELSE 0.0 END) END) ELSE 0.0 END) END) END) [Pengurangan Perolehan], assetdepvalue [Penyusutan Per Bulan], ((CASE WHEN(assetmststatus= 'Disposed' AND disposetime<@startdate) THEN 0.0 ELSE ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl ad WHERE ad.cmpcode=am.cmpcode AND ad.assetmstoid=am.assetmstoid AND assetperiod<@startperiod AND assetdtlstatus='Complete'), 0.0) END) + (CASE WHEN assetdate>@enddate THEN 0.0 ELSE(assetpurchase - assetvalue) END) - (CASE WHEN shipmenttime<@startdate THEN shipmentamt ELSE 0.0 END)) [S.Awal Penyusutan], (CASE assetmststatus WHEN 'DisposedX' THEN 0.0 ELSE ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl ad WHERE ad.cmpcode=am.cmpcode AND ad.assetmstoid=am.assetmstoid AND assetperiod>=@startperiod AND assetperiod<=@endperiod AND assetdtlstatus='Complete'), 0.0) END) [Penambahan Penyusutan], (CASE WHEN(shipmenttime>=@startdate AND shipmenttime<=@enddate) THEN shipmentamt ELSE(CASE WHEN (disposetime>=@startdate AND disposetime<=@enddate) THEN ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl ad WHERE ad.cmpcode=am.cmpcode AND ad.assetmstoid=am.assetmstoid AND assetperiod<=@endperiod AND assetdtlstatus='Complete'), 0.0) ELSE 0.0 END) END) [Pengurangan Penyusutan], (CASE WHEN(assetmststatus= 'Disposed' AND disposetime<@enddate) THEN ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl ad WHERE ad.cmpcode=am.cmpcode AND ad.assetmstoid=am.assetmstoid AND assetdtlstatus='Disposed'), 0.0) ELSE 0.0 END) [Nilai Disposal], disposetime [Tgl Disposal], assetmststatus [Asset Status], ISNULL(assetmstflag,'') [SoldStatus], ISNULL(assetmstres1,'') [Last Asset Status], '' [Header Note FAC], am.assetdepmonth [Total Bln], DATEADD(MONTH,am.assetdepmonth,am.assetdate) [Tgl Akhir] FROM QL_assetmst am INNER JOIN QL_mstacctg a1 ON a1.acctgoid=assetacctgoid INNER JOIN QL_mstdept de ON de.deptoid= am.deptoid WHERE am.cmpcode= @cmpcode AND ISNULL(assetmstres1, '')<>'Assets In Progress' AND assetmststatus<>'In Process' ";

            if (!string.IsNullOrEmpty(DDLAccount))
                sSql += " AND assetacctgoid=" + DDLAccount + "";

            sSql += "AND (CASE WHEN ISNULL(assetmstres1,'')='Assets In Progress Closed' THEN(RIGHT(CONVERT(CHAR(10),closetime,101),4)+''+LEFT(CONVERT(CHAR(10),closetime,101),2)) ELSE am.periodacctg END)<=@endperiod ) AS tblAsset WHERE 1=1 AND([S.Awal Perolehan]>0 OR[Penambahan Perolehan]>0 OR[Nilai Disposal]>0)";

            if (!string.IsNullOrEmpty(TextMaterial))
                sSql += " AND  [Kode Asset] IN ('" + TextMaterial.Replace(";", "','") + "')";

            if (!string.IsNullOrEmpty(TextAssets))
                sSql += " AND [No. Asset] IN ('" + TextAssets.Replace(";", "','") + "')";

            sSql += " ORDER BY [No. Asset]";

            string bulandesc = "";
            if (int.Parse(DDLMonthStart) == 1)
            {
                bulandesc = "Januari";
            }
            else if (int.Parse(DDLMonthStart) == 2)
            {
                bulandesc = "Februari";
            }
            else if (int.Parse(DDLMonthStart) == 3)
            {
                bulandesc = "Maret";
            }
            else if (int.Parse(DDLMonthStart) == 4)
            {
                bulandesc = "April";
            }
            else if (int.Parse(DDLMonthStart) == 5)
            {
                bulandesc = "Mei";
            }
            else if (int.Parse(DDLMonthStart) == 6)
            {
                bulandesc = "Juni";
            }
            else if (int.Parse(DDLMonthStart) == 7)
            {
                bulandesc = "Juli";
            }
            else if (int.Parse(DDLMonthStart) == 8)
            {
                bulandesc = "Agustus";
            }
            else if (int.Parse(DDLMonthStart) == 9)
            {
                bulandesc = "September";
            }
            else if (int.Parse(DDLMonthStart) == 10)
            {
                bulandesc = "Oktober";
            }
            else if (int.Parse(DDLMonthStart) == 11)
            {
                bulandesc = "Nopember";
            }
            else if (int.Parse(DDLMonthStart) == 12)
            {
                bulandesc = "Desember";
            }
            bulandesc = bulandesc + " " + DDLYearStart;

            string bulandesc2 = "";
            if (int.Parse(DDLMonthEnd) == 1)
            {
                bulandesc2 = "Januari";
            }
            else if (int.Parse(DDLMonthEnd) == 2)
            {
                bulandesc2 = "Februari";
            }
            else if (int.Parse(DDLMonthEnd) == 3)
            {
                bulandesc2 = "Maret";
            }
            else if (int.Parse(DDLMonthEnd) == 4)
            {
                bulandesc2 = "April";
            }
            else if (int.Parse(DDLMonthEnd) == 5)
            {
                bulandesc2 = "Mei";
            }
            else if (int.Parse(DDLMonthEnd) == 6)
            {
                bulandesc2 = "Juni";
            }
            else if (int.Parse(DDLMonthEnd) == 7)
            {
                bulandesc2 = "Juli";
            }
            else if (int.Parse(DDLMonthEnd) == 8)
            {
                bulandesc2 = "Agustus";
            }
            else if (int.Parse(DDLMonthEnd) == 9)
            {
                bulandesc2 = "September";
            }
            else if (int.Parse(DDLMonthEnd) == 10)
            {
                bulandesc2 = "Oktober";
            }
            else if (int.Parse(DDLMonthEnd) == 11)
            {
                bulandesc2 = "Nopember";
            }
            else if (int.Parse(DDLMonthEnd) == 12)
            {
                bulandesc2 = "Desember";
            }
            bulandesc2 = bulandesc2 + " " + DDLYearEnd;

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PeriodFrom", bulandesc);
            rptparam.Add("PeriodTo", bulandesc2);
            rptparam.Add("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

    }
}