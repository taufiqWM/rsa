﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class ProdresSheetReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string rawcode { get; set; }
            public string rawdesc { get; set; }
            public string rawtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string sorawno { get; set; }
            public DateTime sorawdate { get; set; }
            public string custname { get; set; }
            public string sorawmstnote { get; set; }
            public string sorawcustpo { get; set; }
        }

        public class listspk
        {
            public string wono { get; set; }
            public DateTime wodate { get; set; }
            public string custname { get; set; }
            public string wotype { get; set; }
            public string womstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetWOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust, string TextSO, string TextProdres)
        {
            sSql = "SELECT DISTINCT 0 seq, w.womstoid,w.wono, w.wodate, isnull(womstres1,'Finish Good') wotype ,c.custname,isnull(womstnote,'') womstnote, i.itemlongdesc itemdesc FROM QL_trnwomst w inner join ql_trnprodresmst p on p.womstoid=w.womstoid INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=w.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            { 
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";

                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND p.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY w.wono";

            List<listspk> tbl = new List<listspk>();
            tbl = db.Database.SqlQuery<listspk>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listProdres
        {
            public string prodresno { get; set; }
            public DateTime prodresdate { get; set; }
            public string result { get; set; }
            public string proses { get; set; }
            public string prodresmstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetProdresData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust, string TextSO, string TextWO)
        {
            sSql = "SELECT DISTINCT 0 seq, p.prodresmstoid,p.prodresno, p.prodresdate,isnull(prodresmstnote,'') prodresmstnote, p.result, (select x.gendesc from ql_mstgen x where x.genoid=p.processoid) proses, i.itemlongdesc itemdesc FROM QL_trnprodresmst p Inner Join QL_trnwomst w on p.womstoid=w.womstoid INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=p.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.woitemno IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY p.prodresno";

            List<listProdres> tbl = new List<listProdres>();
            tbl = db.Database.SqlQuery<listProdres>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust, string TextWO, string TextProdres)
        {
            sSql = "SELECT DISTINCT 0 seq, som.sorawmstoid,som.sorawno, sorawcustref sorawcustpo, sorawdate ,c.custname,sorawmstnote FROM QL_trnsorawmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid inner join ql_trnwomst w inner join ql_trnprodresmst p on p.womstoid=w.womstoid on w.somstoid=som.sorawmstoid and sotype='QL_trnsorawmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if(MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
               
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND wom.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND p.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }
            sSql += "ORDER BY som.sorawno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextSO, string TextWO, string TextProdres)
        {
            sSql = "SELECT DISTINCT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnwomst w inner join ql_trnprodresmst p on p.womstoid=w.womstoid inner join ql_trnsorawmst som on w.somstoid=som.sorawmstoid and sotype='QL_trnsorawmst' WHERE w.cmpcode='" + CompnyCode + "'";  
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND w.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }
            sSql += ") ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate, string prodresno)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.reqrabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsorawmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string prabno, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT DISTINCT  'Sheet' rawcode, sodtldesc rawdesc, '' rawtype, 'PCS' unit FROM QL_trnsorawdtl rd INNER JOIN QL_trnsorawmst som ON som.cmpcode=rd.cmpcode AND som.sorawmstoid=rd.sorawmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN ql_mstgen g ON g.genoid=sorawunitoid  WHERE rd.cmpcode='" + CompnyCode +"'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }

            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND "+ ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY sodtldesc";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string sorawno, string MCBtgl, string wono, string DDLspk, string prodresno)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";

            if (reporttype == "XLS")
                rptfile = "rptProdresXls.rpt";
            else
            {
                rptfile = "rptProdresPdf.rpt";
            }
            rptname = "SOSPK";

            sSql = "SELECT p.cmpcode AS [Business Unit], prodresmstoid,prodresno,prodresdate,p.createuser,p.createtime, p.upduser,p.updtime,isnull(prodresmstnote,'') prodresmstnote, (select x.gendesc from ql_mstgen x where x.genoid=p.processoid) proses, result,ISNULL(resultqty,0.0) resultqty, isnull(rejectqty,0.0) rejectqty, ISNULL(p.rejectreason,'') rejectreason, ISNULL(p.rejectreason2,'') rejectreason2, ISNULL(p.rejectreason3,'') rejectreason3,w.wono,wodate,soqty,spkqty,sm.sorawno sono, sodtldesc itemdesc, sorawdate sodate, c.custname  from QL_trnprodresmst p Inner Join QL_trnwomst w on p.womstoid=w.womstoid inner join ql_mstcust c on c.custoid=w.custoid inner join QL_trnsorawmst sm on w.somstoid=sm.sorawmstoid and sotype='QL_trnsorawmst' inner join QL_trnsorawdtl sd on sd.sorawdtloid=w.sodtloid and sotype='QL_trnsorawmst' inner join ql_mstgen m1 on m1.genoid=w.m1 inner join ql_mstgen m2 on m2.genoid=w.m2 inner join ql_mstgen m3 on m3.genoid=w.m3 inner join ql_mstgen m4 on m4.genoid=w.m4 where isnull(prodresmstres1,'')='Sheet' and p.cmpcode='" + CompnyCode + "' ";
            if (StartPeriod != "" && EndPeriod != "")
            {
                if (MCBtgl != null)
                {
                    if (MCBtgl != "")
                    {
                        sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                }

            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND w.womststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (sorawno != null) {
                if (sorawno != "")
                {
                    string[] arr = sorawno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND sm.sorawno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (DDLspk != null)
            {
                if (DDLspk != "ALL")
                {
                    if (DDLspk == "Completed")
                    {
                        sSql += " And isnull(sm.statusspk,'') = 'Closed' ";
                    }
                    else
                    {
                        sSql += " And isnull(sm.statusspk,'')='' ";
                    }

                }
            }
            if (wono != null)
            {
                if (wono != "")
                {
                    string[] arr = wono.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND w.wono IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (TextCust!=null)
            {
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (prodresno != null)
            {
                if (prodresno != "")
                {
                    string[] arr = prodresno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND p.prodresno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextNomor != null)
            {
                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
           

            if (DDLType == "Detail")
            {
                if (TextMaterial != null)
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND sodtldesc IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
               
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}