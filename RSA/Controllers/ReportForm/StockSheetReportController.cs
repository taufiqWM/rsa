﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class StockSheetReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode = '" + CompnyCode + "' AND gengroup = 'MATERIAL LOCATION' AND activeflag = 'ACTIVE' /*AND (genother1='11' )*/ ORDER BY gendesc";

            if (ViewBag.FormType == "Finish Good")
            {
                sSql = sSql.Replace("MATERIAL LOCATION", "ITEM LOCATION");
            }

            var DDLWarehouse = new SelectList(db.Database.SqlQuery<ReportModels.DDLWarehouseModel>(sSql).ToList(), "genoid", "gendesc");
            ViewBag.DDLWarehouse = DDLWarehouse;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2013;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;

        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string FormType, string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLWarehouseModel> tbl = new List<ReportModels.DDLWarehouseModel>();

            try
            {
                sSql = "SELECT genoid, UPPER(gendesc) AS gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND gengroup='MATERIAL LOCATION' /*AND (genother1= '" + DDLBusinessUnit + "' )*/ ORDER BY gendesc";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.formtitle == "Finish Good")
                {
                    sSql = sSql.Replace("MATERIAL LOCATION", "ITEM LOCATION");
                }

                tbl = db.Database.SqlQuery<ReportModels.DDLWarehouseModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL();
            return View();
        }

        // GET: PRReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportInPrice/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL();
            return View();
        }

        public ActionResult GetMaterialData(string FormType, string DDLAssets)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, sodtldesc [Description], gendesc [Unit] FROM QL_trnsorawdtl m INNER JOIN QL_mstgen g ON genoid=sorawunitoid WHERE m.cmpcode='" + CompnyCode + "' /*AND ISNULL(matrawres3, '') IN ('WIP', 'Umum')*/";

                //if (DDLAssets != "All")
                //{
                //    sSql += " AND matrawres2 ='" + DDLAssets + "'";
                //}

                sSql += "ORDER BY sodtldesc";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                {
                    //sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());
                }





                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var matraw = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                matraw = (i++).ToString();
                            row.Add(col.ColumnName, matraw);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLType, string StartDate, string EndDate, string DDLBusinessUnit, string DDLGrouping, string DDLWarehouse, string TextMaterial, bool WithDO, bool AllowNull, string DDLAssets, string ReportType = "")
        {

            var rptfile = ""; var rptname = "";
            var sGroup = "";
            //var sWithDO = "";
            var sWarehouse1 = "";
            //var sWarehouse2 = "";
            var sWarehouse3 = "";
            var sWhere = " ";
            var objtype = new ReportModels.FullFormType(FormType);

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sWhere = " AND con.cmpcode='" + DDLBusinessUnit + "'";
            else
                sWhere = " AND con.cmpcode LIKE '%%' ";

            if (!string.IsNullOrEmpty(DDLWarehouse))
                sWhere += " AND con.mtrwhoid ='" + DDLWarehouse + "'";

            if (DDLAssets != "All")
            {
                sWhere += " AND matrawres2 ='" + DDLAssets + "'";
            }

            if (!string.IsNullOrEmpty(TextMaterial))
            {
                sWhere += " AND (";
                sWhere += " m.sodtldesc IN ('" + TextMaterial.Replace(";", "','") + "')";
                sWhere += ")";
            }

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            if (DDLType == "Summary")
            {
                rptname = "StockSummaryReport";

                if (DDLGrouping == "MATERIAL")
                {
                    sGroup = "";
                }
                else if (DDLGrouping == "CATEGORY 1")
                {
                    //sWithDO = "";
                    sGroup = "";
                }
                else if (DDLGrouping == "CATEGORY 2")
                {
                    //sWithDO = "";
                    sGroup = "";
                }
                else if (DDLGrouping == "CATEGORY 3")
                {
                    sGroup = "";
                }
                else if (DDLGrouping == "CATEGORY 4")
                {
                    sGroup = "";
                }


                if (WithDO)
                {
                    //sWithDO = "WithDO";
                    sWarehouse1 = "";
                    //sWarehouse2 = "";
                    sWarehouse3 = "";
                }
                else
                {
                    sWarehouse1 = " [WareHouse],";
                    //sWarehouse2 = " g1.gendesc AS [Warehouse],";
                    sWarehouse3 = " con.mtrwhoid,";
                }

                if (ReportType == "XLS")
                {
                    rptfile = "rptStockSumPerCustomer_Excel.rpt";
                }
                else
                {
                    rptfile = "rptStockSumPerCustomer.rpt";
                }

                var Cat1 = "";
                var Cat2 = "";
                var Cat3 = "";
                var Cat4 = "";

                if (DDLGrouping == "CATEGORY 1" || DDLGrouping == "CATEGORY 2" || DDLGrouping == "CATEGORY 3" || DDLGrouping == "CATEGORY 4")
                {
                    Cat1 = " , SUBSTRING([Code],1,2) [Lv1 Code], (SELECT cat1shortdesc FROM QL_mstcat1 c1 WHERE c1.cat1code=SUBSTRING([Code],1,2) AND c1.cat1res1='Raw') [Lv1] ";
                }
                if (DDLGrouping == "CATEGORY 2" || DDLGrouping == "CATEGORY 3" || DDLGrouping == "CATEGORY 4")
                {
                    Cat2 = " , SUBSTRING([Code],4,3) [Lv2 Code], (SELECT cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid AND c1.cat1res1='Raw'  WHERE  c1.cat1code=SUBSTRING([Code],1,2) AND c2.cat2code=SUBSTRING([Code],4,3) AND c2.cat2res1='Raw') [Lv2] ";
                }
                if (DDLGrouping == "CATEGORY 3" || DDLGrouping == "CATEGORY 4")
                {
                    Cat3 = " , SUBSTRING([Code],8,4) [Lv3 Code], (SELECT cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat1oid=c3.cat1oid AND c2.cat2oid=c3.cat2oid AND c2.cat2res1='Raw' INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid AND c1.cat1res1='Raw' WHERE  c3.cat3code=SUBSTRING([Code],8,4) AND c2.cat2code=SUBSTRING([Code],4,3) AND c1.cat1code=SUBSTRING([Code],1,2)AND c3.cat3res1='Raw' ) [Lv3] ";
                }
                if (DDLGrouping == "CATEGORY 4")
                {
                    Cat4 = " , SUBSTRING([Code],13,4) [Lv4 Code], (SELECT cat4shortdesc FROM QL_mstcat4 c4 INNER JOIN QL_mstcat3 c3 ON c3.cmpcode=c4.cmpcode AND c4.cat1oid=c3.cat1oid AND c4.cat2oid=c3.cat2oid AND c3.cat3oid=c4.cat3oid AND c3.cat3res1='Raw'  INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c4.cmpcode AND c2.cat1oid=c4.cat1oid AND c2.cat2oid=c4.cat2oid AND c2.cat2res1='Raw' INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c4.cmpcode AND c1.cat1oid=c4.cat1oid AND c1.cat1res1='Raw' WHERE c1.cat1code=SUBSTRING([Code],1,2) AND c2.cat2code=SUBSTRING([Code],4,3) AND c3.cat3code=SUBSTRING([Code],8,4) AND c4.cat4code=SUBSTRING([Code],13,4) AND c4.cat4res1='Raw' ) [Lv4]";
                }

                sSql = " SELECT *";
                if (WithDO)
                {
                    sSql += " , ISNULL((SELECT SUM(dod.dorawqty) FROM QL_trndorawmst dom INNER JOIN QL_trndorawdtl dod ON dom.cmpcode=dod.cmpcode AND dom.dorawmstoid=dod.dorawmstoid WHERE dom.cmpcode='" + DDLBusinessUnit + "' AND dom.dorawdate<'" + EndDate + " 23:59:59' AND dom.dorawmststatus='Post' AND dod.matrawoid=[Oid] GROUP BY dod.matrawoid ),0) AS [Qty DO] ";
                }
                sSql += " FROM(";
                sSql += "SELECT cmpcode, [Oid], [Code], [Description], 'A' [Assets Flag],[Safety Stock], [Unit], " + sWarehouse1 + " SUM([Saldo Awal] + [Saldo Awal Sum]) AS [Saldo Awal], (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Init Saldo Awal] + [Qty In]) ELSE SUM([Qty In]) END) AS [Qty In], SUM([Qty Out]) AS [Qty Out], [Business Unit], 0.0 [Width], 0.0 [Diameter], 0.0 [Height], 0.0 [Volume], [Department], [Supplier] ";
                sSql += Cat1 + Cat2 + Cat3 + Cat4;
                sSql += " FROM ( ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], '' AS [Code], sodtldesc AS [Description], '' AS [SKU], 0.0 [Safety Stock], g2.gendesc AS [Unit],  g1.gendesc AS [Warehouse], 0.0 AS [Saldo Awal], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal Sum], 0.0 AS [Init Saldo Awal], 0.0 AS [Qty In], 0.0 AS [Qty Out], divname AS [Business Unit],  0.0 AS [CBF], '' AS [Department], '' AS [Supplier] FROM QL_conmat con INNER JOIN QL_trnsorawdtl m ON sorawdtloid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=sorawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.cmpcode='" + DDLBusinessUnit + "' AND con.refname='SHEET' /*AND con.type<>'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/dd/yyyy") + " 00:00:00'*/ AND con.trndate < CAST('" + StartDate + " 00:00:00' AS DATETIME) " + sWhere + " GROUP BY con.refoid, sodtldesc, g1.gendesc, g2.gendesc, con.cmpcode, divname UNION ALL ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], '' AS [Code], sodtldesc AS [Description], '' AS [SKU], 0.0 [Safety Stock], g2.gendesc AS [Unit],  g1.gendesc AS [Warehouse],  0.0 AS [Saldo Awal], 0.0 AS [Saldo Awal Sum], 0.0 AS [Init Saldo Awal], SUM(con.qtyin) AS [Qty In], SUM(con.qtyout) AS [Qty Out], divname AS [Business Unit],  0.0 AS [CBF], ISNULL((SELECT wono FROM QL_trnwomst de WHERE de.womstoid=con.deptoid), '') AS [Department], ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=con.cust_id),'') AS [Supplier] FROM QL_conmat con INNER JOIN QL_trnsorawdtl m ON sorawdtloid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid = sorawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode = con.cmpcode WHERE con.cmpcode='" + DDLBusinessUnit + "' AND con.refname='SHEET' /*AND con.type<>'RMIS'*/ AND con.trndate >= CAST('" + StartDate + " 00:00:00' AS DATETIME) AND con.trndate <= CAST('" + EndDate + " 23:59:59' AS DATETIME) " + sWhere + " GROUP BY con.refoid, sodtldesc, g1.gendesc, g2.gendesc, con.cmpcode, con.refname, " + sWarehouse3 + " divname, con.deptoid, con.cust_id /*UNION ALL*/ ";

                //Table HISTORY
                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], '' AS [Code], sodtldesc AS [Description], '' AS [SKU], 0.0 [Safety Stock], g2.gendesc AS [Unit],  g1.gendesc AS [Warehouse], 0.0 AS [Saldo Awal], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal Sum], 0.0 AS [Init Saldo Awal], 0.0 AS [Qty In], 0.0 AS [Qty Out], divname AS [Business Unit],  0.0 AS [CBF] FROM QL_conmat_hist con INNER JOIN QL_trnsorawdtl m ON sorawdtloid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=sorawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.cmpcode='" + DDLBusinessUnit + "' AND con.refname='SHEET' /*AND con.type<>'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/dd/yyyy") + " 00:00:00'*/ AND con.trndate < CAST('" + StartDate + " 00:00:00' AS DATETIME) " + sWhere + " GROUP BY con.refoid, sodtldesc, g1.gendesc, g2.gendesc, con.cmpcode, divname UNION ALL ";

                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], '' AS [Code], sodtldesc AS [Description], '' AS [SKU], 0.0 [Safety Stock], g2.gendesc AS [Unit],  g1.gendesc AS [Warehouse],  0.0 AS [Saldo Awal], 0.0 AS [Saldo Awal Sum], 0.0 AS [Init Saldo Awal], SUM(con.qtyin) AS [Qty In], SUM(con.qtyout) AS [Qty Out], divname AS [Business Unit],  0.0 AS [CBF] FROM QL_conmat_hist con INNER JOIN QL_trnsorawdtl m ON sorawdtloid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=sorawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.cmpcode='" + DDLBusinessUnit + "' AND con.refname='SHEET' /*AND con.type<>'RMIS'*/ AND con.trndate >= CAST('" + StartDate + " 00:00:00' AS DATETIME) AND con.trndate <= CAST('" + EndDate + " 23:59:59' AS DATETIME) " + sWhere + " GROUP BY con.refoid, sodtldesc, g1.gendesc, g2.gendesc, con.cmpcode, con.refname, " + sWarehouse3 + " divname ";
                //END OF TABLE HISTORY

                sSql += ") AS tblstock GROUP BY cmpcode, [Oid], [Code], [Description],[Safety Stock], [Unit], " + sWarehouse1 + " [Business Unit], [Department], [Supplier]";
                if (!AllowNull)
                {
                    sSql += " HAVING (SUM([Saldo Awal] + [Saldo Awal Sum]) + (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Init Saldo Awal] + [Qty In]) ELSE SUM([Qty In]) END) - SUM([Qty Out])) > 0";
                }
                sSql += ") AS tblStock  ";
                sSql += " ORDER BY cmpcode, " + sWarehouse1 + " [Code]";
            }
            else
            {
                rptname = "StockDetailReport";

                if (ReportType == "XLS")
                {
                    sGroup = "Dtl_Excel";
                }
                else
                {
                    sGroup = "Dtl";
                }
                rptfile = "rptStock" + sGroup + ".rpt";

                sSql = "SELECT cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], [Warehouse], [Reference], [Date], [Batch No.], [No Roll], [Qty In], [Qty Out], [Type], [Business Unit], [KIK No.], [Department], [Division], [Update Time], [Supplier] FROM (";
                sSql += "SELECT cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], [Warehouse], 'Saldo Awal' AS [Reference], CONVERT(DATETIME, '" + StartDate + " 00:00:00') AS [Date], [Batch No.], [No Roll],  SUM([Saldo Awal]) AS [Qty In], 0.0 AS [Qty Out], 1 AS [Type], [Business Unit], '' AS [KIK No.], [Department] AS [Department], '' AS [Division], CONVERT(DATETIME, '" + StartDate + " 00:00:00') AS [Update Time], [Supplier] AS [Supplier] FROM (";
                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], '' AS [Code], sodtldesc AS [Description], refno AS [Batch No.], ISNULL(serialno,'') [No Roll], 'A' AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal], divname AS [Business Unit], '' AS [Department], '' AS [Supplier] FROM QL_conmat con INNER JOIN QL_trnsorawdtl m ON sorawdtloid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=sorawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='SHEET' /* AND con.type <> 'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/dd/yyyy") + " 00:00:00'*/ AND con.trndate < '" + StartDate + " 00:00:00' " + sWhere + " GROUP BY con.cmpcode, con.refoid, sodtldesc, g2.gendesc, g1.gendesc, divname, refno, ISNULL(serialno,'') /*UNION ALL*/ ";

                //TABLE HISTORY
                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal], divname AS [Business Unit] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /* AND con.type <> 'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/dd/yyyy") + " 00:00:00'*/ AND con.trndate < '" + StartDate + " 00:00:00' " + sWhere + " GROUP BY con.cmpcode, con.refoid, matrawcode, matrawlongdesc, matrawres2, g2.gendesc, g1.gendesc, divname /*UNION ALL*/ ";

                //END OF TABLE HISTORY
                sSql += ") AS tbl_saldoawal GROUP BY cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], [Warehouse], [Business Unit], [Batch No.], [No Roll], [Department], [Supplier] UNION ALL ";
                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], con.note AS [Reference], con.updtime AS [Date], ISNULL(con.refno,'') AS [Batch No.], ISNULL(serialno,'') [No Roll], con.qtyin AS [Qty In], con.qtyout AS [Qty Out], 2 AS [Type], divname AS [Business Unit], con.refno AS [KIK No.], ISNULL((SELECT wono FROM QL_trnwomst de WHERE de.womstoid=con.deptoid), '') AS [Department], ISNULL((SELECT TOP 1 (groupcode + ' - ' + groupdesc) FROM QL_mstdeptgroupdtl dgd INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=dgd.cmpcode AND dg.groupoid=dgd.groupoid WHERE dgd.cmpcode=con.cmpcode AND dgd.deptoid=con.deptoid), '') AS [Division], (CASE WHEN con.formaction IN ('QL_trnassetrecdtl', 'QL_trnstockadj') THEN con.trndate ELSE con.updtime END) AS [Update Time], ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=con.cust_id),'') AS [Supplier] FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='SHEET' /*AND con.type<>'RMIS'*/ AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + " /*UNION ALL*/ ";

                //TABLE HISTORY
                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], con.note AS [Reference], con.updtime AS [Date], con.qtyin AS [Qty In], con.qtyout AS [Qty Out], 2 AS [Type], divname AS [Business Unit], con.refno AS [KIK No.], ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.cmpcode=con.cmpcode AND de.deptoid=con.deptoid), '') AS [Department], ISNULL((SELECT TOP 1 (groupcode + ' - ' + groupdesc) FROM QL_mstdeptgroupdtl dgd INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=dgd.cmpcode AND dg.groupoid=dgd.groupoid WHERE dgd.cmpcode=con.cmpcode AND dgd.deptoid=con.deptoid), '') AS [Division], (CASE WHEN con.formaction IN ('QL_trnassetrecdtl', 'QL_trnstockadj') THEN con.trndate ELSE con.updtime END) AS [Update Time] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS'*/ AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + "";

                //END OF TABLE HISTORY
                sSql += ") AS tblstock ORDER BY cmpcode, [Type], [Date], [Update Time], [Warehouse], [Code]";

            }

            //if (objtype.formtitle == "Spare Part")
            //{
            //    sSql = sSql.Replace("matrawwidth", "0.0").Replace("matrawdiameter", "0.0").Replace("matrawheight", "0.0").Replace("matrawvolume", "0.0").Replace(", 0.0, 0.0, 0.0, 0.0", " ").Replace("RAW MATERIAL", "SPARE PART");
            //}

            //if (objtype.formtitle == "General Material")
            //{
            //    sSql = sSql.Replace("matrawwidth", "0.0").Replace("matrawdiameter", "0.0").Replace("matrawheight", "0.0").Replace("matrawvolume", "0.0").Replace(", 0.0, 0.0, 0.0, 0.0", " ").Replace("RAW MATERIAL", "GENERAL MATERIAL");
            //}

            if (objtype.formtitle == "Finish Good")
            {
                //sSql = sSql.Replace("matrawres2", "matrawnote").Replace("[Assets Flag]", "[SKU]").Replace("matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume]", " (matrawpackvolume*1000000000 / 28316846.59) AS [CBF]").Replace("matrawwidth, matrawdiameter, matrawheight, matrawvolume", " matrawpackvolume").Replace("[Width], [Diameter], [Height], [Volume]", "[CBF]").Replace("dod.mat", "dod.").Replace("RAW MATERIAL", "FINISH GOOD");
                if (DDLType == "Summary")
                { }
                else
                {
                    if (!DDLGrouping.Contains("CATEGORY"))
                        rptfile = rptfile.Replace("rptStock", "rptStockFG");
                }  
            }
            //if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
            //{
            //    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower()).Replace("Raw", objtype.formref.ToLower());
            //}


            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintUserName", ("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"] + "'").ToString());

            if (DDLType == "Summary")
            {
                rptparam.Add("FilterLength", "");
                rptparam.Add("showFilter", "0");
            }

            rptparam.Add("StartPeriod", Convert.ToDateTime(StartDate).ToString("dd MMM yyyy"));
            rptparam.Add("EndPeriod", Convert.ToDateTime(EndDate).ToString("dd MMM yyyy"));

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                if (DDLType == "Summary")
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                else
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var matraw in rptparam)
                        report.SetParameterValue(matraw.Key, matraw.Value);
                if (DDLType == "Summary")
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                else
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportInPrice(string FormType, string DDLType, string StartDate, string EndDate, string DDLBusinessUnit, string DDLGrouping, string DDLWarehouse, string TextMaterial, string DDLYear, string DDLCurrency, string DDLMonth, bool ShowAnomali, bool AllowNull, string DDLAssets, string ReportType = "")
        {
            var rptfile = "";
            var rptname = "";
            var sWhere = "";
            var objtype = new ReportModels.FullFormType(FormType);

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sWhere = " AND con.cmpcode ='" + DDLBusinessUnit + "'";
            else
                sWhere = " AND con.cmpcode LIKE '%%'";

            if (!string.IsNullOrEmpty(DDLWarehouse))
                sWhere += " AND con.mtrwhoid ='" + DDLWarehouse + "'";

            if (!string.IsNullOrEmpty(TextMaterial))
            {
                sWhere += " AND (";
                sWhere += " m.matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
                sWhere += ")";
            }

            if (DDLAssets != "All")
            {
                sWhere += " AND matrawres2 ='" + DDLAssets + "'";
            }

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptStockSum_Excel2.rpt";
                else
                    rptfile = "rptStockSum2.rpt";

                rptname = "StockSummaryReportInPrice";

                sSql = "SELECT cmpcode, [Oid], [Code], [Description], [Assets Flag], [Safety Stock], [Unit], [Warehouse], SUM([Saldo Awal] + [Saldo Awal Sum]) AS [Saldo Awal], (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Init Saldo Awal] + [Qty In]) ELSE SUM([Qty In]) END) AS [Qty In], SUM([Qty Out]) AS [Qty Out], [Business Unit], [Width], [Diameter], [Height], [Volume], (SUM([Value IDR]) + (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Value IDR 2]) ELSE 0 END)) AS [Value IDR], (SUM([Value USD]) + (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Value USD 2]) ELSE 0 END)) AS [Value USD], 0.0000 [Value IDR 2], 0.000000 [Value USD 2] FROM (";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], 'Non Assets' AS [Assets Flag], matrawsafetystock [Safety Stock], g2.gendesc AS [Unit], '' AS [Warehouse], 0.0000 AS [Saldo Awal], SUM(con.qtyin - con.qtyout) AS [Saldo Awal Sum], 0.0000 AS [Init Saldo Awal], 0.0000 AS [Qty In], 0.0000 AS [Qty Out], divname AS [Business Unit], matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueidr), 4)) AS [Value IDR], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueusd), 6)) AS [Value USD], 0.0000 AS [Value IDR 2], 0.000000 AS [Value USD 2] FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS' AND con.updtime>=CAST('" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00' AS DATETIME)*/ AND con.updtime<CAST('" + StartDate + " 00:00:00' AS DATETIME) " + sWhere + " GROUP BY con.refoid, matrawcode, matrawlongdesc, matrawsafetystock, g2.gendesc, con.cmpcode, divname, matrawwidth, matrawdiameter, matrawheight, matrawvolume UNION ALL ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], 'Non Assets' AS [Assets Flag], matrawsafetystock [Safety Stock], g2.gendesc AS [Unit], '' AS [Warehouse], 0.0000 AS [Saldo Awal], 0.0000 AS [Saldo Awal Sum], 0.0000 AS [Init Saldo Awal], SUM(con.qtyin) AS [Qty In], SUM(con.qtyout) AS [Qty Out], divname AS [Business Unit], matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueidr), 4)) AS [Value IDR], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueusd), 6)) AS [Value USD], 0.0000 AS [Value IDR 2], 0.000000 AS [Value USD 2] FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS'*/ AND con.updtime>=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND con.updtime<=CAST('" + EndDate + " 23:59:59' AS DATETIME) " + sWhere + " GROUP BY con.refoid, matrawcode, matrawlongdesc, matrawsafetystock, g2.gendesc, con.cmpcode, con.refname, con.mtrwhoid, divname, matrawwidth, matrawdiameter, matrawheight, matrawvolume UNION ALL ";

                //TABLE HISTORY
                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], 'Non Assets' AS [Assets Flag], matrawsafetystock [Safety Stock], g2.gendesc AS [Unit], '' AS [Warehouse], 0.0000 AS [Saldo Awal], SUM(con.qtyin - con.qtyout) AS [Saldo Awal Sum], 0.0000 AS [Init Saldo Awal], 0.0000 AS [Qty In], 0.0000 AS [Qty Out], divname AS [Business Unit], matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueidr), 4)) AS [Value IDR], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueusd), 6)) AS [Value USD], 0.0000 AS [Value IDR 2], 0.000000 AS [Value USD 2] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS' AND con.updtime>=CAST('" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00' AS DATETIME)*/ AND con.updtime<CAST('" + StartDate + " 00:00:00' AS DATETIME) " + sWhere + " GROUP BY con.refoid, matrawcode, matrawlongdesc, matrawsafetystock, g2.gendesc, con.cmpcode, divname, matrawwidth, matrawdiameter, matrawheight, matrawvolume UNION ALL ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], 'Non Assets' AS [Assets Flag], matrawsafetystock [Safety Stock], g2.gendesc AS [Unit], '' AS [Warehouse], 0.0000 AS [Saldo Awal], 0.0000 AS [Saldo Awal Sum], 0.0000 AS [Init Saldo Awal], SUM(con.qtyin) AS [Qty In], SUM(con.qtyout) AS [Qty Out], divname AS [Business Unit], matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueidr), 4)) AS [Value IDR], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueusd), 6)) AS [Value USD], 0.0000 AS [Value IDR 2], 0.000000 AS [Value USD 2] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS'*/ AND con.updtime>=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND con.updtime<=CAST('" + EndDate + " 23:59:59' AS DATETIME) " + sWhere + " GROUP BY con.refoid, matrawcode, matrawlongdesc, matrawsafetystock, g2.gendesc, con.cmpcode, con.refname, con.mtrwhoid, divname, matrawwidth, matrawdiameter, matrawheight, matrawvolume ";

                //END OF TABLE HISTORY
                sSql += ") AS tblstock GROUP BY cmpcode, [Oid], [Code], [Description], [Assets Flag], [Safety Stock], [Unit], [Warehouse], [Business Unit], [Width], [Diameter], [Height], [Volume]";

                if (!AllowNull)
                {
                    sSql += " HAVING (SUM([Saldo Awal] + [Saldo Awal Sum]) + (CASE WHEN SUM([Saldo Awal])=0 THEN SUM([Init Saldo Awal] + [Qty In]) ELSE SUM([Qty In]) END) - SUM([Qty Out])) > 0 ";
                }

                sSql += " ORDER BY cmpcode, [Warehouse], [Code]";

            }

            else if (DDLType == "Detail")
            {
                if (ReportType == "XLS")
                {
                    rptfile = "rptStockDtl_Excel2.rpt";
                }
                else
                {
                    rptfile = "rptStockDtl2.rpt";
                }

                rptname = "StockDetailInPriceReport";

                sSql = "SELECT cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], [Warehouse], [Reference], [Date], [Qty In], [Qty Out], [Type], [Business Unit], [Value IDR], [Value USD], [KIK No.], [Department], [Division], [Update Time], [Batch No.] FROM (";

                sSql += "SELECT cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], '' [Warehouse], 'Saldo Awal' AS [Reference], CONVERT(DATETIME, '" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00') AS [Date], SUM([Saldo Awal]) AS [Qty In], 0.0 AS [Qty Out], 1 AS [Type], [Business Unit], ISNULL((SUM([Saldo Awal IDR]) / ISNULL(NULLIF(SUM([Saldo Awal]), 0), 1)), 0.0) AS [Value IDR], ISNULL((SUM([Saldo Awal USD]) / ISNULL(NULLIF(SUM([Saldo Awal]), 0), 1)), 0.0) AS [Value USD], '' AS [KIK No.], '' AS [Department], '' AS [Division], CONVERT(DATETIME, '" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00') AS [Update Time], [Batch No.] FROM (";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal], divname AS [Business Unit], (SUM(con.qtyin * valueidr) - SUM(con.qtyout * valueidr)) AS [Saldo Awal IDR], (SUM(con.qtyin * valueusd) - SUM(con.qtyout * valueusd)) AS [Saldo Awal USD], refno AS [Batch No.] FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00'*/ AND con.trndate < '" + StartDate + " 00:00:00' " + sWhere + " GROUP BY con.cmpcode, con.refoid, matrawcode, matrawlongdesc, matrawres2, g2.gendesc, g1.gendesc, divname, refno UNION ALL ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], 0.0 AS [Saldo Awal], divname AS [Business Unit], 0.0 AS [Saldo Awal IDR], 0.0 AS [Saldo Awal USD], '' AS [Batch No.] FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + " /*UNION ALL*/ ";

                //TABLE HISTORY
                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], (SUM(con.qtyin) - SUM(con.qtyout)) AS [Saldo Awal], divname AS [Business Unit], (SUM(con.qtyin * valueidr) - SUM(con.qtyout * valueidr)) AS [Saldo Awal IDR], (SUM(con.qtyin * valueusd) - SUM(con.qtyout * valueusd)) AS [Saldo Awal USD] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS' AND con.trndate >= '" + Convert.ToDateTime(StartDate).ToString("MM/01/yyyy") + " 00:00:00'*/ AND con.trndate < '" + StartDate + " 00:00:00' " + sWhere + " GROUP BY con.cmpcode, con.refoid, matrawcode, matrawlongdesc, matrawres2, g2.gendesc, g1.gendesc, divname UNION ALL ";

                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], 0.0 AS [Saldo Awal], divname AS [Business Unit], 0.0 AS [Saldo Awal IDR], 0.0 AS [Saldo Awal USD] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + " ";
                //END OF TABLE HISTORY

                sSql += ") AS tbl_saldoawal GROUP BY cmpcode, [Oid], [Code], [Description], [Assets Flag], [Unit], [Business Unit], [Batch No.] UNION ALL ";

                sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], con.note AS [Reference], con.updtime AS [Date], con.qtyin AS [Qty In], con.qtyout AS [Qty Out], 2 AS [Type], divname AS [Business Unit], con.valueidr AS [Value IDR], con.valueusd AS [Value USD], con.refno AS [KIK No.], ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.cmpcode=con.cmpcode AND de.deptoid=con.deptoid), '') AS [Department], ISNULL((SELECT TOP 1 (groupcode + ' - ' + groupdesc) FROM QL_mstdeptgroupdtl dgd INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=dgd.cmpcode AND dg.groupoid=dgd.groupoid WHERE dgd.cmpcode=con.cmpcode AND dgd.deptoid=con.deptoid), '') AS [Division], (CASE WHEN con.formaction IN ('QL_trnassetrecdtl', 'QL_trnstockadj') THEN con.trndate ELSE con.updtime END) AS [Update Time], ISNULL(con.refno, '') AS [Batch No.],  FROM QL_conmat con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS'*/ AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + " /*UNION ALL*/ ";

                //TABLE HISTORY
                //sSql += "SELECT con.cmpcode, con.refoid AS [Oid], matrawcode AS [Code], matrawlongdesc AS [Description], matrawres2 AS [Assets Flag], g2.gendesc AS [Unit], g1.gendesc AS [Warehouse], con.note AS [Reference], con.updtime AS [Date], con.qtyin AS [Qty In], con.qtyout AS [Qty Out], 2 AS [Type], divname AS [Business Unit], con.valueidr AS [Value IDR], con.valueusd AS [Value USD], con.refno AS [KIK No.], ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.cmpcode=con.cmpcode AND de.deptoid=con.deptoid), '') AS [Department], ISNULL((SELECT TOP 1 (groupcode + ' - ' + groupdesc) FROM QL_mstdeptgroupdtl dgd INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=dgd.cmpcode AND dg.groupoid=dgd.groupoid WHERE dgd.cmpcode=con.cmpcode AND dgd.deptoid=con.deptoid), '') AS [Division], (CASE WHEN con.formaction IN ('QL_trnassetrecdtl', 'QL_trnstockadj') THEN con.trndate ELSE con.updtime END) AS [Update Time] FROM QL_conmat_hist con INNER JOIN QL_mstmatraw m ON matrawoid=con.refoid INNER JOIN QL_mstgen g1 ON g1.genoid=mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrawunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.refname='RAW MATERIAL' /*AND con.type<>'RMIS'*/ AND con.trndate >= '" + StartDate + " 00:00:00' AND con.trndate <= '" + EndDate + " 23:59:59' " + sWhere + " ";

                //END OF TABLE HISTORY
                sSql += ") AS tblstock ORDER BY cmpcode, [Type], [Date], [Update Time], [Warehouse], [Code]";

            }
            else if (DDLType == "Closing")
            {
                rptname = "StockClosingReport";
                if (ReportType == "XLS")
                    rptfile = "crStockClosing_Excel.rpt";
                else
                {
                    rptfile = "crStockClosing.rpt";
                }

                sSql = "SELECT con.cmpcode, di.divname bunit, matrawoid oid, matrawcode kode, matrawlongdesc deskripsi, g.gendesc gudang, con.amtawal_idr, con.amtawal_usd, con.saldoawal, con.qtyin, con.qtyout, con.qtyadjin, con.qtyadjout, con.saldoakhir FROM QL_crdmtr con INNER JOIN QL_mstmatraw m on matrawoid=con.refoid INNER JOIN QL_mstgen g on g.genoid=con.mtrwhoid INNER JOIN QL_mstdivision di ON di.cmpcode=con.cmpcode WHERE con.closeuser<>'' AND con.refname='RAW MATERIAL' AND con.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' " + sWhere + " ORDER BY gudang, kode";
            }

            //Compatibility
            if (objtype.formtitle == "Spare Part")
            {
                sSql = sSql.Replace("matrawwidth", "0.0").Replace("matrawdiameter", "0.0").Replace("matrawheight", "0.0").Replace("matrawvolume", "0.0").Replace(", 0.0, 0.0, 0.0, 0.0", " ").Replace("RAW MATERIAL", "SPARE PART").Replace("SUM(con.qtyin - con.qtyout)", "(SUM(con.qtyin) - SUM(con.qtyout))").Replace("'Non Assets' AS", "matrawres2 AS").Replace(",g1.gendesc, divname", ",g1.gendesc, matrawres2, divname").Replace("[Value IDR], [Value USD]", "[Value IDR], [Value USD], [Value IDR], [Value USD]").Replace("matrawlongdesc, matrawsafetystock,", "matrawlongdesc, matrawsafetystock, matrawres2,");
            }

            if (objtype.formtitle == "General Material")
            {
                sSql = sSql.Replace("matrawwidth", "0.0").Replace("matrawdiameter", "0.0").Replace("matrawheight", "0.0").Replace("matrawvolume", "0.0").Replace(", 0.0, 0.0, 0.0, 0.0", " ").Replace("SUM(con.qtyin - con.qtyout)", "((SUM(con.qtyin)) - (SUM(con.qtyout)))").Replace("'Non Assets' AS", "matrawres2 AS").Replace(", g1.gendesc, divname", ", g1.gendesc, matrawres2, divname").Replace("[Value IDR], [Value USD]", "[Value IDR], [Value USD], [Value IDR], [Value USD]").Replace("RAW MATERIAL", "GENERAL MATERIAL").Replace("matrawlongdesc, matrawsafetystock,", "matrawlongdesc, matrawsafetystock, matrawres2,");
            }

            if (objtype.formtitle == "Finish Good")
            {
                if (DDLType == "Summary")
                {
                    sSql = sSql.Replace("[Assets Flag]", "[SKU]").Replace("'Non Assets'", "matrawnote").Replace("SUM(con.qtyin - con.qtyout)", "(SUM(con.qtyin) - SUM(con.qtyout))").Replace("matrawwidth AS [Width], matrawdiameter AS [Diameter], matrawheight AS [Height], matrawvolume AS [Volume], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueidr), 4)) AS [Value IDR], SUM(ROUND(((con.qtyin - con.qtyout) * con.valueusd), 6)) AS [Value USD], 0.0000 AS [Value IDR 2], 0.000000 AS [Value USD 2]", "SUM((con.qtyin - con.qtyout) * con.valueidr) AS [Value IDR], SUM((con.qtyin - con.qtyout) * con.valueusd) AS [Value USD], 0.0 AS [Value IDR 2], 0.0 AS [Value USD 2], (itempackvolume*1000000000 / 28316846.59) [CBF]").Replace("AND con.updtime<CAST('" + StartDate + " 00:00:00' AS DATETIME)", "AND con.updtime < ('" + StartDate + " 00:00:00')").Replace("matrawlongdesc, matrawsafetystock,", "matrawlongdesc, matrawnote, matrawsafetystock,").Replace("divname, matrawwidth, matrawdiameter, matrawheight, matrawvolume", "divname, matrawpackvolume").Replace("0.0000", "0.0").Replace("[Width], [Diameter], [Height], [Volume]", "[CBF]").Replace("AND con.updtime<CAST('" + StartDate + " 00:00:00' AS DATETIME)", "AND con.updtime < '" + StartDate + " 00:00:00'").Replace("RAW MATERIAL", "FINISH GOOD");
                }
                else if (DDLType == "Detail")
                {
                    sSql = sSql.Replace("[Assets Flag]", "[SKU]").Replace("matrawnote", "matrawres2").Replace("[Saldo Awal USD] FROM QL_conmat", "[Saldo Awal USD], (matrawpackvolume*1000000000 / 28316846.59) [CBF] FROM QL_conmat").Replace("divname UNION ALL", "divname, matrawpackvolume UNION ALL").Replace("[Business Unit] UNION ALL", "[Business Unit], [CBF] UNION ALL").Replace("[Update Time] FROM QL_conmat", "[Update Time], (matrawpackvolume*1000000000 / 28316846.59) [CBF] FROM QL_conmat").Replace("AS [Update Time] FROM (", "AS [Update Time], [CBF] FROM (").Replace("RAW MATERIAL", "FINISH GOOD");
                }
                else
                {
                    sSql = sSql.Replace("RAW MATERIAL", "FINISH GOOD");
                }

                rptfile = rptfile.Replace("rptStock", "rptStockFG");
            }

            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
            {
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower()).Replace("Raw", objtype.formref.ToLower());
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintUserName", ("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"] + "'").ToString());

            if (DDLType == "Summary")
            {
                rptparam.Add("ShowAnomali", ShowAnomali);

                if (objtype.formtitle != "Finish Good")
                {
                    rptparam.Add("FilterLength", "");
                    rptparam.Add("showFilter", "0");
                }

            }
            else if (DDLType == "Detail")
            {
                rptparam.Add("currency", DDLCurrency);
            }
            else if (DDLType == "Closing")
            {
                rptparam.Add("periode", Convert.ToDateTime(DDLMonth + " " + DDLYear).ToString("MMMM yyyy"));
                rptparam.Add("currency", DDLCurrency);
            }

            rptparam.Add("StartPeriod", Convert.ToDateTime(StartDate).ToString("dd MMM yyyy"));
            rptparam.Add("EndPeriod", Convert.ToDateTime(EndDate).ToString("dd MMM yyyy"));


            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                if (DDLType == "Closing")
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                else
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var matraw in rptparam)
                        report.SetParameterValue(matraw.Key, matraw.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}