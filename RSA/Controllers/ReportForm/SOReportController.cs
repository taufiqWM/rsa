﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class SOReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string soitemmstnote { get; set; }
            public string soitemcustpo { get; set; }
        }

        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT 0 seq, som.soitemmstoid,som.soitemno, soitemcustref soitemcustpo, soitemdate ,c.custname,soitemmstnote FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if(MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
               
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += "ORDER BY som.soitemno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoitemmst som WHERE som.cmpcode='" + CompnyCode + "'";  
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += ") ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.reqrabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string prabno, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemdesc, '' itemtype, g.gendesc unit FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=rd.cmpcode AND som.soitemmstoid=rd.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN ql_mstgen g ON g.genoid=soitemunitoid  WHERE rd.cmpcode='" + CompnyCode +"'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND "+ ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL(string sType = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");

            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

        }
        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            InitDDL();
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soitemno, string MCBtgl, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");


            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                //rptfile = "rptSOSumPdf.rpt";
                if (reporttype == "XLS")
                    rptfile = "rptSOSumXls.rpt";
                else
                    rptfile = "rptSOSumPdf.rpt";
                rptname = "SO_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptSODtlXls.rpt";
                else
                {
                    rptfile = "rptSODtlPdf.rpt";
                }
                rptname = "SO_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = ",sod.soitemdtletd [ETD DTL], soitemdtlseq [No.], itemcode [Code], m.itemlongdesc [Material], itemnote [SKU], soitemqty [Qty], g2.gendesc [Unit], soitemprice [Price], soitemdtlamt [Detail Amount],  soitemdtldiscamt [Disc Dtl Amt], soitemdtlnetto [Detail Netto], soitemdtlnote [Detail Note], 0.00 [Pack Volume FG],soitemdtletd [dtl ETD], isnull((select sum(doitemqty) from ql_trndoitemdtl x inner join ql_trndoitemmst xm on x.doitemmstoid=xm.doitemmstoid where x.soitemdtloid=sod.soitemdtloid and xm.doitemmststatus ='Post'),0.0) as [osbokqty], soitemqty-(isnull((select sum(shipmentitemqty) from QL_trnshipmentitemdtl x inner join ql_trnshipmentitemmst xm on x.shipmentitemmstoid=xm.shipmentitemmstoid inner join QL_trndoitemdtl dx on  dx.doitemdtloid=x.doitemdtloid  where dx.soitemdtloid=sod.soitemdtloid),0.0) - isnull((select sum(sxd.sretitemqty) from QL_trnsretitemdtl sxd inner join QL_trnshipmentitemdtl x on sxd.shipmentitemdtloid=x.shipmentitemdtloid  inner join ql_trnshipmentitemmst xm on x.shipmentitemmstoid=xm.shipmentitemmstoid inner join QL_trndoitemdtl dx on  dx.doitemdtloid=x.doitemdtloid  where dx.soitemdtloid=sod.soitemdtloid),0) ) - isnull(sod.soclosingqty,0.0) as [osqty], isnull(sod.soclosingqty,0.0) [closingqty] ";
                Join = " INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=som.cmpcode AND sod.soitemmstoid=som.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = sod.itemoid INNER JOIN ql_mstgen g2 ON g2.genoid = soitemunitoid";
            }
            sSql = "SELECT 'RCA' AS [Business Unit], som.cmpcode [CMPCODE], CONVERT(VARCHAR(20), som.soitemmstoid) [Draft No.], (select x.gendesc from ql_mstgen x where x.genoid=som.divgroupoid) [Divisi], som.soitemmstoid [ID], soitemtype [Type], soitemdate [Date], GETDATE() [ETD], soitemno [SO No.], custname [Customer], custcode [Customer Code], soitemcustref [Customer Ref.], som.soitemdate [Cust Ref Date], currcode [Currency], g1.gendesc [Payment Type], 0.00 [Daily Rate To IDR], '' [Daily Rate To USD], rate2idrvalue [Monthly Rate To IDR], rate2usdvalue [Monthly Rate To USD], soitemtotalamt [Total Amt], 0.00 [Total Disc Dtl Amt], soitemtotalnetto [Total Netto], '' [Tax Type], soitemvat [Tax Pct], soitemvatidr [Tax Amount], soitemgrandtotalamt [Grand Total Amt], soitemmstnote [Header Note], soitemmststatus [Status], '' AS [App User], GETDATE() AS [App Date], som.createuser AS [Create User], som.createtime AS [Create Date], 'FG' AS [TipeneX],0.00 AS [Total CBF], som.closereason, som.closeuser, som.closetime, som.cmpcode[Division],isnull(som.revisereason,'-') as [Revised Note], som.cmpcode[Division Code], isnull(soitemmstres2,'Material') [Type], isnull(soitemmstres1,'0') [Toleransi] " + Dtl + " FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid INNER JOIN QL_mstcurr curr ON curr.curroid = som.curroid INNER JOIN ql_mstgen g1 ON g1.genoid = soitempaytypeoid LEFT JOIN QL_mstrate2 r2 ON r2.rate2oid = som.rate2oid " + Join + " WHERE som.cmpcode='"+ CompnyCode +"'";
            if (StartPeriod != "" && EndPeriod != "")
            {
                if (MCBtgl != null)
                {
                    if (MCBtgl != "")
                    {
                        sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                }

            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND som.divgroupoid=" + DDLdivgroupoid;
            }
            if (soitemno != null) {
                if (soitemno != "")
                {
                    string[] arr = soitemno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
           
            if(TextCust!=null)
            {
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextNomor != null)
            {
                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
           

            if (DDLType == "Detail")
            {
                if (TextMaterial != null)
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
               
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}