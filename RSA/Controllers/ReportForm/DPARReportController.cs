﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class DPARReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL()
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;


        }

        [HttpPost]
        public ActionResult GetCustData(string DDLBusinessUnit, string StartDate, string EndDate, string DDLType)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, custcode [Code], custname [Name], '' [Type], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' ";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCOAData(string DDLBusinessUnit, string StartDate, string EndDate, string TextCOA, string TextSupplier)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_DP_AR");

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCOA");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);

            var result = "SELECT 0 seq, acctgcode [COA], acctgdesc [COA Description] FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private string GetAcctgOidByCode(string TextCOA)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(acctgoid AS VARCHAR(10)) FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgcode IN ('" + TextCOA.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        // GET: ARReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("DPARReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Goods";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string StartDate, string EndDate, string TextCustomer, string DDLType, string ReportType, string DDLCurrency, bool cbInvoice, bool cbHide, string TextCOA, string DDLBusinessUnit_Text)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var sGroup = "";

            if (cbInvoice)
                sGroup = "DtlPerNota";

            var rptfile = ""; var rptname = "DOWNPAYMENTARREPORT";

            if (DDLType == "Detail")
            {
                if (ReportType != "XLS")
                {
                    if (DDLCurrency != "2")
                        rptfile = "crKartuDPAR" + sGroup + ".rpt";
                    else
                        rptfile = "crKartuDPARRate" + sGroup + ".rpt";
                }
                else
                {
                    if (DDLCurrency != "2")
                        rptfile = "crKartuDPAR" + sGroup + "Xls.rpt";
                    else
                        rptfile = "crKartuDPARRate" + sGroup + "Xls.rpt";
                }
            }
            else
            {
                if (ReportType != "XLS")
                {
                    if (DDLCurrency != "2")
                        rptfile = "crKartuDPARSum.rpt";
                    else
                        rptfile = "crKartuDPARRateSum.rpt";
                }
                else
                {
                    if (DDLCurrency != "2")
                        rptfile = "crKartuDPARXlsSum.rpt";
                    else
                        rptfile = "crKartuDPARRateXlsSum.rpt";
                }
            }

            var swhere = ""; var swheresupp = "";
            if (!string.IsNullOrEmpty(TextCustomer))
                swhere = " WHERE custname IN ('" + TextCustomer.Replace(";", "', '") + "')";
            if (!string.IsNullOrEmpty(TextCustomer))
                swheresupp = " WHERE s.custname IN ('" + TextCustomer.Replace(";", "', '") + "')";

            System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
            var awal = DateTime.ParseExact(StartDate, "MM/dd/yyyy", AppsCultureInfo);
            var akhir = DateTime.ParseExact(EndDate, "MM/dd/yyyy", AppsCultureInfo);

            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("cmpcode", CompnyCode);
            rptparam.Add("start", awal);
            rptparam.Add("finish", akhir);
            rptparam.Add("swhere", swheresupp);
            if (cbHide)
            {
                if (DDLCurrency == "IDR")
                    rptparam.Add("swherezero", " WHERE sawalidr<>0 OR ISNULL(amtinidr,0)<>0 OR ISNULL(amtoutidr,0)<>0 ");
                else if (DDLCurrency == "USD")
                    rptparam.Add("swherezero", " WHERE sawalusd<>0 OR ISNULL(amtinusd,0)<>0 OR ISNULL(amtoutusd,0)<>0 ");
                else
                    rptparam.Add("swherezero", " WHERE sawal<>0 OR sawalidr<>0 OR sawalusd<>0 OR ISNULL(amtin,0)<>0 OR ISNULL(amtinidr,0)<>0 OR ISNULL(amtinusd,0)<>0 OR ISNULL(amtout,0)<>0 OR ISNULL(amtoutidr,0)<>0 OR ISNULL(amtout,0)<>0 ");
            }
            else
                rptparam.Add("swherezero", "");

            var acctgoid = GetAcctgOidByCode(TextCOA);

            if (TextCOA == "")
                rptparam.Add("swhereacc", "");
            else
            {
                rptparam.Add("swhereacc", " AND dp.acctgoid IN(" + acctgoid + ") ");
            }
            rptparam.Add("currvalue", DDLCurrency);
            if (DDLType == "Detail")
            {
                if (sGroup != "DtlPerNota")
                    rptparam.Add("companyname", DDLBusinessUnit_Text);
            }
            else
                rptparam.Add("companyname", DDLBusinessUnit_Text);
            
            //DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            rptparam.Add("periodreport", Convert.ToDateTime(StartDate).ToString("dd MM yyyy") + "-" + Convert.ToDateTime(EndDate).ToString("dd MM YYYY"));


            if (ReportType == "")
            {
                //this.HttpContext.Session["rptsource"] = dtRpt;
                //this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                //report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                ClassProcedure.SetDBLogonForReport(report);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }

}