﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class SReturnReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            //if (Session["CompnyCode"].ToString() != CompnyCode)
            //    sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<string> MCBStatus = new List<string> { "In Process", "In Approval", "Approved", "Closed", "Cancel", "Rejected", "Revised" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult GetCustomerData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "'  AND activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND custoid IN (SELECT custoid from QL_trnSRetrawmst sretm INNER JOIN QL_trnSRetrawdtl sretd ON sretm.SRetrawmstoid=sretd.SRetrawmstoid WHERE sretm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND custoid IN (SELECT custoid from QL_trnSRetrawmst sretm INNER JOIN QL_trnSRetrawdtl sretd ON sretm.SRetrawmstoid=sretd.SRetrawmstoid WHERE sretm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY custcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSRData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust, string TextShip, string DDLType, string TextNomor)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, sretm.SRetrawmstoid [Draft No.], sretm.SRetrawno [Return No.], sretm.SRetrawdate [Return Date], CONVERT(VARCHAR(10), sretm.SRetrawdate , 101) , c.custname [Customer], sretm.SRetrawmstnote [Note], sretm.SRetrawmststatus [Status] FROM QL_trnSRetrawmst sretm INNER JOIN QL_trnSRetrawdtl sretd ON sretm.SRetrawmstoid=sretd.SRetrawmstoid INNER JOIN QL_mstcust c ON sretm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE sretm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE sretm.cmpcode LIKE '%%'";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus + "')";
                
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";


                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextShip))
                    {
                        sSql += "AND sretm.SRetrawmstoid IN (SELECT sretm2.SRetrawmstoid FROM QL_trnSRetrawmst sretm2 INNER JOIN QL_trnShipmentrawmst shm ON shm.Shipmentrawmstoid=sretm2.Shipmentrawmstoid ";
                        sSql += " WHERE";
                        sSql += " Shipmentrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                        sSql += ")";
                    }

                }

                sSql += " ORDER BY sretm.SRetrawdate DESC, sretm.SRetrawmstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust, string DDLNomor, string TextNomor, string TextShip, string DDLType)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.matrawlongdesc [Description], m.matrawcode [Code], g2.gendesc [Unit] FROM QL_trnSRetrawdtl sretd INNER JOIN QL_trnSRetrawmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.SRetrawmstoid=sretd.SRetrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=sretd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=sretd.SRetrawunitoid INNER JOIN QL_mstcust c ON sretm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE sretm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE sretm.cmpcode LIKE '%%' ";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Return No.")
                    {
                        sSql += " AND (";
                        sSql += " sretm.SRetrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                        sSql += ")";
                    } else {
                        sSql += " AND (";
                        sSql += " sretm.SRetrawmstoid IN ('" + TextNomor.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if(DDLType == "Detail") {
                    if (!string.IsNullOrEmpty(TextShip)) {
                        sSql += " AND sretm.SRetrawmstoid IN (SELECT sretm2.SRetrawmstoid FROM QL_trnSRetrawmst sretm2 INNER JOIN QL_trnShipmentrawmst shm ON shm.Shipmentrawmstoid=sretm2.Shipmentrawmstoid";
                        sSql += " WHERE Shipmentrawno IN ('" + TextShip.Replace(";", "','") + "')";
                        sSql += ")";

                    }

                }

                sSql += " ORDER BY m.matrawcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSHIPData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCust)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, shm.Shipmentrawmstoid [Draft No.], shm.Shipmentrawno [Shipment No.], shm.Shipmentrawdate, CONVERT(VARCHAR(10), shm.Shipmentrawdate, 101) [Shipment Date], c.custname [Customer], shm.Shipmentrawmstnote [Note], shm.Shipmentrawmststatus [Status] FROM QL_trnShipmentrawmst shm INNER JOIN QL_trnShipmentrawdtl shd ON shm.Shipmentrawmstoid=shd.Shipmentrawmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE shm.Shipmentrawmstoid IN (SELECT sretm.Shipmentrawmstoid from QL_trnSRetrawmst sretm INNER JOIN QL_trnSRetrawdtl sretd ON sretm.SRetrawmstoid=sretd.SRetrawmstoid INNER JOIN QL_mstcust c ON sretm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE sretm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE shm.Shipmentrawmstoid IN (SELECT sretm.Shipmentrawmstoid from QL_trnSRetrawmst sretm INNER JOIN QL_trnSRetrawdtl sretd ON sretm.SRetrawmstoid=sretd.SRetrawmstoid INNER JOIN QL_mstcust c ON sretm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE sretm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(TextCust))
                    sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                
                sSql += ") ORDER BY shm.Shipmentrawdate DESC, shm.Shipmentrawmstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSHIP");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }


        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextCust, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string TextShip)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var objOfType = new ReportModels.FullFormType(FormType);
            var typeOfMaterial = objOfType.mattype.ToLower();

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                {
                    if (typeOfMaterial == "item")
                    {
                        rptfile = "rptSretItem_SumXls.rpt";
                    }
                    else
                    {
                        rptfile = "rptSret_SumXls.rpt";
                    }
                }
                else {
                    //if (typeOfMaterial == "item")
                    //{
                    //    rptfile = "rptSretItem_SumPdf.rpt";
                    //}
                    //else
                    //{
                    rptfile = "rptSret_SumPdf.rpt";
                    //}
                }
                rptname = "SALESRETURNRAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS") {
                    if (typeOfMaterial == "item")
                    {
                        rptfile = "rptSretItem_DtlSretXls.rpt";
                    }
                    else {
                        rptfile = "rptSret_DtlSretXls.rpt";
                    }
                }
                else
                {
                    if (DDLGrouping == "sretm.SRetrawno")
                    {
                        if (typeOfMaterial == "item")
                        {
                            rptfile = "rptSretItem_DtlSretPdf.rpt";
                        }
                        else {
                            rptfile = "rptSret_DtlSretPdf.rpt";
                        }
                    }
                    else if (DDLGrouping == "matcode")
                    {
                        if (typeOfMaterial == "item")
                        {
                            rptfile = "rptSretItem_DtlMatCodePdf.rpt";
                        }
                        else {
                            rptfile = "rptSret_DtlMatCodePdf.rpt";
                        }
                    }
                    else {
                        if (typeOfMaterial == "item")
                        {
                            rptfile = "rptSretItem_DtlCustPdf.rpt";
                        }
                        else
                        {
                            rptfile = "rptSret_DtlCustPdf.rpt";
                        }
                    }
                }
                rptname = "SALESRETURNRAWMATERIALDETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , SRetrawdtlseq [No.], matrawcode [Code], matrawlongdesc [Material], SRetrawqty [Qty], g1.gendesc [Unit], SRetrawdtlnote [Detail Note], 0.0 [Pack Volume FG] ";
                Join = " INNER JOIN QL_trnSRetrawdtl sretd ON sretd.cmpcode=sretm.cmpcode AND sretd.SRetrawmstoid=sretm.SRetrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=sretd.matrawoid INNER JOIN QL_mstgen g1 ON g1.genoid=SRetrawunitoid ";
            }
            sSql = " SELECT (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=sretm.cmpcode) [Business Unit], '' AS [BU Address], '' AS [BU City], '' AS [BU Province], '' AS [BU Country], '' AS [BU Telp. 1], '' AS [BU Telp. 2], '' AS [BU Fax 1], '' AS [BU Fax 2], '' AS [BU Email], '' AS [BU Post Code], CONVERT(VARCHAR(20), sretm.SRetrawmstoid) [Draft No.], SRetrawno [Return No.], SRetrawdate [Return Date], custcode [Customer Code], custname [Customer], Shipmentrawno [Shipment No.], SRetrawmststatus [Status], SRetrawmstnote [Header Note], UPPER(sretm.createuser) [Create User], sretm.createtime [Create Datetime], ISNULL(UPPER(sretm.approvaluser), '') [Approval User], ISNULL(sretm.approvaldatetime, CONVERT(DATETIME, '01/01/1900')) [Approval Datetime], ISNULL(SRetrawmstres2, '') [Return Type], 'RM' AS [SRET Material Type],(SELECT SUM((0.0*1000000000 / 28316846.59)*sretd2.SRetrawqty) FROM QL_trnSRetrawdtl sretd2 INNER JOIN QL_mstmatraw i ON i.matrawoid=sretd2.matrawoid WHERE sretd2.SRetrawmstoid=sretm.SRetrawmstoid AND sretd2.cmpcode = sretm.cmpcode) AS [Grand Total CBF], currcode [Currency] " + Dtl + " ,'' AS [Division]  FROM QL_trnSRetrawmst sretm INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid INNER JOIN QL_trnShipmentrawmst shm ON shm.cmpcode=sretm.cmpcode AND shm.Shipmentrawmstoid=sretm.Shipmentrawmstoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid " + Join;

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE sretm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE sretm.cmpcode LIKE '%%' ";

            if (!string.IsNullOrEmpty(TextCust))
                sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Return No.")
                {
                    sSql += " AND (";
                    sSql += " sretm.SRetrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                    sSql += ")";
                }
                else
                {
                    sSql += " AND (";
                    sSql += " sretm.SRetrawmstoid IN ('" + TextNomor.Replace(";", "','") + "')";
                    sSql += ")";
                }
            }

           

            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextShip))
                {
                    sSql += " AND sretm.SRetrawmstoid IN (SELECT sretm2.SRetrawmstoid FROM QL_trnSRetrawmst sretm2 INNER JOIN QL_trnShipmentrawmst shm ON shm.Shipmentrawmstoid=sretm2.Shipmentrawmstoid";
                    sSql += " WHERE ";
                    sSql += " Shipmentrawno IN ('" + TextShip.Replace(";", "','") + "')";
                    sSql += ")";
                }
                if (!string.IsNullOrEmpty(TextMaterial))
                {
                    sSql += " AND (";
                    sSql += " m.matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
                    sSql += ")";
                }

            }

            if (DDLType == "Summary")
            {
                if (DDLGrouping == "sretm.SRetrawno")
                    sSql += " ORDER BY sretm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , sretm.SRetrawmstoid " + DDLOrderBy;
                
            }
            else
            {
                if (DDLGrouping == "sretm.SRetrawno")
                {
                    sSql += " ORDER BY sretm.cmpcode ASC, sretm.SRetrawno " + DDLOrderBy + " , sretm.SRetrawmstoid " + DDLOrderBy + ", m.matrawcode " + DDLOrderBy;
                }
                else if (DDLGrouping == "matcode")
                {
                    if (DDLSorting == "c.custname")
                        sSql += " ORDER BY sretm.cmpcode ASC, m.matrawcode " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                    else if (DDLSorting == "sretm.SRetrawno")
                        sSql += " ORDER BY sretm.cmpcode ASC, m.matrawcode " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , sretm.SRetrawno " + DDLOrderBy;
                    else
                        if (DDLSorting == "sretm.SRetrawno")
                        sSql += " ORDER BY pretm.cmpcode ASC, c.custname ASC, " + DDLSorting + " " + DDLOrderBy + " ,  sretm.SRetrawmstoid " + DDLOrderBy;
                    else if (DDLSorting == "matcode")
                        sSql += " ORDER BY sretm.cmpcode ASC, c.custname ASC, m.matrawcode " + DDLOrderBy;
                }


            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportInPrice(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextCust, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string TextShip)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptSret_SumXls2.rpt";
                else
                    rptfile = "rptSret_SumPdf2.rpt";
                rptname = "SALESRETURNRAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptSret_DtlSretXls2.rpt";
                else
                {
                    if (DDLGrouping == "sretm.SRetrawno")
                        rptfile = "rptSret_DtlSretPdf2.rpt";
                    else if (DDLGrouping == "matcode")
                        rptfile = "rptSret_DtlMatCodePdf2.rpt";
                    else
                        rptfile = "rptSret_DtlCustPdf2.rpt";
                }
                rptname = "SALESRETURNRAWMATERIALDETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , SRetrawdtlseq [No.], matrawcode [Code], matrawlongdesc [Material], SRetrawqty [Qty], g1.gendesc [Unit], SRetrawdtlnote [Detail Note], 0.0 [Pack Volume FG] ";
                Join = " INNER JOIN QL_trnSRetrawdtl sretd ON sretd.cmpcode=sretm.cmpcode AND sretd.SRetrawmstoid=sretm.SRetrawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=sretd.matrawoid INNER JOIN QL_mstgen g1 ON g1.genoid=SRetrawunitoid ";
                //With Price
                Dtl += " , sretrawvalueidr AS [Value IDR], sretrawvalueusd AS [Value USD] ";

            }

            //Price
            Dtl += " , (SELECT SUM(ISNULL(sretrawvalueidr,0.0)*ISNULL(sretrawqty,0.0)) FROM QL_trnsretrawdtl sretd WHERE sretd.cmpcode=sretm.cmpcode AND sretd.sretrawmstoid=sretm.sretrawmstoid) AS [Total Value IDR], (SELECT SUM(ISNULL(sretrawvalueusd,0.0)*ISNULL(sretrawqty,0.0)) FROM QL_trnsretrawdtl sretd WHERE sretd.cmpcode=sretm.cmpcode AND sretd.sretrawmstoid=sretm.sretrawmstoid) AS [Total Value USD] ";


            sSql = " SELECT (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=sretm.cmpcode) [Business Unit], '' AS [BU Address], '' AS [BU City], '' AS [BU Province], '' AS [BU Country], '' AS [BU Telp. 1], '' AS [BU Telp. 2], '' AS [BU Fax 1], '' AS [BU Fax 2], '' AS [BU Email], '' AS [BU Post Code], CONVERT(VARCHAR(20), sretm.SRetrawmstoid) [Draft No.], SRetrawno [Return No.], SRetrawdate [Return Date], custcode [Customer Code], custname [Customer], Shipmentrawno [Shipment No.], SRetrawmststatus [Status], SRetrawmstnote [Header Note], UPPER(sretm.createuser) [Create User], sretm.createtime [Create Datetime], ISNULL(UPPER(sretm.approvaluser), '') [Approval User], ISNULL(sretm.approvaldatetime, CONVERT(DATETIME, '01/01/1900')) [Approval Datetime], ISNULL(SRetrawmstres2, '') [Return Type], 'RM' AS [SRET Material Type],(SELECT SUM((0.0*1000000000 / 28316846.59)*sretd2.SRetrawqty) FROM QL_trnSRetrawdtl sretd2 INNER JOIN QL_mstmatraw i ON i.matrawoid=sretd2.matrawoid WHERE sretd2.SRetrawmstoid=sretm.SRetrawmstoid AND sretd2.cmpcode = sretm.cmpcode) AS [Grand Total CBF], currcode [Currency] " + Dtl + " ,'' AS [Division] FROM QL_trnSRetrawmst sretm INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid INNER JOIN QL_trnShipmentrawmst shm ON shm.cmpcode=sretm.cmpcode AND shm.Shipmentrawmstoid=sretm.Shipmentrawmstoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid " + Join;



            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE sretm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE sretm.cmpcode LIKE '%%' ";

            if (!string.IsNullOrEmpty(TextCust))
                sSql += " AND c.custcode IN ('" + TextCust.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND sretm.sretrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Return No.")
                {
                    sSql += " AND (";
                    sSql += " sretm.SRetrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                    sSql += ")";
                }
                else
                {
                    sSql += " AND (";
                    sSql += " sretm.SRetrawmstoid IN ('" + TextNomor.Replace(";", "','") + "')";
                    sSql += ")";
                }
            }

            if (DDLType == "Detail") { 
                //Filter Shipment
                if (!string.IsNullOrEmpty(TextShip)) {
                    sSql += " AND sretm.SRetrawmstoid IN (SELECT sretm2.SRetrawmstoid FROM QL_trnSRetrawmst sretm2 INNER JOIN QL_trnShipmentrawmst shm ON shm.Shipmentrawmstoid=sretm2.Shipmentrawmstoid ";
                    sSql += " WHERE ";
                    sSql += " Shipmentrawno IN ('" + TextShip.Replace(";", "','") + "')";
                    sSql += ")";
                }

                //Filter Material
                if (!string.IsNullOrEmpty(TextMaterial)) {
                    sSql += " AND (";
                    sSql += " m.matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
                    sSql += ")";
                }
            }


            if (DDLType == "Summary")
            {
                if (DDLGrouping == "sretm.SRetrawno")
                    sSql += " ORDER BY sretm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , sretm.SRetrawmstoid " + DDLOrderBy;

            }
            else
            {
                if (DDLGrouping == "sretm.SRetrawno")
                {
                    sSql += " ORDER BY sretm.cmpcode ASC, sretm.SRetrawno " + DDLOrderBy + " , sretm.SRetrawmstoid " + DDLOrderBy + ", m.matrawcode " + DDLOrderBy;
                }
                else if (DDLGrouping == "matcode")
                {
                    if (DDLSorting == "c.custname")
                        sSql += " ORDER BY sretm.cmpcode ASC, m.matrawcode " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                    else if (DDLSorting == "sretm.SRetrawno")
                        sSql += " ORDER BY sretm.cmpcode ASC, m.matrawcode " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , sretm.SRetrawno " + DDLOrderBy;
                    else
                        if (DDLSorting == "sretm.SRetrawno")
                        sSql += " ORDER BY pretm.cmpcode ASC, c.custname ASC, " + DDLSorting + " " + DDLOrderBy + " ,  sretm.SRetrawmstoid " + DDLOrderBy;
                    else if (DDLSorting == "matcode")
                        sSql += " ORDER BY sretm.cmpcode ASC, c.custname ASC, m.matrawcode " + DDLOrderBy;
                }


            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}