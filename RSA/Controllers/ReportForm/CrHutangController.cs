﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class CrHutangController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = "RUKUN CITRA ABADI";
        private string sSql = "";

        private void InitDDL()
        {
            sSql =" SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid = con.acctgoid WHERE payrefoid = 0";
            var DDLCOA = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }

        [HttpPost]
        public ActionResult GetSuppData(string StartDate, string EndDate, string TextCustomer)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode [Code], suppname [Name], suppaddr [Address] FROM QL_mstsupp WHERE (suppcode LIKE '%" + "%' OR suppname LIKE '%" + "%') AND suppoid IN (SELECT suppoid FROM QL_conap) ORDER BY suppname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetSuppoidByCode(string TextCustomer)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(suppoid AS VARCHAR(10)) FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppcode IN ('" + TextCustomer.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string StartDate, string EndDate, string DDLType, string ReportType, string TextCustomer, string DDLCOA, bool cbInvoice, bool cbHide, string DDLMonth, string DDLYear, string DDLMonth_Text, string DDLYear_Text, string DateNya)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "CrHutangSumPdf.rpt";
                else
                    rptfile = "CrHutangSumPdf.rpt";
                rptname = "KARTU_HIUTANG_SUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "CrHutangDtlPdf.rpt";
                else
                {
                    rptfile = "CrHutangDtlPdf.rpt";
                }
                rptname = "KARTU_HUTANG_DETAIL";
            }

            var swhere = ""; var sWheresupp = "";
            var suppoid = GetSuppoidByCode(TextCustomer);
            if (!string.IsNullOrEmpty(TextCustomer))
            {
                swhere = " WHERE suppoid IN ('" + suppoid.Replace(";", "', '") + "')";
                sWheresupp = " WHERE s.suppoid IN ('" + suppoid.Replace(";", "', '") + "')";
            }

            var sFilterCOA = "";
            if (DDLCOA != "")
            {
                sFilterCOA = " AND c.acctgoid=" + DDLCOA;
            }

            var DDLMonthUp = DDLMonth_Text.ToUpper();
            System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
            var awal = DateTime.ParseExact(StartDate, "MM/dd/yyyy", AppsCultureInfo);
            var akhir = DateTime.ParseExact(EndDate, "MM/dd/yyyy", AppsCultureInfo);

            if (DDLType == "Summary")
            {
                sSql = "DECLARE @cmpcode AS VARCHAR(10);" +
                "DECLARE @periodacctg AS VARCHAR(10);" +
                "DECLARE @currency AS VARCHAR(10);" +
                "DECLARE @dateacuan AS DATETIME;" +
                "SET @cmpcode='" + CompnyCode + "';" +
                "SET @periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "';" +
                "SET @dateacuan=CAST('" + new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), DateTime.DaysInMonth(int.Parse(DDLYear), int.Parse(DDLMonth))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME);" +
                "SET @currency='IDR';" +
                "/*UNION ALL QUERY*/" +
                "SELECT suppoid, suppname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, " +
                "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " +
                "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " +
                "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " +
                "SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, " +
                "'" + DDLMonthUp + " " + DDLYear_Text + "' periodreport, '" + CompnyName + "' company, acctgoid" +
                ", (SELECT acctgcode FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) kodePiutang, (SELECT acctgdesc FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) descPiutang " +
                "FROM (" +
                "/*SALDO AWAL PERIODE*/" +
                "SELECT suppoid, suppname, " +
                "(SUM(beliidr) - SUM(paymentidr)) saidr, " +
                "(SUM(beliusd) - SUM(paymentusd)) sausd, " +
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid FROM (" +
                "SELECT suppoid, suppname, " +
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sWheresupp + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND c.conapoid>0 " +
                "AND payrefoid=0 AND trnapstatus='Post' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM ql_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype IN ('DNAP', 'DNAPRET') " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM ql_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'CNAPRET') AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL " +
                "SELECT suppoid, suppname, 0.0 beliidr, 0.0 beliusd," +
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN ql_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<@periodacctg " + sFilterCOA + " GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('CNAP', 'CNAPRET') " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'DNAPRET')  AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                ") AS tblSaldoAwalPeriod GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*SALDO AWAL CUT OFF*/" +
                "SELECT s.suppoid, s.suppname, " +
                "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " +
                "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " +
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.conapoid<0 " +
                "GROUP BY s.suppoid, s.suppname, acctgoid " +
                "UNION ALL /*PEMBELIAN*/" +
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, " +
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND c.conapoid>0 " +
                "AND payrefoid=0 AND trnapstatus='Post' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype IN ('DNAP', 'DNAPRET') " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'CNAPRET')  AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*PAYMENT*/" +
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," +
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN ql_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)=@periodacctg " + sFilterCOA + "  GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('CNAP', 'CNAPRET') " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'DNAPRET')  AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*BELUM JATUH TEMPO*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate>@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<=@periodacctg " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                "UNION ALL /*0-30 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " +
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))=@periodacctg " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                "UNION ALL /*31-60 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " +
                "0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))='" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                "UNION ALL /*>60 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "0.0 age2idr, 0.0 age2usd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM ql_mstsupp s " +
                "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + "  " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<'" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                ") AS tbldata " +
                swhere +
                "GROUP BY suppoid, suppname, acctgoid ";

                if (cbHide)
                {
                    sSql += "HAVING SUM(saidr)<>0 OR SUM(beliidr)<>0 OR SUM(paymentidr)<>0 OR SUM(age1idr)<>0 OR SUM(age2idr)<>0 OR SUM(age3idr)<>0 ";
                }
                sSql += "ORDER BY suppname";
            }
            else
            {
                sSql = "DECLARE @cmpcode AS VARCHAR(10); " +
                     " DECLARE @start AS DATETIME;" +
                     " DECLARE @finish AS DATETIME;" +
                     " SET @cmpcode = '" + CompnyCode + "';" +
                     " SET @start = '" + awal + "'; " +
                     " SET @finish = '" + akhir + "';" +
                     " SELECT * FROM ( /* CUSTOMER */ " +
                     " SELECT ISNULL(a.cmpcode, @cmpcode) cmpcode, s.suppoid, s.suppcode, s.suppname, ISNULL(a.transdate, @start) transdate, a.transtype, a.transoid, a.transno, a.currency, a.ratetrans, ISNULL(a.amttrans, 0.0) amttrans, ISNULL(a.amttransidr, 0.0) amttransidr, ISNULL(a.amttransusd, 0.0) amttransusd, ISNULL(a.amtbayar, 0.0) amtbayar, ISNULL(a.amtbayaridr, 0.0) amtbayaridr, ISNULL(a.amtbayarusd, 0.0) amtbayarusd, ISNULL(a.amtbayarother, 0.0) amtbayarother, SUM(ISNULL(b.amt, 0.0)) sawal, SUM(ISNULL(b.amtidr, 0.0)) sawalidr, SUM(ISNULL(b.amtusd, 0.0)) sawalusd, ISNULL(no_sort, 0) no_sort, '"+ CompnyName + "' companyname, '+ periodreport' periodreport, 'IDR' currvalue, '' AS [No.INV], '' AS [No.Faktur], acc.acctgcode AS kodePiutang, acc.acctgdesc AS descPiutang FROM ql_mstsupp s INNER JOIN QL_mstacctg acc ON acc.cmpcode = s.cmpcode AND acc.acctgoid IN (SELECT con.acctgoid FROM ql_conap con WHERE con.cmpcode = @cmpcode AND payrefoid = 0 AND s.suppoid = con.suppoid) LEFT JOIN (/* DETIL IN-OUT AR Detil Invoice  */" +
                     " SELECT ap.cmpcode, ap.suppoid, ap.trnapdate transdate, ap.reftype transtype, ap.refoid transoid, 1 no_sort, vap.transno, '' currency, (CASE WHEN ap.amttrans = 0 THEN 0 ELSE ROUND(ap.amttransidr / ap.amttrans, 2) END) ratetrans, ap.amttrans, ap.amttransidr, ap.amttransusd, ap.amtbayar, ap.amtbayaridr, ap.amtbayarusd, ap.amtbayarother, ap.acctgoid FROM ql_conap ap INNER JOIN (" +
                     "SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawno  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspno  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenno  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) ELSE '' END) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap.cmpcode AND vap.conapoid = ap.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.payrefoid = 0 AND ap.amttrans >= 0 ";
                if (DDLCOA != "")
                {
                    sSql += " AND AP.acctgoid=" + DDLCOA;
                }

                sSql += " UNION ALL /* Detil Invoice Retur */ SELECT ap.cmpcode, ap.suppoid, ap.trnapdate transdate, ap.reftype transtype, ap.refoid transoid, 2 no_sort, vap.transno, '' currency, (CASE WHEN ap.amttrans = 0 THEN 0 ELSE ROUND(ap.amttransidr / ap.amttrans, 2) END) ratetrans, ap.amtbayar amttrans, ap.amtbayaridr amttransidr, ap.amtbayarusd amttransusd, (ap.amttrans * -1) amtbayar, (ap.amttransidr * -1) amtbayaridr, (ap.amttransusd * -1) amtbayarusd, ap.amtbayarother, ap.acctgoid FROM ql_conap ap INNER JOIN(SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN(SELECT aprawno FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN(SELECT apgenno FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN(SELECT apspno FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN(SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN(SELECT apspmststatus FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN(SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) ELSE '' END) AS transstatus, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetmstnote FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT apitemmstnote FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN(SELECT aprawmstnote FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN(SELECT apspmstnote FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN(SELECT apgenmstnote FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) ELSE '' END) AS transduedate, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid)  ELSE '' END) AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap.cmpcode AND vap.conapoid = ap.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.payrefoid = 0 AND ap.amttrans < 0 UNION ALL /* Detil Payment */" +
                    " SELECT ap.cmpcode, ap.suppoid, ISNULL((SELECT CASE WHEN LEFT(cashbanktype, 2) = 'BB' THEN cb.cashbankduedate ELSE cb.cashbankdate END AS transdate FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar' AND cb.cashbankstatus = 'POST'), ap.paymentdate) transdate, ap.reftype transtype, ap.refoid transoid, 3 no_sort , ISNULL((SELECT cb.cashbankno FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid AND cb.cashbankstatus = 'POST' WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar'), ap.payrefno) transno, 'IDR' currency, (CASE WHEN ap2.amttrans = 0 THEN 0 ELSE ROUND(ap2.amttransidr / ap2.amttrans, 2) END) ratetrans, ap.amttrans, ap.amttransidr, ap.amttransusd, ap.amtbayar, ap.amtbayaridr, ap.amtbayarusd, ap.amtbayarother, ap2.acctgoid FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid)  ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype LIKE 'PAYAP%' AND ap.payrefoid <> 0 AND ap.payrefoid NOT IN(SELECT pap.payapoid FROM QL_trnpayap pap WHERE pap.cmpcode = ap.cmpcode AND pap.suppoid = ap.suppoid AND ISNULL(pap.payapres1, '') = 'Lebih Bayar') " + " UNION ALL /* Detil DN-CN */ " +
                    " SELECT ap.cmpcode, ap.suppoid, ap.paymentdate transdate, ap.reftype transtype, ap.refoid transoid, 4 no_sort, ap.payrefno transno, 'IDR' currency, (CASE WHEN ap2.amttrans = 0 THEN 0 ELSE ROUND(ap2.amttransidr / ap2.amttrans, 2) END) ratetrans, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN(ap.amtbayar * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayar < 0 THEN(ap.amtbayar * -1) ELSE 0 END) ELSE ap.amttrans END) amttrans, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN (ap.amtbayaridr * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayaridr < 0 THEN(ap.amtbayaridr * -1) ELSE 0 END) ELSE ap.amttransidr END) amttransidr, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN(ap.amtbayarusd * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayarusd < 0 THEN(ap.amtbayarusd * -1) ELSE 0 END) ELSE ap.amttransusd END) amttransusd, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayar > 0 THEN ap.amtbayar ELSE 0 END) ELSE ap.amtbayar END) amtbayar, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayaridr > 0 THEN ap.amtbayaridr ELSE 0 END) ELSE ap.amtbayaridr END) amtbayaridr, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayarusd > 0 THEN ap.amtbayarusd ELSE 0 END) ELSE ap.amtbayarusd END) amtbayarusd, ap.amtbayarother, ap2.acctgoid FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawno  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenno  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspno  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN(SELECT currcode FROM QL_trnaprawmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN(SELECT currcode FROM QL_trnapgenmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN(SELECT currcode FROM QL_trnapspmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype IN ('DNAP', 'CNAP', 'DNAPRET', 'CNAPRET', 'GL')  AND ap.payrefoid <> 0 ";
                if (DDLCOA != "")
                {
                    sSql += " AND ap2.acctgoid=" + DDLCOA;
                }
                sSql += " ) AS a ON a.suppoid = s.suppoid AND a.transdate >= @start AND a.transdate <= @finish LEFT JOIN ( /* SALDO AWAL */ /* Detil Invoice */ SELECT ap.cmpcode, ap.suppoid, ap.acctgoid, ap.trnapdate transdate, 'IDR' currency, ap.amttrans amt, ap.amttransidr amtidr, ap.amttransusd amtusd FROM ql_conap ap INNER JOIN(SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawno  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenno  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspno  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0)) vap ON vap.cmpcode = ap.cmpcode AND vap.conapoid = ap.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.payrefoid = 0";
                if (DDLCOA != "")
                {
                    sSql += " AND ap.acctgoid=" + DDLCOA;
                }
                sSql += " UNION ALL /* Detil Payment */ SELECT ap.cmpcode, ap.suppoid, ap2.acctgoid, ISNULL((SELECT CASE WHEN LEFT(cashbanktype, 2) = 'BB' THEN cb.cashbankduedate ELSE cb.cashbankdate END AS transdate FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar' AND cb.cashbankstatus = 'POST'), ap.paymentdate ) transdate, 'IDR' currency, (ap.amtbayar * -1) amt, (ap.amtbayaridr * -1) amtidr, (ap.amtbayarusd * -1) amtusd FROM ql_conap ap INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid)  WHEN 'QL_trnaprawmst' THEN (SELECT aprawno  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid)  WHEN 'QL_trnapgenmst' THEN (SELECT apgenno  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspno  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid)  WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype LIKE 'PAYAP%' AND ap.payrefoid <> 0";

                if (DDLCOA != "")
                {
                    sSql += " AND ap2.acctgoid=" + DDLCOA;
                }

                sSql += " AND ap.payrefoid NOT IN(SELECT pap.payapoid FROM QL_trnpayap pap WHERE pap.cmpcode = ap.cmpcode AND pap.suppoid = ap.suppoid AND ISNULL(pap.payapres1, '') = 'Lebih Bayar')";
                sSql += " UNION ALL /* Detil DN-CN */ SELECT ap.cmpcode, ap.suppoid, ap2.acctgoid, ap.paymentdate transdate, 'IDR' currency, (ap.amtbayar * -1) amt, (ap.amtbayaridr * -1) amtidr, (ap.amtbayarusd * -1) amtusd FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawno  FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenno  FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspno  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnaprawmst' THEN (SELECT aprawmststatus FROM QL_trnaprawmst arm WHERE arm.cmpcode = con.cmpcode AND aprawmstoid = con.refoid) WHEN 'QL_trnapgenmst' THEN (SELECT apgenmststatus FROM QL_trnapgenmst arm WHERE arm.cmpcode = con.cmpcode AND apgenmstoid = con.refoid) WHEN 'QL_trnapspmst' THEN (SELECT apspmststatus  FROM QL_trnapspmst arm WHERE arm.cmpcode = con.cmpcode AND apspmstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype IN ('DNAP', 'CNAP', 'DNAPRET', 'CNAPRET', 'GL') AND ap.payrefoid <> 0";
                if (DDLCOA != "")
                {
                    sSql += " AND ap2.acctgoid=" + DDLCOA;
                }
                sSql += ") AS b ON b.acctgoid = acc.acctgoid AND b.suppoid = s.suppoid AND b.transdate < @start " +
                       " GROUP BY a.cmpcode, s.suppoid, s.suppcode, s.suppname, a.transdate, a.transtype, a.transoid, a.transno, a.currency, a.ratetrans , a.amttrans, a.amttransidr, a.amttransusd, a.amtbayar, a.amtbayaridr, a.amtbayarusd, a.amtbayarother, no_sort, acc.acctgcode, acc.acctgdesc ) AS d " + swhere + " ORDER BY d.suppname, d.transdate, d.currency, no_sort, d.transoid";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (DDLType == "Summary")
            {
                rptparam.Add("Periode", "Periode : " + DDLMonth_Text + "-" + DDLYear_Text);
            }
            else
            {
                rptparam.Add("start", ClassFunction.toDate(StartDate));
                rptparam.Add("Periode", Convert.ToDateTime(StartDate).ToString("dd MM yyyy") + "-" + Convert.ToDateTime(EndDate).ToString("dd MM yyyy"));
            }
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            rptparam.Add("CompnyName", CompnyName);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

    }
}