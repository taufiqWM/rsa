﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class SPKReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string soitemmstnote { get; set; }
            public string soitemcustpo { get; set; }
        }

        public class listspk
        {
            public string wono { get; set; }
            public DateTime wodate { get; set; }
            public string custname { get; set; }
            public string wotype { get; set; }
            public string womstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetWOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust,string TextSO)
        {
            sSql = "SELECT DISTINCT 0 seq, w.womstoid,w.wono, w.wodate, isnull(womstres1,'Finish Good') wotype ,c.custname,isnull(womstnote,'') womstnote, i.itemlongdesc itemdesc FROM QL_trnwomst w INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=w.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND w.woststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY w.wono";

            List<listspk> tbl = new List<listspk>();
            tbl = db.Database.SqlQuery<listspk>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl,string TextCust, string TextWO)
        {
            sSql = "SELECT DISTINCT 0 seq, som.soitemmstoid,som.soitemno, soitemcustref soitemcustpo, soitemdate ,c.custname,soitemmstnote FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid Inner Join QL_trnwomst wom on wom.somstoid=som.soitemmstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if(MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
               
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND wom.woststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";

                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND wom.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY som.soitemno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextSO, string TextWO)
        {
            sSql = "SELECT DISTINCT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c inner join ql_trnsoitemmst som on som.custoid=c.custoid Inner join ql_trnwomst w on c.custoid=w.custoid and som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE  som.cmpcode='" + CompnyCode + "'";  
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND w.womststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";

                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            sSql += " ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.reqrabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string prabno, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemlongdesc, '' itemtype, g.gendesc unit FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=rd.cmpcode AND som.soitemmstoid=rd.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN ql_mstgen g ON g.genoid=soitemunitoid WHERE rd.cmpcode='" + CompnyCode +"'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }
           
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND "+ ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soitemno, string MCBtgl, string wono, string DDLspk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");


            var rptfile = "";
            var rptname = "";
            
                if (reporttype == "XLS")
                    rptfile = "rptSPKXls.rpt";
                else
                {
                    rptfile = "rptSPKPdf.rpt";
                }
                rptname = "SOSPK";
            

            //var Dtl = "";
            //var Join = "";

             
            
            sSql = "SELECT 'RCA' AS [Business Unit], w.*, sm.soitemno sono, (isnull(w.sub1,'') + isnull(w.sub2,'') + isnull(w.sub3,'') + isnull(w.sub4,'') + isnull(w.sub5,'')) substance, w1.gendesc [warna1], w2.gendesc [warna2], w3.gendesc [warna3], w4.gendesc [warna4], w5.gendesc [warna5], m1.gendesc [mesin1],m2.gendesc [mesin2], m3.gendesc [mesin3], m4.gendesc [mesin4], i.itemlongdesc itemdesc, soitemdate sodate, c.custname  from QL_trnwomst w inner join ql_mstcust c on c.custoid=w.custoid inner join QL_trnsoitemmst sm on w.somstoid=sm.soitemmstoid and sotype='QL_trnsoitemmst' inner join QL_trnsoitemdtl sd on sd.soitemdtloid=w.sodtloid and sotype='QL_trnsoitemmst' inner join ql_mstitem i on i.itemoid=w.itemoid inner join qL_mstgen w1 on w1.genoid=w.w1 inner join qL_mstgen w2 on w2.genoid=w.w2 inner join qL_mstgen w3 on w3.genoid=w.w3 inner join qL_mstgen w4 on w4.genoid=w.w4 inner join qL_mstgen w5 on w5.genoid=w.w5 inner join ql_mstgen m1 on m1.genoid=w.m1 inner join ql_mstgen m2 on m2.genoid=w.m2 inner join ql_mstgen m3 on m3.genoid=w.m3 inner join ql_mstgen m4 on m4.genoid=w.m4 where isnull(womstres1,'')='' and w.cmpcode='" + CompnyCode +"' ";
            if (StartPeriod != "" && EndPeriod != "")
            {
                if (MCBtgl != null)
                {
                    if (MCBtgl != "")
                    {
                        sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                }

            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND w.womststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (soitemno != null) {
                if (soitemno != "")
                {
                    string[] arr = soitemno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND sm.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (wono != null)
            {
                if (wono != "")
                {
                    string[] arr = wono.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND w.wono IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextCust!=null)
            {
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextNomor != null)
            {
                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
           

            if (DDLType == "Detail")
            {
                if (TextMaterial != null)
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND i.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
               
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}