﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class ConfirmReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string soitemmstnote { get; set; }
            public string soitemcustpo { get; set; }
        }

        public class listspk
        {
            public string wono { get; set; }
            public DateTime wodate { get; set; }
            public string custname { get; set; }
            public string wotype { get; set; }
            public string womstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetWOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust,string TextSO, string TextProdres, string TextCon)
        {
            sSql = "SELECT DISTINCT 0 seq, w.womstoid,w.wono, w.wodate, isnull(womstres1,'Finish Good') wotype ,c.custname,isnull(womstnote,'') womstnote, i.itemlongdesc itemdesc FROM QL_trnwomst w inner join ql_trnprodresmst p on p.womstoid=w.womstoid inner join qL_trnconfirmmst cn on cn.prodresmstoid=p.prodresmstoid and isnull(transtype,'')='' INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=w.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND p.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }
            if (TextCon != null)
            {
                if (TextCon != "")
                {
                    sSql += " AND cn.confirmno IN ( '" + TextCon.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY w.wono";

            List<listspk> tbl = new List<listspk>();
            tbl = db.Database.SqlQuery<listspk>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        public class listProdres
        {
            public string prodresno { get; set; }
            public DateTime prodresdate { get; set; }
            public string result { get; set; }
            public string proses { get; set; }
            public string prodresmstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetProdresData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust, string TextSO, string TextWO, string TextCon)
        {
            sSql = "SELECT DISTINCT 0 seq, p.prodresmstoid,p.prodresno, p.prodresdate,isnull(prodresmstnote,'') prodresmstnote, p.result, (select x.gendesc from ql_mstgen x where x.genoid=p.processoid) proses, i.itemlongdesc itemdesc FROM QL_trnprodresmst p inner join qL_trnconfirmmst cn on cn.prodresmstoid=p.prodresmstoid and isnull(transtype,'')='' Inner Join QL_trnwomst w on p.womstoid=w.womstoid INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=p.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.woitemno IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextCon != null)
            {
                if (TextCon != "")
                {
                    sSql += " AND cn.confirmno IN ( '" + TextCon.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY p.prodresno";

            List<listProdres> tbl = new List<listProdres>();
            tbl = db.Database.SqlQuery<listProdres>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listConfirm
        {
            public string confirmno { get; set; }
            public DateTime confirmdate { get; set; }
            public string confirmmstnote { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetConfirmData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextCust, string TextSO, string TextWO, string TextProdres)
        {
            sSql = "SELECT DISTINCT 0 seq, cn.confirmmstoid,cn.confirmno, cn.confirmdate,isnull(confirmmstnote,'') confirmmstnote FROM QL_trnconfirmmst cn Inner Join QL_trnprodresmst p on p.prodresmstoid=cn.prodresmstoid and isnull(transtype,'')='' Inner Join QL_trnwomst w on p.womstoid=w.womstoid INNER JOIN QL_mstcust c ON c.custoid=w.custoid inner join ql_mstitem i on i.itemoid=p.itemoid inner join ql_trnsoitemmst som on som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";
                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.woitemno IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND p.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY confirmno";

            List<listConfirm> tbl = new List<listConfirm>();
            tbl = db.Database.SqlQuery<listConfirm>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string MCBtgl,string TextCust, string TextWO, string TextProdres, string TextCon)
        {
            sSql = "SELECT DISTINCT 0 seq, som.soitemmstoid,som.soitemno, soitemcustref soitemcustpo, soitemdate ,c.custname,soitemmstnote FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid Inner Join QL_trnwomst w on w.somstoid=som.soitemmstoid and sotype='QL_trnsoitemmst' Inner join ql_trnprodresmst p on p.womstoid=w.womstoid inner join qL_trnconfirmmst cn on cn.prodresmstoid=p.prodresmstoid and isnull(transtype,'')='' WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                if(MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
               
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != null)
            {
                if (TextCust != "")
                {
                    sSql += " AND c.custoid IN ( '" + TextCust.Replace(";", "','") + "')";

                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND p.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }
            if (TextCon != null)
            {
                if (TextCon != "")
                {
                    sSql += " AND cn.confirmno IN ( '" + TextCon.Replace(";", "','") + "')";
                }
            }

            sSql += "ORDER BY som.soitemno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string MCBtgl, string TextSO, string TextWO, string TextProdres, string TextCon)
        {
            sSql = "SELECT DISTINCT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c inner join ql_trnsoitemmst som on som.custoid=c.custoid Inner join ql_trnwomst w on c.custoid=w.custoid and som.soitemmstoid=w.somstoid and sotype='QL_trnsoitemmst' inner join ql_trnprodresmst p on p.womstoid=w.womstoid inner join qL_trnconfirmmst cn on cn.prodresmstoid=p.prodresmstoid and isnull(transtype,'')='' WHERE  som.cmpcode='" + CompnyCode + "'";  
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND p.prodresmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextSO != null)
            {
                if (TextSO != "")
                {
                    sSql += " AND som.soitemno IN ( '" + TextSO.Replace(";", "','") + "')";

                }
            }
            if (TextWO != null)
            {
                if (TextWO != "")
                {
                    sSql += " AND w.wono IN ( '" + TextWO.Replace(";", "','") + "')";
                }
            }
            if (TextProdres != null)
            {
                if (TextProdres != "")
                {
                    sSql += " AND w.prodresno IN ( '" + TextProdres.Replace(";", "','") + "')";
                }
            }
            if (TextCon != null)
            {
                if (TextCon != "")
                {
                    sSql += " AND cn.confirmno IN ( '" + TextCon.Replace(";", "','") + "')";
                }
            }
            sSql += " ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.reqrabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string prabno, string startdate, string enddate, string fdate, string MCBtgl)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemlongdesc, '' itemtype, g.gendesc unit FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=rd.cmpcode AND som.soitemmstoid=rd.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN ql_mstgen g ON g.genoid=soitemunitoid WHERE rd.cmpcode='" + CompnyCode +"' and m.itemoid in (select itemoid from qL_trnconfirmdtl cd inner join qL_trnconfirmmst cn on cn.confirmmstoid=cd.confirmmstoid where cn.cmpcode='"+CompnyCode+"' ";
            if (startdate != "" && enddate != "")
            {
                if (MCBtgl != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
            }
            sSql += ")";
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND "+ ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += " ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soitemno, string MCBtgl, string wono, string DDLspk, string prodresno, string TextCon)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");


            var rptfile = "";
            var rptname = "";
            
                if (reporttype == "XLS")
                    rptfile = "rptConfirmXls.rpt";
                else
                {
                    rptfile = "rptConfirmPdf.rpt";
                }
                rptname = "Confirm";
            

            //var Dtl = "";
            //var Join = "";

             
            
            sSql = "SELECT 'RCA' AS [Business Unit], c.custname, cn.createuser,cn.createtime, cn.upduser, cn.updtime,confirmdate, confirmno, confirmmststatus, wh.gendesc gudang, prodresno, prodresdate, wono,wodate, soitemno sono, soitemdate sodate, itemlongdesc itemdesc, confirmqty, cd.rejectqty, confirmmstnote, cd.confirmdtlnote from ql_trnconfirmmst cn inner join ql_trnconfirmdtl cd on cd.confirmmstoid=cn.confirmmstoid inner join qL_mstgen wh on wh.genoid=confirmwhoid inner join QL_trnprodresmst p on cn.prodresmstoid=p.prodresmstoid and isnull(transtype,'')='' Inner Join QL_trnwomst w on p.womstoid=w.womstoid inner join ql_mstcust c on c.custoid=w.custoid inner join QL_trnsoitemmst sm on w.somstoid=sm.soitemmstoid and sotype='QL_trnsoitemmst' inner join ql_mstitem i on i.itemoid=cd.itemoid where isnull(womstres1,'')='' and cn.cmpcode='" + CompnyCode +"' ";
            if (StartPeriod != "" && EndPeriod != "")
            {
                if (MCBtgl != null)
                {
                    if (MCBtgl != "")
                    {
                        sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                }

            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND cn.confirmmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (soitemno != null) {
                if (soitemno != "")
                {
                    string[] arr = soitemno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND sm.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (wono != null)
            {
                if (wono != "")
                {
                    string[] arr = wono.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND w.wono IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (prodresno != null)
            {
                if (prodresno != "")
                {
                    string[] arr = prodresno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND p.prodresno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (TextCon != null)
            {
                if (TextCon != "")
                {
                    string[] arr = TextCon.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.confirmno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextCust!=null)
            {
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextNomor != null)
            {
                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
           

            if (DDLType == "Detail")
            {
                if (TextMaterial != null)
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND i.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
               
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    report.Close();
                    report.Dispose();
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}