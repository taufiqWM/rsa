﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class ShipmentSheetReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        [HttpPost]
        public ActionResult GetCustomerData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND custoid IN (SELECT custoid from QL_trnShipmentrawmst shm WHERE shm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND custoid IN (SELECT custoid from QL_trnShipmentrawmst shm WHERE shm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += ") ORDER BY custcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDriverData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, drivoid, drivcode [Code], drivname [Name], drivaddr [Address] FROM QL_mstdriver WHERE cmpcode = '" + CompnyCode + "' AND activeflag = 'ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND drivoid IN (SELECT drivoid from QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " AND drivoid IN (SELECT drivoid from QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%'";
                if (!string.IsNullOrEmpty(TextCustomer))
                    sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
              
                sSql += ") ORDER BY drivcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblDriv");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = sSql + e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetVehicleData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, vhcoid, vhccode [Code], vhcno [No.], vhcdesc [Description] FROM QL_mstvehicle WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "AND vhcoid IN (SELECT vhcoid from QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " AND vhcoid IN (SELECT vhcoid from QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(TextCustomer))
                    sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += ") ORDER BY vhccode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "TblVhc");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDOData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer, string TextDriver, string TextVehicle)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, dom.DOrawmstoid [Draft No.], dom.DOrawno [DO No.], dom.DOrawdate [DO Date], CONVERT(VARCHAR(10), dom.DOrawdate, 101) AS dodate, dom.DOrawmststatus [Status], dom.DOrawmstnote [Note], dom.dorawcustpono [Cust PO No.] FROM QL_trnDOrawmst dom ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE dom.DOrawmstoid IN (SELECT shd.DOrawmstoid from QL_trnShipmentrawmst shm INNER JOIN QL_trnShipmentrawdtl shd ON shm.Shipmentrawmstoid=shd.Shipmentrawmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE dom.DOrawmstoid IN (SELECT shd.DOrawmstoid from QL_trnShipmentrawmst shm INNER JOIN QL_trnShipmentrawdtl shd ON shm.Shipmentrawmstoid=shd.Shipmentrawmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(TextCustomer))
                    sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextDriver))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' WHERE driv.drivcode IN ('" + TextDriver.Replace(";", "','") + "'))";
                if (!string.IsNullOrEmpty(TextVehicle))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' WHERE vhc.vhccode IN ('" + TextVehicle.Replace(";", "','") + "'))";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += ") ORDER BY dom.DOrawdate DESC, dom.DOrawmstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "TblDO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetShipmentData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer, string TextDriver, string TextVehicle, string DDLType, string TextDO, string DDLWarehouse)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, shm.Shipmentrawmstoid [Draft No.], shm.Shipmentrawno [Shipment No.], shm.Shipmentrawdate [Shipment Date], CONVERT(VARCHAR(10), shm.Shipmentrawdate, 101) AS shipmentdate, c.custname AS custname, shm.Shipmentrawmststatus [Status], shm.Shipmentrawmstnote [Note] FROM QL_trnShipmentrawmst shm INNER JOIN QL_trnShipmentrawdtl shd ON shm.Shipmentrawmstoid=shd.Shipmentrawmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE shm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE shm.cmpcode LIKE '%%' ";
                if (!string.IsNullOrEmpty(TextCustomer))
                    sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextDriver))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' WHERE driv.drivcode IN ('" + TextDriver.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextVehicle))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' WHERE vhc.vhccode IN ('" + TextVehicle.Replace(";", "','") + "')";
                if (DDLType != "Summary")
                {
                    if (!string.IsNullOrEmpty(TextDO))
                        sSql += " AND shd.DOrawmstoid IN (SELECT shd2.DOrawmstoid FROM QL_trnShipmentrawdtl shd2 INNER JOIN QL_trnDOrawmst dom ON dom.DOrawmstoid=shd2.DOrawmstoid WHERE dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "'))";
                    if (!string.IsNullOrEmpty(DDLWarehouse))
                        sSql += " AND shd.Shipmentrawwhoid IN (" + DDLWarehouse + ")";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += " ORDER BY shm.Shipmentrawdate DESC, shm.Shipmentrawmstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "TblShipment");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextCustomer, string TextDriver, string TextVehicle, string DDLType, string TextDO, string DDLWarehouse, string TextShipment, string DDLShipment)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = " SELECT DISTINCT 0 seq, shd.matrawoid [Code], sodtldesc [Description], 'Sheet' matrawcode, g2.gendesc [Unit] unit FROM QL_trnShipmentrawdtl shd INNER JOIN QL_trnShipmentrawmst shm ON shm.cmpcode=shd.cmpcode AND shm.Shipmentrawmstoid=shd.Shipmentrawmstoid INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=shd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=shd.Shipmentrawunitoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE shm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE shm.cmpcode LIKE '%%' ";
                if (!string.IsNullOrEmpty(TextCustomer))
                    sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextDriver))
                    sSql += "AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' WHERE driv.drivcode IN ('" + TextDriver.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextVehicle))
                    sSql += "AND shm.Shipmentrawmstoid IN (SELECT shm2.Shipmentrawmstoid FROM QL_trnShipmentrawmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhc=vhc.vhcoid AND vhc.activeflag='ACTIVE' WHERE vhc.vhccode IN ('" + TextVehicle.Replace(";", "','") + "')";
                if (DDLType != "Summary")
                {
                    if (!string.IsNullOrEmpty(TextDO))
                        sSql += "AND shd.DOrawmstoid IN (SELECT shd2.DOrawmstoid FROM QL_trnShipmentrawdtl shd2 INNER JOIN QL_trnDOrawmst dom ON dom.DOrawmstoid=shd2.DOrawmstoid WHERE dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "')";
                    if (!string.IsNullOrEmpty(DDLWarehouse))
                        sSql += " AND shd.Shipmentrawwhoid IN (" + DDLWarehouse + ")";
                }
                if (!string.IsNullOrEmpty(TextShipment))
                {
                    if (DDLShipment == "Shipment No.")
                        sSql += "AND shm.Shipmentrawno IN ('" + DDLShipment.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "TblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            if (DDLBusinessUnit.Count() > 0)
            {
                // Isi DropDownList Department
                sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc";
                var DDLWarehouse = new SelectList(db.Database.SqlQuery<ReportModels.DDLWarehouseModel>(sSql).ToList(), "genoid", "gendesc");
                ViewBag.DDLWarehouse = DDLWarehouse;
            }

            List<string> MCBStatus = new List<string> { "In Process", "In Approval", "Approved", "Closed", "Cancel", "Rejected", "Revised" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLWarehouseModel> tbl = new List<ReportModels.DDLWarehouseModel>();

            try
            {
                if (string.IsNullOrEmpty(DDLBusinessUnit))
                {
                    sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc";
                }
                else
                {
                    sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' AND genother1='" + DDLBusinessUnit + "' ORDER BY gendesc";
                }
                tbl = db.Database.SqlQuery<ReportModels.DDLWarehouseModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "RawMaterial":
                    ViewBag.FormType = "Raw Material";
                    break;
                case "GeneralMaterial":
                    ViewBag.FormType = "General Material";
                    break;
                case "SparePart":
                    ViewBag.FormType = "Spare Part";
                    break;
                case "FinishGood":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string TextCustomer, string TextDriver, string TextVehicle, string TextShipment, string DDLShipment, string TextDO, string DDLWarehouse, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptShipment_SumXls.rpt";
                else
                    rptfile = "rptShipment_SumPdf.rpt";
                rptname = "SHIPMENTRAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptShipment_DtlXls.rpt";
                else
                {
                    if (DDLGrouping == "v.vhccode")
                        rptfile = "rptShipment_DtlPdf.rpt";
                    else if (DDLGrouping == "c.custname")
                        rptfile = "rptShipment_DtlCustPdf.rpt";
                    else if (DDLGrouping == "sodtldesc")
                        rptfile = "rptShipment_DtlMatCodePdf.rpt";
                    else 
                        rptfile = "rptShipment_DtlWhPdf.rpt";
                }
                rptname = "SHIPMENTRAWMATERIALDETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , Shipmentrawdtlseq [No.], 'Sheet' [Code], sodtldesc [Material], ISNULL([SO No.],'') AS [SO No.], ISNULL([SO ETD],CONVERT(DATETIME, '01/01/1900')) AS [SO ETD], ISNULL([SO Cust PO Date],CONVERT(DATETIME, '01/01/1900')) AS [SO Cust PO Date], ISNULL([SO Cust PO],'') AS  [SO Cust PO], ISNULL([SO Create User],'') AS  [SO Create User], ISNULL([SO Create Time],CONVERT(DATETIME, '01/01/1900')) [SO Create Time], ISNULL([Approval User],'') AS [Approval User], ISNULL([Approval Date],CONVERT(DATETIME, '01/01/1900')) [Approval Date], '' AS [DO No.], Shipmentrawqty [Qty], g2.gendesc [Unit], g3.gendesc [Warehouse], Shipmentrawdtlnote [Detail Note] ";
                Join = " INNER JOIN QL_trnShipmentrawdtl shd ON shd.cmpcode=shm.cmpcode AND shd.Shipmentrawmstoid=shm.Shipmentrawmstoid inner join ql_trndorawdtl dod on dod.dorawdtloid=shd.dorawdtloid INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=shd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=Shipmentrawunitoid INNER JOIN QL_mstgen g3 ON g3.genoid=Shipmentrawwhoid ";
                Join += "INNER JOIN (SELECT som.sorawmstoid, sod.sorawdtloid, som.sorawno [SO No.], som.sorawetd [SO ETD], som.socustrefdate [SO Cust PO Date], som.sorawcustref [SO Cust PO], som.createuser [SO Create User], som.createtime [SO Create Time], som.approvaluser [Approval User], som.approvaldatetime [Approval Date]  FROM QL_trnsorawmst som INNER JOIN QL_trnsorawdtl sod ON sod.sorawmstoid = sod.sorawmstoid ) AS tblSO ON tblSO.sorawmstoid=dod.sorawmstoid AND dod.sorawdtloid=tblSO.sorawdtloid ";
            }
            sSql = " SELECT (SELECT div.divname FROM QL_mstdivision div WHERE shm.cmpcode = div.cmpcode) AS [Business Unit], (select x.gendesc from ql_mstgen x where x.genoid=shm.divgroupoid) [Divisi], shm.cmpcode [CMPCODE], CONVERT(VARCHAR(20), shm.Shipmentrawmstoid) [Draft No.], shm.Shipmentrawmstoid [ID], Shipmentrawdate [Stuffing Date], Shipmentrawno [Shipment No.], custname [Customer], custcode [Customer Code], currcode [Currency], Shipmentrawmstnote [Header Note], Shipmentrawmststatus [Status], shm.approvaluser [Approval User], shm.approvaldatetime [Approval Date], shm.createuser [Create User], shm.createtime [Create Date], Shipmentraweta [Shipment ETA], Shipmentrawetd [Shipment ETD], Shipmentrawcontno [Container No.], Shipmentrawportship [Port Ship], Shipmentrawportdischarge [Port Discharge], Shipmentrawnopol [VehiclePolice No.], ISNULL(driv.drivname,'') [Driver Name], ISNULL(v.vhcdesc,'') [Vehicle], 'RM' AS [Shipment Type], '' AS [SJ PAJAK] " + Dtl + " FROM QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid LEFT JOIN QL_mstdriver driv ON shm.drivoid=driv.drivoid LEFT JOIN QL_mstvehicle v ON shm.vhcoid=v.vhcoid " + Join + " ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE shm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE shm.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextCustomer))
                sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextDriver))
                sSql += " AND driv.drivcode IN ('" + TextCustomer.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextVehicle))
                sSql += " AND v.vhccode IN ('" + TextVehicle.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextShipment))
            {
                if (DDLShipment == "Shipment No.")
                    sSql += " AND shm.Shipmentrawno IN ('" + TextShipment.Replace(";", "','") + "')";
                else
                    sSql += "AND shm.Shipmentrawmstoid IN ('" + TextShipment.Replace(";", "', '") + "')";
            }
           
            if (DDLType != "Summary")
            {
                if (!string.IsNullOrEmpty(TextDO))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shd2.Shipmentrawmstoid FROM QL_trnShipmentrawdtl shd2 INNER JOIN QL_trnDOrawmst dom ON shd2.DOrawmstoid=dom.DOrawmstoid WHERE dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "'))";
                if (!string.IsNullOrEmpty(DDLWarehouse))
                    sSql += "  AND shd.Shipmentrawwhoid IN (" + DDLWarehouse + ")";
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND i.sodtldesc IN ('" + TextMaterial.Replace(";", "','") + "')";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND shm.divgroupoid=" + DDLdivgroupoid;
            }
            if (DDLType == "Summary")
            {
                if (DDLGrouping == "v.vhccode")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }
               else if (DDLGrouping == "c.custname")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy; 
                }
                else
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
            }
            else
            {
                if (DDLGrouping == "v.vhccode")
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "c.custname")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "sodtldesc")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, sodtldesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, sodtldesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, g3.gendesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, g3.gendesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        public ActionResult PrintReportInPrice(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string TextCustomer, string TextDriver, string TextVehicle, string TextShipment, string DDLShipment, string TextDO, string DDLWarehouse)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptShipment_SumXls2.rpt";
                else
                    rptfile = "rptShipment_SumPdf2.rpt";
                rptname = "SHIPMENTRAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptShipment_DtlXls2.rpt";
                else
                {
                    if (DDLGrouping == "v.vhccode")
                        rptfile = "rptShipment_DtlPdf2.rpt";
                    else if (DDLGrouping == "c.custname")
                        rptfile = "rptShipment_DtlCustPdf2.rpt";
                    else if (DDLGrouping == "sodtldesc")
                        rptfile = "rptShipment_DtlMatCodePdf2.rpt";
                    else
                        rptfile = "rptShipment_DtlWhPdf2.rpt";
                }
                rptname = "SHIPMENTRAWMATERIALDETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , Shipmentrawdtlseq [No.], 'Sheet' [Code], sodtldesc [Material], ISNULL([SO No.],'') AS [SO No.], ISNULL([SO ETD],CONVERT(DATETIME, '01/01/1900')) AS [SO ETD], ISNULL([SO Cust PO Date],CONVERT(DATETIME, '01/01/1900')) AS [SO Cust PO Date], ISNULL([SO Cust PO],'') AS  [SO Cust PO], ISNULL([SO Create User],'') AS  [SO Create User], ISNULL([SO Create Time],CONVERT(DATETIME, '01/01/1900')) [SO Create Time], ISNULL([Approval User],'') AS [Approval User], ISNULL([Approval Date],CONVERT(DATETIME, '01/01/1900')) [Approval Date], ISNULL((dom.DOrawno),'') AS [DO No.], Shipmentrawqty [Qty], g2.gendesc [Unit], g3.gendesc [Warehouse], Shipmentrawdtlnote [Detail Note] ";
                Join = " INNER JOIN QL_trnShipmentrawdtl shd ON shd.cmpcode=shm.cmpcode AND shd.Shipmentrawmstoid=shm.Shipmentrawmstoid INNER JOIN QL_trnDOrawmst dom ON dom.DOrawmstoid=shd.DOrawmstoid INNER JOIN QL_trnsorawdtl i ON i.sorawdtloid=shd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=Shipmentrawunitoid INNER JOIN QL_mstgen g3 ON g3.genoid=Shipmentrawwhoid ";
                Join += "INNER JOIN (SELECT dod.dorawmstoid, dod.dorawdtloid, som.sorawmstoid, som.sorawno [SO No.], som.sorawetd [SO ETD], som.socustrefdate [SO Cust PO Date], som.sorawcustref [SO Cust PO], som.createuser [SO Create User], som.createtime [SO Create Time], som.approvaluser [Approval User], som.approvaldatetime [Approval Date]  FROM QL_trnsorawmst som INNER JOIN QL_trndorawdtl dod ON dod.sorawmstoid=som.sorawmstoid) AS tblSO ON tblSO.dorawmstoid=shd.dorawmstoid AND shd.dorawdtloid=tblSO.dorawdtloid ";
            }
            sSql = " SELECT (SELECT div.divname FROM QL_mstdivision div WHERE shm.cmpcode = div.cmpcode) AS [Business Unit], shm.cmpcode [CMPCODE], CONVERT(VARCHAR(20), shm.Shipmentrawmstoid) [Draft No.], shm.Shipmentrawmstoid [ID], Shipmentrawdate [Stuffing Date], Shipmentrawno [Shipment No.], custname [Customer], custcode [Customer Code], currcode [Currency], Shipmentrawmstnote [Header Note], Shipmentrawmststatus [Status], shm.approvaluser [Approval User], shm.approvaldatetime [Approval Date], shm.createuser [Create User], shm.createtime [Create Date], Shipmentraweta [Shipment ETA], Shipmentrawetd [Shipment ETD], Shipmentrawcontno [Container No.], Shipmentrawportship [Port Ship], Shipmentrawportdischarge [Port Discharge], Shipmentrawnopol [VehiclePolice No.], ISNULL(driv.drivname,'') [Driver Name], ISNULL(v.vhcdesc,'') [Vehicle], 'RM' AS [Shipment Type], '' AS [SJ PAJAK] " + Dtl + " FROM QL_trnShipmentrawmst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid LEFT JOIN QL_mstdriver driv ON shm.drivoid=driv.drivoid LEFT JOIN QL_mstvehicle v ON shm.vhcoid=v.vhcoid " + Join + " ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE shm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE shm.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND shm.shipmentrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextCustomer))
                sSql += " AND c.custcode IN ('" + TextCustomer.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextDriver))
                sSql += " AND driv.drivcode IN ('" + TextCustomer.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextVehicle))
                sSql += " AND v.vhccode IN ('" + TextVehicle.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextShipment))
            {
                if (DDLShipment == "Shipment No.")
                    sSql += " AND shm.Shipmentrawno IN ('" + TextShipment.Replace(";", "','") + "')";
                else
                    sSql += "AND shm.Shipmentrawmstoid IN ('" + TextShipment.Replace(";", "', '") + "')";
            }

            if (DDLType != "Summary")
            {
                if (!string.IsNullOrEmpty(TextDO))
                    sSql += " AND shm.Shipmentrawmstoid IN (SELECT shd2.Shipmentrawmstoid FROM QL_trnShipmentrawdtl shd2 INNER JOIN QL_trnDOrawmst dom ON shd2.DOrawmstoid=dom.DOrawmstoid WHERE dom.DOrawno IN ('" + TextDO.Replace(";", "','") + "'))";
                if (!string.IsNullOrEmpty(DDLWarehouse))
                    sSql += "  AND shd.Shipmentrawwhoid IN (" + DDLWarehouse + ")";
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND i.sodtldesc IN ('" + TextMaterial.Replace(";", "','") + "')";
            }
            if (DDLType == "Summary")
            {
                if (DDLGrouping == "v.vhccode")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "c.custname")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
            }
            else
            {
                if (DDLGrouping == "v.vhccode")
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "c.custname")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, c.custname " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
                else if (DDLGrouping == "sodtldesc")
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, sodtldesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, sodtldesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLSorting == "shm.Shipmentrawno")
                        sSql += " ORDER BY shm.cmpcode ASC, g3.gendesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy + " , shm.Shipmentrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY shm.cmpcode ASC, g3.gendesc " + DDLOrderBy + ", " + DDLSorting + " " + DDLOrderBy;
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}