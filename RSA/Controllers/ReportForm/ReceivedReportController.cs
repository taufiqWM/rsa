﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class ReceivedReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL(string rpttype = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<string> MCBStatus = new List<string> { "In Process", "Post", "Closed" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult GetSupplierData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLSupType)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode [code], suppname [name], suppaddr [address] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ";
               
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrm.mrrawmstoid=mrd.mrrawmstoid WHERE mrm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrm.mrrawmstoid=mrd.mrrawmstoid WHERE mrm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND mrm.mrrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(DDLSupType))
                    sSql += " AND supptaxable='" + DDLSupType + "' ";

                sSql += ") ORDER BY suppcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSup");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRegnoData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSup, string DDLSupType, string DDLRegType)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, regm.registermstoid [Draft No.], regm.registerno [Reg No.], regm.registerdate [Reg. Date], s.suppname AS Supplier, regm.registermstnote [Note], regm.registermststatus [Status] FROM QL_trnregistermst regm INNER JOIN QL_trnregisterdtl regd ON regm.registermstoid=regd.registermstoid INNER JOIN QL_mstsupp s ON regm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";
                sSql += "WHERE regm.registertype='raw' AND regm.registermstoid IN (SELECT DISTINCT mrm.registermstoid FROM QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrm.mrrawmstoid=mrd.mrrawmstoid INNER JOIN QL_mstsupp s ON mrm.suppoid=s.suppoid ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "  WHERE mrm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += "  WHERE mrm.cmpcode LIKE '%%' ";
                if (!string.IsNullOrEmpty(DDLSupType))
                    sSql += " AND s.supptaxable='" + DDLSupType + "' ";
                //supcode dibawah
                if (!string.IsNullOrEmpty(TextSup))
                {
                    sSql += " AND s.suppcode IN ('" + TextSup.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND mrm.mrrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(DDLRegType))
                   sSql += " AND regm.registerflag='" + DDLRegType + "' " + "";

                sSql += ") ORDER BY regm.registermstoid DESC, regm.registerno DESC ";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRegno");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRecData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSup, string TextRegno, string DDLSupType, string DDLRegType)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, mrm.mrrawmstoid [Draf No.], mrm.mrrawno [MR No.], mrm.mrrawdate, CONVERT(VARCHAR(10), mrm.mrrawdate, 101) AS mrdate, s.suppname AS suppname, ISNULL(mrm.mrrawmstnote, '') [Note], mrm.mrrawmststatus [Status]FROM QL_trnmrrawmst mrm /*INNER JOIN QL_trnregistermst regm ON regm.cmpcode=mrm.cmpcode AND regm.registermstoid=mrm.registermstoid*/ INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrm.mrrawmstoid=mrd.mrrawmstoid INNER JOIN QL_trnporawmst pom ON pom.cmpcode=mrm.cmpcode AND pom.porawmstoid=mrm.porawmstoid INNER JOIN QL_mstsupp s ON s.cmpcode=mrm.cmpcode AND mrm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE mrm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE mrm.cmpcode LIKE '%%'";
                if (!string.IsNullOrEmpty(DDLSupType))
                    sSql += " AND s.supptaxable='" + DDLSupType+ "'";
                if (!string.IsNullOrEmpty(TextSup))
                {
                    sSql += " AND s.suppcode IN ('" + TextSup.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(TextRegno))
                {
                    sSql += " AND regm.registerno IN ('" + TextRegno.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(DDLRegType))
                    sSql += " AND regm.registerflag='" + DDLRegType + "' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += "AND mrm.mrrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                
                sSql += " ORDER BY mrm.mrrawdate DESC, mrm.mrrawmstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());


                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRec");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSup, string TextRegno, string DDLNomor, string TextNomor, string DDLSupType, string DDLRegType)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.matrawcode [Code], m.matrawlongdesc [Description] , g2.gendesc [unit] FROM QL_trnmrrawdtl mrd INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mrrawmstoid=mrd.mrrawmstoid /*INNER JOIN QL_trnregistermst regm ON regm.cmpcode=mrm.cmpcode AND regm.registermstoid=mrm.registermstoid*/ INNER JOIN QL_trnporawmst pom ON pom.cmpcode=mrd.cmpcode AND pom.porawmstoid=mrm.porawmstoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=mrd.cmpcode AND pod.porawmstoid=pom.porawmstoid AND pod.porawdtloid=mrd.porawdtloid INNER JOIN QL_mstmatraw m ON m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=mrd.mrrawunitoid INNER JOIN QL_mstsupp s ON mrm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE mrm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE mrm.cmpcode LIKE '%%'";
                if (!string.IsNullOrEmpty(DDLSupType))
                    sSql += " AND s.supptaxable='" + DDLSupType + "'";
               
                if (!string.IsNullOrEmpty(DDLRegType))
                    sSql += " AND regm.registerflag='" + DDLRegType + "'";
                if (!string.IsNullOrEmpty(TextSup))
                {
                    sSql += " AND s.suppcode IN ('" + TextSup.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(TextRegno))
                {
                    sSql += " AND regm.registerno IN ('" + TextRegno.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND mrm.mrrawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Draf No.")
                        sSql += " AND mrm.mrrawmstoid IN ('" + TextNomor.Replace(";", ",") + "')";
                    else
                        sSql += " AND mrm.mrrawno IN ('" + TextNomor.Replace(";", "','") + "')";
                }
                //sSql += " ORDER BY [Code]";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());


                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: ReceivedReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: ReceivedReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportInPrice/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: RecReportStatus
       

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextSup, string TextRegno, string DDLSupType, string DDLRegType, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptMRReportSumXls.rpt";
                else
                    rptfile = "rptMRReportSum.rpt";
                rptname = "RAWMATERIALRECEIVEDSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptMRReportDtlXls.rpt";
                else
                {
                    if (DDLGrouping == "mrm.mrrawno")
                        rptfile = "rptMRReportDtlMRNo.rpt";
                    else if (DDLGrouping == "matrawcode")
                        rptfile = "rptMRReportDtlMatCode.rpt";
                    else
                        rptfile = "rptMRReportDtlSupp.rpt";
                }
                rptname = "RAWMATERIALRECEIVEDDETAIL";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Slct = ""; var Join = "";
            if (DDLType == "Detail")
            {
                //select detail Rec
                Slct += " , m.matrawcode AS [Code], m.matrawlongdesc AS [Material], mrd.mrrawqty AS [MR Qty], g.gendesc AS [Unit], mrd.mrrawdtlnote AS [Detail Note] ";
                //join detail Rec
                Join += " INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid INNER JOIN QL_mstmatraw m ON  m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrrawunitoid ";
                //Join += " INNER JOIN QL_trnregisterdtl regd ON mrd.cmpcode=regd.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid ";
                //select Pret
                Slct += " , ISNULL(pretm.pretrawno,'') AS [PRet No.], ISNULL(pretm.pretrawdate,'') AS [PRet Date] , ISNULL(pretm.approvaldatetime,'') AS [PRet App Date], pretrawqty AS [PRet Qty], ISNULL((SELECT profname FROM QL_mstprof p4 WHERE pretm.approvaluser=p4.profoid),'') AS [PRet App User] ";
                //join pret
                Join += " LEFT JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=mrd.cmpcode AND pretd.mrrawmstoid=mrm.mrrawmstoid AND pretd.mrrawdtloid=mrd.mrrawdtloid  LEFT JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretrawmstoid IN (SELECT pretrawmstoid FROM QL_trnpretrawdtl pretd2 WHERE pretd2.cmpcode=mrd.cmpcode AND pretd2.mrrawmstoid=mrm.mrrawmstoid AND pretd2.mrrawdtloid=mrd.mrrawdtloid)";
                //select pr
                Slct += " , ISNULL((SELECT DISTINCT prm.prrawno FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid) ,'') AS [PR No.] , ISNULL((SELECT DISTINCT prm.prrawdate FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid),'') AS [PR Date], ISNULL((SELECT DISTINCT prm.approvaldatetime FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid),'') AS [PR App Date], ISNULL((SELECT profname FROM QL_mstprof p3 INNER JOIN QL_prrawmst prm ON pod.prrawmstoid=prm.prrawmstoid WHERE prm.approvaluser=p3.profoid),'') AS [PR App User] ";
                //select po
                Slct += " , pom.porawno AS[PO No.], pom.porawdate AS[PO Date], pom.approvaldatetime[PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser = p2.profoid) AS[PO UserApp] ";
                //join po
                Join += " INNER JOIN QL_trnporawdtl pod  ON pod.cmpcode=mrm.cmpcode AND pod.porawdtloid=mrd.porawdtloid INNER JOIN QL_trnporawmst pom ON pom.cmpcode=mrm.cmpcode AND pom.porawmstoid=mrm.porawmstoid AND pod.porawmstoid=pom.porawmstoid ";
            }
            //select Register
            //Slct += " , regm.registerno [Reg No.],regm.registerdate, regm.updtime AS [Reg Post Date], registerdocrefdate AS [Tgl. SJ], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] ";
            Slct += " , '' [Reg No.], CONVERT(DATETIME,'1/1/1900') [Reg Date], CONVERT(DATETIME,'1/1/1900') AS [Reg Post Date], /*registerdocrefdate*/CONVERT(DATETIME,'1/1/1900') AS [Tgl. SJ], (SELECT profname FROM QL_mstprof p1 WHERE pom.upduser=p1.profoid) AS [Reg UserPost] ";
            //join register
            //Join += " INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='raw' ";
            //Join += " INNER JOIN QL_trnporawmst regm ON mrm.cmpcode=regm.cmpcode AND regm.porawmstoid=mrm.porawmstoid ";
            sSql = "SELECT mrm.mrrawmstoid AS [OID], CONVERT(VARCHAR(10), mrm.mrrawmstoid) [Draft No.], mrm.mrrawno AS [MR No.], mrm.mrrawdate AS [MR Date], mrm.mrrawmststatus AS [Status], (select x.gendesc from ql_mstgen x where x.genoid=mrm.divgroupoid) [Divisi], mrm.cmpcode AS [CmpCode], mrm.suppoid [Supplier Oid], s.suppcode [Supplier Code], s.suppname [Supplier Name], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], mrm.mrrawmstnote AS [MR Note], mrm.createuser [Create User], mrm.createtime [Create Date], (CASE mrm.mrrawmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], /*grirposttime*/ CONVERT(DATETIME,'1/1/1900') AS [GRIR Post Date], g1.genoid [WH Oid], g1.gendesc [Warehouse], (CASE mrrawmststatus WHEN 'In Process' THEN '' ELSE UPPER(mrm.upduser) END) [Posting User], (CASE mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE mrm.updtime END) [Posting Date] " + Slct + " FROM QL_trnmrrawmst mrm " + Join + "  INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrrawwhoid /*AND g1.gengroup='MATERIAL LOCATION'*/";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += "WHERE mrm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE mrm.cmpcode LIKE '%%'";
            if (!string.IsNullOrEmpty(DDLSupType))
                sSql += " AND s.supptaxable='" + DDLSupType + "'";
            if (!string.IsNullOrEmpty(TextSup))
            {
                sSql += " AND s.suppcode IN ('" + TextSup.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(DDLRegType))
                sSql += " AND regm.registerflag='" + DDLRegType + "'";
            if (!string.IsNullOrEmpty(TextRegno))
            {
                sSql += " AND regm.registerno IN ('" + TextRegno.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND mrm.mrrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draf No.")
                    sSql += " AND mrm.mrrawmstoid  IN ('" + TextNomor.Replace(";", ",") + "')";
                else
                    sSql += " AND mrm.mrrawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if(DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND mrm.divgroupoid=" + DDLdivgroupoid;
            }

            if (DDLType == "Summary")
                sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC, mrm.mrrawno " + DDLOrderBy + " , mrm.mrrawmstoid " + DDLOrderBy;
            else
            {
                if (DDLGrouping == "mrm.mrrawno")
                    sSql += " ORDER BY mrm.cmpcode ASC, mrm.mrrawno ASC, " + DDLSorting + " " + DDLOrderBy + ", mrm.mrrawmstoid " + DDLOrderBy;
                else if (DDLGrouping == "matrawcode")
                {
                    if (DDLSorting == "mrm.mrrawno")
                        sSql += " ORDER BY mrm.cmpcode ASC, m.matrawcode ASC , " + DDLSorting + " " + DDLOrderBy + " , mrm.mrrawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY mrm.cmpcode ASC, m.matrawcode ASC , " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    //grouping by supplier
                    if (DDLSorting == "mrm.mrrawno")
                        sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC , " + DDLSorting + " " + DDLOrderBy + ", mrm.mrrawmstoid  " + DDLOrderBy;
                    else
                        sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC , " + DDLSorting + " "+ DDLOrderBy;
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportInPrice(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string TextSup, string TextRegno, string DDLSupType, string DDLRegType, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptMRReportSumXls2.rpt";
                else
                    rptfile = "rptMRReportSum2.rpt";
                rptname = "RAWMATERIALRECEIVEDSUMMARYINPRICE";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptMRReportDtlXls2.rpt";
                else
                {
                    if (DDLGrouping == "mrm.mrrawno")
                        rptfile = "rptMRReportDtlMRNo2.rpt";
                    else if (DDLGrouping == "matrawcode")
                        rptfile = "rptMRReportDtlMatCode2.rpt";
                    else
                        rptfile = "rptMRReportDtlSupp2.rpt";
                }
                rptname = "RAWMATERIALRECEIVEDDETAILINPRICE";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Slct = ""; var Join = "";
            if (DDLType == "Detail")
            {
                //select Detail
                Slct += " , m.matrawcode AS [Code], m.matrawlongdesc AS [Material], mrd.mrrawqty AS [MR Qty], g.gendesc AS [Unit], mrd.mrrawdtlnote AS [Detail Note] ";
                //select Price Detail
                Slct += " , mrd.mrrawvalue AS [Value],mrd.mrrawvalueidr AS [Value IDR],mrd.mrrawvalueusd AS [Value USD], (mrd.mrrawvalue-pod.porawprice) [HargaLain], (SELECT c.currcode FROM QL_mstcurr c WHERE c.curroid=mrm.curroid) AS [Currency], (mrd.mrrawqty*mrd.mrrawvalue) AS [Price], (mrd.mrrawqty*mrd.mrrawvalueidr) AS [Price IDR], (mrd.mrrawqty*mrd.mrrawvalueusd) AS [Price USD], dap.aprawdtlres1 AS [AP Invoice], dap.aprawdtlres2 AS [AP Faktur Pajak], dap.aprawmststatus AS [AP Status] ";
                Slct += " ,(mrd.mrrawqty*pod.porawprice) AS [PO Price], (mrd.mrrawqty*pod.porawpriceidr) AS [PO Price IDR], (mrd.mrrawqty*pod.porawpriceusd) AS [PO Price USD], (pod.porawprice) AS [PO Value], (pod.porawpriceidr) AS [PO Value IDR], (pod.porawpriceusd) AS [PO Value USD] ";
                //join detail rec
                Join = " INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid INNER JOIN QL_mstmatraw m ON  m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g ON g.genoid=mrd.mrrawunitoid ";
                Join += " INNER JOIN QL_trnregisterdtl regd ON mrd.cmpcode=regd.cmpcode AND regd.registermstoid=mrm.registermstoid AND regd.registerdtloid=mrd.registerdtloid ";
                //select pret
                Slct += " , ISNULL(pretm.pretrawno,'') AS [PRet No.], ISNULL(pretm.pretrawdate,'') AS [PRet Date] , ISNULL(pretm.approvaldatetime,'') AS [PRet App Date], pretrawqty AS [PRet Qty], ISNULL((SELECT profname FROM QL_mstprof p4 WHERE pretm.approvaluser=p4.profoid),'') AS [PRet App User] ";
                //join pret
                Join += " LEFT JOIN (SELECT retdtl.cmpcode, retdtl.pretrawmstoid, retdtl.pretrawdtloid, retdtl.mrrawmstoid, retdtl.mrrawdtloid, retdtl.pretrawqty FROM QL_trnpretrawmst retmst INNER JOIN QL_trnpretrawdtl retdtl ON retmst.pretrawmstoid=retdtl.pretrawmstoid WHERE retmst.pretrawmststatus='Approved') pretd ON pretd.cmpcode=mrd.cmpcode AND pretd.mrrawmstoid=mrm.mrrawmstoid AND pretd.mrrawdtloid=mrd.mrrawdtloid LEFT JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretrawmstoid IN (SELECT pretrawmstoid FROM QL_trnpretrawdtl pretd2 WHERE pretd2.cmpcode=mrd.cmpcode AND pretd2.mrrawmstoid=mrm.mrrawmstoid AND pretd2.mrrawdtloid=mrd.mrrawdtloid) ";
                //select pr
                Slct += " , ISNULL((SELECT DISTINCT prm.prrawno FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid) ,'') AS [PR No.] , ISNULL((SELECT DISTINCT prm.prrawdate FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid),'') AS [PR Date], ISNULL((SELECT DISTINCT prm.approvaldatetime FROM QL_prrawmst prm WHERE pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid),'') AS [PR App Date], ISNULL((SELECT profname FROM QL_mstprof p3 INNER JOIN QL_prrawmst prm ON pod.cmpcode=prm.cmpcode AND pod.prrawmstoid=prm.prrawmstoid WHERE pod.cmpcode=prm.cmpcode AND prm.approvaluser=p3.profoid),'') AS [PR App User] ";
                //select rate
                Slct += " ,ISNULL(pom.porawtaxtype,'UNKNOWN') [Status PPN], ISNULL((Select TOP 1 r.rateidrvalue FROM QL_mstrate r where /*r.rateoid=pom.rateoid AND*/ registerdocrefdate between r.ratedate AND r.ratetodate AND r.curroid=mrm.curroid),0.0) [Daily Rate To IDR], ISNULL((Select TOP 1 r.rateusdvalue FROM QL_mstrate r where /*r.rateoid=pom.rateoid AND*/ registerdocrefdate between r.ratedate AND r.ratetodate AND r.curroid=mrm.curroid),0.0) [Daily Rate To USD], ISNULL((Select TOP 1 r.rate2idrvalue FROM QL_mstrate2 r where /*r.rate2oid=pom.rate2oid AND*/ r.rate2year = YEAR(mrm.updtime) AND r.curroid=mrm.curroid AND rate2month = MONTH(CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END)),0.0)  [Monthly Rate To IDR], ISNULL((Select TOP 1 r.rate2usdvalue FROM QL_mstrate2 r where /*r.rate2oid=pom.rate2oid AND*/ r.curroid=mrm.curroid AND r.rate2year = YEAR(mrm.updtime) AND rate2month = MONTH(CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END)),0.0) [Monthly Rate To USD] ";
                //select po
                Slct += " , pom.porawno AS [PO No.], pom.porawdate AS [PO Date], pom.approvaldatetime [PO App Date], (SELECT profname FROM QL_mstprof p2 WHERE pom.approvaluser=p2.profoid) AS [PO UserApp] ";
                //join po
                Join += " INNER JOIN QL_trnporawmst pom ON pom.cmpcode=regd.cmpcode AND pom.porawmstoid=regd.porefmstoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=regd.porefmstoid AND pod.porawdtloid=regd.porefdtloid ";
                //join ap
                Join += " LEFT JOIN (SELECT DISTINCT dap.mrrawmstoid ,dap.mrrawdtloid,dap.aprawdtlres1,dap.aprawdtlres2,map.aprawmststatus,map.cmpcode FROM QL_trnaprawmst map INNER JOIN QL_trnaprawdtl dap ON map.cmpcode=dap.cmpcode AND map.aprawmstoid=dap.aprawmstoid) dap ON dap.mrrawmstoid=mrd.mrrawmstoid AND dap.mrrawdtloid=mrd.mrrawdtloid AND dap.cmpcode=mrd.cmpcode ";
            }
            else
            {
                //select total price
                Slct += " , ISNULL((SELECT SUM(mrrawqty * mrrawvalue) FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid), 0.0) [Total Price], ISNULL((SELECT SUM(mrrawqty * mrrawvalueidr) FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid), 0.0) [Total Price IDR], ISNULL((SELECT SUM(mrrawqty * mrrawvalueusd) FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid), 0.0) [Total Price USD] ,(SELECT c.currcode FROM QL_mstcurr c WHERE c.curroid=mrm.curroid) AS [Currency] ";
            }
            //select Register
            Slct += " ,regm.registerno [Reg No.],regm.registerdate [Reg Date], regm.updtime AS [Reg Post Date], registerdocrefdate AS [Tgl. SJ], (SELECT profname FROM QL_mstprof p1 WHERE regm.upduser=p1.profoid) AS [Reg UserPost] ";
            //join register
            Join += " INNER JOIN QL_trnregistermst regm ON mrm.cmpcode=regm.cmpcode AND regm.registermstoid=mrm.registermstoid AND registertype='raw' ";

            sSql = "SELECT mrm.mrrawmstoid AS [OID], CONVERT(VARCHAR(10), mrm.mrrawmstoid) AS [Draft No.], mrm.mrrawno AS [MR No.], (select x.gendesc from ql_mstgen x where x.genoid=mrm.divgroupoid) [Divisi], mrm.mrrawdate AS [MR Date], mrm.mrrawmststatus AS [Status], mrm.cmpcode AS [CmpCode], mrm.suppoid [Supplier Oid], s.suppcode [Supplier Code], s.suppname [Supplier Name], (SELECT div.divname FROM QL_mstdivision div WHERE mrm.cmpcode = div.cmpcode) AS [BU Name], mrm.mrrawmstnote AS [MR Note], mrm.createuser [Create User], mrm.createtime [Create Date], (CASE mrm.mrrawmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], ISNULL((SELECT TOP 1 ISNULL(aprawno,'') FROM QL_trnaprawmst apm INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=apm.cmpcode AND apm.aprawmstoid=apd.aprawmstoid WHERE apd.mrrawmstoid=mrm.mrrawmstoid),'')AS [AP No.], grirposttime AS [GRIR Post Date], g1.genoid [WH Oid], g1.gendesc [Warehouse], (CASE mrrawmststatus WHEN 'In Process' THEN '' ELSE UPPER(mrm.upduser) END) [Posting User], (CASE mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE mrm.updtime END) [Posting Date] " + Slct + " FROM QL_trnmrrawmst mrm  " + Join + "  INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_mstgen g1 ON g1.genoid=mrrawwhoid /*AND g1.gengroup='MATERIAL LOCATION'*/";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE mrm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE mrm.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(DDLSupType))
                sSql += " AND s.supptaxable='" + DDLSupType + "'";
            if (!string.IsNullOrEmpty(TextSup))
            {
                sSql += " AND s.suppcode IN ('" + TextSup.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(DDLRegType))
                sSql += " AND regm.registerflag='" + DDLRegType + "'";
            if (!string.IsNullOrEmpty(TextRegno))
            {
                sSql += " AND regm.registerno IN ('" + TextRegno.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND mrm.mrrawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draf No.")
                    sSql += " AND mrm.mrrawmstoid IN ('" + TextNomor.Replace(";", ",") + "')";
                else
                    sSql += " AND mrm.mrrawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND mrm.divgroupoid=" + DDLdivgroupoid;
            }
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
            }

            if (DDLType == "Summary")
            {
                sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC, mrm.mrrawno " + DDLOrderBy+ " , mrm.mrrawmstoid " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "mrm.mrrawno")
                    sSql += " ORDER BY mrm.cmpcode ASC, mrm.mrrawno ASC, " + DDLSorting + " " + DDLOrderBy + ", mrm.mrrawmstoid " + DDLOrderBy;
                else if (DDLGrouping == "matrawcode")
                {
                    if (DDLSorting == "mrm.mrrawno")
                        sSql += " ORDER BY mrm.cmpcode ASC, m.matrawcode ASC , " + DDLSorting + " " + DDLOrderBy + ", mrm.mrrawmstoid  " + DDLOrderBy;
                    else
                        sSql += " ORDER BY mrm.cmpcode ASC, m.matrawcode ASC , " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    //grouping by supplier
                    if (DDLSorting == "mrm.mrrawno")
                        sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC , " + DDLSorting + " " + DDLOrderBy + ", mrm.mrrawmstoid  " + DDLOrderBy;
                    else
                        sSql += " ORDER BY mrm.cmpcode ASC, s.suppname ASC , " + DDLSorting + " " + DDLOrderBy;
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

    }
}