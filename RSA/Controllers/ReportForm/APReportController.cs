﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class APReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL()
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<string> MCBStatus = new List<string> { "In Process", "In Approval", "Approved", "Closed", "Cancel", "Rejected", "Revised" };
            ViewBag.MCBStatus = MCBStatus;
        }

        [HttpPost]
        public ActionResult GetSupData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode [Code], suppname[Name], '' [Type], suppaddr[Address] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnaprawmst apm LEFT JOIN QL_trnaprawdtl apd ON apm.aprawmstoid=apd.aprawmstoid WHERE apm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnaprawmst apm LEFT JOIN QL_trnaprawdtl apd ON apm.aprawmstoid=apd.aprawmstoid WHERE apm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND apm.aprawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                sSql += ") ORDER BY [Code]";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSup");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSupplier, string DDLNomor, string TextNomor)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, pom.porawmstoid [Draft No.], pom.porawno [PO No.], pom.porawdate [PO Date], s.suppname [Supplier], ISNULL(pom.porawmstnote, '') [Note], pom.porawmststatus [Status] FROM QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE pom.porawmstoid IN (SELECT DISTINCT apd.porawmstoid FROM QL_trnaprawmst apm INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=apm.cmpcode AND apm.aprawmstoid=apd.aprawmstoid AND apd.porawmstoid=pom.porawmstoid /*INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=apd.cmpcode AND regd.registerdtloid=apd.registerdtloid*/  INNER JOIN QL_mstsupp s ON apm.suppoid=s.suppoid AND s.activeflag='ACTIVE' WHERE apm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += "WHERE pom.porawmstoid IN (SELECT DISTINCT apd.porawmstoid FROM QL_trnaprawmst apm INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=apm.cmpcode AND apm.aprawmstoid=apd.aprawmstoid AND apd.porawmstoid=pom.porawmstoid /*INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=apd.cmpcode AND regd.registerdtloid=apd.registerdtloid*/  INNER JOIN QL_mstsupp s ON apm.suppoid=s.suppoid AND s.activeflag='ACTIVE' WHERE apm.cmpcode LIKE '%' ";
                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND apm.aprawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY [PO Date], [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRegData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSupplier, string DDLType, string TextPO, string TextReg, string DDLNomor, string TextNomor)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, regm.registermstoid [Draft No.], regm.registerno [Reg No.], regm.registerdate [Reg Date], s.suppname [Supplier], regm.registermstnote [Note], regm.registermststatus [Status] FROM QL_trnregistermst regm INNER JOIN QL_trnregisterdtl regd ON regm.registermstoid=regd.registermstoid INNER JOIN QL_mstsupp s ON regm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE regm.registermstoid IN (SELECT DISTINCT apd.registermstoid FROM QL_trnaprawmst apm INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=apm.cmpcode AND apm.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstsupp s2 ON apm.suppoid=s2.suppoid AND s2.activeflag='ACTIVE' WHERE apm.cmpcode='" + DDLBusinessUnit + "' ";
                else
                    sSql += " WHERE regm.registermstoid IN (SELECT DISTINCT apd.registermstoid FROM QL_trnaprawmst apm INNER JOIN QL_trnaprawdtl apd ON apd.cmpcode=apm.cmpcode AND apm.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstsupp s2 ON apm.suppoid=s2.suppoid AND s2.activeflag='ACTIVE' WHERE apm.cmpcode LIKE '%' ";

                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }

                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextPO))
                    {
                        sSql += " AND apd.registerdtloid IN (SELECT registerdtloid FROM QL_trnregisterdtl regd2 INNER JOIN QL_trnporawmst pom ON pom.porawmstoid=regd2.porefmstoid ";
                        sSql += " WHERE ";
                        sSql += " pom.porawno IN ( '" + TextPO.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND apm.aprawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += ") ORDER BY [Reg Date] DESC, regm.registermstoid DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblReg");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAPData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string TextSupplier, string DDLType, string TextPO, string TextReg)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, apm.aprawmstoid [Draft No.], apm.aprawno [AP No.], apm.aprawdate [AP Date], s.suppname [Supplier], apm.aprawmstnote [Note], apm.aprawmststatus [Status] FROM QL_trnaprawmst apm LEFT JOIN QL_trnaprawdtl apd ON apm.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstsupp s ON apm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE apm.cmpcode='" + DDLBusinessUnit +"'";
                else
                    sSql += "WHERE apm.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }

                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextPO))
                    {
                        sSql += " AND apd.registerdtloid IN (SELECT registerdtloid FROM QL_trnregisterdtl regd2 INNER JOIN QL_trnporawmst pom ON pom.porawmstoid=regd2.porefmstoid ";
                        sSql += " WHERE ";
                        sSql += " pom.porawno IN ( '" + TextPO.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                    if (!string.IsNullOrEmpty(TextReg))
                    {
                        sSql += " AND apd.registermstoid IN (SELECT regm.registermstoid FROM QL_trnregistermst regm ";
                        sSql += " WHERE ";
                        sSql += " regm.registerno IN ( '" + TextReg.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND apm.aprawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += " ORDER BY [AP Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAP");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMatData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string TextSupplier, string TextPO, string DDLType, string TextReg)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.matrawcode [Code], m.matrawlongdesc [Description], g2.gendesc [Unit] FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=apd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=apd.aprawunitoid INNER JOIN QL_mstsupp s ON apm.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE apm.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE apm.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }

               if(DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextPO))
                    {
                        sSql += " AND apd.registerdtloid IN (SELECT registerdtloid FROM QL_trnregisterdtl regd2 INNER JOIN QL_trnporawmst pom ON pom.porawmstoid=regd2.porefmstoid ";
                        sSql += " WHERE ";
                        sSql += " pom.porawno IN ( '" + TextPO.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                    if (!string.IsNullOrEmpty(TextReg))
                    {
                        sSql += " AND apd.registermstoid IN (SELECT regm.registermstoid FROM QL_trnregistermst regm ";
                        sSql += " WHERE ";
                        sSql += " regm.registerno IN ( '" + TextReg.Replace(";", "','") + "')";
                        sSql += ")";
                    }
                }

                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Draft No.")
                        sSql += " AND apm.aprawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                    else
                        sSql += " AND apm.aprawno IN ('" + TextNomor.Replace(";", "','") + "')";
                }

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND apm.aprawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }




        // GET: APReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderby, string ReportType, string DDLType, string DDLCurrency, string TextSupplier, string TextPO, string TextReg, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";

            if(DDLType == "Summary")
            {
                if(DDLCurrency == "IDR")
                {
                    if (ReportType == "XLS")
                        rptfile = "rptAP_ReportAPNoXls.rpt";
                    else
                        rptfile = "rptAP_ReportAPNo.rpt";
                    rptname = "APRAWMATERIALSUMMARY";
                }
                else if(DDLCurrency == "USD")
                {
                    if (ReportType == "XLS")
                        rptfile = "rptAP_ReportAPNoXls.rpt";
                    else
                        rptfile = "rptAP_ReportAPNoUSD.rpt";
                    rptname = "APRAWMATERIALSUMMARY";
                }
                else
                {
                    if (ReportType == "XLS")
                        rptfile = "rptAP_ReportAPNoXls.rpt";
                    else
                        rptfile = "rptAP_ReportAPNoTRN.rpt";
                    rptname = "APRAWMATERIALSUMMARY";
                }
            }
            else
            {
                if(DDLCurrency == "IDR")
                {
                    if(ReportType == "XLS")
                        rptfile = "rptAP_ReportDtlAPNoXls.rpt";
                    else
                    {
                        if (DDLGrouping == "apno")
                            rptfile = "rptAP_ReportDtlAPNo.rpt";
                        else if (DDLGrouping == "regno")
                            rptfile = "rptAP_ReportDtlRegNo.rpt";
                        else if (DDLGrouping == "apdatetakegiro")
                            rptfile = "rptAP_ReportDtlTakeGiro.rpt";
                        else
                            rptfile = "rptAP_ReportDtlSupp.rpt";
                    }
                    rptname = "APRAWMATERIALDETAIL";
                }
                else
                {
                    if (ReportType == "XLS")
                        rptfile = "rptAP_ReportDtlAPNoXls.rpt";
                    else
                        if (DDLGrouping == "apno")
                        rptfile = "rptAP_ReportDtlAPNoUSD.rpt";
                    else if (DDLGrouping == "regno")
                        rptfile = "rptAP_ReportDtlRegNoUSD.rpt";
                    else if (DDLGrouping == "apdatetakegiro")
                        rptfile = "rptAP_ReportDtlTakeGiroUSD.rpt";
                    else
                        rptfile = "rptAP_ReportDtlSuppUSD.rpt";
                    rptname = "APRAWMATERIALDETAIL";
                }
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());


            var Slct = ""; var Join = "";

            if (DDLType == "Detail"){
                Slct += " , isnull(aprawdtlres2, '') [No. Invoice],apd.aprawdtloid [Dtl Oid], apd.aprawdtlseq [Seq], apd.matrawoid [Mat Oid], m.matrawcode [Mat Code] , m.matrawlongdesc [Mat Longdesc], aprawqty [Qty] , g2.gendesc [Unit], aprawprice [Price], aprawdtlamt [Detail Amount], aprawdtldisctype [Disc Type], aprawdtldiscvalue [Disc Value], aprawdtldiscamt [Disc Amount], aprawdtlnetto [Dtl Netto],aprawdtlres1[Faktur No.],aprawdtlres2 [Faktur Pajak No.], aprawdtltaxamt [Detail Tax Amount] ";
                Slct += ", (SELECT icm.importno FROM QL_trnimportmst icm WHERE icm.cmpcode=apm.cmpcode AND icm.registermstoid=apd.registermstoid) AS [IC No.] ";
                Join += " INNER JOIN QL_trnaprawdtl apd ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=apd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=apd.aprawunitoid ";

                Slct += " , /*registerno*/'' [Reg. No.], /*registerdate*/CONVERT(DATETIME,'1/1/1900') [Reg. Date], /*registerdocrefno*/'' [No. Surat Jalan], /*registerdocrefdate*/CONVERT(DATETIME,'1/1/1900') [Tgl Surat Jalan], /*registernopol*/'' [No. Polisi], /*registermstnote*/'' [Header Note], /*regm.createuser*/'' [Create User], /*regm.createtime*/CONVERT(DATETIME,'1/1/1900') [Create Datetime], (CASE /*registermststatus*/mrrawmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) [Post Reg User], (CASE /*registermststatus*/mrrawmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE mrm.updtime END) [Post Reg Datetime] ";
                //Join += " INNER JOIN QL_trnregistermst regm ON regm.cmpcode=apd.cmpcode AND regm.registermstoid=apd.registermstoid AND registertype='raw' INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=apd.cmpcode AND regd.registermstoid=apd.registermstoid AND regd.registerdtloid=apd.registerdtloid  ";

                Slct += " , mrrawno [MR No.] ";
                Join += " INNER JOIN QL_trnmrrawmst mrm ON mrm.cmpcode=apd.cmpcode AND mrm.mrrawmstoid=apd.mrrawmstoid ";

                Slct += " , CONVERT(VARCHAR(20), pom.porawmstoid) [PO Draft No.], pom.porawmstoid [PO ID], porawtype [PO Type], porawdate [PO Date], porawno [PO No.], porawmstnote [PO Header Note], porawmststatus [PO Status] , '' AS [App User PO], pom.approvaldatetime AS [App Date PO] ";
                Join += " INNER JOIN QL_trnporawmst pom ON apm.cmpcode=pom.cmpcode AND pom.porawmstoid=mrm.porawmstoid AND pom.porawmstoid=apd.porawmstoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=apm.cmpcode AND pod.porawdtloid=apd.porawdtloid AND pod.porawmstoid=pom.porawmstoid ";

                Slct += " ,REPLACE(REPLACE((RTRIM((SELECT NULLIF(cashbankno, '') + '; '  FROM QL_conap con INNER JOIN QL_trnpayap pay ON pay.cmpcode= con.cmpcode AND pay.payapoid=con.payrefoid AND pay.refoid=con.refoid AND pay.reftype=con.reftype INNER JOIN QL_trncashbankmst cbm on cbm.cmpcode=pay.cmpcode and cbm.cashbankoid=pay.cashbankoid   WHERE ISNULL(payapres1,'')='' AND con.reftype='QL_trnaprawmst' AND con.trnaptype in ('PAYAPRM', 'PAYAP') AND con.cmpcode=apm.cmpcode AND con.refoid=apm.aprawmstoid FOR XML PATH (''))) + 'END'), ';END', ''), 'END', '') AS [No. Cash/Bank],  CONVERT(Datetime,DATEADD(day,(CASE g.gendesc WHEN '-' THEN 0 WHEN 'ROG' THEN 0 ELSE CAST(g.gendesc AS INT) END),aprawdate), 101) AS [Tgl JT], '' AS [COA] ";
            }
                sSql = "SELECT apm.aprawmstoid AS [apmstoid], (select x.gendesc from ql_mstgen x where x.genoid=apm.divgroupoid) [Divisi], (CASE apm.aprawno WHEN '' THEN CONVERT(VARCHAR(10), apm.aprawmstoid) ELSE apm.aprawno END) AS [Draft No.], apm.aprawno AS [AP No.], apm.aprawdate AS [AP Date], apm.aprawdatetakegiro [AP Date Take Giro], apm.aprawmststatus AS [Status], apm.cmpcode AS [CmpCode], apm.suppoid [Suppoid], s.suppcode [Suppcode], s.suppname [Suppname], '' [Supptype], g.gendesc [Payment Type], c.currcode [Currency], (SELECT CAST(r.rateres1 AS real) FROM QL_mstrate r WHERE r.rateoid=apm.rateoid) [Rate IDR], (SELECT CAST(r.rateres2 AS real) FROM QL_mstrate r WHERE r.rateoid=apm.rateoid) [Rate USD], (SELECT CAST(r.rate2res1 AS real) FROM QL_mstrate2 r WHERE r.rate2oid=apm.rate2oid) [Rate Montly IDR], (SELECT CAST(r.rate2res2 AS real) FROM QL_mstrate2 r WHERE r.rate2oid=apm.rate2oid) [Rate Monthly USD] , apm.aprawtotalamt [AP Total Amt], apm.aprawtotalamtidr [AP Total Amt IDR], apm.aprawtotalamtusd [AP Total Amt USD], apm.aprawtotaldisc [AP Total Disc], apm.aprawtotaldiscidr [AP Total Disc IDR], apm.aprawtotaldiscusd [AP Total Disc USD], apm.aprawtotaltax [AP Total Tax], apm.aprawtotaltaxidr [AP Total Tax IDR], apm.aprawtotaltaxusd [AP Total Tax USD], apm.aprawgrandtotal [AP Grand Total], apm.aprawgrandtotalidr [AP Grand Total IDR], apm.aprawgrandtotalusd [AP Grand Total USD], (SELECT div.divname FROM QL_mstdivision div WHERE apm.cmpcode = div.cmpcode) AS [BU Name], apm.aprawmstnote AS [Header Note], apm.createuser [Create User], apm.createtime [Create Time], apm.approvaluser [App User], apm.approvaldatetime [App Datetime], apm.aprawsupptotal [Grand Total Supplier],(CASE s.supptaxable WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) [Tax] " + Slct + " FROM QL_trnaprawmst apm  " + Join + "  INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstgen g ON g.genoid=apm.aprawpaytypeoid AND g.gengroup='PAYMENT TYPE' INNER JOIN QL_mstcurr c ON c.curroid=apm.curroid ";

            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE apm.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE apm.cmpcode LIKE '%%'";

            if (!string.IsNullOrEmpty(TextSupplier))
            {
                sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
            }

            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextPO))
                    sSql += " AND pom.porawno IN ( '" + TextPO.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextReg))
                    sSql += " AND regm.registerno IN ( '" + TextReg.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND matrawcode IN ( '" + TextMaterial.Replace(";", "','") + "')";
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND apm.divgroupoid="+DDLdivgroupoid;
            }
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND apm.aprawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND apm.aprawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND apm.aprawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";


            if (DDLType == "Summary")
            {
                if (DDLSorting == "apm.prrawno")
                    sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby;
                else if (DDLSorting == "apm.approvaldatetime")
                    sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.approvaldatetime " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                else
                    sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.aprawdatetakegiro " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
            }
            else
            {
                if (DDLGrouping == "apno")
                {
                    if (DDLSorting == "regm.registerno")
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby + ", regm.registerno ASC ";
                    else if (DDLSorting == "apm.approvaldatetime")
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby + ", apm.approvaldatetime " + DDLOrderby;
                    else
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby + ", apm.aprawdatetakegiro " + DDLOrderby;
                }
                else if(DDLGrouping == "regno")
                {
                    if (DDLSorting == "apm.aprawno")
                        sSql += " ORDER BY apm.cmpcode ASC, regm.registerno ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby;
                    else if (DDLSorting == "apm.approvaldatetime")
                        sSql += " ORDER BY apm.cmpcode ASC, regm.registerno ASC, apm.approvaldatetime " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                    else
                        sSql += " ORDER BY apm.cmpcode ASC, regm.registerno ASC, apm.aprawdatetakegiro " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                }
                else if(DDLGrouping == "apdatetakegiro")
                {
                    if (DDLSorting == "apm.aprawno")
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawdatetakegiro ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby;
                    else if (DDLSorting == "regm.registerno")
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawdatetakegiro ASC, regm.registerno " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                    else if (DDLSorting == "apm.approvaldatetime")
                        sSql += " ORDER BY apm.cmpcode ASC, apm.aprawdatetakegiro ASC, apm.approvaldatetime " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                }
                else
                {
                    if (DDLSorting == "apm.aprawno")
                        sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.aprawno " + DDLOrderby + " , apm.aprawmstoid " + DDLOrderby;
                    else if (DDLSorting == "regm.registerno")
                        sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, regm.registerno " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                    else if (DDLSorting == "apm.approvaldatetime")
                        sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.approvaldatetime " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                    else
                        sSql += " ORDER BY apm.cmpcode ASC, s.suppname ASC, apm.aprawdatetakegiro " + DDLOrderby + ", apm.aprawno ASC , apm.aprawmstoid ASC ";
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower()).Replace("RM", objtype.formabbr.ToUpper());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintTerminalID", "".ToString());
            if(ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
				this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
				if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if(ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }

}