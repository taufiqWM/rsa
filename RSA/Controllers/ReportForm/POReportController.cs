﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.ReportForm
{
    public class POReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        public class divisi
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
        private void InitDDL(string rpttype = "")
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = " select 0 genoid, 'ALL' gendesc UNION ALL select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.ddldivgroupoid = new SelectList(db.Database.SqlQuery<divisi>(sSql).ToList(), "genoid", "gendesc");
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

       
            List<string> MCBStatus = new List<string> { "In Process", "In Approval", "Approved", "Closed", "Cancel", "Rejected", "Revised" };
            ViewBag.MCBStatus = MCBStatus;

            sSql = "SELECT DISTINCT UPPER(createuser) sfield FROM QL_trnregistermst WHERE registertype='Raw'";
            var DDlCreateUser = new SelectList(db.Database.SqlQuery<ReportModels.DDLSingleField>(sSql).ToList(), "sfield", "sfield");
            ViewBag.DDlCreateUser = DDlCreateUser;
        }

        [HttpPost]
        public ActionResult InitDDlCreateUser(string DDLCreate)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLDepartmentModel> tbl = new List<ReportModels.DDLDepartmentModel>();

            try
            {
                if (DDLCreate == "ISNULL(regm.createuser,'')")
                    sSql = "SELECT DISTINCT UPPER(createuser) createuser FROM QL_trnregistermst WHERE registertype='Raw'";
                else if (DDLCreate == "pom.createuser")
                    sSql = "SELECT DISTINCT UPPER(createuser) createuser FROM QL_trnporawmst";
                tbl = db.Database.SqlQuery<ReportModels.DDLDepartmentModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment (string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLDepartmentModel> tbl = new List<ReportModels.DDLDepartmentModel>();

            try
            {
                if (string.IsNullOrEmpty(DDLBusinessUnit))
                {
                    sSql = "SELECT deptoid, (deptname + ' - ' + divname) deptname FROM QL_mstdept de INNER JOIN QL_mstdivision div ON div.cmpcode=de.cmpcode WHERE de.activeflag='ACTIVE' AND div.activeflag='ACTIVE' ORDER BY deptname";
                }
                else
                {
                    sSql = "SELECT deptoid, deptname FROM QL_mstdept de WHERE de.activeflag='ACTIVE' AND de.cmpcode='" + DDLBusinessUnit + "' ORDER BY deptname";
                }
                tbl = db.Database.SqlQuery<ReportModels.DDLDepartmentModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLFilterPOQty, string TextFilterPOQty)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT Distinct 0 seq, suppcode [Code], suppname [Name], suppaddr[Addres] FROM QL_mstsupp WHERE cmpcode= '" + CompnyCode + "' AND activeflag='ACTIVE' ";

                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " AND suppoid IN (SELECT suppoid from QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid WHERE pom.cmpcode LIKE '%' ";

                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND porawmststatus IN ('" + MCBStatus + "')";

                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";

                if (!string.IsNullOrEmpty(DDLFilterPOQty))
                    sSql += " AND ISNULL(pod.porawqty,0) " + DDLFilterPOQty + " " + TextFilterPOQty;

                sSql += ") ORDER BY suppcode";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSup");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string TextSupplier, string DDLFilterPOQty, string TextFilterPOQty)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, prm.prrawmstoid [Draft No.], prm.prrawno [PR No.], CONVERT(VARCHAR(10), prm.prrawdate, 101) [PR Date],prm.deptoid [Department], prm.prrawmstnote [Note], CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) [ETA], prm.prrawmststatus [Status]  FROM QL_prrawmst prm INNER JOIN QL_prrawdtl prd ON prm.prrawmstoid=prd.prrawmstoid ";
;
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE prm.prrawmstoid IN (SELECT pod.prrawmstoid from QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid AND s.activeflag='ACTIVE' WHERE pom.cmpcode='"+ DDLBusinessUnit + "'";
                else
                    sSql += "  WHERE prm.prrawmstoid IN (SELECT pod.prrawmstoid from QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid AND s.activeflag='ACTIVE' WHERE pom.cmpcode LIKE '%' ";
                
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND pom.porawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(DDLFilterPOQty))
                    sSql += " AND ISNULL(pod.porawqty,0) " + DDLFilterPOQty + " " + TextFilterPOQty;
                sSql += ") ORDER BY [PR Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string FormType, string DDLBusinessUnit, string TextSupplier, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLType, string TextPr, string DDLPOType, string DDLFilterPOQty, string TextFilterPOQty)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, pom.porawmstoid [Draft No.], pom.porawno [PO No.], CONVERT(VARCHAR(10), pom.porawdate, 101) [PO Date] ,s.suppname [Supplier], pom.porawmststatus [Status], pom.porawmstnote [Note] FROM QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawmst prm ON prm.prrawmstoid=pod.prrawmstoid AND pom.cmpcode=prm.cmpcode LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE pom.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(TextSupplier))
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND pom.porawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(DDLPOType))
                    if (DDLPOType != "ALL")
                        sSql += " AND pom.porawtype='" + DDLPOType + "'";
                if (!string.IsNullOrEmpty(DDLFilterPOQty))
                    sSql += " AND ISNULL(pod.porawqty,0) " + DDLFilterPOQty + " " + TextFilterPOQty;

                if (DDLType == "Detail" || !string.IsNullOrEmpty(DDLFilterPOQty))
                {
                    if (!string.IsNullOrEmpty(TextPr))
                    {
                        sSql += " AND prm.prrawno IN ( '" + TextPr.Replace(";", "','") + "')";
                    }
                }
                sSql += " ORDER BY [PO Date] DESC, [Draft No.] DESC";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("raw", objtype.reftype.ToLower());
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus, string DDLNomor, string TextNomor, string TextSupplier, string DDLType, string TextPr, string DDLPOType, string DDLFilterPOQty, string TextFilterPOQty)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.matrawcode [Code], m.matrawlongdesc [Description], g2.gendesc [Unit] FROM QL_trnporawdtl pod INNER JOIN QL_trnporawmst pom ON pom.cmpcode=pod.cmpcode AND pom.porawmstoid=pod.porawmstoid LEFT JOIN QL_prrawmst prm ON prm.prrawmstoid=pod.prrawmstoid AND pom.cmpcode=prm.cmpcode LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.porawunitoid INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid AND s.activeflag='ACTIVE' ";
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += "WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += "WHERE pom.cmpcode LIKE '%%'";
                if (!string.IsNullOrEmpty(MCBStatus))
                    sSql += " AND pom.porawmststatus IN ('" + MCBStatus + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN ( '" + TextSupplier.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(DDLPOType))
                    if (DDLPOType != "ALL")
                        sSql += " AND pom.porawtype='" + DDLPOType + "'";
                if (!string.IsNullOrEmpty(DDLFilterPOQty))
                    sSql += " AND ISNULL(pod.porawqty,0) " + DDLFilterPOQty + " " + TextFilterPOQty;

                if (DDLType == "Detail" || !string.IsNullOrEmpty(DDLFilterPOQty))
                {
                    if (!string.IsNullOrEmpty(TextPr))
                    {
                        sSql += " AND prm.prrawno IN ( '" + TextPr.Replace(";", "','") + "')";
                    }
                }

                if (!string.IsNullOrEmpty(TextNomor))
                {
                   if(DDLNomor == "PO No.")
                    {
                        sSql += " AND pom.porawno IN ( '" + TextNomor.Replace(";", "','") + "')";
                    }
                   else {
                        sSql += " AND pom.porawmstoid IN ( '" + TextNomor.Replace(";", "','") + "')";
                    }
                }
                

                sSql += " ORDER BY [Code]";

                var objtype = new ReportModels.FullFormType(FormType);
                if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                    sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportInPrice
        public ActionResult ReportInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportInPrice/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportStatus
        public ActionResult ReportStatus(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportStatus/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        // GET: PRReportStatusInPrice
        public ActionResult ReportStatusInPrice(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportStatusInPrice/" + id, (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var objtype = new ReportModels.FullFormType(id);
            ViewBag.FormType = objtype.formtitle;

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string FormType, string DDLBusinessUnit, string DDLDepartment, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType,string TextSupplier,string TextPo,string DDLPono, string TextPr, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptPO_SumXls.rpt";
                else
                    rptfile = "rptPO_SumPdf.rpt";
                rptname = "PORAWMATERIALSUMMARY";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptPO_DtlXls.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPO_DtlPOPdf.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPO_DtlMatCodePdf.rpt";
                    else if (DDLGrouping == "ATGL")
                        rptfile = "rptPO_DtlATglPdf.rpt";
                    else
                        rptfile = "rptPO_DtlSuppPdf.rpt";
                }
                rptname = "PORAWMATERIALSUMMARY";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = " , pod.porawdtloid AS [Dtl Oid], porawdtlseq [No.], matrawcode [Code], matrawlongdesc [Material], ISNULL((prrawno),'') AS [PR No.], ISNULL((prd.prrawarrdatereq), CONVERT(DATETIME, '01/01/1900')) [ETA], prm.approvaldatetime [PR Approval Date], ISNULL((SELECT profname FROM QL_mstprof p4 WHERE prm.approvaluser=p4.profoid),'') AS [PR App User], porawqty [Qty], g2.gendesc [Unit], porawprice [Price], porawdtlamt [Detail Amount], porawdtldisctype [Disc Dtl Type], porawdtldiscvalue [Disc Dtl Value], porawdtldiscamt [Disc Dtl Amt], porawdtlnetto [Detail Netto], porawdtlnote [Detail Note] ";
                Join = " INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=porawunitoid LEFT JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid ";
            }
            sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE pom.cmpcode = div.cmpcode) AS [Business Unit], pom.cmpcode [CMPCODE], (select x.gendesc from ql_mstgen x where x.genoid=pom.divgroupoid) [Divisi], CONVERT(VARCHAR(20), pom.porawmstoid) [Draft No.], pom.porawmstoid [ID], porawtype [Type], porawdate [Date], porawno [PO No.], suppname [Supplier], suppcode [Supplier Code], porawsuppref [Supplier Ref.], currcode [Currency], g1.gendesc [Payment Type], /*rate2usdvalue*/0 [Daily Rate To IDR], /*rateusdvalue*/0 [Daily Rate To USD], /*rate2idrvalue*/0 [Monthly Rate To IDR], /*rate2usdvalue*/0 [Monthly Rate To USD], porawtotalamt [Total Amt], porawtotaldiscdtl [Total Disc Dtl Amt], porawtotalnetto [Total Netto], porawtaxtype [Tax Type], porawtaxamt [Tax Pct], porawvat [Tax Amount], porawgrandtotalamt [Grand Total Amt], porawmstnote [Header Note], porawmststatus [Status], pom.approvaluser AS [App User], pom.approvaldatetime AS [App Date], pom.createuser AS [Create User], pom.createtime AS [Create Date], pom.closereason, pom.closeuser, pom.closetime " + Dtl + " FROM QL_trnporawmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid INNER JOIN QL_mstgen g1 ON g1.genoid=porawpaytypeoid /*INNER JOIN QL_mstrate r ON r.rateoid=pom.rateoid INNER JOIN QL_mstrate2 r2 ON r2.rate2oid=pom.rate2oid*/ " + Join + " ";
            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += "WHERE pom.cmpcode LIKE '%%'";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND porawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextSupplier))
            {
                sSql += " AND s.suppcode IN( '" + TextSupplier.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(TextPo))
            {
                if (DDLPono == "PO No.")
                {
                    sSql += " AND pom.porawno IN ('" + TextPo.Replace(";", "','") + "')";
                }
                else
                {
                    sSql += " AND pom.porawmstoid IN ('" + TextPo.Replace(";", "','") + "')";
                }
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND pom.divgroupoid=" + DDLdivgroupoid;
            }
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(TextMaterial))
                    sSql += " AND matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
                if (!string.IsNullOrEmpty(TextPr))
                    sSql += " AND prm.prrawno IN ('" + TextPr.Replace(";", "','") + "')";
            }
            if (DDLType == "Summary")
            {
                if (DDLSorting == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , pom.porawmstoid " + DDLOrderBy;
                else
                    sSql += " ORDER BY pom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "m.matrawcode")
                {
                    if (DDLSorting == "pom.porawno")
                        sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy + " , prm.porawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }   
                else
                {
                    if (DDLSorting == "pom.porawno")
                        sSql += " ORDER BY pom.cmpcode ASC, s.suppname ASC, " + DDLSorting + " " + DDLOrderBy + " , pom.porawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY pom.cmpcode ASC, s.suppname ASC, " + DDLSorting + " " + DDLOrderBy;
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportInPrice(string FormType, string DDLBusinessUnit, string DDLDepartment, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string TextSupplier, string TextPo, string DDLPono,string TextPr, string divgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptPO_SumXls2.rpt";
                else
                    rptfile = "rptPO_SumPdf2.rpt";
                rptname = "PORAWMATERIALSUMMARYINPRICE";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptPO_DtlXls2.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPO_DtlPOPdf2.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPO_DtlMatCodePdf2.rpt";
                    else if (DDLGrouping == "ATGL")
                        rptfile = "rptPO_DtlATglPdf.rpt";
                    else if (DDLGrouping == "Voucher")
                        rptfile = "rptPO_Trn.rpt";
                    else
                        rptfile = "rptPO_DtlSuppPdf2.rpt";
                }
                rptname = "PORAWMATERIALDETAILINPRICE";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                 if (DDLGrouping == "Voucher")
                {

                }
                else
                {
                    Dtl = " , pod.porawdtloid AS [Dtl Oid] , porawdtlseq [No.], matrawcode [Code], matrawlongdesc [Material], ISNULL((prrawno),'') AS [PR No.], ISNULL((prd.prrawarrdatereq), CONVERT(DATETIME, '01/01/1900')) [ETA], prm.approvaldatetime [PR Approval Date], ISNULL((SELECT profname FROM QL_mstprof p4 WHERE prm.approvaluser=p4.profoid),'') AS [PR App User], porawqty [Qty], g2.gendesc [Unit], porawprice [Price], porawdtlamt [Detail Amount], porawdtldisctype [Disc Dtl Type], porawdtldiscvalue [Disc Dtl Value], porawdtldiscamt [Disc Dtl Amt], porawdtlnetto [Detail Netto], porawdtlnote [Detail Note] ";
                    Join = " INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=porawunitoid LEFT JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid ";

                }
            }
            if (DDLGrouping != "Voucher")
            {
                sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE pom.cmpcode = div.cmpcode) AS [Business Unit], pom.cmpcode [CMPCODE], CONVERT(VARCHAR(20), pom.porawmstoid) [Draft No.], pom.porawmstoid [ID], porawtype [Type], porawdate [Date], porawno [PO No.], suppname [Supplier], suppcode [Supplier Code], porawsuppref [Supplier Ref.], currcode [Currency], g1.gendesc [Payment Type], rateidrvalue [Daily Rate To IDR], rateusdvalue [Daily Rate To USD], rate2idrvalue [Monthly Rate To IDR], rate2usdvalue [Monthly Rate To USD], porawtotalamt [Total Amt], porawtotaldiscdtl [Total Disc Dtl Amt], porawtotalnetto [Total Netto], porawtaxtype [Tax Type], porawtaxamt [Tax Pct], porawvat [Tax Amount], porawgrandtotalamt [Grand Total Amt], porawmstnote [Header Note], porawmststatus [Status], pom.approvaluser AS [App User], pom.approvaldatetime AS [App Date], pom.createuser AS [Create User], pom.createtime AS [Create Date], pom.closereason, pom.closeuser, pom.closetime " + Dtl + " FROM QL_trnporawmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid INNER JOIN QL_mstgen g1 ON g1.genoid=porawpaytypeoid INNER JOIN QL_mstrate r ON r.rateoid=pom.rateoid INNER JOIN QL_mstrate2 r2 ON r2.rate2oid=pom.rate2oid " + Join + " ";

            }
            else
            {
                sSql = " SELECT pom.cmpcode [CMPCODE], (select x.gendesc from ql_mstgen x where x.genoid=pom.divgroupoid) [Divisi], (SELECT d.divname FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Name], (SELECT d.divaddress FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Address], (SELECT g3.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid WHERE d.cmpcode=pom.cmpcode) AS [BU City], (SELECT g4.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g4 ON g4.cmpcode=g3.cmpcode AND g4.genoid=CONVERT(INT, g3.genother1) WHERE d.cmpcode=pom.cmpcode) AS [BU Province], (SELECT d.divphone FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone], (SELECT d.divemail FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Email], (SELECT d.divphone2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Phone2], (SELECT d.divpostcode FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU PostCode], (SELECT d.divfax1 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode) AS [BU Fax1], (SELECT d.divfax2 FROM QL_mstdivision d WHERE d.cmpcode=pom.cmpcode)	AS [BU Fax2], (SELECT g5.gendesc FROM QL_mstdivision d INNER JOIN QL_mstgen g3 ON g3.genoid=d.divcityoid INNER JOIN QL_mstgen g5 ON g5.cmpcode=g3.cmpcode AND g5.genoid=CONVERT(INT, g3.genother2) WHERE d.cmpcode=pom.cmpcode) AS [BU Country], pom.porawmstoid [Oid], CONVERT(VARCHAR(10), pom.porawmstoid) AS [Draft No.], pom.porawno [PO No.], pom.porawdate [PO Date], pom.porawmstnote [Header Note], pom.porawmststatus [Status], pom.suppoid [Suppoid], (SELECT s.suppname FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Name], (SELECT s.suppcode FROM QL_mstsupp s  WHERE s.suppoid=pom.suppoid) AS [Supp Code], g1.gendesc AS [Payment Type], pom.porawmstdisctype [Disc Type], '' AS [Disc Value], ISNULL(pom.porawmstdiscamt, 0) AS [Disc Amount], pom.porawtotalnetto [Total Netto], pom.porawtaxtype [Tax Type], CONVERT(VARCHAR(10), pom.porawtaxamt) AS [Tax Amount], ISNULL(pom.porawvat, 0) AS [VAT], ISNULL(pom.porawdeliverycost, 0) AS [Delivery Cost], ISNULL(pom.porawothercost, 0) AS [Other Cost], pom.porawgrandtotalamt [Grand Total], pod.porawdtloid [DtlOid], ISNULL((SELECT m2.matrawdtl2code FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawcode) AS [Code], ISNULL((SELECT m2.matrawdtl2name FROM QL_mstmatrawdtl2 m2 WHERE pom.suppoid=m2.suppoid AND pod.matrawoid=m2.matrawoid), m.matrawlongdesc) AS [Material], prm.prrawno AS [PR No.], prm.prrawdate AS [PR Date], pod.porawqty AS [Qty], g2.gendesc AS [Unit], pod.porawprice AS [Price], pod.porawdtlamt AS [Amount], ISNULL(pod.porawdtldiscamt, 0) AS [Detail Disc.], pod.porawdtlnetto AS [Netto], pod.porawdtlnote AS [Note], pom.porawtype AS [POType], c.currcode AS [CurrCode], c.currdesc AS [CurrDesc], pom.porawsuppref AS [SuppReff], pod.porawdtldisctype AS [DtlDiscType], ISNULL(pod.porawdtldiscvalue,0) AS [DtlDiscValue], pod.porawdtlnote AS [DetailNote], pom.approvaldatetime AS [DateTime], pom.approvaluser AS [UserIDApproved], pa.profname AS [UserNameApproved], prd.prrawarrdatereq AS [ArrDateReq], pom.createuser AS [CreateUserID], pc.profname AS [CreateUserName], pom.approvaldatetime AS [ApprovalDate], currsymbol AS [CurrSymbol] FROM QL_trnporawmst pom INNER JOIN QL_mstgen g1 ON g1.genoid=pom.porawpaytypeoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.matrawoid LEFT JOIN QL_prrawmst prm ON prm.cmpcode=pod.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.porawunitoid INNER JOIN QL_mstcurr c ON c.curroid=pom.curroid LEFT JOIN QL_mstprof pa ON pa.profoid=pom.approvaluser INNER JOIN QL_mstprof pc ON pc.profoid=pom.createuser ";

            }

            if (DDLGrouping != "Voucher")
            {
                if (!string.IsNullOrEmpty(DDLBusinessUnit))
                    sSql += " WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
                else
                    sSql += " WHERE pom.cmpcode LIKE '%%'";

                if (!string.IsNullOrEmpty(MCBStatus_Tx))
                    sSql += " AND pom.porawmststatus IN ('" + MCBStatus_Tx + "')";
                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(TextNomor))
                {
                    if (DDLNomor == "Draft No.")
                        sSql += " AND pom.prawmstoid IN (" + TextNomor.Replace(";", "','") + ")";
                    else
                        sSql += " AND porawno IN ('" + TextNomor.Replace(";", "','") + "')";
                }

                if (!string.IsNullOrEmpty(TextSupplier))
                {
                    sSql += " AND s.suppcode IN( '" + TextSupplier.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(TextPo))
                {
                    if (DDLPono == "PO No.")
                    {
                        sSql += " AND pom.porawno IN ('" + TextPo.Replace(";", "','") + "')";
                    }
                    else
                    {
                        sSql += " AND pom.porawmstoid IN ('" + TextPo.Replace(";", "','") + "')";
                    }
                }

                if (DDLType == "Detail")
                {
                    if (!string.IsNullOrEmpty(TextPr))
                        sSql += " AND prm.prrawno IN ('" + TextMaterial.Replace(";", "','") + "')";
                    if (!string.IsNullOrEmpty(TextMaterial))
                        sSql += " AND matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
                }

                if (divgroupoid != "0")
                {
                    sSql += " AND pom.divgroupoid=" + divgroupoid;
                }
                if (DDLType == "Summary")
                {
                    if (DDLSorting == "pom.porawno")
                        sSql += " ORDER BY pom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy + " , pom.porawmstoid " + DDLOrderBy;
                    else
                        sSql += " ORDER BY pom.cmpcode ASC, " + DDLSorting + " " + DDLOrderBy;
                }
                else
                {
                    if (DDLGrouping == "pom.porawno" || DDLGrouping == "ATGL")
                        sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                    else if (DDLGrouping == "m.matrawcode")
                    {
                        if (DDLSorting == "pom.porawno")
                            sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy + " , pom.porawmstoid " + DDLOrderBy;
                        else
                            sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                    }
                    else
                    {
                        if (DDLSorting == "pom.porawno")
                            sSql += " ORDER BY pom.cmpcode ASC, s.suppname ASC, " + DDLSorting + " " + DDLOrderBy + " , pom.porawmstoid " + DDLOrderBy;
                        else
                            sSql += " ORDER BY pom.cmpcode ASC, s.suppname ASC, " + DDLSorting + " " + DDLOrderBy;
                    }
                }
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (ReportType == "")
            {
                if (DDLGrouping != "Voucher")
                    this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (DDLGrouping != "Voucher")
                    report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportStatus(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string DDLQty, string DDLCursorQty, string TextQty, string DDLPOType, string DDLCreate, string DDlCreateUser, string TextPRNo, string TextSupplier, string DDLdivgroupoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptPOStatusXls.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPOStatusPONoPdf.rpt";
                    else if(DDLGrouping == "prm.prrawno")
                        rptfile = "rptPOStatusPRNoPdf.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPOStatusMatCodePdf.rpt";
                    else
                        rptfile = "rptPOStatusSuppPdf.rpt";
                }
                rptname = "PORAWMATERIALSUMMARYSTATUS";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptPOStatusDtlXls.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPOStatusDtlPONoPdf.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPOStatusDtlMatCodePdf.rpt";
                    else if (DDLGrouping == "prm.prrawno")
                        rptfile = "rptPOStatusDtlPRNoPdf.rpt";
                    else if (DDLGrouping == "regm.createuser")
                        rptfile = "rptPOStatusDtlRegCreatePdf.rpt";
                    else
                        rptfile = "rptPOStatusDtlSuppPdf.rpt";
                }
                rptname = "PORAWMATERIALDETAILSTATUS";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                //Detail Register
                Dtl = " , ISNULL(regd.mrrawqty, 0) AS registerqty , ISNULL((CASE regm.mrrawno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), regm.mrrawmstoid) ELSE regm.mrrawno END),'') AS registerno,  ISNULL (regm.mrrawdate,CAST('01/01/1900' AS datetime))  AS registerdate, '' AS registerdocrefno, ISNULL (regm.mrrawdate,CAST('01/01/1900' AS datetime))  AS registerdocrefdate,  ISNULL(regm.createuser,'') AS Regcreateuser  , ISNULL (regm.createtime,CAST('01/01/1900' AS datetime)) AS RegCreateDate ";
                //Detail Return
                //Dtl += " ,(SELECT ISNULL(SUM(retd.retrawqty), 0) FROM  QL_trnretrawmst retm INNER JOIN QL_trnretrawdtl retd ON retd.cmpcode=retm.cmpcode AND retm.retrawmstoid=retd.retrawmstoid AND retd.registerdtloid=regd.registerdtloid AND retd.matrawoid=regd.matrefoid WHERE retm.cmpcode=regm.cmpcode AND retm.registermstoid=regm.registermstoid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND retm.retrawmststatus IN ('Post','Closed')) AS retqty";
                //Detail Purchase Return
                //Dtl += " ,(SELECT ISNULL(SUM(pretd.pretrawqty), 0) FROM QL_trnpretrawmst pretm INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid  AND pretd.matrawoid=regd.matrefoid AND pretd.mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnmrrawdtl mr WHERE mr.registerdtloid=regd.registerdtloid) WHERE pretm.cmpcode=regm.cmpcode AND pretm.registermstoid=regm.registermstoid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND pretm.pretrawmststatus IN ('Approved')) AS preturnqty";
                //Join Register
                Join = " LEFT JOIN QL_trnmrrawdtl regd ON regd.cmpcode=pod.cmpcode AND regd.matrawoid=pod.matrawoid AND regd.porawdtloid=pod.porawdtloid AND regd.mrrawmstoid IN (SELECT x.mrrawmstoid FROM QL_trnmrrawmst x WHERE x.mrrawmststatus IN ('Post','Closed')) LEFT JOIN QL_trnmrrawmst regm ON regm.cmpcode=pom.cmpcode AND regm.mrrawmstoid=regd.mrrawmstoid AND regm.mrrawmststatus IN ('Post','Closed')";
            }
            else
            {
                //Detail Register
                Dtl = " , (SELECT ISNULL(SUM(regd.mrrawqty), 0) FROM QL_trnmrrawdtl regd INNER JOIN QL_trnmrrawmst regm ON regd.cmpcode=regm.cmpcode AND regm.mrrawmstoid=regd.mrrawmstoid AND regm.porawmstoid=pod.porawmstoid AND regd.porawdtloid=pod.porawdtloid WHERE regd.cmpcode=pod.cmpcode AND regd.matrawoid=pod.matrawoid AND regd.porawdtloid=pod.porawdtloid AND regm.mrrawmststatus IN ('Post','Closed')) AS registerqty ";
                //Detail Return
                //Dtl += " , (SELECT ISNULL(SUM(retd.retrawqty), 0) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.cmpcode=regm.cmpcode AND regm.registermstoid=regd.registermstoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid INNER JOIN QL_trnretrawmst retm ON retm.cmpcode=regm.cmpcode AND retm.registermstoid=regm.registermstoid INNER JOIN QL_trnretrawdtl retd ON retd.cmpcode=retm.cmpcode AND retm.retrawmstoid=retd.retrawmstoid AND retd.registerdtloid=regd.registerdtloid AND retd.matrawoid=regd.matrefoid WHERE regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND retm.retrawmststatus IN ('Post','Closed')) AS retqty";
                //Detail Purchase Return
                //Dtl += " , (SELECT ISNULL(SUM(pretd.pretrawqty), 0) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.cmpcode=regm.cmpcode AND regm.registermstoid=regd.registermstoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=regm.cmpcode AND pretm.registermstoid=regm.registermstoid INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid AND pretd.matrawoid=regd.matrefoid AND pretd.mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnmrrawdtl mr WHERE mr.registerdtloid=regd.registerdtloid) WHERE regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND pretm.pretrawmststatus IN ('Approved')) AS preturnqty ";
            }
            sSql = "SELECT pom.porawmstoid AS pomstoid, pod.porawdtloid AS podtloid, (CONVERT(VARCHAR(10), pom.porawmstoid)) AS draftno, (select x.gendesc from ql_mstgen x where x.genoid=pom.divgroupoid) [Divisi], pom.porawno AS pono, pom.porawdate AS podate, pom.porawmststatus AS pomststatus, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, (CASE WHEN ISNULL(CONVERT(decimal(18,4), pod.porawdtlres1),0)=0 then ISNULL(pod.porawqty,0) ELSE CONVERT(decimal(18,4), pod.porawdtlres1) END) AS poqty, ISNULL(pod.closeqty, 0) AS POClosingQty, ISNULL(pod.porawqty,0) AS POQtyAwal, pom.cmpcode, pom.suppoid, s.suppname " + Dtl + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gendesc AS pounit, (SELECT div.divname FROM QL_mstdivision div WHERE pom.cmpcode = div.cmpcode) AS divname, pod.porawdtlnote AS podtlnote, pom.porawmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, ISNULL(prm.prrawno,'') AS [PR No.], s.suppcode AS [Code Supplier], ISNULL((SELECT deptname FROM QL_mstdept d1 WHERE d1.deptoid=prm.deptoid),'') AS [PR Dept], ISNULL(prm.prrawexpdate,'') AS [PR Exp Date], ISNULL(prm.prrawmstnote,'') AS [PR Header Note], /*ISNULL((SELECT ISNULL(prd.prrawarrdatereq,'') FROM QL_prrawdtl prd WHERE prd.cmpcode=prm.cmpcode AND prd.prrawmstoid=prm.prrawmstoid AND prd.prrawdtloid = pod.prrawdtloid),'')*/ ISNULL(prd.prrawarrdatereq,'') AS [PR Date Req], pom.closereason, pom.closeuser, pom.closetime FROM QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid LEFT JOIN QL_prrawmst prm ON pom.cmpcode=prm.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid " + Join + " INNER JOIN QL_mstmatraw m ON  m.matrawoid=pod.matrawoid INNER JOIN QL_mstgen g ON g.genoid=pod.porawunitoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid ";

            // Bagian Filter
            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE pom.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(DDLPOType))
                if (DDLPOType != "ALL")
                    sSql += " AND pom.porawtype='" + DDLPOType + "'";
            if (!string.IsNullOrEmpty(TextSupplier))
                sSql += " AND s.suppcode IN ('" + TextSupplier.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextPRNo))
                sSql += " AND prm.prrawno IN ('" + TextPRNo.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND pom.porawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND pom.porawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND porawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(TextMaterial))
                sSql += " AND m.matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(DDlCreateUser))
                    if (DDlCreateUser.ToUpper() != "ALL")
                    {
                        if (DDlCreateUser.ToUpper() == "NONE")
                            sSql += " AND " + DDLCreate + " = '' ";
                        else
                            sSql += " AND " + DDLCreate + " = '" + DDlCreateUser + "' ";
                    }
            }
            if (DDLdivgroupoid != "0")
            {
                sSql += " AND pom.divgroupoid=" + DDLdivgroupoid;
            }
            
            // Bagian Order
            if (DDLType == "Summary")
            {
                if (DDLGrouping == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "m.matrawcode")
                    sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else
                    sSql += " ORDER BY pom.cmpcode ASC, s.suppcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "m.matrawcode")
                    sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, s.suppcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "regm.createuser")
                    sSql += " ORDER BY pom.cmpcode ASC,regm.createuser ASC, pom.porawno ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else
                    sSql += " ORDER BY pom.cmpcode ASC, s.suppcode ASC, m.matrawcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("registertype='Raw'", "registertype='" + objtype.reftype + "'");
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
            {
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());
            }
            else if (objtype.flagtype == "B")
            {
                sSql = sSql.Replace("m.matrawcode", "(CASE prassetreftype WHEN 'General' THEN (SELECT matgencode FROM QL_mstmatgen m WHERE matgenoid=prassetrefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart m WHERE sparepartoid=prassetrefoid) ELSE '' END)").Replace("m.matrawlongdesc", "(CASE prassetreftype WHEN 'General' THEN (SELECT matgenlongdesc FROM QL_mstmatgen m WHERE matgenoid=prassetrefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart m WHERE sparepartoid=prassetrefoid) ELSE '' END)").Replace("pod.matrawoid", "poassetrefoid").Replace("prd.matrawoid", "prassetrefoid").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", "").Replace("raw", objtype.reftype.ToLower());
            }
            else if (objtype.flagtype == "C")
            {
                if (objtype.reftype.ToLower() != "log")
                {
                    sSql = sSql.Replace("m.matrawcode", "(cat1code + '.' + cat2code + '.' + cat3code)").Replace("m.matrawlongdesc", "RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END))").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", " INNER JOIN QL_matcat3 c3 ON cat3oid=matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid").Replace("pod.matrawoid", "pod.matwipoid").Replace("prd.matrawoid", "prd.matwipoid").Replace("raw", objtype.reftype.ToLower());
                }
                else
                {
                    sSql = sSql.Replace("m.matrawcode", "(cat1code + '.' + cat2code)").Replace("m.matrawlongdesc", "RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END))").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", " INNER JOIN QL_mstcat2 c2 ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid").Replace("pod.matrawoid", "pod.matsawnoid").Replace("prd.matrawoid", "prd.matsawnoid").Replace("raw", objtype.reftype.ToLower());
                }
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            for (int i = 0; i < dtRpt.Rows.Count; i++)
            {
                if (DDLType == "Detail")
                {
                    dtRpt.Rows[i]["balanceqty"] = (decimal)dtRpt.Rows[i]["POQtyAwal"] - (decimal)dtRpt.Rows[i]["POClosingQty"] - (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString()) + (decimal)dtRpt.Compute("SUM(sumretqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString()) + (decimal)dtRpt.Compute("SUM(sumpretqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    dtRpt.Rows[i]["sumregqty"] = (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    //dtRpt.Rows[i]["sumretqty"] = (decimal)dtRpt.Compute("SUM(retqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    //dtRpt.Rows[i]["sumpretqty"] = (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    if (dtRpt.Rows[i]["pomststatus"].ToString() == "Closed" && (decimal)dtRpt.Compute("SUM(balanceqty)", "pomstoid=" + dtRpt.Rows[i]["pomstoid"].ToString()) > 0)
                        dtRpt.Rows[i]["pomststatus"] = "Approved";
                }
            }

            DataView dvRpt = dtRpt.DefaultView;
            if (!string.IsNullOrEmpty(TextQty))
            {
                if (DDLQty == "PO Qty")
                    dvRpt.RowFilter = "POQtyAwal " + DDLCursorQty + " " + TextQty;
                else
                    dvRpt.RowFilter = "balanceqty " + DDLCursorQty + " " + TextQty;
            }

            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (ReportType != "XLS")
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        [HttpPost]
        public ActionResult PrintReportStatusInPrice(string FormType, string DDLBusinessUnit, string StartDate, string EndDate, string DDLPeriod, string MCBStatus_Tx, string DDLNomor, string TextNomor, string TextMaterial, string DDLGrouping, string DDLSorting, string DDLOrderBy, string ReportType, string DDLType, string DDLQty, string DDLCursorQty, string TextQty, string DDLPOType, string DDLCreate, string DDlCreateUser, string TextPRNo, string TextSupplier)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptPOStatusXls2.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPOStatusPONoPdf2.rpt";
                    else if (DDLGrouping == "prm.prrawno")
                        rptfile = "rptPOStatusPRNoPdf2.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPOStatusMatCodePdf2.rpt";
                    else
                        rptfile = "rptPOStatusSuppPdf2.rpt";
                }
                rptname = "PORAWMATERIALSUMMARYSTATUS";
            }
            else
            {
                if (ReportType == "XLS")
                    rptfile = "rptPOStatusDtlXls2.rpt";
                else
                {
                    if (DDLGrouping == "pom.porawno")
                        rptfile = "rptPOStatusDtlPONoPdf2.rpt";
                    else if (DDLGrouping == "m.matrawcode")
                        rptfile = "rptPOStatusDtlMatCodePdf2.rpt";
                    else if (DDLGrouping == "prm.prrawno")
                        rptfile = "rptPOStatusDtlPRNoPdf2.rpt";
                    else if (DDLGrouping == "regm.createuser")
                        rptfile = "rptPOStatusDtlRegCreatePdf2.rpt";
                    else
                        rptfile = "rptPOStatusDtlSuppPdf2.rpt";
                }
                rptname = "PORAWMATERIALDETAILSTATUS";
            }
            rptname = rptname.Replace("RAWMATERIAL", FormType.Replace(" ", "").ToUpper());

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                //Detail Register
                Dtl = " , ISNULL(regd.registerqty, 0) AS registerqty , ISNULL((CASE regm.registerno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), regm.registermstoid) ELSE regm.registerno END),'') AS registerno,  ISNULL (regm.registerdate,CAST('01/01/1900' AS datetime))  AS registerdate, ISNULL(regm.registerdocrefno,'') AS registerdocrefno, ISNULL (regm.registerdocrefdate ,CAST('01/01/1900' AS datetime))  AS registerdocrefdate,  ISNULL(regm.createuser,'') AS Regcreateuser  , ISNULL (regm.createtime,CAST('01/01/1900' AS datetime)) AS RegCreateDate ";
                //Detail Return
                Dtl += " , (SELECT ISNULL(SUM(retd.retrawqty), 0) FROM  QL_trnretrawmst retm INNER JOIN QL_trnretrawdtl retd ON retd.cmpcode=retm.cmpcode AND retm.retrawmstoid=retd.retrawmstoid AND retd.registerdtloid=regd.registerdtloid AND retd.matrawoid=regd.matrefoid WHERE retm.cmpcode=regm.cmpcode AND retm.registermstoid=regm.registermstoid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND retm.retrawmststatus IN ('Post','Closed')) AS retqty";
                //Detail Purchase Return
                Dtl += " , (SELECT ISNULL(SUM(pretd.pretrawqty), 0) FROM QL_trnpretrawmst pretm INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid  AND pretd.matrawoid=regd.matrefoid AND pretd.mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnmrrawdtl mr WHERE mr.registerdtloid=regd.registerdtloid) WHERE pretm.cmpcode=regm.cmpcode AND pretm.registermstoid=regm.registermstoid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND pretm.pretrawmststatus IN ('Approved')) AS preturnqty";
                //Join Register
                Join = "  LEFT JOIN QL_trnregisterdtl regd ON regd.cmpcode=pod.cmpcode AND regd.matrefoid=pod.matrawoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid AND regd.registermstoid IN (SELECT x.registermstoid FROM QL_trnregistermst x WHERE x.registermststatus IN ('Post','Closed') AND x.registertype='Raw') LEFT JOIN QL_trnregistermst regm ON regm.cmpcode=pom.cmpcode AND regm.registermstoid=regd.registermstoid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed')";
            }
            else
            {
                //Detail Register
                Dtl = " , (SELECT ISNULL(SUM(regd.registerqty), 0) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.cmpcode=regm.cmpcode AND regm.registermstoid=regd.registermstoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid WHERE regd.cmpcode=pod.cmpcode AND regd.matrefoid=pod.matrawoid AND regd.porefdtloid=pod.porawdtloid AND regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed')) AS registerqty ";
                //Detail Return
                Dtl += " , (SELECT ISNULL(SUM(retd.retrawqty), 0) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.cmpcode=regm.cmpcode AND regm.registermstoid=regd.registermstoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid INNER JOIN QL_trnretrawmst retm ON retm.cmpcode=regm.cmpcode AND retm.registermstoid=regm.registermstoid INNER JOIN QL_trnretrawdtl retd ON retd.cmpcode=retm.cmpcode AND retm.retrawmstoid=retd.retrawmstoid AND retd.registerdtloid=regd.registerdtloid AND retd.matrawoid=regd.matrefoid WHERE regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND retm.retrawmststatus IN ('Post','Closed')) AS retqty";
                //Detail Purchase Return
                Dtl += " , (SELECT ISNULL(SUM(pretd.pretrawqty), 0) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.cmpcode=regm.cmpcode AND regm.registermstoid=regd.registermstoid AND regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod.porawdtloid INNER JOIN QL_trnpretrawmst pretm ON pretm.cmpcode=regm.cmpcode AND pretm.registermstoid=regm.registermstoid INNER JOIN QL_trnpretrawdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretrawmstoid=pretd.pretrawmstoid AND pretd.matrawoid=regd.matrefoid AND pretd.mrrawdtloid IN (SELECT mrrawdtloid FROM QL_trnmrrawdtl mr WHERE mr.registerdtloid=regd.registerdtloid) WHERE regm.registertype='Raw' AND regm.registermststatus IN ('Post','Closed') AND pretm.pretrawmststatus IN ('Approved')) AS preturnqty ";
            }
            sSql = "SELECT pom.porawmstoid AS pomstoid, pod.porawdtloid AS podtloid, (CONVERT(VARCHAR(10), pom.porawmstoid)) AS draftno, pom.porawno AS pono, pom.porawdate AS podate, pom.porawmststatus AS pomststatus, m.matrawcode AS matcode, m.matrawlongdesc AS matlongdesc, (CASE WHEN ISNULL(CONVERT(decimal(18,4), pod.porawdtlres1),0)=0 then ISNULL(pod.porawqty,0) ELSE CONVERT(decimal(18,4), pod.porawdtlres1) END) AS poqty, ISNULL(pod.closeqty, 0) AS POClosingQty, ISNULL(pod.porawqty,0) AS POQtyAwal, pod.porawprice AS poprice, pod.porawdtlamt AS podtlamt, pod.porawdtldiscvalue AS podtldiscvalue, pod.porawdtldiscamt AS podtldiscamt, pod.porawdtlnetto AS podtlnetto, pom.cmpcode, pom.suppoid, s.suppname, (CASE s.supptaxable WHEN 0 THEN 'NON TAX' ELSE 'TAX' END) AS Tax " + Dtl + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gendesc AS pounit, (SELECT div.divname FROM QL_mstdivision div WHERE pom.cmpcode = div.cmpcode) AS divname, pod.porawdtlnote AS podtlnote, pom.porawmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, ISNULL(prm.prrawno,'') AS [PR No.], pom.curroid, s.suppcode AS [Code Supplier], ISNULL((SELECT deptname FROM QL_mstdept d1 WHERE d1.deptoid=prm.deptoid),'') AS [PR Dept], ISNULL(prm.prrawexpdate,'') AS [PR Exp Date], ISNULL(prm.prrawmstnote,'') AS [PR Header Note], /* ISNULL((SELECT ISNULL(prd.prrawarrdatereq,'') FROM QL_prrawdtl prd WHERE prd.cmpcode=prm.cmpcode AND prd.prrawmstoid=prm.prrawmstoid AND prd.prrawdtloid = pod.prrawdtloid),'')*/ ISNULL(prd.prrawarrdatereq,'') AS [PR Date Req], pom.closereason, pom.closeuser, pom.closetime FROM QL_trnporawmst pom INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=pom.cmpcode AND pod.porawmstoid=pom.porawmstoid LEFT JOIN QL_prrawmst prm ON pom.cmpcode=prm.cmpcode AND prm.prrawmstoid=pod.prrawmstoid LEFT JOIN QL_prrawdtl prd ON pod.cmpcode=prd.cmpcode AND prd.prrawmstoid=pod.prrawmstoid AND prd.prrawdtloid=pod.prrawdtloid " + Join + " INNER JOIN QL_mstmatraw m ON  m.matrawoid=pod.matrawoid INNER JOIN QL_mstgen g ON g.genoid=pod.porawunitoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid ";

            // Bagian Filter
            if (!string.IsNullOrEmpty(DDLBusinessUnit))
                sSql += " WHERE pom.cmpcode='" + DDLBusinessUnit + "'";
            else
                sSql += " WHERE pom.cmpcode LIKE '%'";
            if (!string.IsNullOrEmpty(DDLPOType))
                if (DDLPOType != "ALL")
                    sSql += " AND pom.porawtype='" + DDLPOType + "'";
            if (!string.IsNullOrEmpty(TextSupplier))
                sSql += " AND s.suppcode IN ('" + TextSupplier.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(TextPRNo))
                sSql += " AND prm.prrawno IN ('" + TextPRNo.Replace(";", "','") + "')";
            if (!string.IsNullOrEmpty(MCBStatus_Tx))
                sSql += " AND pom.porawmststatus IN ('" + MCBStatus_Tx + "')";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLNomor == "Draft No.")
                    sSql += " AND pom.porawmstoid IN (" + TextNomor.Replace(";", ",") + ")";
                else
                    sSql += " AND porawno IN ('" + TextNomor.Replace(";", "','") + "')";
            }
            if (!string.IsNullOrEmpty(TextMaterial))
                sSql += " AND m.matrawcode IN ('" + TextMaterial.Replace(";", "','") + "')";
            if (DDLType == "Detail")
            {
                if (!string.IsNullOrEmpty(DDlCreateUser))
                    if (DDlCreateUser.ToUpper() != "ALL")
                    {
                        if (DDlCreateUser.ToUpper() == "NONE")
                            sSql += " AND " + DDLCreate + " = '' ";
                        else
                            sSql += " AND " + DDLCreate + " = '" + DDlCreateUser + "' ";
                    }
            }

            // Bagian Order
            if (DDLType == "Summary")
            {
                if (DDLGrouping == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "m.matrawcode")
                    sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else
                    sSql += " ORDER BY pom.cmpcode ASC, s.suppcode ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
            }
            else
            {
                if (DDLGrouping == "pom.porawno")
                    sSql += " ORDER BY pom.cmpcode ASC, pom.porawno ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "m.matrawcode")
                    sSql += " ORDER BY pom.cmpcode ASC, m.matrawcode ASC, s.suppcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
                else if (DDLGrouping == "regm.createuser")
                    sSql += " ORDER BY pom.cmpcode ASC,regm.createuser ASC, pom.porawno ASC, m.matrawcode ASC, " + DDLSorting + " " + DDLOrderBy;
                else
                    sSql += " ORDER BY pom.cmpcode ASC, s.suppcode ASC, m.matrawcode ASC, pom.porawno ASC, pom.porawmstoid ASC, " + DDLSorting + " " + DDLOrderBy;
            }

            var objtype = new ReportModels.FullFormType(FormType);
            if (objtype.reftype.ToLower() != "raw")
                sSql = sSql.Replace("registertype='Raw'", "registertype='" + objtype.reftype + "'");
            if (objtype.flagtype == "A" && objtype.reftype.ToLower() != "raw")
            {
                sSql = sSql.Replace("matraw", objtype.mattype.ToLower()).Replace("raw", objtype.reftype.ToLower());
            }
            else if (objtype.flagtype == "B")
            {
                sSql = sSql.Replace("m.matrawcode", "(CASE prassetreftype WHEN 'General' THEN (SELECT matgencode FROM QL_mstmatgen m WHERE matgenoid=prassetrefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart m WHERE sparepartoid=prassetrefoid) ELSE '' END)").Replace("m.matrawlongdesc", "(CASE prassetreftype WHEN 'General' THEN (SELECT matgenlongdesc FROM QL_mstmatgen m WHERE matgenoid=prassetrefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart m WHERE sparepartoid=prassetrefoid) ELSE '' END)").Replace("pod.matrawoid", "poassetrefoid").Replace("prd.matrawoid", "prassetrefoid").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", "").Replace("raw", objtype.reftype.ToLower());
            }
            else if (objtype.flagtype == "C")
            {
                if (objtype.reftype.ToLower() != "log")
                {
                    sSql = sSql.Replace("m.matrawcode", "(cat1code + '.' + cat2code + '.' + cat3code)").Replace("m.matrawlongdesc", "RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat3shortdesc)='None' OR LTRIM(cat3shortdesc)='') THEN '' ELSE cat3shortdesc + ' ' END))").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", " INNER JOIN QL_matcat3 c3 ON cat3oid=matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid").Replace("pod.matrawoid", "pod.matwipoid").Replace("prd.matrawoid", "prd.matwipoid").Replace("raw", objtype.reftype.ToLower());
                }
                else
                {
                    sSql = sSql.Replace("m.matrawcode", "(cat1code + '.' + cat2code)").Replace("m.matrawlongdesc", "RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END))").Replace(" INNER JOIN QL_mstmatraw m ON m.matrawoid=prd.matrawoid", " INNER JOIN QL_mstcat2 c2 ON cat2oid=matsawnoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid").Replace("pod.matrawoid", "pod.matsawnoid").Replace("prd.matrawoid", "prd.matsawnoid").Replace("raw", objtype.reftype.ToLower());
                }
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            for (int i = 0; i < dtRpt.Rows.Count; i++)
            {
                if (DDLType == "Detail")
                {
                    dtRpt.Rows[i]["balanceqty"] = (decimal)dtRpt.Rows[i]["POQtyAwal"] - (decimal)dtRpt.Rows[i]["POClosingQty"] - (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString()) + (decimal)dtRpt.Compute("SUM(retqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString()) + (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    dtRpt.Rows[i]["sumregqty"] = (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    dtRpt.Rows[i]["sumretqty"] = (decimal)dtRpt.Compute("SUM(retqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    dtRpt.Rows[i]["sumpretqty"] = (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[i]["podtloid"].ToString());
                    if (dtRpt.Rows[i]["pomststatus"].ToString() == "Closed" && (decimal)dtRpt.Compute("SUM(balanceqty)", "pomstoid=" + dtRpt.Rows[i]["pomstoid"].ToString()) > 0)
                        dtRpt.Rows[i]["pomststatus"] = "Approved";
                }
            }

            DataView dvRpt = dtRpt.DefaultView;
            if (!string.IsNullOrEmpty(TextQty))
            {
                if (DDLQty == "PO Qty")
                    dvRpt.RowFilter = "POQtyAwal " + DDLCursorQty + " " + TextQty;
                else
                    dvRpt.RowFilter = "balanceqty " + DDLCursorQty + " " + TextQty;
            }

            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            string[] exctbl = { "command" };
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                this.HttpContext.Session["rptlogon"] = "True";
                this.HttpContext.Session["rptexctblname"] = exctbl;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                ClassProcedure.SetDBLogonForReport(report, exctbl);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (ReportType != "XLS")
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}