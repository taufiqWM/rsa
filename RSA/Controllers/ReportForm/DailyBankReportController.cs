﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class DailyBankReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL()
        {

            sSql = "SELECT 'RCA' divcode,'RUKUN CITRA ABADI' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            var sVar = "VAR_BANK";
            var DDLAccount = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, sVar)).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLAccount = DDLAccount;
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string DDLBusinessUnit)
        {
            var sVar = "VAR_BANK";
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, sVar);
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }
             
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLBusinessUnit_Text, string DDLAccount, string StartDate, string EndDate, string ReportType, string DDLType, string DDLCurrency)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account"); 

            var rptfile = ""; var rptname = "DAILYBANKREPORT";
            if (DDLType == "Summary")
            {
                //if (DDLCurrency != "VALAS")
                rptfile = "rptDailyBank.rpt";
                //else
                //    rptfile = "rptDailyBankValas.rpt";
            }
            //else
            //{
            //    if (ReportType == "XLS")
            //        rptfile = "rptDailyKasBankDtlXls.rpt";
            //    else
            //        rptfile = "rptDailyKasBankDtl.rpt";
            //}

            var sPeriod = Convert.ToDateTime(StartDate).ToString("dd MM yyyy") + "-" + Convert.ToDateTime(EndDate).ToString("dd MM yyyy");
            var sWhere = " WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + DDLAccount;
            sWhere += " AND  [Tanggal]>=CAST('" + StartDate + " 00:00' AS DATETIME) AND [Tanggal]<=CAST('" + EndDate + " 23:59' AS DATETIME)";

            sSql = "DECLARE @cmpcode VARCHAR(10), @acctgoid INT;" +
                "SET @cmpcode='" + CompnyCode + "';" +
                "SET @acctgoid=" + Convert.ToInt32(DDLAccount) + ";";

            sSql += "SELECT *, "+ DDLBusinessUnit_Text + " [Business Unit] " +
                "FROM (" +
                "SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], ISNULL(suppcode, '') [Kode Supp/Cust], ISNULL(suppname, '') [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], cashbankglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], cashbankglamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], cashbankgloid [Urutan], gl.cashbankglnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid LEFT JOIN QL_mstsupp s ON s.suppoid=(CASE WHEN cashbankgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN refsuppoid ELSE 0 END) INNER JOIN QL_mstacctg acc ON (CASE WHEN cashbankgroup IN ('MUTATION') THEN ISNULL((SELECT ax.acctgoid FROM QL_mstacctg ax INNER JOIN QL_mstinterface i ON i.cmpcode=ax.cmpcode AND interfacevalue=ax.acctgcode WHERE interfacevar='VAR_AYAT_SILANG' AND interfaceres1=gl.cmpcode), 0) ELSE gl.acctgoid END)=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBK' AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
               "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], ISNULL(custcode, '') [Kode Supp/Cust], ISNULL(custname, '') [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], cashbankglamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], cashbankglamt [Amt. Credit Valas], cashbankgloid [Urutan], gl.cashbankglnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid LEFT JOIN QL_mstcust s ON s.custoid=(CASE WHEN cashbankgroup IN ('RECEIPT GIRO') THEN refsuppoid ELSE 0 END) INNER JOIN QL_mstacctg acc ON (CASE WHEN cashbankgroup IN ('MUTATIONTO') THEN ISNULL((SELECT ax.acctgoid FROM QL_mstacctg ax INNER JOIN QL_mstinterface i ON i.cmpcode=ax.cmpcode AND interfacevalue=ax.acctgcode WHERE interfacevar='VAR_AYAT_SILANG' AND interfaceres1=gl.cmpcode), 0) ELSE gl.acctgoid END)=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBM' AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN ISNULL(ap.payapres1, '')='Kurang Bayar' THEN (payapamtidr*-1) WHEN ISNULL(ap.payapres1, '')='' THEN (payapamtidr + ISNULL((SELECT SUM(px.payapamtidr) FROM QL_trnpayap px WHERE px.cmpcode=ap.cmpcode AND px.cashbankoid=ap.cashbankoid AND px.reftype=ap.reftype AND px.refoid=ap.refoid AND ISNULL(px.payapres1, '')='Kurang Bayar'), 0)) ELSE payapamtidr END) [Amt. Debet IDR], 0.0 [Amt. Credit IDR], (CASE WHEN ISNULL(ap.payapres1, '')='Kurang Bayar' THEN (payapamt*-1) WHEN ISNULL(ap.payapres1, '')='' THEN (payapamt + ISNULL((SELECT SUM(px.payapamt) FROM QL_trnpayap px WHERE px.cmpcode=ap.cmpcode AND px.cashbankoid=ap.cashbankoid AND px.reftype=ap.reftype AND px.refoid=ap.refoid AND ISNULL(px.payapres1, '')='Kurang Bayar'), 0)) ELSE payapamt END) [Amt. Debet Valas], 0.0 [Amt. Credit Valas], ap.payapoid [Urutan], ap.payapnote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(ap.payapres1, '')='' THEN '' ELSE ap.payapres1 + ' ' END) + vap.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbankoid=ap.cashbankoid INNER JOIN View_AddInfoPayAP vap ON vap.cmpcode=ap.cmpcode AND vap.payapoid=ap.payapoid INNER JOIN QL_mstacctg acc ON ap.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBK' AND cashbankstatus='POST' AND cashbankgroup LIKE 'AP%' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], (CASE WHEN ISNULL(ar.payarres1, '')='Kurang Bayar' THEN (payaramtidr*-1) WHEN ISNULL(ar.payarres1, '')='' THEN (payaramtidr + ISNULL((SELECT SUM(px.payaramtidr) FROM QL_trnpayar px WHERE px.cmpcode=ar.cmpcode AND px.cashbankoid=ar.cashbankoid AND px.reftype=ar.reftype AND px.refoid=ar.refoid AND ISNULL(px.payarres1, '')='Kurang Bayar'), 0)) ELSE payaramtidr END) [Amt. Credit IDR], 0.0 [Amt. Debet Valas], (CASE WHEN ISNULL(ar.payarres1, '')='Kurang Bayar' THEN (payaramt*-1) WHEN ISNULL(ar.payarres1, '')='' THEN (payaramt + ISNULL((SELECT SUM(px.payaramt) FROM QL_trnpayar px WHERE px.cmpcode=ar.cmpcode AND px.cashbankoid=ar.cashbankoid AND px.reftype=ar.reftype AND px.refoid=ar.refoid AND ISNULL(px.payarres1, '')='Kurang Bayar'), 0)) ELSE payaramt END) [Amt. Credit Valas], ar.payaroid [Urutan], ar.payarnote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(ar.payarres1, '')='' THEN '' ELSE ar.payarres1 + ' ' END) + var.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar ar ON cb.cmpcode=ar.cmpcode AND cb.cashbankoid=ar.cashbankoid INNER JOIN View_AddInfoPayAR var ON var.cmpcode=ar.cmpcode AND var.payaroid=ar.payaroid INNER JOIN QL_mstacctg acc ON ar.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstcust s ON s.custoid=ar.custoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBM' AND cashbankstatus='POST' AND cashbankgroup LIKE 'AR%' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], dpapamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], dpapamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], dpapoid [Urutan], ap.dpapnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trndpap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbankoid=ap.cashbankoid INNER JOIN QL_mstacctg acc ON ap.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBK' AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], dparamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], dparamt [Amt. Credit Valas], dparoid [Urutan], ap.dparnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trndpar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbankoid=ap.cashbankoid INNER JOIN QL_mstacctg acc ON ap.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype='BBM' AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN ISNULL(pay.payapres1, '')='Kurang Bayar' THEN (payapamtidr*-1) WHEN ISNULL(pay.payapres1, '')='' THEN (payapamtidr + ISNULL((SELECT SUM(px.payapamtidr) FROM QL_trnpayap px WHERE px.cmpcode=pay.cmpcode AND px.cashbankoid=pay.cashbankoid AND px.reftype=pay.reftype AND px.refoid=pay.refoid AND ISNULL(px.payapres1, '')='Kurang Bayar'), 0)) ELSE payapamtidr END) [Amt. Debet IDR], 0.0 [Amt. Credit IDR], (CASE WHEN ISNULL(pay.payapres1, '')='Kurang Bayar' THEN (payapamt*-1) WHEN ISNULL(pay.payapres1, '')='' THEN (payapamt + ISNULL((SELECT SUM(px.payapamt) FROM QL_trnpayap px WHERE px.cmpcode=pay.cmpcode AND px.cashbankoid=pay.cashbankoid AND px.reftype=pay.reftype AND px.refoid=pay.refoid AND ISNULL(px.payapres1, '')='Kurang Bayar'), 0)) ELSE payapamt END) [Amt. Debet Valas], 0.0 [Amt. Credit Valas], payapgirooid+pay.payapoid [Urutan], gm.payapgironote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(pay.payapres1, '')='' THEN '' ELSE pay.payapres1 + ' ' END) + vap.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb2.cmpcode AND pay.cashbankoid=cb2.cashbankoid INNER JOIN View_AddInfoPayAP vap ON vap.cmpcode=pay.cmpcode AND vap.payapoid=pay.payapoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=pay.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstsupp s ON s.suppoid=gm.suppoid WHERE cb.cashbanktype='BBK' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trncashbankmst' AND cb2.cashbankgroup LIKE 'AP%' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], cashbankglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], cashbankglamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], payapgirooid [Urutan], gm.payapgironote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb2.cmpcode AND gl.cashbankoid=cb2.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=gl.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstsupp s ON s.suppoid=gm.suppoid WHERE cb.cashbanktype='BBK' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trncashbankmst' AND cb2.cashbankgroup NOT LIKE 'AP%' AND cb2.cashbankgroup NOT IN ('DPAP') AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], dpapamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], dpapamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], payapgirooid [Urutan], gm.payapgironote [Detail Note], cb.cashbankrefno [No Ref], dpapno [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trndpap dp ON dp.cmpcode=cb2.cmpcode AND dp.cashbankoid=cb2.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=dp.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstsupp s ON s.suppoid=gm.suppoid WHERE cb.cashbanktype='BBK' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trncashbankmst' AND cb2.cashbankgroup IN ('DPAP') AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], payapgiroamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], payapgiroamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], payapgirooid [Urutan], gm.payapgironote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankgl cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankgloid=gm.refoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb2.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstsupp s ON s.suppoid=gm.suppoid WHERE cb.cashbanktype='BBK' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trncashbankgl' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], payapgiroamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], payapgiroamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], payapgirooid [Urutan], gm.payapgironote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(cb2.payapres1, '')='' THEN '' ELSE cb2.payapres1 + ' ' END) + vap.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trnpayap cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.payapoid=gm.refoid INNER JOIN View_AddInfoPayAP vap ON vap.cmpcode=cb2.cmpcode AND vap.payapoid=cb2.payapoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb2.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstsupp s ON s.suppoid=gm.suppoid WHERE cb.cashbanktype='BBK' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trnpayap' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], (CASE WHEN ISNULL(pay.payarres1, '')='Kurang Bayar' THEN (payaramtidr*-1) WHEN ISNULL(pay.payarres1, '')='' THEN (payaramtidr + ISNULL((SELECT SUM(px.payaramtidr) FROM QL_trnpayar px WHERE px.cmpcode=pay.cmpcode AND px.cashbankoid=pay.cashbankoid AND px.reftype=pay.reftype AND px.refoid=pay.refoid AND ISNULL(px.payarres1, '')='Kurang Bayar'), 0)) ELSE payaramtidr END) [Amt. Credit IDR], 0.0 [Amt. Debet Valas], (CASE WHEN ISNULL(pay.payarres1, '')='Kurang Bayar' THEN (payaramt*-1) WHEN ISNULL(pay.payarres1, '')='' THEN (payaramt + ISNULL((SELECT SUM(px.payaramt) FROM QL_trnpayar px WHERE px.cmpcode=pay.cmpcode AND px.cashbankoid=pay.cashbankoid AND px.reftype=pay.reftype AND px.refoid=pay.refoid AND ISNULL(px.payarres1, '')='Kurang Bayar'), 0)) ELSE payaramt END) [Amt. Credit Valas], payargirooid+pay.payaroid [Urutan], gm.payargironote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(pay.payarres1, '')='' THEN '' ELSE pay.payarres1 + ' ' END) + var.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb2.cmpcode AND pay.cashbankoid=cb2.cashbankoid INNER JOIN View_AddInfoPayAR var ON var.cmpcode=pay.cmpcode AND var.payaroid=pay.payaroid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=pay.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstcust s ON s.custoid=gm.custoid WHERE cb.cashbanktype='BBM' AND cb.cashbankstatus='POST' AND gm.reftype='Pay A/R Giro In' AND cb2.cashbankgroup LIKE 'AR%' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], cashbankglamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], cashbankglamt [Amt. Credit Valas], payargirooid [Urutan], gm.payargironote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb2.cmpcode AND gl.cashbankoid=cb2.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=gl.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstcust s ON s.custoid=gm.custoid WHERE cb.cashbanktype='BBM' AND cb.cashbankstatus='POST' AND gm.reftype='Pay A/R Giro In' AND cb2.cashbankgroup NOT LIKE 'AR%' AND cb2.cashbankgroup NOT IN ('DPAR') AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], dparamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], dparamt [Amt. Credit Valas], payargirooid [Urutan], gm.payargironote [Detail Note], cb.cashbankrefno [No Ref], dparno [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trndpar dp ON dp.cmpcode=cb2.cmpcode AND dp.cashbankoid=cb2.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=dp.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstcust s ON s.custoid=gm.custoid WHERE cb.cashbanktype='BBM' AND cb.cashbankstatus='POST' AND gm.reftype='Pay A/R Giro In' AND cb2.cashbankgroup IN ('DPAR') AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], payargiroamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], payargiroamt [Amt. Credit Valas], payargirooid [Urutan], gm.payargironote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trncashbankgl cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankgloid=gm.refoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb2.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstcust s ON s.custoid=gm.custoid WHERE cb.cashbanktype='BBM' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trncashbankgl' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc.acctgcode [Kode Perk. Transaksi], acc.acctgdesc [Ket. Perk. Transaksi], acc2.acctgcode [Kode Perk. Lawan], acc2.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], 0.0 [Amt. Debet IDR], payargiroamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], payargiroamt [Amt. Credit Valas], payargirooid [Urutan], gm.payargironote [Detail Note], cb.cashbankrefno [No Ref], ((CASE WHEN ISNULL(cb2.payarres1, '')='' THEN '' ELSE cb2.payarres1 + ' ' END) + var.transno) [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro gm ON gm.cashbankoid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode INNER JOIN QL_trnpayar cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.payaroid=gm.refoid INNER JOIN View_AddInfoPayAR var ON var.cmpcode=cb2.cmpcode AND var.payaroid=cb2.payaroid INNER JOIN QL_mstacctg acc ON acc.acctgoid=cb.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb2.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstcust s ON s.custoid=gm.custoid WHERE cb.cashbanktype='BBM' AND cb.cashbankstatus='POST' AND gm.reftype='QL_trnpayar' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], ISNULL(suppcode, ISNULL(custcode, '')) [Kode Supp/Cust], ISNULL(suppname, ISNULL(custname, '')) [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN gd.glother4='K' THEN glamtidr ELSE 0.0 END) [Amt. Debet IDR], (CASE WHEN gd.glother4='M' THEN glamtidr ELSE 0.0 END) [Amt. Credit IDR], (CASE WHEN gd.glother4='K' THEN glamt ELSE 0.0 END) [Amt. Debet Valas], (CASE WHEN gd.glother4='M' THEN glamt ELSE 0.0 END) [Amt. Credit Valas], gldtloid [Urutan], gd.glnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trngldtl gd ON gd.cmpcode=cb.cmpcode AND gd.glother1='QL_trncashbankmst ' + CAST(cb.cashbankoid AS VARCHAR(10)) AND ISNULL(gd.glother3, '')<>'' AND ISNULL(gd.glother4, '')<>'' INNER JOIN QL_mstacctg acc ON gd.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s1 ON s1.suppoid=(CASE WHEN cashbankgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN refsuppoid WHEN cashbankgroup IN ('DPAP') THEN cb.personoid WHEN cashbankgroup LIKE 'AP%' THEN cb.personoid ELSE 0 END) LEFT JOIN QL_mstcust s2 ON s2.custoid=(CASE WHEN cashbankgroup IN ('RECEIPT GIRO') THEN refsuppoid WHEN cashbankgroup IN ('DPAR') THEN cb.personoid WHEN cashbankgroup LIKE 'AR%' THEN cb.personoid ELSE 0 END) INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype IN ('BBK', 'BBM') AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cashbankduedate [Tanggal], cashbankno [Nomor Bukti], cashbanknote [Keterangan], ISNULL(suppcode, ISNULL(custcode, '')) [Kode Supp/Cust], ISNULL(suppname, ISNULL(custname, '')) [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN gd.glother4='K' THEN glamtidr ELSE 0.0 END) [Amt. Debet IDR], (CASE WHEN gd.glother4='M' THEN glamtidr ELSE 0.0 END) [Amt. Credit IDR], (CASE WHEN gd.glother4='K' THEN glamt ELSE 0.0 END) [Amt. Debet Valas], (CASE WHEN gd.glother4='M' THEN glamt ELSE 0.0 END) [Amt. Credit Valas], gldtloid [Urutan], gd.glnote [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trngldtl_hist gd ON gd.cmpcode=cb.cmpcode AND gd.glother1='QL_trncashbankmst ' + CAST(cb.cashbankoid AS VARCHAR(10)) AND ISNULL(gd.glother3, '')<>'' AND ISNULL(gd.glother4, '')<>'' INNER JOIN QL_mstacctg acc ON gd.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s1 ON s1.suppoid=(CASE WHEN cashbankgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN refsuppoid WHEN cashbankgroup IN ('DPAP') THEN cb.personoid WHEN cashbankgroup LIKE 'AP%' THEN cb.personoid ELSE 0 END) LEFT JOIN QL_mstcust s2 ON s2.custoid=(CASE WHEN cashbankgroup IN ('RECEIPT GIRO') THEN refsuppoid WHEN cashbankgroup IN ('DPAR') THEN cb.personoid WHEN cashbankgroup LIKE 'AR%' THEN cb.personoid ELSE 0 END) INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cashbanktype IN ('BBK', 'BBM') AND cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], ISNULL(suppcode, ISNULL(custcode, '')) [Kode Supp/Cust], ISNULL(suppname, ISNULL(custname, '')) [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN gd.glother4='K' THEN glamtidr ELSE 0.0 END) [Amt. Debet IDR], (CASE WHEN gd.glother4='M' THEN glamtidr ELSE 0.0 END) [Amt. Credit IDR], (CASE WHEN gd.glother4='K' THEN glamt ELSE 0.0 END) [Amt. Debet Valas], (CASE WHEN gd.glother4='M' THEN glamt ELSE 0.0 END) [Amt. Credit Valas], gldtloid [Urutan], ISNULL(gd.glother3, '') [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cmpcode=cb.cmpcode AND gm.cashbankoid=cb.cashbankoid INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trngldtl gd ON gd.cmpcode=cb2.cmpcode AND gd.glother1='QL_trncashbankmst ' + CAST(cb2.cashbankoid AS VARCHAR(10)) AND ISNULL(gd.glother3, '')<>'' AND ISNULL(gd.glother4, '')<>'' INNER JOIN QL_mstacctg acc ON gd.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s1 ON s1.suppoid=(CASE WHEN cb2.cashbankgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN cb2.refsuppoid WHEN cb2.cashbankgroup IN ('DPAP') THEN cb2.personoid WHEN cb2.cashbankgroup LIKE 'AP%' THEN cb2.personoid ELSE 0 END) LEFT JOIN QL_mstcust s2 ON s2.custoid=(CASE WHEN cb2.cashbankgroup IN ('RECEIPT GIRO') THEN cb2.refsuppoid WHEN cb2.cashbankgroup IN ('DPAR') THEN cb2.personoid WHEN cb2.cashbankgroup LIKE 'AR%' THEN cb2.personoid ELSE 0 END) INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cb.cashbanktype IN ('BBK', 'BBM') AND cb.cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT cb.cmpcode, cb.acctgoid, cb.cashbankduedate [Tanggal], cb.cashbankno [Nomor Bukti], cb.cashbanknote [Keterangan], ISNULL(suppcode, ISNULL(custcode, '')) [Kode Supp/Cust], ISNULL(suppname, ISNULL(custname, '')) [Nama Supp/Cust], acc2.acctgcode [Kode Perk. Transaksi], acc2.acctgdesc [Ket. Perk. Transaksi], acc.acctgcode [Kode Perk. Lawan], acc.acctgdesc [Ket. Perk. Lawan], currcode [Mata Uang], (CASE WHEN gd.glother4='K' THEN glamtidr ELSE 0.0 END) [Amt. Debet IDR], (CASE WHEN gd.glother4='M' THEN glamtidr ELSE 0.0 END) [Amt. Credit IDR], (CASE WHEN gd.glother4='K' THEN glamt ELSE 0.0 END) [Amt. Debet Valas], (CASE WHEN gd.glother4='M' THEN glamt ELSE 0.0 END) [Amt. Credit Valas], gldtloid [Urutan], ISNULL(gd.glother3, '') [Detail Note], cb.cashbankrefno [No Ref], '' [No Trans. Ref] FROM QL_trncashbankmst cb INNER JOIN QL_trnpayapgiro gm ON gm.cmpcode=cb.cmpcode AND gm.cashbankoid=cb.cashbankoid INNER JOIN QL_trncashbankmst cb2 ON cb2.cmpcode=gm.cmpcode AND cb2.cashbankoid=gm.refoid INNER JOIN QL_trngldtl_hist gd ON gd.cmpcode=cb2.cmpcode AND gd.glother1='QL_trncashbankmst ' + CAST(cb2.cashbankoid AS VARCHAR(10)) AND ISNULL(gd.glother3, '')<>'' AND ISNULL(gd.glother4, '')<>'' INNER JOIN QL_mstacctg acc ON gd.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s1 ON s1.suppoid=(CASE WHEN cb2.cashbankgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN cb2.refsuppoid WHEN cb2.cashbankgroup IN ('DPAP') THEN cb2.personoid WHEN cb2.cashbankgroup LIKE 'AP%' THEN cb2.personoid ELSE 0 END) LEFT JOIN QL_mstcust s2 ON s2.custoid=(CASE WHEN cb2.cashbankgroup IN ('RECEIPT GIRO') THEN cb2.refsuppoid WHEN cb2.cashbankgroup IN ('DPAR') THEN cb2.personoid WHEN cb2.cashbankgroup LIKE 'AR%' THEN cb2.personoid ELSE 0 END) INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cb.cashbanktype IN ('BBK', 'BBM') AND cb.cashbankstatus='POST' AND cb.cmpcode=@cmpcode AND cb.acctgoid=@acctgoid " +
                "UNION ALL SELECT gm.cmpcode, (CASE WHEN gd1.acctgoid=@acctgoid THEN gd1.acctgoid ELSE gd2.acctgoid END) acctgoid, gldate [Tanggal], '' [Nomor Bukti], gm.glnote [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], (CASE WHEN gd1.acctgoid=@acctgoid THEN a1.acctgcode ELSE a2.acctgcode END) [Kode Perk. Transaksi], (CASE WHEN gd1.acctgoid=@acctgoid THEN a1.acctgdesc ELSE a2.acctgdesc END) [Ket. Perk. Transaksi], (CASE WHEN gd1.acctgoid=@acctgoid THEN a2.acctgcode ELSE a1.acctgcode END) [Kode Perk. Lawan], (CASE WHEN gd1.acctgoid=@acctgoid THEN a2.acctgdesc ELSE a1.acctgdesc END) [Ket. Perk. Lawan], ISNULL((SELECT currcode FROM QL_mstcurr c WHERE c.curroid=(CASE WHEN gd1.acctgoid=@acctgoid THEN a1.curroid ELSE a2.curroid END)), '') [Mata Uang], gd1.glamtidr [Amt. Debet IDR], gd2.glamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], 0.0 [Amt. Credit Valas], (CASE WHEN gd1.acctgoid=@acctgoid THEN gd2.gldtloid ELSE gd1.gldtloid END) [Urutan], (CASE WHEN gd1.acctgoid=@acctgoid THEN (gd2.noref + ' - ' + gd2.glnote) ELSE (gd1.noref + ' - ' + gd1.glnote) END) [Detail Note], (CASE WHEN gd1.acctgoid=@acctgoid THEN gd1.noref ELSE gd2.noref END) [No Ref], '' [No Trans. Ref] FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd1 ON gd1.cmpcode=gm.cmpcode AND gd1.glmstoid=gm.glmstoid AND gd1.gldbcr='D' INNER JOIN QL_trngldtl gd2 ON gd2.cmpcode=gm.cmpcode AND gd2.glmstoid=gm.glmstoid AND gd2.gldbcr='C' INNER JOIN QL_mstacctg a1 ON a1.acctgoid=gd1.acctgoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gd2.acctgoid WHERE gm.glnote='Monthly Closing Auto Posting' AND gm.cmpcode=@cmpcode AND @acctgoid IN (gd1.acctgoid, gd2.acctgoid) AND gd1.glamtidr<>0 AND gd2.glamtidr<>0 " +
                "UNION ALL SELECT gm.cmpcode, (CASE WHEN gd1.acctgoid=@acctgoid THEN gd1.acctgoid ELSE gd2.acctgoid END) acctgoid, gldate [Tanggal], '' [Nomor Bukti], gm.glnote [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], (CASE WHEN gd1.acctgoid=@acctgoid THEN a1.acctgcode ELSE a2.acctgcode END) [Kode Perk. Transaksi], (CASE WHEN gd1.acctgoid=@acctgoid THEN a1.acctgdesc ELSE a2.acctgdesc END) [Ket. Perk. Transaksi], (CASE WHEN gd1.acctgoid=@acctgoid THEN a2.acctgcode ELSE a1.acctgcode END) [Kode Perk. Lawan], (CASE WHEN gd1.acctgoid=@acctgoid THEN a2.acctgdesc ELSE a1.acctgdesc END) [Ket. Perk. Lawan], ISNULL((SELECT currcode FROM QL_mstcurr c WHERE c.curroid=(CASE WHEN gd1.acctgoid=@acctgoid THEN a1.curroid ELSE a2.curroid END)), '') [Mata Uang], (CASE WHEN gd1.acctgoid=@acctgoid THEN 0.0 ELSE gd1.glamtidr END) [Amt. Debet IDR], (CASE WHEN gd1.acctgoid=@acctgoid THEN gd2.glamtidr ELSE 0.0 END) [Amt. Credit IDR], 0.0 [Amt. Debet Valas], 0.0 [Amt. Credit Valas], (CASE WHEN gd1.acctgoid=@acctgoid THEN gd2.gldtloid ELSE gd1.gldtloid END) [Urutan], (CASE WHEN gd1.acctgoid=@acctgoid THEN (gd2.noref + ' - ' + gd2.glnote) ELSE (gd1.noref + ' - ' + gd1.glnote) END) [Detail Note], (CASE WHEN gd1.acctgoid=@acctgoid THEN gd1.noref ELSE gd2.noref END) [No Ref], '' [No Trans. Ref] FROM QL_trnglmst_hist gm INNER JOIN QL_trngldtl_hist gd1 ON gd1.cmpcode=gm.cmpcode AND gd1.glmstoid=gm.glmstoid AND gd1.gldbcr='D' INNER JOIN QL_trngldtl_hist gd2 ON gd2.cmpcode=gm.cmpcode AND gd2.glmstoid=gm.glmstoid AND gd2.gldbcr='C' INNER JOIN QL_mstacctg a1 ON a1.acctgoid=gd1.acctgoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gd2.acctgoid WHERE gm.glnote='Monthly Closing Auto Posting' AND gm.cmpcode=@cmpcode AND @acctgoid IN (gd1.acctgoid, gd2.acctgoid) AND gd1.glamtidr<>0 AND gd2.glamtidr<>0" +
                ") AS tblshow " + sWhere + " " +
                "ORDER BY cmpcode, acctgoid, [Nomor Bukti], [Tanggal], [Urutan]";

            DataTable dtRpt = null;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (DDLType == "Summary")
            {
                sWhere = "WHERE CompCode = '" + CompnyCode + "' AND Oid = " + DDLAccount;
                var sAnd = "";
                var iAcctgOid = Convert.ToInt32(DDLAccount);
                sWhere += " AND Tanggal BETWEEN '" + StartDate + "' AND '" + EndDate + "'";
                sAnd += " AND gm.gldate<'" + StartDate + "' ";

                rptparam.Add("sCompnyName", DDLBusinessUnit_Text);
                rptparam.Add("sPeriod", sPeriod);
                rptparam.Add("sWhere", sWhere);
                rptparam.Add("iAcctgOid", iAcctgOid);
                rptparam.Add("sAnd", sAnd);
                rptparam.Add("sCurrency", DDLCurrency);
                rptparam.Add("cmpcode", CompnyCode);
            }
            else
            {
                dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
                rptparam.Add("sPeriod", sPeriod);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("PrintUserName", Session["UserName"].ToString());
            }
            if (ReportType == "")
            {
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptsource"] = dtRpt;
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (DDLType == "Summary")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    report.SetDataSource(dtRpt);
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary")
                {
                    ClassProcedure.SetDBLogonForReport(report);
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }

}