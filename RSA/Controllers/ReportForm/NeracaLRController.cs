﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;
using RSA.Models.DB;
using static RSA.Controllers.ClassFunction;
using System.Data;
using RSA.Controllers;

namespace SENTRIS_ASP.Controllers
{
    public class NeracaLRController : Controller
    {
        // GET: NeracaLR
        private QL_RSAEntities db;
        private ReportModels.SQLQuery sSql = new ReportModels.SQLQuery();

        public NeracaLRController()
        {
            db = new QL_RSAEntities();
            db.Database.CommandTimeout = 0;
        }
        public class sFilter
        {
            public string ddltype { get; set; }
            public int periodmonth { get; set; }
            public int periodyear { get; set; }
            public int periodyear1 { get; set; }
            public string month_name { get; set; }
            public string tipe { get; set; }
            public string sum_dtl { get; set; }
        }

        #region LABA RUGI REPORT

        public ActionResult LabaRugiReport()
        {
            ViewBag.Title = "Laba Rugi";
            ViewBag.ddltype = new SelectList(new Dictionary<string, string>() { 
                ["SumBulanan"] = "Summary Bulanan", 
                ["DtlBulanan"] = "Detail Bulanan", 
                ["SumTahunan"] = "Summary Tahunan", 
                ["DtlTahunan"] = "Detail Tahunan" 
            }, "Key", "Value");
            ViewBag.periodmonth = new SelectList(GetBulan(), "Value", "Text", GetServerTime().ToString("MM"));
            ViewBag.periodyear = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            ViewBag.periodyear1 = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            return View();
        }

        public static string GetQueryLR(string sSql = "", string sFilter = "", string qType = "") {            
            if (sSql == "gl_amt") {
                sSql = $"SELECT rd.acctgoid coa_id, Month(r.gldate) gl_month, Year(r.gldate) gl_year, SUM((case rd.gldbcr when 'D' then rd.glamtidr * 1 else rd.glamtidr * (-1) end)) * (-1) amt, a.acctggrp1 config_d_key FROM QL_trnglmst r INNER JOIN QL_trngldtl rd ON rd.glmstoid=r.glmstoid Inner join QL_mstacctg a ON a.acctgoid=rd.acctgoid WHERE a.acctgoid NOT IN ( SELECT CAST(x.acctggrp3 AS INTEGER) FROM QL_mstacctg x) AND a.acctggrp1 IN ('PENDAPATAN USAHA','HARGA POKOK','BIAYA USAHA','BIAYA LAIN','PENDAPATAN LAIN', 'PENDAPATAN DAN BIAYA LAIN-LAIN') AND Month(r.gldate) = @gl_month AND YEAR(r.gldate) = @gl_year {sFilter} Group BY rd.acctgoid, a.acctgcode, Month(r.gldate), YEAR(r.gldate), a.acctggrp1";
            }                
            else if (sSql == "coa")
                sSql = $"Select a.acctgoid coa_id, a.acctgcode coa_code, CONCAT (a.acctgcode,' ',a.acctgdesc) coa_name, a.acctggrp1 coa_type, ISNULL((SELECT x.acctgdesc FROM QL_mstacctg x WHERE x.acctgoid = CAST(a.acctggrp3 AS INTEGER)), '') group2, case when a.acctggrp1 in ('PENDAPATAN USAHA','HARGA POKOK') then 'LABA KOTOR' else '' end grp3, ISNULL((SELECT x1.acctgdesc FROM QL_mstacctg x1 WHERE x1.acctgoid = (SELECT CAST(a.acctggrp3 AS INTEGER) FROM QL_mstacctg x WHERE x.acctgoid = CAST(a.acctggrp3 AS INTEGER))), '') group1, 'RSA' company_code, Upper('pt rukun semangat abadi') company_name, 0.0 AS amt, 0.0 AS amt1, 0.0 AS amt2, 0.0 AS amt3, 0.0 AS amt4, 0.0 AS amt5, 0.0 AS amt6, 0.0 AS amt7, 0.0 AS amt8, 0.0 AS amt9, 0.0 AS amt10, 0.0 AS amt11, 0.0 AS amt12, 0.0 penjualan, 0.0 penjualan1, 0.0 penjualan2, 0.0 penjualan3, 0.0 penjualan4, 0.0 penjualan5, 0.0 penjualan6, 0.0 penjualan7, 0.0 penjualan8, 0.0 penjualan9, 0.0 penjualan10, 0.0 penjualan11, 0.0 penjualan12, 0.0 total_amt, 0.0 total_persen from QL_mstacctg a WHERE a.acctgoid NOT IN ( SELECT CAST(x.acctggrp3 AS INTEGER) FROM QL_mstacctg x ) And a.acctggrp1 IN ('PENDAPATAN USAHA','HARGA POKOK','BIAYA USAHA','BIAYA LAIN','PENDAPATAN LAIN', 'PENDAPATAN DAN BIAYA LAIN-LAIN') order by acctgoid";
            return sSql;
        }

        [HttpPost]
        public ActionResult ViewReportLRYear(sFilter param, string rpttype)
        {
            var rpt_name = $"rptLabaRugiDtlTahunan"; var sDeclare = ""; decimal penjualan = 0m;
            int iMonth = GetBulan().ToList().Count(); var sQuery = GetQueryLR("coa");
            var dt = new SharedConnection().getDataTable(sQuery, "data");
            foreach (DataRow item in dt.Rows)
            {
                for (int i = 1; i <= iMonth; i++)
                {
                    sDeclare = $"Declare @gl_month int; Declare @gl_year int; Set @gl_month = {i}; Set @gl_year = {param.periodyear1};";
                    sQuery = $"{sDeclare} select * from ({GetQueryLR("gl_amt", $"AND a.acctgoid={item[$"coa_id"]}")}) r where 1=1";
                    var dt_rpt = new SharedConnection().getDataTable(sQuery, "data");
                    for (int x = 0; x < dt_rpt.Rows.Count; x++) item[$"amt{i}"] = dt_rpt.Rows[x]["amt"].ToString().ToDecimal();
                }
            }
            dt.AcceptChanges();

            foreach (DataRow items in dt.Rows)
            {
                var total_amt = 0m;
                for (int xx = 1; xx <= GetBulan().ToList().Count(); xx++)
                {    
                    sDeclare = $"Declare @gl_month int; Declare @gl_year int; Set @gl_month = {xx}; Set @gl_year = {param.periodyear1};";
                    sQuery = $"{sDeclare} select Isnull(SUM(amt), 0.0) from ({GetQueryLR("gl_amt", "")}) r where r.config_d_key='PENDAPATAN USAHA'";
                    penjualan = db.Database.SqlQuery<decimal>(sQuery)?.FirstOrDefault() ?? 0m;
                    if (items[$"amt{xx}"].ToString().ToDecimal() != 0m && penjualan != 0m)
                        items[$"penjualan{xx}"] = (items[$"amt{xx}"].ToString().ToDecimal() / penjualan) * 100m;
                    else items[$"penjualan{xx}"] = 0m;
                    total_amt += items[$"amt{xx}"].ToString().ToDecimal();
                };
                items[$"total_amt"] = total_amt;
            }
            dt.AcceptChanges();
            
            var req = new ReportModels.PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_name = rpt_name,
                file_name = $"LAPORAN_LABARUGI_TAHUNAN_{param.month_name.ToUpper()}_{param.periodyear.ToString()}",
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt;
            req.rpt_param.Add("periode", $"{param.month_name.ToUpper()} {param.periodyear.ToString()}");
            req.rpt_param.Add("report_type", $"{param.ddltype}");
            req.rpt_param.Add("iTahun", $"{param.periodyear1}");
            return setPrintOut(req);
        }

        [HttpPost]
        public ActionResult ViewReportLRMonth(sFilter param, string rpttype)
        {
            var rpt_name = $"rptLabaRugiDtlBulanan"; var sDeclare = ""; decimal penjualan = 0m; 
            string start_date = new DateTime(param.periodyear, param.periodmonth, 1).ToString();
            string end_date = new DateTime(param.periodyear, param.periodmonth, DateTime.DaysInMonth(param.periodyear, param.periodmonth)).ToString();
            var sQuery = GetQueryLR("coa");
            var dt = new SharedConnection().getDataTable(sQuery, "data");
            foreach (DataRow item in dt.Rows)
            {
                sDeclare = $"Declare @gl_month int; Declare @gl_year int; Set @gl_month = {param.periodmonth}; Set @gl_year = {param.periodyear};";
                sQuery = $"{sDeclare} select * from ({GetQueryLR("gl_amt", $"AND a.acctgoid={item[$"coa_id"]}", "")}) r where 1=1";
                var dt_rpt = new SharedConnection().getDataTable(sQuery, "data");
                for (int x = 0; x < dt_rpt.Rows.Count; x++) item["amt1"] = dt_rpt.Rows[x]["amt"].ToString().ToDecimal();
            }
            dt.AcceptChanges();

            foreach (DataRow items in dt.Rows)
            {
                sDeclare = $"Declare @gl_month int; Declare @gl_year int; Set @gl_month = {param.periodmonth}; Set @gl_year = {param.periodyear};";
                sQuery = $"{sDeclare} select Isnull(SUM(amt), 0.0) from ({GetQueryLR("gl_amt", "", "")}) r where r.config_d_key='PENDAPATAN USAHA'";
                penjualan = db.Database.SqlQuery<decimal>(sQuery)?.FirstOrDefault() ?? 0m;
                if (items[$"amt1"].ToString().ToDecimal() != 0m && penjualan != 0m)
                    items[$"penjualan1"] = (items[$"amt1"].ToString().ToDecimal() / penjualan) * 100m;
                else items[$"penjualan1"] = 0m;
            }
            dt.AcceptChanges();

            var req = new ReportModels.PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_sub_src = new List<DataTable>(),
                rpt_name = rpt_name,
                file_name = $"LAPORAN_LABARUGI_{param.month_name.ToUpper()}_{param.periodyear.ToString()}",
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt;
            req.rpt_param.Add("periode", $"{param.month_name.ToUpper()} {param.periodyear.ToString()}");
            req.rpt_param.Add("report_type", $"{param.ddltype}");
            req.rpt_param.Add("nama_bulan", $"{param.month_name.ToUpper()}");
            return setPrintOut(req);
        }
        #endregion

        public static string raw_query()
        {
            return $"select isnull((select sum(amt) from (select ((case gldbcr when 'D' then glamtidr * 1 else glamtidr * (-1) end) * (case c.acctggrp1 when 'AKTIVA' then 1 else -1 end)) amt from QL_trnglmst h inner join QL_trngldtl d on d.glmstoid=h.glmstoid inner join QL_mstacctg c on c.acctgoid=d.acctgoid where 1=1) y), 0.0)";
        }

        #region Neraca Report

        public ActionResult NeracaReport()
        {
            ViewBag.Title = "Neraca";
            ViewBag.periodmonth = new SelectList(GetBulan(), "Value", "Text", GetServerTime().ToString("MM"));
            ViewBag.periodyear = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            ViewBag.tipe = new SelectList(new Dictionary<string, string>()
            {
                ["B"] = "BULANAN",
                ["T"] = "TAHUNAN",
            }, "Key", "Value");
            ViewBag.sum_dtl = new SelectList(new Dictionary<string, string>()
            {
                ["Sum"] = "SUMMARY",
                ["Dtl"] = "DETAIL",
            }, "Key", "Value");

            return View();
        }

        [HttpPost]
        public ActionResult viewNeracaReport(sFilter param, string rpttype)
        {
            var rpt_name = $"rptNeraca{(param.tipe == "T" ? "Tahunan" : "")}{param.sum_dtl}{(rpttype == $"XLS" ? $"XLS" : $"XLS")}";
            DateTime end_date = new DateTime(param.periodyear, param.periodmonth, DateTime.DaysInMonth(param.periodyear, param.periodmonth));
            DateTime awal_tahun = new DateTime(param.periodyear, 1, DateTime.DaysInMonth(param.periodyear, 1));
            DateTime tahun_lalu = new DateTime(param.periodyear - 1, 12, DateTime.DaysInMonth(param.periodyear - 1, 12));
            var gt_aktiva = 0m;var gt_pasiva = 0m;

            var query = $"select t2.acctggrp1 coa_type , t1.acctgcode grp1code , t1.acctgdesc group1 , isnull((select y.acctgcode from QL_mstacctg y where y.acctgoid=CAST(t2.acctggrp3 AS INTEGER)), '') grp2code, isnull((select y.acctgdesc from QL_mstacctg y where y.acctgoid=CAST(t2.acctggrp3 AS INTEGER)), '') group2, t2.acctgoid coa_id, t2.acctgcode coa_code , t2.acctgdesc coa_name , 0.0 amt, 0.0 amt_1, 0.0 amt_2, 0.0 amt_3, 0.0 amt_4, 0.0 amt_5, 0.0 amt_6, 0.0 amt_7, 0.0 amt_8, 0.0 amt_9, 0.0 amt_10, 0.0 amt_11, 0.0 amt_12, case when t2.acctggrp1 in ('AKTIVA') then 'TOTAL AKTIVA' else 'TOTAL PASSIVA' end sub_total from (select * from QL_mstacctg c where len(acctgcode)=2) t1 inner join QL_mstacctg t2 on t2.acctgcode like concat(t1.acctgcode, '%') and t2.acctggrp2 > 1 where t2.acctggrp1 in ('AKTIVA','KEWAJIBAN','EKUITAS') order by grp1code, grp2code, t2.acctgcode";
            var t1 = new SharedConnection().getDataTable(query, "data");
            if (param.tipe == "B")
            {
                for (int i = 0; i < t1.Rows.Count; i++)
                {
                    var coa_id = t1.Rows[i]["coa_id"].ToString().toInteger(); var filter = $"format(gldate, 'yyyyMM')<='{end_date:yyyyMM}' and d.acctgoid={coa_id}";
                    var amt = db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    if (t1.Rows[i]["coa_code"].ToString() == "31202") //LABA TAHUN BERJALAN
                    {
                        filter = $"format(gldate, 'yyyy')='{end_date:yyyy}' and format(gldate, 'yyyyMM')<'{end_date:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    if (t1.Rows[i]["coa_code"].ToString() == "31201") //LABA DI TAHAN
                    {
                        filter = $"format(gldate, 'yyyy')<'{end_date:yyyy}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    if (t1.Rows[i]["coa_code"].ToString() == "31203") //LABA RUGI BULAN INI
                    {
                        filter = $"format(gldate, 'yyyyMM')='{end_date:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    t1.Rows[i]["amt"] = amt;
                }
            }
            else
            {
                var periode = awal_tahun; var loop = 12;
                if (periode.Year == GetServerTime().Year) loop = GetServerTime().Month;
                for (int i = 0; i < t1.Rows.Count; i++)
                {
                    var coa_id = t1.Rows[i]["coa_id"].ToString().toInteger();
                    var filter = $"format(gldate, 'yyyyMM')<='{tahun_lalu:yyyyMM}' and d.acctgoid={coa_id}";
                    var amt = db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    if (t1.Rows[i]["coa_code"].ToString() == "31202") //LABA TAHUN BERJALAN
                    {
                        filter = $"format(gldate, 'yyyy')='{tahun_lalu:yyyy}' and format(gldate, 'yyyyMM')<'{tahun_lalu:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    if (t1.Rows[i]["coa_code"].ToString() == "31201") //LABA DI TAHAN
                    {
                        filter = $"format(gldate, 'yyyy')<'{tahun_lalu:yyyy}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    if (t1.Rows[i]["coa_code"].ToString() == "31203") //LABA RUGI BULAN INI
                    {
                        filter = $"format(gldate, 'yyyyMM')='{tahun_lalu:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                        amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                    }
                    t1.Rows[i]["amt"] = amt;

                    for (int t = 1; t <= loop; t++)
                    {
                        filter = $"format(gldate, 'yyyyMM')<='{periode:yyyyMM}' and d.acctgoid={coa_id}";
                        amt = db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                        if (t1.Rows[i]["coa_code"].ToString() == "31202") //LABA TAHUN BERJALAN
                        {
                            filter = $"format(gldate, 'yyyy')='{periode:yyyy}' and format(gldate, 'yyyyMM')<'{periode:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                            amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                        }
                        if (t1.Rows[i]["coa_code"].ToString() == "31201") //LABA DI TAHAN
                        {
                            filter = $"format(gldate, 'yyyy')<'{periode:yyyy}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                            amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                        }
                        if (t1.Rows[i]["coa_code"].ToString() == "31203") //LABA RUGI BULAN INI
                        {
                            filter = $"format(gldate, 'yyyyMM')='{periode:yyyyMM}' and acctggrp1 not in ('AKTIVA','KEWAJIBAN','EKUITAS')";
                            amt += db.Database.SqlQuery<decimal>(raw_query().Replace("1=1", filter)).FirstOrDefault();
                        }
                        t1.Rows[i][$"amt_{t}"] = amt;
                        periode = awal_tahun.AddMonths(t);
                    }
                    periode = awal_tahun;
                }
            }

            t1.AcceptChanges();
            var new_dt_view = t1.DefaultView;

            if (param.tipe == "T") new_dt_view.RowFilter = "";
            else
            {
                new_dt_view.RowFilter = "coa_type in ('AKTIVA')";
            }
            var dt_rpt = new_dt_view.ToTable();
            var dt_rpt2 = new_dt_view.ToTable();
            new_dt_view.RowFilter = "";

            new_dt_view.RowFilter = "coa_type not in ('AKTIVA')";
            var dt_rpt3 = new_dt_view.ToTable();
            new_dt_view.RowFilter = "";

            for (int i = 0; i < dt_rpt2.Rows.Count; i++)
            {
                gt_aktiva += getObjVal(dt_rpt2.Rows[i]["amt"]).ToDecimal();
            }
            for (int i = 0; i < dt_rpt3.Rows.Count; i++)
            {
                gt_pasiva += getObjVal(dt_rpt3.Rows[i]["amt"]).ToDecimal();
            }

            decimal total_aktiva = dt_rpt2.Compute("SUM(amt)", "").ToString().ToDecimal();
            decimal total_pasiva = dt_rpt3.Compute("SUM(amt)", "").ToString().ToDecimal();
            var company_name = db.QL_mstdivision.FirstOrDefault()?.divname;
            var req = new ReportModels.PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_sub_src = new List<DataTable>(),
                rpt_name = rpt_name,
                file_name = $"LAPORAN_NERACA_{param.month_name.ToUpper()}_{param.periodyear.ToString()}",
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt_rpt;
            req.rpt_sub_src = new List<DataTable> { dt_rpt2, dt_rpt3 };
            if (param.tipe == "T") req.rpt_sub_src = new List<DataTable> { dt_rpt2 };
            else
            {
                req.rpt_sub_src = new List<DataTable> { dt_rpt2, dt_rpt3 };
            }
            req.rpt_param.Add("company_name", company_name);
            if (param.tipe == "T") req.rpt_param.Add("periode", $"TAHUN {param.periodyear.ToString()}");
            else
            {
                req.rpt_param.Add("periode", $"{param.month_name.ToUpper()} {param.periodyear.ToString()}");
            }
            req.rpt_param.Add("total_aktiva", $"{gt_aktiva}");
            req.rpt_param.Add("total_pasiva", $"{gt_pasiva}");
            return setPrintOut(req);
        }

        #endregion
    }
}
