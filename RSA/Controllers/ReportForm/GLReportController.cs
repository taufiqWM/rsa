﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.ReportForm
{
    public class GLReportController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = "RCA";
        private string sSql = "";

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            if (DDLBusinessUnit.Count() > 0)
            {
                // Isi DropDownList Department
                sSql = "SELECT groupoid, groupcode + ' - ' + groupdesc groupcode FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
                var DDLDivision = new SelectList(db.Database.SqlQuery<ReportModels.DDLDivisionModel>(sSql).ToList(), "groupoid", "groupcode");
                ViewBag.DDLDivision = DDLDivision;
            }

        }

        [HttpPost]
        public ActionResult InitDDLDivision(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLDivisionModel> tbl = new List<ReportModels.DDLDivisionModel>();
            try
            {
                sSql = "SELECT groupoid, groupcode + ' - ' + groupdesc groupcode FROM QL_mstdeptgroup WHERE cmpcode='" + DDLBusinessUnit + "' AND activeflag='ACTIVE'";
                tbl = db.Database.SqlQuery<ReportModels.DDLDivisionModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAccountData()
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 as seq, acctgcode as Code,acctgdesc as Name FROM QL_mstacctg WHERE acctgoid not in (select distinct a.acctggrp3 From QL_mstacctg a where a.acctggrp3 is not null ) ORDER BY acctgcode ";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAccount");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetAcctgOidByCode(string AcctgCode)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(acctgoid AS VARCHAR(10)) FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgcode IN ('" + AcctgCode.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Goods";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLBusinessUnit_Text, string DDLDivision, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string TextNoRef, string ReportType, string DDLType, string TextAccount)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sWhere = "";
            var sWhere_Hist = "";
            var sWhereAwal = "";
            var sWhereTrans = "";

            var rptfile = ""; var rptname = "";
            if (DDLType == "0")
            {
                if (ReportType == "XLS")
                    rptfile = "crGLSummaryXls.rpt";
                else
                    rptfile = "crGLSummary.rpt"; rptname = "GLSUMMARYREPORT";
            }
            else if (DDLType == "1")
            {
                if (ReportType == "XLS")
                    rptfile = "crGLDetailXls.rpt";
                else
                    rptfile = "crGLDetail.rpt"; rptname = "GLDETAILREPORT";
            }
            else if (DDLType == "2")
            {
                if (string.IsNullOrEmpty(TextNoRef))
                {
                    sWhere = " WHERE d.cmpcode = '" + CompnyCode + "' AND m.gldate BETWEEN '" + StartDate + "' AND '" + EndDate + "' AND m.glflag='POST' AND m.glmstoid>0 ";
                }
                else
                {
                    sWhere = " WHERE d.cmpcode = '" + CompnyCode + "' AND m.glflag='POST' AND m.glmstoid>0 ";
                    sWhere += " AND d.noref IN ('" + TextNoRef.Replace(";", "','") + "')";
                }
                if (!string.IsNullOrEmpty(DDLDivision))
                {
                    sWhere += " AND d.groupoid=" + DDLDivision;
                }
                sWhere_Hist = sWhere;
                if (!string.IsNullOrEmpty(TextAccount))
                {
                    sWhere += " AND m.glmstoid IN (SELECT a.glmstoid FROM QL_trnglmst a INNER JOIN QL_trngldtl b ON a.glmstoid = b.glmstoid AND a.cmpcode = b.cmpcode INNER JOIN QL_mstacctg c ON c.acctgoid=b.acctgoid WHERE a.cmpcode = d.cmpcode AND a.gldate BETWEEN '" + StartDate + "' AND '" + EndDate + "'  AND b.acctgoid IN (" + GetAcctgOidByCode(TextAccount) + "))";
                    //sWhere_Hist += " AND m.glmstoid IN (SELECT a.glmstoid FROM QL_trnglmst_hist a INNER JOIN QL_trngldtl__hist b ON a.glmstoid = b.glmstoid AND a.cmpcode = b.cmpcode INNER JOIN QL_mstacctg c ON c.acctgoid=a.acctgoid WHERE a.cmpcode = d.cmpcode AND a.gldate BETWEEN '" + StartDate + "' AND '" + EndDate + "'  AND acctgoid IN (" + GetAcctgOidByCode(TextAccount) + "))";

                }
                if (ReportType == "XLS")
                    rptfile = "crGenJournalXls.rpt";
                else
                    rptfile = "crGenJournal.rpt"; rptname = "GLJOURNALREPORT";
            }


            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (DDLType == "0" || DDLType == "1")
            {
                if (!string.IsNullOrEmpty(DDLDivision))
                {
                    sWhereAwal = " WHERE groupoid=" + DDLDivision;
                    sWhereTrans = " AND d.groupoid=" + DDLDivision;
                }
                else
                {
                    sWhereAwal = " WHERE groupoid<>-1000";
                    sWhereTrans = " AND d.groupoid<>-1000";
                }

                if (!string.IsNullOrEmpty(TextAccount))
                {
                    sWhereAwal += " AND acctgoid IN (" + GetAcctgOidByCode(TextAccount) + ")";
                    sWhereTrans += " AND a.acctgoid IN (" + GetAcctgOidByCode(TextAccount) + ")";
                }

                System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
                var awal = DateTime.ParseExact(StartDate, "MM/dd/yyyy", AppsCultureInfo);
                var akhir = DateTime.ParseExact(EndDate, "MM/dd/yyyy", AppsCultureInfo);
                rptparam.Add("awal", awal);
                rptparam.Add("akhir", akhir);
                rptparam.Add("periodacctg", ClassFunction.GetDateToPeriodAcctg(Convert.ToDateTime(StartDate)));
                rptparam.Add("cmpcode", CompnyCode);
                rptparam.Add("swhereawal", sWhereAwal);
                rptparam.Add("swheretrans", sWhereTrans);
                rptparam.Add("currency", DDLCurrency);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);
            }
            else if (DDLType == "2")
            {
                var periode = "-";
                if (string.IsNullOrEmpty(TextNoRef))
                {
                    periode = "" + ClassFunction.toDate(StartDate).ToString() + " - " + ClassFunction.toDate(EndDate).ToString() + "";
                }
                rptparam.Add("periode", periode);
                rptparam.Add("sWhere", sWhere);
                rptparam.Add("sCurrency", DDLCurrency);
                rptparam.Add("sCompany", CompnyName);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);
            }

            if (ReportType == "")
            {
                //this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                //report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                ClassProcedure.SetDBLogonForReport(report);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}