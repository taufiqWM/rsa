﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.Models.ViewModel;
using RSA.Models.DB;
using System.ComponentModel;
using System.Data;

namespace RSA.Controllers
{
    public class ClassRate
    {
        public int GetRateDailyOid { get; set; } = 0;
        public int GetRateMonthlyOid { get; set; } = 0;
        public decimal GetRateDailyIDRValue { get; set; } = 0;
        public decimal GetRateDailyUSDValue { get; set; } = 0;
        public decimal GetRateMonthlyIDRValue { get; set; } = 0;
        public decimal GetRateMonthlyUSDValue { get; set; } = 0;
        public string GetRateDailyLastError { get; set; } = "";
        public string GetRateMonthlyLastError { get; set; } = "";
        public int iCurrOid { get; set; } = 0;
        public string sCurrCode { get; set; } = "";
        public string sSql = "";

        public class DailyRate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            //public DateTime ratedate { get; set; }
        }

        public class MonthlyRate
        {
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        public void SetRateDailyOid(int iOid)
        {
            GetRateDailyOid = iOid;
        }

        public void SetRateMonthlyOid(int iOid)
        {
            GetRateMonthlyOid = iOid;
        }

        public void SetRateDailyIDRValue(decimal dValue)
        {
            GetRateDailyIDRValue = dValue;
        }

        public void SetRateMonthlyIDRValue(decimal dValue)
        {
            GetRateMonthlyIDRValue = dValue;
        }

        public void SetRateDailyUSDValue(decimal dValue)
        {
            GetRateDailyUSDValue = dValue;
        }

        public void SetRateMonthlyUSDValue(decimal dValue)
        {
            GetRateMonthlyUSDValue = dValue;
        }

        public void SetRateValue(int iCurr, string sDate)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_RSAEntities db = new QL_RSAEntities();
            iCurrOid = iCurr;
            sSql = "SELECT currcode FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND curroid=" + iCurrOid;
            sCurrCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ////Get Daily Rate Data
            //sSql = "SELECT TOP 1 rateoid, CAST (rateres1 AS REAL) AS rateidrvalue, CAST (rateres2 AS REAL) AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" + CompnyCode + "' AND curroid=" + iCurrOid + " AND '" + sDate + " 00:00:00'>=ratedate AND '" + sDate + " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC";
            //List<DailyRate> TblDailyRate = db.Database.SqlQuery<DailyRate>(sSql).ToList();
            //if (TblDailyRate != null)
            //{
            //    if (TblDailyRate.Count() > 0)
            //    {
            //        for (var i = 0; i < TblDailyRate.Count(); i++)
            //        {
            //            GetRateDailyOid = TblDailyRate[i].rateoid;
            //            GetRateDailyIDRValue = Convert.ToDecimal(TblDailyRate[i].rateidrvalue);
            //            GetRateDailyUSDValue = Convert.ToDecimal(TblDailyRate[i].rateusdvalue);
            //        }
            //    }else
            //    {
            //        GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
            //        GetRateDailyOid = 0;
            //        GetRateDailyIDRValue = 0;
            //        GetRateDailyUSDValue = 0;
            //    }
            //}else
            //{
            //    GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
            //    GetRateDailyOid = 0;
            //    GetRateDailyIDRValue = 0;
            //    GetRateDailyUSDValue = 0;
            //}

            if (iCurrOid != 1)
            {
                //Get Monthly Rate Data
                sSql = "SELECT TOP 1 rateoid rate2oid, rateidrvalue rate2idrvalue, rateusdvalue rate2usdvalue FROM QL_mstrate WHERE cmpcode='" + CompnyCode + "' AND curroid=" + iCurrOid + " AND '" + sDate + " 00:00:00'>=ratedate AND '" + sDate + " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY rateoid DESC";
                List<MonthlyRate> TblMonthlyRate = db.Database.SqlQuery<MonthlyRate>(sSql).ToList();
                if (TblMonthlyRate != null)
                {
                    if (TblMonthlyRate.Count() > 0)
                    {
                        for (var i = 0; i < TblMonthlyRate.Count(); i++)
                        {
                            GetRateDailyOid = TblMonthlyRate[i].rate2oid;
                            GetRateDailyIDRValue = TblMonthlyRate[i].rate2idrvalue;
                            GetRateDailyUSDValue = TblMonthlyRate[i].rate2usdvalue;
                            GetRateMonthlyOid = TblMonthlyRate[i].rate2oid;
                            GetRateMonthlyIDRValue = TblMonthlyRate[i].rate2idrvalue;
                            GetRateMonthlyUSDValue = TblMonthlyRate[i].rate2usdvalue;
                        }
                    }
                    else
                    {
                        GetRateMonthlyLastError = "Silahkan Isi Nilai Tukar Mata Uang : " + sCurrCode + " Pada Tanggal : " + ClassFunction.toDate(sDate) + " !!";
                        GetRateDailyOid = 0;
                        GetRateDailyIDRValue = 1;
                        GetRateDailyUSDValue = 0;
                        GetRateMonthlyOid = 0;
                        GetRateMonthlyIDRValue = 1;
                        GetRateMonthlyUSDValue = 0;
                    }
                }
                else
                {
                    GetRateMonthlyLastError = "Silahkan Isi Nilai Tukar Mata Uang : " + sCurrCode + " Pada Tanggal : " + ClassFunction.toDate(sDate) + " !!";
                    GetRateDailyOid = 0;
                    GetRateDailyIDRValue = 1;
                    GetRateDailyUSDValue = 0;
                    GetRateMonthlyOid = 0;
                    GetRateMonthlyIDRValue = 1;
                    GetRateMonthlyUSDValue = 0;
                }
            }
            else
            {
                GetRateDailyOid = 0;
                GetRateDailyIDRValue = 1;
                GetRateDailyUSDValue = 0;
                GetRateMonthlyOid = 0;
                GetRateMonthlyIDRValue = 1;
                GetRateMonthlyUSDValue = 0;
            }
        }

        //public void SetRateValue(string sCurr, string sDate)
        //{
        //    string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        //    QL_SELFIEEntities db = new QL_SELFIEEntities();
        //    sCurrCode = sCurr;
        //    sSql = "SELECT curroid FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND currcode=" + sCurrCode;
        //    iCurrOid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

        //    //Get Daily Rate Data
        //    sSql = "SELECT TOP 1 rateoid, CAST (rateres1 AS REAL) AS rateidrvalue, CAST (rateres2 AS REAL) AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" + CompnyCode + "' AND curroid=" + iCurrOid + " AND '" + sDate + " 00:00:00'>=ratedate AND '" + sDate + " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC";
        //    List<DailyRate> TblDailyRate = db.Database.SqlQuery<DailyRate>(sSql).ToList();
        //    if (TblDailyRate != null)
        //    {
        //        if (TblDailyRate.Count() > 0)
        //        {
        //            for (var i = 0; i < TblDailyRate.Count(); i++)
        //            {
        //                GetRateDailyOid = TblDailyRate[i].rateoid;
        //                GetRateDailyIDRValue = Convert.ToDecimal(TblDailyRate[i].rateidrvalue);
        //                GetRateDailyUSDValue = Convert.ToDecimal(TblDailyRate[i].rateusdvalue);
        //            }
        //        }
        //        else
        //        {
        //            GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
        //            GetRateDailyOid = 0;
        //            GetRateDailyIDRValue = 0;
        //            GetRateDailyUSDValue = 0;
        //        }
        //    }
        //    else
        //    {
        //        GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
        //        GetRateDailyOid = 0;
        //        GetRateDailyIDRValue = 0;
        //        GetRateDailyUSDValue = 0;
        //    }

        //    //Get Monthly Rate Data
        //    sSql = "SELECT TOP 1 rate2oid, CAST (rate2res1 AS REAL) AS rate2idrvalue, CAST (rate2res2 AS REAL) AS rate2usdvalue FROM QL_mstrate2 WHERE cmpcode='" + CompnyCode + "' AND curroid=" + iCurrOid + " AND rate2month=" + Convert.ToDateTime(sDate).Month + " AND rate2year=" + Convert.ToDateTime(sDate).Year + " AND activeflag='ACTIVE' ORDER BY rate2oid DESC";
        //    List<MonthlyRate> TblMonthlyRate = db.Database.SqlQuery<MonthlyRate>(sSql).ToList();
        //    if (TblMonthlyRate != null)
        //    {
        //        if (TblMonthlyRate.Count() > 0)
        //        {
        //            for (var i = 0; i < TblMonthlyRate.Count(); i++)
        //            {
        //                GetRateMonthlyOid = TblMonthlyRate[i].rate2oid;
        //                GetRateMonthlyIDRValue = Convert.ToDecimal(TblMonthlyRate[i].rate2idrvalue);
        //                GetRateMonthlyUSDValue = Convert.ToDecimal(TblMonthlyRate[i].rate2usdvalue);
        //            }
        //        }
        //        else
        //        {
        //            GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
        //            GetRateDailyOid = 0;
        //            GetRateDailyIDRValue = 0;
        //            GetRateDailyUSDValue = 0;
        //        }
        //    }
        //    else
        //    {
        //        GetRateDailyLastError = "Please define some Rate Daily data for Currency : " + sCurrCode + " and Date : " + sDate + " first before continue using this form!";
        //        GetRateDailyOid = 0;
        //        GetRateDailyIDRValue = 0;
        //        GetRateDailyUSDValue = 0;
        //    }
        //}

        //public void SetRateValue(int iRateOid, int iRate2Oid)
        //{
        //    string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        //    QL_SELFIEEntities db = new QL_SELFIEEntities();

        //    //Get Daily Rate Data
        //    sSql = "SELECT TOP 1 rateoid, CAST (rateres1 AS REAL) AS rateidrvalue, CAST (rateres2 AS REAL) AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" + CompnyCode + "' AND rateoid=" + iRateOid;
        //    List<DailyRate> TblDailyRate = db.Database.SqlQuery<DailyRate>(sSql).ToList();
        //    if (TblDailyRate != null)
        //    {
        //        if (TblDailyRate.Count() > 0)
        //        {
        //            for (var i = 0; i < TblDailyRate.Count(); i++)
        //            {
        //                GetRateDailyOid = TblDailyRate[i].rateoid;
        //                GetRateDailyIDRValue = Convert.ToDecimal(TblDailyRate[i].rateidrvalue);
        //                GetRateDailyUSDValue = Convert.ToDecimal(TblDailyRate[i].rateusdvalue);
        //            }
        //        }
        //    }

        //    //Get Monthly Rate Data
        //    sSql = "SELECT TOP 1 rate2oid, CAST (rate2res1 AS REAL) AS rate2idrvalue, CAST (rate2res2 AS REAL) AS rate2usdvalue FROM QL_mstrate2 WHERE cmpcode='" + CompnyCode + "' AND rate2oid=" + iRate2Oid;
        //    List<MonthlyRate> TblMonthlyRate = db.Database.SqlQuery<MonthlyRate>(sSql).ToList();
        //    if (TblMonthlyRate != null)
        //    {
        //        if (TblMonthlyRate.Count() > 0)
        //        {
        //            for (var i = 0; i < TblMonthlyRate.Count(); i++)
        //            {
        //                GetRateMonthlyOid = TblMonthlyRate[i].rate2oid;
        //                GetRateMonthlyIDRValue = Convert.ToDecimal(TblMonthlyRate[i].rate2idrvalue);
        //                GetRateMonthlyUSDValue = Convert.ToDecimal(TblMonthlyRate[i].rate2usdvalue);
        //            }
        //        }
        //    }
        //}
    }
}