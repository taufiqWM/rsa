﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;

namespace RSA.Controllers
{
    public class ClassProcedure
    {
        public static string SetDBLogonForReport(ReportDocument reportDoc)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.IntegratedSecurity = false;
            connectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DB-Server"];
            connectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DB-Name"];
            connectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["DB-User"];
            connectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["DB-Password"];

            Tables tables = reportDoc.Database.Tables;
            foreach (Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }

            foreach (ReportDocument subreport in reportDoc.Subreports)
            {
                Tables myTablesSub = subreport.Database.Tables;
                foreach (Table table in myTablesSub)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
            }

            return "OK";
        }

        public static string SetDBLogonForReport(ReportDocument reportDoc, string[] exctablename)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.IntegratedSecurity = false;
            connectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DB-Server"];
            connectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DB-Name"];
            connectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["DB-User"];
            connectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["DB-Password"];

            Tables tables = reportDoc.Database.Tables;
            foreach (Table table in tables)
            {
                if (!exctablename.Contains(table.Name))
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
            }

            foreach (ReportDocument subreport in reportDoc.Subreports)
            {
                Tables myTablesSub = subreport.Database.Tables;
                foreach (Table table in myTablesSub)
                {
                    if (!exctablename.Contains(table.Name))
                    {
                        TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                        tableLogonInfo.ConnectionInfo = connectionInfo;
                        table.ApplyLogOnInfo(tableLogonInfo);
                    }
                }
            }

            return "OK";
        }

        public static string SetDBLogonForReportTable(Table reportTbl)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.IntegratedSecurity = false;
            connectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DB-Server"];
            connectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DB-Name"];
            connectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["DB-User"];
            connectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["DB-Password"];

            TableLogOnInfo tableLogonInfo = reportTbl.LogOnInfo;
            tableLogonInfo.ConnectionInfo = connectionInfo;
            reportTbl.ApplyLogOnInfo(tableLogonInfo);

            return "OK";
        }
    }
}