﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class SetAppController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstapprovalperson
        {
            public string cmpcode { get; set; }
            public int apppersonoid { get; set; }
            public string tablename { get; set; }
            public string apppersonnote { get; set; }
            public string activeflag { get; set; }
            public string profoid { get; set; }
            public string personname { get; set; }
            public string superuser { get; set; }

        }

        // GET/POST: User
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SetApp", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " And " + filter.ddlfilter + " like '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " And activeflag = '" + filter.ddlstatus + "'";

                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }

            //sSql = "Select * FROM QL_mstprof us WHERE us.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            sSql = "SELECT app.cmpcode, app.apppersonoid, app.tablename, app.apppersonnote, app.activeflag, p.profoid, ISNULL(per.personname, '') [personname], CASE WHEN app.issuperuser = 0 THEN '' ELSE 'SuperUser' END [superuser] FROM QL_approvalperson app INNER JOIN QL_mstprof p ON p.cmpcode = app.cmpcode AND p.profoid = app.approvaluser LEFT JOIN  QL_mstperson per ON per.cmpcode = app.cmpcode AND per.personoid = p.personoid WHERE app.cmpcode = '" + Session["CompnyCode"].ToString() + "' " + sfilter;
            List<mstapprovalperson> dt = db.Database.SqlQuery<mstapprovalperson>(sSql).ToList();
            return View(dt);
        }


        // GET: User/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SetApp", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var cmpcode = Session["CompnyCode"].ToString();
            QL_approvalperson qL_approvalperson;
            if (id == null)
            {
                qL_approvalperson = new QL_approvalperson();
                qL_approvalperson.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                qL_approvalperson = db.QL_approvalperson.Find(cmpcode, id);
                ViewBag.action = "Edit";
            }
            
            if (qL_approvalperson == null)
            {
                return HttpNotFound();
            }

            setViewBag(qL_approvalperson);
            return View(qL_approvalperson);
        }

        // POST: User/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_approvalperson qL_approvalperson, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SetApp", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_approvalperson.approvaluser))
                ModelState.AddModelError("approvaluser", "Please Fill User ID!!");
            else if (db.QL_approvalperson.Where(w => w.approvaluser == qL_approvalperson.approvaluser && w.tablename == qL_approvalperson.tablename).Count() > 0 & action == "Create")
                ModelState.AddModelError("approvaluser", "This User ID already use, Please Fill Another User ID!!");

            var mstoid = ClassFunction.GenerateID("QL_approvalperson");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        qL_approvalperson.apppersontype = "Final";
                        qL_approvalperson.apppersonlevel = 1;
                        qL_approvalperson.apppersonnote = qL_approvalperson.apppersonnote == null ? "" : qL_approvalperson.apppersonnote;

                        if (action == "Create")
                        {
                            //Insert
                            qL_approvalperson.apppersonoid = mstoid;
                            qL_approvalperson.createuser = Session["UserID"].ToString();
                            qL_approvalperson.createtime = ClassFunction.GetServerTime();
                            qL_approvalperson.upduser = Session["UserID"].ToString();
                            qL_approvalperson.updtime = ClassFunction.GetServerTime();
                            db.QL_approvalperson.Add(qL_approvalperson);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_approvalperson'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            qL_approvalperson.upduser = Session["UserID"].ToString();
                            qL_approvalperson.updtime = ClassFunction.GetServerTime();
                            db.Entry(qL_approvalperson).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;

            setViewBag(qL_approvalperson);
            return View(qL_approvalperson);
        }

        private void setViewBag(QL_approvalperson qL_approvalperson)
        {
            sSql = " SELECT * FROM QL_mstprof WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var approvaluser = new SelectList(db.Database.SqlQuery<QL_mstprof>(sSql), "profoid", "profname", qL_approvalperson.approvaluser);
            ViewBag.approvaluser = approvaluser;

            if(approvaluser.Count() > 0)
            {
                sSql = "SELECT ISNULL(personname, '') FROM QL_mstprof pr LEFT JOIN QL_mstperson p ON pr.cmpcode=p.cmpcode AND pr.personoid=p.personoid WHERE pr.cmpcode='" + CompnyCode + "' AND pr.profoid='" + approvaluser.First().Value + "'";
                var personname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                ViewBag.personname = personname;
            }
        }

        [HttpPost]
        public ActionResult getNameProf(string profoid)
        {
            sSql = "SELECT ISNULL(personname, '') FROM QL_mstprof pr LEFT JOIN QL_mstperson p ON pr.cmpcode=p.cmpcode AND pr.personoid=p.personoid WHERE pr.cmpcode='" + CompnyCode + "' AND pr.profoid='" + profoid + "'";
            var personname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            return Json(personname);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SetApp", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string cmp = Session["CompnyCode"].ToString();
            string result = "sukses";
            string msg = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {

                    QL_approvalperson qL_approvalperson = db.QL_approvalperson.Find(cmp, id);
                    db.QL_approvalperson.Remove(qL_approvalperson);
                    db.SaveChanges();
                    // Oh we are here, looks like everything is fine - save all the data permanently
                    objTrans.Commit();
                    //return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // roll back all database operations, if any thing goes wrong
                    objTrans.Rollback();

                    result = "failed";
                    msg += ex.ToString();
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
