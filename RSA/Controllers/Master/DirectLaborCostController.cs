﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class DirectLaborCostController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstdlc
        {
            public string cmpcode { get; set; }
            public int dlcoid { get; set; }
            public DateTime dlcdate { get; set; }
            public string dlcdesc { get; set; }
            public string activeflag { get; set; }
            public string dlcnote { get; set; }
            public string bomdesc { get; set; }
            public string itemlongdesc { get; set; }
            public string itemcode { get; set; }
        }

        public class mstdlcdtl
        {
            public int dlcdtlseq { get; set; }
            public int deptoid { get; set; }
            public decimal dlcpct { get; set; }
            public decimal dlcdtlamount { get; set; }
            public decimal dlcohdamount { get; set; }
            public string dlcdtlnote { get; set; }
            public decimal dlcdtlweightpoint { get; set; }
        }

        public class mstbom
        {
            public int bomoid { get; set; }
            public string bomdesc { get; set; }
            public string bomdate { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string precostdesc { get; set; }
            public decimal precostdlcamt { get; set; }
            public decimal precostoverheadamt { get; set; }
            public DateTime updtimebom { get; set; }
            public int curroid_overhead { get; set; }
            public string lastupduser_precost { get; set; }
            public DateTime lastupdtime_precost { get; set; }
            public string itemnote { get; set; }
        }

        private void InitDDL(QL_mstdlc tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid_ohd = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid_ohd);
            var curroid_ohd_precost = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid_ohd_precost);
            sSql = "SELECT * FROM QL_mstdept WHERE deptoid IN (SELECT bomdtl1deptoid FROM QL_mstbomdtl1 WHERE cmpcode='" + tbl.cmpcode + "' AND bomoid=" + tbl.bomoid + ")";
            var deptname = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", null);

            ViewBag.curroid_ohd = curroid_ohd;
            ViewBag.curroid_ohd_precost = curroid_ohd_precost;
            ViewBag.deptname = deptname;

            if (string.IsNullOrEmpty(tbl.cmpcode) && cmpcode.Count() > 0)
            {
                sSql = GetQueryBindListCOA(cmpcode.First().Value, "VAR_DIRECT_LABOR_COST");
                var dlcacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc");
                ViewBag.dlcacctgoid = dlcacctgoid;
                sSql = GetQueryBindListCOA(cmpcode.First().Value, "VAR_OVERHEAD_COST");
                var ohdacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc");
                ViewBag.ohdacctgoid = ohdacctgoid;
            }
            else
            {
                sSql = GetQueryBindListCOA(tbl.cmpcode, "VAR_DIRECT_LABOR_COST");
                var dlcacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dlcacctgoid);
                ViewBag.dlcacctgoid = dlcacctgoid;
                sSql = GetQueryBindListCOA(tbl.cmpcode, "VAR_OVERHEAD_COST");
                var ohdacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.ohdacctgoid);
                ViewBag.ohdacctgoid = ohdacctgoid;
            }
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult InitDDLDept(string cmpcode, int bomoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_mstdept> tbl = new List<QL_mstdept>();
                tbl = db.Database.SqlQuery<QL_mstdept>("SELECT * FROM QL_mstdept WHERE deptoid IN (SELECT bomdtl1deptoid FROM QL_mstbomdtl1 WHERE cmpcode='" + cmpcode + "' AND bomoid=" + bomoid + ")").ToList();
                js = Json(tbl, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                js = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, sVar)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindBOMData(string cmpcode)
        {
            JsonResult js = null;
            try
            {
                List<mstbom> tbl = new List<mstbom>();
                sSql = "SELECT bom.bomoid, bom.bomdesc, CONVERT(VARCHAR(10), bom.bomdate, 101) AS bomdate, i.itemcode, i.itemlongdesc, pr.precostdesc, pr.precostdlcamt, pr.precostoverheadamt, bom.updtime updtimebom, curroid_overhead, pr.upduser lastupduser_precost, pr.updtime lastupdtime_precost, i.itemnote FROM QL_mstbom bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid INNER JOIN QL_mstprecost pr ON pr.precostoid=bom.precostoid WHERE bom.cmpcode='" + cmpcode + "' AND bom.activeflag='ACTIVE' AND bom.dlcstatus='' ORDER BY CAST(bomdate AS DATETIME) DESC, bom.bomoid DESC";
                tbl = db.Database.SqlQuery<mstbom>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception ex)
            {
                js = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult InitDLCDesc(string cmpcode, string bomdesc, int bomoid, string bomdesc2 = "")
        {
            JsonResult js = null;
            try
            {
                var icounter = db.QL_mstdlc.Where(x => x.cmpcode == cmpcode && x.bomoid == bomoid).DefaultIfEmpty().Count() + 1;
                var desc = "DLC " + bomdesc + " " + icounter.ToString();
                bomdesc2 = desc;
                return Json(desc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                bomdesc2 = ex.ToString();
                js = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<mstdlcdtl> dtDtl)
        {
            Session["QL_mstdlcdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstdlcdtl"] == null)
            {
                Session["QL_mstdlcdtl"] = new List<mstdlcdtl>();
            }

            List<mstdlcdtl> dataDtl = (List<mstdlcdtl>)Session["QL_mstdlcdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_mstdlc tbl)
        {

        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";

            sSql = "SELECT dlc.dlcoid, dlc.cmpcode, dlc.dlcoid [ID], bom.bomdesc [BOM], CONVERT(VARCHAR(10), dlcdate, 101) [Date], i.itemcode [Code], i.itemlongdesc [Description], dlc.activeflag [Status], dlc.dlcnote [Note], '' [Print] FROM QL_mstdlc dlc INNER JOIN QL_mstbom bom ON bom.cmpcode=dlc.cmpcode AND bom.bomoid=dlc.bomoid INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid ";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "WHERE dlc.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "WHERE dlc.cmpcode LIKE '%'";
            sSql += " ORDER BY dlc.dlcdate DESC, dlc.dlcoid DESC";
            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstdlc");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "ID")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["dlcoid"].ToString() + "/" + dr["cmpcode"].ToString(), "DirectLaborCost") + "'>" + item + "</a>";
                            }
                            if (col.ColumnName == "Print")
                            {
                                item = "<button title='Print' type='button' class='btn btn-sm btn - primary' onclick='PrintReport(" + dr["dlcoid"].ToString() + ")'><i class='fa fa-print'></i></button>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: dlcMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdlc tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstdlc();
                tbl.dlcoid = ClassFunction.GenerateID("QL_mstdlc");
                tbl.dlcdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.activeflag = "ACTIVE";
                tbl.dlctotalpct = 100;

                Session["QL_mstdlcdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstdlc.Find(cmp, id);
                sSql = "SELECT dlcd.dlcdtlseq, dlcd.deptoid, de.deptname, dlcd.dlcpct, dlcd.dlcdtlamount, dlcd.dlcohdamount, dlcd.dlcdtlnote, dlcd.dlcdtlweightpoint FROM QL_mstdlcdtl dlcd INNER JOIN QL_mstdept de ON de.cmpcode=dlcd.cmpcode AND de.deptoid=dlcd.deptoid WHERE dlcd.dlcoid=" + tbl.dlcoid + " ORDER BY dlcd.dlcdtlseq";
                var tbldtl = db.Database.SqlQuery<mstdlcdtl>(sSql).ToList();

                Session["QL_mstdlcdtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dpapMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstdlc tbl, string action, string closing, decimal precostdlcamt, decimal precostoverheadamt)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<mstdlcdtl> dtDtl = (List<mstdlcdtl>)Session["QL_mstdlcdtl"];
            if (tbl != null)
            {
                if (tbl.bomoid == 0)
                    ModelState.AddModelError("", "Please select BOM field!");
                else
                {
                    if (action == "New Data")
                    {
                        if (db.QL_mstdlc.Where(x => x.cmpcode == tbl.cmpcode && x.bomoid == tbl.bomoid).Count() > 0)
                            ModelState.AddModelError("", "Selected BOM already used by another data!");
                    }
                }
                if (tbl.dlcamount == 0)
                    ModelState.AddModelError("", "DL COST field must be more than 0!");
                else
                {
                    if (tbl.dlcamount > precostdlcamt)
                        ModelState.AddModelError("", "DL COST field must be less than MAX DLC!");
                }
                if (tbl.curroid_ohd != tbl.curroid_ohd_precost)
                    ModelState.AddModelError("", "Selected currency in OHD COST must be same with selected currency in MAX OHD!");
                else
                {
                    if (tbl.dlcoverheadamt == 0)
                        ModelState.AddModelError("", "OHD COST field must be more than 0!");
                    else
                    {
                        if (tbl.dlcoverheadamt > precostoverheadamt)
                            ModelState.AddModelError("", "OHD COST field must be less than MAX OHD!");
                    }
                }
            }
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                decimal tWeight = dtDtl.Sum(x=>x.dlcdtlweightpoint);
                if (tWeight > 0)
                {
                    if (tWeight != 100)
                        ModelState.AddModelError("", "- Total weight point must be 0 or 100 % .. !");
                }
            }

            var precost = (from pr in db.QL_mstprecost join bom in db.QL_mstbom on pr.precostoid equals bom.precostoid where bom.bomoid == tbl.bomoid select new { pr.updtime, pr.upduser }).FirstOrDefault();
            tbl.lastupdtime_precost = precost.updtime;
            tbl.lastupduser_precost = precost.upduser;

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_MSTDLC");
                var dtloid = ClassFunction.GenerateID("QL_MSTDLCDTL");
                var log_oid = ClassFunction.GenerateID("QL_LOGHIST");
                var servertime = ClassFunction.GetServerTime();
                tbl.dlcdesc = ClassFunction.Left(tbl.dlcdesc, 250);

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_mstdlc.Find(tbl.cmpcode, tbl.dlcoid) != null)
                                tbl.dlcoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_mstdlc.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.dlcoid + " WHERE tablename='QL_MSTDLC'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var lastbomoid = tbl.bomoid;
                            if (lastbomoid != tbl.bomoid)
                            {
                                sSql = "UPDATE QL_mstprecost SET dlcstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" + lastbomoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_mstbom SET dlcstatus='' WHERE bomoid=" + lastbomoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            QL_loghist loghist = new QL_loghist();
                            loghist.cmpcode = tbl.cmpcode; loghist.log_oid = log_oid; loghist.log_table = "QL_mstdlc"; loghist.log_form = "Direct Labor Cost"; loghist.log_refoid = tbl.dlcoid; loghist.log_userid = tbl.upduser; loghist.log_datetime = tbl.updtime; loghist.log_activity = "UPDATE"; loghist.log_note = "";
                            db.QL_loghist.Add(loghist);
                            db.SaveChanges();

                            var trndtl = db.QL_mstdlcdtl.Where(a => a.dlcoid == tbl.dlcoid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstdlcdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstprecost SET dlcstatus='Created' WHERE cmpcode='" + tbl.cmpcode + "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" + tbl.bomoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstbom SET dlcstatus='Created' WHERE bomoid=" + tbl.bomoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        decimal dTotalDL = 0, dTotalFOH = 0;
                        QL_mstdlcdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dValDL = Math.Round(dtDtl[i].dlcdtlamount, 4, MidpointRounding.AwayFromZero);
                            decimal dValFOH = Math.Round(dtDtl[i].dlcohdamount, 4, MidpointRounding.AwayFromZero);
                            dTotalDL += dValDL; dTotalFOH += dValFOH;
                            if (i == dtDtl.Count())
                            {
                                if (tbl.dlcamount != dTotalDL)
                                    dValDL += tbl.dlcamount - dTotalDL;
                                if (tbl.dlcoverheadamt != dTotalFOH)
                                    dValFOH += tbl.dlcoverheadamt - dTotalFOH;
                            }

                            tbldtl = new QL_mstdlcdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.dlcdtloid = dtloid++;
                            tbldtl.dlcoid = tbl.dlcoid;
                            tbldtl.dlcdtlseq = i + 1;
                            tbldtl.deptoid = dtDtl[i].deptoid;
                            tbldtl.dlcrefoid = 0;
                            tbldtl.dlcreftype = "";
                            tbldtl.dlcpct= dtDtl[i].dlcpct;
                            tbldtl.dlcdtlamount = dValDL;
                            tbldtl.dlcdtlnote = (dtDtl[i].dlcdtlnote == null ? "" : dtDtl[i].dlcdtlnote);
                            tbldtl.dlcdtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.dlcohdamount = dValFOH;
                            tbldtl.dlcdtlweightpoint = dtDtl[i].dlcdtlweightpoint;

                            db.QL_mstdlcdtl.Add(tbldtl);
                            db.SaveChanges();
                        }
                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_MSTDLCDTL'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        if (string.IsNullOrEmpty(closing))
                            return RedirectToAction("Form/" + tbl.dlcoid + "/" + tbl.cmpcode);
                        else
                            return RedirectToAction("Index");
                    }
                    catch(Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdlc tbl = db.QL_mstdlc.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();
            var log_oid = ClassFunction.GenerateID("QL_LOGHIST");

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else
            {
                if (ClassFunction.CheckDataExists("SELECT COUNT(wod.wodtl1oid) FROM QL_trnwodtl1 wod INNER JOIN QL_mstbom bom ON bom.cmpcode=wod.cmpcode AND bom.itemoid=wod.itemoid WHERE wod.cmpcode='" + tbl.cmpcode + "' AND bom.bomoid=" + tbl.bomoid))
                {
                    result = "failed";
                    msg = "This data can't be deleted because it is being used by another data!";
                }
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_mstprecost SET dlcstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" + tbl.bomoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_mstbom SET dlcstatus='' WHERE bomoid=" + tbl.bomoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_mstdlcdtl.Where(a => a.dlcoid == id && a.cmpcode == cmp);
                        db.QL_mstdlcdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_mstdlc.Remove(tbl);
                        db.SaveChanges();

                        QL_loghist loghist = new QL_loghist();
                        loghist.cmpcode = tbl.cmpcode; loghist.log_oid = log_oid; loghist.log_table = "QL_mstdlc"; loghist.log_form = "Direct Labor Cost"; loghist.log_refoid = tbl.dlcoid; loghist.log_userid = tbl.upduser; loghist.log_datetime = tbl.updtime; loghist.log_activity = "DELETE"; loghist.log_note = "";
                        db.QL_loghist.Add(loghist);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var servertime = ClassFunction.GetServerTime().ToString("MM/dd/yyyy");

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDLC_DtlFGPdf.rpt"));
            sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dlcm.cmpcode = div.cmpcode) AS [Business Unit], dlcm.cmpcode [CMPCODE], dlcm.dlcoid [Oid], dlcm.createtime [DLC Create Date], dlcm.dlcdesc [DLC Desc], dlcm.dlcamount [DLC Amount], dlcoverheadamt [DLC Overhead Amt], dlcm.dlctotalpct [Total Cost %], dlcm.dlcnote [Header Note],convert(datetime,dlcm.dlcres1,20) AS [Last BOM Update], pr.precostdlcamt [Max DLC], pr.precostoverheadamt [Max OHD], itemcode AS [FG Code], itemshortdesc AS [FG Short Desc.], itemlongdesc AS [FG Long Desc.], itemlength AS [FG Length], itemwidth AS [FG Width], itemheight AS [FG Height], itemdiameter AS [FG Diameter], itemvolume AS [FG Volume], itempacklength AS [FG Packing Length], itempackwidth AS [FG Packing Width], itempackheight AS [FG Packing Height], itempackdiameter AS [FG Packing Diameter], itempackvolume AS [FG Packing Volume], (itempackvolume*1000000000 / 28316846.59) AS [CBF], g.gendesc [Unit], dlcm.createuser [Create User], dlcm.upduser [Update User], dlcm.updtime [Update Time], dlcm.activeflag [Status], pre.precostdate [Precosting Date], pre.createtime [Precosting Create Date], pre.createuser [Precosting Create User], pre.updtime [Precosting Update Time], pre.upduser [Precosting Update User] , bom.bomdate [BOM Date], bom.createtime [BOM Create Date], bom.createuser [BOM Create User], bom.updtime [BOM Update Time], bom.upduser [BOM Update User] , dlcd.dlcdtlseq [DLC Seq] , dlcd.dlcpct [Cost %], dlcd.dlcdtlamount [DL Cost Amt.], dlcd.dlcohdamount [OHD Cost Amt.], dlcd.dlcdtlnote [Detail Note], dlcd.deptoid [Dept Oid From], (SELECT deptname FROM QL_mstdept df WHERE df.cmpcode=dlcd.cmpcode AND df.deptoid=dlcd.deptoid)  [Dept. From], bom.bomoid [BOM Oid], bomd1.bomdtl1todeptoid [Dept Oid To], (CASE bomd1.bomdtl1reftype WHEN 'FG' THEN 'END' ELSE (SELECT deptname FROM QL_mstdept dt WHERE dt.cmpcode=bomd1.cmpcode AND dt.deptoid=bomd1.bomdtl1todeptoid) END) [Dept. To] FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON bom.cmpcode=dlcm.cmpcode AND bom.bomoid=dlcm.bomoid INNER JOIN QL_mstprecost pr ON pr.precostoid=bom.precostoid INNER JOIN QL_mstitem i ON bom.itemoid=i.itemoid INNER JOIN QL_mstgen g ON g.genoid=i.itemunitoid INNER JOIN QL_mstprecost pre ON pre.cmpcode=dlcm.cmpcode AND pre.precostoid=bom.precostoid INNER JOIN QL_mstdlcdtl dlcd ON dlcm.cmpcode=dlcd.cmpcode AND dlcm.dlcoid=dlcd.dlcoid INNER JOIN QL_mstbomdtl1 bomd1 ON dlcm.cmpcode=bomd1.cmpcode AND bomd1.bomoid=dlcm.bomoid AND dlcd.deptoid=bomd1.bomdtl1deptoid WHERE dlcm.dlcoid IN (" + id + ")";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptTrn");
            report.SetDataSource(dtRpt);

            report.SetParameterValue("PrintUserID", Session["UserID"]);
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "DLCPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}