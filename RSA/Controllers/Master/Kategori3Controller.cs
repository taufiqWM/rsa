﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class Kategori3Controller : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori3", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c3.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT c3.*, c1.cat1shortdesc, c2.cat2shortdesc FROM QL_mstcat3 c3 inner join ql_mstcat1 c1 on c3.cat1oid=c1.cat1oid inner join ql_mstcat2 c2 on c3.cat2oid=c2.cat2oid WHERE c3.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and c3.cat3oid>0 ORDER BY cat3oid DESC";
            List<ListCat3> vbag = db.Database.SqlQuery<ListCat3>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori3", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat3 tbl;
            if (id == null)
            {
                tbl = new QL_mstcat3();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_mstcat3.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat3 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori3", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tbl.cat1oid < 1)
                ModelState.AddModelError("cat1oid", "Kategori 1 Tidak Boleh Kosong !!!");
            if (tbl.cat2oid < 1)
                ModelState.AddModelError("cat2oid", "Kategori 2 Tidak Boleh Kosong !!!");
            if (string.IsNullOrEmpty(tbl.cat3code))
                ModelState.AddModelError("cat3code", "Silahkan isi Kode!");
            else if (db.QL_mstcat3.Where(w => w.cat3code == tbl.cat3code & w.cat3oid != tbl.cat3oid).Count() > 0)
                ModelState.AddModelError("cat3code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.cat3shortdesc))
                ModelState.AddModelError("cat3shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat3.Where(w => w.cat3shortdesc == tbl.cat3shortdesc & w.cat1oid==tbl.cat1oid & w.cat2oid==tbl.cat2oid & w.cat3oid != tbl.cat3oid).Count() > 0)
                ModelState.AddModelError("cat3shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat3note))
                tbl.cat3note = "";

                var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat3");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.cat3oid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat3.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat3'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori3", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat3 tbl = db.QL_mstcat3.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat3.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void setViewBag(QL_mstcat3 qL_mstcat3)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + qL_mstcat3.cmpcode + "' AND activeflag='ACTIVE'";
            var cat1oid = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1oid", "cat1shortdesc", qL_mstcat3.cat1oid);
            ViewBag.cat1oid = cat1oid;

            if(qL_mstcat3.cat1oid > 0)
            {
                sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + qL_mstcat3.cmpcode + "' AND cat1oid=" + qL_mstcat3.cat1oid + " AND activeflag='ACTIVE'";
            }else{
                sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + qL_mstcat3.cmpcode + "' AND cat1oid=" + cat1oid.First().Value + " AND activeflag='ACTIVE'";
            }
            
            var cat2oid = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2oid", "cat2shortdesc", qL_mstcat3.cat2oid);
            ViewBag.cat2oid = cat2oid;
        }

        public class ListCat3
        {
            public int cat3oid { get; set; }
            public string cat3code { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat3longdesc { get; set; }
            public string cat3note { get; set; }
            public string activeflag { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }

        }

        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            List<QL_mstcat2> objcity = new List<QL_mstcat2>();
            objcity = db.QL_mstcat2.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1).ToList();
            SelectList obgcity = new SelectList(objcity, "cat2oid", "cat2shortdesc", 0);
            return Json(obgcity);
        }
    }
}
