﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class IndexHargaController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        public class indexharga
        {
            public string cmpcode { get; set; }
            public int indexoid { get; set; }
            public string tipewall { get; set; }
            public DateTime indexdate { get; set; }
            public string flute { get; set; }
            public decimal indexprice { get; set; }
            public string activeflag { get; set; }
            public string indexnote { get; set; }
            public string sub { get; set; }

        }

        [HttpPost]
        public ActionResult Getsub1(string sub1)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub1 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub2(string sub2)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub2 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub3(string sub3)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub3 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub4(string sub4)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub4 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub5(string sub5)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub5 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getflute(string fl)
        {
            List<QL_mstgen> objgen = new List<QL_mstgen>();
            if (fl == "SW")
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=2 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            else
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=3 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            objgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objgen, "genoid", "gendesc", 0);

            return Json(DDLCat2);
        }



        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c3.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT indexoid, indexdate, tipewall, gendesc flute, hargaindex as indexprice, i.activeflag, indexnote, (case when tipewall='SW' then(sub1+'/'+sub2+'/'+sub3) else (sub1+'/'+sub2+'/'+sub3+'/'+sub4+'/'+sub5) end) sub from ql_msthargaindex i inner join ql_mstgen g on g.genoid=fluteoid where i.cmpcode='" + CompnyCode + "'";
            List<indexharga> vbag = db.Database.SqlQuery<indexharga>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_msthargaindex tbl;
            if (id == null)
            {
                tbl = new QL_msthargaindex();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_msthargaindex.Find(id);
                ViewBag.action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_msthargaindex tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
           
            if (string.IsNullOrEmpty(tbl.indexnote))
                tbl.indexnote = "";
            if (tbl.tipewall == "SW")
            {
                tbl.sub4 = "";
                tbl.sub5 = "";
            }

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_msthargaindex");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.indexoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_msthargaindex.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + mstoid + " WHERE tablename='QL_msthargaindex'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_msthargaindex set activeflag='INACTIVE' where cmpcode='" + CompnyCode+ "' and tipewall='" + tbl.tipewall + "' and fluteoid='" + tbl.fluteoid + "' and isnull(sub1,'')='"+tbl.sub1+ "' and isnull(sub2,'')='" + tbl.sub2 + "'and isnull(sub3,'')='" + tbl.sub3 + "'and isnull(sub4,'')='" + tbl.sub4 + "'and isnull(sub5,'')='" + tbl.sub5 + "' and indexoid <>" + tbl.indexoid + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_msthargaindex tbl = db.QL_msthargaindex.Find( id);
                        db.QL_msthargaindex.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void setViewBag(QL_msthargaindex tbl)
        {
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=2 ORDER BY gendesc";
            if (tbl.tipewall == "DW")
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=3 ORDER BY gendesc";
            }
            var fluteoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.fluteoid);
            ViewBag.fluteoid = fluteoid;

            //substance
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub1 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub1);
            ViewBag.sub1 = sub1;
            ViewBag.sub2 = sub1;
            ViewBag.sub3 = sub1;
            ViewBag.sub4 = sub1;
            ViewBag.sub5 = sub1;



        }

      
    }
}
