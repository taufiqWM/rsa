﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class BankController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstbank
        {
            public int bankoid { get; set; }
            public string bankcode { get; set; }
            public string bankname { get; set; }
            public string bankbranch { get; set; }
            public string bankaddress { get; set; }
            public int bankcityoid { get; set; }
            public string cityname { get; set; }
            public string bankphone { get; set; }
            public string bankemail { get; set; }
            public string banknote { get; set; }
            public string activeflag { get; set; }
        }

        private void InitDDL(QL_mstbank tbl)
        {
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='CITY' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var bankcityoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.bankcityoid);
            ViewBag.bankcityoid = bankcityoid;
        }

        private string GenerateCodeBank(string CompnyCode)
        {
            //QL_RSAEntities db = new QL_RSAEntities();
            string IDNEW = "";
            var sSql = "SELECT ISNULL(MAX(CAST(RIGHT(bankcode, 3) AS INTEGER)), 0) + 1 [IDNEW] FROM QL_mstbank WHERE cmpcode='" + CompnyCode + "'";
            var ID = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            
            if (Convert.ToString(ID).Length < 3)
            {
                for(var i = Convert.ToInt16(Convert.ToString(ID).Length); i < 3; i++)
                {
                    IDNEW = "0" + IDNEW;
                }
            }
            IDNEW = "BK" + IDNEW + Convert.ToString(ID);
            return IDNEW;
        }

        // GET: Bank
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT i.*, g.gendesc [cityname] FROM QL_mstbank i inner join QL_mstgen g on g.genoid=i.bankcityoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY bankname";
            List<mstbank> vbag = db.Database.SqlQuery<mstbank>(sSql).ToList();
            return View(vbag);
        }

        // GET: Service/Form
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbank tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstbank();
                tbl.bankoid = ClassFunction.GenerateID("QL_mstbank");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstbank.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstbank tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            //if (tbl.bankcode == null)
            //    ModelState.AddModelError("bankcode", "Kode Bank HARUS DIISI");
            if (tbl.bankname == null)
                ModelState.AddModelError("bankname", "Nama Bank HARUS DIISI");

            var mstoid = ClassFunction.GenerateID("QL_mstbank");
            if (action == "Update Data")
            {
                mstoid = tbl.bankoid;
            }
            //var dtloid = ClassFunction.GenerateID("QL_mstbankdtl");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.bankcode = tbl.bankcode == null ? "" : tbl.bankcode;
                        tbl.bankbranch = tbl.bankbranch == null ? "" : tbl.bankbranch;
                        tbl.bankaddress = tbl.bankaddress == null ? "" : tbl.bankaddress;
                        tbl.bankphone = tbl.bankphone == null ? "" : tbl.bankphone;
                        tbl.bankemail = tbl.bankemail == null ? "" : tbl.bankemail;
                        tbl.bankswiftcode = tbl.bankswiftcode == null ? "" : tbl.bankswiftcode;
                        tbl.banknote = tbl.banknote == null ? "" : tbl.banknote;
                        tbl.bankres1 = tbl.bankres1 == null ? "" : tbl.bankres1;
                        tbl.bankres2 = tbl.bankres2 == null ? "" : tbl.bankres2;
                        tbl.bankres3 = tbl.bankres3 == null ? "" : tbl.bankres3;

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.cmpcode = CompnyCode;
                            tbl.bankoid = mstoid;
                            tbl.bankcode = GenerateCodeBank(CompnyCode);
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstbank.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstbank'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Bank/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbank qL_mstbank = db.QL_mstbank.Find(CompnyCode, Convert.ToInt32(id));

            string result = "success";
            string msg = "";
            if (qL_mstbank == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.QL_mstbank.Remove(qL_mstbank);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
