﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers
{
    public class PreCostingController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstprecost
        {
            public string cmpcode { get; set; }
            public int precostoid { get; set; }
            public DateTime precostdate { get; set; }
            public string activeflag { get; set; }
            public string bomstatus { get; set; }
            public string dlcstatus { get; set; }
            public string precostnote { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public string SKU { get; set; }
            public string divname { get; set; }
        }

        public class mstitem
        {
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public decimal cbf { get; set; }
            public string itemnote { get; set; }
        }

        public class mstcust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custtype { get; set; }
            public decimal amortdiecut { get; set; }
            public decimal amortdies { get; set; }
            public string frightcost { get; set; }
        }

        public class mstMat
        {
            public int bomoid { get; set; }
            public int itemoid { get; set; }
            public int c1 { get; set; }
            public int c2 { get; set; }
            public int c3 { get; set; }
            public int c4 { get; set; }
            public int c5 { get; set; }
            public string matcode { get; set; }
            public string itemlongdesc { get; set; }
            public string tipewall { get; set; }
            public string tipeflute { get; set; }
            public string sub1 { get; set; }
            public string sub2 { get; set; }
            public string sub3 { get; set; }
            public string sub4 { get; set; }
            public string sub5 { get; set; }
            public decimal pd { get; set; }
            public decimal ld { get; set; }
            public decimal td { get; set; }
            public decimal pl { get; set; }
            public decimal ll { get; set; }
            public decimal tl { get; set; }
            public decimal panjangsheet { get; set; }
            public decimal lebarsheet { get; set; }
            public decimal luassheet { get; set; }
            public decimal beratsheet { get; set; }
            public int countwarna { get; set; }
            public int jmlperlengkapan { get; set; }
        }
        public class combom
        {
            public int itemoid { get; set; }
            public int qty { get; set; }
            public string tipemat { get; set; }
            
        }


            [HttpPost]
        public ActionResult Getcomponen(int bomoid, int itemoid, int waxoid, int diecutmachineoid, int jmlwarna, decimal jmlorder, string itemtype,int custoid, decimal freightcost, decimal amortdiecut, decimal amortdies,decimal wrapingpc, decimal palletpc)
        {
            var lmesin = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='LEBAR MESIN'").FirstOrDefault());
            var OUTMAXCORR = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='OUT MAX CORR'").FirstOrDefault());
            var CHANGEORDER = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='CHANGE ORDER'").FirstOrDefault());
            var DEFECTCORR = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='DEFECT CORR'").FirstOrDefault());
            var DEFECTFLEXO = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='DEFECT FLEXO'").FirstOrDefault());
            var HARGAPERMETER2 = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='HARGA TINTA PER METER2'").FirstOrDefault());
            var OTHERCHEMICAL = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='OTHER CHEMICAL'").FirstOrDefault());
            var ENERGY = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='ENERGY'").FirstOrDefault());
            var BIAYAASTRALON = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='BIAYA ASTRALON'").FirstOrDefault());
            var BIAYASETTING = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='BIAYA SETTING'").FirstOrDefault());
            var LOADINGCOST = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='LOADING COST'").FirstOrDefault());
            var STRAPPINGCOST = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='STRAPPING COST'").FirstOrDefault());
            var LABORCOST = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='LABOR COST'").FirstOrDefault());
            var ADMINCOST = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where gendesc='ADMIN COST'").FirstOrDefault());
            var waxhdr = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where genoid='"+waxoid+"'").FirstOrDefault());
            var dccost = Convert.ToDecimal(db.Database.SqlQuery<string>("select genother1 from ql_mstgen where genoid='" + diecutmachineoid + "'").FirstOrDefault());
            //-------------------------------PERHITUNGAN WASTE BOX, LAYER, PARTITION:----------------------------------------
            decimal wastecorr1 = 0;
            decimal wastecorr2 = 0;
            decimal OUTCORR = 0;
            decimal LebarKertas = 0;
            decimal RmOrder = 0;
            decimal Trim = 0;
            decimal Changeorder = 0;
            decimal Totalcorr = 0;
            decimal wasteflexo1 = 0;
            decimal wasteflexo2 = 0;
            decimal totalflexo = 0;
            //decimal defectflexo = 0;
            decimal totalwaste = 0;
            decimal kertascol1lapisan1 = 0;
            decimal kertascol1lapisan2 = 0;
            decimal kertascol1lapisan3 = 0;
            decimal kertascol1lapisan4 = 0;
            decimal kertascol1lapisan5 = 0;
            decimal totalkertascol1perlapisan = 0;
            decimal totalkertascol2perlapisan = 0;
            //----------------------------------------PERHITUNGAN HARGA BOX, LAYER, PARTITION:-------------------------------
            decimal papperpkg = 0;
            decimal papperppcs = 0;
            decimal wastekg = 0;
            decimal wastepcs = 0;
            decimal inkkg = 0;
            decimal inkpcs = 0;
            decimal otrcemkg = 0;
            decimal otrcempcs = 0;
            decimal energykg = 0;
            decimal energypcs = 0;
            decimal waxkg = 0;
            decimal waxpcs = 0;
            decimal diecutkg = 0;
            decimal diecutpcs = 0;
            decimal rbrpollykg = 0;
            decimal rbrpollypcs = 0;
            decimal loadingckg = 0;
            decimal loadingcpcs = 0;
            decimal strapingckg = 0;
            decimal strapingcpcs = 0;
            decimal sftymarginkg = 0;
            decimal sftymarginpcs = 0;
            decimal totvarcostkg = 0;
            decimal totvarcostpcs = 0;
            decimal laborcostkg = 0;
            decimal laborcostpcs = 0;
            decimal admincostkg = 0;
            decimal admincostpcs = 0;
            decimal ttlfxmarginkg = 0;
            decimal ttlfxmarginpcs = 0;
            //decimal totalbtmpricekg = 0;
            //decimal totalbtmpricepcs = 0;
            decimal btmpricekg = 0;
            decimal btmpricepcs = 0;
            decimal wrapingkg = 0;
            decimal wrapingpcs = 0;
            decimal palletkg = 0;
            decimal palletpcs = 0;
            //----------------------------------------------var Material----------------------------------------------------
            decimal lsheet = 0;
            decimal bsheet = 0;
            decimal pnjsheet = 0;
            decimal lbrsheet = 0;
            decimal b1 = 0;
            decimal b2 = 0;
            decimal b3 = 0;
            decimal b4 = 0;
            decimal b5 = 0;
            decimal hkertas = 0;
            decimal hkertas2 = 0;
            decimal hkertas3 = 0;
            decimal hkertas4= 0;
            decimal hkertas5 = 0;
            decimal fc = 0;
            decimal fcpcs = 0;
            //---------------------------------------------MST-----------------------------
            decimal papperpkgmst = 0;
            decimal papperppcsmst = 0;
            decimal wastekgmst = 0;
            decimal wastepcsmst = 0;
            decimal inkkgmst = 0;
            decimal inkpcsmst = 0;
            decimal otrcemkgmst = 0;
            decimal otrcempcsmst = 0;
            decimal energykgmst = 0;
            decimal energypcsmst = 0;
            decimal waxpcsmst = 0;
            decimal waxkgmst = 0;
            decimal diecutkgmst = 0;
            decimal diecutpcsmst = 0;
            decimal rbrpollykgmst = 0;
            decimal rbrpollypcsmst = 0;
            decimal fcmst = 0;
            decimal fcpcsmst = 0;
            decimal loadingckgmst = 0;
            decimal loadingcpcsmst = 0;
            decimal strapingckgmst = 0;
            decimal strapingcpcsmst = 0;
            decimal sftymarginkgmst = 0;
            decimal sftymarginpcsmst = 0;
            decimal totvarcostkgmst = 0;
            decimal totvarcostpcsmst = 0;
            decimal laborcostkgmst = 0;
            decimal laborcostpcsmst = 0;
            decimal admincostkgmst = 0;
            decimal admincostpcsmst = 0;
            decimal ttlfxmarginkgmst = 0;
            decimal ttlfxmarginpcsmst = 0;
            decimal btmpricekgmst = 0;
            decimal btmpricepcsmst = 0;
            decimal wrapingkgmst = 0;
            decimal wrapingpcsmst = 0;
            decimal palletkgmst = 0;
            decimal palletpcsmst = 0;
            decimal totalberat = 0;


            List<combom> bomcomp = new List<combom>();
            List<QL_mstprecostdtl> precostdtl = new List<QL_mstprecostdtl>();
            List<QL_mstprecost> precostmst = new List<QL_mstprecost>();
            if (itemtype == "BOM")
            {
                sSql = "select i.itemoid, 1 as qty, 'ITEM' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.itemoid=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, 1 as qty, 'ITEM2' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.itemoid2=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, 1 as qty, 'ITEM3' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.itemoid3=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, ISNULL(pt1qty,0) as qty, 'PARTISI' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.pt1=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, ISNULL(pt2qty,0) as qty, 'PARTISI' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.pt2=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, ISNULL(pt3qty,0) as qty, 'PARTISI' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.pt3=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, ISNULL(ly1qty,0) as qty, 'LAYER' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.ly1=i.itemoid where bomoid='" + bomoid + "' union all select i.itemoid, ISNULL(ly2qty,0) as qty, 'LAYER' tipemat from QL_mstbillofmat b inner join ql_mstitem i on b.ly2=i.itemoid where bomoid='" + bomoid + "' ";
            }
            else
            {
                sSql = "select i.itemoid, 1 as qty, 'ITEM' tipemat from  ql_mstitem i where i.itemoid='"+ itemoid+"'";
            }
            bomcomp = db.Database.SqlQuery<combom>(sSql).ToList();
            if (bomcomp != null)
            {
                if (bomcomp.Count > 0)
                {
                    for (int i = 0; i < bomcomp.Count(); i++)
                    {
                        lsheet = db.Database.SqlQuery<decimal>("select luassheet from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        bsheet = db.Database.SqlQuery<decimal>("select beratpcs from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        pnjsheet = db.Database.SqlQuery<decimal>("select panjangsheet from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        lbrsheet = db.Database.SqlQuery<decimal>("select lebarsheet from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        b1 = db.Database.SqlQuery<decimal>("select isnull(berat1,0) from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        b2 = db.Database.SqlQuery<decimal>("select isnull(berat2,0) from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        b3 = db.Database.SqlQuery<decimal>("select isnull(berat3,0) from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        b4 = db.Database.SqlQuery<decimal>("select isnull(berat4,0) from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        b5 = db.Database.SqlQuery<decimal>("select isnull(berat5,0) from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "'").FirstOrDefault();
                        hkertas = db.Database.SqlQuery<decimal>("select price from QL_msrpaperprice where cat4oid=(select sub1 from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "')").FirstOrDefault();
                        hkertas2 = db.Database.SqlQuery<decimal>("select price from QL_msrpaperprice where cat4oid=(select sub2 from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "')").FirstOrDefault();
                        hkertas3 = db.Database.SqlQuery<decimal>("select price from QL_msrpaperprice where cat4oid=(select sub3 from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "')").FirstOrDefault();
                        hkertas4 = db.Database.SqlQuery<decimal>("select price from QL_msrpaperprice where cat4oid=(select sub4 from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "')").FirstOrDefault();
                        hkertas5 = db.Database.SqlQuery<decimal>("select price from QL_msrpaperprice where cat4oid=(select sub5 from ql_mstitem where itemoid='" + bomcomp[i].itemoid + "')").FirstOrDefault();
                        bsheet = (b1 + b2 + b3 + b4 + b5) * bomcomp[i].qty;
                        wastecorr1 = Math.Floor(lmesin / lbrsheet);
                        wastecorr2 = Math.Round((wastecorr1 * lbrsheet) / 50) + 1;
                        if (wastecorr1 > OUTMAXCORR)
                        {
                            OUTCORR = OUTMAXCORR;
                        }
                        else
                        {
                            OUTCORR = wastecorr1;
                        }
                        LebarKertas = 50 * wastecorr2;
                        RmOrder = Math.Round((jmlorder / OUTCORR) * (pnjsheet/1000));
                        Trim = LebarKertas - (OUTCORR * lbrsheet);
                        Changeorder = Math.Round((CHANGEORDER/RmOrder) * 100,2);
                        Totalcorr = Changeorder + DEFECTCORR;
                        if (jmlwarna == 0)
                        {
                            wasteflexo1 = 0;
                        }
                        else
                        {
                            wasteflexo1 = Convert.ToDecimal(db.Database.SqlQuery<string>("select isnull(genother1,14) from ql_mstgen where gendesc='" + jmlwarna + "'").FirstOrDefault());
                        }
                        wasteflexo2 = Math.Round((wasteflexo1 / jmlorder) * 100,2);
                        totalflexo = Math.Round((wasteflexo2 + DEFECTFLEXO)*(1-(Totalcorr/100)),2);
                        totalwaste = Totalcorr + totalflexo;
                        kertascol1lapisan1 = Math.Round(b1 * jmlorder);
                        kertascol1lapisan2 = Math.Round(b2 * jmlorder);
                        kertascol1lapisan3 = Math.Round(b3 * jmlorder);
                        kertascol1lapisan4 = Math.Round(b4 * jmlorder);
                        kertascol1lapisan5 = Math.Round(b5 * jmlorder);
                        totalkertascol1perlapisan = kertascol1lapisan1+ kertascol1lapisan2+ kertascol1lapisan3+ kertascol1lapisan4+ kertascol1lapisan5;
                        totalkertascol2perlapisan = (hkertas * kertascol1lapisan1) + (hkertas2 * kertascol1lapisan2) + (hkertas3 * kertascol1lapisan3) + (hkertas4 * kertascol1lapisan4) + (hkertas5 * kertascol1lapisan5);
                        //----------------------------------- Yang disimpan
                        papperpkg = Math.Round(totalkertascol2perlapisan / totalkertascol1perlapisan);
                        papperppcs = Math.Round(papperpkg * bsheet);
                       
                        wastekg = Math.Round((papperpkg * totalwaste) / 100);
                        wastepcs = Math.Round(wastekg * bsheet);
                        if (jmlwarna == 0)
                        {
                            inkkg = 0;
                        }
                        else
                        {
                            inkkg = Math.Round((HARGAPERMETER2 * lsheet) * (1 / bsheet));
                        }
                        inkpcs = Math.Round(bsheet * inkkg);
                        otrcemkg = OTHERCHEMICAL;
                        otrcempcs = Math.Round(OTHERCHEMICAL * bsheet);
                        energykg = ENERGY;
                        energypcs = Math.Round(energykg * bsheet);
                        waxpcs = Math.Round(waxhdr *(lbrsheet/1000)*(pnjsheet/1000));
                        waxkg = Math.Round(waxpcs / bsheet);
                        if (amortdiecut == 0)
                        {
                            diecutkg = 0;
                        }
                        else
                        {
                            diecutkg = Math.Round((dccost / amortdiecut) / bsheet);
                        }
                        diecutpcs = Math.Round(diecutkg * bsheet);
                        if (amortdies == 0)
                        {
                            rbrpollykg = 0;
                        }
                        else
                        {
                            rbrpollykg = Math.Round((((lsheet * jmlwarna * BIAYAASTRALON) + (jmlwarna * BIAYASETTING)) / amortdies) * (1 / bsheet), 2);
                        }
                        rbrpollypcs = Math.Round(rbrpollykg * bsheet,2);
                        fc = freightcost;
                        fcpcs = Math.Round(fc * bsheet);
                        loadingckg = LOADINGCOST;
                        loadingcpcs = Math.Round(loadingckg * bsheet);
                        strapingckg = STRAPPINGCOST;
                        strapingcpcs = Math.Round(strapingckg * bsheet);
                        sftymarginkg = Math.Round((papperpkg + wastekg + inkkg + otrcemkg + energykg + waxkg + diecutkg + rbrpollykg + freightcost + loadingckg + wrapingpc+ palletpc + strapingckg)/100);
                        sftymarginpcs = Math.Round(sftymarginkg * bsheet);
                        totvarcostkg = Math.Round(papperpkg + wastekg + inkkg + otrcemkg + energykg + waxkg + diecutkg + rbrpollykg + freightcost + loadingckg + wrapingkg + palletkg + strapingckg + sftymarginkg);
                        totvarcostpcs = Math.Round(totvarcostkg * bsheet);
                        laborcostkg = LABORCOST;
                        laborcostpcs = Math.Round(laborcostkg * bsheet);
                        admincostkg = ADMINCOST;
                        admincostpcs = Math.Round(admincostkg * bsheet);
                        ttlfxmarginkg = laborcostkg + admincostkg;
                        ttlfxmarginpcs = Math.Round(ttlfxmarginkg * bsheet);
                        btmpricekg = totvarcostkg + ttlfxmarginkg;
                        btmpricepcs = totvarcostpcs + ttlfxmarginpcs;
                        wrapingkg = wrapingpc;
                        wrapingpcs = Math.Round(wrapingkg * bsheet);
                        palletkg = palletpcs;
                        palletpcs = Math.Round(palletkg * bsheet);
                
                        precostdtl.Add(new QL_mstprecostdtl { precostdtlseq = i, papperpkg = papperpkg, papperppcs = papperppcs, wastekg = wastekg, wastepcs = wastepcs, inkkg=inkkg, inkpcs= inkpcs, otrcemkg= otrcemkg, otrcempcs= otrcempcs, energykg= energykg, energypcs= energypcs, waxpcs= waxpcs, waxkg=waxkg, diecutkg= diecutkg, diecutpcs= diecutpcs, rbrpollykg= rbrpollykg, rbrpollypcs = rbrpollypcs , fc=fc, fcpcs=fcpcs, loadingckg= loadingckg, loadingcpcs = loadingcpcs, strapingckg= strapingckg, strapingcpcs= strapingcpcs, sftymarginkg= sftymarginkg, sftymarginpcs= sftymarginpcs, totvarcostkg= totvarcostkg, totvarcostpcs= totvarcostpcs, laborcostkg= laborcostkg, laborcostpcs= laborcostpcs, admincostkg= admincostkg, admincostpcs= admincostpcs, ttlfxmarginkg= ttlfxmarginkg, ttlfxmarginpcs= ttlfxmarginpcs, btmpricekg= btmpricekg, btmpricepcs= btmpricepcs, wrapingkg= wrapingkg, wrapingpcs= wrapingpcs, palletkg = palletkg , palletpcs = palletpcs, precostdtlrefoid=bomcomp[i].itemoid, precostdtlrefqty=bomcomp[i].qty,precostdtlreftype=bomcomp[i].tipemat, kertascol1lapisan1= kertascol1lapisan1, kertascol1lapisan2= kertascol1lapisan2, kertascol1lapisan3= kertascol1lapisan3, kertascol1lapisan4= kertascol1lapisan4, kertascol1lapisan5= kertascol1lapisan5 });

                        //----------------------------------------------------MST
                        totalberat += bsheet;
                        papperpkgmst += papperpkg;
                        papperppcsmst += papperppcs;
                        wastekgmst += wastekg;
                        wastepcsmst += wastepcs;
                        inkkgmst += inkkg;
                        inkpcsmst += inkpcs;
                        otrcemkgmst += otrcemkg;
                        otrcempcsmst += otrcempcs;
                        energykgmst += energykg;
                        energypcsmst += energypcs;
                        waxpcsmst += waxpcs;
                        waxkgmst += waxkg;
                        diecutkgmst += diecutkg;
                        diecutpcsmst += diecutpcs;
                        rbrpollykgmst += rbrpollykg;
                        rbrpollypcsmst += rbrpollypcs;
                        fcmst += fc;
                        fcpcsmst += fcpcs;
                        loadingckgmst += loadingckg;
                        loadingcpcsmst += loadingcpcs;
                        strapingckgmst += strapingckg;
                        strapingcpcsmst += strapingcpcs;
                        sftymarginkgmst += sftymarginkg;
                        sftymarginpcsmst += sftymarginpcs;
                        totvarcostkgmst += totvarcostkg;
                        totvarcostpcsmst += totvarcostpcs;
                        laborcostkgmst += laborcostkg;
                        laborcostpcsmst += laborcostpcs;
                        admincostkgmst += admincostkg;
                        admincostpcsmst += admincostpcs;
                        ttlfxmarginkgmst += ttlfxmarginkg;
                        ttlfxmarginpcsmst += ttlfxmarginpcs;
                        btmpricekgmst += btmpricekg;
                        btmpricepcsmst += btmpricepcs;
                        wrapingkgmst += wrapingkg;
                        wrapingpcsmst += wrapingpcs;
                        palletkgmst += palletkg;
                        palletpcsmst += palletpcs;


                    }
                    Session["QL_mstprecostdtl"] = precostdtl;
                    decimal temwax = waxpcsmst;
                    papperpkgmst = papperpkgmst / totalberat;
                    wastekgmst = wastekgmst / totalberat;
                    inkkgmst = inkkgmst / totalberat;
                    otrcemkgmst = otrcemkgmst / totalberat;
                    energykgmst = energykgmst / totalberat;
                    waxpcsmst = waxkgmst / totalberat;
                    waxkgmst = temwax;
                    diecutkgmst = diecutkgmst / totalberat;
                    rbrpollykgmst = rbrpollykgmst / totalberat;
                    loadingckgmst = loadingckgmst / totalberat;
                    wrapingkgmst = wrapingkgmst / totalberat;
                    palletkgmst = palletkgmst / totalberat;
                    strapingckgmst = strapingckgmst / totalberat;
                    sftymarginkgmst = sftymarginkgmst / totalberat;
                    totvarcostkgmst = totvarcostkgmst / totalberat;
                    laborcostkgmst = laborcostkgmst / totalberat;
                    admincostkgmst = admincostkgmst / totalberat;
                    ttlfxmarginkgmst = laborcostkgmst + admincostkgmst;
                    btmpricekgmst = btmpricekgmst / totalberat;
                    totvarcostkgmst = palletkgmst + wastekgmst + inkkgmst + otrcemkgmst + energykgmst + waxkgmst + diecutkgmst + rbrpollykgmst + freightcost + laborcostkgmst + wrapingkgmst + palletkgmst + strapingckgmst + sftymarginkgmst;
                    totvarcostpcsmst = totvarcostkgmst * totalberat;
                    precostmst.Add(new QL_mstprecost {  papperpkg = papperpkgmst, papperppcs = papperppcsmst, wastekg = wastekgmst, wastepcs = wastepcsmst, inkkg = inkkgmst, inkpcs = inkpcsmst, otrcemkg = otrcemkgmst, otrcempcs = otrcempcsmst, energykg = energykgmst, energypcs = energypcsmst, waxpcs = waxpcsmst, waxkg = waxkgmst, diecutkg = diecutkgmst, diecutpcs = diecutpcsmst, rbrpollykg = rbrpollykgmst, rbrpollypcs = rbrpollypcsmst, freightcostpcs = fcpcsmst, loadingckg = loadingckgmst, loadingcpcs = loadingcpcsmst, strapingckg = strapingckgmst, strapingcpcs = strapingcpcsmst, sftymarginkg = sftymarginkgmst, sftymarginpcs = sftymarginpcsmst, totvarcostkg = totvarcostkgmst, totvarcostpcs = totvarcostpcsmst, laborcostkg = laborcostkgmst, laborcostpcs = laborcostpcsmst, admincostkg = admincostkgmst, admincostpcs = admincostpcsmst, ttlfxmarginkg = ttlfxmarginkgmst, ttlfxmarginpcs = ttlfxmarginpcsmst, btmpricekg = btmpricekgmst, btmpricepcs = btmpricepcsmst, wrapingkg = wrapingkgmst, wrapingpcs = wrapingpcsmst, palletkg = palletkgmst, palletpcs = palletpcsmst });
                    
                }
            }
                   

            return Json(precostmst, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetMatData(string itype)
        {
            List<mstMat> tbl = new List<mstMat>();

            if (itype == "BOM")
            {
                sSql = "select bomoid,i.itemoid,bomcode matcode,itemlongdesc,tipewall, g.gendesc tipeflute, pd,ld,td,pl,ll,tl,sub1,sub2,sub3,isnull(sub4,'') sub4, isnull(sub5,'') sub5, isnull(colour1oid,1700) c1, isnull(colour2oid,1700) c2, isnull(colour3oid,1700) c3,isnull(colour4oid,1700) c4,isnull(colour5oid,1700) c5, panjangsheet,lebarsheet,luassheet,beratpcs beratsheet, (1+ ISNULL(pt1qty,0)+ ISNULL(pt2qty,0) + ISNULL(pt3qty,0) + ISNULL(ly1qty,0) + ISNULL(ly2qty,0) + ISNULL(item2qty,0) + isnull(item3qty,0) ) jmlperlengkapan from ql_mstbillofmat b inner join QL_mstitem i on b.itemoid=i.itemoid inner join QL_mstgen g on i.fluteoid=g.genoid where left(bomcode,1)='M'";
            }
            else
            {
                sSql = "select 0 bomoid,itemoid,itemcode matcode, itemlongdesc, tipewall, g.gendesc tipeflute, pd, ld, td, pl, ll, tl, sub1, sub2, sub3, isnull(sub4,'') sub4, isnull(sub5, '') sub5, isnull(colour1oid, 1700) c1, isnull(colour2oid, 1700) c2, isnull(colour3oid, 1700) c3,isnull(colour4oid, 1700) c4,isnull(colour5oid, 1700) c5, panjangsheet,lebarsheet,luassheet,beratpcs beratsheet,1 jmlperlengkapan from QL_mstitem i inner join QL_mstgen g on i.fluteoid = g.genoid where left(itemcode, 2) = 'BX'";
            }
           
            tbl = db.Database.SqlQuery<mstMat>(sSql).ToList();
            var count = 0;
            for (int i = 0; i < tbl.Count(); i++)
            {
                count = 0;
                if (tbl[i].c1 != 1700)
                {
                    count += 1;
                }
                if (tbl[i].c2 != 1700)
                {
                    count += 1;
                }
                if (tbl[i].c3 != 1700)
                {
                    count += 1;
                }
                if (tbl[i].c4 != 1700)
                {
                    count += 1;
                }
                if (tbl[i].c5 != 1700)
                {
                    count += 1;
                }
                tbl[i].countwarna = count;
            }
                return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<mstcust> tbl = new List<mstcust>();

            sSql = "SELECT custoid, custcode, custname,custaddr, custtype, amortdiecut, amortdies, isnull(g.genother1, 0) as frightcost FROM QL_mstcust c inner join ql_mstgen g on c.freightcostoid=g.genoid WHERE c.cmpcode='" + CompnyCode + "' AND c.activeflag='ACTIVE' AND custoid>0 ORDER BY custname DESC";
            tbl = db.Database.SqlQuery<mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitDDL(QL_mstprecost tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;
            sSql = "select * from ql_mstgen where gengroup='WAX' And cmpcode='" + CompnyCode + "' and activeflag='ACTIVE'";
            var waxoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.waxoid);
            ViewBag.waxoid = waxoid;
            sSql = "select * from ql_mstgen where gengroup='DIECUT MACHINE' And cmpcode='" + CompnyCode + "' and activeflag='ACTIVE'";
            var dcm = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.diecutmachineoid);
            ViewBag.diecutmachineoid = dcm;
            List<QL_mstcurr> tblcurr = new List<QL_mstcurr>();
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            tblcurr = db.Database.SqlQuery<QL_mstcurr>(sSql).ToList();
            var currcode_budget = "";
            for (int i = 0; i < tblcurr.Count; i++)
            {
                currcode_budget += "<option value=\'" + tblcurr[i].curroid + "\'>" + tblcurr[i].currcode + "</option>";
            }
            ViewBag.currcode_budget = currcode_budget;
        }

        [HttpPost]
        public ActionResult BindListItem(string cmpcode)
        {
            JsonResult js = null;
            try
            {
                List<mstitem> tbl = new List<mstitem>();
                sSql = "SELECT i.itemoid, i.itemcode, i.itemlongdesc, 1.0 AS cbf, i.itemnote FROM QL_mstitem i WHERE i.cmpcode='" + CompnyCode + "' AND i.itemoid NOT IN (SELECT pr.itemoid FROM QL_mstprecost pr WHERE pr.cmpcode='" + cmpcode + "') AND i.activeflag='ACTIVE' ORDER BY i.itemcode";
                tbl = db.Database.SqlQuery<mstitem>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception ex)
            {
                js = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

      

    

        [HttpPost]
        public ActionResult GetRateIDR(int curroid)
        {
            JsonResult js = null;
            try
            {
                var cRate = new ClassRate();
                var sDate = ClassFunction.GetServerTime().ToString("MM/dd/yyyy");
                cRate.SetRateValue(curroid, sDate);
                js = Json(cRate.GetRateMonthlyIDRValue, JsonRequestBehavior.AllowGet);
            }
            catch(Exception)
            {
                js = Json(0, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult IsMatExistsInBOM(int precostdtlrefoid, int precostoid, string cmpcode)
        {
            JsonResult js = null;
            try
            {
                int CheckMat = db.Database.SqlQuery<int>("SELECT COUNT(*) FROM QL_mstbomdtl2 bod INNER JOIN QL_mstbom bom ON bom.cmpcode=bod.cmpcode AND bom.bomoid=bod.bomoid WHERE bod.cmpcode='" + cmpcode + "' AND precostdtlrefoid=" + precostdtlrefoid + " AND precostoid=" + precostoid + "").FirstOrDefault();
                js = Json(CheckMat, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                js = Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

     

        private void FillAdditionalField(QL_mstprecost tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("select custname from ql_mstcust where custoid = '" +tbl.custoid+"'").FirstOrDefault(); 
            ViewBag.amortdiecut = Math.Round(db.Database.SqlQuery<decimal>("select amortdiecut from ql_mstcust where custoid = '" + tbl.custoid + "'").FirstOrDefault());
            ViewBag.amortdies = Math.Round(db.Database.SqlQuery<decimal>("select amortdies from ql_mstcust where custoid = '" + tbl.custoid + "'").FirstOrDefault());
            ViewBag.material = db.Database.SqlQuery<string>("select itemlongdesc from ql_mstitem where itemoid = '" + tbl.itemoid + "'").FirstOrDefault();
        }

     

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("PreCosting", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";
            sSql = "SELECT precostoid, pr.cmpcode, precostoid [ID], itemcode [Kode FG], itemlongdesc [Finish Good], CONVERT(VARCHAR(10), pr.updtime, 101) [Date], pr.activeflag [Status], i.itemnote AS [SKU],  divname [Business Unit], 'Print' [Print] FROM QL_mstprecost pr INNER JOIN QL_mstitem i ON i.itemoid=pr.itemoid INNER JOIN QL_mstdivision div ON div.cmpcode=pr.cmpcode ";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "WHERE pr.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "WHERE pr.cmpcode LIKE '%'";
            sSql += " ORDER BY precostoid DESC";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstprecost");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "ID")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["precostoid"].ToString() , "PreCosting") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: precostMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("PreCosting", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstprecost tbl;
            string action = "New Data";
            string SaveAs = "";
            if (id == null)
            {
                tbl = new QL_mstprecost();
                tbl.precostoid = ClassFunction.GenerateID("QL_mstprecost");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.activeflag = "ACTIVE";
                tbl.palletpc = 0;
                tbl.wrapingpc = 0;
                tbl.jmlorder = 0;

                Session["QL_mstprecostdtl"] = null;
            }
            else
            {
                action = "Update Data";
               
                tbl = db.QL_mstprecost.Find(id);

                sSql = "Select * from ql_mstprecostdtl WHERE precostoid = " + id + " AND cmpcode = '" + cmp + "'";
                Session["QL_mstprecostdtl"] = db.Database.SqlQuery<QL_mstprecostdtl>(sSql).ToList();

            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            ViewBag.SaveAs = SaveAs;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dpapMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstprecost tbl, string action, string closing)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("PreCosting", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List< QL_mstprecostdtl> dtDtl = (List<QL_mstprecostdtl>)Session["QL_mstprecostdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please Generate first!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please Generate first!");
            //if (tbl.precostdlcamt == 0)
            //    ModelState.AddModelError("", "DLC COST field must be more than 0!");
            //if (tbl.precostoverheadamt == 0)
            //    ModelState.AddModelError("", "OVERHEAD COST field must be more than 0!");
            //if (tbl.precostsalesprice == 0)
            //    ModelState.AddModelError("", "RECM. SALES PRICE field must be more than 0!");
            //if (cbf == 0)
            //    ModelState.AddModelError("", "CBF field must be more than 0!");
            if (tbl.tipewall=="SW")
            {
                tbl.sub4 = "";
                tbl.sub5 = "";
            }
            
           
            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_MSTPRECOST");
                var dtloid = ClassFunction.GenerateID("QL_MSTPRECOSTDTL");
                var servertime = ClassFunction.GetServerTime();


                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //if (SaveAs != "")
                        //    action = "New Data";

                        if (action == "New Data")
                        {
                            if (db.QL_mstprecost.Find(tbl.precostoid) != null)
                                tbl.precostoid = mstoid;
                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_mstprecost.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.precostoid + " WHERE tablename='QL_MSTPRECOST'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstprecostdtl.Where(a => a.precostoid == tbl.precostoid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstprecostdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_mstprecostdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            
                            tbldtl = new QL_mstprecostdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.precostdtloid = dtloid++;
                            tbldtl.precostoid = tbl.precostoid;
                            tbldtl.precostdtlseq = i + 1;
                            tbldtl.precostdtlrefoid = dtDtl[i].precostdtlrefoid;
                            tbldtl.precostdtlrefqty = dtDtl[i].precostdtlrefqty;
                            tbldtl.precostdtlreftype = dtDtl[i].precostdtlreftype;
                            tbldtl.precostdtlstatus = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.papperpkg = dtDtl[i].papperpkg;
                            tbldtl.papperppcs = dtDtl[i].papperppcs;
                            tbldtl.wastekg = dtDtl[i].wastekg;
                            tbldtl.wastepcs = dtDtl[i].wastepcs;
                            tbldtl.inkkg = dtDtl[i].inkkg;
                            tbldtl.inkpcs = dtDtl[i].inkpcs;
                            tbldtl.otrcemkg = dtDtl[i].otrcemkg;
                            tbldtl.otrcempcs = dtDtl[i].otrcempcs;
                            tbldtl.energykg = dtDtl[i].energykg;
                            tbldtl.energypcs = dtDtl[i].energypcs;
                            tbldtl.waxkg = dtDtl[i].waxkg;
                            tbldtl.waxpcs = dtDtl[i].waxpcs;
                            tbldtl.diecutkg = dtDtl[i].diecutkg;
                            tbldtl.diecutpcs = dtDtl[i].diecutpcs;
                            tbldtl.rbrpollykg = dtDtl[i].rbrpollykg;
                            tbldtl.rbrpollypcs = dtDtl[i].rbrpollypcs;
                            tbldtl.loadingckg = dtDtl[i].loadingckg;
                            tbldtl.loadingcpcs = dtDtl[i].loadingcpcs;
                            tbldtl.strapingckg = dtDtl[i].strapingckg;
                            tbldtl.strapingcpcs = dtDtl[i].strapingcpcs;
                            tbldtl.sftymarginkg = dtDtl[i].sftymarginkg;
                            tbldtl.sftymarginpcs = dtDtl[i].sftymarginpcs;
                            tbldtl.totvarcostkg = dtDtl[i].totvarcostkg;
                            tbldtl.totvarcostpcs = dtDtl[i].totvarcostpcs;
                            tbldtl.laborcostkg = dtDtl[i].laborcostkg;
                            tbldtl.laborcostpcs = dtDtl[i].laborcostpcs;
                            tbldtl.admincostkg = dtDtl[i].admincostkg;
                            tbldtl.admincostpcs = dtDtl[i].admincostpcs;
                            tbldtl.ttlfxmarginkg = dtDtl[i].ttlfxmarginkg;
                            tbldtl.ttlfxmarginpcs = dtDtl[i].ttlfxmarginpcs;
                            tbldtl.totalbtmpricekg = dtDtl[i].totalbtmpricekg;
                            tbldtl.totalbtmpricepcs = dtDtl[i].totalbtmpricepcs;
                            tbldtl.btmpricekg = dtDtl[i].btmpricekg;
                            tbldtl.btmpricepcs = dtDtl[i].btmpricepcs;
                            tbldtl.kertascol1lapisan1 = dtDtl[i].kertascol1lapisan1;
                            tbldtl.kertascol1lapisan2 = dtDtl[i].kertascol1lapisan2;
                            tbldtl.kertascol1lapisan3 = dtDtl[i].kertascol1lapisan3;
                            tbldtl.kertascol1lapisan4 = dtDtl[i].kertascol1lapisan4;
                            tbldtl.kertascol1lapisan5 = dtDtl[i].kertascol1lapisan5;
                            tbldtl.fc = dtDtl[i].fc;
                            tbldtl.fcpcs = dtDtl[i].fcpcs;
                            tbldtl.wrapingkg = dtDtl[i].wrapingkg;
                            tbldtl.wrapingpcs = dtDtl[i].wrapingpcs;
                            tbldtl.palletkg = dtDtl[i].palletkg;
                            tbldtl.palletpcs = dtDtl[i].palletpcs;

                            db.QL_mstprecostdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_mstprecostdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        objTrans.Commit();


                            return RedirectToAction("Index");
                    }
                    catch(Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("PreCosting", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstprecost tbl = db.QL_mstprecost.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else
            {
                if (ClassFunction.CheckDataExists("SELECT COUNT(*) FROM QL_mstbom WHERE cmpcode='" + tbl.cmpcode + "' AND precostoid=" + tbl.precostoid))
                {
                    result = "failed";
                    msg = "This data can't be deleted because it is being used by another data!";
                }
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstprecostdtl.Where(a => a.precostoid == id && a.cmpcode == cmp);
                        db.QL_mstprecostdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_mstprecost.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("PreCosting", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var servertime = ClassFunction.GetServerTime().ToString("MM/dd/yyyy");
            if (printtype == "")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPreCosting.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPreCostingExcel.rpt"));

            report.SetParameterValue("sWhere", " WHERE pr.cmpcode='" + cmp + "' AND pr.precostoid=" + id);
            report.SetParameterValue("sSetDate", servertime);
            report.SetParameterValue("PrintUserID", Session["UserID"]);
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "PreCostingPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}