﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers
{
    public class PositionController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];

        // GET: Position
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View(db.QL_mstlevel.ToList());
        }

        public ActionResult Form(int? id )
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var cmp = CompnyCode;
            QL_mstlevel qL_mstlevel;
            string action = "New Data";
            if (id == null | cmp == null)
            {

                qL_mstlevel = new QL_mstlevel();
                qL_mstlevel.createuser = Session["UserID"].ToString();
                qL_mstlevel.createtime = ClassFunction.GetServerTime();

            }
            else
            {
                action = "Update Data";
                qL_mstlevel = db.QL_mstlevel.Find(cmp, id);


            }
           
            if (qL_mstlevel == null)
            {
                return HttpNotFound();
            }
            
            InitAllDDL(qL_mstlevel);
            ViewBag.action = action;
            return View(qL_mstlevel);

        }

        // POST: Position/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form( QL_mstlevel qL_mstlevel,string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            qL_mstlevel.updtime = ClassFunction.GetServerTime();
            qL_mstlevel.upduser = Session["UserID"].ToString();
            int mstoid = db.QL_mstlevel.Any() ? db.QL_mstlevel.Max(o => o.leveloid) + 1 : 1;
            if (ModelState.IsValid & IsInputValid(qL_mstlevel))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {

                    try
                    {
                        //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor
                      
                        if (action == "New Data")
                        {

                            qL_mstlevel.cmpcode = Session["CompnyCode"].ToString();
                            qL_mstlevel.leveloid = mstoid;
                            qL_mstlevel.createuser = Session["UserID"].ToString();
                            qL_mstlevel.createtime = ClassFunction.GetServerTime();
                           
                            db.QL_mstlevel.Add(qL_mstlevel);
                            db.SaveChanges();
                        }
                        else
                        {
                           
                            qL_mstlevel.cmpcode = Session["CompnyCode"].ToString();
                            

                            db.Entry(qL_mstlevel).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }

            }
            ViewBag.action = action;
            InitAllDDL(qL_mstlevel);
            return View(qL_mstlevel);
        }

      
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id,string cmp)
        {
            QL_mstlevel list = db.QL_mstlevel.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstlevel.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void InitAllDDL(QL_mstlevel qL_mstlevel)
        {
            ViewBag.levelmineduoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "EDUCATION").ToList(), "genoid", "gendesc");
        }
        public bool IsInputValid(QL_mstlevel qL_mstlevel)
        {
            bool isValid = true;

            if (qL_mstlevel.levelcode == null | qL_mstlevel.levelcode == "")
            {
                ModelState.AddModelError("levelcode", "Please fill Code");
            }
            else
            {
                if (db.QL_mstlevel.Where(w => w.levelcode == qL_mstlevel.levelcode & w.leveloid != qL_mstlevel.leveloid).Count() > 0)
                {
                    ModelState.AddModelError("levelcode", "Level Code has been used by another data. Please fill another level code");
                }
            }
            if (qL_mstlevel.leveldesc == null | qL_mstlevel.leveldesc == "")
            {
                ModelState.AddModelError("leveldesc", "Please fill Description");
            }

            if (db.QL_mstlevel.Where(w => w.leveldesc == qL_mstlevel.leveldesc & w.leveloid != qL_mstlevel.leveloid).Count() > 0)
            {
                ModelState.AddModelError("leveldesc", "Level Description has been used by another data. Please fill another level description");
            }
            if(qL_mstlevel.levelmineduoid == null)
            {
                ModelState.AddModelError("levelmineduoid", "Please fill Min Education");
            }
            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
    }
}
