﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers
{
    public class VehicleController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        // GET: Vehicle
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstvehicle qL_mstvehicle = new QL_mstvehicle();
           
            return View(db.QL_mstvehicle.ToList());
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstvehicle qL_mstvehicle;
            var cmp = CompnyCode;
            string action = "New Data";
            if (id == null )
            {


                qL_mstvehicle = new QL_mstvehicle();
                qL_mstvehicle.cmpcode = Session["CompnyCode"].ToString();

                qL_mstvehicle.createuser = Session["UserID"].ToString();
                qL_mstvehicle.createtime = ClassFunction.GetServerTime();
                qL_mstvehicle.vhclicexpdate = ClassFunction.GetServerTime();
                qL_mstvehicle.vhccode = generateNo();
            }
            else
            {
                action = "Update Data";
                qL_mstvehicle = db.QL_mstvehicle.Find(cmp, id);
                //qL_mstdiv.divtotalperson = 0;

            }

            if (qL_mstvehicle == null)
            {
                return HttpNotFound();
            }

            InitAllDDL(qL_mstvehicle);
            ViewBag.action = action;
            return View(qL_mstvehicle);

        }

        // POST: Vehicle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstvehicle qL_mstvehicle, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            if (qL_mstvehicle.vhclicexpdate <= ClassFunction.GetServerTime())
            {
                ModelState.AddModelError("vhclicexpdate", "The license expired date must more than now");
            }
            int mstoid = db.QL_mstvehicle.Any() ? db.QL_mstvehicle.Max(o => o.vhcoid) + 1 : 1;
            if (ModelState.IsValid & IsInputValid(qL_mstvehicle))
            {

                using (var objTrans = db.Database.BeginTransaction())
                {

                    try
                    {
                        //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor

                        if (action == "New Data")
                        {

                            qL_mstvehicle.vhcmodeloid = 0;
                            qL_mstvehicle.vhctypeoid = 0;
                            qL_mstvehicle.vhcoid = mstoid;
                            qL_mstvehicle.vhccode = generateNo();
                            qL_mstvehicle.createuser = Session["UserID"].ToString();
                            qL_mstvehicle.createtime = ClassFunction.GetServerTime();
                            qL_mstvehicle.upduser= Session["UserID"].ToString();
                            qL_mstvehicle.updtime = ClassFunction.GetServerTime();
                            

                            db.QL_mstvehicle.Add(qL_mstvehicle);
                            db.SaveChanges();
                        }
                        else
                        {

                            db.Entry(qL_mstvehicle).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }

            }


            ViewBag.action = action;
            InitAllDDL(qL_mstvehicle);
            return View(qL_mstvehicle);
        }

        // GET: Vehicle/Edit/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstvehicle list = db.QL_mstvehicle.Find(cmp, id);

            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstvehicle.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        private void InitAllDDL(QL_mstvehicle qL_mstvehicle)
        {
            ViewBag.vhctypeoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "VEHICLE TYPE").ToList(), "genoid", "gendesc");
            ViewBag.vhcmodeloid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "VEHICLE MODEL").ToList(), "genoid", "gendesc");
            ViewBag.drivoid = new SelectList(db.QL_mstdriver.Where(w => w.activeflag == "ACTIVE").ToList(), "drivoid", "drivname");

        }
        private string generateNo()//UNTUK MENGGENERATE NOMER FORM SCR OTOMATIS
        {
            string sNo = "VHC";
            //int formatCounter = 4;
            string sSql = "SELECT ISNULL(MAX(CAST(RIGHT(vhccode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstvehicle WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND vhccode LIKE '" + sNo + "%'";

            string sCounter = "0000" + db.Database.SqlQuery<int>(sSql).FirstOrDefault().ToString();
            sCounter = sCounter.Substring(sCounter.Length - 4);

            sNo = sNo + sCounter;
            return sNo;
        }
        public bool IsInputValid(QL_mstvehicle qL_mstvehicle)
        {
            bool isValid = true;
            //if(qL_mstvehicle.vhcmodeloid == 0)
            //{
            //    ModelState.AddModelError("vhcmodeloid", "Please fill Vehicle Model");
            //}
           
            if(qL_mstvehicle.drivoid == 0)
            {
                ModelState.AddModelError("drivoid", "Please fill Default Driver ");
            }
            if(qL_mstvehicle.vhcdesc== null | qL_mstvehicle.vhcdesc == "")
            {
                ModelState.AddModelError("vhcdesc", "Please fill Description");
            }
            if (qL_mstvehicle.vhcno == null | qL_mstvehicle.vhcno == "")
            {
                ModelState.AddModelError("vhcno", "Please fill Vehicle No.");
            }

            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
