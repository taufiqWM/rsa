﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Master
{
    public class SparepartController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string imgpath = "~/Images/SparepartImages";
        private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        public class sparepart
        {
            public string cmpcode { get; set; }
            public int sparepartoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string sparepartcode { get; set; }
            public string sparepartunit { get; set; }
            public string sparepartdesc { get; set; }
            public string spareparttype { get; set; }
            public string activeflag { get; set; }

        }
        public class suppdtl
        {
            public int sparepartdtloid { get; set; }
            public int sparepartoid { get; set; }
            public int suppoid { get; set; }
            public int suppdtl2oid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string picname { get; set; }
            public string activeflag { get; set; }
          
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class mstsparepartlocation
        {
            public int locoid { get; set; }
            public string locdesc { get; set; }
        }

        private void InitDDL(QL_mstsparepart tbl)
        {
            var sqlPlus = "";
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + tbl.cmpcode + "' AND cat1shortdesc='Sparepart' AND activeflag='ACTIVE' ORDER BY cat1shortdesc";
            var cat1oid = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1oid", "cat1shortdesc", tbl.cat1oid);
            ViewBag.cat1oid = cat1oid;

            //CAT2
            if (tbl.cat1oid > 0)
            {
                sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1oid=" + tbl.cat1oid + " AND activeflag='ACTIVE' ORDER BY cat2shortdesc";
            }
            else
            {
                sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1oid=" + cat1oid.First().Value + " AND activeflag='ACTIVE' ORDER BY cat2shortdesc";
            }
            var cat2oid = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2oid", "cat2shortdesc", tbl.cat2oid);
            ViewBag.cat2oid = cat2oid;

            //CAT3
            if (tbl.cat1oid > 0)
            {
                sqlPlus += " AND cat1oid=" + tbl.cat1oid;
            }
            else
            {
                sqlPlus += " AND cat1oid=" + cat1oid.First().Value;
            }

            if (tbl.cat2oid > 0)
            {
                sqlPlus += " AND cat2oid=" + tbl.cat2oid;
            }
            else
            {
                sqlPlus += " AND cat2oid=" + cat2oid.First().Value;
            }
            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "'" + sqlPlus + " AND  activeflag='ACTIVE' ORDER BY cat3shortdesc";
            var cat3oid = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3oid", "cat3shortdesc", tbl.cat3oid);
            ViewBag.cat3oid = cat3oid;

            //CAT4
            sqlPlus = "";
            if (tbl.cat1oid > 0)
            {
                sqlPlus += " AND cat1oid=" + tbl.cat1oid;
            }
            else
            {
                sqlPlus += " AND cat1oid=" + cat1oid.First().Value;
            }

            if (tbl.cat2oid > 0)
            {
                sqlPlus += " AND cat2oid=" + tbl.cat2oid;
            }
            else
            {
                sqlPlus += " AND cat2oid=" + cat2oid.First().Value;
            }

            if (tbl.cat3oid > 0)
            {
                sqlPlus += " AND cat3oid=" + tbl.cat3oid;
            }
            else
            {
                sqlPlus += " AND cat3oid=" + cat3oid.First().Value;
            }            

            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'SATUAN'";
            var sparepartunitoid= new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.sparepartunitoid);
            ViewBag.sparepartunitoid = sparepartunitoid;

            //WAREHOUSE / LOCATION
            sSql = "SELECT genoid [locoid], gendesc [locdesc] FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='GUDANG' AND gendesc='GUDANG BARANG' AND activeflag='ACTIVE'";
            var sparepartlocoid = new SelectList(db.Database.SqlQuery<mstsparepartlocation>(sSql).ToList(), "locoid", "locdesc", tbl.sparepartlocoid);
            ViewBag.sparepartlocoid = sparepartlocoid;

            //SUPPLIER
            sSql = "SELECT ISNULL(suppname, '') [suppname] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid;
            ViewBag.suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }
        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            sSql = "SELECT i.*, i.sparepartlongdesc [sparepartdesc], g.gendesc [sparepartunit], c1.cat1shortdesc, c2.cat2shortdesc, c3.cat3shortdesc FROM QL_mstsparepart i Inner Join QL_mstcat3 c3 on i.cat3oid=c3.cat3oid inner join ql_mstcat1 c1 on i.cat1oid=c1.cat1oid inner join ql_mstcat2 c2 on i.cat2oid=c2.cat2oid inner join QL_mstgen g ON g.genoid=i.sparepartunitoid WHERE c3.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY cat3oid DESC";
            List<sparepart> vbag = db.Database.SqlQuery<sparepart>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsparepart tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstsparepart();
                tbl.sparepartoid = ClassFunction.GenerateID("QL_mstsparepart");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstsparepart.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstsparepart tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            //List<suppdtl> dtDtl = (List<suppdtl>)Session["QL_suppdtl"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Please fill detail data!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill detail data!");
            if (tbl.sparepartcode == null)
                ModelState.AddModelError("sparepartcode", "Kode barang HARUS DIISI");
            if (tbl.sparepartlongdesc == null)
                ModelState.AddModelError("sparepartlongdesc", "Nama Panjang HARUS DIISI.");
            if (tbl.suppoid == 0)
                ModelState.AddModelError("suppoid", "Supplier HARUS DIISI.");
            if (tbl.sparepartsafetystock == 0)
                ModelState.AddModelError("sparepartsafetystock", "Stok aman HARUS DIISI.");
            if (tbl.sparepartlimitqty == 0)
                ModelState.AddModelError("sparepartlimitqty", "Round Qty HARUS DIISI.");
            if (tbl.sparepartlocoid == 0)
                ModelState.AddModelError("sparepartlocoid", "Lokasi gudang HARUS DIISI.");

            var mstoid = ClassFunction.GenerateID("QL_mstsparepart");
            if (action == "Update Data")
            {
                mstoid = tbl.sparepartoid;
            }
            //var dtloid = ClassFunction.GenerateID("QL_mstsparepartdtl");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.sparepartshortdesc = tbl.sparepartshortdesc == null ? "" : tbl.sparepartshortdesc;
                        tbl.sparepartbarcode = "";

                        tbl.sparepartavgsales = 0;
                        tbl.sparepartlastpoprice = 0;
                        tbl.sparepartavgsales = 0;
                        tbl.sparepartavgpurchase = 0;                       
                        tbl.sparepartnote = tbl.sparepartnote == null ? "" : tbl.sparepartnote;
                        tbl.sparepartres1 = "";
                        tbl.sparepartres2 = tbl.sparepartres2 == null ? "0" : tbl.sparepartres2;
                        tbl.sparepartsku = tbl.sparepartsku == null ? "0" : tbl.sparepartsku;
                        tbl.sparepartres3 = "";
                        tbl.sparepartunit2oid = 0;
                        tbl.sparepartconvertunit = 0;
                        tbl.sparepartavgpurchase = tbl.sparepartavgpurchase == null ? 0 : tbl.sparepartavgpurchase;
                        tbl.sparepartlastpoprice = tbl.sparepartlastpoprice == null ? 0 : tbl.sparepartlastpoprice;
                        tbl.sparepartno = tbl.sparepartno == null ? "" : tbl.sparepartno;
                        tbl.sparepartinterchange = tbl.sparepartinterchange == null ? "" : tbl.sparepartinterchange;

                        //Upload
                        //sparepartpictureloc
                        if (System.IO.File.Exists(Server.MapPath(tbl.sparepartpictureloc)))
                        {
                            var sExt = Path.GetExtension(tbl.sparepartpictureloc);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "Imagessparepart_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.sparepartpictureloc) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.sparepartpictureloc), sdir + "/" + sfilename);
                            }

                            tbl.sparepartpictureloc = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.sparepartpictureloc = tbl.sparepartpictureloc == null ? "" : tbl.sparepartpictureloc;
                        }

                        //sparepartpicturemat
                        if (System.IO.File.Exists(Server.MapPath(tbl.sparepartpicturemat)))
                        {
                            var sExt = Path.GetExtension(tbl.sparepartpicturemat);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesSparepart_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.sparepartpicturemat) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.sparepartpicturemat), sdir + "/" + sfilename);
                            }

                            tbl.sparepartpicturemat = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.sparepartpicturemat = tbl.sparepartpicturemat == null ? "" : tbl.sparepartpicturemat;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.sparepartoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstsparepart.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_id SET lastoid=" + mstoid + " WHERE /*cmpcode='" + CompnyCode + "' AND*/ tablename='QL_mstsparepart'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //var trndtl = db.QL_mstsparepartdtl.Where(a => a.sparepartoid == tbl.sparepartoid && a.cmpcode == tbl.cmpcode);
                            //db.QL_mstsparepartdtl.RemoveRange(trndtl);
                            //db.SaveChanges();
                        }

                        //QL_mstsparepartdtl tbldtl;
                        //for (int i = 0; i < dtDtl.Count(); i++)
                        //{
                        //    tbldtl = new QL_mstsparepartdtl();
                        //    tbldtl.cmpcode = tbl.cmpcode;
                        //    tbldtl.sparepartdtloid = dtloid++;
                        //    tbldtl.sparepartoid = tbl.sparepartoid;
                        //    tbldtl.suppoid = dtDtl[i].suppoid;
                        //    tbldtl.picoid = dtDtl[i].suppdtl2oid;
                        //    tbldtl.upduser = tbl.upduser;
                        //    tbldtl.updtime = tbl.updtime;
                         
                        //    db.QL_mstsparepartdtl.Add(tbldtl);
                        //}

                        //sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_mstsparepartdtl'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstsparepart tbl = db.QL_mstsparepart.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstsparepart.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            List<QL_mstcat2> objCat2 = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' ORDER BY cat2shortdesc";
            objCat2 = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objCat2, "cat2oid", "cat2shortdesc", 0);

            return Json(DDLCat2);
        }
        public ActionResult Getcat3(int cat1, int cat2)
        {
            List<QL_mstcat3> objcity = new List<QL_mstcat3>();
            objcity = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2).ToList();
            SelectList obgcity = new SelectList(objcity, "cat3oid", "cat3shortdesc", 0);
            return Json(obgcity);
        }        

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<suppdtl> tbl = new List<suppdtl>();

            sSql = "SELECT s.cmpcode, s.suppoid,suppcode, suppname, s.activeflag, (select top 1 x.suppdtl2oid from QL_mstsuppdtl2 x where x.cmpcode=s.cmpcode and x.suppoid=s.suppoid ) suppdtl2oid, (select top 1 x.suppdtl2picname from QL_mstsuppdtl2 x where x.cmpcode=s.cmpcode and x.suppoid=s.suppoid ) picname FROM QL_mstsupp s WHERE s.cmpcode='" + cmp + "' and activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupp(string cmp)
        {
            List<mstsupp> cust = new List<mstsupp>();
            sSql = "SELECT cu.*, ci.gendesc [CityDesc] FROM QL_mstsupp cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=suppcityOid WHERE cu.cmpcode='" + cmp + "' AND UPPER(cu.activeflag)='ACTIVE'";
            cust = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetsuppData(List<suppdtl> dtDtl)
        {
            Session["QL_suppdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilsuppData()
        {
            if (Session["QL_suppdtl"] == null)
            {
                Session["QL_suppdtl"] = new List<suppdtl>();
            }

            List<suppdtl> dataDtl = (List<suppdtl>)Session["QL_suppdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileMat()
        {
            var result = "";
            var picturepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        picturepath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, picturepath);
            }
            result = "Sukses";

            return Json(new { result, picturepath }, JsonRequestBehavior.AllowGet);
        }
    }
}
