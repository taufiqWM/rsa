﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class ServiceController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstservice
        {
            public string cmpcode { get; set; }
            public int serviceoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string servicecode { get; set; }
            public string serviceunit { get; set; }
            public string serviceshortdesc { get; set; }
            public string servicesupp { get; set; }
            public string activeflag { get; set; }
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class listcoa
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        private void InitDDL(QL_mstservice tbl)
        {
            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'MATERIAL UNIT' AND activeflag='ACTIVE'";
            var serviceunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.serviceunitoid);
            ViewBag.serviceunitoid = serviceunitoid;

            //SUPPLIER
            sSql = "SELECT ISNULL(suppname, '') [suppname] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid;
            ViewBag.suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            //COA
            string[] sVar = { "VAR_SERVICE_EXPENSE" };
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            var serviceacctgoid = new SelectList(db.Database.SqlQuery<listcoa>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.serviceacctgoid);
            ViewBag.serviceacctgoid = serviceacctgoid;
        }

        [HttpPost]
        public ActionResult GetSupp(string cmp)
        {
            List<mstsupp> cust = new List<mstsupp>();
            sSql = "SELECT cu.*, ci.gendesc [CityDesc] FROM QL_mstsupp cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=suppcityOid WHERE cu.cmpcode='" + cmp + "' AND UPPER(cu.activeflag)='ACTIVE'";
            cust = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            List<QL_mstcat2> objCat2 = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' ORDER BY cat2shortdesc";
            objCat2 = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objCat2, "cat2oid", "cat2shortdesc", 0);

            return Json(DDLCat2);
        }
        public ActionResult Getcat3(int cat1, int cat2)
        {
            List<QL_mstcat3> objcity = new List<QL_mstcat3>();
            objcity = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2).ToList();
            SelectList obgcity = new SelectList(objcity, "cat3oid", "cat3shortdesc", 0);
            return Json(obgcity);
        }

        // GET: Service
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT i.*, g.gendesc [serviceunit], s.suppname [servicesupp] FROM QL_mstservice i inner join ql_mstgen g ON g.genoid=i.serviceunitoid inner join QL_mstsupp s ON s.suppoid=i.suppoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY i.serviceshortdesc DESC";
            List<mstservice> vbag = db.Database.SqlQuery<mstservice>(sSql).ToList();
            return View(vbag);
        }

        // GET: Service/Form
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstservice tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstservice();
                tbl.serviceoid = ClassFunction.GenerateID("QL_mstservice");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstservice.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }
        private string generateNo()
        {
            var sNo = "SCV-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(servicecode, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstservice WHERE cmpcode='" + CompnyCode + "' AND servicecode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 6);
            sNo = sNo + sCounter;

            return sNo;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstservice tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            
            
            if (tbl.servicelongdesc == null)
                ModelState.AddModelError("servicelongdesc", "Nama Panjang HARUS DIISI.");
            if (tbl.suppoid == 0)
                ModelState.AddModelError("suppoid", "Supplier HARUS DIISI.");
            tbl.servicecat1oid = 0;
            tbl.servicecat2oid = 0;
            tbl.servicecat3oid = 0;
           
            var mstoid = ClassFunction.GenerateID("QL_mstservice");
            if (action == "Update Data")
            {
                mstoid = tbl.serviceoid;
            }
            //var dtloid = ClassFunction.GenerateID("QL_mstservicedtl");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.serviceshortdesc = tbl.serviceshortdesc == null ? "" : tbl.serviceshortdesc;
                        tbl.servicenote = tbl.servicenote == null ? "" : tbl.servicenote;
                        tbl.serviceres1 = "";
                        tbl.serviceres2 = tbl.serviceres2 == null ? "0" : tbl.serviceres2;
                        tbl.serviceres3 = tbl.serviceres3 == null ? "0" : tbl.serviceres3;
                        //tbl.servicecat2oid = tbl.servicecat2oid == null ? 0 : tbl.servicecat2oid;
                        //tbl.servicecat3oid = tbl.servicecat3oid == null ? 0 : tbl.servicecat3oid;
                        //tbl.cat4oid = tbl.cat4oid == null ? 0 : tbl.cat4oid;

                        
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.serviceoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.servicecode = generateNo();
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstservice.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstservice'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Service/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            QL_mstservice qL_mstservice = db.QL_mstservice.Find(id);
            db.QL_mstservice.Remove(qL_mstservice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
