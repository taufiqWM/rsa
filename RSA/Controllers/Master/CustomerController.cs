﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Master
{
    public class CustomerController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounter"];
        private string sSql = "";
        
        public class custmst
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string divgroup { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custcity { get; set; }
            public string custprovince { get; set; }
            public string custtype { get; set; }
            public string custphone1 { get; set; }
            public string custphone2 { get; set; }
            public string custfax1 { get; set; }
            public string custfax2 { get; set; }
            public string custemail { get; set; }
            public string custstatus { get; set; }
        }
        public class mstgen
        {
            public int oid { get; set; }
            public string sdesc { get; set; }
        }

        public class mstdiv
        {
            public string code { get; set; }
            public string sdesc { get; set; }
        }

        private string generateNo()
        {
            var sNo = "CC-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custcode, 7) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 7);
            sNo = sNo + sCounter;

            return sNo;
        }
        private void InitDDL(QL_mstcust tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid,"genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' "+sqlplus+"";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PREFIX NAME' AND activeflag='ACTIVE'";
            var custprefixoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.custprefixoid);
            ViewBag.custprefixoid = custprefixoid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='SEKTOR INDUSTRI' AND activeflag='ACTIVE'";
            var custres1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.custres1);
            ViewBag.custres1 = custres1;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            //var custcurroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.custcurroid);
            //ViewBag.custcurroid = custcurroid;
            sSql = "select * from QL_mstperson p inner join QL_mstlevel l on p.leveloid=l.leveloid and leveldesc='SALES' where p.cmpcode='" + CompnyCode + "' order by personname";
            var salesoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personname", "personname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var custpaymentoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.custpaymentoid);
            ViewBag.custpaymentoid = custpaymentoid;

            var iCustCountry = "";
            var iCustProvince = "";
            var iCustCity = 0;
            if (tbl.custcityoid != 0)
            {
                iCustCountry = db.Database.SqlQuery<string>("SELECT genother2 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + tbl.custcityoid + " ORDER BY gendesc").FirstOrDefault();
                iCustProvince = db.Database.SqlQuery<string>("SELECT genother1 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + tbl.custcityoid + " ORDER BY gendesc").FirstOrDefault();
                iCustCity = tbl.custcityoid;
            }
            else
            {
                iCustCity = db.Database.SqlQuery<int>("SELECT top 1 genoid FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "'  AND gengroup='CITY' ORDER BY (CASE genother2 WHEN (SELECT genoid FROM QL_mstgen WHERE gengroup='COUNTRY' AND gendesc like '%INDONESIA%') THEN 1 ELSE 2 END)").FirstOrDefault();
                iCustCountry = db.Database.SqlQuery<string>("SELECT genother2 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + iCustCity + " ORDER BY gendesc").FirstOrDefault();
                iCustProvince = db.Database.SqlQuery<string>("SELECT genother1 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + iCustCity + " ORDER BY gendesc").FirstOrDefault();
            }

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genother1='" + iCustProvince + "' AND genother2='" + iCustCountry + "' ORDER BY gendesc";
            var custcityoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", iCustCity);
            ViewBag.custcityoid = custcityoid;

            var custprovince = db.QL_mstgen.Where(x => x.genother1 == iCustCountry && x.gengroup=="PROVINCE")
                  .Select(x => new SelectListItem
                  {
                      Value = x.genoid.ToString(),
                      Text = x.gencode + " - " + x.gendesc.ToString()
                  });


            //sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND genother1='" + iCustCountry + "' ORDER BY gendesc";
            ViewBag.custprovince = new SelectList(custprovince, "Value", "Text", iCustProvince);

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY' AND activeflag='ACTIVE'  ORDER BY gendesc";
            var custcountry = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", iCustCountry);
            ViewBag.custcountry = custcountry;

            //cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'

            var custres3 = db.QL_mstcat1.Where(x => x.cmpcode == CompnyCode && x.activeflag == "ACTIVE")
                .Select(x => new SelectListItem
                {
                    Value = x.cat1shortdesc.ToString(),
                    Text = x.cat1res1 + " - " + x.cat1shortdesc.ToString()
                });
            if (string.IsNullOrEmpty(tbl.custres3))
            {
                ViewBag.res3 = new SelectList(custres3, "Value", "Text", null);
            }
            else
            {
                var x = tbl.custres3.Split(';');
                ViewBag.res3 = new SelectList(custres3, "Value", "Text", x);
            }
      
            sSql = " SELECT genoid oid, gendesc sdesc FROM (SELECT 0 genoid, 'None' gendesc UNION ALL SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CUSTOMER WAREHOUSE' AND activeflag='ACTIVE') AS tbl  ORDER BY (CASE genoid WHEN 0 then 1 ELSE 2 END), gendesc ASC ";
            //var custwhoid = new SelectList(db.Database.SqlQuery<mstgen>(sSql).ToList(), "oid", "sdesc", tbl.custwhoid);
            //ViewBag.custwhoid = custwhoid;
            //var custwhoid2 = new SelectList(db.Database.SqlQuery<mstgen>(sSql).ToList(), "oid", "sdesc", tbl.custwhoid2);
            //ViewBag.custwhoid2 = custwhoid2;

            sSql = " SELECT divcode code, divname sdesc FROM (SELECT '' divcode, 'None' divname UNION ALL SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE') AS tbl  ORDER BY (CASE divcode WHEN '' then 1 ELSE 2 END), divname ASC ";

            //var custdivcode = new SelectList(db.Database.SqlQuery<mstdiv>(sSql).ToList(), "code", "sdesc", tbl.custdivcode);
            //ViewBag.custdivcode = custdivcode;
           

        }
        private void FillAdditionalField(QL_mstcust tbl)
        {
            ViewBag.Freight = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid='" + tbl.freightcostoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLProvince(string custcountry)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND genother1='" + custcountry + "' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLCity(string custcountry, string custprovince)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genother1='" + custprovince +"' AND genother2='" + custcountry +  "' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFgData()
        {
            List<QL_mstgen> tbl = new List<QL_mstgen>();

            sSql = "SELECT * from ql_mstgen s WHERE s.cmpcode='" + CompnyCode + "' AND s.gengroup='Freight Cost' AND s.activeflag='ACTIVE' ORDER BY gendesc DESC";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "s", divgroupoid,"divgroupoid");
            sSql = "SELECT S.CustOid [custoid], S.CustCode [custcode], custtype [custtype], S.CustName [custname], S.CustAddr [custaddr], C.gendesc [custcity], p.gendesc [custprovince], S.CustPhone1 [custphone1], S.CustPhone2 [custphone2], S.CustFax1 [custfax1], S.CustFax2 [custfax2], S.CustEmail [custemail], S.activeflag [custstatus], (select x.gendesc from ql_mstgen x where x.genoid=s.divgroupoid) divgroup FROM QL_mstCust S INNER JOIN QL_mstgen C ON (C.cmpcode='" + CompnyCode + "' AND C.gengroup='CITY' AND C.genoid=S.CustCityOid AND C.activeflag='ACTIVE') INNER JOIN QL_mstgen p ON CAST(c.genother1 AS INT)=p.genoid AND p.gengroup='PROVINCE' WHERE s.cmpcode='" + CompnyCode + "' "+sqlplus;
            List<custmst> vbag = db.Database.SqlQuery<custmst>(sSql).ToList();
            return View(vbag);
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";
            sSql = "SELECT S.CustOid [oid], S.CustCode [Code], custtype [Type], S.CustName [Customer], S.CustAddr [Address], C.gendesc [City], S.CustPhone1 [Phone 1], S.CustPhone2 [Phone 2], S.CustFax1 [Fax 1], S.CustFax2 [Fax 2], S.CustEmail [Email] FROM QL_mstCust S INNER JOIN QL_mstgen C ON (C.cmpcode='" + CompnyCode + "' AND C.gengroup='CITY' AND C.genoid=S.CustCityOid AND C.activeflag='ACTIVE') WHERE s.cmpcode='" + CompnyCode + "'"; 

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstcust");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Code")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "Customer") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }


        // GET: Customer/Form/5/11
        public ActionResult Form(int? id)
        {

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string cmp = CompnyCode;
            QL_mstcust tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstcust();
                tbl.custoid = ClassFunction.GenerateID("QL_mstcust");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstcust.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: Customer/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcust tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var mstoid = ClassFunction.GenerateID("QL_mstcust");
            var servertime = ClassFunction.GetServerTime();

            var ss = tbl.custres3;
            if (string.IsNullOrEmpty(tbl.custname))
                ModelState.AddModelError("", "Please fill NAME field! !");
            else if (db.QL_mstcust.Where(w => w.custname == tbl.custname & tbl.divgroupoid==w.divgroupoid & w.custoid != tbl.custoid).Count() > 0)
                ModelState.AddModelError("", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if (tbl.custcreditlimit <= 0)
                ModelState.AddModelError("", "Please fill Credit Limit field! !");
            if (string.IsNullOrEmpty(tbl.custaddr))
                ModelState.AddModelError("", "Please fill ADDRESS field! !");
            if (tbl.custtaxable == 1)
            {
                if (string.IsNullOrEmpty(tbl.custnpwp))
                    ModelState.AddModelError("", "Please fill NPWP field! !");
            }
            else
            {
                if (tbl.custnpwp == "" || tbl.custnpwp == null)
                    tbl.custnpwp = "00.000.000.0-000.000";
            }

            int iCode = db.Database.SqlQuery<int>("SELECT COUNT(-1) FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custcode='" + tbl.custcode + "' AND custoid<>" + tbl.custoid).FirstOrDefault();
            if (iCode > 0)
                ModelState.AddModelError("", "CODE has been used by another data. Please fill another CODE !");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.rptfilenamesummary = tbl.rptfilenamesummary == null ? "" : tbl.rptfilenamesummary;
                        tbl.rptfilenamedetail = tbl.rptfilenamedetail == null ? "" : tbl.rptfilenamedetail;

                        if (action == "New Data")
                        {
                            if (db.QL_mstcust.Find(CompnyCode, tbl.custoid) != null)
                                tbl.custoid = mstoid;

                            tbl.cmpcode = CompnyCode;
                            tbl.custcode = generateNo();
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_mstcust.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.custoid + " WHERE tablename='QL_mstcust'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: PRRawMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcust tbl = db.QL_mstcust.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.QL_mstcust.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string stype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (stype == "PDF")
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCust.rpt"));
            else
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCustExcel.rpt"));

            string sWhere = " ORDER BY custtype, custoid ";
            report.SetParameterValue("sWhere", sWhere);
            report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
           
            if (stype == "PDF")
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "MasterCustomer.pdf");
            }
            else
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/excel", "MasterCustomer.xls");
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
