﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class BPSheetController : Controller
    {
        // GET: BPSheet
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_mstbpsheet WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and bpsheetoid>0 ORDER BY bpsheetoid DESC";
            List<Listbpsheet> vbag = db.Database.SqlQuery<Listbpsheet>(sSql).ToList();
            return View(vbag);
        }

        private string generateNo()
        {
            var sNo = "SS-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(bpsheetcode, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstbpsheet WHERE cmpcode='" + CompnyCode + "' AND bpsheetcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 3);
            sNo = sNo + sCounter;

            return sNo;
        }

        private List<QL_mstacctg> BindListCOA(string cmp, string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, cmp) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return tbl;
        }

        public class listmat : QL_mstmatraw
        {
            public string matrawunit { get; set; }
        }

        [HttpPost]
        public ActionResult GetMaterialData()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT m.*, g.gendesc matrawunit FROM QL_mstmatraw m inner join QL_mstgen g on g.genoid=m.matrawunitoid inner join QL_mstcat1 c1 on c1.cat1oid=m.cat1oid WHERE m.activeflag='ACTIVE' and c1.cat1shortdesc='BAHAN PENOLONG' ORDER BY matrawcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbpsheet tbl;
            if (id == null)
            {
                tbl = new QL_mstbpsheet();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_mstbpsheet.Find(id);
                ViewBag.action = "Edit";
            }

            ViewBag.borax_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.borax_acctgoid);
            ViewBag.soda_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.soda_acctgoid);
            ViewBag.tapioka_rb_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.tapioka_rb_acctgoid);
            ViewBag.tapioka_rb2_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.tapioka_rb2_acctgoid);
            ViewBag.tapioka_sg_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.tapioka_sg_acctgoid);
            ViewBag.soda_fl_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.soda_fl_acctgoid);
            ViewBag.cms_acctgoid = new SelectList(BindListCOA(Session["CompnyCode"].ToString(), "VAR_EXPENSE").ToList(), "acctgoid", "acctgdesc", tbl.cms_acctgoid);

            if (tbl == null)
            {
                return HttpNotFound();
            }
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstbpsheet tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            //is Input Valid 
            if (db.QL_mstbpsheet.Where(w => w.flute == tbl.flute & w.bpsheetoid != tbl.bpsheetoid).Count() > 0) msg += "- Flute yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Flute lainnya!<br>";

            if (tbl.borax_matrawoid == 0) msg += "- Silahkan Pilih Borax!<br>";
            if (tbl.soda_matrawoid == 0) msg += "- Silahkan Pilih SB-100 (SODA)!<br>";
            if (tbl.tapioka_rb_matrawoid == 0) msg += "- Silahkan Pilih Tepung Tapioka Rosebrand!<br>";
            //if (tbl.tapioka_rb2_matrawoid == 0) msg += "- Silahkan Pilih Tepung Tapioka Rosebrand 2!<br>";
            //if (tbl.tapioka_sg_matrawoid == 0) msg += "- Silahkan Pilih Tepung Tapioka SG!<br>";
            if (tbl.soda_fl_matrawoid == 0) msg += "- Silahkan Pilih Caustic Sodaflakes!<br>";
            if (tbl.cms_matrawoid == 0) msg += "- Silahkan Pilih CMS-431!<br>";

            //var total_persen = tbl.borax_persen + tbl.soda_persen + tbl.tapioka_rb_persen + tbl.tapioka_rb2_persen + tbl.tapioka_sg_persen + tbl.soda_fl_persen + tbl.cms_persen;
            //if (total_persen != 100) msg += "- Total Persen harus 100!<br>";

            if (msg == "")
            {
                if (action == "Create") tbl.bpsheetcode = generateNo();
                var mstoid = ClassFunction.GenerateID("QL_mstbpsheet");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.bpsheetoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            //tbl.bpsheetlongdesc = "";
                            db.QL_mstbpsheet.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + mstoid + " WHERE tablename='QL_mstbpsheet'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstbpsheet tbl = db.QL_mstbpsheet.Find(id);
                        db.QL_mstbpsheet.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class Listbpsheet
        {
            public int bpsheetoid { get; set; }
            public string bpsheetcode { get; set; }
            public string flute { get; set; }
            public string activeflag { get; set; }

        }
    }
}