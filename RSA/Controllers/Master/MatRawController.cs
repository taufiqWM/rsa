﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Master
{
    public class MatRawController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string imgpath = "~/Images/RawImages";
        private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class matraw
        {
            public string cmpcode { get; set; }
            public int matrawoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string matrawcode { get; set; }
            public string matrawunit { get; set; }
            public string matrawdesc { get; set; }
            public string matrawtype { get; set; }
            public string activeflag { get; set; }

        }

        public class suppdtl
        {
            public int matrawdtloid { get; set; }
            public int matrawoid { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string picname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
            public string activeflag { get; set; }
          
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class mstmanufacture
        {
            public int manufactureoid { get; set; }
            public string manufacturecode { get; set; }
            public string manufacturename { get; set; }
            public string manufactureaddr { get; set; }
            public string citydesc { get; set; }
            public string manufacturephone1 { get; set; }
        }

        public class mstmatrawlocation
        {
            public int locoid { get; set; }
            public string locdesc { get; set; }
        }

        private string generateNo()
        {
            var sNo = "RM-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matrawcode, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstmatraw WHERE cmpcode='" + CompnyCode + "' AND matrawcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }

        private void InitDDL(QL_mstmatraw tbl, string action)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            var sqlPlus = "";
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' And cat1res1='Raw' ORDER BY cat1shortdesc";
            var cat1oid = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1oid", "cat1shortdesc", tbl.cat1oid);
            ViewBag.cat1oid = cat1oid;

            //cat2
            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1oid='" + tbl.cat1oid + "' AND activeflag='ACTIVE' ORDER BY cat2shortdesc";         
            var cat2oid = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2oid", "cat2shortdesc", tbl.cat2oid);
            ViewBag.cat2oid = cat2oid;

            //CAT3
            sqlPlus += " AND cat1oid='" + tbl.cat1oid +"'";
            sqlPlus += " AND cat2oid='" + tbl.cat2oid + "'";
            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "'" + sqlPlus + " AND  activeflag='ACTIVE' ORDER BY cat3shortdesc";
            var cat3oid = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3oid", "cat3shortdesc", tbl.cat3oid);
            if (action == "Update Data" && tbl.cat3oid == 0) cat3oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>("SELECT 0 ifield, '' sfield").ToList(), "ifield", "sfield");
            ViewBag.cat3oid = cat3oid;

            //CAT4
            sqlPlus += " AND cat3oid='" + tbl.cat3oid + "'"; 
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "'" + sqlPlus + " AND activeflag ='ACTIVE' ORDER BY cat4shortdesc";
            var cat4oid = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4oid", "cat4shortdesc", tbl.cat4oid);
            if (action == "Update Data" && tbl.cat4oid == 0) cat4oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>("SELECT 0 ifield, '' sfield").ToList(), "ifield", "sfield");
            ViewBag.cat4oid = cat4oid;

            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'MATERIAL UNIT'";
            var matrawunitoid= new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.matrawunitoid);
            ViewBag.matrawunitoid = matrawunitoid;

            //WAREHOUSE / LOCATION
            sSql = "SELECT genoid [locoid], gendesc [locdesc] FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE'";
            var matrawlocoid = new SelectList(db.Database.SqlQuery<mstmatrawlocation>(sSql).ToList(), "locoid", "locdesc", tbl.matrawlocoid);
            ViewBag.matrawlocoid = matrawlocoid;

            //SUPPLIER
            //if(tbl.suppoid == null) { tbl.suppoid = 0; }
            //sSql = "SELECT ISNULL(suppname, '') [suppname] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid;
            //ViewBag.suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "i", divgroupoid, "divgroupoid");

            sSql = "SELECT i.*, i.matrawlongdesc [matrawdesc], g.gendesc [matrawunit], c1.cat1shortdesc, c2.cat2shortdesc, c3.cat3shortdesc FROM QL_mstmatraw i /*inner join QL_mstcat4 c4 on i.cat4oid=c4.cat4oid*/ Inner Join QL_mstcat3 c3 on i.cat3oid=c3.cat3oid inner join ql_mstcat1 c1 on i.cat1oid=c1.cat1oid inner join ql_mstcat2 c2 on i.cat2oid=c2.cat2oid inner join ql_mstgen g ON g.genoid=i.matrawunitoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "'"+sqlplus+" AND i.matrawres3='' ORDER BY cat4oid DESC";
            List<matraw> vbag = db.Database.SqlQuery<matraw>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstmatraw tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstmatraw();
                tbl.matrawoid = ClassFunction.GenerateID("QL_mstmatraw");
                tbl.manufactureoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = CompnyCode;
                tbl.matrawsafetystock = 1;
                tbl.matrawlimitqty = 1;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstmatraw.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstmatraw tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            //is Input Valid 
            if (string.IsNullOrEmpty(tbl.matrawlongdesc))
                msg += "- Nama Panjang HARUS DIISI!<br>";
            else tbl.matrawlongdesc = tbl.matrawlongdesc.ToUpper();
            if (string.IsNullOrEmpty(tbl.matrawshortdesc))
                msg += "- Lebar/Deskripsi HARUS DIISI!<br>";
            else tbl.matrawshortdesc = tbl.matrawshortdesc.ToUpper();
            if (tbl.matrawsafetystock == 0)
                msg += "- Stok aman HARUS DIISI!";
            if (tbl.matrawlimitqty == 0)
                msg += "- Round Qty HARUS DIISI!";
            if (tbl.matrawlocoid == 0)
                msg += "- Lokasi gudang HARUS DIISI!";

            tbl.matrawlength = 0;
            tbl.matrawwidth = 0;
            tbl.matrawheight = 0;
            tbl.matrawdiameter = 0;
            tbl.matrawvolume = 0;
            tbl.matrawbarcode = "";
            tbl.manufactureoid = 0;
            tbl.suppoid = 0;
            tbl.matrawavgsales = 0;
            tbl.matrawlastpoprice = 0;
            tbl.matrawavgsales = 0;
            tbl.matrawavgpurchase = 0;
            tbl.matrawnote = tbl.matrawnote ?? "";
            tbl.matrawres1 = "";
            tbl.matrawres2 = tbl.matrawres2 ?? "0";
            tbl.matrawres3 = "";
            tbl.matrawunit2oid = 0;
            tbl.matrawconvertunit = 0;

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_mstmatraw");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Upload
                        //matrawpictureloc
                        if (System.IO.File.Exists(Server.MapPath(tbl.matrawpictureloc)))
                        {
                            var sExt = Path.GetExtension(tbl.matrawpictureloc);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesRaw_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matrawpictureloc) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matrawpictureloc), sdir + "/" + sfilename);
                            }

                            tbl.matrawpictureloc = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matrawpictureloc = tbl.matrawpictureloc == null ? "" : tbl.matrawpictureloc;
                        }

                        //matrawpicturemat
                        if (System.IO.File.Exists(Server.MapPath(tbl.matrawpicturemat)))
                        {
                            var sExt = Path.GetExtension(tbl.matrawpicturemat);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesRawMat_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matrawpicturemat) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matrawpicturemat), sdir + "/" + sfilename);
                            }

                            tbl.matrawpicturemat = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matrawpicturemat = tbl.matrawpicturemat == null ? "" : tbl.matrawpicturemat;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.matrawoid = mstoid;
                            tbl.matrawcode = generateNo();
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstmatraw.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstmatraw'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstmatraw tbl = db.QL_mstmatraw.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstmatraw.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' And cat1res1='Raw' ORDER BY cat2shortdesc";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
         

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Getcat3(int cat1,int cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            tbl = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid==cat1 && g.cat2oid == cat2).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Getcat4(int cat1, int cat2, int cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            tbl = db.QL_mstcat4.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2 && g.cat3oid == cat3).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<suppdtl> tbl = new List<suppdtl>();

            sSql = "SELECT matrawdtloid, matrawoid, s.cmpcode, s.suppoid,suppcode, suppname, s.activeflag, ISNULL(cperson1, '') [picname] FROM QL_mstsupp s INNER JOIN QL_mstmatrawdtl r ON r.cmpcode=s.cmpcode AND r.suppoid=s.suppoid WHERE s.cmpcode='" + cmp + "' and activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupp(string cmp)
        {
            List<suppdtl> cust = new List<suppdtl>();
            //sSql = "SELECT cu.*, ci.gendesc [CityDesc] FROM QL_mstsupp cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=suppcityOid WHERE cu.cmpcode='" + cmp + "' AND UPPER(cu.activeflag)='ACTIVE'";
            sSql = "SELECT suppoid, suppcode, suppname, ISNULL(cperson1, '') picname, suppaddr, gendesc [CityDesc], suppphone1, s.activeflag FROM QL_mstsupp s INNER JOIN QL_mstgen g ON g.cmpcode = s.cmpcode AND g.genoid = s.suppcityOid WHERE s.cmpcode = '" + cmp + "' AND s.activeflag = 'ACTIVE' ORDER BY suppname";
            cust = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetManufacture()
        {
            List<mstmanufacture> manufacture = new List<mstmanufacture>();
            sSql = "SELECT cu.manufactureoid, cu.manufacturecode, cu.manufacturename, cu.manufactureaddr, ci.gendesc [citydesc], cu.manufacturephone1 FROM QL_mstmanufacture cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=manufacturecityOid WHERE cu.cmpcode='" + CompnyCode + "' AND UPPER(cu.activeflag)='ACTIVE'";
            manufacture = db.Database.SqlQuery<mstmanufacture>(sSql).ToList();

            return Json(manufacture, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetsuppData(List<suppdtl> dtDtl)
        {
            Session["QL_mstmatrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FilsuppData()
        {
            if (Session["QL_mstmatrawdtl"] == null)
            {
                Session["QL_mstmatrawdtl"] = new List<suppdtl>();
            }

            List<suppdtl> dataDtl = (List<suppdtl>)Session["QL_mstmatrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileMat()
        {
            var result = "";
            var picturepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        picturepath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, picturepath);
            }
            result = "Sukses";

            return Json(new { result, picturepath }, JsonRequestBehavior.AllowGet);
        }
    }
}
