﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class UserRoleController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET/POST: UserRole
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("UserRole", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (filter != null)
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " And " + filter.ddlfilter + " like '%" + filter.txtfilter + "%'";
            }

            string sSql = "SELECT ur.cmpcode, ur.profoid, us.profname, COUNT(*) total from QL_mstuserrole ur INNER JOIN QL_mstprof us ON us.profoid=ur.profoid WHERE ur.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " group by ur.cmpcode, ur.profoid, us.profname";
            var qL_mstuserrole = db.Database.SqlQuery<listuserrole>(sSql).ToList();
            return View(qL_mstuserrole);
        }

        // GET: UserRole/Form/5
        public ActionResult Form(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("UserRole", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstuserrole qL_mstuserrole;
            if (id == null)
            {
                qL_mstuserrole = new QL_mstuserrole();
                qL_mstuserrole.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
                Session["QL_mstuserrole"] = null;
            }
            else
            {
                qL_mstuserrole = db.QL_mstuserrole.Where(w => w.profoid == id).FirstOrDefault();
                ViewBag.action = "Edit";
                //string sSql = "select ur.rloid, rl.rlname, ur.urflag from QL_mstuserrole ur inner join QL_mstuserrole rl ON rl.rloid=ur.rloid where ur.userroleoid='" + id + "'";
                string sSql = "select ur.roleoid, rl.rolename, ur.special from QL_mstuserrole ur inner join QL_mstrole rl ON rl.roleoid=ur.roleoid where ur.profoid='" + id + "'";
                Session["QL_mstuserrole"] = db.Database.SqlQuery<userrole>(sSql).ToList();
            }
            
            if (qL_mstuserrole == null)
            {
                return HttpNotFound();
            }

            setViewBag(qL_mstuserrole);
            return View(qL_mstuserrole);
        }

        // POST: UserRole/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstuserrole qL_mstuserrole, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("UserRole", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<userrole> dataDtl = (List<userrole>)Session["QL_mstuserrole"];
            if (dataDtl == null)
                ModelState.AddModelError("", "Please fill User Role Detail");
            else if (dataDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill User Role Detail");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstuserrole");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                        }
                        else if (action == "Edit")
                        {
                            //delete old
                            var trndtl = db.QL_mstuserrole.Where(a => a.profoid == qL_mstuserrole.profoid);
                            db.QL_mstuserrole.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        //insert
                        for (int i = 0; i < dataDtl.Count(); i++)
                        {
                            sSql = "INSERT INTO QL_mstuserrole (cmpcode, userroleoid, profoid, roleoid, special, createuser, createtime, upduser, updtime) VALUES ('" + qL_mstuserrole.cmpcode + "', "+ (mstoid++) + ", '"+ qL_mstuserrole.profoid + "', "+ dataDtl[i].roleoid + ", '"+ dataDtl[i].special + "', '" + Session["UserID"].ToString() + "', CURRENT_TIMESTAMP, '" + Session["UserID"].ToString() + "', CURRENT_TIMESTAMP)";
                            db.Database.ExecuteSqlCommand(sSql);
                            //QL_mstuserrole UR = new QL_mstuserrole();
                            //UR.cmpcode = qL_mstuserrole.cmpcode;
                            //UR.userroleoid = qL_mstuserrole.userroleoid;
                            //UR.uroid = mstoid++;
                            //UR.rloid = dataDtl[i].rloid;
                            //UR.urflag = dataDtl[i].urflag;
                            //db.QL_mstuserrole.Add(UR);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_mstoid set lastoid = " + mstoid + " Where tablename = 'QL_mstuserrole'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }

            setViewBag(qL_mstuserrole);
            ViewBag.action = action;
            return View(qL_mstuserrole);
        }

        private void setViewBag(QL_mstuserrole qL_mstuserrole)
        {

            sSql = " SELECT * FROM QL_mstprof WHERE ( activeflag = 'ACTIVE' AND profoid not in (select x.profoid from QL_mstuserrole x)) OR profoid = '" + qL_mstuserrole.profoid + "'";
            ViewBag.profoid = new SelectList(db.Database.SqlQuery<QL_mstprof>(sSql), "profoid", "profoid", qL_mstuserrole.profoid);
            ViewBag.roleoid = new SelectList(db.QL_mstrole, "roleoid", "rolename");
        }

        public ActionResult getQL_mstuserrole()
        {
            if (Session["QL_mstuserrole"] == null)
            {
                Session["QL_mstuserrole"] = new List<userrole>();
            }

            List<userrole> dataDtl = (List<userrole>)Session["QL_mstuserrole"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_mstuserrole(List<userrole> mdataDtl)
        {
            Session["QL_mstuserrole"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // POST: UserRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("m02RL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    var userroles = db.QL_mstuserrole.Where(a => a.profoid == id);
                    db.QL_mstuserrole.RemoveRange(userroles);
                    db.SaveChanges();
                    // Oh we are here, looks like everything is fine - save all the data permanently
                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    // roll back all database operations, if any thing goes wrong
                    objTrans.Rollback();

                    result = "failed";
                    msg += ex.ToString();
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public class userrole
        {
            public int roleoid { get; set; }
            public string rolename { get; set; }
            public string special { get; set; }
        }

        public class listuserrole
        {
            public string cmpcode { get; set; }
            public string profoid { get; set; }
            public string profname { get; set; }
            public int total { get; set; }
        }
    }
}
