﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class SupplierController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class suppmst
        {
            public string cmpcode { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string divgroup { get; set; }
            public string suppaddr { get; set; }
            public string suppphone { get; set; }
            public string suppemail { get; set; }
            public String suppname { get; set; }
            public string activeflag { get; set; }            
        }

        public class suppdtl1
        {            
            public int suppdtl1seq { get; set; }
            public string suppdtl1addr { get; set; }
            public int suppcityoid { get; set; }
            public string suppdtl1phone { get; set; }
            public string suppdtl1status { get; set; }
        }

        private void InitDDL(QL_mstsupp tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

        }

        //[HttpPost]
        //public ActionResult InitDDLDepartment(string cmpcode)
        //{
        //    var result = "sukses";
        //    var msg = "";
        //    List<QL_mstsupp> tbl = new List<QL_mstsupp>();
        //    sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
        //    tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

        //    return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int reqoid)
        {
            List<suppdtl1> tbl = new List<suppdtl1>();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<suppdtl1> dtDtl)
        {
            Session["QL_mstsuppdtl1"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstsuppdtl1"] == null)
            {
                Session["QL_mstsuppdtl1"] = new List<suppdtl1>();
            }

            List<suppdtl1> dataDtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: Supplier
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Supplier", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");            
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "s", divgroupoid, "divgroupoid");
            sSql = "SELECT *, (select x.gendesc from ql_mstgen x where x.genoid=s.divgroupoid) divgroup FROM QL_mstsupp s WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + sqlplus +"ORDER BY suppoid DESC";
            List<suppmst> vbag = db.Database.SqlQuery<suppmst>(sSql).ToList();
            return View(vbag);
        }

        // GET: Supplier/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Supplier", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsupp tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstsupp();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.suppcode = generateCode();
                tbl.suppprovinceoid = db.QL_mstgen.Where(o => o.gengroup == "PROVINCE").FirstOrDefault().genoid;
                //tbl.suppcityOid = db.QL_mstgen.Where(o => o.gengroup == "CITY" && o.genother1 == tbl.suppprovinceoid.ToString()).FirstOrDefault().genoid;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                //Session["QL_mstsuppdtl1"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstsupp.Find(Session["CompnyCode"].ToString(), id);

                //string sSql = "SELECT sd1.suppdtl1seq,sd1.suppdtl1addr, sd1.suppcityoid, sd1.suppdtl1phone, sd1.suppdtl1status FROM QL_mstsuppdtl1 sd1 where suppoid =" + tbl.suppoid;
                //Session["QL_mstsuppdtl1"] = db.Database.SqlQuery<suppdtl1>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            SetViewBag(tbl);
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Supplier/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstsupp tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Supplier", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //List<suppdtl1> dt1dtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            //if (dt1dtl == null)
            //    ModelState.AddModelError("", "Please fill Detail");
            //else if (dt1dtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill Detail");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.suppcode))
                ModelState.AddModelError("suppcode", "Silahkan isi Kode!");
            else if (db.QL_mstsupp.Where(w => w.suppcode == tbl.suppname & w.suppoid != tbl.suppoid).Count() > 0)
                ModelState.AddModelError("suppcode", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.suppcode))
                ModelState.AddModelError("suppname", "Silahkan isi Nama!");
            else if (db.QL_mstsupp.Where(w => w.suppname == tbl.suppname & tbl.divgroupoid==w.divgroupoid & w.suppoid != tbl.suppoid).Count() > 0)
                ModelState.AddModelError("suppname", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if(string.IsNullOrEmpty(tbl.suppprovinceoid.ToString()))
                ModelState.AddModelError("suppprovinceoid", "Provinsi harus diisi!");
            if (string.IsNullOrEmpty(tbl.suppaddr))
                tbl.suppaddr = "";
            if (string.IsNullOrEmpty(tbl.supppostcode))
                tbl.supppostcode = "";
            if (string.IsNullOrEmpty(tbl.suppphone1))
                tbl.suppphone1 = "";
            if (string.IsNullOrEmpty(tbl.suppphone2))
                tbl.suppphone2 = "";
            if (string.IsNullOrEmpty(tbl.suppfax1))
                tbl.suppfax1 = "";
            if (string.IsNullOrEmpty(tbl.suppemail))
                tbl.suppemail = "";
            if (string.IsNullOrEmpty(tbl.suppgaransi))
                tbl.suppgaransi = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpno))
                tbl.suppnpwpno = "";
            if (string.IsNullOrEmpty(tbl.suppnpwp))
                tbl.suppnpwp = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpaddr))
                tbl.suppnpwpaddr = "";
            if (string.IsNullOrEmpty(tbl.suppktpimg))
                tbl.suppktpimg = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpimg))
                tbl.suppnpwpimg = "";
            if (string.IsNullOrEmpty(tbl.suppnote))
                tbl.suppnote = "";
            if (string.IsNullOrEmpty(tbl.cperson1))
                tbl.cperson1 = "";
            if (string.IsNullOrEmpty(tbl.cperson2))
                tbl.cperson2 = "";
            if (string.IsNullOrEmpty(tbl.cphone1))
                tbl.cphone1 = "";
            if (string.IsNullOrEmpty(tbl.cphone2))
                tbl.cphone2 = "";
            tbl.pengakuanh = "";
            tbl.supppengakuan = "";
            tbl.suppsurgan = "";
            tbl.suppgaransi = "";

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstsupp");

            if (ModelState.IsValid)
            {
                //var kntrdtloid = ClassFunction.GenerateID("QL_mstsuppdtl1");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.suppoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;                            
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstsupp.Add(tbl);
                            db.SaveChanges();                            

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE tablename='QL_mstsupp'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //delete detail
                            //var kntrdtl = db.QL_mstsuppdtl1.Where(a => a.suppoid == tbl.suppoid);
                            //db.QL_mstsuppdtl1.RemoveRange(kntrdtl);
                            //db.SaveChanges();
                        }

                        //insert detail
                        //QL_mstsuppdtl1 tbldtl1;
                        //for (int i = 0; i < dt1dtl.Count(); i++)
                        //{
                        //    tbldtl1 = new QL_mstsuppdtl1();
                        //    tbldtl1.cmpcode = tbl.cmpcode;
                        //    tbldtl1.suppdtl1seq = i++;
                        //    tbldtl1.suppdtl1oid = kntrdtloid++;
                        //    tbldtl1.suppoid = tbl.suppoid;
                        //    tbldtl1.suppdtl1addr = dt1dtl[i].suppdtl1addr;
                        //    tbldtl1.suppcityoid = dt1dtl[i].suppcityoid;
                        //    tbldtl1.suppdtl1phone = dt1dtl[i].suppdtl1phone;
                        //    tbldtl1.suppdtl1status = dt1dtl[i].suppdtl1status;
                        //    tbldtl1.updtime = tbl.updtime;
                        //    tbldtl1.upduser = tbl.upduser;
                        //    db.QL_mstsuppdtl1.Add(tbldtl1);
                        //}
                        //db.SaveChanges();
                        ////Update lastoid
                        //sSql = "Update QL_mstoid set lastoid = " + kntrdtloid + " Where tablename = 'QL_mstsuppdtl1'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            SetViewBag(tbl);
            ViewBag.action = action;            
            return View(tbl);
            //ViewBag(QL_mstsuppdtl1);
        }

        // POST: Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission("Supplier", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsupp tbl = db.QL_mstsupp.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //var trndtl = db.QL_mstsuppdtl1.Where(a => a.suppoid == id);
                        //db.QL_mstsuppdtl1.RemoveRange(trndtl);
                        //db.SaveChanges();

                        db.QL_mstsupp.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private void SetViewBag(QL_mstsupp qL_mstsupp)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.divgroupoid);

            if (qL_mstsupp == null) qL_mstsupp = new QL_mstsupp();
            string sqlPlus = "";

            //NATION
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY'";
            var suppnationoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.suppnationoid);
            ViewBag.suppnationoid = suppnationoid;

            //PROVINCE
            if(qL_mstsupp.suppnationoid > 0)
            {
                sqlPlus = " AND genother1='" + qL_mstsupp.suppnationoid.ToString() + "'";
            }
            else
            {
                sqlPlus = " AND genother1='" + suppnationoid.First().Value.ToString() + "'";
            }
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND gendesc!='-'" + sqlPlus;
            var suppprovinceoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.suppprovinceoid);
            ViewBag.suppprovinceoid = suppprovinceoid;

            if (qL_mstsupp.suppprovinceoid > 0)
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND activeflag='ACTIVE' AND genother1='" + qL_mstsupp.suppprovinceoid.ToString() + "'";
                var suppcityOid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.suppcityOid);
                ViewBag.suppcityOid = suppcityOid;
            }
            else
            {
                var iOid = "";
                if(suppprovinceoid.Count() == 0) //suppprovinceoid.First().Value.ToString()
                {
                    sSql = "SELECT CAST(genoid AS VARCHAR) FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND gendesc='JAWA TIMUR'";
                    iOid = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                }
                else
                {
                    iOid = suppprovinceoid.First().Value.ToString();
                }
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND activeflag='ACTIVE' AND genother1='" + iOid.ToString() + "'";
                var suppcityOid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.suppcityOid);
                ViewBag.suppcityOid = suppcityOid;
            }

            //PAYMENT TYPE
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var supppaymentoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstsupp.supppaymentoid);
            ViewBag.supppaymentoid = supppaymentoid;
        }

        [HttpPost]
        public ActionResult GetProvince(int nationoid)
        {
            List<QL_mstgen> objProvince = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND genother1='" + nationoid.ToString() + "'";
            objProvince = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList obgProvince = new SelectList(objProvince, "genoid", "gendesc", 0);
            return Json(obgProvince);
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_mstgen> objcity = new List<QL_mstgen>();
            objcity = db.QL_mstgen.Where(g => g.gengroup == "CITY" && g.activeflag == "ACTIVE" && g.genother1 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "genoid", "gendesc", 0);
            return Json(obgcity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult getQL_mstsuppdtl1()
        {
            if (Session["QL_mstsuppdtl1"] == null)
            {
                Session["QL_mstsuppdtl1"] = new List<suppdtl1>();
            }

            List<suppdtl1> dataDtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_mstsuppdtl1(List<suppdtl1> mdataDtl)
        {
            Session["QL_mstsuppdtl1"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string generateCode()
        {
            string sCode = "SUPP-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND SuppCode LIKE '" + sCode + "%'";
            
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }
    }
}
