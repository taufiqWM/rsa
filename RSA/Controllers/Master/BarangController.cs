﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Master
{
    public class BarangController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        //private string imgpath = "~/Images/ItemImages";
        private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        public class item
        {
            public string cmpcode { get; set; }
            public string divgroup { get; set; }
            public int itemoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string itemcode { get; set; }
            public string itemunit { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string boxtype { get; set; }
            public string activeflag { get; set; }

        }
        public class suppdtl
        {
            public int itemdtloid { get; set; }
            public int itemoid { get; set; }
            public int suppoid { get; set; }
            public int suppdtl2oid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string picname { get; set; }
            public string activeflag { get; set; }
          
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class mstitemlocation
        {
            public int locoid { get; set; }
            public string locdesc { get; set; }
        }

        public class filterprintout
        {
            public int itemoid { get; set; }
            public string itemlongdesc { get; set; }
            public string itemcode { get; set; }
            public string box { get; set; }
            public string djflag { get; set; }
            public string dctype { get; set; }
        }

        public class listbarang
        {
            public string itemcode { get; set; }
            public string itemshortdesc { get; set; }
            public string itemlongdesc { get; set; }
            public string color1 { get; set; }
            public string color2 { get; set; }
            public string color3{ get; set;}
            public string color4 { get; set; }
            public string color5 { get; set; }
            public string substance { get; set; }
            public string tipebox { get; set; }
            public string tipeflute { get; set; }
            public string tipejoint { get; set; }
            public decimal pl { get; set; }
            public decimal ll { get; set; }
            public decimal tl { get; set; }
            public decimal pd { get; set; }
            public decimal ld { get; set; }
            public decimal td { get; set; }
            public decimal p1 { get; set; }
            public decimal p2 { get; set; }
            public decimal l1 { get; set; }
            public decimal l2 { get; set; }
            public decimal flap1 { get; set; }
            public decimal flap2 { get; set; }
            public decimal luassheet { get; set; }
            public decimal panjangsheet { get; set; }
            public decimal lebarsheet { get; set; }
            public decimal lidah { get; set; }
            public decimal beratpcs { get; set; }
            public string nodiecut { get; set; }
            public string noplat { get; set; }
            public string itemnote { get; set; }
            public string itemnote2 { get; set; }
            public string itemnote3 { get; set; }
            public decimal jmlstitch { get; set; }
        }

        public class wlap
        {
            public decimal berat1 { get; set; }
            public decimal berat2 { get; set; }
            public decimal berat3 { get; set; }
            public decimal berat4 { get; set; }
            public decimal berat5 { get; set; }
        }

        [HttpPost]
        public ActionResult GetWarnaData(string action, int divgroupoid)
        {
            var result = "sukses";
            var msg = "";
            var divgroup = db.QL_mstgen.FirstOrDefault(x => x.genoid == divgroupoid).gendesc;
            List<ReportModels.DDLDoubleField> tbl = new List<ReportModels.DDLDoubleField>();
            sSql = "SELECT 0 ifield, '-' sfield UNION ALL SELECT m.matrawoid ifield, CONCAT(m.matrawshortdesc,' - ',m.matrawlongdesc) sfield FROM QL_mstmatraw m INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=m.cat2oid WHERE m.activeflag='ACTIVE'";
            if (divgroup == "CONVERTING")
            {
                sSql += " AND c2.cat2code IN('CHMV','CMHT')";
            }
            else
            {
                sSql += " AND c2.cat2code IN('CHMF','CHMC')";
            }
            tbl = db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList();
            
            JsonResult js = Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult getwlap(decimal pd, decimal ld, decimal td, int boxoid, int fluteoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5, decimal lidah, decimal trim, decimal out1, decimal out2, decimal pl, decimal ll, decimal tl, decimal f1, decimal f2, string djflag)
        {
            List<wlap> w1 = new List<wlap>();
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var bx = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp1,0) tolpp1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl1,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal p = 0;
            if (bx == "A1" | bx == "A2" | bx == "A3" | bx == "A5" | bx == "C3" | bx == " C4")
            {
                if (djflag.ToUpper() == "YES")
                {
                    p = (((pd + tolpp1) + (ld + tolpl2) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }
                else
                {
                    p = (((pd + tolpp1) + (ld + tolpl1) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }

            }
            else
            {
                p = (((pl) + (ll) + (pl) + (ll)) * out1 * out2) + lidah + trim;
            }



            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal l = tl + f1 + f2;

            var luas = (p * l) / 1000000;
            var w = (luas * (g1 / 1000)) ;
            var w2 = ((luas * (g2 / 1000)) * tuf);
            var w3 = (luas * (g3 / 1000));
            var w4 = ((luas * (g4 / 1000)) * tuf2);
            var w5 = (luas * (g5 / 1000));

            w1.Add(new wlap { berat1 = w, berat2 = w2, berat3 = w3, berat4 = w4, berat5 = w5 });

            return Json(w1);
        }
        private string generateNo(int cat1)
        {
            sSql = "select cat1code from ql_mstcat1 where cmpcode='" + CompnyCode + "' and cat1oid='" + cat1 + "'";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = tmp + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(itemcode, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }


        private void InitDDL(QL_mstitem tbl, string action)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + tbl.cmpcode + "' AND cat1res1='Finish Good' and cat1shortdesc='KARTON BOX' AND activeflag='ACTIVE' ORDER BY cat1shortdesc";
            var cat1oid = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1oid", "cat1shortdesc", tbl.cat1oid);
            ViewBag.cat1oid = cat1oid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE BOX' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var boxoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.boxoid);
            ViewBag.boxoid = boxoid;
          
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=2 ORDER BY gendesc";
            if (tbl.tipewall == "DW")
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=3 ORDER BY gendesc";
            }
            var fluteoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.fluteoid);
            ViewBag.fluteoid = fluteoid;

            //machine
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine1);
            ViewBag.machine1 = machine1;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine2 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine2);
            ViewBag.machine2 = machine2;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine3 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine3);
            ViewBag.machine3 = machine3;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine4 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine4);
            ViewBag.machine4 = machine4;

            //substance
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub1 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub1);
            ViewBag.sub1 = sub1;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub2 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub2);
            ViewBag.sub2 = sub2;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub3 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub3);
            ViewBag.sub3 = sub3;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub4 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub4);
            ViewBag.sub4 = sub4;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub5 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub5);
            ViewBag.sub5 = sub5;

            //Color from Bahan Baku
            sSql = "SELECT 0 ifield, '-' sfield UNION ALL SELECT m.matrawoid ifield, CONCAT(m.matrawshortdesc,' - ',m.matrawlongdesc) sfield FROM QL_mstmatraw m INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=m.cat2oid WHERE m.activeflag='ACTIVE'";
            var divgroup = (action == "Update Data" ? db.QL_mstgen.FirstOrDefault(x => x.genoid == tbl.divgroupoid).gendesc : divgroupoid);
            if (divgroup == "CONVERTING")
            {
                sSql += " AND c2.cat2code IN('CHMV','CMHT')";
            }
            else
            {
                sSql += " AND c2.cat2code IN('CHMF','CHMC')";
            }
            var colour1oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour1oid);
            ViewBag.colour1oid = colour1oid;
            var colour2oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour2oid);
            ViewBag.colour2oid = colour2oid;
            var colour3oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour3oid);
            ViewBag.colour3oid = colour3oid;
            var colour4oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour4oid);
            ViewBag.colour4oid = colour4oid;
            var colour5oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour5oid);
            ViewBag.colour5oid = colour5oid;
            var colour6oid = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(sSql).ToList(), "ifield", "sfield", tbl.colour6oid);
            ViewBag.colour6oid = colour6oid;
            ViewBag.cat3oid = 0;

            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'ITEM UNIT'";
            var itemunitoid= new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.itemunitoid);
            ViewBag.itemunitoid = itemunitoid;

            //WAREHOUSE / LOCATION
            sSql = "SELECT genoid [locoid], gendesc [locdesc] FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='ITEM LOCATION' AND activeflag='ACTIVE'";
            var itemlocoid = new SelectList(db.Database.SqlQuery<mstitemlocation>(sSql).ToList(), "locoid", "locdesc", tbl.itemlocoid);
            ViewBag.itemlocoid = itemlocoid;
        }
        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "i", divgroupoid, "divgroupoid");
            sSql = "SELECT i.*, i.itemlongdesc [itemdesc], g.gendesc [itemunit], c1.cat1shortdesc, g1.gendesc as boxtype, (select x.gendesc from ql_mstgen x where x.genoid=i.divgroupoid) divgroup  FROM QL_mstitem i inner join ql_mstcat1 c1 on i.cat1oid=c1.cat1oid inner join ql_mstgen g ON g.genoid=i.itemunitoid inner join ql_mstgen g1 on g1.genoid=i.boxoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' and i.cat1oid=2 and left(itemcode,1)<>'D' " + sqlplus+" ORDER BY cat4oid DESC";
            List<item> vbag = db.Database.SqlQuery<item>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstitem tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstitem();
                tbl.itemoid = ClassFunction.GenerateID("QL_mstitem");
                tbl.cat4oid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();
             
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstitem.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstitem tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            //List<suppdtl> dtDtl = (List<suppdtl>)Session["QL_suppdtl"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Please fill detail data!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill detail data!");
            if (tbl.itemcode == null)
                tbl.itemcode = "";
                //ModelState.AddModelError("itemcode", "Kode barang HARUS DIISI");
            if (tbl.itemlongdesc == null)
                ModelState.AddModelError("itemlongdesc", "Nama Panjang HARUS DIISI.");
            if (tbl.itemlocoid == 0)
                ModelState.AddModelError("itemlocoid", "Lokasi gudang HARUS DIISI.");
            if (tbl.tipewall == "SW")
            {
                tbl.sub4 = "";
                tbl.sub5 = "";
            }
            if(tbl.boxoid != 1670)
            {
                tbl.dctype = "";
            }
            tbl.itemshortdesc = tbl.itemshortdesc.ToUpper();
            tbl.itemlongdesc = tbl.itemlongdesc.ToUpper();
            //tbl.nodiecut = tbl.nodiecut.ToUpper();
            //tbl.noplat = tbl.noplat.ToUpper();
            //tbl.itemnote = tbl.itemnote.ToUpper();
            //tbl.itemnote2 = tbl.itemnote2.ToUpper();
            //tbl.itemnote3 = tbl.itemnote3.ToUpper();
            var mstoid = ClassFunction.GenerateID("QL_mstitem");
            //var dtloid = ClassFunction.GenerateID("QL_mstitemdtl");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.itemshortdesc = tbl.itemshortdesc == null ? "" : tbl.itemshortdesc;
                        //tbl.itempicturemat = "";
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(tbl.itempicturemat)))
                        {
                            var sExt = Path.GetExtension(tbl.itempicturemat);
                            var sdir = Server.MapPath(imgtemppath);
                            var sfilename = tbl.itemoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.itempicturemat.ToLower() != (imgtemppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.itempicturemat), sdir + "/" + sfilename);
                            }
                            tbl.itempicturemat = imgtemppath + "/" + sfilename;
                        }
                        tbl.itemavgsales = 0;
                        tbl.itemlastsoprice = 0;
                        tbl.itemnote = tbl.itemnote == null ? "" : tbl.itemnote;
                        //tbl.cat2oid = tbl.cat2oid == null ? 0 : tbl.cat2oid;
                        //tbl.cat3oid = tbl.cat3oid == null ? 0 : tbl.cat3oid;
                        //tbl.cat4oid = tbl.cat4oid == null ? 0 : tbl.cat4oid;

                        tbl.itemsafetystock = 0;
                        tbl.itempackvolume = 0;

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.itemoid = mstoid;
                            tbl.itemcode = generateNo(tbl.cat1oid);
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstitem.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstitem'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            var code = db.Database.SqlQuery<string>("select left(itemcode,1) from ql_mstitem where itemoid=" + tbl.itemoid).FirstOrDefault();
                            if (code == "D")
                            {
                                tbl.itemcode = generateNo(tbl.cat1oid);
                            }
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //var trndtl = db.QL_mstitemdtl.Where(a => a.itemoid == tbl.itemoid && a.cmpcode == tbl.cmpcode);
                            //db.QL_mstitemdtl.RemoveRange(trndtl);
                            //db.SaveChanges();
                        }

                        //QL_mstitemdtl tbldtl;
                        //for (int i = 0; i < dtDtl.Count(); i++)
                        //{
                        //    tbldtl = new QL_mstitemdtl();
                        //    tbldtl.cmpcode = tbl.cmpcode;
                        //    tbldtl.itemdtloid = dtloid++;
                        //    tbldtl.itemoid = tbl.itemoid;
                        //    tbldtl.suppoid = dtDtl[i].suppoid;
                        //    tbldtl.picoid = dtDtl[i].suppdtl2oid;
                        //    tbldtl.upduser = tbl.upduser;
                        //    tbldtl.updtime = tbl.updtime;
                         
                        //    db.QL_mstitemdtl.Add(tbldtl);
                        //}

                        //sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_mstitemdtl'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori4", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            List<QL_mstcat2> objCat2 = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' ORDER BY cat2shortdesc";
            objCat2 = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objCat2, "cat2oid", "cat2shortdesc", 0);

            return Json(DDLCat2);
        }

        [HttpPost]
        public ActionResult Getflute(string fl)
        {
            List<QL_mstgen> objgen = new List<QL_mstgen>();
            if(fl=="SW")
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=2 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            else
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=3 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            objgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objgen, "genoid", "gendesc", 0);

            return Json(DDLCat2);
        }


        [HttpPost]
        public ActionResult Getpl(decimal pd, int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tolpp1,0) tolp1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolp1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            var total = tolp1 + pd;
            return Json(total);
        }

        [HttpPost]
        public ActionResult Getll(decimal ld, int boxoid, int fluteoid, string dj)
        {
            decimal total = 0;
            sSql = "select top 1 isnull(tolpl1,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl2,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            if (dj == "DW")
            {
                //total = tolpl2 + ld;
                total = tolpl1 + ld;
            }
            else
            {
               total = tolpl1 + ld;
            }
           
            return Json(total);
        }

        [HttpPost]
        public ActionResult Gettl(decimal td, int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tolltinggi,0) tolltinggi from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpt1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            var total = tolpt1 + td;
            return Json(total);
        }

        [HttpPost]
        public ActionResult Getflap1(decimal ld, int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tolflap1,0) tolflap1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolflap1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var box = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            var total = tolflap1 + (ld / 2);
            if (box == "A3")
            {
                total = ld / 2;
            }
            else if (box == "A5")
            {
                total = ld;
            }
            return Json(total);
        }

        [HttpPost]
        public ActionResult Getflap2(decimal ld, decimal pd, int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tolflap2,0) tolflap2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolflap2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var box = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            var total = tolflap2 + (ld / 2);
            if (box == "A3")
            {
                total = pd / 2;
            }
            else if (box == "A5")
            {
                total = ld;
            }
            return Json(total);
        }

        [HttpPost]
        public ActionResult Getlidah( int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tollidah,0) tollidah from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tollidah = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            return Json(tollidah);
        }

        [HttpPost]
        public ActionResult Gettrim(int boxoid, int fluteoid)
        {
            sSql = "select top 1 isnull(tolptrim,0) tolptrim from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolptrim = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            return Json(tolptrim);
        }

        [HttpPost]
        public ActionResult Getpsheet(decimal ld, decimal pd, int boxoid, int fluteoid, decimal lidah,decimal trim, decimal out1, decimal out2, decimal pl, decimal ll, string djflag)
        {
            sSql = "select top 1 isnull(tolpp1,0) tolpp1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl1,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
           
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var bx = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            decimal total = 0;
            if (bx == "A1" | bx == "A2" | bx == "A3" | bx == "A5" | bx == "C3" | bx == " C4")
            {
                if (djflag.ToUpper() == "YES")
                {
                    total = (((pd + tolpp1) + (ld + tolpl2) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }
                else
                {
                    total = (((pd + tolpp1) + (ld + tolpl1) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }
               
            }
            else
            {
                total = (((pl) + (ll) + (pl) + (ll) ) * out1 * out2) + lidah + trim;
            }
           
            return Json(total);
        }

        [HttpPost]
        public ActionResult getlsheet(decimal tl, decimal f1, decimal f2)
        {

            //sSql = "select top 1 isnull(tolltinggi,0) tolltinggi from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            //var tolltinggi = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            //sSql = "select top 1 isnull(tolflap1,0) tolflap1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            //var tolflap1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            //sSql = "select top 1 isnull(tolflap2,0) tolflap2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            //var tolflap2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
           
            decimal total = tl + f1 + f2;
            

            return Json(total);
        }
       

        [HttpPost]
        public ActionResult getluas(decimal pd, decimal ld, decimal td, int boxoid, int fluteoid, decimal lidah, decimal trim, decimal out1, decimal out2, decimal pl, decimal ll, decimal tl, decimal f1, decimal f2, string djflag)
        {
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var bx = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp1,0) tolpp1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl1,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal p = 0;
            if (bx == "A1" | bx == "A2" | bx == "A3" | bx == "A5" | bx == "C3" | bx == " C4")
            {
                if (djflag.ToUpper() == "YES")
                {
                    p = (((pd + tolpp1) + (ld + tolpl2) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }
                else
                {
                    p = (((pd + tolpp1) + (ld + tolpl1) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }

            }
            else
            {
                p = (((pl) + (ll) + (pl) + (ll)) * out1 * out2) + lidah + trim;
            }


            decimal l = tl + f1 + f2;

            var total = (p * l)/1000000;

            return Json(total);
        }

        [HttpPost]
        public ActionResult getw1(decimal pd, decimal ld, decimal td, int boxoid, int fluteoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5, decimal lidah, decimal trim, decimal out1, decimal out2, decimal pl, decimal ll, decimal tl, decimal f1, decimal f2, string djflag)
        {
            sSql = "select gendesc from ql_mstgen where genoid='" + boxoid + "'";
            var bx = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp1,0) tolpp1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpp2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpp2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl1,0) tolpl1 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select top 1 isnull(tolpl2,0) tolpp2 from ql_msttol where cmpcode='" + CompnyCode + "' and boxoid='" + boxoid + "' and tfoid='" + fluteoid + "'";
            var tolpl2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal p = 0;
            if (bx == "A1" | bx == "A2" | bx == "A3" | bx == "A5" | bx == "C3" | bx == " C4")
            {
                if (djflag.ToUpper() == "YES")
                {
                    p = (((pd + tolpp1) + (ld + tolpl2) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }
                else
                {
                    p = (((pd + tolpp1) + (ld + tolpl1) + (pd + tolpp2) + (ld + tolpl2)) * out1 * out2) + lidah + trim;
                }

            }
            else
            {
                p = (((pl) + (ll) + (pl) + (ll)) * out1 * out2) + lidah + trim;
            }



            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal l = tl + f1 + f2;

            var luas = (p * l) / 1000000;
            var total = (luas * (g1 / 1000)) + ((luas * (g2 / 1000)) * tuf) + (luas * (g3 / 1000)) + ((luas * (g4 / 1000)) * tuf2) + (luas * (g5 / 1000));

            return Json(total);
        }

        [HttpPost]
        public ActionResult getLuasRecal(decimal p, decimal l, int fluteoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5)
        {
            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            var luas = (p * l) / 1000000;
            var w1 = (luas * (g1 / 1000));
            var w2 = ((luas * (g2 / 1000)) * tuf);
            var w3 = (luas * (g3 / 1000));
            var w4 = 0M;
            var w5 = 0M;
            if (g4 > 0) w4 = ((luas * (g4 / 1000)) * tuf2);
            if (g5 > 0) w5 = (luas * (g5 / 1000));
            var berat = w1 + w2 + w3 + w4 + w5;

            return Json(new { luas = luas, w1 = w1, w2 = w2, w3 = w3, w4 = w4, w5 = w5, berat = berat });
        }

        [HttpPost]
        public ActionResult Getsub1(string sub1)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub1 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub2(string sub2)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub2 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub3(string sub3)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub3 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub4(string sub4)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub4 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub5(string sub5)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub5 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }

    

        public ActionResult Getcat3(int cat1,int cat2)
        {
            List<QL_mstcat3> objcity = new List<QL_mstcat3>();
            objcity = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid==cat1 && g.cat2oid == cat2).ToList();
            SelectList obgcity = new SelectList(objcity, "cat3oid", "cat3shortdesc", 0);
            return Json(obgcity);
        }
        public ActionResult Getcat4(int cat1, int cat2, int cat3)
        {
            List<QL_mstcat4> objcity = new List<QL_mstcat4>();
            objcity = db.QL_mstcat4.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2 && g.cat3oid == cat3).ToList();
            SelectList obgcity = new SelectList(objcity, "cat4oid", "cat4shortdesc", 0);
            return Json(obgcity);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_mstitem.Find(CompnyCode, id);
            if (tbl == null)
                return null;
            List<filterprintout> filter = new List<filterprintout>();
            sSql = "select itemoid, itemcode, itemlongdesc , g.gendesc as box, isnull(dbjoinflag,'NO') djflag, dctype from ql_mstitem i inner join ql_mstgen g on g.genoid=i.boxoid where i.cat1oid=2 and itemoid=" + id +"";
            filter = db.Database.SqlQuery<filterprintout>(sSql).ToList();

            if(filter[0].box=="Die Cut" & filter[0].djflag.ToUpper() == "NO")
            {
                if(filter[0].dctype== "BIASA")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDC.rpt"));
                }
               else if(filter[0].dctype == "SEARAH")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCsearah.rpt"));
                }
                else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCcross.rpt"));
                }
            }
            else if (filter[0].box == "Die Cut" & filter[0].djflag.ToUpper() == "NO")
            {
                if (filter[0].dctype == "BIASA")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDC.rpt"));
                }
                else if (filter[0].dctype == "SEARAH")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCsearah.rpt"));
                }
                else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCcross.rpt"));
                }
            }
            else if (filter[0].box == "DC" & filter[0].djflag.ToUpper() == "YES")
            {
                if (filter[0].dctype == "BIASA")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDC.rpt"));
                }
                else if (filter[0].dctype == "SEARAH")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCsearah.rpt"));
                }
                else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCcross.rpt"));
                }
            }
            else if (filter[0].box == "DC" & filter[0].djflag.ToUpper() == "NO")
            {
                if (filter[0].dctype == "BIASA")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDC.rpt"));
                }
                else if (filter[0].dctype == "SEARAH")
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCsearah.rpt"));
                }
                else
                {
                    report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemDCcross.rpt"));
                }
            }
            else if (filter[0].box == "A1" & filter[0].djflag.ToUpper() == "YES")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitem2j.rpt"));
            }
            else if (filter[0].box == "A1" & filter[0].djflag.ToUpper() == "NO")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemA1.rpt"));
            }
            else if (filter[0].box.ToUpper() == "C3" & filter[0].djflag.ToUpper() == "NO")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemtumbu.rpt"));
            }
            else if (filter[0].box.ToUpper() == "C3" & filter[0].djflag.ToUpper() == "YES")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemtumbu2j.rpt"));
            }
            else if (filter[0].box.ToUpper() == "C4" & filter[0].djflag.ToUpper() == "NO")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemtutup.rpt"));
            }
            else if (filter[0].box.ToUpper() == "C4" & filter[0].djflag.ToUpper() == "YES")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemtutup2j.rpt"));
            }
            else if (filter[0].box == "A5")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitemA5.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptitem.rpt"));
            }

            
            sSql = "SELECT itemcode, itemshortdesc, itemlongdesc, ISNULL((select matrawshortdesc from QL_mstmatraw d where d.matrawoid=i.colour1oid),'') as color1, ISNULL((select matrawshortdesc from QL_mstmatraw d where d.matrawoid=i.colour2oid),'') as color2, ISNULL((select matrawshortdesc from QL_mstmatraw d where d.matrawoid=i.colour3oid),'') as color3, ISNULL((select matrawshortdesc from QL_mstmatraw d where d.matrawoid=i.colour4oid),'') as color4, ISNULL((select matrawshortdesc from QL_mstmatraw d where d.matrawoid=i.colour5oid),'') as color5, i.sub1+ i.sub2 + i.sub3 + isnull(i.sub4,'') + isnull(i.sub5,'') as substance, bx.gendesc tipebox, gf.gendesc as tipeflute, i.tipejoint, lidah, pd, ld, td, pl, ll, tl,  panjangsheet, lebarsheet, luassheet, beratpcs, i.pl p1, i.pl p2, i.ll l1, i.ld l2, flap1, flap2, itemnote, itemnote2, nodiecut, itemnote3, noplat, isnull(jmlstitch,0.0) jmlstitch from QL_mstitem i inner join QL_mstgen bx on i.boxoid=bx.genoid inner join QL_mstgen gf on gf.genoid=i.fluteoid left join ql_msttol t on t.boxoid=i.boxoid and t.tfoid=i.fluteoid WHERE i.cmpcode='" + CompnyCode + "' AND i.itemoid=" + id + "";
            List<listbarang> dtRpt = db.Database.SqlQuery<listbarang>(sSql).ToList();

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sUserID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            report.Close();
            report.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "BarangJadiCB.pdf");
        }



        [HttpPost]
        public async Task<JsonResult> UploadFileMat()
        {
            var result = "";
            var picturepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        picturepath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, picturepath);
            }
            result = "Sukses";

            return Json(new { result, picturepath }, JsonRequestBehavior.AllowGet);
        }
    }
}
