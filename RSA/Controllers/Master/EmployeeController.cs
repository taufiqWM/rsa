﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers
{
    public class EmployeeController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string userimgpath = "~/Images/EmployeeImage";
        private string usersignpath = "~/Images/CardPicture";
        private string imgtemppath = "~/Images/ImagesTemps";
        public class mstperson
        {
            public int personoid { get; set; }
            public string personname { get; set; }
            public string nip { get; set; }
          
        }
        public ActionResult GetNipData()
        {
            List<mstperson> tbl = new List<mstperson>();

            sSql = "SELECT personoid, personname, nip FROM ql_mstperson WHERE activeflag='ACTIVE' ORDER BY nip DESC";
            tbl = db.Database.SqlQuery<mstperson>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        // GET: Employee
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View(db.QL_mstperson.OrderByDescending(o=>o.personoid).ToList());
        }

      
        public ActionResult Form( int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstperson qL_mstperson;
            var cmp = CompnyCode;
            string action = "New Data";
            if (id == null | cmp==null )
            {

                
                qL_mstperson = new QL_mstperson();
               
                qL_mstperson.cmpcode = Session["CompnyCode"].ToString();

                qL_mstperson.createuser = Session["UserID"].ToString();
                qL_mstperson.createtime = ClassFunction.GetServerTime();
                ViewBag.dateofbirth = ClassFunction.GetServerTime().ToString("MM/dd/yyyy");
                //qL_mstperson.isgroupname = "Y";

            }
            else
            {
                action = "Update Data";
                
                qL_mstperson = db.QL_mstperson.Find(cmp,id);//sesuaikan dengan urutan parameter

            }

            if (qL_mstperson == null)
            {
                return HttpNotFound();
            }

            InitAllDDL(qL_mstperson);
            ViewBag.action = action;
            return View(qL_mstperson);
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form( QL_mstperson qL_mstperson,string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            int mstoid = db.QL_mstperson.Any() ? db.QL_mstperson.Max(o => o.personoid) + 1 : 1;
            qL_mstperson.divgroupoid = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {

                    try
                    {
                        //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(qL_mstperson.personpicture)))
                        {
                            var sExt = Path.GetExtension(qL_mstperson.personpicture);
                            var sdir = Server.MapPath(userimgpath);
                            var sfilename = qL_mstperson.personoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_mstperson.personpicture.ToLower() != (userimgpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_mstperson.personpicture), sdir + "/" + sfilename);
                            }
                            qL_mstperson.personpicture = userimgpath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(qL_mstperson.cardpicture)))
                        {
                            var sExt = Path.GetExtension(qL_mstperson.cardpicture);
                            var sdir = Server.MapPath(usersignpath);
                            var sfilename = qL_mstperson.personoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_mstperson.cardpicture.ToLower() != (usersignpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_mstperson.cardpicture), sdir + "/" + sfilename);
                            }
                            qL_mstperson.cardpicture = usersignpath + "/" + sfilename;
                        }
                        if (action == "New Data")
                        {

                            //qL_mstperson.isgroupname = "Y";
                            qL_mstperson.upduser = qL_mstperson.createuser;
                            qL_mstperson.updtime = qL_mstperson.createtime;

                            db.QL_mstperson.Add(qL_mstperson);
                            db.SaveChanges();
                        }
                        else
                        {
                            qL_mstperson.updtime = ClassFunction.GetServerTime();
                            qL_mstperson.upduser = Session["UserID"].ToString();

                            db.Entry(qL_mstperson).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                }

            

            ViewBag.action = action;
            InitAllDDL(qL_mstperson);
            return View(qL_mstperson);
        }

        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }
        // GET: Employee/Delete/5
       

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstperson list = db.QL_mstperson.Find(cmp, id);

            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (list != null)
                        {
                            System.IO.File.Delete(Server.MapPath(list.personpicture));
                            System.IO.File.Delete(Server.MapPath(list.cardpicture));
                        }
                        db.QL_mstperson.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        public bool IsInputValid(QL_mstperson qL_mstperson)
        {
            bool isValid = true;

            
            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
        private void InitAllDDL(QL_mstperson qL_mstperson)
        {
            //var cmpcode = "SELECT cmpcode FROM QL_mstdivision WHERE divoid=" + qL_mstperson.divisioid;
            ViewBag.leveloid = new SelectList(db.QL_mstlevel.Where(w => w.activeflag == "ACTIVE").ToList(), "leveloid", "leveldesc");
            ViewBag.divisioid = new SelectList(db.QL_mstdivision.Where(w => w.activeflag == "ACTIVE").ToList(), "divoid", "divname");
            ViewBag.deptoid = new SelectList(db.QL_mstdept.Where(w => w.activeflag == "ACTIVE").ToList(), "deptoid", "deptname");
            ViewBag.persontitleoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "PERSON TITLE").ToList(), "genoid", "gendesc");
            ViewBag.cityofbirth=ViewBag.curraddrcityoid=ViewBag.origaddrcityoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "CITY").ToList(), "genoid", "gendesc");
            ViewBag.religionoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "RELIGION").ToList(), "genoid", "gendesc");
           
            //ViewBag.nip = db.QL_mstperson.Where(w => w.personoid == qL_mstperson.personoid).Select(s => s.nip).DefaultIfEmpty("").First(); // untuk mengambil nama supplier waktu edit
            //ViewBag.personname = db.QL_mstperson.Where(w => w.personoid == qL_mstperson.personoid).Select(s => s.personname).DefaultIfEmpty("").First(); // untuk mengambil nama supplier waktu edit
           
            //sSql = "SELECT * FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND suppres1='Pemborong' ORDER BY suppname";
            //var suppoid = new SelectList(db.Database.SqlQuery<QL_mstsupp>(sSql).ToList(), "suppoid", "suppname", 0);
            //ViewBag.suppoid = suppoid;

            //sSql = "SELECT * FROM QL_mstgen WHERE  gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE' ORDER BY gendesc";
            //var whoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc",qL_mstperson.whoid);
            //ViewBag.whoid = whoid;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
