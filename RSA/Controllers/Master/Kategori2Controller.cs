﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class Kategori2Controller : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori2", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c2.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT c2.*, c1.cat1shortdesc FROM QL_mstcat2 c2 inner join ql_mstcat1 c1 on c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and c2.cat2oid>0 ORDER BY cat2oid DESC";
            List<ListCat2> vbag = db.Database.SqlQuery<ListCat2>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori2", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat2 tbl;
            if (id == null)
            {
                tbl = new QL_mstcat2();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_mstcat2.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat2 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori2", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tbl.cat1oid < 1)
                ModelState.AddModelError("cat1oid", "Pilih Kategori 1 Dahulu !!");
            if (string.IsNullOrEmpty(tbl.cat2code))
                ModelState.AddModelError("cat2code", "Silahkan isi Kode!");
            else if (db.QL_mstcat2.Where(w => w.cat2code == tbl.cat2code & w.cat2oid != tbl.cat2oid).Count() > 0)
                ModelState.AddModelError("cat2code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.cat2shortdesc))
                ModelState.AddModelError("cat2shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat2.Where(w => w.cat2shortdesc == tbl.cat2shortdesc & w.cat2oid != tbl.cat2oid).Count() > 0)
                ModelState.AddModelError("cat2shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat2note))
                tbl.cat2note = "";

                var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat2");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.cat2oid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat2.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori2", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat2 tbl = db.QL_mstcat2.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat2.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void setViewBag(QL_mstcat2 qL_mstcat2)
        {
            ViewBag.cat1oid = new SelectList(db.QL_mstcat1.Where(w => w.activeflag == "ACTIVE" | w.cat1oid == qL_mstcat2.cat1oid).ToList(), "cat1oid", "cat1shortdesc", qL_mstcat2.cat1oid);
        }

        public class ListCat2
        {
            public int cat2oid { get; set; }
            public string cat2code { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat2note { get; set; }
            public string activeflag { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2longdesc { get; set; }

        }
    }
}
