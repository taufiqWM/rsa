﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class BankaccountController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class mstbankaccount
        {
            public int accountoid { get; set; }
            public string accountno { get; set; }
            public int bankoid { get; set; }
            public string bankname { get; set; }
            public string activeflag { get; set; }
        }

        private void InitDDL(QL_mstaccount tbl)
        {
            sSql = "SELECT * FROM QL_mstbank WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY bankname";
            var bankoid = new SelectList(db.Database.SqlQuery<QL_mstbank>(sSql).ToList(), "bankoid", "bankname", tbl.bankoid);
            ViewBag.bankoid = bankoid;
        }

        // GET: Bankaccount
        public ActionResult Index(mdFilter filter)
        {
            if(Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Rekening Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT a.accountoid, a.accountno, a.bankoid, i.bankname, a.activeflag FROM QL_mstaccount a INNER JOIN QL_mstbank i ON i.bankoid=a.bankoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY i.bankname";
            List<mstbankaccount> vbag = db.Database.SqlQuery<mstbankaccount>(sSql).ToList();
            return View(vbag);
        }

        // GET: Service/Form
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Rekening Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstaccount tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstaccount();
                tbl.bankoid = ClassFunction.GenerateID("QL_mstaccount");
                tbl.acctgoid = 0;
                tbl.curroid = 1;
                tbl.accountres1 = "";
                tbl.accountres2 = "";
                tbl.accountres3 = "";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstaccount.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstaccount tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Rekening Bank", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            if (tbl.accountno == null)
                ModelState.AddModelError("accountno", "No. Rekening HARUS DIISI");

            var mstoid = ClassFunction.GenerateID("QL_mstaccount");
            if (action == "Update Data")
            {
                mstoid = tbl.accountoid;
            }

            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.accounttitle = tbl.accounttitle == null ? "" : tbl.accounttitle;

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.cmpcode = CompnyCode;
                            tbl.accountoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstaccount.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstaccount'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Bankaccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstaccount qL_mstaccount = db.QL_mstaccount.Find(CompnyCode, id);
            string result = "success";
            string msg = "";
            if (qL_mstaccount == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var tbl = db.QL_mstaccount.Where(a => a.accountoid == id && a.cmpcode == CompnyCode);
                        db.QL_mstaccount.RemoveRange(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
