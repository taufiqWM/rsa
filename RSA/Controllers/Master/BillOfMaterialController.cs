﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class BillOfMaterialController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class bommst
        {
            public string cmpcode { get; set; }
            public int bomoid { get; set; }
            public string bomcode { get; set; }
            public string bomwipdesc { get; set; }
            public string itemlongdesc { get; set; }
            public string bomfromdept { get; set; }
            public string bomtodept { get; set; }
            public string bomnote { get; set; }
            public string divname { get; set; }
        }

        public class bomdtl
        {
            public int bomdtlseq { get; set; }
            public string bomreftype { get; set; }
            public int bomrefoid { get; set; }
            public string bomrefcode { get; set; }
            public string bomreflongdesc { get; set; }
            public decimal bomdtlqty { get; set; }
            public decimal bomrefqty { get; set; }
            public int bomdtlunitoid { get; set; }
            public string bomdtlunit { get; set; }
            public string bomdtlnote { get; set; }
        }

        public class itemdata
        {
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemlongdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }
        public class printout_bom
        {
            public string BusinessUnit { get; set; }
            public int ID { get; set; }
            public string WIPCode { get; set; }
            public string WIPDesc { get; set; }
            public string FGCode { get; set; }
            public string FinishGood { get; set; }
            public string FromDept { get; set; }
            public string ToDept { get; set; }
            public decimal WIPQty { get; set; }
            public string WIPUnit { get; set; }
            public string HeaderNote { get; set; }
            public string CreateUser { get; set; }
            public DateTime CreateDateTime { get; set; }
            public string LastUpdUser { get; set; }
            public DateTime LastUpdDateTime { get; set; }
            public decimal Thick { get; set; }
            public decimal Width { get; set; }
            public decimal Length { get; set; }
            public int IDDetail { get; set; }
            public int Nmr { get; set; }
            public string Type { get; set; }
            public string Code { get; set; }
            public string Description { get; set; }
            public decimal Qty { get; set; }
            public string Unit { get; set; }
            public string DetailNote { get; set; }

        }

        private string generateNo()
        {
            
            var sNo =  "M-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(bomcode, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstbillofmat WHERE cmpcode='" + CompnyCode + "' AND bomcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }

        private void InitDDL(QL_mstbillofmat tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            var bomfromdeptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.bomfromdeptoid);
            ViewBag.bomfromdeptoid = bomfromdeptoid;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            List<SelectListItem> bomtodeptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.bomtodeptoid).ToList();
            bomtodeptoid.Insert(0, (new SelectListItem { Text = "END", Value = "-1" }));
            ViewBag.bomtodeptoid = bomtodeptoid;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_mstbillofmat tbl)
        {
            ViewBag.itemlongdesc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid + "'").FirstOrDefault();
            ViewBag.i2desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid2 + "'").FirstOrDefault();
            ViewBag.i3desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid3 + "'").FirstOrDefault();
            var bomunit = db.Database.SqlQuery<string>("SELECT gendesc FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid='" + tbl.bomunitoid + "'").FirstOrDefault();
            ViewBag.pt1desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.pt1 + "'").FirstOrDefault();
            ViewBag.pt2desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.pt2 + "'").FirstOrDefault();
            ViewBag.pt3desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.pt3 + "'").FirstOrDefault();
            ViewBag.ly1desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.ly1 + "'").FirstOrDefault();
            ViewBag.ly2desc = db.Database.SqlQuery<string>("SELECT itemlongdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.ly2 + "'").FirstOrDefault();
            //if (!string.IsNullOrEmpty(bomunit.ToString()))
            //ViewBag.bomunit = "Per " + bomunit.ToString();
        }

        [HttpPost]
        public ActionResult GetItemData(string cmp)
        {
            List<itemdata> tbl = new List<itemdata>();

            sSql = "SELECT i.itemoid, itemcode, itemlongdesc, itemunitoid, gendesc itemunit FROM QL_mstitem i INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE i.cmpcode='" + cmp + "' AND i.activeflag='ACTIVE' and cat1oid=2 AND i.activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<itemdata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetItem2Data(string cmp)
        {
            List<itemdata> tbl = new List<itemdata>();

            sSql = "SELECT i.itemoid, itemcode, itemlongdesc, itemunitoid, gendesc itemunit FROM QL_mstitem i INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE i.cmpcode='" + cmp + "' AND i.activeflag='ACTIVE' and cat1oid=2 AND i.activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<itemdata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetItem3Data(string cmp)
        {
            List<itemdata> tbl = new List<itemdata>();

            sSql = "SELECT i.itemoid, itemcode, itemlongdesc, itemunitoid, gendesc itemunit FROM QL_mstitem i INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE i.cmpcode='" + cmp + "' AND i.activeflag='ACTIVE' and cat1oid=2 AND i.activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<itemdata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetptData(string cmp)
        {
            List<itemdata> tbl = new List<itemdata>();

            sSql = "SELECT i.itemoid, itemcode, itemlongdesc, itemunitoid, gendesc itemunit FROM QL_mstitem i INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE i.cmpcode='" + cmp + "' AND i.activeflag='ACTIVE' and cat1oid=5 ORDER BY itemcode";
            tbl = db.Database.SqlQuery<itemdata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetlyData(string cmp)
        {
            List<itemdata> tbl = new List<itemdata>();

            sSql = "SELECT i.itemoid, itemcode, itemlongdesc, itemunitoid, gendesc itemunit FROM QL_mstitem i INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE i.cmpcode='" + cmp + "' and cat1oid=4 AND i.activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<itemdata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }


        // GET: BillOfMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("BillOfMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "All Editable Data";
            var DisplayCol = "normal";

            sSql = "SELECT bom.cmpcode, bom.bomoid, bomcode, bomwipdesc, itemlongdesc, df.deptname bomfromdept, ISNULL(dt.deptname, 'END') bomtodept, bomnote, divname FROM QL_mstbillofmat bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid Left JOIN QL_mstdept df ON df.cmpcode=bom.cmpcode AND df.deptoid=bomfromdeptoid LEFT JOIN QL_mstdept dt ON dt.cmpcode=bom.cmpcode AND dt.deptoid=bomtodeptoid INNER JOIN QL_mstdivision div ON div.cmpcode=bom.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "bom.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "bom.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "PW")
                {
                    sSql += " AND bom.createtime>=CAST('" + ClassFunction.GetServerTime().AddDays(-7).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND bom.createtime<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    DDTitle = "Editable Data In Past Week";
                }
                else if (filter == "PM")
                {
                    sSql += " AND bom.createtime>=CAST('" + ClassFunction.GetServerTime().AddMonths(-1).ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND bom.createtime<=CAST('" + ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                    DDTitle = "Editable Data In Past Month";
                }
                else if (filter == "CF" & modfil != null)
                {
                    sSql += " AND bom.createtime>=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND bom.createtime<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                }
            }

            if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
                sSql += " AND bom.createuser='" + Session["UserID"].ToString() + "'";

            List<bommst> dt = db.Database.SqlQuery<bommst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: BillOfMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
          if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("BillOfMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbillofmat tbl;
            string action = "New Data";
            if (id == null )
            {
                tbl = new QL_mstbillofmat();
                tbl.bomoid = ClassFunction.GenerateID("QL_mstbillofmat");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.itemoid = 0;

                Session["QL_mstbillofmatdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstbillofmat.Find(CompnyCode, id);
               
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: BillOfMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstbillofmat tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("BillOfMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            // TAMBAHAN VALIDASI UTK BAGIAN HEADER
            if (tbl.itemoid == 0)
                ModelState.AddModelError("itemoid", "This field is required.");

            tbl.bomwipcode = "";
            if(tbl.bomwipdesc != null)
            {
                if (tbl.bomwipdesc != "")
                {
                    tbl.bomwipdesc = tbl.bomwipdesc.ToUpper();
                }
            }
            if (tbl.bomnote != null)
            {
                if (tbl.bomnote != "")
                {
                    tbl.bomnote = tbl.bomnote.ToUpper();
                }
            }
            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_mstbillofmat");
                var dtloid = ClassFunction.GenerateID("QL_mstbillofmatdtl");
                var servertime = ClassFunction.GetServerTime();
               

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.bomoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            tbl.bomcode = generateNo();
                          
                            db.QL_mstbillofmat.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.bomoid + " WHERE tablename='QL_mstbillofmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

               
                        }

                     

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: BillOfMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("BillOfMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbillofmat tbl = db.QL_mstbillofmat.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstbillofmat.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        public class listbarang
        {
            public int bomoid { get; set; }
            public string bomcode { get; set; }
            public string itemcode { get; set; }
            public string itemshortdesc { get; set; }
            public string itemlongdesc { get; set; }
            public string color1 { get; set; }
            public string color2 { get; set; }
            public string color3 { get; set; }
            public string color4 { get; set; }
            public string color5 { get; set; }
            public string substance { get; set; }
            public string tipebox { get; set; }
            public string tipeflute { get; set; }
            public string tipejoint { get; set; }
            public decimal pl { get; set; }
            public decimal ll { get; set; }
            public decimal tl { get; set; }
            public decimal pd { get; set; }
            public decimal ld { get; set; }
            public decimal td { get; set; }
            public decimal p1 { get; set; }
            public decimal p2 { get; set; }
            public decimal l1 { get; set; }
            public decimal l2 { get; set; }
            public decimal flap1 { get; set; }
            public decimal flap2 { get; set; }
            public decimal luassheet { get; set; }
            public decimal panjangsheet { get; set; }
            public decimal lebarsheet { get; set; }
            public decimal lidah { get; set; }
            public decimal beratpcs { get; set; }
            public string nodiecut { get; set; }
            public string noplat { get; set; }
            public string itemnote { get; set; }
            public string itemnote2 { get; set; }
            public string itemnote3 { get; set; }
            public decimal jmlstitch { get; set; }
            public string pictmat { get; set; }
        }
        // GET: BillOfMaterial/PrintReport/5/11
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
          if (ClassFunction.checkPagePermission("BillOfMaterial", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_mstbillofmat.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptBillOfMaterial.rpt"));
            sSql = "select * from( SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, c1.gendesc as color1, c2.gendesc as color2, c3.gendesc as color3, c4.gendesc as color4, c5.gendesc as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, bx.gendesc tipebox, gf.gendesc as tipeflute, i.tipejoint,lidah,pd,ld,td,pl,ll,tl, panjangsheet,lebarsheet,luassheet,beratpcs, (i.pd + isnull(t.tolpp1, 0)) p1, (i.pd + isnull(t.tolpp2, 0)) p2,(i.ld + isnull(tolpl1, 0)) l1,(i.ld + isnull(tolpl2, 0)) l2,flap1,flap2,itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,(bx.gendesc + ISNULL(dctype, '') + isnull(dbjoinflag, '')) + '.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.itemoid = I.itemoid inner join ql_mstgen c1 on c1.genoid = i.colour1oid inner join ql_mstgen c2 on c2.genoid = i.colour2oid inner join ql_mstgen c3 on c3.genoid = i.colour3oid inner join ql_mstgen c4 on c4.genoid = i.colour4oid inner join ql_mstgen c5 on c5.genoid = i.colour5oid inner join QL_mstgen bx on i.boxoid = bx.genoid inner join QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
              sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, c1.gendesc as color1, c2.gendesc as color2, c3.gendesc as color3, c4.gendesc as color4, c5.gendesc as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, bx.gendesc tipebox, gf.gendesc as tipeflute, i.tipejoint,lidah,pd,ld,td,pl,ll,tl, panjangsheet,lebarsheet,luassheet,beratpcs, (i.pd + isnull(t.tolpp1, 0)) p1, (i.pd + isnull(t.tolpp2, 0)) p2,(i.ld + isnull(tolpl1, 0)) l1,(i.ld + isnull(tolpl2, 0)) l2,flap1,flap2,itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,(bx.gendesc + ISNULL(dctype, '') + isnull(dbjoinflag, '')) + '.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.itemoid2 = I.itemoid inner join ql_mstgen c1 on c1.genoid = i.colour1oid inner join ql_mstgen c2 on c2.genoid = i.colour2oid inner join ql_mstgen c3 on c3.genoid = i.colour3oid inner join ql_mstgen c4 on c4.genoid = i.colour4oid inner join ql_mstgen c5 on c5.genoid = i.colour5oid inner join QL_mstgen bx on i.boxoid = bx.genoid inner join    QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, c1.gendesc as color1, c2.gendesc as color2, c3.gendesc as color3, c4.gendesc as color4, c5.gendesc as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, bx.gendesc tipebox, gf.gendesc as tipeflute, i.tipejoint,lidah,pd,ld,td,pl,ll,tl, panjangsheet,lebarsheet,luassheet,beratpcs, (i.pd + isnull(t.tolpp1, 0)) p1, (i.pd + isnull(t.tolpp2, 0)) p2,(i.ld + isnull(tolpl1, 0)) l1,(i.ld + isnull(tolpl2, 0)) l2,flap1,flap2,itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,(bx.gendesc + ISNULL(dctype, '') + isnull(dbjoinflag, '')) + '.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.itemoid3 = I.itemoid inner join ql_mstgen c1 on c1.genoid = i.colour1oid inner join ql_mstgen c2 on c2.genoid = i.colour2oid inner join ql_mstgen c3 on c3.genoid = i.colour3oid inner join ql_mstgen c4 on c4.genoid = i.colour4oid inner join ql_mstgen c5 on c5.genoid = i.colour5oid inner join QL_mstgen bx on i.boxoid = bx.genoid inner join    QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, '-' as color1, '-' as color2, '-' as color3, '-' as color4, '-' as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, 'Other' tipebox, gf.gendesc as tipeflute, i.tipejoint,isnull(lidah,0.0) lidah,isnull(pd,0.0) pd,isnull(ld,0.0) ld,isnull(td,0.0) td,isnull(pl,0.0) pl,isnull(ll,0.0) ll,isnull(tl,0.0) tl, panjangsheet,lebarsheet,luassheet,beratpcs, 0.0 p1, 0.0 p2,0.0 l1,0.0 l2, 0.0 flap1, 0.0 flap2, itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,'Other.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.pt1 = I.itemoid inner join   QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, '-' as color1, '-' as color2, '-' as color3, '-' as color4, '-' as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, 'Other' tipebox, gf.gendesc as tipeflute, i.tipejoint,isnull(lidah,0.0) lidah,isnull(pd,0.0) pd,isnull(ld,0.0) ld,isnull(td,0.0) td,isnull(pl,0.0) pl,isnull(ll,0.0) ll,isnull(tl,0.0) tl, panjangsheet,lebarsheet,luassheet,beratpcs, 0.0 p1, 0.0 p2,0.0 l1,0.0 l2, 0.0 flap1, 0.0 flap2, itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,'Other.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.pt2 = I.itemoid inner join  QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, '-' as color1, '-' as color2, '-' as color3, '-' as color4, '-' as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, 'Other' tipebox, gf.gendesc as tipeflute, i.tipejoint,isnull(lidah,0.0) lidah,isnull(pd,0.0) pd,isnull(ld,0.0) ld,isnull(td,0.0) td,isnull(pl,0.0) pl,isnull(ll,0.0) ll,isnull(tl,0.0) tl, panjangsheet,lebarsheet,luassheet,beratpcs, 0.0 p1, 0.0 p2,0.0 l1,0.0 l2, 0.0 flap1, 0.0 flap2, itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,'Other.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.pt3 = I.itemoid inner join  QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += "  SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, '-' as color1, '-' as color2, '-' as color3, '-' as color4, '-' as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, 'Other' tipebox, gf.gendesc as tipeflute, i.tipejoint,isnull(lidah,0.0) lidah,isnull(pd,0.0) pd,isnull(ld,0.0) ld,isnull(td,0.0) td,isnull(pl,0.0) pl,isnull(ll,0.0) ll,isnull(tl,0.0) tl, panjangsheet,lebarsheet,luassheet,beratpcs, 0.0 p1, 0.0 p2,0.0 l1,0.0 l2, 0.0 flap1, 0.0 flap2, itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,'Other.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.ly1 = I.itemoid inner join  QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
            sSql += " union ALL";
            sSql += " SELECT bomoid, bomcode, itemcode, itemshortdesc, itemlongdesc, '-' as color1, '-' as color2, '-' as color3, '-' as color4, '-' as color5,i.sub1 + i.sub2 + i.sub3 + isnull(i.sub4, '') + isnull(i.sub5, '') as substance, 'Other' tipebox, gf.gendesc as tipeflute, i.tipejoint,isnull(lidah,0.0) lidah,isnull(pd,0.0) pd,isnull(ld,0.0) ld,isnull(td,0.0) td,isnull(pl,0.0) pl,isnull(ll,0.0) ll,isnull(tl,0.0) tl, panjangsheet,lebarsheet,luassheet,beratpcs, 0.0 p1, 0.0 p2,0.0 l1,0.0 l2, 0.0 flap1, 0.0 flap2, itemnote,itemnote2,nodiecut,itemnote3,noplat, isnull(dbjoinflag, '') dbjoinflag,isnull(jmlstitch, 0.0) jmlstitch,'Other.jpg' pictmat from QL_mstbillofmat b inner join QL_mstitem i on b.ly2 = I.itemoid inner join  QL_mstgen gf on gf.genoid = i.fluteoid LEFT join ql_msttol t on t.boxoid = i.boxoid and t.tfoid = i.fluteoid";
           sSql+=" )as tbl WHERE tbl.bomoid=" + id + "";
            List<listbarang> dtRpt = db.Database.SqlQuery<listbarang>(sSql).ToList();

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sUserID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "BillOfMaterialPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
