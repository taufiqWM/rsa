﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class ManufactureController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class manufacturemst
        {
            public string cmpcode { get; set; }
            public int manufactureoid { get; set; }
            public string manufacturecode { get; set; }
            public string manufactureaddr { get; set; }
            public string manufacturephone { get; set; }
            public string manufactureemail { get; set; }
            public String manufacturename { get; set; }
            public string activeflag { get; set; }            
        }

        public class manufacturedtl1
        {            
            public int manufacturedtl1seq { get; set; }
            public string manufacturedtl1addr { get; set; }
            public int manufacturecityoid { get; set; }
            public string manufacturedtl1phone { get; set; }
            public string manufacturedtl1status { get; set; }
        }

        private void InitDDL(QL_mstmanufacture tbl)
        {
            //sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            //if (Session["CompnyCode"].ToString() != CompnyCode)
            //    sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            //sSql += " ORDER BY divname";
            //var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            //ViewBag.cmpcode = cmpcode;

            //sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            //var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            //ViewBag.deptoid = deptoid;

            //sSql = "SELECT * FROM QL_approvalperson WHERE tablename='QL_prrawmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "', '" + CompnyCode + "') ORDER BY approvaluser";
            //var approvaluser = new SelectList(db.Database.SqlQuery<QL_approvalperson>(sSql).ToList(), "approvaluser", "approvaluser", tbl.approvaluser);
            //ViewBag.approvaluser = approvaluser;

        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int reqoid)
        {
            List<manufacturedtl1> tbl = new List<manufacturedtl1>();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<manufacturedtl1> dtDtl)
        {
            Session["QL_mstmanufacturedtl1"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstmanufacturedtl1"] == null)
            {
                Session["QL_mstmanufacturedtl1"] = new List<manufacturedtl1>();
            }

            List<manufacturedtl1> dataDtl = (List<manufacturedtl1>)Session["QL_mstmanufacturedtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: Manufacture
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Manufacture", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");            
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_mstmanufacture WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY manufactureoid DESC";
            List<manufacturemst> vbag = db.Database.SqlQuery<manufacturemst>(sSql).ToList();
            return View(vbag);
        }

        // GET: Manufacture/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Manufacture", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstmanufacture tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstmanufacture();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.manufacturecode = generateCode();
                tbl.manufactureprovinceoid = db.QL_mstgen.Where(o => o.gengroup == "PROVINCE").FirstOrDefault().genoid;
                //tbl.manufacturecityOid = db.QL_mstgen.Where(o => o.gengroup == "CITY" && o.genother1 == tbl.manufactureprovinceoid.ToString()).FirstOrDefault().genoid;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                //Session["QL_mstmanufacturedtl1"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstmanufacture.Find(Session["CompnyCode"].ToString(), id);

                //string sSql = "SELECT sd1.manufacturedtl1seq,sd1.manufacturedtl1addr, sd1.manufacturecityoid, sd1.manufacturedtl1phone, sd1.manufacturedtl1status FROM QL_mstmanufacturedtl1 sd1 where manufactureoid =" + tbl.manufactureoid;
                //Session["QL_mstmanufacturedtl1"] = db.Database.SqlQuery<manufacturedtl1>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            SetViewBag(tbl);
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Manufacture/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstmanufacture tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Manufacture", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //List<manufacturedtl1> dt1dtl = (List<manufacturedtl1>)Session["QL_mstmanufacturedtl1"];
            //if (dt1dtl == null)
            //    ModelState.AddModelError("", "Please fill Detail");
            //else if (dt1dtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill Detail");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.manufacturecode))
                ModelState.AddModelError("manufacturecode", "Silahkan isi Kode!");
            else if (db.QL_mstmanufacture.Where(w => w.manufacturecode == tbl.manufacturename & w.manufactureoid != tbl.manufactureoid).Count() > 0)
                ModelState.AddModelError("manufacturecode", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.manufacturecode))
                ModelState.AddModelError("manufacturename", "Silahkan isi Nama!");
            else if (db.QL_mstmanufacture.Where(w => w.manufacturename == tbl.manufacturename & w.manufactureoid != tbl.manufactureoid).Count() > 0)
                ModelState.AddModelError("manufacturename", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if(string.IsNullOrEmpty(tbl.manufactureprovinceoid.ToString()))
                ModelState.AddModelError("manufactureprovinceoid", "Provinsi harus diisi!");
            if (string.IsNullOrEmpty(tbl.manufactureaddr))
                tbl.manufactureaddr = "";
            if (string.IsNullOrEmpty(tbl.manufactureaddr2))
                tbl.manufactureaddr2 = "";
            if (string.IsNullOrEmpty(tbl.manufactureaddr3))
                tbl.manufactureaddr3 = "";
            if (string.IsNullOrEmpty(tbl.manufacturepostcode))
                tbl.manufacturepostcode = "";
            if (string.IsNullOrEmpty(tbl.manufacturephone1))
                tbl.manufacturephone1 = "";
            if (string.IsNullOrEmpty(tbl.manufacturephone2))
                tbl.manufacturephone2 = "";
            if (string.IsNullOrEmpty(tbl.manufacturefax1))
                tbl.manufacturefax1 = "";
            if (string.IsNullOrEmpty(tbl.manufactureemail))
                tbl.manufactureemail = "";
            if (string.IsNullOrEmpty(tbl.manufacturenpwpno))
                tbl.manufacturenpwpno = "";
            if (string.IsNullOrEmpty(tbl.manufacturenpwp))
                tbl.manufacturenpwp = "";
            if (string.IsNullOrEmpty(tbl.manufacturenpwpaddr))
                tbl.manufacturenpwpaddr = "";
            if (string.IsNullOrEmpty(tbl.manufacturenote))
                tbl.manufacturenote = "";

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstmanufacture");

            if (ModelState.IsValid)
            {
                //var kntrdtloid = ClassFunction.GenerateID("QL_mstmanufacturedtl1");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.manufactureoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;                            
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstmanufacture.Add(tbl);
                            db.SaveChanges();                            

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE tablename='QL_mstmanufacture'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //delete detail
                            //var kntrdtl = db.QL_mstmanufacturedtl1.Where(a => a.manufactureoid == tbl.manufactureoid);
                            //db.QL_mstmanufacturedtl1.RemoveRange(kntrdtl);
                            //db.SaveChanges();
                        }

                        //insert detail
                        //QL_mstmanufacturedtl1 tbldtl1;
                        //for (int i = 0; i < dt1dtl.Count(); i++)
                        //{
                        //    tbldtl1 = new QL_mstmanufacturedtl1();
                        //    tbldtl1.cmpcode = tbl.cmpcode;
                        //    tbldtl1.manufacturedtl1seq = i++;
                        //    tbldtl1.manufacturedtl1oid = kntrdtloid++;
                        //    tbldtl1.manufactureoid = tbl.manufactureoid;
                        //    tbldtl1.manufacturedtl1addr = dt1dtl[i].manufacturedtl1addr;
                        //    tbldtl1.manufacturecityoid = dt1dtl[i].manufacturecityoid;
                        //    tbldtl1.manufacturedtl1phone = dt1dtl[i].manufacturedtl1phone;
                        //    tbldtl1.manufacturedtl1status = dt1dtl[i].manufacturedtl1status;
                        //    tbldtl1.updtime = tbl.updtime;
                        //    tbldtl1.upduser = tbl.upduser;
                        //    db.QL_mstmanufacturedtl1.Add(tbldtl1);
                        //}
                        //db.SaveChanges();
                        ////Update lastoid
                        //sSql = "Update QL_mstoid set lastoid = " + kntrdtloid + " Where tablename = 'QL_mstmanufacturedtl1'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            SetViewBag(tbl);
            ViewBag.action = action;            
            return View(tbl);
            //ViewBag(QL_mstmanufacturedtl1);
        }

        // POST: Manufacture/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission("Manufacture", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstmanufacture tbl = db.QL_mstmanufacture.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //var trndtl = db.QL_mstmanufacturedtl1.Where(a => a.manufactureoid == id);
                        //db.QL_mstmanufacturedtl1.RemoveRange(trndtl);
                        //db.SaveChanges();

                        db.QL_mstmanufacture.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private void SetViewBag(QL_mstmanufacture qL_mstmanufacture)
        {
            if (qL_mstmanufacture == null) qL_mstmanufacture = new QL_mstmanufacture();
            string sqlPlus = "";

            //NATION
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY'";
            var manufacturenationoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstmanufacture.manufacturenationoid);
            ViewBag.manufacturenationoid = manufacturenationoid;

            //PROVINCE
            if(qL_mstmanufacture.manufacturenationoid > 0)
            {
                sqlPlus = " AND genother1='" + qL_mstmanufacture.manufacturenationoid.ToString() + "'";
            }
            else
            {
                sqlPlus = " AND genother1='" + manufacturenationoid.First().Value.ToString() + "'";
            }
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND gendesc!='-'" + sqlPlus;
            var manufactureprovinceoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstmanufacture.manufactureprovinceoid);
            ViewBag.manufactureprovinceoid = manufactureprovinceoid;

            if (qL_mstmanufacture.manufactureprovinceoid > 0)
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND activeflag='ACTIVE' AND genother1='" + qL_mstmanufacture.manufactureprovinceoid.ToString() + "'";
                var manufacturecityOid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstmanufacture.manufacturecityOid);
                ViewBag.manufacturecityOid = manufacturecityOid;
            }
            else
            {
                var iOid = "";
                if(manufactureprovinceoid.Count() == 0) //manufactureprovinceoid.First().Value.ToString()
                {
                    sSql = "SELECT CAST(genoid AS VARCHAR) FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND gendesc='JAWA TIMUR'";
                    iOid = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                }
                else
                {
                    iOid = manufactureprovinceoid.First().Value.ToString();
                }
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND activeflag='ACTIVE' AND genother1='" + iOid.ToString() + "'";
                var manufacturecityOid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstmanufacture.manufacturecityOid);
                ViewBag.manufacturecityOid = manufacturecityOid;
            }

            //PAYMENT TYPE
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'";
            var manufacturepaymentoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", qL_mstmanufacture.manufacturepaymentoid);
            ViewBag.manufacturepaymentoid = manufacturepaymentoid;
        }

        [HttpPost]
        public ActionResult GetProvince(int nationoid)
        {
            List<QL_mstgen> objProvince = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND genother1='" + nationoid.ToString() + "'";
            objProvince = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList obgProvince = new SelectList(objProvince, "genoid", "gendesc", 0);
            return Json(obgProvince);
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_mstgen> objcity = new List<QL_mstgen>();
            objcity = db.QL_mstgen.Where(g => g.gengroup == "CITY" && g.activeflag == "ACTIVE" && g.genother1 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "genoid", "gendesc", 0);
            return Json(obgcity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult getQL_mstmanufacturedtl1()
        {
            if (Session["QL_mstmanufacturedtl1"] == null)
            {
                Session["QL_mstmanufacturedtl1"] = new List<manufacturedtl1>();
            }

            List<manufacturedtl1> dataDtl = (List<manufacturedtl1>)Session["QL_mstmanufacturedtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_mstmanufacturedtl1(List<manufacturedtl1> mdataDtl)
        {
            Session["QL_mstmanufacturedtl1"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string generateCode()
        {
            string sCode = "MANF-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(ManufactureCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstmanufacture WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND manufacturecode LIKE '" + sCode + "%'";
            
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }
    }
}
