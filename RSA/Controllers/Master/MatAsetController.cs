﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class MatAsetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string imgpath = "~/Images/GenImages";
        private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        public class matgen
        {
            public string cmpcode { get; set; }
            public int matgenoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string matgencode { get; set; }
            public string matgenunit { get; set; }
            public string matgendesc { get; set; }
            public string matgentype { get; set; }
            public string activeflag { get; set; }

        }
        public class suppdtl
        {
            public int matgendtloid { get; set; }
            public int matgenoid { get; set; }
            public int suppoid { get; set; }
            public int suppdtl2oid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string picname { get; set; }
            public string activeflag { get; set; }

        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class mstmatgenlocation
        {
            public int locoid { get; set; }
            public string locdesc { get; set; }
        }

        public class listcoa
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        private void InitDDL(QL_mstmatgen tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);
            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'MATERIAL UNIT'";
            var matgenunitoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.matgenunitoid);
            ViewBag.matgenunitoid = matgenunitoid;

            //WAREHOUSE / LOCATION
            sSql = "SELECT genoid [locoid], gendesc [locdesc] FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE'";
            var matgenlocoid = new SelectList(db.Database.SqlQuery<mstmatgenlocation>(sSql).ToList(), "locoid", "locdesc", tbl.matgenlocoid);
            ViewBag.matgenlocoid = matgenlocoid;

            //SUPPLIER
            sSql = "SELECT ISNULL(suppname, '') [suppname] FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid;
            ViewBag.suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            //COA
            string[] sVar = { "VAR_ASSET" };
            string acctgoid_pr = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid_pr + ") ORDER BY acctgcode";
            var acctgoid = new SelectList(db.Database.SqlQuery<listcoa>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
        }
        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("MatGen", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "i", divgroupoid, "divgroupoid");

            sSql = "SELECT i.*, i.matgenlongdesc [matgendesc], g.gendesc [matgenunit] FROM QL_mstmatgen i inner join ql_mstgen g ON g.genoid=i.matgenunitoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' and isnull(matgenres1,'')='ASET' " + sqlplus + " ORDER BY cat4oid DESC";
            List<matgen> vbag = db.Database.SqlQuery<matgen>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("MatGen", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstmatgen tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstmatgen();
                tbl.matgenoid = ClassFunction.GenerateID("QL_mstmatgen");
                tbl.cat4oid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.matgenlimitqty = 1;
                tbl.matgensafetystock = 1;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstmatgen.Find(cmp, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        private string generateNo()
        {

            var tmp = "AST";

            var sNo = tmp + "-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matgencode, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstmatgen WHERE cmpcode='" + CompnyCode + "' AND matgencode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }


        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstmatgen tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("MatGen", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            //List<suppdtl> dtDtl = (List<suppdtl>)Session["QL_suppdtl"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Please fill detail data!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill detail data!");

            if (tbl.matgenlongdesc == null)
                ModelState.AddModelError("matgenlongdesc", "Nama Panjang HARUS DIISI.");
            else if (db.QL_mstmatgen.Where(w => w.matgenlongdesc == tbl.matgenlongdesc & w.matgenoid != tbl.matgenoid).Count() > 0)
                ModelState.AddModelError("", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if (tbl.matgensafetystock == 0)
                tbl.matgensafetystock = 0;
            if (tbl.matgenlimitqty == 0)
                tbl.matgenlimitqty = 0;
            if (tbl.matgenlocoid == 0)
                tbl.matgenlocoid = 0;

            var mstoid = ClassFunction.GenerateID("QL_mstmatgen");
            if (action == "Update Data")
            {
                mstoid = tbl.matgenoid;
            }
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.matgenshortdesc = tbl.matgenshortdesc == null ? "" : tbl.matgenshortdesc;
                        tbl.matgenbarcode = "";

                        tbl.matgenpicturemat = tbl.matgenpicturemat == null ? "" : tbl.matgenpicturemat;
                        tbl.suppoid = 0;
                        tbl.matgenavgsales = 0;
                        tbl.matgenlastpoprice = 0;
                        tbl.matgenavgsales = 0;
                        tbl.matgenavgpurchase = 0;
                        tbl.matgennote = tbl.matgennote == null ? "" : tbl.matgennote;
                        tbl.matgenres1 = "ASET";
                        tbl.matgenres2 = tbl.matgenres2 == null ? "0" : tbl.matgenres2;
                        tbl.matgenres3 = "";
                        tbl.matgenunit2oid = 0;
                        tbl.matgenconvertunit = 0;
                        tbl.cat1oid = (int?)tbl.cat1oid ?? 0;
                        tbl.cat2oid = (int?)tbl.cat2oid ?? 0;
                        tbl.cat3oid = (int?)tbl.cat3oid ?? 0;
                        tbl.cat4oid = (int?)tbl.cat4oid ?? 0;

                        //Upload
                        //matgenpictureloc
                        if (System.IO.File.Exists(Server.MapPath(tbl.matgenpictureloc)))
                        {
                            var sExt = Path.GetExtension(tbl.matgenpictureloc);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesGen_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matgenpictureloc) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matgenpictureloc), sdir + "/" + sfilename);
                            }

                            tbl.matgenpictureloc = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matgenpictureloc = tbl.matgenpictureloc == null ? "" : tbl.matgenpictureloc;
                        }

                        //matgenpicturemat
                        if (System.IO.File.Exists(Server.MapPath(tbl.matgenpicturemat)))
                        {
                            var sExt = Path.GetExtension(tbl.matgenpicturemat);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesGenMat_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matgenpicturemat) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matgenpicturemat), sdir + "/" + sfilename);
                            }

                            tbl.matgenpicturemat = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matgenpicturemat = tbl.matgenpicturemat == null ? "" : tbl.matgenpicturemat;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.matgenoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.matgencode = generateNo();
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstmatgen.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstmatgen'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //var trndtl = db.QL_mstmatgendtl.Where(a => a.matgenoid == tbl.matgenoid && a.cmpcode == tbl.cmpcode);
                            //db.QL_mstmatgendtl.RemoveRange(trndtl);
                            //db.SaveChanges();
                        }

                        //QL_mstmatgendtl tbldtl;
                        //for (int i = 0; i < dtDtl.Count(); i++)
                        //{
                        //    tbldtl = new QL_mstmatgendtl();
                        //    tbldtl.cmpcode = tbl.cmpcode;
                        //    tbldtl.matgendtloid = dtloid++;
                        //    tbldtl.matgenoid = tbl.matgenoid;
                        //    tbldtl.suppoid = dtDtl[i].suppoid;
                        //    tbldtl.picoid = dtDtl[i].suppdtl2oid;
                        //    tbldtl.upduser = tbl.upduser;
                        //    tbldtl.updtime = tbl.updtime;

                        //    db.QL_mstmatgendtl.Add(tbldtl);
                        //}

                        //sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_mstmatgendtl'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("MatGen", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            List<QL_mstcat2> objCat2 = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' ORDER BY cat2shortdesc";
            objCat2 = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objCat2, "cat2oid", "cat2shortdesc", 0);

            return Json(DDLCat2);
        }
        public ActionResult Getcat3(int cat1, int cat2)
        {
            List<QL_mstcat3> objcity = new List<QL_mstcat3>();
            objcity = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2).ToList();
            SelectList obgcity = new SelectList(objcity, "cat3oid", "cat3shortdesc", 0);
            return Json(obgcity);
        }
        public ActionResult Getcat4(int cat1, int cat2, int cat3)
        {
            List<QL_mstcat4> objcity = new List<QL_mstcat4>();
            objcity = db.QL_mstcat4.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2 && g.cat3oid == cat3).ToList();
            SelectList obgcity = new SelectList(objcity, "cat4oid", "cat4shortdesc", 0);
            return Json(obgcity);
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<suppdtl> tbl = new List<suppdtl>();

            sSql = "SELECT s.cmpcode, s.suppoid,suppcode, suppname, s.activeflag, (select top 1 x.suppdtl2oid from QL_mstsuppdtl2 x where x.cmpcode=s.cmpcode and x.suppoid=s.suppoid ) suppdtl2oid, (select top 1 x.suppdtl2picname from QL_mstsuppdtl2 x where x.cmpcode=s.cmpcode and x.suppoid=s.suppoid ) picname FROM QL_mstsupp s WHERE s.cmpcode='" + cmp + "' and activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupp(string cmp)
        {
            List<mstsupp> cust = new List<mstsupp>();
            sSql = "SELECT cu.*, ci.gendesc [CityDesc] FROM QL_mstsupp cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=suppcityOid WHERE cu.cmpcode='" + cmp + "' AND UPPER(cu.activeflag)='ACTIVE'";
            cust = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetsuppData(List<suppdtl> dtDtl)
        {
            Session["QL_suppdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilsuppData()
        {
            if (Session["QL_suppdtl"] == null)
            {
                Session["QL_suppdtl"] = new List<suppdtl>();
            }

            List<suppdtl> dataDtl = (List<suppdtl>)Session["QL_suppdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileMat()
        {
            var result = "";
            var picturepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        picturepath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, picturepath);
            }
            result = "Sukses";

            return Json(new { result, picturepath }, JsonRequestBehavior.AllowGet);
        }
    }
}