﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class GeneralGFAController : Controller
    {
        // GET: GeneralGFA
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public GeneralGFAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listgeneral
        {
            public int assetacctgoid { get; set; }
            public string assetaccount { get; set; }
            public string accumdepaccount { get; set; }
            public string depaccount { get; set; }
            public int assetdepmonth { get; set; }

        }

        // GET/POST: General
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select assetacctgoid, a.acctgdesc assetaccount, a2.acctgdesc accumdepaccount, a3.acctgdesc depaccount, assetdepmonth FROM QL_mstassetgroup gn inner join QL_mstacctg a On a.acctgoid=gn.assetacctgoid inner join QL_mstacctg a2 On a2.acctgoid=gn.accumdepacctgoid inner join QL_mstacctg a3 On a3.acctgoid=gn.depacctgoid WHERE gn.cmpcode='" + CompnyCode + "' " + sfilter + " ";
            List<listgeneral> generals = db.Database.SqlQuery<listgeneral>(sSql).ToList();
            return View(generals);
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void InitDDL(QL_mstassetgroup tbl)
        {
            var assetacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_ASSET")).ToList(), "acctgoid", "acctgdesc", tbl.assetacctgoid);
            ViewBag.assetacctgoid = assetacctgoid;

            var accumdepacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_ASSET_ACCUM")).ToList(), "acctgoid", "acctgdesc", tbl.accumdepacctgoid);
            ViewBag.accumdepacctgoid = accumdepacctgoid;

            var depacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_DEPRECIATION")).ToList(), "acctgoid", "acctgdesc", tbl.depacctgoid);
            ViewBag.depacctgoid = depacctgoid;
        }

        // GET: General/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstassetgroup tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_mstassetgroup();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                tbl = db.QL_mstassetgroup.Find(Session["CompnyCode"].ToString(), id);
                action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: General/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstassetgroup tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed"; var hdrid = "";

            //Is Input Valid?
            if (tbl.assetacctgoid == 0)
                msg += "- Please Fill Asset Account!!<br/>";
            if (tbl.accumdepacctgoid == 0)
                msg += "- Please Fill Accum Dep Account!!<br/>";
            if (tbl.depacctgoid == 0)
                msg += "- Please Fill Dep Account!!<br/>";
            if (tbl.assetdepmonth <= 0)
                msg += "- Please Input Depreciation!!<br/>";
            var cek = db.QL_mstassetgroup.Where(x => x.assetacctgoid == tbl.assetacctgoid).Count();
            if (action == "Create")
            {
                if (cek == 1)
                    msg += "- Asset Account Sudah Digunakan!!<br/>";
            }
            else
            {
                if (cek > 1)
                    msg += "- Asset Account Sudah Digunakan!!<br/>";
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_mstassetgroup.Add(tbl);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = ClassFunction.GetServerTime();
                            tbl.upduser = Session["UserID"].ToString();

                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.assetacctgoid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_mstassetgroup tbl)
        {
            
        }

        // POST: General/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstassetgroup tbl = db.QL_mstassetgroup.Find(CompnyCode, id);
                        db.QL_mstassetgroup.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}