﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers
{
    public class GeneralController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounter"];
        private string sSql = "";
        public class genmst
        {
            public int genoid { get; set; }
            public string gencode { get; set; }
            public string gendesc { get; set; }
            public string gengroup { get; set; }

        }
       

        private void InitDDL(QL_mstgen tbl)
        {

            sSql = "SELECT * FROM QL_mstoid WHERE cmpcode='" + CompnyCode + "' AND tablegroup='GENERAL' ORDER BY tablename";
            var gengroup = new SelectList(db.Database.SqlQuery<QL_mstoid>(sSql).ToList(), "tablename", "tablename", tbl.gengroup) ;
            ViewBag.gengroup = gengroup;

            if (tbl.gengroup != null) { 
                if (tbl.gengroup.ToUpper() == "FORM NAME")
                {
                    sSql = "SELECT * FROM QL_mstoid WHERE cmpcode='" + CompnyCode + "' AND tablegroup='MODULE' ORDER BY tablename";
                    var ddlgenother3 = new SelectList(db.Database.SqlQuery<QL_mstoid>(sSql).ToList(), "tablename", "tablename", tbl.genother3);
                    ViewBag.ddlgenother3 = ddlgenother3;
                }
                if (tbl.gengroup.ToUpper() == "PROVINCE" || tbl.gengroup.ToUpper() == "CITY"  )
                {
                    sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY' AND activeflag='ACTIVE' ORDER BY gendesc";
                    if (tbl.gengroup.ToUpper() == "CITY")
                    { 
                        var ddlgenother2 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.genother2);
                        ViewBag.ddlgenother2 = ddlgenother2;

                        var other2 = "";

                        if (tbl.genother2 != null)
                            other2 = tbl.genother2;
                        else
                            other2 = db.Database.SqlQuery<string>("SELECT genoid FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY' AND activeflag='ACTIVE' ORDER BY gendesc").FirstOrDefault();


                        sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND genother1='" + other2 +  "' ORDER BY gendesc";
                        
                        
                        var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.genother1);
                        ViewBag.ddlgenother1 = ddlgenother1;
                    }
                    else
                    {
                        var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.genother1);
                        ViewBag.ddlgenother1 = ddlgenother1;
                    }
                }

                if (tbl.gengroup.ToUpper() == "ADJUSTMENT REASON")
                {
                    string acctgoid = ClassFunction.GetDataAcctgOid("VAR_STOCK_ADJUSTMENT", Session["CompnyCode"].ToString());
                    sSql = "SELECT acctgoid, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
                   
                    var ddlgenother1 = new SelectList(db.Database.SqlQuery<mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.genother1);
                    ViewBag.ddlgenother1 = ddlgenother1;
                }
                if (tbl.gengroup.ToUpper() == "ROOM")
                {
                    sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";


                    var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "divcode", "divname", tbl.genother1);
                    ViewBag.ddlgenother1 = ddlgenother1;
                }
                if (tbl.gengroup.ToUpper() == "FIELD OID")
                {
                    sSql = "SELECT * FROM QL_mstoid WHERE tablegroup='MASTER' OR tablegroup='TRANSACTION' ORDER BY tablename";


                    var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstoid>(sSql).ToList(), "tablename", "tablename", tbl.genother1);
                    ViewBag.ddlgenother1 = ddlgenother1;
                }
                if (tbl.gengroup.ToUpper() == "DATABASE")
                {
                    sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE' ORDER BY divname";

                    var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstoid>(sSql).ToList(), "cmpcode", "tablename", tbl.genother1);
                    ViewBag.ddlgenother1 = ddlgenother1;
                }
                if (tbl.gengroup.ToUpper() == "MOBILE ROLE DETAIL")
                {
                    sSql = "SELECT * FROM QL_mstgen WHERE gengroup='MOBILE ROLE MASTER' AND activeflag='ACTIVE' ORDER BY gendesc";

                    var ddlgenother1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.genother1);
                    ViewBag.ddlgenother1 = ddlgenother1;
                }
            }
            
        }

        private void FillAdditionalField(QL_mstgen tbl)
        {
            //ViewBag.ddlgenother3 = tbl.genother3;
            //ViewBag.potype = tbl.porawtype;
            //ViewBag.potaxtype = tbl.porawtaxtype;
        }

        [HttpPost]
        public ActionResult InitDDLModule()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstoid> tbl = new List<QL_mstoid>();
            sSql = "SELECT * FROM QL_mstoid WHERE cmpcode='" + CompnyCode + "' AND tablegroup='MODULE' ORDER BY tablename";
            tbl = db.Database.SqlQuery<QL_mstoid>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLNegara()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLPropinsi(string genother2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND genother1='" + genother2 + "' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public class mstacctg
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }

        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<mstacctg> tbl = new List<mstacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, Session["CompnyCode"].ToString());
            sSql = "SELECT acctgoid, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLBusUnit()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdivision> tbl = new List<QL_mstdivision>();
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdivision>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLField()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstoid> tbl = new List<QL_mstoid>();
            sSql = "SELECT * FROM QL_mstoid WHERE tablegroup='MASTER' OR tablegroup='TRANSACTION' ORDER BY tablename";
            tbl = db.Database.SqlQuery<QL_mstoid>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDatabase()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdivision> tbl = new List<QL_mstdivision>();
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE' ORDER BY divname";
            tbl = db.Database.SqlQuery<QL_mstdivision>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLRole()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT * FROM QL_mstgen WHERE gengroup='MOBILE ROLE MASTER' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMobileRoleMaster(string ddlgenother1)
        {
            var genother1 = "";
            genother1 = db.Database.SqlQuery<string>("SELECT genother1 FROM QL_mstgen WHERE gengroup='MOBILE ROLE MASTER' AND genoid='" + ddlgenother1 + "'").FirstOrDefault();

            return Json(new { genother1 }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("General", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account"); return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";

            sSql = "SELECT genoid [oid], gencode [Code], gendesc [Desc.], gengroup [Group] FROM QL_mstgen m WHERE m.cmpcode='" + CompnyCode + "'";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstgen");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Code")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "General") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: General/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("General", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstgen tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstgen();
                tbl.genoid = ClassFunction.GenerateID("QL_mstgen");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstgen.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: General/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstgen tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("General", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var mstoid = ClassFunction.GenerateID("QL_mstgen");
            var servertime = ClassFunction.GetServerTime();
            //var log_oid = ClassFunction.GenerateID("QL_loghist");


            if (tbl.gendesc == "1234567890")
                ModelState.AddModelError("", "Please fill CODE field!");
            //if (string.IsNullOrEmpty(tbl.matgencode))
            //    ModelState.AddModelError("", "Please fill CODE field!");
            //if (string.IsNullOrEmpty(tbl.matgenshortdesc))
            //    ModelState.AddModelError("", "Please fill SHORT DESC. field! If SHORT DESC is empty please fill with text: 'None'");
            //if (string.IsNullOrEmpty(tbl.matgenlongdesc))
            //    ModelState.AddModelError("", "Please fill LONG DESC. field! ");
            //if (string.IsNullOrEmpty(tbl.matgenpictureloc))
            //    tbl.matgenpictureloc = "~/Images/";
            //if (string.IsNullOrEmpty(tbl.matgenpicturemat))
            //    tbl.matgenpicturemat = "~/Images/";
            //if (string.IsNullOrEmpty(tbl.matgenhscode))
            //    tbl.matgenhscode = "";
            //if (tbl.matgensafetystock == 0)
            //    ModelState.AddModelError("", "SAFETY STOCK field must be more than 0!");
            //if (tbl.matgenlimitqty == 0)
            //    ModelState.AddModelError("", "Please fill ROUNDING QTY field!");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                           tbl.genoid = mstoid;

                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_mstgen.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.genoid + " WHERE tablename='QL_mstgen'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            
                        }
                        else
                        {
                            tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: General/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("General", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstgen tbl = db.QL_mstgen.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();
            var log_oid = ClassFunction.GenerateID("QL_loghist");

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }


            //Start Cek Data Exist
            sSql = "SELECT * FROM QL_oidusage WHERE cmpcode='" + CompnyCode + "' AND tblname='QL_MSTGEN'";
            List<QL_oidusage> objTblUsage = db.Database.SqlQuery<QL_oidusage>(sSql).ToList();
            string[] sColumnName = new string [objTblUsage.Count - 1];
            string[] sTable = new string[objTblUsage.Count - 1];

            for (int i = 0; i < objTblUsage.Count - 1; i++)
            {
                sColumnName[i] = objTblUsage[i].colusage;
                sTable[i] = objTblUsage[i].tblusage;
            }

            if (ClassFunction.CheckDataExists(tbl.genoid,sColumnName, sTable) == true)
            {
                result = "failed";
                msg = "This data can't be deleted because it is being used by another data!";
            }
            // Finish Cek Data Exist

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {   
                       
                        db.QL_mstgen.Remove(tbl);
                        db.SaveChanges();
                        
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult PrintReport(string stype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("General", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptGeneral.rpt"));
          

            string sWhere = " ORDER BY gengroup, gencode, gendesc ";
            report.SetParameterValue("sWhere", sWhere);
            //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            if (stype == "PDF")
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "MasterGeneral.pdf");
            }
            else
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/excel", "MasterGeneral.xls");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
