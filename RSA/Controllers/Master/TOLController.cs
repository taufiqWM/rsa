﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class TOLController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class msttol
        {
            public int toloid { get; set; }
            public string tfcode { get; set; }
            public string boxcode { get; set; }
            public string note { get; set; }
            public string status { get; set; }
        }

        public class box
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }

        private void InitDDL(QL_msttol tbl)
        {
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var tfoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.tfoid);
            ViewBag.tfoid = tfoid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE BOX' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var boxoid = new SelectList(db.Database.SqlQuery<box>(sSql).ToList(), "genoid", "gendesc", tbl.boxoid);
            ViewBag.boxoid = boxoid;
        }

      
        // GET: Bank
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("TOL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT i.*, g.gendesc [tfcode], g2.gendesc [boxcode] FROM QL_msttol i inner join QL_mstgen g on g.genoid=i.tfoid inner join ql_mstgen g2 on i.boxoid=g2.genoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY toloid";
            List<msttol> vbag = db.Database.SqlQuery<msttol>(sSql).ToList();
            return View(vbag);
        }

        // GET: Service/Form
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("TOL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_msttol tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_msttol();
                tbl.toloid = ClassFunction.GenerateID("QL_msttol");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_msttol.Find(id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_msttol tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("TOL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            //if (tbl.bankcode == null)
            //    ModelState.AddModelError("bankcode", "Kode Bank HARUS DIISI");
            
            var mstoid = ClassFunction.GenerateID("QL_msttol");
            if (action == "Update Data")
            {
                mstoid = tbl.toloid;
            }

            if (db.QL_msttol.Where(w => w.tfoid == tbl.tfoid & w.boxoid == tbl.boxoid & w.toloid != tbl.toloid).Count() > 0)
                ModelState.AddModelError("", "Tipe Box dan Flute yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan pilih Box/Flute lainnya!");
            //var dtloid = ClassFunction.GenerateID("QL_mstbankdtl");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.cmpcode = CompnyCode;
                            tbl.toloid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_msttol.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_msttol'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Bank/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("TOL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_msttol qL_msttol = db.QL_msttol.Find(Convert.ToInt32(id));

            string result = "success";
            string msg = "";
            if (qL_msttol == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.QL_msttol.Remove(qL_msttol);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
