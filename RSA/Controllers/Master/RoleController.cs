﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class RoleController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET/POST: Role
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Role", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " And " + filter.ddlfilter + " like '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " And activeflag = '" + filter.ddlstatus + "'";

                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "Select * FROM QL_mstrole WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            List<QL_mstrole> m02RL = db.Database.SqlQuery<QL_mstrole>(sSql).ToList();
            return View(m02RL);
        }

        // GET: Role/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Role", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstrole qL_mstrole;

            if (id == null)
            {
                qL_mstrole = new QL_mstrole();
                qL_mstrole.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
                Session["QL_mstroledtl"] = null;
            }
            else
            {
                qL_mstrole = db.QL_mstrole.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Edit";
                string sSql = "select * from QL_m04MN where mnoid in (select x.mnoid from QL_mstroledtl x where x.roleoid="+ id + ")" ;
                Session["QL_mstroledtl"] = db.Database.SqlQuery<roledtl>(sSql).ToList();
            }

            if (qL_mstrole == null)
            {
                return HttpNotFound();
            }

            setViewBag(qL_mstrole);
            return View(qL_mstrole);
        }
        // POST: Role/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstrole qL_mstrole, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Role", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<roledtl> list = (List<roledtl>)Session["QL_mstroledtl"];
            if (list == null)
                ModelState.AddModelError("", "Please fill Role Detail");
            else if (list.Count <= 0)
                ModelState.AddModelError("", "Please fill Role Detail");

            var mstoid = ClassFunction.GenerateID("QL_mstrole");
            var dtloid = ClassFunction.GenerateID("QL_mstroledtl");
            var servertime = ClassFunction.GetServerTime();


            if (String.IsNullOrEmpty(qL_mstrole.rolename))
                ModelState.AddModelError("rolename", "Please fill Role Name");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            qL_mstrole.roleoid = mstoid;
                            qL_mstrole.createuser = Session["UserID"].ToString();
                            qL_mstrole.createtime = servertime;
                            qL_mstrole.upduser = Session["UserID"].ToString();
                            qL_mstrole.updtime = servertime;
                            db.QL_mstrole.Add(qL_mstrole);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_mstoid set lastoid = " + mstoid + " Where cmpcode='" + CompnyCode + "' AND tablename = 'QL_mstrole'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            qL_mstrole.upduser = Session["UserID"].ToString();
                            qL_mstrole.updtime = servertime;
                            db.Entry(qL_mstrole).State = EntityState.Modified;
                            db.SaveChanges();

                            //delete detail
                            var trndtl = db.QL_mstroledtl.Where(a => a.roleoid == qL_mstrole.roleoid);
                            db.QL_mstroledtl.RemoveRange(trndtl);
                            db.SaveChanges();

                        }

                        //insert detail
                        QL_mstroledtl ql_m02RLdtl;
                        for (int i = 0; i < list.Count(); i++)
                        {
                            ql_m02RLdtl = new QL_mstroledtl();
                            ql_m02RLdtl.cmpcode = qL_mstrole.cmpcode;
                            ql_m02RLdtl.roledtloid = dtloid++;
                            ql_m02RLdtl.roleoid = qL_mstrole.roleoid;
                            ql_m02RLdtl.mnoid = list[i].mnoid;
                            ql_m02RLdtl.createuser = Session["UserID"].ToString();
                            ql_m02RLdtl.upduser = Session["UserID"].ToString();
                            ql_m02RLdtl.updtime = servertime;
                            db.QL_mstroledtl.Add(ql_m02RLdtl);
                        }
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "Update QL_mstoid set lastoid = " + dtloid + " Where tablename = 'QL_mstroledtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            setViewBag(qL_mstrole);
            return View(qL_mstrole);
        }
        
        private void setViewBag(QL_mstrole qL_mstrole)
        {
            sSql = "Select distinct mnmodule Value, mnmodule Text from QL_m04MN ";
            ViewBag.mnmodule = new SelectList(db.Database.SqlQuery<SelectListItem>(sSql).ToList(), "Value", "Text");
            sSql = "Select distinct mntype Value, mntype Text from QL_m04MN ";
            ViewBag.mntype = new SelectList(db.Database.SqlQuery<SelectListItem>(sSql).ToList(), "Value", "Text");
        }

        public ActionResult getQL_mstroledtl()
        {
            if (Session["QL_mstroledtl"] == null)
            {
                Session["QL_mstroledtl"] = new List<roledtl>();
            }

            List<roledtl> dataDtl = (List<roledtl>)Session["QL_mstroledtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_mstroledtl(List<roledtl> mdataDtl)
        {
            Session["QL_mstroledtl"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }


        // POST: Role/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("m02RL", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (db.QL_mstuserrole.Where(w => w.roleoid == id).Count() > 0)
            {
                result = "failed";
                msg += "this Role already used by User";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstroledtl.Where(a => a.roleoid == id);
                        db.QL_mstroledtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        //db.Database.ExecuteSqlCommand("delete from QL_mstworkdtl where workoid = {0}", id);

                        QL_mstrole qL_mstrole = db.QL_mstrole.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstrole.Remove(qL_mstrole);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }
            

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult GetMenu(string mnmodule, string mntype)
        {
            List<QL_m04MN> mstmenu = new List<QL_m04MN>();
            mstmenu = db.QL_m04MN.Where(w => w.mnflag == "ACTIVE" & w.mntype == mntype & w.mnmodule == mnmodule).ToList();
            SelectList menu = new SelectList(mstmenu, "mnoid", "mnname", 0);
            return Json(menu);
        }

        public class roledtl
        {
            public string mnmodule { get; set; }
            public string mntype { get; set; }
            public string mnname { get; set; }
            public int mnoid { get; set; }

        }
    }
}