﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
namespace RSA.Controllers
{
    public class DriverController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        // GET: Driver
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View(db.QL_mstdriver.ToList());
        }

      
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstdriver qL_mstdriver;
            var cmp = CompnyCode;
            string action = "New Data";
            if (id == null | cmp == null)
            {


                qL_mstdriver = new QL_mstdriver();
                qL_mstdriver.cmpcode = Session["CompnyCode"].ToString();

                qL_mstdriver.createuser = Session["UserID"].ToString();
                qL_mstdriver.createtime = ClassFunction.GetServerTime();
                qL_mstdriver.drivlicexpdate = ClassFunction.GetServerTime();
                qL_mstdriver.drivcode = generateNo();
            }
            else
            {
                action = "Update Data";
                qL_mstdriver = db.QL_mstdriver.Find(cmp, id);
                //qL_mstdiv.divtotalperson = 0;

            }

            if (qL_mstdriver == null)
            {
                return HttpNotFound();
            }

            InitAllDDL(qL_mstdriver);
            ViewBag.action = action;
            return View(qL_mstdriver);

        }

        // POST: Driver/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form( QL_mstdriver qL_mstdriver,string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            qL_mstdriver.upduser = Session["UserID"].ToString();
            qL_mstdriver.updtime = ClassFunction.GetServerTime();
            if (qL_mstdriver.drivlicexpdate <= ClassFunction.GetServerTime())
            {
                ModelState.AddModelError("drivlicexpdate", "The license expired date must more than now");
            }
            int mstoid = db.QL_mstdriver.Any() ? db.QL_mstdriver.Max(o => o.drivoid) + 1 : 1;
                if (ModelState.IsValid & IsInputValid(qL_mstdriver))
                {
              
                using (var objTrans = db.Database.BeginTransaction())
                    {
                   
                        try
                        {
                            //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor

                            if (action == "New Data")
                            {

                               
                                qL_mstdriver.drivoid = mstoid;
                                qL_mstdriver.drivcode = generateNo();
                                qL_mstdriver.createuser = Session["UserID"].ToString();
                                qL_mstdriver.createtime = ClassFunction.GetServerTime();
                               
                            
                                db.QL_mstdriver.Add(qL_mstdriver);
                                db.SaveChanges();
                            }
                            else
                            {
                               
                                 db.Entry(qL_mstdriver).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            objTrans.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                            ModelState.AddModelError("", ex.ToString());
                        }
                    }

                }

            
            ViewBag.action = action;
            InitAllDDL(qL_mstdriver);
            return View(qL_mstdriver);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id,string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdriver list = db.QL_mstdriver.Find(cmp, id);
           
            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstdriver.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void InitAllDDL(QL_mstdriver qL_mstdriver)
        {
            ViewBag.drivcityoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "CITY").ToList(), "genoid", "gendesc");
            ViewBag.drivtypeoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "DRIVER TYPE").ToList(), "genoid", "gendesc");
            ViewBag.drivcardcityoid = ViewBag.drivcityoid;
           ViewBag.drivlictypeoid = new SelectList(db.QL_mstgen.Where(w => w.activeflag == "ACTIVE" & w.gengroup == "LICENSE TYPE").ToList(), "genoid", "gendesc");

        }
        private string generateNo()//UNTUK MENGGENERATE NOMER FORM AANJWIJZING SCR OTOMATIS
        {
            string sNo = "DRV";
            //int formatCounter = 4;
            string sSql = "SELECT ISNULL(MAX(CAST(RIGHT(drivcode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstdriver WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND drivcode LIKE '" + sNo + "%'";

            string sCounter = "0000" + db.Database.SqlQuery<int>(sSql).FirstOrDefault().ToString();
            sCounter = sCounter.Substring(sCounter.Length - 4);

            sNo = sNo + sCounter;
            return sNo;
        }
        public bool IsInputValid(QL_mstdriver qL_mstdriver)
        {
            bool isValid = true;

            
            if (qL_mstdriver.drivname == null | qL_mstdriver.drivname == "")
            {
                ModelState.AddModelError("drivname", "Please fill Name");
            }
           
            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
    }
}
