﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class DepartemenController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public class listdepartemen
        {
            public int deptoid { get; set; }
            public string deptcode { get; set; }
            public string deptname { get; set; }
            public string activeflag { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DEPT/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(deptcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstdept WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND deptcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        // GET/POST: Departemen
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select d.deptoid, d.deptcode, d.deptname, d.deptcreationdate, d.activeflag FROM QL_mstdept d WHERE d.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by d.deptcode ";
            List<listdepartemen> tblmst = db.Database.SqlQuery<listdepartemen>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Departemen/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdept tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstdept();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.deptcode = generateNo(ClassFunction.GetServerTime());
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.deptcreationdate = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstdept.Find(Session["CompnyCode"].ToString(), id);     
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstdept tblmst, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tblmst.deptname))
                ModelState.AddModelError("usoid", "Please Fill Nama Dept!!");
            else if (db.QL_mstdept.Where(w => w.deptname == tblmst.deptname & w.deptoid == tblmst.deptoid & w.deptoid != tblmst.deptoid).Count() > 0)
                ModelState.AddModelError("usoid", "Nama Dept Sudah Dipakai!!");
            tblmst.deptaddress = "";
            tblmst.deptcityoid = 0;
            tblmst.deptemail = "";
            tblmst.deptmaxbasesalary = 0;
            tblmst.deptmaxperson = 0;
            tblmst.deptnote = "";
            tblmst.deptphone = "";
            tblmst.deptpic = "";
            tblmst.depttotalbasesalary = 0;
            tblmst.depttotalperson = 0;
            tblmst.deptvar = "";
            tblmst.deptacctgoid = 0;

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstdept");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.deptoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            tblmst.deptcreationdate = servertime;
                            db.QL_mstdept.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_MSTOID set lastoid = " + mstoid + " Where tablename = 'QL_mstdept'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_mstdept tblmst)
        {
            ViewBag.deptoid = new SelectList(db.QL_mstdept.Where(w => w.activeflag == "ACTIVE" | w.deptoid == tblmst.deptoid).ToList(), "deptoid", "deptoid", tblmst.deptoid);
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstdept tblmst = db.QL_mstdept.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstdept.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
