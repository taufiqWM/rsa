﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class Kategori1Controller : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori1", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and cat1oid>0 ORDER BY cat1oid DESC";
            List<ListCat1> vbag = db.Database.SqlQuery<ListCat1>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori1", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat1 tbl;
            if (id == null)
            {
                tbl = new QL_mstcat1();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_mstcat1.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat1 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori1", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.cat1code))
                ModelState.AddModelError("cat1code", "Silahkan isi Kode!");
            else if (db.QL_mstcat1.Where(w => w.cat1code == tbl.cat1code & w.cat1oid != tbl.cat1oid).Count() > 0)
                ModelState.AddModelError("cat1code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.cat1shortdesc))
                ModelState.AddModelError("cat1shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat1.Where(w => w.cat1shortdesc == tbl.cat1shortdesc & w.cat1oid != tbl.cat1oid).Count() > 0)
                ModelState.AddModelError("cat1shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat1note))
                tbl.cat1note = "";

                var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat1");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.cat1oid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.cat1longdesc = "";
                            db.QL_mstcat1.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_MSTOID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat1'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.cat1longdesc = "";
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori1", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat1 tbl = db.QL_mstcat1.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat1.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class ListCat1
        {
            public int cat1oid { get; set; }
            public string cat1code { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat1res1 { get; set; }
            public string cat1note { get; set; }
            public string activeflag { get; set; }

        }
    }
}
