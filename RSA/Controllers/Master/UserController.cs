﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace RSA.Controllers.Master
{
    public class UserController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string userimgpath = "~/Images/UserImage";
        private string imgtemppath = "~/Images/ImagesTemps";

        public class item
        {
            public string cmpcode { get; set; }
            public string profoid { get; set; }
            public string profname { get; set; }
            public string proftype { get; set; }
            public string divgroup { get; set; }
            public string activeflag { get; set; }
        }
            // GET/POST: User
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("User", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " And " + filter.ddlfilter + " like '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " And activeflag = '" + filter.ddlstatus + "'";

                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            //sSql = "Select * FROM QL_mstprof us LEFT JOIN QL_m07ST st ON st.stoid=us.stoid AND st.cmpcode=us.cmpcode WHERE us.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            sSql = "Select us.cmpcode, profoid, profname, proftype, d.gendesc divgroup, us.activeflag FROM QL_mstprof us inner join ql_mstgen d on us.divgroupoid=d.genoid WHERE us.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            List<item> mstuser = db.Database.SqlQuery<item>(sSql).ToList();

            return View(mstuser);
        }


        // GET: User/Form/5
        public ActionResult Form(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("User", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var action = "";
            QL_mstprof qL_mstprof;
            if (id == null)
            {
                qL_mstprof = new QL_mstprof();
                qL_mstprof.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "New Data";
                action = "New Data";
            }
            else
            {
                qL_mstprof = db.QL_mstprof.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Update Data";
                action = "Update Data";
            }
            
            if (qL_mstprof == null)
            {
                return HttpNotFound();
            }

            setViewBag(qL_mstprof,action);
            return View(qL_mstprof);
        }

        // POST: User/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstprof qL_mstprof, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("User", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_mstprof.profoid))
                ModelState.AddModelError("profoid", "Please Fill User ID!!");
            else if (db.QL_mstprof.Where(w => w.profoid == qL_mstprof.profoid).Count() > 0 & action == "New Data")
                ModelState.AddModelError("profoid", "This User ID already use, Please Fill Another User ID!!");
            if (db.QL_mstprof.Where(w => w.personoid == qL_mstprof.personoid & w.divgroupoid == qL_mstprof.divgroupoid).Count() > 0 & action == "Create")
                ModelState.AddModelError("", "This Employee already use, Please Fill Another Employee!!");

            if (string.IsNullOrEmpty(qL_mstprof.profname))
                ModelState.AddModelError("profname", "Please Fill User Name");

            if (string.IsNullOrEmpty(qL_mstprof.profpass))
                ModelState.AddModelError("profpass", "Please Fill Password");

            if (string.IsNullOrEmpty(qL_mstprof.proftype))
                ModelState.AddModelError("proftype", "Please Fill User Type");

            if (string.IsNullOrEmpty(qL_mstprof.profimgfile))
                qL_mstprof.profimgfile = "~/Images/";
                //ModelState.AddModelError("profimgfile", "Please Upload Foto");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        qL_mstprof.cmpcode = CompnyCode;
                        qL_mstprof.createuser = Session["UserID"].ToString();
                        qL_mstprof.createtime = ClassFunction.GetServerTime();
                        qL_mstprof.upduser = Session["UserID"].ToString();
                        qL_mstprof.updtime = ClassFunction.GetServerTime();

                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(qL_mstprof.profimgfile)))
                        {
                            var sExt = Path.GetExtension(qL_mstprof.profimgfile);
                            var sdir = Server.MapPath(userimgpath);
                            var sfilename = qL_mstprof.profoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_mstprof.profimgfile.ToLower() != (userimgpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_mstprof.profimgfile), sdir + "/" + sfilename);
                            }
                            qL_mstprof.profimgfile = userimgpath + "/" + sfilename;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            db.QL_mstprof.Add(qL_mstprof);
                            db.SaveChanges();
                            
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            db.Entry(qL_mstprof).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;

            setViewBag(qL_mstprof, action);
            return View(qL_mstprof);
        }

        private void setViewBag(QL_mstprof tbl, string action)
        {
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>("select * from ql_mstgen where gengroup='DIVGROUP' and activeflag='ACTIVE'").ToList(), "genoid", "gendesc", tbl.divgroupoid);
            if (action=="New Data")
            {
                sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' AND personoid not in (select personoid from ql_mstprof where cmpcode='" + tbl.cmpcode + "' and divgroupoid='" + tbl.divgroupoid + "') ORDER BY personname";
            }
            else
            {
                sSql = "SELECT * FROM QL_mstperson where personoid='"+tbl.personoid+"'";
            }            
            var personoid = new SelectList(db.Database.SqlQuery<QL_mstperson>(sSql).ToList(), "personoid", "personname", tbl.personoid);
            ViewBag.personoid = personoid;
        }
        [HttpPost]
        public ActionResult InitDDLPerson(string cmp)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstperson> tbl = new List<QL_mstperson>();
            sSql = "SELECT * FROM QL_mstperson WHERE activeflag='ACTIVE' /*AND personoid not in (select personoid from ql_mstprof where divgroupoid=" + cmp + ")*/ ORDER BY personname";
            tbl = db.Database.SqlQuery<QL_mstperson>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Menu", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (id.ToLower() == "admin")
            {
                result = "failed";
                msg += "this User Can't be Deleted";
            }
            else if(db.QL_mstuserrole.Where(w => w.profoid == id).Count() > 0)
            {
                result = "failed";
                msg += "this User already used in Another Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        QL_mstprof qL_mstprof = db.QL_mstprof.Find(Session["CompnyCode"].ToString(), id);

                        if (qL_mstprof != null)
                        {
                            if (System.IO.File.Exists(Server.MapPath(qL_mstprof.profimgfile)))
                            {
                                System.IO.File.Delete(Server.MapPath(qL_mstprof.profimgfile));
                            }
                        }
                        db.QL_mstprof.Remove(qL_mstprof);
                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        //return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }
            
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }
    }
}
