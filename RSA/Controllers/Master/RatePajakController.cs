﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class RatePajakController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        // GET: RatePajak
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            return View(db.QL_mstrate.ToList());
        }

        // GET: RatePajak/Details/5

        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstrate qL_mstrate;
            cmp = CompnyCode;
            string action = "New Data";
            if (id == null )
            {


                qL_mstrate = new QL_mstrate();
                qL_mstrate.cmpcode = Session["CompnyCode"].ToString();
                
                qL_mstrate.createuser = Session["UserID"].ToString();
                qL_mstrate.createtime = ClassFunction.GetServerTime();
                qL_mstrate.ratedate = ClassFunction.GetServerTime();
                qL_mstrate.ratetodate = qL_mstrate.ratedate;
            }
            else
            {
                action = "Update Data";
                qL_mstrate = db.QL_mstrate.Find(cmp, id);
                //qL_mstdiv.divtotalperson = 0;

            }

            if (qL_mstrate == null)
            {
                return HttpNotFound();
            }

            InitAllDDL(qL_mstrate);
            ViewBag.action = action;
            return View(qL_mstrate);

        }

        // POST: RatePajak/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstrate qL_mstrate,string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //qL_mstrate.ratetodate= qL_mstrate.ratedate.AddDays(7);
            if (qL_mstrate.ratedate > qL_mstrate.ratetodate)
            {
                ModelState.AddModelError("ratedate", "Start Date must be less than End Date");

            }
            if (qL_mstrate.ratetodate == qL_mstrate.ratedate)
            {
                qL_mstrate.ratetodate = qL_mstrate.ratedate.AddDays(7);
            }
            if (qL_mstrate.rateidrvalue == 0)
            {
                ModelState.AddModelError("rateidrvalue", "Please fill Rate to IDR");
            }
            if(qL_mstrate.rateusdvalue == 0)
            {
                ModelState.AddModelError("rateusdvalue", "Please fill Rate to USD");
            }
            int mstoid = db.QL_mstrate.Any() ? db.QL_mstrate.Max(o => o.rateoid) + 1 : 1;
            if (ModelState.IsValid & IsInputValid(qL_mstrate))
            {

                using (var objTrans = db.Database.BeginTransaction())
                {

                    try
                    {
                        //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor

                        if (action == "New Data")
                        {

                            qL_mstrate.rateoid = mstoid;
                           
                            qL_mstrate.upduser = qL_mstrate.createuser;
                            qL_mstrate.updtime = qL_mstrate.createtime;
                            
                            db.QL_mstrate.Add(qL_mstrate);
                            db.SaveChanges();
                        }
                        else
                        {
                            qL_mstrate.updtime = ClassFunction.GetServerTime();
                            qL_mstrate.upduser = Session["UserID"].ToString();
                           
                            db.Entry(qL_mstrate).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }

            }


            ViewBag.action = action;
            InitAllDDL(qL_mstrate);
            return View(qL_mstrate);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id,string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstrate list = db.QL_mstrate.Find(cmp, id);

            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstrate.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        private void InitAllDDL(QL_mstrate qL_mstrate)
        {
           ViewBag.curroid= new SelectList(db.QL_mstcurr.Where(w => w.activeflag == "ACTIVE" & w.currcode !="IDR").ToList(), "curroid", "currcode");

        }
        public bool IsInputValid(QL_mstrate qL_mstrate)
        {
            bool isValid = true;

           
            if (qL_mstrate.rateidrvalue <= 0 )
            {
                ModelState.AddModelError("rateidrvalue", "Rate to IDR field must be more than 0");
            }
            if(qL_mstrate.rateusdvalue <= 0)
            {
                ModelState.AddModelError("rateusdvalue", "Rate to USD field must be more than 0");
            }
           
            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
