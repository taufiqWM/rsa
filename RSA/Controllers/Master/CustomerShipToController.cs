﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers
{
    public class CustomerShipToController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql;
        public class mstcust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }

        }
        [HttpPost]
        public ActionResult GetCustData(string divgroupoid)
        {
            List<mstcust> tbl = new List<mstcust>();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "c", divgroupoid, "divgroupoid");

            sSql = "SELECT custoid, custcode, custname,custaddr from QL_mstcust c where  cmpcode='" + CompnyCode + "' AND  activeflag='ACTIVE' and custoid>0 "+sqlplus+" order by custname ";
            tbl = db.Database.SqlQuery<mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitDDLProvince( int country)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT genoid, (gencode + ' - ' + gendesc) gendesc, * FROM QL_mstgen WHERE  cmpcode='" + CompnyCode + "' AND gengroup='PROVINCE' AND genother1='" + country + "' AND activeflag='ACTIVE' ORDER BY gendesc";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitDDLCity(string cmpcode, int province, int country)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstgen> tbl = new List<QL_mstgen>();
            sSql = "SELECT genoid, gendesc, * FROM QL_mstgen WHERE cmpcode='" + cmpcode + "' AND gengroup='CITY' AND genother1='" + province + "' AND genother2='" + country + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        // GET: CustomerShipTo
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var uslevel = db.Database.SqlQuery<string>("SELECT isnull(proflevel,'USER') from QL_mstprof where profoid='" + Session["UserID"] + "'").FirstOrDefault();
            var divgroupoid = Session["DivGroup"].ToString();
            if (uslevel == "USER")
            {
                sSql += "select * from QL_mstcustdtl where divgroupoid="+divgroupoid;
            }
            else
            {
                sSql += "select * from QL_mstcustdtl";
            }

            List<QL_mstcustdtl> vbag = db.Database.SqlQuery<QL_mstcustdtl>(sSql).ToList();
            return View(vbag);
        }

       
        // GET: CustomerShipTo/Create
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var cmp = CompnyCode;
            QL_mstcustdtl tbl;

            string action = "New Data";
            if (id == null | cmp == null)
            {


                tbl = new QL_mstcustdtl();
                tbl.cmpcode = Session["CompnyCode"].ToString();

                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstcustdtl.Find(cmp, id);
                //qL_mstdiv.divtotalperson = 0;

            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            InitAllDDL(tbl);
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: CustomerShipTo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form( QL_mstcustdtl tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            int mstoid = db.QL_mstcustdtl.Any() ? db.QL_mstcustdtl.Max(o => o.custdtloid) + 1 : 1;
            if (ModelState.IsValid & IsInputValid(tbl))
            {

                using (var objTrans = db.Database.BeginTransaction())
                {

                    try
                    {
                        //semua data harus di deklarasikan krn semua field di database hrs terisi data bisa dideklarasikan di form pada bagian HiddenFor

                        if (action == "New Data")
                        {

                            tbl.custdtloid = mstoid;

                            tbl.upduser = tbl.createuser;
                            tbl.updtime = tbl.createtime;

                            db.QL_mstcustdtl.Add(tbl);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = ClassFunction.GetServerTime();
                            tbl.upduser = Session["UserID"].ToString();

                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }

            }


            ViewBag.action = action;
            InitAllDDL(tbl);
            return View(tbl);
        }


        private void InitAllDDL(QL_mstcustdtl tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            ViewBag.custname = db.QL_mstcust.Where(w => w.custoid == tbl.custoid).Select(s => s.custname).DefaultIfEmpty("").First();


            sSql = "SELECT genother2 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND genoid='" + tbl.custdtlcityoid+"'";
            var countryTemp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            ViewBag.countryTemp = countryTemp;

            var iCustCountry = "";
            var iCustProvince = "";
            var iCustCity = 0;
            if (tbl.custdtlcityoid != 0)
            {
                iCustCountry = db.Database.SqlQuery<string>("SELECT genother2 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + tbl.custdtlcityoid + " ORDER BY gendesc").FirstOrDefault();
                iCustProvince = db.Database.SqlQuery<string>("SELECT genother1 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + tbl.custdtlcityoid + " ORDER BY gendesc").FirstOrDefault();
                iCustCity = tbl.custdtlcityoid;
            }
            else
            {
                iCustCity = db.Database.SqlQuery<int>("SELECT top 1 genoid FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "'  AND gengroup='CITY' ORDER BY (CASE genother2 WHEN (SELECT genoid FROM QL_mstgen WHERE gengroup='COUNTRY' AND gendesc like '%INDONESIA%') THEN 1 ELSE 2 END)").FirstOrDefault();
                iCustCountry = db.Database.SqlQuery<string>("SELECT genother2 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + iCustCity + " ORDER BY gendesc").FirstOrDefault();
                iCustProvince = db.Database.SqlQuery<string>("SELECT genother1 FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genoid=" + iCustCity + " ORDER BY gendesc").FirstOrDefault();
            }

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='CITY' AND genother1='" + iCustProvince + "' AND genother2='" + iCustCountry + "' ORDER BY gendesc";
            var custcityoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", iCustCity);
            ViewBag.custdtlcityoid = custcityoid;

            var custprovince = db.QL_mstgen.Where(x => x.genother1 == iCustCountry && x.gengroup == "PROVINCE")
                  .Select(x => new SelectListItem
                  {
                      Value = x.genoid.ToString(),
                      Text = x.gencode + " - " + x.gendesc.ToString()
                  });

            ViewBag.province = new SelectList(custprovince, "Value", "Text", iCustProvince);

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='COUNTRY' AND activeflag='ACTIVE'  ORDER BY gendesc";
            var custcountry = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", iCustCountry);
            ViewBag.country = custcountry;
        }

        // POST: CustomerShipTo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id,string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcustdtl list = db.QL_mstcustdtl.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.QL_mstcustdtl.Remove(list);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        public bool IsInputValid(QL_mstcustdtl tbl)
        {
            bool isValid = true;

            if (db.QL_mstcustdtl.Where(w => w.custdtlcode == tbl.custdtlcode & w.custdtloid != tbl.custdtloid).Count() > 0)
            {
                ModelState.AddModelError("custdtlcode", "Code has been used by another data. Please fill another Code!");
            }
            if (db.QL_mstcustdtl.Where(w => w.custoid == tbl.custoid & w.custdtloid != tbl.custdtloid & w.custdtlname == tbl.custdtlname).Count() > 0)
            {
                ModelState.AddModelError("custoid", "Name has been used by another data. Please fill another Name!");
            }
            if (tbl.custoid == 0)
            {
                ModelState.AddModelError("custoid", "Please choose Customer field");
            }
            if (tbl.custdtlcode == "" | tbl.custdtlcode == null)
            {
                ModelState.AddModelError("custdtlcode", "Please fill Code");
            }
            if (tbl.custdtlname == "" | tbl.custdtlname == null)
            {
                ModelState.AddModelError("custdtlname", "Please fill Name");

            }
            if (tbl.custdtlcityoid == 0)
            {
                ModelState.AddModelError("custdtlcityoid", "Please select City Field");
            }
            if (!ModelState.IsValid)
            {
                isValid = false;

            }
            return isValid;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
