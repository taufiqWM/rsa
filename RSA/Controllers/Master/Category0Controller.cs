﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace RSA.Controllers.Master
{
    public class Category0Controller : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounter"];
        private string sSql = "";

        public class Listcat0
        {
            public int cat0oid { get; set; }
            public string cat0code { get; set; }
            public string cat0desc { get; set; }
            public string activeflag { get; set; }
            public string cat0note { get; set; }
        }

        public class Listcat0dtl
        {
            public int cat0dtlseq { get; set; }
            public int cat1oid { get; set; }
            public string cat1desc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            List<Listcat0dtl> tbl = new List<Listcat0dtl>();
            string sSql = "";
            
            sSql = "SELECT 0 AS cat0dtlseq, cat1oid, ((CASE cat1res1 WHEN 'Raw' THEN 'RM' WHEN 'General' THEN 'GM' WHEN 'Spare Part' THEN 'SP' ELSE 'FG' END) +'.' + cat1code + ' - ' + cat1shortdesc) AS cat1desc FROM QL_mstcat1 WHERE cmpcode = '" + CompnyCode + "' AND activeflag = 'ACTIVE' AND cat1oid NOT IN (SELECT cat1oid FROM QL_mstcat0dtl WHERE cmpcode = '" + CompnyCode + "') ORDER BY cat1res1, cat1res2, cat1code";  

               tbl = db.Database.SqlQuery<Listcat0dtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<Listcat0dtl> dtDtl)
        {
            Session["QL_mstcat0dtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstcat0dtl"] == null)
            {
                Session["QL_mstcat0dtl"] = new List<Listcat0dtl>();
            }

            List<Listcat0dtl> dataDtl = (List<Listcat0dtl>)Session["QL_mstcat0dtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_mstcat0 tbl)
        {
            //ViewBag.sres1 = db.Database.SqlQuery<string>("SELECT cat0res1 FROM QL_mstcat0 WHERE cat0oid='" + tbl.cat0oid + "'").FirstOrDefault();
            //ViewBag.sres2 = db.Database.SqlQuery<string>("SELECT cat0res2 FROM QL_mstcat0 WHERE cat0oid='" + tbl.cat0oid + "'").FirstOrDefault();
        }

        // GET: Kategory
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("Category0", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";

            sSql = "SELECT cat0oid [oid], cat0code [Code], cat0desc [Description], activeflag [Status], cat0note [Note] FROM QL_mstcat0 WHERE cmpcode='" + CompnyCode + "'";

            //if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
            //    sSql += " AND createuser='" + Session["UserID"].ToString() + "'";

            sSql += " ORDER BY cat0code";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstcat0");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Code")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "Category0") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("Category0", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat0 tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstcat0();
                tbl.cmpcode = CompnyCode;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

                Session["QL_mstcat0dtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstcat0.Find(CompnyCode, id);

                sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY d.cat0dtloid) AS INT) AS cat0dtlseq, c.cat1oid, ((CASE c.cat1res1 WHEN 'Raw' THEN 'RM' WHEN 'General' THEN 'GM' WHEN 'Spare Part' THEN 'SP' ELSE 'FG' END) +'.' + c.cat1code + ' - ' + c.cat1shortdesc) AS cat1desc FROM QL_mstcat0dtl d INNER JOIN QL_mstcat1 c ON c.cat1oid=d.cat1oid WHERE d.cmpcode='" + CompnyCode + "' AND d.cat0oid="+ id +"";
                Session["QL_mstcat0dtl"] = db.Database.SqlQuery<Listcat0dtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat0 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("Category0", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.cat0code))
                ModelState.AddModelError("cat0code", "Please fill CODE field!");
            else
            {
                if (db.QL_mstcat0.Where(w => w.cat0code == tbl.cat0code && w.cat0oid != tbl.cat0oid).Count() > 0)
                    ModelState.AddModelError("cat0code", "Code have been used by another data. Please fill another Code!");
                if (tbl.cat0code.Length > 10)
                    ModelState.AddModelError("cat0code", "Length of CODE must be 10 characters!");
            }
            if (string.IsNullOrEmpty(tbl.cat0desc))
                ModelState.AddModelError("cat0shortdesc", "Please fill DESCRIPTION field!");
            else
            {
                if (db.QL_mstcat0.Where(w => w.cat0desc == tbl.cat0desc && w.cat0oid != tbl.cat0oid).Count() > 0)
                    ModelState.AddModelError("cat0desc", "Description have been used by another data. Please fill another Description!");
                if (tbl.cat0desc.Length > 100)
                    ModelState.AddModelError("cat0desc", "Length of Description must be 100 characters!");
            }
            if (string.IsNullOrEmpty(tbl.cat0note))
                tbl.cat0note = "";
            
            tbl.cmpcode = CompnyCode;

            List<Listcat0dtl> dtDtl = (List<Listcat0dtl>)Session["QL_mstcat0dtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (ModelState.IsValid)
            {
                var servertime = ClassFunction.GetServerTime();
                var mstoid = ClassFunction.GenerateID("QL_mstcat0");
                var dtloid = ClassFunction.GenerateID("QL_mstcat0dtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.cat0oid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat0.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat0'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstcat0dtl.Where(a => a.cat0oid == tbl.cat0oid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstcat0dtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_mstcat0dtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_mstcat0dtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cat0dtloid = dtloid++;
                            tbldtl.cat0oid = tbl.cat0oid;
                            tbldtl.cat1oid = dtDtl[i].cat1oid;
                            tbldtl.cat0dtlnote = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_mstcat0dtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_mstcat0dtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission("Category0", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat0 tbl = db.QL_mstcat0.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstcat0dtl.Where(a => a.cat0oid == id && a.cmpcode == CompnyCode);
                        db.QL_mstcat0dtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_mstcat0.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}