﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;

namespace RSA.Controllers.Master
{
    public class CoaController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET/POST: COA
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("COA", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = "";// ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "i", divgroupoid, "divgroupoid");
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " And " + filter.ddlfilter + " like '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " And activeflag = '" + filter.ddlstatus + "'";

                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT *,Case acctgdbcr When 'D' Then 'DEBET' Else 'CREDIT' End dc FROM QL_mstacctg i WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + sqlplus + " order by Acctgcode ASC";
            List<listCoa> coa = db.Database.SqlQuery<listCoa>(sSql).ToList();
            return View(coa);
        }


        // GET: COA/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("COA", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstacctg QL_mstacctg;
            string action = "Create";
            if (id == null)
            {
                QL_mstacctg = new QL_mstacctg();
                QL_mstacctg.cmpcode = Session["CompnyCode"].ToString();
                QL_mstacctg.createuser = Session["UserID"].ToString();
                QL_mstacctg.createtime = ClassFunction.GetServerTime();               
            }
            else
            {
                action = "Edit";
                QL_mstacctg = db.QL_mstacctg.Find(Session["CompnyCode"].ToString(), id);                
            }

            if (QL_mstacctg == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(QL_mstacctg);
            FillAdditionalField(QL_mstacctg);
            return View(QL_mstacctg);
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstacctg QL_mstacctg, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("COA", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //Is Inputvalid
            if (db.QL_mstacctg.Where(w => w.acctgcode == QL_mstacctg.acctgcode && w.acctgoid != QL_mstacctg.acctgoid).Count() > 0)
            {
                ModelState.AddModelError("acctgcode", "Kode akun sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya..!!");
            }
            if (db.QL_mstacctg.Where(w => w.acctgdesc == QL_mstacctg.acctgdesc & w.acctgoid != QL_mstacctg.acctgoid).Count() > 0)
                ModelState.AddModelError("acctgdesc", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstacctg");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            QL_mstacctg.acctgoid = mstoid;
                            QL_mstacctg.createtime = servertime;
                            QL_mstacctg.createuser = Session["UserID"].ToString();
                            QL_mstacctg.updtime = servertime;
                            QL_mstacctg.upduser = Session["UserID"].ToString();
                            db.QL_mstacctg.Add(QL_mstacctg);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update ql_mstoid set lastoid = " + mstoid + " Where tablename = 'QL_mstacctg'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            QL_mstacctg.updtime = servertime;
                            QL_mstacctg.upduser = Session["UserID"].ToString(); 
                            db.Entry(QL_mstacctg).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(QL_mstacctg);
            FillAdditionalField(QL_mstacctg);
            return View(QL_mstacctg);
        }

        public class listDiv
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }

        private void InitDDL(QL_mstacctg tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from(select 0 genoid, 'NONE' gendesc union all select genoid, gendesc from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + ") as tb";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<listDiv>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode",tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private void FillAdditionalField(QL_mstacctg tbl)
        {
            ViewBag.acctgcodeind = db.Database.SqlQuery<string>("SELECT acctgcode FROM QL_mstacctg a WHERE CAST(a.acctgoid AS VARCHAR(30)) ='" + tbl.acctggrp3 + "'").FirstOrDefault();

            ViewBag.acctgcode1 = db.Database.SqlQuery<string>("SELECT acctgcode FROM QL_mstacctg a WHERE CAST(a.acctgoid AS VARCHAR(30)) ='" + tbl.acctggrp3 + "'").FirstOrDefault();
        }


        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("COA", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstacctg QL_mstacctg = db.QL_mstacctg.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstacctg.Remove(QL_mstacctg);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
        public class listCoa
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctggrp1 { get; set; }
            public string acctgdbcr { get; set; }
            public string acctgnote { get; set; }
            public string activeflag { get; set; }        
            public int acctgoid1 { get; set; }
            public int curroid { get; set; }
            public string acctgcodeseq { get; set; }
            public string dc { get; set; }
        }

        public class dataBANH
        {
            public string acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctggrp2 { get; set; }
        }
                        
        [HttpPost]
        public ActionResult getCoa(string acctggrp1)
        {
            List<dataBANH> getCoa = new List<dataBANH>();
            sSql = "SELECT CAST (acctgoid as VARCHAR(30)) acctgoid, acctgcode, acctgdesc, acctggrp2 FROM ql_mstacctg WHERE acctggrp1='"+ acctggrp1 + "' AND activeflag='ACTIVE' ORDER BY acctgcode";
            getCoa = db.Database.SqlQuery<dataBANH>(sSql).ToList();
            return Json(getCoa, JsonRequestBehavior.AllowGet);
        }
    }
}
