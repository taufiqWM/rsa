﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RSA.Models.DB;
using RSA.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
//using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Controllers.Master
{
    public class MatRawSheetController : Controller
    {
        private QL_RSAEntities db = new QL_RSAEntities();
        private string imgpath = "~/Images/RawImages";
        private string imgtemppath = "~/Images/ImagesTemps";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";


        public class matraw
        {
            public string cmpcode { get; set; }
            public int matrawoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string matrawcode { get; set; }
            public string matrawunit { get; set; }
            public string matrawdesc { get; set; }
            public string matrawtype { get; set; }
            public string activeflag { get; set; }

        }
        public class suppdtl
        {
            public int matrawdtloid { get; set; }
            public int matrawoid { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string picname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
            public string activeflag { get; set; }
          
        }

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string CityDesc { get; set; }
            public string suppphone1 { get; set; }
        }

        public class mstmanufacture
        {
            public int manufactureoid { get; set; }
            public string manufacturecode { get; set; }
            public string manufacturename { get; set; }
            public string manufactureaddr { get; set; }
            public string citydesc { get; set; }
            public string manufacturephone1 { get; set; }
        }

        public class mstmatrawlocation
        {
            public int locoid { get; set; }
            public string locdesc { get; set; }
        }
        private string generateNo()
        {
            var sNo = "RM-";

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matrawcode, 9) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstmatraw WHERE cmpcode='" + CompnyCode + "' AND matrawcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 9);
            sNo = sNo + sCounter;

            return sNo;
        }
        private void InitDDL(QL_mstmatraw tbl)
        {
            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "g", divgroupoid, "genoid");
            sSql = "select * from ql_mstgen g where gengroup='DIVGROUP' and activeflag='ACTIVE' " + sqlplus + "";
            ViewBag.divgroupoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.divgroupoid);

            //substance
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub1 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub1);
            ViewBag.sub1 = sub1;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub2 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub2);
            ViewBag.sub2 = sub2;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub3 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub3);
            ViewBag.sub3 = sub3;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub4 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub4);
            ViewBag.sub4 = sub4;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "'  AND activeflag='ACTIVE' AND cat4code!='-' ORDER BY cat4code";
            var sub5 = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4code", tbl.sub5);
            ViewBag.sub5 = sub5;

            //machine
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine1 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine1);
            ViewBag.machine1 = machine1;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine2 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine2);
            ViewBag.machine2 = machine2;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine3 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine3);
            ViewBag.machine3 = machine3;
            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='MACHINE' AND activeflag='ACTIVE' AND gencode!='-' ORDER BY gendesc";
            var machine4 = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.machine4);
            ViewBag.machine4 = machine4;

            //UNIT
            sSql = "select * from QL_mstgen where cmpcode='" + CompnyCode + "' AND gengroup = 'MATERIAL UNIT'";
            var matrawunitoid= new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.matrawunitoid);
            ViewBag.matrawunitoid = matrawunitoid;

            //WAREHOUSE / LOCATION
            sSql = "SELECT genoid [locoid], gendesc [locdesc] FROM QL_mstgen WHERE cmpcode='" + CompnyCode + "' AND gengroup='MATERIAL LOCATION' AND gendesc='GUDANG BAHAN BAKU' AND activeflag='ACTIVE'";
            var matrawlocoid = new SelectList(db.Database.SqlQuery<mstmatrawlocation>(sSql).ToList(), "locoid", "locdesc", tbl.matrawlocoid);
            ViewBag.matrawlocoid = matrawlocoid;

            sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=2 ORDER BY gendesc";
            if (tbl.tipewall == "DW")
            {
                sSql = "SELECT * FROM QL_mstgen WHERE cmpcode='" + tbl.cmpcode + "' AND gengroup='TIPE FLUTE' AND activeflag='ACTIVE' AND gencode!='-' and len(gendesc)=3 ORDER BY gendesc";
            }
            var fluteoid = new SelectList(db.Database.SqlQuery<QL_mstgen>(sSql).ToList(), "genoid", "gendesc", tbl.fluteoid);
            ViewBag.fluteoid = fluteoid;
        }

        [HttpPost]
        public ActionResult Getflute(string fl)
        {
            List<QL_mstgen> objgen = new List<QL_mstgen>();
            if (fl == "SW")
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=2 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            else
            {
                sSql = "select * from QL_mstgen where gengroup='Tipe Flute' and len(gendesc)=3 and cmpcode='" + CompnyCode + "' and activeflag='ACTIVE' ORDER BY gendesc";
            }
            objgen = db.Database.SqlQuery<QL_mstgen>(sSql).ToList();
            SelectList DDLCat2 = new SelectList(objgen, "genoid", "gendesc", 0);

            return Json(DDLCat2);
        }
        [HttpPost]
        public ActionResult Getsub1(string sub1)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub1 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub2(string sub2)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub2 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub3(string sub3)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub3 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub4(string sub4)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub4 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        [HttpPost]
        public ActionResult Getsub5(string sub5)
        {
            sSql = "select cat3code from ql_mstcat3 where cmpcode ='" + CompnyCode + "' and cat3oid=(select cat3oid from ql_mstcat4 where cat4code='" + sub5 + "')";
            var tmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return Json(tmp);
        }
        public class wlap
        {
            public decimal berat1 { get; set; }
            public decimal berat2 { get; set; }
            public decimal berat3 { get; set; }
            public decimal berat4 { get; set; }
            public decimal berat5 { get; set; }
        }
        [HttpPost]
        public ActionResult getwlap(int boxoid, int fluteoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5, decimal p, decimal l)
        {


            List<wlap> w1 = new List<wlap>();

            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();



            var luas = (p * l) / 1000000;
            var w = (luas * (g1 / 1000));
            var w2 = ((luas * (g2 / 1000)) * tuf);
            var w3 = (luas * (g3 / 1000));
            var w4 = ((luas * (g4 / 1000)) * tuf2);
            var w5 = (luas * (g5 / 1000));
            w1.Add(new wlap { berat1 = w, berat2 = w2, berat3 = w3, berat4 = w4, berat5 = w5 });

            return Json(w1);
        }


        [HttpPost]
        public ActionResult getw1(int boxoid, int fluteoid, decimal g1, decimal g2, decimal g3, decimal g4, decimal g5, decimal p, decimal l)
        {

            sSql = "select isnull(tuf,0) tuf from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "select isnull(tuf2,0) tuf2 from ql_msttuf where tfoid='" + fluteoid + "'";
            var tuf2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();



            var luas = (p * l) / 1000000;
            var total = (luas * (g1 / 1000)) + ((luas * (g2 / 1000)) * tuf) + (luas * (g3 / 1000)) + ((luas * (g4 / 1000)) * tuf2) + (luas * (g5 / 1000));

            return Json(total);
        }


        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var divgroupoid = Session["DivGroup"].ToString();
            var sqlplus = ClassFunction.FilterDataByUserLevel(Session["UserID"].ToString(), "i", divgroupoid, "divgroupoid");

            sSql = "SELECT i.*, i.matrawlongdesc [matrawdesc], g.gendesc [matrawunit] FROM QL_mstmatraw i inner join ql_mstgen g ON g.genoid=i.matrawunitoid WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "'"+sqlplus+ " AND i.matrawres3='Sheet'";
            List<matraw> vbag = db.Database.SqlQuery<matraw>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstmatraw tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_mstmatraw();
                tbl.matrawoid = ClassFunction.GenerateID("QL_mstmatraw");
              
                //tbl.cat4oid = 0;
                tbl.manufactureoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.matrawsafetystock = 1;
                tbl.matrawlimitqty = 1;

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstmatraw.Find(cmp, id);

                //sSql = "SELECT matrawdtloid, matrawoid, s.cmpcode, s.suppoid,suppcode, suppname, s.activeflag, ISNULL(cperson1, '') [picname] FROM QL_mstsupp s INNER JOIN QL_mstmatrawdtl r ON r.cmpcode=s.cmpcode AND r.suppoid=s.suppoid WHERE s.cmpcode='" + cmp + "' and activeflag='ACTIVE' ORDER BY suppname";
                sSql = "SELECT r.suppoid, suppcode, suppname, ISNULL(cperson1, '') [picname], suppaddr, gendesc [CityDesc], suppphone1, s.activeflag " +
                    "FROM QL_mstmatrawdtl r " +
                    "INNER JOIN QL_mstsupp s ON r.cmpcode = s.cmpcode AND r.suppoid = s.suppoid " +
                    "INNER JOIN QL_mstgen g ON g.cmpcode = s.cmpcode AND g.genoid = s.suppcityOid " +
                    "WHERE s.cmpcode = '" + cmp + "' AND matrawoid = " + id + " ORDER BY suppname";
                Session["QL_mstmatrawdtl"] = db.Database.SqlQuery<suppdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstmatraw tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Barang", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


           
            


            if (tbl.matrawsafetystock == 0)
                ModelState.AddModelError("matrawsafetystock", "Stok aman HARUS DIISI.");
            if (tbl.matrawlimitqty == 0)
                ModelState.AddModelError("matrawlimitqty", "Round Qty HARUS DIISI.");
            if (tbl.matrawlocoid == 0)
                ModelState.AddModelError("matrawlocoid", "Lokasi gudang HARUS DIISI.");

            tbl.matrawlength = 0;
            tbl.matrawres2 = "0";
            tbl.matrawwidth = 0;
            tbl.matrawheight = 0;
            tbl.matrawdiameter = 0;
            tbl.matrawvolume = 0;
            tbl.flagbatch = false;

            tbl.matrawshortdesc = tbl.matrawshortdesc.ToUpper();
            tbl.matrawlongdesc = tbl.matrawshortdesc;
            var mstoid = ClassFunction.GenerateID("QL_mstmatraw");
            var dtloid = ClassFunction.GenerateID("QL_mstmatrawdtl");
            if (action == "Update Data")
            {
                mstoid = tbl.matrawoid;
            }
            //var dtloid = ClassFunction.GenerateID("QL_mstmatrawdtl");
            var servertime = ClassFunction.GetServerTime();
            tbl.matrawshortdesc = tbl.matrawshortdesc == null ? "" : tbl.matrawshortdesc;
            tbl.matrawbarcode = "";
            tbl.manufactureoid = 0;
            tbl.suppoid = 0;
            tbl.matrawavgsales = 0;
            tbl.matrawlastpoprice = 0;
            tbl.matrawavgsales = 0;
            tbl.matrawavgpurchase = 0;
            tbl.matrawlength = tbl.matrawlength == null ? 0 : tbl.matrawlength;
            tbl.matrawheight = tbl.matrawheight == null ? 0 : tbl.matrawheight;
            tbl.matrawwidth = tbl.matrawwidth == null ? 0 : tbl.matrawwidth;
            tbl.matrawdiameter = tbl.matrawdiameter == null ? 0 : tbl.matrawdiameter;
            tbl.matrawvolume = tbl.matrawvolume == null ? 0 : tbl.matrawvolume;
            tbl.matrawnote = tbl.matrawnote == null ? "" : tbl.matrawnote;
            tbl.matrawres1 = "";
            tbl.matrawres2 = tbl.matrawres2 == null ? "0" : tbl.matrawres2;
            tbl.matrawres3 = "Sheet";
            tbl.matrawunit2oid = 0;
            tbl.matrawconvertunit = 0;
          
            //tbl.cat2oid = tbl.cat2oid == null ? 0 : tbl.cat2oid;
            //tbl.cat3oid = tbl.cat3oid == null ? 0 : tbl.cat3oid;
            //tbl.cat4oid = tbl.cat4oid == null ? 0 : tbl.cat4oid;
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        

                        //Upload
                        //matrawpictureloc
                        if (System.IO.File.Exists(Server.MapPath(tbl.matrawpictureloc)))
                        {
                            var sExt = Path.GetExtension(tbl.matrawpictureloc);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesRaw_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matrawpictureloc) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matrawpictureloc), sdir + "/" + sfilename);
                            }

                            tbl.matrawpictureloc = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matrawpictureloc = tbl.matrawpictureloc == null ? "" : tbl.matrawpictureloc;
                        }

                        //matrawpicturemat
                        if (System.IO.File.Exists(Server.MapPath(tbl.matrawpicturemat)))
                        {
                            var sExt = Path.GetExtension(tbl.matrawpicturemat);
                            var sdir = Server.MapPath(imgpath);
                            var sfilename = "ImagesRawMat_" + mstoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (Server.MapPath(tbl.matrawpicturemat) != sdir + "/" + sfilename)
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.matrawpicturemat), sdir + "/" + sfilename);
                            }

                            tbl.matrawpicturemat = imgpath + "/" + sfilename;
                        }
                        else
                        {
                            tbl.matrawpicturemat = tbl.matrawpicturemat == null ? "" : tbl.matrawpicturemat;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.matrawoid = mstoid;
                            tbl.matrawcode = generateNo();
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstmatraw.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_mstoid SET lastoid=" + mstoid + " WHERE cmpcode='" + CompnyCode + "' AND tablename='QL_mstmatraw'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstmatrawdtl.Where(a => a.matrawoid == tbl.matrawoid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstmatrawdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                     
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("Kategori4", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult Getcat2(int cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT c2.* FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c2.cat1oid=c1.cat1oid WHERE c2.cmpcode='" + CompnyCode + "' AND c2.cat1oid=" + cat1 + " AND c2.activeflag='ACTIVE' And cat1res1='Raw' ORDER BY cat2shortdesc";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
         

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Getcat3(int cat1,int cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            tbl = db.QL_mstcat3.Where(g => g.activeflag == "ACTIVE" && g.cat1oid==cat1 && g.cat2oid == cat2).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Getcat4(int cat1, int cat2, int cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            tbl = db.QL_mstcat4.Where(g => g.activeflag == "ACTIVE" && g.cat1oid == cat1 && g.cat2oid == cat2 && g.cat3oid == cat3).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSuppData(string cmp)
        {
            List<suppdtl> tbl = new List<suppdtl>();

            sSql = "SELECT matrawdtloid, matrawoid, s.cmpcode, s.suppoid,suppcode, suppname, s.activeflag, ISNULL(cperson1, '') [picname] FROM QL_mstsupp s INNER JOIN QL_mstmatrawdtl r ON r.cmpcode=s.cmpcode AND r.suppoid=s.suppoid WHERE s.cmpcode='" + cmp + "' and activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupp(string cmp)
        {
            List<suppdtl> cust = new List<suppdtl>();
            //sSql = "SELECT cu.*, ci.gendesc [CityDesc] FROM QL_mstsupp cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=suppcityOid WHERE cu.cmpcode='" + cmp + "' AND UPPER(cu.activeflag)='ACTIVE'";
            sSql = "SELECT suppoid, suppcode, suppname, ISNULL(cperson1, '') picname, suppaddr, gendesc [CityDesc], suppphone1, s.activeflag FROM QL_mstsupp s INNER JOIN QL_mstgen g ON g.cmpcode = s.cmpcode AND g.genoid = s.suppcityOid WHERE s.cmpcode = '" + cmp + "' AND s.activeflag = 'ACTIVE' ORDER BY suppname";
            cust = db.Database.SqlQuery<suppdtl>(sSql).ToList();

            return Json(cust, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetManufacture()
        {
            List<mstmanufacture> manufacture = new List<mstmanufacture>();
            sSql = "SELECT cu.manufactureoid, cu.manufacturecode, cu.manufacturename, cu.manufactureaddr, ci.gendesc [citydesc], cu.manufacturephone1 FROM QL_mstmanufacture cu INNER JOIN QL_mstgen ci ON ci.cmpcode=cu.cmpcode AND ci.genoid=manufacturecityOid WHERE cu.cmpcode='" + CompnyCode + "' AND UPPER(cu.activeflag)='ACTIVE'";
            manufacture = db.Database.SqlQuery<mstmanufacture>(sSql).ToList();

            return Json(manufacture, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetsuppData(List<suppdtl> dtDtl)
        {
            Session["QL_mstmatrawdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilsuppData()
        {
            if (Session["QL_mstmatrawdtl"] == null)
            {
                Session["QL_mstmatrawdtl"] = new List<suppdtl>();
            }

            List<suppdtl> dataDtl = (List<suppdtl>)Session["QL_mstmatrawdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileMat()
        {
            var result = "";
            var picturepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        picturepath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, picturepath);
            }
            result = "Sukses";

            return Json(new { result, picturepath }, JsonRequestBehavior.AllowGet);
        }
    }
}
