﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewerNew.aspx.cs" Inherits="RSA.Other.ReportViewerNew" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        html, body, form { width: 100%; height: 100%; margin: 0; padding: 0 }
    </style>
</head>
<body">
    <form id="form1" runat="server">
        <div>
            <CR:CrystalReportViewer ID="CRViewerNew" runat="server"  Width="100%" Height="100%"  AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" ToolPanelView="None" HasDrilldownTabs="False" />
        </div>
    </form>
</body>
</html>