﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;

namespace RSA.Other
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        ReportDocument rd = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            var rptSource = System.Web.HttpContext.Current.Session["rptsource"];
            var rptPaper = System.Web.HttpContext.Current.Session["rptpaper"];
            Dictionary<string, object> rptParam = (Dictionary<string, object>)System.Web.HttpContext.Current.Session["rptparam"];
            var rptfile = Request["rptfile"].Trim();
            //var rptname = Request["rptname"].Trim();
            
            string strRptPath = Server.MapPath("~/") + "Report//" + rptfile;
            //Loading Report
            rd.Load(strRptPath);
            if (rptPaper != null)
                rd.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)rptPaper;

            // Setting report data source
            if (rptSource != null && rptSource.GetType().ToString() != "System.String")
            {
                rd.SetDataSource(rptSource);
            }

            if (rptParam != null && rptParam.Count > 0)
                foreach (var item in rptParam)
                {
                    rd.SetParameterValue(item.Key, item.Value);
                }

            CRViewer.ReportSource = rd;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            try
            {
                if (rd != null)
                {
                    if (rd.IsLoaded)
                    {
                        rd.Dispose();
                        rd.Close();
                    }
                }
            }
            catch
            {
                rd.Dispose();
                rd.Close();
            }
        }
    }
}