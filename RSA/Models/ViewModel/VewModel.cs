﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSA.Models.ViewModel
{

    public class mdFilter
    {
        public string ddlfilter { get; set; }
        public string txtfilter { get; set; }
        public string ddlstatus { get; set; }
        public bool cbfilterperiod { get; set; }
        public string filterperiod1 { get; set; }
        public string filterperiod2 { get; set; }
        public DateTime filterperiodfrom { get; set; }
        public DateTime filterperiodto { get; set; }
        public string filterstatus { get; set; }
        public string filterddl { get; set; }
        public string filtertext { get; set; }
        public bool isperiodchecked { get; set; }
    }

    //public class FilterHPE
    //{
    //    public IEnumerable<SELFIE.Models.DB.QL_t05HPE> HPE { get; set; }
    //    public mdFilter HPEfil { get; set; }
    //}

    public class dtAppHeader
    {
        public string caption { get; set; }
        public string sept { get; set; }
        public string value { get; set; }
    }

    public class dtFiles
    {
        public int seq { get; set; }
        public string filename { get; set; }
        public string fileurl { get; set; }
        public string fileflag { get; set; }
    }


    public class dtUpm
    {
        //Header
        public string cmpcode { get; set; }
        public int upmoid { get; set; }
        public string upmno { get; set; }
        public string upmwono { get; set; }
        public DateTime upmdate { get; set; }
        public int upmtypeoid { get; set; }
        public string upmtype { get; set; }
        public string upmmstnote { get; set; }
        public string crtuser { get; set; }
        public DateTime crttime { get; set; }
        public string appuser1 { get; set; }
        public string appuser2 { get; set; }
        public string appuser3 { get; set; }
        public string crtstname { get; set; }
        public string appstname1 { get; set; }
        public string appstname2 { get; set; }
        public string appstname3 { get; set; }
        public string crtpath { get; set; }
        public string apppath1 { get; set; }
        public string apppath2 { get; set; }
        public string apppath3 { get; set; }
        public Byte[] crtttd { get; set; }
        public Byte[] appttd1 { get; set; }
        public Byte[] appttd2 { get; set; }
        public Byte[] appttd3 { get; set; }
        //Detail
        public int upmdtloid { get; set; }
        public string upmreftype { get; set; }
        public int upmrefoid { get; set; }
        public string upmrefno { get; set; }
        public string upmrefname { get; set; }
        public string upmrefdesc { get; set; }
        public decimal upmqty { get; set; }
        public string upmunit { get; set; }
        public int upmunitoid { get; set; }
        public DateTime upmdateneed { get; set; }
        public string upmreason { get; set; }
        public string upmdtlnote { get; set; }
    }

    public class RoleSpecial
    {
        public string formaddress { get; set; }
        public string special { get; set; }
    }

    public class ModelFilter
    {
        public DateTime filterperiodfrom { get; set; }
        public DateTime filterperiodto { get; set; }
        public string filterstatus { get; set; }
        public string filterddl { get; set; }
        public string filtertext { get; set; }
        public bool isperiodchecked { get; set; }
    }
}