﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSA.Models.DB
{
    public class ReportModels
    {
        public class DDLBusinessUnitModel
        {
            public string divcode { get; set; }
            public string divname { get; set; }
        }

        public class DDLDepartmentModel
        {
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        public class DDLAccountModel
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        public class DDLSingleField
        {
            public string sfield { get; set; }
        }

        public class DDLDoubleField
        {
            public int ifield { get; set; }
            public string sfield { get; set; }
        }

        public class DDLTripleField
        {
            public int ifield { get; set; }
            public string sfield { get; set; }
            public int iorder { get; set; }
        }

        public class DDLDoubleFieldString
        {
            public string valuefield { get; set; }
            public string textfield { get; set; }
        }

        public class DDLReasonModel
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }
		
		public class DDLDivisionModel
        {
            public int groupoid { get; set; }
            public string groupcode { get; set; }
        }
		
		public class DDLWarehouseModel
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }

        public class DDLGroupModel
        {
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
        }
		
		public class DDLUserModel
        {
            public string a { get; set; }

            public string b { get; set; }
        }

        public class DDLBAReturnUserModel
        {
            public string createuser { get; set; }

            public string createuser1 { get; set; }
        }

        public class DDLSupModel
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
        }

        public class FullFormType
        {
            public string formtype { get; set; }
            public string formtitle { get; set; }
            public string reftype { get; set; }
            public string mattype { get; set; }
            public string flagtype { get; set; }
            public string formref { get; set; }
            public string formabbr { get; set; }


            public FullFormType()
            {
                this.formtype = "";
                this.formtitle = "";
                this.reftype = "";
                this.mattype = "";
                this.flagtype = "";
                this.formref = "";
                this.formabbr = "";
            }

            public FullFormType(string formparam)
            {
                if (formparam.Replace(" ", "").ToUpper() == "RAWMATERIAL")
                {
                    this.formtype = "RawMaterial";
                    this.formtitle = "Raw Material";
                    this.reftype = "raw";
                    this.mattype = "matraw";
                    this.flagtype = "A";
                    this.formref = "Raw";
                    this.formabbr = "RM";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "GENERALMATERIAL")
                {
                    this.formtype = "GeneralMaterial";
                    this.formtitle = "General Material";
                    this.reftype = "gen";
                    this.mattype = "matgen";
                    this.flagtype = "A";
                    this.formref = "General";
                    this.formabbr = "GM";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "SPAREPART")
                {
                    this.formtype = "SparePart";
                    this.formtitle = "Spare Part";
                    this.reftype = "sp";
                    this.mattype = "sparepart";
                    this.flagtype = "A";
                    this.formref = "Spare Part";
                    this.formabbr = "SP";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "FINISHGOOD")
                {
                    this.formtype = "FinishGood";
                    this.formtitle = "Finish Good";
                    this.reftype = "item";
                    this.mattype = "item";
                    this.flagtype = "A";
                    this.formref = "Finish Good";
                    this.formabbr = "FG";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "FIXEDASSETS")
                {
                    this.formtype = "FixedAssets";
                    this.formtitle = "Fixed Assets";
                    this.reftype = "asset";
                    this.mattype = "";
                    this.flagtype = "B";
                    this.formref = "Asset";
                    this.formabbr = "FA";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "LOG")
                {
                    this.formtype = "Log";
                    this.formtitle = "Log";
                    this.reftype = "wip";
                    this.mattype = "log";
                    this.flagtype = "C";
                    this.formref = "Log";
                    this.formabbr = "LOG";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "SAWNTIMBER")
                {
                    this.formtype = "SawnTimber";
                    this.formtitle = "Sawn Timber";
                    this.reftype = "sawn";
                    this.mattype = "pallet";
                    this.flagtype = "C";
                    this.formref = "Sawn";
                    this.formabbr = "ST";
                }
            }
        }

        public class dropdownlistdata
        {
            public string ddlvalue { get; set; }
            public string ddltext { get; set; }

            public dropdownlistdata(string dval, string dtext)
            {
                ddlvalue = dval;
                ddltext = dtext;
            }
        }

        public class SQLQuery
        {
            public string _select { get; set; }
            public string _from { get; set; }
            public string _where { get; set; }
            public string _order { get; set; }

            public SQLQuery()
            {
                _select = ""; _from = ""; _where = ""; _order = "";
            }

            public SQLQuery(string _s, string _f, string _w, string _o)
            {
                _select = _s; _from = _f; _where = _w; _order = _o;
            }

            public string Concat(bool no_order = false)
            {
                return $"{_select} {_from} {_where} {(no_order ? "" : _order)}";
            }

            public string ConcatIn()
            {
                return $"select * from ({_select} {_from} {_where}) t {_order}";
            }
        }

        public class PrintOutRequest
        {
            public string rpt_name { get; set; }
            public PrintOutSysView view_param { get; set; }
            public List<PrintOutSysView> sub_view_param { get; set; }
            public string file_name { get; set; }
            public Dictionary<string, string> rpt_param { get; set; }
            public System.Data.DataTable rpt_src { get; set; }
            public List<System.Data.DataTable> rpt_sub_src { get; set; }
            public string query { get; set; }
            public List<string> query_sub { get; set; }
            public bool rpt_logo { get; set; }
        }

        public class PrintOutSysView
        {
            public string view_name { get; set; }
            public string view_filter { get; set; }

            public PrintOutSysView(string viewName, string viewFilter)
            {
                view_name = viewName;
                view_filter = viewFilter;
            }
        }
    }
}