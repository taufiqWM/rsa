//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnpretassetdtl
    {
        public string cmpcode { get; set; }
        public int pretassetdtloid { get; set; }
        public int pretassetmstoid { get; set; }
        public int pretassetdtlseq { get; set; }
        public int mrassetmstoid { get; set; }
        public int mrassetdtloid { get; set; }
        public int pretassetwhoid { get; set; }
        public string pretassetreftype { get; set; }
        public int pretassetrefoid { get; set; }
        public decimal pretassetqty { get; set; }
        public int pretassetunitoid { get; set; }
        public string pretassetdtlstatus { get; set; }
        public string pretassetdtlnote { get; set; }
        public string pretassetdtlres1 { get; set; }
        public string pretassetdtlres2 { get; set; }
        public string pretassetdtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public decimal pretassetvalue { get; set; }
        public decimal pretassetvalueidr { get; set; }
        public decimal pretassetvalueusd { get; set; }
        public int reasonoid { get; set; }
    }
}
