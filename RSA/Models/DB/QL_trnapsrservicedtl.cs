//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnapsrservicedtl
    {
        public string cmpcode { get; set; }
        public int apsrservicedtloid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public int apsrservicemstoid { get; set; }
        public int apsrservicedtlseq { get; set; }
        public int srserviceresmstoid { get; set; }
        public int srserviceresdtloid { get; set; }
        public int costacctgoid { get; set; }
        public decimal apsrserviceqty { get; set; }
        public int apsrserviceunitoid { get; set; }
        public decimal apsrserviceprice { get; set; }
        public decimal apsrservicedtlamt { get; set; }
        public string apsrservicedtlnote { get; set; }
        public string apsrservicedtlstatus { get; set; }
        public string apsrservicedtlres1 { get; set; }
        public string apsrservicedtlres2 { get; set; }
        public string apsrservicedtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
