//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnapsubcondtl
    {
        public string cmpcode { get; set; }
        public int apsubcondtloid { get; set; }
        public int apsubconmstoid { get; set; }
        public int apsubcondtlseq { get; set; }
        public int registermstoid { get; set; }
        public int registerdtloid { get; set; }
        public int mrsubconmstoid { get; set; }
        public int mrsubcondtloid { get; set; }
        public string matreftype { get; set; }
        public int matrefoid { get; set; }
        public decimal apsubconqty { get; set; }
        public int apsubconunitoid { get; set; }
        public decimal apsubconprice { get; set; }
        public decimal apsubcondtlamt { get; set; }
        public string apsubcondtldisctype { get; set; }
        public Nullable<decimal> apsubcondtldiscvalue { get; set; }
        public Nullable<decimal> apsubcondtldiscamt { get; set; }
        public decimal apsubcondtlnetto { get; set; }
        public decimal apsubcondtltaxamt { get; set; }
        public string apsubcondtlnote { get; set; }
        public string apsubcondtlstatus { get; set; }
        public string apsubcondtlres1 { get; set; }
        public string apsubcondtlres2 { get; set; }
        public string apsubcondtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
