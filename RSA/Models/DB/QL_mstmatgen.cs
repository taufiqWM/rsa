//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstmatgen
    {
        public string cmpcode { get; set; }
        public int matgenoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public int cat1oid { get; set; }
        public int cat2oid { get; set; }
        public int cat3oid { get; set; }
        public string matgencode { get; set; }
        public int matgenunitoid { get; set; }
        public string matgenshortdesc { get; set; }
        public string matgenlongdesc { get; set; }
        public decimal matgensafetystock { get; set; }
        public Nullable<decimal> matgenlimitqty { get; set; }
        public int suppoid { get; set; }
        public int manufactureoid { get; set; }
        public string matgenbarcode { get; set; }
        public string matgenpictureloc { get; set; }
        public string matgenpicturemat { get; set; }
        public int matgenlocoid { get; set; }
        public Nullable<decimal> matgenavgsales { get; set; }
        public Nullable<decimal> matgenavgpurchase { get; set; }
        public Nullable<decimal> matgenlastpoprice { get; set; }
        public string matgennote { get; set; }
        public string matgenres1 { get; set; }
        public string matgenres2 { get; set; }
        public string matgenres3 { get; set; }
        public string activeflag { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public int matgenunit2oid { get; set; }
        public decimal matgenconvertunit { get; set; }
        public Nullable<int> acctgoid { get; set; }
        public int cat4oid { get; set; }
    }
}
