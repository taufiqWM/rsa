//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstgen
    {
        public string cmpcode { get; set; }
        public int genoid { get; set; }
        public string gencode { get; set; }
        public string gendesc { get; set; }
        public string gengroup { get; set; }
        public string genother1 { get; set; }
        public string genother2 { get; set; }
        public string genother3 { get; set; }
        public Nullable<int> genother4 { get; set; }
        public string genother5 { get; set; }
        public string genother6 { get; set; }
        public string activeflag { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public Nullable<int> acctgoid { get; set; }
        public string genother7 { get; set; }
        public string genother8 { get; set; }
    }
}
