//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trntransformdivmst
    {
        public string cmpcode { get; set; }
        public int transformmstoid { get; set; }
        public int divgroupfromoid { get; set; }
        public int divgrouptooid { get; set; }
        public string transformno { get; set; }
        public System.DateTime transformdate { get; set; }
        public int transformwhoid { get; set; }
        public int custoid { get; set; }
        public string custname { get; set; }
        public int sheetoid { get; set; }
        public string sheetdesc { get; set; }
        public decimal sheetvalue { get; set; }
        public string transformmststatus { get; set; }
        public string transformmstnote { get; set; }
        public decimal transformtotalvalue { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
