//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trndoassetdtl
    {
        public string cmpcode { get; set; }
        public int doassetdtloid { get; set; }
        public int doassetmstoid { get; set; }
        public int doassetdtlseq { get; set; }
        public int soassetmstoid { get; set; }
        public int soassetdtloid { get; set; }
        public int assetmstoid { get; set; }
        public decimal doassetqty { get; set; }
        public int doassetunitoid { get; set; }
        public string doassetdtlnote { get; set; }
        public string doassetdtlres1 { get; set; }
        public string doassetdtlres2 { get; set; }
        public string doassetdtlres3 { get; set; }
        public string doassetdtlstatus { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public decimal doassetprice { get; set; }
        public decimal doassetdtlamt { get; set; }
    }
}
