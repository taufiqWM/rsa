//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnmrgenmst
    {
        public string cmpcode { get; set; }
        public int mrgenmstoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public string periodacctg { get; set; }
        public System.DateTime mrgendate { get; set; }
        public string mrgenno { get; set; }
        public int suppoid { get; set; }
        public int registermstoid { get; set; }
        public int pogenmstoid { get; set; }
        public int mrgenwhoid { get; set; }
        public string mrgenmstnote { get; set; }
        public string mrgenmststatus { get; set; }
        public string mrgenmstres1 { get; set; }
        public string mrgenmstres2 { get; set; }
        public string mrgenmstres3 { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public int curroid { get; set; }
    }
}
