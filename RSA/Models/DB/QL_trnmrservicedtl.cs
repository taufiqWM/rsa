//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnmrservicedtl
    {
        public string cmpcode { get; set; }
        public int mrservicedtloid { get; set; }
        public int mrservicemstoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public int mrservicedtlseq { get; set; }
        public int poservicedtloid { get; set; }
        public int serviceoid { get; set; }
        public string mrservicedtlstatus { get; set; }
        public string mrservicedtlnote { get; set; }
        public string mrservicedtlres1 { get; set; }
        public string mrservicedtlres2 { get; set; }
        public string mrservicedtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public int mrserviceqty { get; set; }
        public int mrserviceunitoid { get; set; }
        public Nullable<decimal> mrservicevalue { get; set; }
        public Nullable<decimal> mrservicevalueidr { get; set; }
        public Nullable<decimal> mrservicevalueusd { get; set; }
    }
}
