//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnapspmst
    {
        public string cmpcode { get; set; }
        public int apspmstoid { get; set; }
        public string periodacctg { get; set; }
        public System.DateTime apspdate { get; set; }
        public string apspno { get; set; }
        public int suppoid { get; set; }
        public int apsppaytypeoid { get; set; }
        public int curroid { get; set; }
        public int rateoid { get; set; }
        public string apspratetoidr { get; set; }
        public string apspratetousd { get; set; }
        public int rate2oid { get; set; }
        public string apsprate2toidr { get; set; }
        public string apsprate2tousd { get; set; }
        public decimal apsptotalamt { get; set; }
        public decimal apsptotalamtidr { get; set; }
        public decimal apsptotalamtusd { get; set; }
        public Nullable<decimal> apsptotaldisc { get; set; }
        public Nullable<decimal> apsptotaldiscidr { get; set; }
        public Nullable<decimal> apsptotaldiscusd { get; set; }
        public Nullable<decimal> apsptotaltax { get; set; }
        public Nullable<decimal> apsptotaltaxidr { get; set; }
        public Nullable<decimal> apsptotaltaxusd { get; set; }
        public decimal apspgrandtotal { get; set; }
        public decimal apspgrandtotalidr { get; set; }
        public decimal apspgrandtotalusd { get; set; }
        public string apspmstnote { get; set; }
        public string apspmststatus { get; set; }
        public string apspmstres1 { get; set; }
        public string apspmstres2 { get; set; }
        public string apspmstres3 { get; set; }
        public string approvaluser { get; set; }
        public string approvalcode { get; set; }
        public Nullable<System.DateTime> approvaldatetime { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public decimal apspsupptotal { get; set; }
        public System.DateTime apspdatetakegiro { get; set; }
        public string revisereason { get; set; }
        public string reviseuser { get; set; }
        public System.DateTime revisetime { get; set; }
        public string rejectreason { get; set; }
        public string rejectuser { get; set; }
        public System.DateTime rejecttime { get; set; }
        public string closereason { get; set; }
        public string closeuser { get; set; }
        public Nullable<System.DateTime> closetime { get; set; }
    }
}
