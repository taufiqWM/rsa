//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstprof
    {
        public string cmpcode { get; set; }
        public string profoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public string profname { get; set; }
        public string profpass { get; set; }
        public Nullable<int> profapplimit { get; set; }
        public string activeflag { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public int personoid { get; set; }
        public string proftype { get; set; }
        public string profimgfile { get; set; }
        public string proflevel { get; set; }
    }
}
