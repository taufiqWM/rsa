//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnmatreqdtl
    {
        public string cmpcode { get; set; }
        public int matreqdtloid { get; set; }
        public int matreqmstoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public int matreqdtlseq { get; set; }
        public string matreqreftype { get; set; }
        public int matreqrefoid { get; set; }
        public decimal matreqqty { get; set; }
        public int matrequnitoid { get; set; }
        public string matreqdtlnote { get; set; }
        public string matreqdtlstatus { get; set; }
        public string matreqdtlres1 { get; set; }
        public string matreqdtlres2 { get; set; }
        public string matreqdtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
