//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnshipmentassetmst
    {
        public string cmpcode { get; set; }
        public int shipmentassetmstoid { get; set; }
        public string periodacctg { get; set; }
        public string shipmentassetno { get; set; }
        public System.DateTime shipmentassetdate { get; set; }
        public int custoid { get; set; }
        public string shipmentassetcontno { get; set; }
        public string shipmentassetportship { get; set; }
        public System.DateTime shipmentassetetd { get; set; }
        public string shipmentassetportdischarge { get; set; }
        public System.DateTime shipmentasseteta { get; set; }
        public int vhcoid { get; set; }
        public int drivoid { get; set; }
        public string shipmentassetnopol { get; set; }
        public string shipmentassetmstnote { get; set; }
        public string shipmentassetmstres1 { get; set; }
        public string shipmentassetmstres2 { get; set; }
        public string shipmentassetmstres3 { get; set; }
        public string shipmentassetmststatus { get; set; }
        public string approvaluser { get; set; }
        public string approvalcode { get; set; }
        public Nullable<System.DateTime> approvaldatetime { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public int curroid { get; set; }
    }
}
