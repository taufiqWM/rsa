//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trntransrawdtl
    {
        public string cmpcode { get; set; }
        public int transrawdtloid { get; set; }
        public int divgroupoid { get; set; }
        public int transrawmstoid { get; set; }
        public int transrawdtlseq { get; set; }
        public int transrawfromwhoid { get; set; }
        public int transrawtowhoid { get; set; }
        public int matrawoid { get; set; }
        public decimal transrawqty { get; set; }
        public int transrawunitoid { get; set; }
        public string transrawdtlnote { get; set; }
        public string transrawdtlstatus { get; set; }
        public string transrawdtlres1 { get; set; }
        public string transrawdtlres2 { get; set; }
        public string transrawdtlres3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public Nullable<int> transrawfrompalletoid { get; set; }
        public Nullable<int> transrawtopalletoid { get; set; }
        public decimal transrawvalueidr { get; set; }
        public decimal transrawvalueusd { get; set; }
        public string refno { get; set; }
        public string serialno { get; set; }
    }
}
