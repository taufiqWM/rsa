//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_prassetmst
    {
        public string cmpcode { get; set; }
        public int prassetmstoid { get; set; }
        public string periodacctg { get; set; }
        public int divoid { get; set; }
        public int deptoid { get; set; }
        public System.DateTime prassetdate { get; set; }
        public string prassetno { get; set; }
        public string prassettype { get; set; }
        public System.DateTime prassetexpdate { get; set; }
        public string prassetmstnote { get; set; }
        public string prassetmststatus { get; set; }
        public string prassetmstres1 { get; set; }
        public string prassetmstres2 { get; set; }
        public string prassetmstres3 { get; set; }
        public string approvaluser { get; set; }
        public string approvalcode { get; set; }
        public Nullable<System.DateTime> approvaldatetime { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public string closereason { get; set; }
        public string closeuser { get; set; }
        public Nullable<System.DateTime> closetime { get; set; }
        public int divgroupoid { get; set; }
        public string revisereason { get; set; }
        public string reviseuser { get; set; }
        public System.DateTime revisetime { get; set; }
        public string rejectreason { get; set; }
        public string rejectuser { get; set; }
        public System.DateTime rejecttime { get; set; }
    }
}
