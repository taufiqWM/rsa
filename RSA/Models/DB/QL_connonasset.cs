//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_connonasset
    {
        public string cmpcode { get; set; }
        public int connonassetoid { get; set; }
        public System.DateTime trndate { get; set; }
        public string periodacctg { get; set; }
        public string refname { get; set; }
        public int refoid { get; set; }
        public int locoid { get; set; }
        public decimal qtyin { get; set; }
        public decimal qtyout { get; set; }
        public int unitoid { get; set; }
        public string refno { get; set; }
        public string note { get; set; }
        public string res1 { get; set; }
        public string res2 { get; set; }
        public string res3 { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public decimal valueidr { get; set; }
        public decimal valueusd { get; set; }
    }
}
