//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trncashbankmst
    {
        public string cmpcode { get; set; }
        public int cashbankoid { get; set; }
        public Nullable<int> divgroupoid { get; set; }
        public string periodacctg { get; set; }
        public string cashbankno { get; set; }
        public System.DateTime cashbankdate { get; set; }
        public string cashbanktype { get; set; }
        public string cashbankgroup { get; set; }
        public int acctgoid { get; set; }
        public int curroid { get; set; }
        public decimal cashbankamt { get; set; }
        public decimal cashbankamtidr { get; set; }
        public decimal cashbankamtusd { get; set; }
        public Nullable<int> personoid { get; set; }
        public Nullable<System.DateTime> cashbankduedate { get; set; }
        public string cashbankrefno { get; set; }
        public string cashbanknote { get; set; }
        public string cashbankres1 { get; set; }
        public string cashbankres2 { get; set; }
        public string cashbankres3 { get; set; }
        public string cashbankstatus { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public System.DateTime cashbanktakegiro { get; set; }
        public int giroacctgoid { get; set; }
        public int refsuppoid { get; set; }
        public string cashbanktaxtype { get; set; }
        public decimal cashbanktaxpct { get; set; }
        public decimal cashbanktaxamt { get; set; }
        public decimal cashbankothertaxamt { get; set; }
        public decimal cashbankdpp { get; set; }
        public System.DateTime cashbanktakegiroreal { get; set; }
        public System.DateTime cashbankgiroreal { get; set; }
        public decimal cashbankresamt { get; set; }
        public int addacctgoid1 { get; set; }
        public decimal addacctgamt1 { get; set; }
        public int addacctgoid2 { get; set; }
        public decimal addacctgamt2 { get; set; }
        public int addacctgoid3 { get; set; }
        public decimal addacctgamt3 { get; set; }
        public string cashbankaptype { get; set; }
        public int cashbankapoid { get; set; }
        public int deptoid { get; set; }
        public Nullable<int> groupoid { get; set; }
    }
}
