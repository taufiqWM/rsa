//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSA.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstinterfacedtl
    {
        public string cmpcode { get; set; }
        public int interfacedtloid { get; set; }
        public int interfaceseq { get; set; }
        public int interfaceoid { get; set; }
        public int acctgoid { get; set; }
        public string acctgcode { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
